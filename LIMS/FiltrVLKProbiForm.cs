﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using static LIMS.FiltrVLKProbiClass;

namespace LIMS
{
    public partial class FiltrVLKProbiForm : DevExpress.XtraEditors.XtraForm
    {
        FiltrVLKProbi FiltrVLK = new FiltrVLKProbi();
        public FiltrVLKProbiForm(MainForm MF, string AnalizID, int? OperKontrolID = null)
        {
            InitializeComponent();
            try
            {
                FiltrVLK.OperKontrolID = OperKontrolID;
                FiltrVLK.AnalizID = AnalizID;                
                var Mounths = new List<string>();
                for (int i = -11; i < 1; i++)
                {
                    Mounths.Add(DateTime.Now.AddMonths(i).ToString("MMMM yyyy", CultureInfo.CreateSpecificCulture("ru-RU")));
                }
                MounthComboBox.DataSource = Mounths;
                MounthComboBox.SelectedIndex = 10;
                ShifrOt3.Value = Convert.ToInt32(DateTime.Now.Year.ToString().Substring(2));
                ShifrDo3.Value = Convert.ToInt32(DateTime.Now.Year.ToString().Substring(2));
                using (DbDataContext db = new DbDataContext())
                {
                    var Analiz = db.Analiz.Where(c => c.Id == Convert.ToInt32(AnalizID)).FirstOrDefault();
                    if (Analiz != null)
                    {
                        var NewText = "";
                        if (Analiz.Шифр.Length >= 3)
                            NewText = Analiz.Шифр + "-";
                        if(Analiz.Шифр.Length == 2)
                            NewText = Analiz.Шифр + " -";
                        if (Analiz.Шифр.Length == 1)
                            NewText = "  " + Analiz.Шифр + " -";
                        ShifrOt1.Text = NewText;
                        ShifrDo1.Text = NewText;
                    }
                    db.Dispose();
                }
                var Filtr = GetFiltr(AnalizID);
                if (Filtr != null)
                {
                    if (Filtr.StartDate.HasValue || Filtr.EndDate.HasValue)
                    {
                        if (Filtr.StartDate != null)
                            DateOt.Value = Filtr.StartDate.Value;
                        else
                            DateOt.Checked = false;
                        if (Filtr.EndDate != null)
                            DateDo.Value = Filtr.EndDate.Value;
                        else
                            DateDo.Checked = false;
                        DateCheckBox.Checked = true;
                        if (Filtr.StartDate.HasValue && Filtr.EndDate.HasValue)
                        {
                            if (Filtr.StartDate.Value.AddMonths(1) == Filtr.EndDate.Value.AddDays(1))
                            {                                
                                MounthComboBox.SelectedItem = Filtr.StartDate.Value.ToString("MMMM yyyy", CultureInfo.CreateSpecificCulture("ru-RU"));
                                MounthRadioButton.Checked = true;
                            }
                        }
                        
                    }

                    if (Filtr.ShifrDoN.HasValue || Filtr.ShifrOtN.HasValue)
                    {
                        if (Filtr.ShifrOtN.HasValue)
                        {
                            ShifrOt2.Value = Filtr.ShifrOtN.Value;
                            ShifrOt3.Value = Filtr.ShifrOtYear.HasValue ? Filtr.ShifrOtYear.Value : 0;
                        }
                        else
                            ShifrOtCheckBox.Checked = false;
                        if (Filtr.ShifrDoN.HasValue)
                        {
                            ShifrDo2.Value = Filtr.ShifrDoN.Value;
                            ShifrDo3.Value = Filtr.ShifrDoYear.HasValue ? Filtr.ShifrDoYear.Value : 0;
                        }
                        else
                            ShifrDoCheckBox.Checked = false;
                        ShifrCheckBox.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (DateCheckBox.Checked)
                {
                    if (MounthRadioButton.Checked)
                    {
                        var SelectedDate = DateTime.Now.AddMonths(MounthComboBox.SelectedIndex - 11);
                        var mounth = SelectedDate.Month;
                        var year = SelectedDate.Year;
                        var date = new DateTime(year, mounth, 1);
                        FiltrVLK.StartDate = new DateTime(year, mounth, 1);
                        FiltrVLK.EndDate = new DateTime(year, mounth, 1).AddMonths(1).AddDays(-1);
                    }
                    else //DateRadioButton.Checked
                    {
                        if (DateOt.Checked)
                            FiltrVLK.StartDate = DateOt.Value;
                        else
                            FiltrVLK.StartDate = null;
                        if (DateDo.Checked)
                            FiltrVLK.EndDate = DateDo.Value;
                        else
                            FiltrVLK.EndDate = null;
                    }
                }
                else
                {
                    FiltrVLK.StartDate = null;
                    FiltrVLK.EndDate = null;
                }
                if (ShifrCheckBox.Checked)
                {
                    if (ShifrOtCheckBox.Checked)
                    {
                        FiltrVLK.ShifrOtN = Convert.ToInt32(ShifrOt2.Value);
                        FiltrVLK.ShifrOtYear = Convert.ToInt32(ShifrOt3.Value);
                    }
                    else
                    {
                        FiltrVLK.ShifrOtN = null;
                        FiltrVLK.ShifrOtYear = null;
                    }
                    if (ShifrDoCheckBox.Checked)
                    {
                        FiltrVLK.ShifrDoN = Convert.ToInt32(ShifrDo2.Value);
                        FiltrVLK.ShifrDoYear = Convert.ToInt32(ShifrDo3.Value);
                    }
                    else
                    {
                        FiltrVLK.ShifrDoN = null;
                        FiltrVLK.ShifrDoYear = null;
                    }
                }
                else
                {
                    FiltrVLK.ShifrOtN = null;
                    FiltrVLK.ShifrOtYear = null;
                    FiltrVLK.ShifrDoN = null;
                    FiltrVLK.ShifrDoYear = null;
                }
                AddNewFiltr(FiltrVLK);
                this.Close();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            DateCheckBox_CheckedChanged(DateCheckBox.Checked);
        }

        private void DateCheckBox_CheckedChanged(bool Checked)
        {
            MounthRadioButton.Enabled = Checked;
            DateRadioButton.Enabled = Checked;
            MounthComboBox.Enabled = MounthRadioButton.Checked ? Checked : false;
            DateOt.Enabled = DateRadioButton.Checked ? Checked : false;
            DateDo.Enabled = DateRadioButton.Checked ? Checked : false;
        }

        private void ShifrCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ShifrCheckBox_CheckedChanged(ShifrCheckBox.Checked);
        }

        private void ShifrCheckBox_CheckedChanged(bool Checked)
        {
            ShifrOtCheckBox.Enabled = Checked;
            ShifrDoCheckBox.Enabled = Checked;
            ShifrOt1.Enabled = ShifrOtCheckBox.Checked ? Checked : false;
            ShifrOt2.Enabled = ShifrOtCheckBox.Checked ? Checked : false;
            ShifrOt3.Enabled = ShifrOtCheckBox.Checked ? Checked : false;
            ShifrDo1.Enabled = ShifrDoCheckBox.Checked ? Checked : false;
            ShifrDo2.Enabled = ShifrDoCheckBox.Checked ? Checked : false;
            ShifrDo3.Enabled = ShifrDoCheckBox.Checked ? Checked : false;

        }

        private void MounthComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var SelectedDate = DateTime.Now.AddMonths(MounthComboBox.SelectedIndex - 11);
            var mounth = SelectedDate.Month;
            var year = SelectedDate.Year;
            var date = new DateTime(year, mounth, 1);
            DateOt.Value = new DateTime(year, mounth, 1);
            DateDo.Value = new DateTime(year, mounth, 1).AddMonths(1).AddDays(-1);
        }

        private void MounthRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            MounthComboBox.Enabled = MounthRadioButton.Checked;
        }

        private void DateRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            DateDo.Enabled = DateRadioButton.Checked;
            DateOt.Enabled = DateRadioButton.Checked;
        }

        private void ShifrOtCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ShifrOt1.Enabled = ShifrOtCheckBox.Checked;
            ShifrOt2.Enabled = ShifrOtCheckBox.Checked;
            ShifrOt3.Enabled = ShifrOtCheckBox.Checked;
        }

        private void ShifrDoCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ShifrDo1.Enabled = ShifrDoCheckBox.Checked;
            ShifrDo2.Enabled = ShifrDoCheckBox.Checked;
            ShifrDo3.Enabled = ShifrDoCheckBox.Checked;
        }

        private void DateCheckBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void FiltrVLKProbiForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
