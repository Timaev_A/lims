﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LIMS
{
    class WordDocument
    {
        Object wordMissing = System.Reflection.Missing.Value;
        Object wordTrue = true;
        Object wordFalse = false;

        public Microsoft.Office.Interop.Word._Application wordApplication;
        public Microsoft.Office.Interop.Word._Document wordDocument;
        Object templatePathObj;

        public bool Visible
        {
            get { return wordApplication.Visible; }
            set { 
                using (DbDataContext db = new DbDataContext())
                {
                    if (UserSettings.Protect)
                    {
                        var Use = db.Users.Where(c => c.UserName == UserSettings.User && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1 || c.VisibleStatus == 2)).FirstOrDefault();
                        if (Use != null && Use.Password != null && Use.UseDocProtect)
                        {                            
                            var P1 = (object)Crypt.Decrypt(Use.Password, true);
                            var P3 = (object)false;
                            var P4 = (object)true;
                            wordDocument.Protect(WdProtectionType.wdAllowOnlyReading, ref P3, ref P1, ref P3, ref P4);
                        }
                    }
                    db.Dispose();
                    wordApplication.Visible = value;                    
                    //Курсор ввода устанавливается в начало документа
                    object unit = Microsoft.Office.Interop.Word.WdUnits.wdStory;
                    object extend = Microsoft.Office.Interop.Word.WdMovementType.wdMove;
                    wordApplication.Selection.HomeKey(ref unit, ref extend);
                    
                    wordApplication.WindowState = WdWindowState.wdWindowStateMaximize;
                }
            }
        }

        public void AddTableCell(int TableNumber, int _Count)
        {
            for (int i = 0; i < _Count; i++)
            {
                wordDocument.Tables[TableNumber].Rows.Add();
            }
        }

        public void AddTable(System.Data.DataTable table)
        {
            int i = 1;
            foreach (DataRow row in table.Rows)
            {
                wordDocument.Tables[1].Cell(i + 1, 0).Range.Text = i.ToString();
                for (int j = 1; j < row.ItemArray.Length; j++)
                    wordDocument.Tables[1].Cell(i + 1, j + 1).Range.Text = row.ItemArray[j].ToString();

                if (i < table.Rows.Count) wordDocument.Tables[1].Rows.Add();
                i++;
            }
        }

        public void SaveAs(string path)
        {
            object file = path;
            object missing = System.Reflection.Missing.Value;
            wordDocument.SaveAs(ref file, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
                                ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);

        }

        public WordDocument(string templatePath = "")
        {
            if (templatePath != "")
            {
                createFromTemplate(templatePath);
            }
        }

        public void createFromTemplate(string templatePath)
        {
            //создаем обьект приложения word 
            wordApplication = new Microsoft.Office.Interop.Word.Application();
            wordApplication.Visible = false;
            //создаем обьект документа word 
            wordDocument = new Microsoft.Office.Interop.Word.Document();


            // создаем путь к файлу используя имя файла 
            templatePathObj = templatePath;

            //добавляем шаблон новым документом в наше приложение word 
            wordDocument = wordApplication.Documents.Add(ref templatePathObj, ref wordMissing, ref wordMissing, ref wordMissing);
        }



        public void DoSearchAndReplaceInWord(string strToFind, string replaceStr)
        {
            object missing = System.Type.Missing;
            try
            {
                // Activate the document
                wordDocument.Activate();

                foreach (Microsoft.Office.Interop.Word.Range tmpRange in wordDocument.StoryRanges)
                {
                    Microsoft.Office.Interop.Word.Range rng = tmpRange;
                    while (rng != null)
                    {
                        int rngc = rng.StoryLength;
                        string mt = rng.Text;

                        rng.Find.ClearFormatting();
                        rng.Find.Execute(strToFind, ReplaceWith: replaceStr, Replace: Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll);                        
                        rng = rng.NextStoryRange;
                    }
                    //{
                    //tmpRange.Find.Text = strToFind;
                    //tmpRange.Find.Replacement.Text = replaceStr;
                    //tmpRange.Find.Wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue;

                    //tmpRange.Find.ClearFormatting();

                    //object matchCase = false;
                    //object matchWholeWord = true;
                    //object matchWildCards = false;
                    //object matchSoundsLike = false;
                    //object matchAllWordForms = false;
                    //object forward = true;
                    //object format = false;
                    //object matchKashida = false;
                    //object matchDiacritics = false;
                    //object matchAlefHamza = false;
                    //object matchControl = false;
                    //object read_only = false;
                    //object visible = true;
                    //object replace = 2;//2;
                    //object wrap = 1;//1;


                    //wordDocument.Content.Find.Execute(strToFind, false, true, false, false, false, true, 1, false, replaceStr, 2,
                    //false, false, false, false);
                    //}

                    
                    //            wordApplication.Selection.Find.Execute(strToFind, ref matchCase, ref matchWholeWord,
                    //ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, replaceStr, ref replace,
                    //ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);   

                }
                //wordDocument.Save();
            }
            catch (Exception ex)
            {
                wordDocument.Close(ref missing, ref missing, ref missing);
                wordApplication.Application.Quit(ref missing, ref missing, ref missing);
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void VerticalTableValue(int Table, int Строка, int Столбец)
        {
           wordDocument.Tables[Table].Cell(Строка, Столбец).Range.FormattedText.Orientation = WdTextOrientation.wdTextOrientationUpward;
        }

        internal void MergeCells(int Table, int Start, int End, int Столбец)
        {            
            wordDocument.Tables[Table].Cell(Start, Столбец).Merge(wordDocument.Tables[Table].Cell(End, Столбец));
        }

        internal void MergeCells(int Table, int Start, int End, int СтолбецStart, int СтолбецEnd)
        {            
            wordDocument.Tables[Table].Cell(Start, СтолбецStart).Merge(wordDocument.Tables[Table].Cell(End, СтолбецEnd));            
        }

        internal void AddTableValue(int Table, int Строка, int Столбец, string Content)
        {            
            wordDocument.Tables[Table].Cell(Строка, Столбец).Range.Text = Content;
        }        

        internal string TableValue(int Table, int Строка, int Столбец)
        {           
            return wordDocument.Tables[Table].Cell(Строка, Столбец).Range.Text;            
        }

        internal void DeleteTableColumn(int Table, int Столбец)
        {
            wordDocument.Tables[Table].Columns[Столбец].Delete();
        }

        // ПОИСК И ЗАМЕНА ЗАДАННОЙ СТРОКИ 
        public void ReplaceString(string strToFind, string replaceStr)
        {
            DoSearchAndReplaceInWord(strToFind, replaceStr);
        }

        // закрытие открытого документа и приложения 
        public void Close()
        {
            try
            {
                wordDocument.Close(ref wordFalse, ref wordMissing, ref wordMissing);
                wordApplication.Quit(ref wordMissing, ref wordMissing, ref wordMissing);
                wordDocument = null;
                wordApplication = null;
                GC.Collect();
                GC.Collect();
            }
            catch
            {
            }
        }

        public void Завершить_Word_Proccess()
        {
            try
            {
                string target_name = "WINWORD";
                System.Diagnostics.Process[] local_procs = System.Diagnostics.Process.GetProcesses();
                List<System.Diagnostics.Process> target_proc = local_procs.Where(p => p.ProcessName.Contains(target_name)).ToList();
                if (target_proc != null && target_proc.Count() > 0)
                {
                    foreach (var i in target_proc)
                    {
                        i.Kill();
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }


        public void Print(ReactivPrintAktvVhodnogoKontrolaForm RF)
        {
            RF.TopMost = false;
            wordDocument.Activate();
            wordApplication.Visible = true;
            var dialogResult = wordApplication.Dialogs[WdWordDialog.wdDialogFilePrint].Show();
            if (dialogResult == 1)
                wordDocument.PrintOut();
        }

        internal void DelTabl(int Number)
        {
            wordDocument.Tables[Number].Delete();
        }

        internal void TableResize(int Table)
        {
            wordDocument.Tables[Table].AutoFitBehavior(WdAutoFitBehavior.wdAutoFitContent);
            wordDocument.Tables[Table].AutoFitBehavior(WdAutoFitBehavior.wdAutoFitWindow);            
        }

        internal void MergeColumns(int Table, int Start, int End, int Строка)
        {
            wordDocument.Tables[Table].Cell(Строка, Start).Merge(wordDocument.Tables[Table].Cell(Строка, End));
        }

        internal void BoltText(object findText)
        {
            object missing = Type.Missing;
            object matchCase = true;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundLike = false;
            object nmatchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            wordApplication.Selection.Find.Execute(ref findText,
                           ref matchCase,
                           ref matchWholeWord,
                           ref matchWildCards,
                           ref matchSoundLike,
                           ref nmatchAllWordForms,
                           ref forward,
                           ref wrap,
                           ref format,
                           ref missing,
                           ref missing,
                           ref matchKashida,
                           ref matchDiacritics,
                           ref matchAlefHamza,
                           ref matchControl);            
            wordApplication.Application.Selection.Font.Bold = 1;            
        }
    }
}

