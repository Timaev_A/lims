﻿namespace LIMS
{
    partial class PredeliForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PredeliForm));
            this.label1 = new System.Windows.Forms.Label();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.RBig = new System.Windows.Forms.Label();
            this.rSmall = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.A2l = new System.Windows.Forms.Label();
            this.A1 = new System.Windows.Forms.Label();
            this.O3Rl = new System.Windows.Forms.Label();
            this.O2R = new System.Windows.Forms.Label();
            this.O1r = new System.Windows.Forms.Label();
            this.RKBig = new System.Windows.Forms.Label();
            this.rKSmall = new System.Windows.Forms.Label();
            this.X = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.Tabl12 = new System.Windows.Forms.Label();
            this.Tabl13 = new System.Windows.Forms.Label();
            this.Tabl21 = new System.Windows.Forms.Label();
            this.Tabl22 = new System.Windows.Forms.Label();
            this.Tabl23 = new System.Windows.Forms.Label();
            this.Tabl31 = new System.Windows.Forms.Label();
            this.Tabl32 = new System.Windows.Forms.Label();
            this.Tabl33 = new System.Windows.Forms.Label();
            this.Tabl11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.TableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(4, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 39);
            this.label1.TabIndex = 14;
            this.label1.Text = "Пределы предупреждения";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.RBig);
            this.panelControl1.Controls.Add(this.rSmall);
            this.panelControl1.Controls.Add(this.label11);
            this.panelControl1.Controls.Add(this.label13);
            this.panelControl1.Controls.Add(this.label14);
            this.panelControl1.Controls.Add(this.label16);
            this.panelControl1.Controls.Add(this.A2l);
            this.panelControl1.Controls.Add(this.A1);
            this.panelControl1.Controls.Add(this.O3Rl);
            this.panelControl1.Controls.Add(this.O2R);
            this.panelControl1.Controls.Add(this.O1r);
            this.panelControl1.Controls.Add(this.RKBig);
            this.panelControl1.Controls.Add(this.rKSmall);
            this.panelControl1.Controls.Add(this.X);
            this.panelControl1.Controls.Add(this.label40);
            this.panelControl1.Controls.Add(this.label41);
            this.panelControl1.Controls.Add(this.label42);
            this.panelControl1.Controls.Add(this.label43);
            this.panelControl1.Controls.Add(this.label45);
            this.panelControl1.Controls.Add(this.label27);
            this.panelControl1.Controls.Add(this.label28);
            this.panelControl1.Controls.Add(this.label29);
            this.panelControl1.Controls.Add(this.label30);
            this.panelControl1.Controls.Add(this.label31);
            this.panelControl1.Controls.Add(this.label32);
            this.panelControl1.Controls.Add(this.label33);
            this.panelControl1.Controls.Add(this.label34);
            this.panelControl1.Controls.Add(this.label35);
            this.panelControl1.Controls.Add(this.label25);
            this.panelControl1.Controls.Add(this.label22);
            this.panelControl1.Controls.Add(this.label23);
            this.panelControl1.Controls.Add(this.label24);
            this.panelControl1.Controls.Add(this.label19);
            this.panelControl1.Controls.Add(this.label20);
            this.panelControl1.Controls.Add(this.label21);
            this.panelControl1.Controls.Add(this.label18);
            this.panelControl1.Controls.Add(this.label17);
            this.panelControl1.Controls.Add(this.label6);
            this.panelControl1.Controls.Add(this.TableLayoutPanel);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(569, 316);
            this.panelControl1.TabIndex = 0;
            // 
            // RBig
            // 
            this.RBig.AutoSize = true;
            this.RBig.BackColor = System.Drawing.Color.Transparent;
            this.RBig.Location = new System.Drawing.Point(392, 227);
            this.RBig.Name = "RBig";
            this.RBig.Size = new System.Drawing.Size(0, 13);
            this.RBig.TabIndex = 149;
            // 
            // rSmall
            // 
            this.rSmall.AutoSize = true;
            this.rSmall.BackColor = System.Drawing.Color.Transparent;
            this.rSmall.Location = new System.Drawing.Point(223, 227);
            this.rSmall.Name = "rSmall";
            this.rSmall.Size = new System.Drawing.Size(0, 13);
            this.rSmall.TabIndex = 148;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(371, 227);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(15, 13);
            this.label11.TabIndex = 147;
            this.label11.Text = "=";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(346, 227);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 13);
            this.label13.TabIndex = 145;
            this.label13.Text = "R";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(202, 227);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 13);
            this.label14.TabIndex = 144;
            this.label14.Text = "=";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(184, 227);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(11, 13);
            this.label16.TabIndex = 142;
            this.label16.Text = "r";
            // 
            // A2l
            // 
            this.A2l.AutoSize = true;
            this.A2l.BackColor = System.Drawing.Color.Transparent;
            this.A2l.Location = new System.Drawing.Point(223, 281);
            this.A2l.Name = "A2l";
            this.A2l.Size = new System.Drawing.Size(0, 13);
            this.A2l.TabIndex = 141;
            // 
            // A1
            // 
            this.A1.AutoSize = true;
            this.A1.BackColor = System.Drawing.Color.Transparent;
            this.A1.Location = new System.Drawing.Point(56, 281);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(0, 13);
            this.A1.TabIndex = 140;
            // 
            // O3Rl
            // 
            this.O3Rl.AutoSize = true;
            this.O3Rl.BackColor = System.Drawing.Color.Transparent;
            this.O3Rl.Location = new System.Drawing.Point(392, 254);
            this.O3Rl.Name = "O3Rl";
            this.O3Rl.Size = new System.Drawing.Size(0, 13);
            this.O3Rl.TabIndex = 139;
            // 
            // O2R
            // 
            this.O2R.AutoSize = true;
            this.O2R.BackColor = System.Drawing.Color.Transparent;
            this.O2R.Location = new System.Drawing.Point(223, 254);
            this.O2R.Name = "O2R";
            this.O2R.Size = new System.Drawing.Size(0, 13);
            this.O2R.TabIndex = 138;
            // 
            // O1r
            // 
            this.O1r.AutoSize = true;
            this.O1r.BackColor = System.Drawing.Color.Transparent;
            this.O1r.Location = new System.Drawing.Point(56, 254);
            this.O1r.Name = "O1r";
            this.O1r.Size = new System.Drawing.Size(0, 13);
            this.O1r.TabIndex = 137;
            // 
            // RKBig
            // 
            this.RKBig.AutoSize = true;
            this.RKBig.BackColor = System.Drawing.Color.Transparent;
            this.RKBig.Location = new System.Drawing.Point(392, 200);
            this.RKBig.Name = "RKBig";
            this.RKBig.Size = new System.Drawing.Size(0, 13);
            this.RKBig.TabIndex = 136;
            // 
            // rKSmall
            // 
            this.rKSmall.AutoSize = true;
            this.rKSmall.BackColor = System.Drawing.Color.Transparent;
            this.rKSmall.Location = new System.Drawing.Point(223, 200);
            this.rKSmall.Name = "rKSmall";
            this.rKSmall.Size = new System.Drawing.Size(0, 13);
            this.rKSmall.TabIndex = 135;
            // 
            // X
            // 
            this.X.AutoSize = true;
            this.X.BackColor = System.Drawing.Color.Transparent;
            this.X.Location = new System.Drawing.Point(56, 200);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(0, 13);
            this.X.TabIndex = 134;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Location = new System.Drawing.Point(202, 281);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(15, 13);
            this.label40.TabIndex = 133;
            this.label40.Text = "=";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label41.Location = new System.Drawing.Point(194, 287);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(10, 11);
            this.label41.TabIndex = 132;
            this.label41.Text = "л";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Location = new System.Drawing.Point(184, 281);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(14, 13);
            this.label42.TabIndex = 131;
            this.label42.Text = "∆";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Location = new System.Drawing.Point(35, 281);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(15, 13);
            this.label43.TabIndex = 130;
            this.label43.Text = "=";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Location = new System.Drawing.Point(16, 281);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(14, 13);
            this.label45.TabIndex = 128;
            this.label45.Text = "∆";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Location = new System.Drawing.Point(371, 200);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(15, 13);
            this.label27.TabIndex = 126;
            this.label27.Text = "=";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(356, 206);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(11, 11);
            this.label28.TabIndex = 125;
            this.label28.Text = "К";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Location = new System.Drawing.Point(346, 200);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(14, 13);
            this.label29.TabIndex = 124;
            this.label29.Text = "R";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Location = new System.Drawing.Point(202, 200);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(15, 13);
            this.label30.TabIndex = 123;
            this.label30.Text = "=";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(191, 206);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(11, 11);
            this.label31.TabIndex = 122;
            this.label31.Text = "К";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Location = new System.Drawing.Point(184, 200);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(11, 13);
            this.label32.TabIndex = 121;
            this.label32.Text = "r";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Location = new System.Drawing.Point(35, 200);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(15, 13);
            this.label33.TabIndex = 120;
            this.label33.Text = "=";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label34.Location = new System.Drawing.Point(24, 205);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(14, 11);
            this.label34.TabIndex = 119;
            this.label34.Text = "ср";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Location = new System.Drawing.Point(16, 200);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(13, 13);
            this.label35.TabIndex = 118;
            this.label35.Text = "X";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(363, 262);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(10, 11);
            this.label25.TabIndex = 117;
            this.label25.Text = "л";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(371, 254);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(15, 13);
            this.label22.TabIndex = 116;
            this.label22.Text = "=";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(355, 260);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 11);
            this.label23.TabIndex = 115;
            this.label23.Text = "R";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(345, 254);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(15, 13);
            this.label24.TabIndex = 114;
            this.label24.Text = "Ϭ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(202, 254);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(15, 13);
            this.label19.TabIndex = 113;
            this.label19.Text = "=";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(193, 260);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 11);
            this.label20.TabIndex = 112;
            this.label20.Text = "R";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(183, 254);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(15, 13);
            this.label21.TabIndex = 111;
            this.label21.Text = "Ϭ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(35, 254);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(15, 13);
            this.label18.TabIndex = 110;
            this.label18.Text = "=";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(26, 259);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(8, 11);
            this.label17.TabIndex = 109;
            this.label17.Text = "r";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(16, 254);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 13);
            this.label6.TabIndex = 108;
            this.label6.Text = "Ϭ";
            // 
            // TableLayoutPanel
            // 
            this.TableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.TableLayoutPanel.ColumnCount = 4;
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31F));
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.TableLayoutPanel.Controls.Add(this.label5, 0, 0);
            this.TableLayoutPanel.Controls.Add(this.Tabl12, 2, 1);
            this.TableLayoutPanel.Controls.Add(this.Tabl13, 3, 1);
            this.TableLayoutPanel.Controls.Add(this.Tabl21, 1, 2);
            this.TableLayoutPanel.Controls.Add(this.Tabl22, 2, 2);
            this.TableLayoutPanel.Controls.Add(this.Tabl23, 3, 2);
            this.TableLayoutPanel.Controls.Add(this.Tabl31, 1, 3);
            this.TableLayoutPanel.Controls.Add(this.Tabl32, 2, 3);
            this.TableLayoutPanel.Controls.Add(this.Tabl33, 3, 3);
            this.TableLayoutPanel.Controls.Add(this.Tabl11, 1, 1);
            this.TableLayoutPanel.Controls.Add(this.label4, 3, 0);
            this.TableLayoutPanel.Controls.Add(this.label3, 2, 0);
            this.TableLayoutPanel.Controls.Add(this.label2, 1, 0);
            this.TableLayoutPanel.Controls.Add(this.label1, 0, 1);
            this.TableLayoutPanel.Controls.Add(this.label10, 0, 3);
            this.TableLayoutPanel.Controls.Add(this.label7, 0, 2);
            this.TableLayoutPanel.Location = new System.Drawing.Point(12, 12);
            this.TableLayoutPanel.Name = "TableLayoutPanel";
            this.TableLayoutPanel.RowCount = 4;
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TableLayoutPanel.Size = new System.Drawing.Size(545, 175);
            this.TableLayoutPanel.TabIndex = 107;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(4, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(161, 52);
            this.label5.TabIndex = 142;
            this.label5.Text = "Контрольная карта";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Tabl12
            // 
            this.Tabl12.AutoSize = true;
            this.Tabl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabl12.Location = new System.Drawing.Point(297, 54);
            this.Tabl12.Name = "Tabl12";
            this.Tabl12.Size = new System.Drawing.Size(118, 39);
            this.Tabl12.TabIndex = 112;
            this.Tabl12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tabl13
            // 
            this.Tabl13.AutoSize = true;
            this.Tabl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabl13.Location = new System.Drawing.Point(422, 54);
            this.Tabl13.Name = "Tabl13";
            this.Tabl13.Size = new System.Drawing.Size(119, 39);
            this.Tabl13.TabIndex = 113;
            this.Tabl13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tabl21
            // 
            this.Tabl21.AutoSize = true;
            this.Tabl21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabl21.Location = new System.Drawing.Point(172, 94);
            this.Tabl21.Name = "Tabl21";
            this.Tabl21.Size = new System.Drawing.Size(118, 39);
            this.Tabl21.TabIndex = 114;
            this.Tabl21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tabl22
            // 
            this.Tabl22.AutoSize = true;
            this.Tabl22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabl22.Location = new System.Drawing.Point(297, 94);
            this.Tabl22.Name = "Tabl22";
            this.Tabl22.Size = new System.Drawing.Size(118, 39);
            this.Tabl22.TabIndex = 115;
            this.Tabl22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tabl23
            // 
            this.Tabl23.AutoSize = true;
            this.Tabl23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabl23.Location = new System.Drawing.Point(422, 94);
            this.Tabl23.Name = "Tabl23";
            this.Tabl23.Size = new System.Drawing.Size(119, 39);
            this.Tabl23.TabIndex = 116;
            this.Tabl23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tabl31
            // 
            this.Tabl31.AutoSize = true;
            this.Tabl31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabl31.Location = new System.Drawing.Point(172, 134);
            this.Tabl31.Name = "Tabl31";
            this.Tabl31.Size = new System.Drawing.Size(118, 40);
            this.Tabl31.TabIndex = 117;
            this.Tabl31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tabl32
            // 
            this.Tabl32.AutoSize = true;
            this.Tabl32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabl32.Location = new System.Drawing.Point(297, 134);
            this.Tabl32.Name = "Tabl32";
            this.Tabl32.Size = new System.Drawing.Size(118, 40);
            this.Tabl32.TabIndex = 118;
            this.Tabl32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tabl33
            // 
            this.Tabl33.AutoSize = true;
            this.Tabl33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabl33.Location = new System.Drawing.Point(422, 134);
            this.Tabl33.Name = "Tabl33";
            this.Tabl33.Size = new System.Drawing.Size(119, 40);
            this.Tabl33.TabIndex = 119;
            this.Tabl33.Text = "0";
            this.Tabl33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tabl11
            // 
            this.Tabl11.AutoSize = true;
            this.Tabl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabl11.Location = new System.Drawing.Point(172, 54);
            this.Tabl11.Name = "Tabl11";
            this.Tabl11.Size = new System.Drawing.Size(118, 39);
            this.Tabl11.TabIndex = 110;
            this.Tabl11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(422, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 52);
            this.label4.TabIndex = 109;
            this.label4.Text = "Для контроля погрешности";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(297, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 52);
            this.label3.TabIndex = 108;
            this.label3.Text = "Для контроля внутрилабораторной прецизионности";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(172, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 52);
            this.label2.TabIndex = 107;
            this.label2.Text = "Для контроля повторяемости";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(4, 134);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(161, 40);
            this.label10.TabIndex = 106;
            this.label10.Text = "Средняя линия";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(4, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(161, 39);
            this.label7.TabIndex = 103;
            this.label7.Text = "Пределы действия";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PredeliForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 316);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "PredeliForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Пределы";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PredeliForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.TableLayoutPanel.ResumeLayout(false);
            this.TableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Tabl12;
        private System.Windows.Forms.Label Tabl13;
        private System.Windows.Forms.Label Tabl21;
        private System.Windows.Forms.Label Tabl22;
        private System.Windows.Forms.Label Tabl23;
        private System.Windows.Forms.Label Tabl31;
        private System.Windows.Forms.Label Tabl32;
        private System.Windows.Forms.Label Tabl33;
        private System.Windows.Forms.Label Tabl11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label A2l;
        private System.Windows.Forms.Label A1;
        private System.Windows.Forms.Label O3Rl;
        private System.Windows.Forms.Label O2R;
        private System.Windows.Forms.Label O1r;
        private System.Windows.Forms.Label RKBig;
        private System.Windows.Forms.Label rKSmall;
        private System.Windows.Forms.Label X;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label RBig;
        private System.Windows.Forms.Label rSmall;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
    }
}