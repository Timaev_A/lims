﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using static LIMS.NumberToString.Число;

namespace LIMS
{
    public class SITab
    {
        bool NewSI = false;
        bool CopySI = false;
        List<int> orderList = new List<int>();
        bool ChangeWidth = true;
        WordDocument WordDoc1;
        WordDocument WordDoc2;
        WordDocument WordDoc3;
        WordDocument WordDoc4;
        WordDocument WordDoc5;
        WordDocument WordDoc6;
        MainForm MF;
        bool arhiv = false;
        bool real = true;
        internal bool HistoryOfChange = false;
        string SelectedRowId = "";
        DevExpress.Data.ColumnSortOrder SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
        internal void GetSI(MainForm Main)
        {
            try
            {
                new Thread(GetSIThread).Start(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void GetSIThread(object MainF)
        {
            try
            {
                var Main = MainF as MainForm;
                MF = Main;
                using (DbDataContext db = new DbDataContext())
                {
                    List<SI> ВсеЗначения = new List<SI>();
                    try
                    {
                        if (!UserSettings.Developer)
                        {
                            if (!arhiv && real) ВсеЗначения = db.SI.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null).OrderBy(c => c.Nint).ToList();
                            else if (arhiv && !real) ВсеЗначения = db.SI.Where(c => c.VisibleStatus == 1).OrderBy(c => c.Nint).ToList();
                            else if (arhiv && real) ВсеЗначения = db.SI.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null || c.VisibleStatus == 1).OrderBy(c => c.Nint).ToList();
                            else ВсеЗначения = null;
                        }
                        else ВсеЗначения = db.SI.OrderBy(c => c.Nint).ToList();
                    }
                    catch
                    {
                        LogErrors Log = new LogErrors("Ошибка соединения с сервером - СИ");
                        Main.Invoke(new System.Action(() => MessageBox.Show("Сервер не отвечает!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    }

                    if (HistoryOfChange) ВсеЗначения = AddSIHistory(ВсеЗначения);
                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.SIGridView.FocusedRowChanged -= Main.SIGridView_FocusedRowChanged;
                        Main.SIGrid.DataSource = ВсеЗначения;
                        if (!UserSettings.Developer)
                        {
                            Main.SIGridView.Columns[0].Visible = false;
                            Main.SIGridView.Columns[21].Visible = false;
                            Main.SIGridView.Columns[22].Visible = false;
                            Main.SIGridView.Columns[23].Visible = false;
                            Main.SIGridView.Columns[24].Visible = false;
                            Main.SIGridView.Columns[25].Visible = false;
                            Main.SIGridView.Columns[26].Visible = false;
                        }
                    }));
                    if (orderList.Count < 1)
                    {
                        Main.Invoke(new System.Action(() =>
                        {
                            for (int i = 0; i < Main.SIGridView.Columns.Count; i++)
                                orderList.Add(Main.SIGridView.Columns[i].VisibleIndex);
                        }));
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        for (int i = 0; i < Main.SIGridView.Columns.Count; i++)
                            Main.SIGridView.Columns[i].VisibleIndex = orderList[i];
                    }));
                    if (UserSettings.SIColumns != null)
                    {
                        var Visibiliti = UserSettings.SIColumns.Split(',');
                        for (int i = 0; i < Visibiliti.Length; i++)
                        {
                            Main.Invoke(new System.Action(() => Main.SIGridView.Columns[i + 1].Visible = (Visibiliti[i] == "1")));
                        }
                    }

                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.SIGridView.FocusedRowChanged += Main.SIGridView_FocusedRowChanged;
                        ColumnView View = Main.SIGridView;
                        GridColumn column = View.Columns["Id"];
                        if (column != null)
                        {
                            int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                            if (rhFound != GridControl.InvalidRowHandle)
                            {
                                View.FocusedRowHandle = rhFound;
                                View.FocusedColumn = column;
                            }
                        }
                    }));

                    Main.Invoke(new System.Action(() =>
                    {
                        Main.SIGridView.Columns[1].Caption = "№ п/п";
                        Main.SIGridView.Columns[2].Caption = "Наименование определяемых характеристик продукции";
                        Main.SIGridView.Columns[3].Caption = "Наименование СИ";
                        Main.SIGridView.Columns[4].Caption = "Тип";
                        Main.SIGridView.Columns[5].Caption = "Заводской номер";
                        Main.SIGridView.Columns[6].Caption = "Год выпуска";
                        Main.SIGridView.Columns[7].Caption = "Изготовитель (предприятие)";
                        Main.SIGridView.Columns[8].Caption = "Год ввода в эксплуатацию";
                        Main.SIGridView.Columns[9].Caption = "Инвентарный номер";
                        Main.SIGridView.Columns[10].Caption = "Диапазон измерений";
                        Main.SIGridView.Columns[11].Caption = "Класс точности, погрешность";
                        Main.SIGridView.Columns[12].Caption = "Свидетельство о поверке СИ, номер";
                        Main.SIGridView.Columns[13].Caption = "Дата поверки";
                        Main.SIGridView.Columns[14].Caption = "Mежповерочный интервал";
                        Main.SIGridView.Columns[15].Caption = "Дата следующей поверки";
                        Main.SIGridView.Columns[16].Caption = "Право собственности";
                        Main.SIGridView.Columns[17].Caption = "Место установки или хранения";
                        Main.SIGridView.Columns[18].Caption = "Дата последнего ТО";
                        Main.SIGridView.Columns[19].Caption = "Дата следующего ТО";
                        Main.SIGridView.Columns[20].Caption = "Примечание";
                        Main.SIGridView.Columns[13].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.SIGridView.Columns[13].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.SIGridView.Columns[15].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.SIGridView.Columns[15].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.SIGridView.Columns[18].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.SIGridView.Columns[18].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.SIGridView.Columns[19].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.SIGridView.Columns[19].DisplayFormat.FormatString = "dd/MM/yyyy";
                    }));
                    if (UserSettings.SIColumnWidth != "" && UserSettings.SIColumnWidth != null)
                    {
                        ChangeWidth = false;
                        var ColumnWidth = UserSettings.SIColumnWidth.Split(',');
                        for (int i = 1; i < 21; i++)
                        {
                            Main.Invoke(new System.Action(() => Main.SIGridView.Columns[i].Width = Convert.ToInt32(ColumnWidth[i - 1])));
                        }
                        ChangeWidth = true;
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        Main.SIGridView.OptionsView.RowAutoHeight = true;
                        Main.SIGridView.OptionsView.ColumnAutoWidth = UserSettings.SIColumnAutoWidth;
                        //Wrap Text
                        DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
                        MemoEdit.WordWrap = true;
                        Main.SIGridView.OptionsView.RowAutoHeight = true;
                        Main.SIGridView.OptionsView.AllowCellMerge = UserSettings.SICellMerge;
                        foreach (GridColumn My_GridColumn in Main.SIGridView.Columns)
                        {
                            My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                            My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            My_GridColumn.ColumnEdit = MemoEdit;
                        }
                        NewSI = false;
                        Main.SISaveButton.Enabled = false;
                        Main.SIEditCheckBox.Checked = false;
                        Main.SIDateSledPoverkiDate.Checked = false;
                    }));
                    db.Dispose();
                }

                Main.Invoke(new System.Action(() =>
                {
                    ColumnView View = Main.SIGridView;
                    GridColumn column = View.Columns["Id"];
                    if (column != null)
                    {
                        int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                        if (rhFound != GridControl.InvalidRowHandle)
                        {
                            View.FocusedRowHandle = rhFound;
                            View.FocusedColumn = column;
                        }
                    }
                }));
            }
            catch (Exception ex)
            {
                ChangeWidth = true;
                LogErrors Log = new LogErrors(ex.ToString());
                MF.Invoke(new System.Action(() => MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
            }
        }

        private List<SI> AddSIHistory(List<SI> всеЗначения)
        {
            try
            {
                List<SI> NewList = new List<SI>();
                using (DbDataContext db = new DbDataContext())
                {
                    foreach (SI StrokeSI in всеЗначения)
                    {
                        var SIHistory = db.SI.Where(c => c.Owner == StrokeSI.Id && (c.VisibleStatus == 100 || c.VisibleStatus == 200)).OrderBy(c => c.ДатаСозданияЗаписи).ToList();
                        if (SIHistory != null && SIHistory.Count > 0)
                        {
                            var i = 0;
                            foreach (SI OtherSI in SIHistory)
                            {
                                i++;
                                if (i > 1)
                                {
                                    OtherSI.N = "";
                                    OtherSI.НаименованиеСИ = "";
                                    OtherSI.НаименованиеОпределяемыхХарактеристикПродукции = "";                                    
                                }
                                NewList.Add(OtherSI);
                            }
                        }
                        else NewList.Add(StrokeSI);
                    }
                    db.Dispose();
                }
                return NewList;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return всеЗначения;
            }
        }

        internal void SICopyButton_Click(MainForm Main)
        {
            try
            {
                if (Main.SIID.Text != "")
                {
                    Main.SIEditCheckBox.Enabled = false;
                    NewSI = false;
                    Main.SINewButton.Text = "Добавить";
                    Main.SINewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    ActivateEditSI(true, Main);
                    NewSI = false;
                    CopySI = true;
                    Main.SISaveButton.Enabled = true;
                    Main.SIEditCheckBox.Checked = false;
                }
                else
                {
                    MessageBox.Show("Не выбран элемент!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void SIGrid_Click(MainForm Main)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gv = Main.SIGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                var Stroke = (SI)gv.GetRow(gv.FocusedRowHandle);
                if (Stroke == null) return;
                SelectedRowId = Stroke.Id.ToString();
                Main.SIID.Text = Stroke.Id.ToString();
                Main.SIN.Text = Stroke.N;
                Main.SINameProd.Text = Stroke.НаименованиеОпределяемыхХарактеристикПродукции;
                Main.SINameSI.Text = Stroke.НаименованиеСИ;// ТипЗаводскойНомерГодВыпуска;
                Main.SITip.Text = Stroke.Тип;
                Main.SIZavodNomer.Text = Stroke.ЗаводскойНомер;
                Main.SIGod.Text = Stroke.ГодВыпуска;
                Main.SIIzgotovitel.Text = Stroke.ИзготовительПредриятие;
                Main.SIVvodVEkspluatachiu.Text = Stroke.ГодВводаВЭксплуатацию;
                Main.SIInventarNomer.Text = Stroke.ИнвентарныйНомер;
                Main.SIDiapozonIzm.Text = Stroke.ДиапозонИзмерений;
                Main.SIClassTochnosti.Text = Stroke.КлассТочностиПогрешность;
                Main.SISvidetelstvoOPoverke.Text = Stroke.СвидетельствоОПоверкеСИНомер;
                Main.SIDataPoverki.Text = Stroke.ДатаПоверки == null ? "" : Stroke.ДатаПоверки;
                Main.SISrokDeystv.Text = Stroke.СрокДействия;
                Main.SIDateSledPoverkiDate.Text = Stroke.ДатаСледПоверки.ToString();
                Main.SIMestoUstanovki.Text = Stroke.МестоУстановкиИлиХранения;
                Main.SIPravoSobstvennosti.Text = Stroke.ПравоСобственности;
                Main.SIDataPosledTO.Text = Stroke.ПровДатаПослТО.ToString();
                Main.SIDataSledTODate.Text = Stroke.ДатаСледПроведенияТО.ToString();
                Main.SIPrimechanie.Text = Stroke.Примечание;
                Main.SIDateSledPoverkiDate.Checked = false;
                Main.SIEditCheckBox.Checked = false;
                ActivateEditSI(false, Main);
                if (Stroke.ДатаСледПоверки.HasValue)
                    Main.SIDateSledPoverkiTip.Text = "дата";
                else Main.SIDateSledPoverkiTip.Text = "нет";
                if (Stroke.ДатаСледПроведенияТО.HasValue)
                    Main.SIDataSledTOTip.Text = "дата";
                else Main.SIDataSledTOTip.Text = "нет";
                if (Stroke.ДатаПоверки != "" && Stroke.ДатаПоверки != null)
                    Main.SIDataPoverkiTip.Text = "дата";
                else Main.SIDataPoverkiTip.Text = "нет";
                if (Stroke.ПровДатаПослТО.HasValue)
                    Main.SIDataPosledTOTip.Text = "дата";
                else Main.SIDataPosledTOTip.Text = "нет";
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void SIEditCheckBox_CheckedChanged(MainForm Main)
        {
            if (!NewSI && Main.SIID.Text != "")
            {
                ActivateEditSI(Main.SIEditCheckBox.Checked, Main);
            }
            else
            {
                Main.SIEditCheckBox.Checked = false;
            }
        }

        internal void SIDeleteButton_Click(MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    if (Main.SIID.Text != "")
                    {
                        DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить \"" + Main.SINameSI.Text + "\"?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            var Строка3 = db.SI.Where(c => c.Id == Convert.ToInt32(Main.SIID.Text)).FirstOrDefault();
                            if (Строка3 != null)
                            {
                                //Перевод в историю
                                Строка3.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                Строка3.ДатаСозданияЗаписи = DateTime.Now;
                                Строка3.UserName = UserSettings.User;
                                //db.SI.DeleteOnSubmit(Строка3);
                                db.SubmitChanges();
                            }
                            DevExpress.XtraGrid.Views.Grid.GridView gv = Main.SIGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                            if (gv.FocusedRowHandle > 0)
                            {
                                var Stroke = (SI)gv.GetRow(gv.FocusedRowHandle - 1);
                                if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                            }
                            ClearControlsSI(Main);
                            GetSI(Main);
                            ActivateEditSI(false, Main);
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не выбрана строка для удаления!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        protected void ClearControlsSI(MainForm Main)
        {
            foreach (Control c in Main.SIPanel.Controls)
            {
                if (c is TextBox)
                {
                    var TB = c as TextBox;
                    TB.Text = "";
                }
                if (c is NumericUpDown)
                {
                    var NUB = c as NumericUpDown;
                    NUB.Text = "";
                }
                if (c is ComboBox)
                {
                    var CB = c as ComboBox;
                    CB.Text = "";
                }
                if (c is DateTimePicker)
                {
                    var DTP = c as DateTimePicker;
                    DTP.Text = "";
                    DTP.Checked = false;
                }
                Main.SIID.Text = "";
            }
        }

        internal void NewSIButton(MainForm Main)
        {
            try
            {
                if (NewSI)
                {
                    ClearControlsSI(Main);
                    NewSI = false;
                    Main.SINewButton.Text = "Добавить";
                    Main.SINewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.SISaveButton.Enabled = false;
                }
                else
                {
                    NewSI = true;
                    ClearControlsSI(Main);
                    Main.SINewButton.Text = "Отменить";
                    Main.SINewButton.Image = global::LIMS.Properties.Resources.cancel_16x16;
                    Main.SISaveButton.Enabled = true;
                }
                ActivateEditSI(Main.SINewButton.Text == "Отменить", Main);
                Main.SIEditCheckBox.Enabled = Main.SINewButton.Text != "Отменить";
                Main.SIEditCheckBox.Checked = false;
                Main.SIDataSledTOTip.Text = "дата";
                Main.SIDateSledPoverkiTip.Text = "дата";
                Main.SIDataPosledTOTip.Text = "дата";
                Main.SIDataPoverkiTip.Text = "дата";
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void ActivateEditSI(bool EditableFields, MainForm Main)
        {
            try
            {
                if (EditableFields)
                {
                    foreach (Control c in Main.SIPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = false;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.ReadOnly = false;
                            NUD.Enabled = false;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = true;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = true;
                            DTP.Checked = false;
                        }
                    }
                    Main.SISaveButton.Enabled = true;
                }
                else
                {
                    foreach (Control c in Main.SIPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = true;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.ReadOnly = true;
                            NUD.Enabled = true;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = false;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = false;
                            DTP.Checked = false;
                        }
                    }
                    NewSI = false;
                    Main.SINewButton.Text = "Добавить";
                    Main.SINewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.SISaveButton.Enabled = false;
                    Main.SIEditCheckBox.Checked = false;
                    Main.SIEditCheckBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }


        internal void SISaveButton_Click(MainForm Main)
        {
            try
            {
                try
                {
                    var t = Convert.ToInt32(Main.SIN.Text);
                }
                catch
                {
                    MessageBox.Show("Не корректное значения в поле \"№ п/п\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                using (DbDataContext db = new DbDataContext())
                {
                    //Редактирование
                    if (!NewSI && !CopySI)
                    {
                        if (Main.SIID.Text != "")
                        {
                            var EditID = Convert.ToInt32(Main.SIID.Text);
                            var Stroka = db.SI.Where(c => c.Id == EditID).FirstOrDefault();
                            Stroka.Id = EditID;
                            try
                            {
                                var IntN = Main.SIN.Text.Split('.');
                                Stroka.Nint = Convert.ToInt32(IntN[0]);
                            }
                            catch { }
                            Stroka.N = Main.SIN.Text;
                            Stroka.НаименованиеОпределяемыхХарактеристикПродукции = Main.SINameProd.Text;
                            Stroka.НаименованиеСИ = Main.SINameSI.Text;
                            Stroka.Тип = Main.SITip.Text;
                            Stroka.ЗаводскойНомер = Main.SIZavodNomer.Text;
                            Stroka.ГодВыпуска = Main.SIGod.Text;
                            Stroka.ИзготовительПредриятие = Main.SIIzgotovitel.Text;
                            Stroka.ГодВводаВЭксплуатацию = Main.SIVvodVEkspluatachiu.Text;
                            Stroka.ИнвентарныйНомер = Main.SIInventarNomer.Text;
                            Stroka.ДиапозонИзмерений = Main.SIDiapozonIzm.Text;
                            Stroka.КлассТочностиПогрешность = Main.SIClassTochnosti.Text;
                            Stroka.СвидетельствоОПоверкеСИНомер = Main.SISvidetelstvoOPoverke.Text;
                            if (Main.SIDataPoverkiTip.Text != "нет")
                                Stroka.ДатаПоверки = Main.SIDataPoverki.Text;
                            else
                                Stroka.ДатаПоверки = null;
                            Stroka.СрокДействия = Main.SISrokDeystv.Text;
                            if (Main.SIDateSledPoverkiTip.Text != "нет")
                            {
                                if (Main.SIDateSledPoverkiTip.Text == "дата")
                                    Stroka.ДатаСледПоверки = DateTime.Parse(Main.SIDateSledPoverkiDate.Text).Date;
                                else
                                {
                                    try
                                    {
                                        Stroka.ДатаСледПоверки = (Stroka.ДатаПоверки != "" && Stroka.ДатаПоверки != null) ? DateTime.Parse(Stroka.ДатаПоверки).AddMonths(Convert.ToInt32(Main.SIDateSledPoverkiKol.Text)).Date : Stroka.ДатаСледПоверки;
                                    }
                                    catch { }
                                }
                            }
                            else
                                Stroka.ДатаСледПоверки = null;
                            if (Main.SIDataPosledTOTip.Text != "нет")
                                Stroka.ПровДатаПослТО = DateTime.Parse(Main.SIDataPosledTO.Text).Date;
                            else
                                Stroka.ПровДатаПослТО = null;
                            if (Main.SIDataSledTOTip.Text != "нет")
                            {
                                if (Main.SIDataSledTOTip.Text == "дата")
                                    Stroka.ДатаСледПроведенияТО = DateTime.Parse(Main.SIDataSledTODate.Text).Date;
                                else Stroka.ДатаСледПроведенияТО = Stroka.ПровДатаПослТО.HasValue ? Main.SIDataPosledTO.Value.AddMonths(Convert.ToInt32(Main.SIDataSledTOKol.Text)).Date : Stroka.ДатаСледПроведенияТО;
                            }
                            else
                                Stroka.ДатаСледПроведенияТО = null;
                            //Stroka.ДатаСледПоверки = Main.SIDateSledPoverkiTip.Text != "нет" ? Main.SIDateSledPoverkiTip.Text == "дата" ? Main.SIDateSledPoverkiDate.Checked ? DateTime.Parse(Main.SIDateSledPoverkiDate.Text).Date : Stroka.ДатаСледПоверки : Stroka.ДатаПоверки.HasValue ? Stroka.ДатаПоверки.Value.AddMonths(Convert.ToInt32(Main.SIDateSledPoverkiKol.Text)).Date : Stroka.ДатаСледПоверки : null;
                            Stroka.МестоУстановкиИлиХранения = Main.SIMestoUstanovki.Text;
                            Stroka.ПравоСобственности = Main.SIPravoSobstvennosti.Text;
                            //Stroka.ПровДатаПослТО = Main.SIDataPosledTOTip.Text != "нет" ? Main.SIDataPosledTO.Checked ? DateTime.Parse(Main.SIDataPosledTO.Text).Date : Stroka.ПровДатаПослТО : null;
                            //Stroka.ДатаСледПроведенияТО = Main.SIDataSledTOTip.Text != "нет" ? Main.SIDataSledTOTip.Text == "дата" ? Main.SIDataSledTODate.Checked ? DateTime.Parse(Main.SIDataSledTODate.Text).Date : Stroka.ДатаСледПроведенияТО : Stroka.ДатаСледПроведенияТО.HasValue ? Main.SIDataPosledTO.Value.AddMonths(Convert.ToInt32(Main.SIDataSledTOKol.Text)).Date : Stroka.ДатаСледПроведенияТО : null;                             
                            Stroka.Примечание = Main.SIPrimechanie.Text;
                            Stroka.UserName = UserSettings.User;
                            //Stroka.VisibleStatus = 0;
                            Stroka.ДатаСозданияЗаписи = DateTime.Now;
                            Stroka.Owner = null;
                            //История
                            HistorySI(Stroka, 200);
                            db.SubmitChanges();
                            GetSI(Main);
                            //Выбор стороки таблицы
                            int rowHandle = Main.SIGridView.LocateByValue("Id", int.Parse(Main.SIID.Text));
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                Main.SIGridView.FocusedRowHandle = rowHandle;
                        }
                    }
                    else
                    {
                        //Добавление                        
                        var NextID = db.SI != null && db.SI.Count() > 0 ? db.SI.Max(c => c.Id) + 1 : 1;
                        string ДатаПоверки = null;
                        if (Main.SIDataPoverkiTip.Text != "нет")
                        {
                            /*if (Main.SIDataPoverki.Checked)*/
                            ДатаПоверки = Main.SIDataPoverki.Text;
                        }
                        DateTime? ДатаПоверкиДата = null;
                        try
                        {
                            ДатаПоверкиДата = Convert.ToDateTime(Main.SIDataPoverki.Text);
                        }
                        catch { }

                        DateTime? ДатаСледПоверки = null;
                        if (Main.SIDateSledPoverkiTip.Text == "дата")
                        {
                            /*if (Main.SIDateSledPoverkiDate.Checked)*/
                            ДатаСледПоверки = Main.SIDateSledPoverkiDate.Value.Date;
                        }
                        else if (Main.SIDateSledPoverkiTip.Text != "нет")
                        {
                            try
                            {
                                if (ДатаПоверки != null && ДатаПоверки != "") ДатаСледПоверки = DateTime.Parse(Main.SIDataPoverki.Text).AddMonths(Convert.ToInt32(Main.SIDateSledPoverkiKol.Text)).Date;
                            }
                            catch { }
                        }
                        DateTime? ПровДатаПослТО = null;
                        if (Main.SIDataPosledTOTip.Text != "нет")
                        {
                            /*if (Main.SIDataPosledTO.Checked)*/
                            ПровДатаПослТО = DateTime.Parse(Main.SIDataPosledTO.Text).Date;
                        }
                        DateTime? ДатаСледПроведенияТО = null;
                        if (Main.SIDataSledTOTip.Text == "дата")
                        {
                            /*if (Main.SIDataSledTODate.Checked)*/
                            ДатаСледПроведенияТО = DateTime.Parse(Main.SIDataSledTODate.Text).Date;
                        }
                        else if (Main.SIDataSledTOTip.Text != "нет")
                        {
                            if (ПровДатаПослТО != null) ДатаСледПроведенияТО = Main.SIDataPosledTO.Value.AddMonths(Convert.ToInt32(Main.SIDataSledTOKol.Text)).Date;
                        }
                        int NINT = 0;
                        try
                        {
                            var IntN = Main.SIN.Text.Split('.');
                            NINT = Convert.ToInt32(IntN[0]);
                        }
                        catch { }
                        db.SI.InsertOnSubmit(new SI
                        {
                            Id = NextID,
                            N = Main.SIN.Text,
                            Nint = NINT,
                            НаименованиеОпределяемыхХарактеристикПродукции = Main.SINameProd.Text,
                            НаименованиеСИ = Main.SINameSI.Text,
                            Тип = Main.SITip.Text,
                            ЗаводскойНомер = Main.SIZavodNomer.Text,
                            ГодВыпуска = Main.SIGod.Text,
                            ИзготовительПредриятие = Main.SIIzgotovitel.Text,
                            ГодВводаВЭксплуатацию = Main.SIVvodVEkspluatachiu.Text,
                            ИнвентарныйНомер = Main.SIInventarNomer.Text,
                            ДиапозонИзмерений = Main.SIDiapozonIzm.Text,
                            КлассТочностиПогрешность = Main.SIClassTochnosti.Text,
                            СвидетельствоОПоверкеСИНомер = Main.SISvidetelstvoOPoverke.Text,
                            ДатаПоверки = ДатаПоверки,
                            СрокДействия = Main.SISrokDeystv.Text,
                            ДатаСледПоверки = ДатаСледПоверки,
                            МестоУстановкиИлиХранения = Main.SIMestoUstanovki.Text,
                            ПравоСобственности = Main.SIPravoSobstvennosti.Text,
                            ДатаПоверкиDateTime = ДатаПоверкиДата,
                            ПровДатаПослТО = ПровДатаПослТО,
                            ДатаСледПроведенияТО = ДатаСледПроведенияТО,
                            Примечание = Main.SIPrimechanie.Text,
                            VisibleStatus = 0,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = null
                        });
                        //История изменений
                        db.SI.InsertOnSubmit(new SI
                        {
                            Id = NextID + 1,
                            N = Main.SIN.Text,
                            Nint = NINT,
                            НаименованиеОпределяемыхХарактеристикПродукции = Main.SINameProd.Text,
                            НаименованиеСИ = Main.SINameSI.Text,
                            Тип = Main.SITip.Text,
                            ЗаводскойНомер = Main.SIZavodNomer.Text,
                            ГодВыпуска = Main.SIGod.Text,
                            ИзготовительПредриятие = Main.SIIzgotovitel.Text,
                            ГодВводаВЭксплуатацию = Main.SIVvodVEkspluatachiu.Text,
                            ИнвентарныйНомер = Main.SIInventarNomer.Text,
                            ДиапозонИзмерений = Main.SIDiapozonIzm.Text,
                            КлассТочностиПогрешность = Main.SIClassTochnosti.Text,
                            СвидетельствоОПоверкеСИНомер = Main.SISvidetelstvoOPoverke.Text,
                            ДатаПоверки = ДатаПоверки,
                            СрокДействия = Main.SISrokDeystv.Text,
                            ДатаСледПоверки = ДатаСледПоверки,
                            МестоУстановкиИлиХранения = Main.SIMestoUstanovki.Text,
                            ПравоСобственности = Main.SIPravoSobstvennosti.Text,
                            ДатаПоверкиDateTime = ДатаПоверкиДата,
                            ПровДатаПослТО = ПровДатаПослТО,
                            ДатаСледПроведенияТО = ДатаСледПроведенияТО,
                            Примечание = Main.SIPrimechanie.Text,
                            VisibleStatus = 100,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = NextID
                        });
                        db.SubmitChanges();
                        NewSIButton(Main);
                        GetSI(Main);
                        ActivateEditSI(false, Main);
                        Main.SISaveButton.Enabled = false;
                    }
                    Main.SIDateSledPoverkiDate.Checked = false;
                    NewSI = false;
                    CopySI = false;
                    db.Dispose();
                    Main.SIEditCheckBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void HistorySI(SI stroka, int Kod)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.SI.InsertOnSubmit(new SI
                    {
                        Id = db.SI != null && db.SI.Count() > 0 ? db.SI.Max(c => c.Id) + 1 : 1,
                        N = stroka.N,
                        Nint = stroka.Nint,
                        НаименованиеОпределяемыхХарактеристикПродукции = stroka.НаименованиеОпределяемыхХарактеристикПродукции,
                        НаименованиеСИ = stroka.НаименованиеСИ,
                        Тип = stroka.Тип,
                        ЗаводскойНомер = stroka.ЗаводскойНомер,
                        ГодВыпуска = stroka.ГодВыпуска,
                        ИзготовительПредриятие = stroka.ИзготовительПредриятие,
                        ГодВводаВЭксплуатацию = stroka.ГодВводаВЭксплуатацию,
                        ИнвентарныйНомер = stroka.ИнвентарныйНомер,
                        ДиапозонИзмерений = stroka.ДиапозонИзмерений,
                        КлассТочностиПогрешность = stroka.КлассТочностиПогрешность,
                        СвидетельствоОПоверкеСИНомер = stroka.СвидетельствоОПоверкеСИНомер,
                        ДатаПоверки = stroka.ДатаПоверки,
                        СрокДействия = stroka.СрокДействия,
                        ДатаСледПоверки = stroka.ДатаСледПоверки,
                        МестоУстановкиИлиХранения = stroka.МестоУстановкиИлиХранения,
                        ПровДатаПослТО = stroka.ПровДатаПослТО,
                        ДатаСледПроведенияТО = stroka.ДатаСледПроведенияТО,
                        Примечание = stroka.Примечание,
                        VisibleStatus = Kod,
                        ДатаСозданияЗаписи = DateTime.Now,
                        UserName = UserSettings.User,
                        Owner = stroka.Id
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void SIPrint(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc1 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Средства измерения.dotx");
                    var RowCount = MF.SIGridView.RowCount;
                    if (RowCount > 1) WordDoc1.AddTableCell(2, RowCount - 1);
                    int Cell = 1;
                    for (int rowHandle = 0; rowHandle < RowCount; rowHandle++)
                    {
                        Cell++;
                        if (MF.SIGridView.IsDataRow(rowHandle) || Cell >= 39)
                        {
                            try
                            {
                                var SelectedRow = (SI)MF.SIGridView.GetRow(rowHandle);
                                var DatePoverki = (SelectedRow.ДатаПоверки != null || SelectedRow.ДатаПоверки != "") ? " " + SelectedRow.ДатаПоверки + "г." : "";
                                WordDoc1.AddTableValue(2, Cell, 1, SelectedRow.N.ToString());
                                WordDoc1.AddTableValue(2, Cell, 2, SelectedRow.НаименованиеОпределяемыхХарактеристикПродукции);
                                WordDoc1.AddTableValue(2, Cell, 3, SelectedRow.НаименованиеСИ);
                                WordDoc1.AddTableValue(2, Cell, 4, SelectedRow.Тип);
                                WordDoc1.AddTableValue(2, Cell, 5, SelectedRow.ЗаводскойНомер == "" ? "" : SelectedRow.ЗаводскойНомер.Contains('№') ? SelectedRow.ЗаводскойНомер : "№ " + SelectedRow.ЗаводскойНомер);
                                WordDoc1.AddTableValue(2, Cell, 6, SelectedRow.ГодВыпуска == "" ? "" : SelectedRow.ГодВыпуска.Contains('г') ? SelectedRow.ГодВыпуска : SelectedRow.ГодВыпуска + "г.");
                                WordDoc1.AddTableValue(2, Cell, 7, SelectedRow.ИзготовительПредриятие);
                                WordDoc1.AddTableValue(2, Cell, 8, SelectedRow.ГодВводаВЭксплуатацию == "" ? "" : SelectedRow.ГодВводаВЭксплуатацию.Contains('г') ? SelectedRow.ГодВводаВЭксплуатацию : SelectedRow.ГодВводаВЭксплуатацию + "г.");
                                WordDoc1.AddTableValue(2, Cell, 9, SelectedRow.ИнвентарныйНомер == "" ? "" : SelectedRow.ИнвентарныйНомер.Contains('№') ? SelectedRow.ИнвентарныйНомер : "№ " + SelectedRow.ИнвентарныйНомер);
                                WordDoc1.AddTableValue(2, Cell, 10, SelectedRow.ДиапозонИзмерений);
                                WordDoc1.AddTableValue(2, Cell, 11, SelectedRow.КлассТочностиПогрешность);
                                WordDoc1.AddTableValue(2, Cell, 12, SelectedRow.СвидетельствоОПоверкеСИНомер + (DatePoverki.Length < 4 ? "" : " от " + DatePoverki));
                                //WordDoc.AddTableValue(2, Cell, 13, SelectedRow.ДатаПоверки.HasValue ? SelectedRow.ДатаПоверки.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 13, SelectedRow.СрокДействия);
                                WordDoc1.AddTableValue(2, Cell, 14, SelectedRow.ДатаСледПоверки.HasValue ? SelectedRow.ДатаСледПоверки.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 15, SelectedRow.ПравоСобственности);
                                WordDoc1.AddTableValue(2, Cell, 16, SelectedRow.МестоУстановкиИлиХранения);
                                WordDoc1.AddTableValue(2, Cell, 17, SelectedRow.ПровДатаПослТО.HasValue ? SelectedRow.ПровДатаПослТО.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 18, SelectedRow.ДатаСледПроведенияТО.HasValue ? SelectedRow.ДатаСледПроведенияТО.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 19, SelectedRow.Примечание);
                            }
                            catch { }
                        }
                    }
                    if (UserSettings.SIColumns != null)
                    {
                        var Visibiliti = UserSettings.SIColumns.Split(',').ToList<string>();
                        Visibiliti.RemoveAt(13);
                        for (int i = Visibiliti.Count - 1; i > 0; i--)
                        {
                            if (Visibiliti[i] != "1")
                                WordDoc1.DeleteTableColumn(2, i + 1);
                        }
                    }
                    WordDoc1.TableResize(2);
                    StringBuilder rezult = new StringBuilder();
                    var result = Пропись((uint)(Cell - 1), new КлассБезТысяч(), rezult);
                    WordDoc1.ReplaceString("{КолвоДокументов}", (Cell - 1).ToString() + "(" + rezult + ")");
                    WordDoc1.ReplaceString("{Дата}", DateTime.Now.ToShortDateString() + "г.");
                    WordDoc1.Visible = true;
                    WordDoc1.wordApplication.Activate();
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc1.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void SISrokDeystv_TextChanged(MainForm mainForm)
        {
            try
            {
                var Srok = mainForm.SISrokDeystv.Text.Split(' ');
                mainForm.SIDateSledPoverkiKol.Value = Convert.ToInt32(Srok[0]);
            }
            catch { }
        }

        internal void CheckColumnButton_Click(MainForm Main)
        {
            try
            {
                CheckSIColumnForm f = new CheckSIColumnForm(Main, this);
                f.Owner = Main;
                f.ShowDialog();
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void SIReturnFromArhivButton_Click(int id, MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.SI.Where(c => c.Id == id).FirstOrDefault();
                    Stroka.VisibleStatus = 0;
                    Stroka.ДатаСозданияЗаписи = DateTime.Now;
                    Stroka.UserName = UserSettings.User;
                    //История 
                    HistorySI(Stroka, 400);
                    db.SubmitChanges();
                    db.Dispose();
                    if (!real)
                    {
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.SIGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        if (gv.FocusedRowHandle > 0)
                        {
                            var Stroke = (SI)gv.GetRow(gv.FocusedRowHandle - 1);
                            if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                        }
                    }
                    GetSI(Main);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void SIArhivButton_Click(int id, MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.SI.Where(c => c.Id == id).FirstOrDefault();
                    Stroka.VisibleStatus = 1;
                    Stroka.ДатаСозданияЗаписи = DateTime.Now;
                    Stroka.UserName = UserSettings.User;
                    //История                     
                    HistorySI(Stroka, 401);
                    db.SubmitChanges();
                    db.Dispose();
                    if (!arhiv)
                    {
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.SIGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        if (gv.FocusedRowHandle > 0)
                        {
                            var Stroke = (SI)gv.GetRow(gv.FocusedRowHandle - 1);
                            if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                        }
                    }
                    GetSI(Main);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void SIGridView_ColumnWidthChanged(MainForm MF)
        {
            try
            {
                if (ChangeWidth)
                {
                    UserSettings.SIColumnWidth = "";
                    for (int i = 1; i < 21; i++)
                    {
                        if (UserSettings.SIColumnWidth != "")
                            UserSettings.SIColumnWidth = UserSettings.SIColumnWidth + ",";
                        UserSettings.SIColumnWidth = UserSettings.SIColumnWidth + MF.SIGridView.Columns[i].Width.ToString();
                    }
                    UserSettings.SaveSettings();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void SIDateSledPoverkiTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.SIDateSledPoverkiTip.Text == "дата")
            {
                Main.SIDateSledPoverkiDate.Visible = true;
                Main.SIDateSledPoverkiKol.Visible = false;
            }
            else if (Main.SIDateSledPoverkiTip.Text == "нет")
            {
                Main.SIDateSledPoverkiDate.Visible = false;
                Main.SIDateSledPoverkiKol.Visible = false;
            }
            else
            {
                Main.SIDateSledPoverkiDate.Visible = false;
                Main.SIDateSledPoverkiKol.Visible = true;
            }
        }

        internal void SIDataSledTOTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.SIDataSledTOTip.Text == "дата")
            {
                Main.SIDataSledTODate.Visible = true;
                Main.SIDataSledTOKol.Visible = false;
            }
            else if (Main.SIDataSledTOTip.Text == "нет")
            {
                Main.SIDataSledTODate.Visible = false;
                Main.SIDataSledTOKol.Visible = false;
            }
            else
            {
                Main.SIDataSledTODate.Visible = false;
                Main.SIDataSledTOKol.Visible = true;
                Main.SIDataSledTOKol.Value = 12;
            }
        }

        internal void AutoWidthColumn(MainForm MF)
        {
            if (UserSettings.SIColumnAutoWidth)
            {
                UserSettings.SIColumnAutoWidth = false;
            }
            else UserSettings.SIColumnAutoWidth = true;
            GetSI(MF);
            UserSettings.SaveSettings();
        }

        internal void SIArhivButton_Click_1(MainForm Main)
        {
            if (Main.SIArhivButton.ContextMenuStrip != null)
            {
                Main.SIArhivButton.ContextMenuStrip.Close();
                Main.SIArhivButton.ContextMenuStrip = null;
                return;
            }
            MF = Main;
            ContextMenuStrip ContextMS = new ContextMenuStrip();
            ContextMS.Items.Add("&Показать рабочие средства измерения", null, SIRealVisible);
            if (real)
            {
                ((ToolStripMenuItem)ContextMS.Items[0]).Checked = true;
            }
            else
            {
                ((ToolStripMenuItem)ContextMS.Items[0]).Checked = false;
            }
            ContextMS.Items.Add("&Архив", null, SIArhivVisible);
            if (arhiv)
            {
                ((ToolStripMenuItem)ContextMS.Items[1]).Checked = true;
            }
            else
            {
                ((ToolStripMenuItem)ContextMS.Items[1]).Checked = false;
            }
            ContextMS.Items.Add("&Показать изменения", null, SIHistoryVisible);
            if (HistoryOfChange)
            {
                ((ToolStripMenuItem)ContextMS.Items[2]).Checked = true;
            }
            else
            {
                ((ToolStripMenuItem)ContextMS.Items[2]).Checked = false;
            }
            ContextMS.Items.Add("&Закрыть", global::LIMS.Properties.Resources.delete_16x16, ContextMenuStripClose_Click);
            ContextMS.AutoClose = false;
            Main.SIArhivButton.ContextMenuStrip = ContextMS;
            ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);
        }

        private void SIHistoryVisible(object sender, EventArgs e)
        {
            HistoryOfChange = HistoryOfChange ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = HistoryOfChange;
            GetSI(MF);
        }

        private void ContextMenuStripClose_Click(object sender, EventArgs e)
        {
            var TSMI = sender as ToolStripMenuItem;
            var CMS = TSMI.Owner as ContextMenuStrip;
            CMS.AutoClose = true;
            MF.SIArhivButton.ContextMenuStrip = null;
            CMS.Close();
        }

        private void SIRealVisible(object sender, EventArgs e)
        {
            real = real ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = real;
            GetSI(MF);
        }

        private void SIArhivVisible(object sender, EventArgs e)
        {
            arhiv = arhiv ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = arhiv;
            GetSI(MF);
        }

        internal void SIDataPoverkiTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.SIDataPoverkiTip.Text == "дата")
            {
                Main.SIDataPoverki.Visible = true;
            }
            else
            {
                Main.SIDataPoverki.Visible = false;
            }
        }

        internal void SIDataPosledTOTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.SIDataPosledTOTip.Text == "дата")
            {
                Main.SIDataPosledTO.Visible = true;
            }
            else
            {
                Main.SIDataPosledTO.Visible = false;
            }
        }

        internal void SICellMergeButton_Click(MainForm Main)
        {
            UserSettings.SICellMerge = !UserSettings.SICellMerge;
            Main.SIGridView.OptionsView.AllowCellMerge = UserSettings.SICellMerge;
            if (UserSettings.SICellMerge)
                Main.SICellMergeButton.Image = global::LIMS.Properties.Resources.freezetoprow_32x32;
            else Main.SICellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            UserSettings.SaveSettings();
        }

        internal void splitContainerControl4_Panel1_SizeChanged(object sender)
        {
            try
            {
                var SplitCon = sender as DevExpress.XtraEditors.SplitGroupPanel;
                UserSettings.SISplitWidth = SplitCon.Width;
                UserSettings.SaveSettings();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void SIPrintGrafickAttestachii(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc2 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\График поверки и проверки СИ.dotx");
                    List<SI> ВсеЗначения = new List<SI>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.SI.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null) && c.ДатаСледПоверки.HasValue && c.ДатаСледПоверки.Value.Year == GlobalStatic.BuferInt).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - SI");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc2.AddTableCell(2, RowCount - 1);
                    int Cell = 1;
                    foreach (var SI in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                WordDoc2.AddTableValue(2, Cell, 1, (Cell - 1).ToString());
                                WordDoc2.AddTableValue(2, Cell, 2, SI.НаименованиеСИ);
                                WordDoc2.AddTableValue(2, Cell, 3, SI.Тип);
                                WordDoc2.AddTableValue(2, Cell, 4, SI.ЗаводскойНомер == "" ? "" : SI.ЗаводскойНомер.Contains('№') ? SI.ЗаводскойНомер : "№ " + SI.ЗаводскойНомер);
                                WordDoc2.AddTableValue(2, Cell, 5, SI.ДатаПоверки);
                                WordDoc2.AddTableValue(2, Cell, 6, SI.МестоУстановкиИлиХранения);
                                WordDoc2.AddTableValue(2, Cell, 7, "В работе");
                            }
                            catch { }
                        }
                    }
                    WordDoc2.TableResize(2);
                    WordDoc2.ReplaceString("{ГОД}", GlobalStatic.BuferInt.ToString());
                    WordDoc2.Visible = true;
                    WordDoc2.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc2.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void SIPrintGrafikTO(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc6 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\График ТО СИ.dotx");
                    List<SI> ВсеЗначения = new List<SI>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.SI.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null) && c.ДатаСледПроведенияТО.HasValue && c.ДатаСледПроведенияТО.Value.Year == GlobalStatic.BuferInt).OrderBy(c => c.НаименованиеСИ).ThenBy(c => c.Тип).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - SI");
                        }
                        db.Dispose();
                    }

                    int Cell = 1;
                    string Name = "";
                    int StartName = 2;
                    string Data = "";
                    int StartData = Cell + 1;
                    string Perechen = "";
                    int StartPerechen = Cell + 1;
                    string Dolgnost = "";
                    foreach (var SI in ВсеЗначения)
                    {
                        try
                        {
                            if (Name != (SI.НаименованиеСИ + " " + SI.Тип) || Data != (SI.ДатаСледПроведенияТО.HasValue ? SI.ДатаСледПроведенияТО.Value.ToShortDateString() : ""))
                            {
                                Cell++;
                                if (Cell > 2) WordDoc6.AddTableCell(1, 1);
                                WordDoc6.AddTableValue(1, Cell, 2, SI.НаименованиеСИ + " " + SI.Тип);
                                Name = SI.НаименованиеСИ + " " + SI.Тип;

                                WordDoc6.AddTableValue(1, Cell, 3, (SI.ЗаводскойНомер));
                                WordDoc6.AddTableValue(1, Cell, 4, SI.ДатаСледПроведенияТО.HasValue ? SI.ДатаСледПроведенияТО.Value.ToShortDateString() : "");
                                WordDoc6.AddTableValue(1, Cell, 5, GetPerechenRabot(SI.НаименованиеСИ + " " + SI.Тип));
                                WordDoc6.AddTableValue(1, Cell, 6, GetDolgnost(SI.НаименованиеСИ + " " + SI.Тип));
                            }
                            else
                            {
                                if (!WordDoc6.TableValue(1, Cell, 3).Contains(SI.ЗаводскойНомер))
                                    WordDoc6.AddTableValue(1, Cell, 3, (WordDoc6.TableValue(1, Cell, 3) + SI.ЗаводскойНомер));
                                if (!WordDoc6.TableValue(1, Cell, 4).Contains(SI.ДатаСледПроведенияТО.HasValue ? SI.ДатаСледПроведенияТО.Value.ToShortDateString() : ""))
                                    WordDoc6.AddTableValue(1, Cell, 4, (WordDoc6.TableValue(1, Cell, 4) + (SI.ДатаСледПроведенияТО.HasValue ? SI.ДатаСледПроведенияТО.Value.ToShortDateString() : "")));
                                WordDoc6.AddTableValue(1, Cell, 5, GetPerechenRabot(SI.НаименованиеСИ + " " + SI.Тип));
                                WordDoc6.AddTableValue(1, Cell, 6, GetDolgnost(SI.НаименованиеСИ + " " + SI.Тип));
                            }
                            Data = SI.ДатаСледПроведенияТО.HasValue ? SI.ДатаСледПроведенияТО.Value.ToShortDateString() : "";
                        }
                        catch { }
                    }

                    Name = "";
                    StartData = 2;
                    Perechen = "";
                    for (int i = 2; i < Cell + 1; i++)
                    {
                        if (Name != WordDoc6.TableValue(1, i, 2))
                        {
                            if (StartName < i - 1)
                            {
                                WordDoc6.MergeCells(1, StartName, i - 1, 2);
                            }
                            StartName = i;
                            Name = WordDoc6.TableValue(1, i, 2);
                        }
                        else
                        {
                            WordDoc6.AddTableValue(1, i, 2, "");
                        }

                        if (Perechen != WordDoc6.TableValue(1, i, 5))
                        {
                            if (StartPerechen < i - 1)
                            {
                                WordDoc6.MergeCells(1, StartPerechen, i - 1, 5);
                                WordDoc6.MergeCells(1, StartPerechen, i - 1, 6);
                            }
                            if (StartData < i - 1)
                                WordDoc6.MergeCells(1, StartData, i - 1, 4);

                            Data = WordDoc6.TableValue(1, i, 4);
                            Perechen = WordDoc6.TableValue(1, i, 5);
                            Dolgnost = WordDoc6.TableValue(1, i, 6);
                            StartData = i;
                            StartPerechen = i;
                        }
                        else
                        {
                            if (Data != WordDoc6.TableValue(1, i, 4))
                            {
                                if (StartData < i - 1)
                                    WordDoc6.MergeCells(1, StartData, i - 1, 4);
                                Data = WordDoc6.TableValue(1, i, 4);
                                StartData = i;
                            }
                            else
                            {
                                WordDoc6.AddTableValue(1, i, 4, "");
                            }

                            WordDoc6.AddTableValue(1, i, 5, "");
                            WordDoc6.AddTableValue(1, i, 6, "");
                        }
                    }

                    if (StartName < Cell - 1)
                        WordDoc6.MergeCells(1, StartName, Cell - 1, 2);
                    if (StartData < Cell - 1)
                        WordDoc6.MergeCells(1, StartData, Cell - 1, 4);
                    if (StartPerechen < Cell - 1)
                    {
                        WordDoc6.MergeCells(1, StartPerechen, Cell - 1, 5);
                        WordDoc6.MergeCells(1, StartPerechen, Cell - 1, 6);
                    }
                    WordDoc6.ReplaceString("{Год}", GlobalStatic.BuferInt.ToString());
                    WordDoc6.TableResize(1);
                    WordDoc6.Visible = true;
                    WordDoc6.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc6.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void SIPrintPerechenDocumentov(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc5 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\ПЕРЕЧЕНЬ документации на СИ.dotx");
                    List<SI> ВсеЗначения = new List<SI>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.SI.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null)).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - SI");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc5.AddTableCell(1, RowCount - 1);
                    int Cell = 2;
                    foreach (var SI in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                //WordDoc4.AddTableValue(1, Cell, 1, (Cell - 2).ToString() + ".");
                                WordDoc5.AddTableValue(1, Cell, 2, SI.НаименованиеСИ + " " + SI.Тип);
                                WordDoc5.AddTableValue(1, Cell, 3, "№ " + SI.ЗаводскойНомер);
                                WordDoc5.AddTableValue(1, Cell, 4, SI.ДиапозонИзмерений);
                                WordDoc5.AddTableValue(1, Cell, 5, SI.КлассТочностиПогрешность);
                            }
                            catch { }
                        }
                    }
                    StringBuilder rezult = new StringBuilder();
                    var result = Пропись((uint)(RowCount), new КлассБезТысяч(), rezult);
                    WordDoc5.ReplaceString("{колво}", RowCount.ToString() + " (" + rezult + ")");
                    WordDoc5.ReplaceString("{Дата}", DateTime.Now.ToShortDateString());
                    WordDoc5.TableResize(1);
                    WordDoc5.Visible = true;
                    WordDoc5.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc5.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void SIPrintPerechen(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc4 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\ПЕРЕЧЕНЬ СИ.dotx");
                    List<SI> ВсеЗначения = new List<SI>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.SI.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null)).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - SI");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc4.AddTableCell(1, RowCount - 1);
                    int Cell = 2;
                    foreach (var SI in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                //WordDoc4.AddTableValue(1, Cell, 1, (Cell - 2).ToString() + ".");
                                WordDoc4.AddTableValue(1, Cell, 2, SI.НаименованиеСИ + " " + SI.Тип);
                                WordDoc4.AddTableValue(1, Cell, 3, "№ " + SI.ЗаводскойНомер);
                                WordDoc4.AddTableValue(1, Cell, 4, SI.ДиапозонИзмерений);
                                WordDoc4.AddTableValue(1, Cell, 5, SI.КлассТочностиПогрешность);
                            }
                            catch { }
                        }
                    }
                    StringBuilder rezult = new StringBuilder();
                    var result = Пропись((uint)(RowCount), new КлассБезТысяч(), rezult);
                    WordDoc4.ReplaceString("{колво}", RowCount.ToString() + " (" + rezult + ")");
                    WordDoc4.ReplaceString("{Дата}", DateTime.Now.ToShortDateString());
                    WordDoc4.TableResize(1);
                    WordDoc4.Visible = true;
                    WordDoc4.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc4.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void SIPrintOsnachinnost(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc3 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Оснащенность СИ.dotx");
                    List<SI> ВсеЗначения = new List<SI>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.SI.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null)).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - SI");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc3.AddTableCell(1, RowCount - 1);
                    int Cell = 3;
                    int StartNaim = 4;
                    int StartPrimechanie = 4;
                    string TextNaim = "";
                    string TextPrimechanie = "";
                    foreach (var SI in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                WordDoc3.AddTableValue(1, Cell, 1, (Cell - 2).ToString());
                                WordDoc3.AddTableValue(1, Cell, 2, SI.НаименованиеОпределяемыхХарактеристикПродукции);
                                WordDoc3.AddTableValue(1, Cell, 3, SI.НаименованиеСИ + " " + SI.Тип + "\r\n№ " + SI.ЗаводскойНомер + "\r\n" + SI.ГодВыпуска + "г.");
                                WordDoc3.AddTableValue(1, Cell, 4, SI.ИзготовительПредриятие);
                                WordDoc3.AddTableValue(1, Cell, 5, SI.ГодВводаВЭксплуатацию + "г. Инв. № " + SI.ИнвентарныйНомер);
                                WordDoc3.AddTableValue(1, Cell, 6, SI.ДиапозонИзмерений);
                                WordDoc3.AddTableValue(1, Cell, 7, SI.КлассТочностиПогрешность);
                                WordDoc3.AddTableValue(1, Cell, 8, "Свид-во " + (SI.СвидетельствоОПоверкеСИНомер.Contains("№") ? SI.СвидетельствоОПоверкеСИНомер + " от " + SI.ДатаПоверки + "г." : SI.СвидетельствоОПоверкеСИНомер.Contains("от") ? SI.СвидетельствоОПоверкеСИНомер : "от " + SI.СвидетельствоОПоверкеСИНомер) + ", " + SI.СрокДействия);
                                WordDoc3.AddTableValue(1, Cell, 9, SI.ПравоСобственности);
                                WordDoc3.AddTableValue(1, Cell, 10, SI.МестоУстановкиИлиХранения);
                                WordDoc3.AddTableValue(1, Cell, 11, SI.Примечание);
                                WordDoc3.BoltText(SI.НаименованиеСИ + " " + SI.Тип);

                                if (TextNaim != SI.НаименованиеОпределяемыхХарактеристикПродукции || SI.НаименованиеОпределяемыхХарактеристикПродукции == "")
                                {
                                    if (Cell - StartNaim > 1)
                                    {
                                        WordDoc3.MergeCells(1, StartNaim, Cell - 1, 6);
                                    }
                                    StartNaim = Cell;
                                    TextNaim = SI.НаименованиеОпределяемыхХарактеристикПродукции;
                                }
                                else WordDoc3.AddTableValue(1, Cell, 6, "");
                                if (TextPrimechanie != SI.Примечание || SI.Примечание == "")
                                {
                                    if (Cell - StartPrimechanie > 1)
                                    {
                                        WordDoc3.MergeCells(1, StartPrimechanie, Cell - 1, 11);
                                    }
                                    StartPrimechanie = Cell;
                                    TextPrimechanie = SI.Примечание;
                                }
                                else WordDoc3.AddTableValue(1, Cell, 11, "");
                            }
                            catch { }
                        }
                    }
                    WordDoc3.ReplaceString("{ГОД}", DateTime.Now.Year.ToString());
                    WordDoc3.TableResize(1);
                    WordDoc3.Visible = true;
                    WordDoc3.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc3.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void SIGridView_StartSorting(MainForm Main)
        {
            try
            {
                var GV = Main.SIGridView;
                if (GV.SortInfo.Count > 0)
                {
                    for (int i = GV.SortInfo.Count - 1; i >= 0; i--)
                    {
                        if (GV.SortInfo[i].Column.FieldName != "Nint")
                        {
                            if (GV.SortInfo[i].Column.FieldName == "N")
                            {
                                GV.ClearSorting();
                                GV.Columns["Nint"].SortOrder = SortOrder;
                                if (SortOrder == DevExpress.Data.ColumnSortOrder.Ascending)
                                    SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
                                else
                                    SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                                //GV.SortInfo[i].Column = SIGridView.Columns[24];
                                //GV.SortInfo[i].Column.SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
                            }
                            else
                                SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private string GetPerechenRabot(string Name)
        {
            if (Name.ToUpper().Contains("ХРОМАТОГРАФ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Проверка уровня шума.\r\n4.Замена лайнера, мембраны, сорбента.\r\n5.Замена ионитового фильтра(при необходимости).\r\n6.Калибровка прибора.\r\n7.Проверка исправности работы СИ в соответствии технической документацией.";
            }
            if (Name.ToUpper().Contains("ТЕРМОМЕТР") && Name.ToUpper().Contains("ЛТ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса прибора и датчика температуры;\r\n-целостности кабеля - удлинителя;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка:\r\n-поверхности корпуса и датчика температуры.\r\n4.Замена элементов питания(при необходимости)\r\n5.Проверка исправности работы СИ в соответствии технической документацией.";
            }
            if (Name.ToUpper().Contains("ТЕРМОМЕТР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса, отсутствия разрывов ртутной шкалы термометра;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка поверхности корпуса термометра и футляра";
            }
            if (Name.ToUpper().Contains("МАНОМЕТР МТИ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка поверхности корпуса\r\n4.Установка нуля(при необходимости).\r\n";
            }
            if (Name.ToUpper().Contains("СЕКУНДОМЕР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса прибора;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка поверхности корпуса от пыли и загрязнений.\r\n4.Проверка исправности работы СИ в соответствии технической документацией.";
            }
            if (Name.ToUpper().Contains("ВИСКОЗИМЕТР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка стеклянной поверхности корпуса.";
            }
            if (Name.ToUpper().Contains("ВЛАЖН") && Name.ToUpper().Contains("ТЕМПЕРАТУР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка:\r\n-поверхности корпуса и преобразователя.\r\n4 Проверка исправности работы СИ в соответствии технической документацией.\r\n5.Проверка заряда элементов питания.Замена элементов питания, при необходимости.";
            }
            if (Name.ToUpper().Contains("ГИГРОМЕТР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса, отсутствия разрывов шкалы термометров;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Замена фитиля, заполнение питателя дистиллированной водой.";
            }
            if (Name.ToUpper().Contains("КОНДУКТОМЕТР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса прибора и датчика электропроводности;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка поверхности корпуса и датчика.\r\n4.Замена элементов питания(при необходимости).\r\n5.Проверка исправности работы СИ в соответствии технической документацией.";
            }
            if (Name.ToUpper().Contains("СПЕКТРОСКАН"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Градуировка(при необходимости).\r\n4.Проверка исправности работы СИ в соответствии технической документацией.";
            }
            if (Name.ToUpper().Contains("ДАТЧИК") && Name.ToUpper().Contains("ГАЗОАНАЛИЗАТОР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса прибора;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка газоанализатора от пыли.";
            }
            if (Name.ToUpper().Contains("ГАЗОАНАЛИЗАТОР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса прибора;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Заряд аккумуляторной батареи(при необходимости).\r\n4.Очистка газоанализатора от пыли.\r\n5.Проверка исправности работы СИ в соответствии технической документацией.";
            }
            if (Name.ToUpper().Contains("БАРОМЕТР") && Name.ToUpper().Contains("АНЕРОИД"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса прибора;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Проверка горизонтальности установки прибора.\r\n4.Свинчивание возможного разъединения крышки и корпуса в байонетном соединении(при необходимости).";
            }
            if (Name.ToUpper().Contains("РН"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса прибора и электрода;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Проверка заполнения электрода раствором.\r\n4.Проверка исправности работы СИ в соответствии технической документацией";
            }
            if (Name.ToUpper().Contains("ТИГРАТОР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, даты последней поверки.\r\n3.Очистка:\r\n-поверхностей оборудования от загрязнений;\r\n-промывка цилиндра бюретки, поршня, трубки деионизированной водой.\r\n4.Проверка герметичности жидкостных линий.\r\n5.Проверка исправности работы СИ в соответствии технической документацией.";
            }
            if (Name.ToUpper().Contains("ВЕСЫ ЛАБОРАТОРН"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на весы, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка поверхности весов.\r\n4.Проверка уровня установки весов с помощью пузырькового индикатора уровня.\r\n5.Проверка исправности работы СИ в соответствии технической документацией.";
            }
            if (Name.ToUpper().Contains("АРЕОМЕТР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса, целостности грузика ареометра;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка поверхности корпуса.";
            }
            if (Name.ToUpper().Contains("МАНОМЕТР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка поверхности корпуса.";
            }
            if (Name.ToUpper().Contains("МИКРОШПРИЦ"))
            {
                return "1.Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-отсутствия механических повреждений;\r\n-на цилиндре отсутствие трещин;\r\n-на кончике иглы -отсутствие задира.\r\n3.Проверка плавности хода поршня в пределах шкалы.\r\n4.Очистка цилиндра от загрязнений, чистка иглы.";
            }
            if (Name.ToUpper().Contains("ГИРЯ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка поверхности гири.";
            }
            if (Name.ToUpper().Contains("МЕРНИК"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения:\r\n-комплектности согласно эксплуатационной документации;\r\n-целостности корпуса;\r\n-наличия бирки(наименования оборудования, заводского номера, даты выпуска, даты последней поверки).\r\n3.Очистка и промывка внутренней поверхности мерника.";
            }
            return String.Empty;
        }

        private string GetDolgnost(string Name)
        {
            if (Name.ToUpper().Contains("ХРОМАТОГРАФ"))
            {
                return "Инженер ООО «ХроматэкСервис-Казань»;\r\nИнженер хим. анализа О.";
            }
            else
            {
                return "Лаборант хим. анализа ;\r\nИнженер хим. анализа .";
            }
        }
    }
}
