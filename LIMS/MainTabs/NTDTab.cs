﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace LIMS
{
    public class NTDTab
    {
        MainForm MF;
        public bool NewNTD = false;
        public bool CopyNTD = false;
        List<int> orderList = new List<int>();
        private bool ChangeWidth = true;
        WordDocument WordDoc1 = new WordDocument();
        WordDocument WordDoc2 = new WordDocument();
        WordDocument WordDoc3 = new WordDocument();
        WordDocument WordDoc4 = new WordDocument();
        bool arhiv = false;
        bool real = true;
        string SelectedRowId = "";

        public void GetNTD(MainForm Main)
        {
            try
            {
                new Thread(GetNTDThread).Start(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void GetNTDThread(object MainF)
        {
            try
            {
                var Main = MainF as MainForm;
                MF = Main;
                using (DbDataContext db = new DbDataContext())
                {
                    List<NTD> ВсеЗначения = new List<NTD>();
                    try
                    {
                        if (!UserSettings.Developer)
                        {
                            if (!arhiv && real) ВсеЗначения = db.NTD.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null).OrderBy(c => c.Уровень).ThenBy(c => c.Группа).ThenBy(c => c.Nint).ToList();
                            else if (arhiv && !real) ВсеЗначения = db.NTD.Where(c => c.VisibleStatus == 1).OrderBy(c => c.Уровень).ThenBy(c => c.Группа).ThenBy(c => c.Nint).ToList();
                            else if (arhiv && real) ВсеЗначения = db.NTD.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null || c.VisibleStatus == 1).OrderBy(c => c.Уровень).ThenBy(c => c.Группа).ThenBy(c => c.N).ToList();
                            else ВсеЗначения = null;
                        }
                        else ВсеЗначения = db.NTD.OrderBy(c => c.Уровень).ThenBy(c => c.Группа).ThenBy(c => c.Nint).ToList();
                    }
                    catch
                    {
                        LogErrors Log = new LogErrors("Ошибка соединения с сервером - НТД");
                        Main.Invoke(new System.Action(() => MessageBox.Show("Сервер не отвечает!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.NTDGridView.FocusedRowChanged -= Main.NTDGridView_FocusedRowChanged;
                        Main.NTDGrid.DataSource = ВсеЗначения;
                        if (!UserSettings.Developer)
                        {
                            Main.NTDGridView.Columns[0].Visible = false;
                            Main.NTDGridView.Columns[15].Visible = false;
                            Main.NTDGridView.Columns[16].Visible = false;
                            Main.NTDGridView.Columns[17].Visible = false;
                            Main.NTDGridView.Columns[18].Visible = false;
                            Main.NTDGridView.Columns[19].Visible = false;
                        }
                    }));
                    if (orderList.Count < 1)
                    {
                        Main.Invoke(new System.Action(() =>
                        {
                            for (int i = 0; i < Main.NTDGridView.Columns.Count; i++)
                                orderList.Add(Main.NTDGridView.Columns[i].VisibleIndex);
                        }));
                    }
                    for (int i = 0; i < Main.NTDGridView.Columns.Count; i++)
                        Main.Invoke(new System.Action(() => Main.NTDGridView.Columns[i].VisibleIndex = orderList[i]));
                    if (UserSettings.NTDColumns != null)
                    {
                        var Visibiliti = UserSettings.NTDColumns.Split(',');
                        for (int i = 0; i < Visibiliti.Length; i++)
                        {
                            Main.Invoke(new System.Action(() => Main.NTDGridView.Columns[i + 1].Visible = (Visibiliti[i] == "1")));
                        }
                    }

                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.NTDGridView.FocusedRowChanged += Main.NTDGridView_FocusedRowChanged;
                        ColumnView View = Main.NTDGridView;
                        GridColumn column = View.Columns["Id"];
                        if (column != null)
                        {
                            int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                            if (rhFound != GridControl.InvalidRowHandle)
                            {
                                View.FocusedRowHandle = rhFound;
                                View.FocusedColumn = column;
                            }
                        }
                    }));

                    Main.Invoke(new System.Action(() =>
                    {
                        Main.NTDGridView.Columns[1].Caption = "Уровень документа";
                        Main.NTDGridView.Columns[2].Caption = "Группа";
                        Main.NTDGridView.Columns[3].Caption = "№ п/п";
                        Main.NTDGridView.Columns[4].Caption = "НД";
                        Main.NTDGridView.Columns[5].Caption = "Наименование документа (обозначение)";
                        Main.NTDGridView.Columns[6].Caption = "Регистрационный номер по журналу учёта и регистраций документов";
                        Main.NTDGridView.Columns[7].Caption = "Дата утверждения (дата ввода в действие)";
                        Main.NTDGridView.Columns[8].Caption = "Контрольный экземпляр. Кол-во";
                        Main.NTDGridView.Columns[9].Caption = "Контрольный экземпляр. Место нахождения";
                        Main.NTDGridView.Columns[10].Caption = "Рабочий экземпляр. Кол-во";
                        Main.NTDGridView.Columns[11].Caption = "Рабочий экземпляр. Место нахождения";
                        Main.NTDGridView.Columns[12].Caption = "Дата последней актуализации";
                        Main.NTDGridView.Columns[13].Caption = "Срок действия";
                        Main.NTDGridView.Columns[14].Caption = "Примечание";
                        Main.NTDGridView.Columns[7].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.NTDGridView.Columns[7].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.NTDGridView.Columns[12].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.NTDGridView.Columns[12].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.NTDGridView.Columns[13].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.NTDGridView.Columns[13].DisplayFormat.FormatString = "dd/MM/yyyy";
                    }));

                    if (UserSettings.NTDColumnWidth != "" && UserSettings.NTDColumnWidth != null)
                    {
                        ChangeWidth = false;
                        var ColumnWidth = UserSettings.NTDColumnWidth.Split(',');
                        for (int i = 1; i < 15; i++)
                        {
                            Main.Invoke(new System.Action(() => Main.NTDGridView.Columns[i].Width = Convert.ToInt32(ColumnWidth[i - 1])));
                        }
                        ChangeWidth = true;
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        Main.NTDGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
                        Main.NTDGridView.OptionsView.RowAutoHeight = true;
                        Main.NTDGridView.OptionsView.ColumnAutoWidth = UserSettings.NTDColumnAutoWidth;
                        //Wrap Text
                        DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
                        MemoEdit.WordWrap = true;
                        Main.NTDGridView.OptionsView.RowAutoHeight = true;
                        Main.NTDGridView.OptionsView.AllowCellMerge = UserSettings.NTDCellMerge;
                        foreach (GridColumn My_GridColumn in Main.NTDGridView.Columns)
                        {
                            My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                            My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            My_GridColumn.ColumnEdit = MemoEdit;
                        }
                        Main.NTDSaveButton.Enabled = false;
                        Main.NTDEditCheckBox.Checked = false;
                    }));
                    db.Dispose();
                }

                Main.Invoke(new System.Action(() =>
                {
                    //Main.NTDGridView.FocusedRowChanged -= Main.NTDGridView_FocusedRowChanged;
                    ColumnView View = Main.NTDGridView;
                    GridColumn column = View.Columns["Id"];
                    if (column != null)
                    {
                        int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                        if (rhFound != GridControl.InvalidRowHandle)
                        {
                            View.FocusedRowHandle = rhFound;
                            View.FocusedColumn = column;
                        }
                    }
                    //Main.NTDGridView.FocusedRowChanged += Main.NTDGridView_FocusedRowChanged;
                }));
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MF.Invoke(new System.Action(() => MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
            }
        }
             

        public void NTDCopyButton_Click(MainForm Main)
        {
            try
            {
                if (Main.NTDID.Text != "")
                {
                    Main.NTDEditCheckBox.Enabled = false;
                    NewNTD = false;
                    Main.NTDNewButton.Text = "Добавить";
                    Main.NTDNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    ActivateEditNTD(true, Main);
                    CopyNTD = true;
                    Main.NTDSaveButton.Enabled = true;
                    Main.NTDEditCheckBox.Checked = false;
                }
                else
                {
                    MessageBox.Show("Не выбран элемент!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void NewNTDButton(MainForm Main)
        {
            try
            {
                if (NewNTD)
                {
                    NewNTD = false;
                    Main.NTDNewButton.Text = "Добавить";
                    Main.NTDNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.NTDSaveButton.Enabled = false;
                }
                else
                {
                    NewNTD = true;
                    ClearControlsNTD(Main);
                    Main.NTDNewButton.Text = "Отменить";
                    Main.NTDNewButton.Image = global::LIMS.Properties.Resources.cancel_16x16;
                    Main.NTDSaveButton.Enabled = true;
                }
                ActivateEditNTD(Main.NTDNewButton.Text == "Отменить", Main);
                Main.NTDEditCheckBox.Enabled = Main.NTDNewButton.Text != "Отменить";
                Main.NTDEditCheckBox.Checked = false;
                Main.NTDDateAktualizTip.Text = "дата";
                Main.NTDDateUtvergdeniaTip.Text = "дата";
                Main.NTDSrokDeisrviaTip.Text = "дата";
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        protected void ClearControlsNTD(MainForm Main)
        {
            foreach (Control c in Main.NTDPanel.Controls)
            {
                if (c is TextBox)
                {
                    var TB = c as TextBox;
                    TB.Text = "";
                }
                if (c is ComboBox)
                {
                    var CB = c as ComboBox;
                    CB.Text = "";
                }
                if (c is DateTimePicker)
                {
                    var DTP = c as DateTimePicker;
                    DTP.Text = "";
                    DTP.Checked = false;
                }
                if (c is NumericUpDown)
                {
                    var DTP = c as NumericUpDown;
                    DTP.Text = "";
                }
                Main.NTDID.Text = "";
            }
        }


        private void ActivateEditNTD(bool EditableFields, MainForm Main)
        {
            try
            {
                if (EditableFields)
                {
                    foreach (Control c in Main.NTDPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = false;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = true;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.Enabled = true;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = true;
                            DTP.Checked = false;
                        }
                    }
                    Main.NTDSaveButton.Enabled = true;
                }
                else
                {
                    foreach (Control c in Main.NTDPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = true;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = false;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.Enabled = false;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = false;
                            DTP.Checked = false;
                        }
                        Main.NTDEditCheckBox.Enabled = true;
                    }
                    NewNTD = false;
                    Main.NTDNewButton.Text = "Добавить";
                    Main.NTDNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.NTDSaveButton.Enabled = false;
                    Main.NTDEditCheckBox.Checked = false;
                }                
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void NTDEditCheckBox_CheckedChanged(MainForm Main)
        {
            if (!NewNTD && Main.NTDID.Text != "")
            {
                ActivateEditNTD(Main.NTDEditCheckBox.Checked, Main);
            }
            else
            {
                Main.NTDEditCheckBox.Checked = false;
            }
        }

        public void NTDSaveButton_Click(MainForm Main)
        {
            try
            {
                try
                {
                    var newint = Main.NTDN.Text.Split('.');
                    var Nint = newint[0] == "" ? 0 : Convert.ToInt32(newint[0]);
                }
                catch
                {
                    MessageBox.Show("Не корректное значения в поле \"№ п/п\"", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                using (DbDataContext db = new DbDataContext())
                {
                    //Редактирование
                    if (!NewNTD && !CopyNTD)
                    {
                        if (Main.NTDID.Text != "")
                        {
                            var EditID = Convert.ToInt32(Main.NTDID.Text);
                            var Stroka = db.NTD.Where(c => c.Id == EditID).FirstOrDefault();
                            Stroka.Id = EditID;
                            Stroka.N = Main.NTDN.Text;
                            var newint = Main.NTDN.Text.Split('.');
                            Stroka.Nint = newint[0] == "" ? 0 : Convert.ToInt32(newint[0]);
                            Stroka.Уровень = Main.NTDUroven.Text;
                            Stroka.Группа = Main.NTDGroup.Text;
                            if (Main.NTDND1.Text == "ПРЖ/А-04-") Stroka.НД = Main.NTDND1.Text + Main.NTDND2.Text;
                            else Stroka.НД = Main.NTDND1.Text + " " + Main.NTDND2.Text;
                            Stroka.НаименованиеДокумента = Main.NTDName.Text;
                            Stroka.РегистрационныйНомер = Main.NTDRegistN.Text;
                            if (Main.NTDDateUtvergdeniaTip.Text != "нет")
                                Stroka.ДатаУтверждения = DateTime.Parse(Main.NTDDateUtvergdenia.Text).Date;
                            else Stroka.ДатаУтверждения = null;
                            Stroka.НаличиеКонтрольногоЭкземпляраКолво = Main.NTDKontrolKolvo.Value.ToString() == "0" ? "-" : Main.NTDKontrolKolvo.Value.ToString();
                            Stroka.НаличиеКонтрольногоЭкземпляраМестоНахождения = Main.NTDKontrolMesto.Text;
                            Stroka.НаличиеРабочегоЭкземпляраКолво = Main.NTDRabKolvo.Value.ToString() == "0" ? "-" : Main.NTDRabKolvo.Value.ToString();
                            Stroka.НаличиеРабочегоЭкземпляраМестоНахождения = Main.NTDRabMesto.Text.ToString();
                            if (Main.NTDDateAktualizTip.Text != "нет")
                                Stroka.ДатаПоследнейАктуализации = DateTime.Parse(Main.NTDDateAktualiz.Text).Date;
                            else Stroka.ДатаПоследнейАктуализации = null;
                            if (Main.NTDSrokDeisrviaTip.Text != "нет")
                                Stroka.СрокДействия = DateTime.Parse(Main.NTDSrokDeisrvia.Text).Date;
                            else Stroka.СрокДействия = null;
                            Stroka.Примечание = Main.NTDPrimechanie.Text;
                            Stroka.VisibleStatus = Stroka.VisibleStatus;
                            Stroka.ДатаСозданияЗаписи = DateTime.Now;
                            Stroka.UserName = UserSettings.User;
                            Stroka.Owner = null;
                            HistoryNTD(Stroka, 200);
                            db.SubmitChanges();
                            GetNTD(Main);
                            //Выбор стороки таблицы
                            int rowHandle = Main.NTDGridView.LocateByValue("Id", int.Parse(Main.NTDID.Text));
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                Main.NTDGridView.FocusedRowHandle = rowHandle;
                        }
                    }
                    else
                    {
                        var Proverka = db.NTD.Where(c => c.N == Main.NTDN.Text && c.Уровень == Main.NTDUroven.Text && c.Группа == Main.NTDGroup.Text).FirstOrDefault();
                        if (Proverka != null)
                        {
                            MessageBox.Show("НТД с уровнем " + Proverka.Уровень + ",  группой " + Proverka.Группа + " и номером " + Proverka.N + "уже существует!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        //Добавление                        
                        var NextID = db.NTD != null && db.NTD.Count() > 0 ? db.NTD.Max(c => c.Id) + 1 : 1;
                        DateTime? DA = null;
                        if (Main.NTDDateAktualizTip.Text != "нет") DA = DateTime.Parse(Main.NTDDateAktualiz.Text).Date;
                        DateTime? SD = null;
                        if (Main.NTDSrokDeisrviaTip.Text != "нет") SD = DateTime.Parse(Main.NTDSrokDeisrvia.Text).Date;
                        DateTime? DU = null;
                        if (Main.NTDDateUtvergdeniaTip.Text != "нет") DU = DateTime.Parse(Main.NTDDateUtvergdenia.Text).Date;
                        var ND = "";
                        if (Main.NTDND1.Text == "ПРЖ/А-04-") ND = Main.NTDND1.Text + Main.NTDND2.Text;
                        else ND = Main.NTDND1.Text + " " + Main.NTDND2.Text;
                        var newint = Main.NTDN.Text.Split('.');
                        db.NTD.InsertOnSubmit(new NTD
                        {
                            Id = NextID,
                            N = Main.NTDN.Text,
                            Nint = newint[0] == "" ? 0 : Convert.ToInt32(newint[0]),
                            Уровень = Main.NTDUroven.Text,
                            Группа = Main.NTDGroup.Text,
                            НД = ND,
                            НаименованиеДокумента = Main.NTDName.Text,
                            РегистрационныйНомер = Main.NTDRegistN.Text,
                            ДатаУтверждения = DU,
                            НаличиеКонтрольногоЭкземпляраКолво = Main.NTDKontrolKolvo.Value.ToString() == "0" ? "-" : Main.NTDKontrolKolvo.Value.ToString(),
                            НаличиеКонтрольногоЭкземпляраМестоНахождения = Main.NTDKontrolMesto.Text,
                            НаличиеРабочегоЭкземпляраКолво = Main.NTDRabKolvo.Value.ToString() == "0" ? "-" : Main.NTDRabKolvo.Value.ToString(),
                            НаличиеРабочегоЭкземпляраМестоНахождения = Main.NTDRabMesto.Text.ToString(),
                            ДатаПоследнейАктуализации = DA,
                            СрокДействия = SD,
                            Примечание = Main.NTDPrimechanie.Text,
                            VisibleStatus = 0,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = null
                        });
                        db.NTD.InsertOnSubmit(new NTD
                        {
                            Id = NextID + 1,
                            N = Main.NTDN.Text,
                            Nint = newint[0] == "" ? 0 : Convert.ToInt32(newint[0]),
                            Уровень = Main.NTDUroven.Text,
                            Группа = Main.NTDGroup.Text,
                            НД = ND,
                            НаименованиеДокумента = Main.NTDName.Text,
                            РегистрационныйНомер = Main.NTDRegistN.Text,
                            ДатаУтверждения = DU,
                            НаличиеКонтрольногоЭкземпляраКолво = Main.NTDKontrolKolvo.Value.ToString() == "0" ? "-" : Main.NTDKontrolKolvo.Value.ToString(),
                            НаличиеКонтрольногоЭкземпляраМестоНахождения = Main.NTDKontrolMesto.Text,
                            НаличиеРабочегоЭкземпляраКолво = Main.NTDRabKolvo.Value.ToString() == "0" ? "-" : Main.NTDRabKolvo.Value.ToString(),
                            НаличиеРабочегоЭкземпляраМестоНахождения = Main.NTDRabMesto.Text.ToString(),
                            ДатаПоследнейАктуализации = DA,
                            СрокДействия = SD,
                            Примечание = Main.NTDPrimechanie.Text,
                            VisibleStatus = 100,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = NextID,
                        });
                        db.SubmitChanges();
                        NewNTDButton(Main);
                        GetNTD(Main);
                        ActivateEditNTD(false, Main);
                        Main.NTDSaveButton.Enabled = false;
                    }
                    NewNTD = false;
                    CopyNTD = false;
                    Main.NTDDateUtvergdenia.Checked = false;
                    Main.NTDDateAktualiz.Checked = false;
                    Main.NTDSrokDeisrvia.Checked = false;
                    db.Dispose();
                    Main.NTDEditCheckBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void HistoryNTD(NTD stroka, int Kod)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.NTD.InsertOnSubmit(new NTD
                    {
                        Id = db.NTD != null && db.NTD.Count() > 0 ? db.NTD.Max(c => c.Id) + 1 : 1,
                        N = stroka.N,
                        Nint = stroka.Nint,
                        Уровень = stroka.Уровень,
                        Группа = stroka.Группа,
                        НД = stroka.НД,
                        НаименованиеДокумента = stroka.НаименованиеДокумента,
                        РегистрационныйНомер = stroka.РегистрационныйНомер,
                        ДатаУтверждения = stroka.ДатаУтверждения,
                        НаличиеКонтрольногоЭкземпляраКолво = stroka.НаличиеКонтрольногоЭкземпляраКолво,
                        НаличиеКонтрольногоЭкземпляраМестоНахождения = stroka.НаличиеКонтрольногоЭкземпляраМестоНахождения,
                        НаличиеРабочегоЭкземпляраКолво = stroka.НаличиеРабочегоЭкземпляраКолво,
                        НаличиеРабочегоЭкземпляраМестоНахождения = stroka.НаличиеРабочегоЭкземпляраМестоНахождения,
                        ДатаПоследнейАктуализации = stroka.ДатаПоследнейАктуализации,
                        СрокДействия = stroka.СрокДействия,
                        Примечание = stroka.Примечание,
                        VisibleStatus = Kod,
                        ДатаСозданияЗаписи = DateTime.Now,
                        UserName = UserSettings.User,
                        Owner = stroka.Id
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void NTDReturnFromArhivButton_Click(int id, MainForm Main)
        {
            MF = Main;
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.NTD.Where(c => c.Id == id).FirstOrDefault();
                    Stroka.VisibleStatus = 0;
                    Stroka.ДатаСозданияЗаписи = DateTime.Now;
                    Stroka.UserName = UserSettings.User;
                    HistoryNTD(Stroka, 400);
                    db.SubmitChanges();
                    db.Dispose();
                    if (!real)
                    {
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.NTDGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        if (gv.FocusedRowHandle > 0)
                        {
                            var Stroke = (NTD)gv.GetRow(gv.FocusedRowHandle - 1);
                            if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                        }
                    }
                    GetNTD(MF);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void NTDArhivButton_Click(int id, MainForm Main)
        {
            MF = Main;
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.NTD.Where(c => c.Id == id).FirstOrDefault();
                    Stroka.VisibleStatus = 1;
                    Stroka.ДатаСозданияЗаписи = DateTime.Now;
                    Stroka.UserName = UserSettings.User;
                    HistoryNTD(Stroka, 401);
                    db.SubmitChanges();
                    db.Dispose();
                    if (!arhiv)
                    {
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.NTDGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        if (gv.FocusedRowHandle > 0)
                        {
                            var Stroke = (NTD)gv.GetRow(gv.FocusedRowHandle - 1);
                            if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                        }
                    }
                    GetNTD(MF);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void NTDPrintSpisok(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                try
                {
                    REPEAT = false;
                    ttt++;
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc1 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\НТД.dotx");
                    var RowCount = MF.NTDGridView.RowCount;
                    //var info = (GridViewInfo)Main.NTDGridView.GetViewInfo();
                    if (RowCount > 1) WordDoc1.AddTableCell(2, RowCount - 1);
                    int Cell = 2;
                    for (int rowHandle = 0; rowHandle < RowCount; rowHandle++)
                    {
                        Cell++;
                        if (MF.NTDGridView.IsDataRow(rowHandle) || Cell >= 39)
                        {
                            try
                            {
                                var SelectedRow = (NTD)MF.NTDGridView.GetRow(rowHandle);
                                WordDoc1.AddTableValue(2, Cell, 1, SelectedRow.Уровень);
                                WordDoc1.AddTableValue(2, Cell, 2, SelectedRow.Группа);
                                WordDoc1.AddTableValue(2, Cell, 3, SelectedRow.N);
                                WordDoc1.AddTableValue(2, Cell, 4, SelectedRow.НД);
                                WordDoc1.AddTableValue(2, Cell, 5, SelectedRow.НаименованиеДокумента);
                                WordDoc1.AddTableValue(2, Cell, 6, SelectedRow.РегистрационныйНомер);
                                WordDoc1.AddTableValue(2, Cell, 7, SelectedRow.ДатаУтверждения == null ? "" : SelectedRow.ДатаУтверждения.Value.ToShortDateString());
                                WordDoc1.AddTableValue(2, Cell, 8, SelectedRow.НаличиеКонтрольногоЭкземпляраКолво);
                                WordDoc1.AddTableValue(2, Cell, 9, SelectedRow.НаличиеКонтрольногоЭкземпляраМестоНахождения);
                                WordDoc1.AddTableValue(2, Cell, 10, SelectedRow.НаличиеРабочегоЭкземпляраКолво);
                                WordDoc1.AddTableValue(2, Cell, 11, SelectedRow.НаличиеРабочегоЭкземпляраМестоНахождения);
                                WordDoc1.AddTableValue(2, Cell, 12, SelectedRow.ДатаПоследнейАктуализации == null ? "" : SelectedRow.ДатаПоследнейАктуализации.Value.ToShortDateString());
                                WordDoc1.AddTableValue(2, Cell, 13, SelectedRow.СрокДействия == null ? "" : SelectedRow.СрокДействия.Value.ToShortDateString());
                                WordDoc1.AddTableValue(2, Cell, 14, SelectedRow.Примечание);
                            }
                            catch { }
                        }
                    }
                    if (UserSettings.NTDColumns != null)
                    {
                        var Visibiliti = UserSettings.NTDColumns.Split(',');
                        for (int i = Visibiliti.Length - 1; i > -1; i--)
                        {
                            if (Visibiliti[i] != "1")
                            {
                                WordDoc1.DeleteTableColumn(2, i + 1);
                            }
                        }

                        int ColumnsCount = 0;
                        for (int Visibl = 0; Visibl < 7; Visibl++)
                        {
                            if (Visibiliti[Visibl] == "1") ColumnsCount++;
                        }
                        if (Visibiliti[7] == "1" && Visibiliti[8] == "1")
                        {
                            WordDoc1.MergeColumns(2, ColumnsCount + 1, ColumnsCount + 2, 1);
                            WordDoc1.AddTableValue(2, 1, ColumnsCount + 1, "Наличие контрольного экземпляра");
                            WordDoc1.AddTableValue(2, 2, ColumnsCount + 1, "Кол-во");
                            WordDoc1.AddTableValue(2, 2, ColumnsCount + 2, "Место нахождения");
                        }
                        else if (Visibiliti[7] == "1")
                        {
                            WordDoc1.AddTableValue(2, 1, ColumnsCount + 1, "Кол-во контрольного экземпляра");
                            WordDoc1.MergeCells(2, 1, 2, ColumnsCount + 1);
                        }
                        else if (Visibiliti[8] == "1")
                        {
                            WordDoc1.AddTableValue(2, 1, ColumnsCount + 1, "Место нахождения контрольного экземпляра");
                            WordDoc1.MergeCells(2, 1, 2, ColumnsCount + 1);
                        }

                        if (Visibiliti[9] == "1" && Visibiliti[10] == "1")
                        {
                            if (Visibiliti[7] == "1" && Visibiliti[8] == "1")
                            {
                                WordDoc1.MergeColumns(2, ColumnsCount + 2, ColumnsCount + 3, 1);
                                WordDoc1.AddTableValue(2, 1, ColumnsCount + 2, "Наличие рабочего экземпляра(копия)");
                                WordDoc1.AddTableValue(2, 2, ColumnsCount + 3, "Кол-во");
                                WordDoc1.AddTableValue(2, 2, ColumnsCount + 4, "Место нахождения");
                            }
                            else
                            {
                                if (Visibiliti[7] == "1" || Visibiliti[8] == "1")
                                {
                                    if (Visibiliti[7] == "1")
                                    {
                                        WordDoc1.AddTableValue(2, 1, ColumnsCount + 1, "Кол-во рабочего экземпляра");
                                        WordDoc1.MergeCells(2, 1, 2, ColumnsCount + 1, ColumnsCount + 1);
                                    }
                                    else
                                    {
                                        WordDoc1.AddTableValue(2, 1, ColumnsCount + 2, "Место нахождения рабочего экземпляра");
                                        WordDoc1.MergeCells(2, 1, 2, ColumnsCount + 1, ColumnsCount + 1);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Visibiliti[9] == "1" || Visibiliti[10] == "1")
                            {
                                if (Visibiliti[9] == "1")
                                {
                                    if (Visibiliti[7] == "1" && Visibiliti[8] == "1")
                                    {
                                        WordDoc1.AddTableValue(2, 1, ColumnsCount + 2, "Кол-во рабочего экземпляра");
                                        WordDoc1.MergeCells(2, 1, 2, ColumnsCount + 2, ColumnsCount + 3);
                                    }
                                    else
                                    {
                                        if (Visibiliti[7] == "1" || Visibiliti[8] == "1")
                                        {
                                            WordDoc1.AddTableValue(2, 1, ColumnsCount + 2, "Кол-во рабочего экземпляра");
                                            WordDoc1.MergeCells(2, 1, 2, ColumnsCount + 2);
                                        }
                                        else
                                        {
                                            WordDoc1.AddTableValue(2, 1, ColumnsCount + 1, "Кол-во рабочего экземпляра");
                                            WordDoc1.MergeCells(2, 1, 2, ColumnsCount + 1);
                                        }
                                    }
                                }
                                else
                                {
                                    if (Visibiliti[7] == "1" && Visibiliti[8] == "1")
                                    {
                                        WordDoc1.AddTableValue(2, 1, ColumnsCount + 2, "Место нахождения рабочего экземпляра");
                                        WordDoc1.MergeCells(2, 1, 2, ColumnsCount + 2, ColumnsCount + 3);
                                    }
                                    else
                                    {
                                        if (Visibiliti[7] == "1" || Visibiliti[8] == "1")
                                        {
                                            WordDoc1.AddTableValue(2, 1, ColumnsCount + 2, "Место нахождения рабочего экземпляра");
                                            WordDoc1.MergeCells(2, 1, 2, ColumnsCount + 2);
                                        }
                                        else
                                        {
                                            WordDoc1.AddTableValue(2, 1, ColumnsCount + 1, "Место нахождения рабочего экземпляра");
                                            WordDoc1.MergeCells(2, 1, 2, ColumnsCount + 1);
                                        }
                                    }
                                }
                            }
                        }
                        //Обьединение строк
                        for (int Columm = 1; Columm < ColumnsCount + 1; Columm++)
                        {
                            if (Visibiliti[Columm - 1] == "1")
                            {
                                WordDoc1.MergeCells(2, 1, 2, Columm);
                            }
                        }
                        for (int t = 7; t < 11; t++)
                        {
                            if (Visibiliti[t] == "1")
                            {
                                ColumnsCount++;
                            }
                        }
                        int LastColumnsCount = ColumnsCount;
                        for (int t = 11; t < Visibiliti.Count(); t++)
                        {
                            if (Visibiliti[t] == "1")
                            {
                                LastColumnsCount++;
                            }
                        }
                        if (Visibiliti[7] == "1" && Visibiliti[8] == "1")
                        {
                            if (Visibiliti[9] == "1" && Visibiliti[10] == "1")
                            {
                                for (int C = ColumnsCount + 1; C < LastColumnsCount + 1; C++)
                                {
                                    WordDoc1.MergeCells(2, 1, 2, C - 2, C);
                                }
                            }
                            else
                            {
                                for (int C = ColumnsCount + 1; C < LastColumnsCount + 1; C++)
                                {
                                    WordDoc1.MergeCells(2, 1, 2, C - 1, C);
                                }
                            }
                        }
                        else
                        {
                            if (Visibiliti[9] == "1" && Visibiliti[10] == "1")
                            {
                                if (Visibiliti[7] == "1" || Visibiliti[8] == "1")
                                {
                                    for (int C = ColumnsCount + 1; C < LastColumnsCount + 1; C++)
                                    {
                                        WordDoc1.MergeCells(2, 1, 2, C - 1, C);
                                    }
                                }
                            }
                            else
                            {
                                for (int C = ColumnsCount + 1; C < LastColumnsCount + 1; C++)
                                {
                                    WordDoc1.MergeCells(2, 1, 2, C);
                                }
                            }
                        }
                    }
                    WordDoc1.TableResize(2);
                    WordDoc1.Visible = true;
                    WordDoc1.wordApplication.Activate();
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc1.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        public void NTDDeleteButton_Click(MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    if (Main.NTDID.Text != "")
                    {
                        DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить \"" + Main.NTDName.Text + "\"?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            var Строка3 = db.NTD.Where(c => c.Id == Convert.ToInt32(Main.NTDID.Text)).FirstOrDefault();
                            if (Строка3 != null)
                            {
                                Строка3.ДатаСозданияЗаписи = DateTime.Now;
                                Строка3.UserName = UserSettings.User;
                                Строка3.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                //db.NTD.DeleteOnSubmit(Строка3);
                                db.SubmitChanges();
                            }
                            DevExpress.XtraGrid.Views.Grid.GridView gv = Main.NTDGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                            if (gv.FocusedRowHandle > 0)
                            {
                                var Stroke = (NTD)gv.GetRow(gv.FocusedRowHandle - 1);
                                if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                            }
                            ClearControlsNTD(Main);
                            GetNTD(Main);
                            ActivateEditNTD(false, Main);
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не выбрана строка для удаления!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void NTDGrid_Click(MainForm Main)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gv = Main.NTDGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                var Stroke = (NTD)gv.GetRow(gv.FocusedRowHandle);
                if (Stroke == null) return;
                SelectedRowId = Stroke.Id.ToString();
                Main.NTDID.Text = Stroke.Id.ToString();
                Main.NTDN.Text = Stroke.N;
                Main.NTDUroven.Text = Stroke.Уровень;
                Main.NTDGroup.Text = Stroke.Группа;
                if (Stroke.НД != null && Stroke.НД.Length > 0)
                {
                    Main.NTDND1.Text = "";
                    Main.NTDND2.Text = "";
                    if (Stroke.НД.Length > 9)
                    {
                        if (Stroke.НД.Substring(0, 9) == "ПРЖ/А-04-")
                        {
                            Main.NTDND1.Text = "ПРЖ/А-04-";
                            Main.NTDND2.Text = Stroke.НД.Substring(9, Stroke.НД.Length - 9);
                        }
                    }
                    if (Main.NTDND1.Text == "" && Main.NTDND2.Text == "")
                    {
                        var ND = Stroke.НД.Split(' ');
                        for (int i = 1; i < ND.Length; i++)
                        {
                            if (i > 1)
                                Main.NTDND2.Text = Main.NTDND1.Text + "";
                            Main.NTDND2.Text = Main.NTDND2.Text + ND[i];
                        }
                        Main.NTDND1.Text = ND[0];
                    }
                }
                else
                {
                    Main.NTDND1.Text = "";
                    Main.NTDND2.Text = "";
                }
                Main.NTDName.Text = Stroke.НаименованиеДокумента;
                Main.NTDRegistN.Text = Stroke.РегистрационныйНомер;
                Main.NTDDateUtvergdenia.Text = Stroke.ДатаУтверждения.ToString();
                Main.NTDKontrolKolvo.Value = Stroke.НаличиеКонтрольногоЭкземпляраКолво == "-" ? 0 : Convert.ToInt32(Stroke.НаличиеКонтрольногоЭкземпляраКолво);
                Main.NTDKontrolMesto.Text = Stroke.НаличиеКонтрольногоЭкземпляраМестоНахождения;
                Main.NTDRabKolvo.Value = Stroke.НаличиеРабочегоЭкземпляраКолво == "-" ? 0 : Convert.ToInt32(Stroke.НаличиеРабочегоЭкземпляраКолво);
                Main.NTDRabMesto.Text = Stroke.НаличиеРабочегоЭкземпляраМестоНахождения;
                Main.NTDDateAktualiz.Text = Stroke.ДатаПоследнейАктуализации.ToString();
                Main.NTDSrokDeisrvia.Text = Stroke.СрокДействия.ToString();
                Main.NTDPrimechanie.Text = Stroke.Примечание;
                Main.NTDEditCheckBox.Checked = false;
                ActivateEditNTD(false, Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void NTDFiltrButton_Click(MainForm Main)
        {
            //try
            //{
            //    SearchNTDForm f = new SearchNTDForm(Main);
            //    f.Owner = Main;
            //    f.ShowDialog();
            //    f.Dispose();
            //}
            //catch (Exception ex)
            //{
            //    LogErrors Log = new LogErrors(ex.ToString());
            //    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        internal void NTDGridView_ColumnWidthChanged(MainForm MF)
        {
            try
            {
                if (ChangeWidth)
                {
                    UserSettings.NTDColumnWidth = "";
                    for (int i = 1; i < 15; i++)
                    {
                        if (UserSettings.NTDColumnWidth != "")
                            UserSettings.NTDColumnWidth = UserSettings.NTDColumnWidth + ",";
                        UserSettings.NTDColumnWidth = UserSettings.NTDColumnWidth + MF.NTDGridView.Columns[i].Width.ToString();
                    }
                    UserSettings.SaveSettings();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void PrintObshiiPerechenNTD(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc3 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\ОбщийПереченьНТД.dotx");
                    EditTableObchii(ref WordDoc3);
                    WordDoc3.Visible = true;
                    WordDoc3.wordApplication.Activate();
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc3.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void AutoWidthColumn(MainForm MF)
        {
            if (UserSettings.NTDColumnAutoWidth)
            {
                UserSettings.NTDColumnAutoWidth = false;
            }
            else UserSettings.NTDColumnAutoWidth = true;
            GetNTD(MF);
            UserSettings.SaveSettings();
        }

        internal void PrintMasterList(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc2 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\МастерЛист.dotx");
                    EditTable(ref WordDoc2);
                    WordDoc2.Visible = true;
                    WordDoc2.wordApplication.Activate();
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc2.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        private void EditTable(ref WordDocument WordDoc)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var ВсеЗначения = db.NTD.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0).OrderBy(c => c.Уровень).ThenBy(c => c.Группа).ThenBy(c => c.Nint).ToList();
                    if (ВсеЗначения != null && ВсеЗначения.Count() > 0)
                    {
                        var SortedNTD = ВсеЗначения; //SortNTD(ВсеЗначения);
                        if (SortedNTD.Count != 0 && SortedNTD != null)
                        {
                            var Urov = "";
                            int TableNumber = 0;
                            int Number = 0;
                            int Group = 0;
                            var CellStart = 3;
                            var CellEnd = 2;
                            var PrintedUrovni = "";
                            foreach (NTD Cell in SortedNTD)
                            {
                                if (Cell.Уровень == "") continue;
                                if (Cell.Уровень != Urov)
                                {
                                    if (TableNumber > 0)
                                    {
                                        if (CellEnd > CellStart)
                                        {
                                            WordDoc.MergeCells(TableNumber + 1, CellStart, CellEnd, 1);
                                        }
                                        if (CellEnd > 2)
                                        {
                                            AddValueGroupField(TableNumber, Group, CellStart, WordDoc);
                                        }
                                    }
                                    Number = 1;
                                    Urov = Cell.Уровень;
                                    switch (Cell.Уровень)
                                    {
                                        case "I.":
                                            TableNumber = 1;
                                            WordDoc.ReplaceString("{1Группа}", "\r\rI. Нормативные документы, регламентирующие деятельность лаборатории в области аккредитации, правовые и организационные документы аккредитованной лаборатории. Документы, описывающие систему обеспечения качества\r");
                                            break;
                                        case "II.":
                                            TableNumber = 2;
                                            WordDoc.ReplaceString("{2Группа}", "\r\rII. Нормативные документы, устанавливающие требования в области метрологического обеспечения, технические требования к объектам и к методам аналитических работ\r");
                                            break;
                                        case "III.":
                                            TableNumber = 3;
                                            WordDoc.ReplaceString("{3Группа}", "\r\rIII. Документы содержащие сведения об используемом оборудовании. Документы на применение технических средств, материалы, реактивы\r");
                                            break;
                                        case "IV.":
                                            TableNumber = 4;
                                            WordDoc.ReplaceString("{4Группа}", "\r\rVI. Документы, отвечающие требованиям менеджмента\r");
                                            break;
                                        case "V.":
                                            TableNumber = 5;
                                            WordDoc.ReplaceString("{5Группа}", "\r\rV. Документы, содержащие свидетельства выполненных действий\r");
                                            break;
                                        case "VI.":
                                            TableNumber = 6;
                                            break;
                                        case "VII.":
                                            TableNumber = 7;
                                            break;
                                        case "VIII.":
                                            TableNumber = 8;
                                            break;
                                        case "IX.":
                                            TableNumber = 9;
                                            break;
                                        case "X.":
                                            TableNumber = 10;
                                            break;
                                        case "XI.":
                                            TableNumber = 11;
                                            break;
                                        case "XII.":
                                            TableNumber = 12;
                                            break;
                                        case "XIII.":
                                            TableNumber = 13;
                                            break;
                                        case "XIV.":
                                            TableNumber = 14;
                                            break;
                                        case "XV.":
                                            TableNumber = 15;
                                            break;
                                    }
                                    if (PrintedUrovni != "")
                                        PrintedUrovni = PrintedUrovni + ",";
                                    PrintedUrovni = PrintedUrovni + TableNumber;
                                    CellStart = 3;
                                    CellEnd = 2;
                                }
                                if (Cell.Группа == "") continue;
                                if (Convert.ToInt32(Cell.Группа.Split('.')[0]) != Group)
                                {
                                    if (CellEnd > CellStart)
                                    {
                                        WordDoc.MergeCells(TableNumber + 1, CellStart, CellEnd, 1);
                                    }
                                    if (CellEnd > 2)
                                    {
                                        AddValueGroupField(TableNumber, Group, CellStart, WordDoc);
                                    }
                                    CellStart = CellEnd + 1;
                                    Group = Convert.ToInt32(Cell.Группа.Substring(0, Cell.Группа.Length - 1));
                                    Number = 1;
                                }
                                CellEnd++;
                                if (CellEnd != 3)
                                    WordDoc.AddTableCell(TableNumber + 1, 1);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 2, Cell.N);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 3, Cell.НД);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 4, Cell.НаименованиеДокумента);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 5, Cell.РегистрационныйНомер);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 6, Cell.ДатаПоследнейАктуализации.HasValue ? Cell.ДатаПоследнейАктуализации.Value.ToShortDateString() : "");
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 7, Cell.НаличиеКонтрольногоЭкземпляраКолво);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 8, Cell.НаличиеКонтрольногоЭкземпляраМестоНахождения);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 9, Cell.НаличиеРабочегоЭкземпляраКолво);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 10, Cell.НаличиеРабочегоЭкземпляраМестоНахождения);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 11, Cell.Примечание);
                                Number++;
                            }
                            if (CellEnd > CellStart)
                            {
                                WordDoc.MergeCells(TableNumber + 1, CellStart, CellEnd, 1);
                            }
                            if (CellEnd > 2)
                            {
                                AddValueGroupField(TableNumber, Group, CellStart, WordDoc);
                            }
                            var DontDel = PrintedUrovni.Split(',');
                            for (int i = 9; i > 0; i--)
                            {
                                var NoDel = false;
                                foreach (var Tabl in DontDel)
                                {
                                    if (i.ToString() == Tabl)
                                    {
                                        NoDel = true;
                                        break;
                                    }
                                }
                                if (!NoDel)
                                {
                                    WordDoc.DelTabl(i + 1);
                                    WordDoc.ReplaceString("{" + i + "Группа}", "");
                                }
                            }
                        }
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void NTDPrintOsnachinnost(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc4 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Оснащенность НТД.dotx");
                    List<NTD> ВсеЗначения = new List<NTD>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.NTD.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null).OrderBy(c => c.Уровень).ThenBy(c => c.Группа).ThenBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - NTD");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc4.AddTableCell(1, RowCount - 1);
                    int Cell = 2;
                    foreach (var NTD in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                WordDoc4.AddTableValue(1, Cell, 1, (Cell - 2).ToString());
                                WordDoc4.AddTableValue(1, Cell, 2, NTD.НД);
                                WordDoc4.AddTableValue(1, Cell, 3, NTD.НаименованиеДокумента);
                                WordDoc4.AddTableValue(1, Cell, 4, (NTD.СрокДействия.HasValue ? NTD.СрокДействия.Value.ToShortDateString() : "Без ограничения"));
                                WordDoc4.AddTableValue(1, Cell, 5, NTD.РегистрационныйНомер + (NTD.РегистрационныйНомер == String.Empty ? String.Empty : " ") + (NTD.ДатаУтверждения.HasValue ? NTD.ДатаУтверждения.Value.ToShortDateString() : String.Empty));
                                WordDoc4.AddTableValue(1, Cell, 6, NTD.Примечание);
                            }
                            catch { }
                        }
                    }
                    WordDoc4.TableResize(1);
                    WordDoc4.ReplaceString("{ГОД}", DateTime.Now.Year.ToString());
                    WordDoc4.Visible = true;
                    WordDoc4.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc4.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void NTDVisibleArhivButton_Click(MainForm Main)
        {
            if (Main.NTDArhivButton.ContextMenuStrip != null)
            {
                Main.NTDArhivButton.ContextMenuStrip.Close();
                Main.NTDArhivButton.ContextMenuStrip = null;
                return;
            }
            MF = Main;
            ContextMenuStrip ContextMS = new ContextMenuStrip();
            ContextMS.Items.Add("&Показать рабочие документы", null, NTDRealVisible);
            if (real)
            {
                //ContextMS.Items.Add("&Скрыть рабочие документы", null, NTDRealVisible);
                ((ToolStripMenuItem)ContextMS.Items[0]).Checked = true;
            }
            else
            {
                ((ToolStripMenuItem)ContextMS.Items[0]).Checked = false;
            }
            ContextMS.Items.Add("&Показать архивные документы", null, NTDArhivVisible);
            if (arhiv)
            {
                //ContextMS.Items.Add("&Скрыть архивные документы", null, NTDArhivVisible);
                ((ToolStripMenuItem)ContextMS.Items[1]).Checked = true;
            }
            else
            {
                ((ToolStripMenuItem)ContextMS.Items[1]).Checked = false;
            }
            ContextMS.Items.Add("&Закрыть", global::LIMS.Properties.Resources.delete_16x16, ContextMenuStripClose_Click);
            ContextMS.AutoClose = false;
            Main.NTDArhivButton.ContextMenuStrip = ContextMS;
            ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);
        }

        public void ContextMenuStripClose_Click(object sender, EventArgs e)
        {
            var TSMI = sender as ToolStripMenuItem;
            var CMS = TSMI.Owner as ContextMenuStrip;
            CMS.AutoClose = true;
            MF.NTDArhivButton.ContextMenuStrip = null;
            CMS.Close();
        }

        private void NTDRealVisible(object sender, EventArgs e)
        {
            real = real ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = real;
            GetNTD(MF);
        }

        private void NTDArhivVisible(object sender, EventArgs e)
        {
            arhiv = arhiv ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = arhiv;
            GetNTD(MF);
        }

        private void EditTableObchii(ref WordDocument WordDoc)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var ВсеЗначения = db.NTD.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0).OrderBy(c => c.Уровень).ThenBy(c => c.Группа).ThenBy(c => c.Nint).ToList();
                    if (ВсеЗначения != null && ВсеЗначения.Count() > 0)
                    {
                        var SortedNTD = SortNTD(ВсеЗначения);
                        if (SortedNTD.Count != 0 && SortedNTD != null)
                        {
                            var Urov = "";
                            int TableNumber = 0;
                            int Number = 0;
                            int Group = 0;
                            var CellStart = 2;
                            var CellEnd = 1;
                            var PrintedUrovni = "";
                            foreach (NTD Cell in SortedNTD)
                            {
                                if (Cell.Уровень == "") continue;
                                if (Cell.Уровень != Urov)
                                {
                                    if (TableNumber > 0)
                                    {
                                        if (CellEnd > CellStart)
                                        {
                                            WordDoc.MergeCells(TableNumber + 1, CellStart, CellEnd, 1);
                                        }
                                        if (CellEnd > 1)
                                        {
                                            AddValueGroupField(TableNumber, Group, CellStart, WordDoc);
                                        }
                                    }
                                    Number = 1;
                                    Urov = Cell.Уровень;
                                    switch (Cell.Уровень)
                                    {
                                        case "I.":
                                            TableNumber = 1;
                                            WordDoc.ReplaceString("{1Группа}", "\r\rI. Нормативные документы, регламентирующие деятельность лаборатории в области аккредитации, правовые и организационные документы аккредитованной лаборатории. Документы, описывающие систему обеспечения качества\r");
                                            break;
                                        case "II.":
                                            TableNumber = 2;
                                            WordDoc.ReplaceString("{2Группа}", "\r\rII. Нормативные документы, устанавливающие требования в области метрологического обеспечения, технические требования к объектам и к методам аналитических работ\r");
                                            break;
                                        case "III.":
                                            TableNumber = 3;
                                            WordDoc.ReplaceString("{3Группа}", "\r\rIII. Документы содержащие сведения об используемом оборудовании. Документы на применение технических средств, материалы, реактивы\r");
                                            break;
                                        case "IV.":
                                            TableNumber = 4;
                                            WordDoc.ReplaceString("{4Группа}", "\r\rVI. Документы, отвечающие требованиям менеджмента\r");
                                            break;
                                        case "V.":
                                            TableNumber = 5;
                                            WordDoc.ReplaceString("{5Группа}", "\r\rV. Документы, содержащие свидетельства выполненных действий\r");
                                            break;
                                        case "VI.":
                                            TableNumber = 6;
                                            break;
                                        case "VII.":
                                            TableNumber = 7;
                                            break;
                                        case "VIII.":
                                            TableNumber = 8;
                                            break;
                                        case "IX.":
                                            TableNumber = 9;
                                            break;
                                        case "X.":
                                            TableNumber = 10;
                                            break;
                                        case "XI.":
                                            TableNumber = 11;
                                            break;
                                        case "XII.":
                                            TableNumber = 12;
                                            break;
                                        case "XIII.":
                                            TableNumber = 13;
                                            break;
                                        case "XIV.":
                                            TableNumber = 14;
                                            break;
                                        case "XV.":
                                            TableNumber = 15;
                                            break;
                                    }
                                    if (PrintedUrovni != "")
                                        PrintedUrovni = PrintedUrovni + ",";
                                    PrintedUrovni = PrintedUrovni + TableNumber;
                                    CellStart = 2;
                                    CellEnd = 1;
                                }
                                if (Cell.Группа == "") continue;
                                if (Convert.ToInt32(Cell.Группа.Split('.')[0]) != Group)
                                {
                                    if (CellEnd > CellStart)
                                    {
                                        WordDoc.MergeCells(TableNumber + 1, CellStart, CellEnd, 1);
                                    }
                                    if (CellEnd > 2)
                                    {
                                        AddValueGroupField(TableNumber, Group, CellStart, WordDoc);
                                    }
                                    CellStart = CellEnd + 1;
                                    Group = Convert.ToInt32(Cell.Группа.Substring(0, Cell.Группа.Length - 1));
                                    Number = 1;
                                }
                                CellEnd++;
                                if (CellEnd != 2)
                                    WordDoc.AddTableCell(TableNumber + 1, 1);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 2, Number + ".");
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 3, Cell.НД);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 4, Cell.НаименованиеДокумента);
                                WordDoc.AddTableValue(TableNumber + 1, CellEnd, 5, Cell.Примечание);
                                Number++;
                            }
                            if (CellEnd > CellStart)
                            {
                                WordDoc.MergeCells(TableNumber + 1, CellStart, CellEnd, 1);
                            }
                            if (CellEnd > 1)
                            {
                                AddValueGroupField(TableNumber, Group, CellStart, WordDoc);
                            }
                            var DontDel = PrintedUrovni.Split(',');
                            for (int i = 9; i > 0; i--)
                            {
                                var NoDel = false;
                                foreach (var Tabl in DontDel)
                                {
                                    if (i.ToString() == Tabl)
                                    {
                                        NoDel = true;
                                        break;
                                    }
                                }
                                if (!NoDel)
                                {
                                    WordDoc.DelTabl(i + 1);
                                    WordDoc.ReplaceString("{" + i + "Группа}", "");
                                }
                            }
                        }
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddValueGroupField(int TableNumber, int Group, int CellStart, WordDocument WordDoc)
        {
            WordDoc.VerticalTableValue(TableNumber + 1, CellStart, 1);
            switch (TableNumber)
            {
                case 1:
                    switch (Group)
                    {
                        case 1:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "1. Организационные документы лаборатории (Положение о лаборатории; Аттестат аккредитации; Область аккредитации; Паспорт лаборатории; Руководство по качеству; Должностные инструкции).");
                            break;
                        case 2:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "2. Государственные стандарты на нефть и на методики выполнения измерений.");
                            break;
                        case 3:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "3. Документы, регламентирующие деятельность Системы менеджмента качества (Политика в области качества, Программа проведения внутренних проверок, График проведения внутреннего горизонтального аудита, График проведения внутреннего вертикального аудита и т.д.).");
                            break;
                        case 4:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "4. Документы, регламентирующие деятельность аккредитованной лаборатории.");
                            break;
                    }
                    break;
                case 2:
                    switch (Group)
                    {
                        case 1:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "1. Документы Системы обеспечения единства измерений в области метрологического обеспечения испытаний нефти, Государственный стандарт на отбор проб.");
                            break;
                        case 2:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "2. Государственные стандарты на средства измерений, лабораторную посуду, методы приготовления вспомогательных и тированных растворов.");
                            break;
                        case 3:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "3. Документы Системы стандартов пожарной безопасности, охраны труда, Системы стандартов в области охраны природы и др.");
                            break;
                        case 4:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "4. Методические пособия, справочные и расчетные таблицы.");
                            break;
                    }
                    break;
                case 3:
                    switch (Group)
                    {
                        case 1:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "1. Паспорта на оборудование.");
                            break;
                        case 2:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "2. Паспорта на реактивы.");
                            break;
                        case 3:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "3. Паспорта на СО.");
                            break;
                        case 4:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "4. Графики поверки, аттестации, проверки тех. состояния и технического обслуживания.");
                            break;
                        case 5:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "5. Документы на средства измерений (Методики поверки, свидетельства о поверке, формуляры, протоколы и т.д.).");
                            break;
                        case 6:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "6. Документы на испытательное оборудование (Методики аттестации, аттестаты, формуляры, протоколы и т.д.).");
                            break;
                        case 7:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "7. Документы на вспомогательное оборудование (Методики проверки технического состояния, формуляры, протоколы и т.д.).");
                            break;
                        case 8:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "8. Акты проверки технического состояния.");
                            break;
                        case 9:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "9. Инструкции по эксплуатации оборудования и охране труда при эксплуатации.");
                            break;
                    }
                    break;
                case 4:
                    switch (Group)
                    {
                        case 1:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "1. Журналы внешнего контроля и внутренних проверок.");
                            break;
                        case 2:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "2. Внутрилабораторный контроль качества результатов испытаний.");
                            break;
                        //case 3:
                        //    WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "3. Документы, о предоставлении услуг сторонним организациям.");
                        //    break;
                        case 3:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "3. Заключение об участии лаборатории в МСИ.");
                            break;
                        case 4:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "4. Инструкции, разработанные лабораторией.");
                            break;
                    }
                    break;
                case 5:
                    switch (Group)
                    {
                        case 1:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "1. Журналы и документы регистрации технических данных.");
                            break;
                        //case 2:
                        //    WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "2. Журналы регистрации данных по качеству.");
                        //    break;
                        case 2:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "2. Протоколы результатов испытаний.");
                            break;
                        case 3:
                            WordDoc.AddTableValue(TableNumber + 1, CellStart, 1, "3.Графики.");
                            break;
                    }
                    break;
            }
        }


        private List<NTD> SortNTD(List<NTD> DB)
        {
            List<NTD> SortedDB = new List<NTD>();
            for (int Uroven = 1; Uroven < 16; Uroven++)
            {
                var Urov = "";
                switch (Uroven)
                {
                    case 1:
                        Urov = "I";
                        break;
                    case 2:
                        Urov = "II";
                        break;
                    case 3:
                        Urov = "III";
                        break;
                    case 4:
                        Urov = "IV";
                        break;
                    case 5:
                        Urov = "V";
                        break;
                    case 6:
                        Urov = "VI";
                        break;
                    case 7:
                        Urov = "VII";
                        break;
                    case 8:
                        Urov = "VIII";
                        break;
                    case 9:
                        Urov = "IX";
                        break;
                    case 10:
                        Urov = "X";
                        break;
                    case 11:
                        Urov = "XI";
                        break;
                    case 12:
                        Urov = "XII";
                        break;
                    case 13:
                        Urov = "XIII";
                        break;
                    case 14:
                        Urov = "XIV";
                        break;
                    case 15:
                        Urov = "XV";
                        break;
                }
                for (int Group = 1; Group < 16; Group++)
                {
                    for (int N = 1; N < 50/*DB.Count + 1*/; N++)
                    {
                        foreach (NTD Cell in DB)
                        {
                            var N0 = Cell.N.Split('.');
                            var G = Cell.Группа.Split('.');
                            var U = Cell.Уровень.Split('.');
                            if (U[0] == Urov && G[0] == Group.ToString() && N0[0] == N.ToString())
                            {
                                SortedDB.Add(Cell);
                            }
                        }
                    }
                }
            }
            return SortedDB;
        }

        private List<NTD> NewSortNTD(List<NTD> DB)
        {
            List<NTD> SortedDB = new List<NTD>();
            for (int Uroven = 1; Uroven < 16; Uroven++)
            {
                var Urov = "";
                switch (Uroven)
                {
                    case 1:
                        Urov = "I";
                        break;
                    case 2:
                        Urov = "II";
                        break;
                    case 3:
                        Urov = "III";
                        break;
                    case 4:
                        Urov = "IV";
                        break;
                    case 5:
                        Urov = "V";
                        break;
                    case 6:
                        Urov = "VI";
                        break;
                    case 7:
                        Urov = "VII";
                        break;
                    case 8:
                        Urov = "VIII";
                        break;
                    case 9:
                        Urov = "IX";
                        break;
                    case 10:
                        Urov = "X";
                        break;
                    case 11:
                        Urov = "XI";
                        break;
                    case 12:
                        Urov = "XII";
                        break;
                    case 13:
                        Urov = "XIII";
                        break;
                    case 14:
                        Urov = "XIV";
                        break;
                    case 15:
                        Urov = "XV";
                        break;
                }
                for (int Group = 1; Group < 16; Group++)
                {
                    for (int N = 1; N < 50/*DB.Count + 1*/; N++)
                    {
                        var NextCell = true;
                        string RealGroup = "";
                        //string RealN = "";
                        string RealUroven = "";
                        while (NextCell)
                        {
                            foreach (NTD Cell in DB)
                            {
                                RealGroup = Cell.Группа;
                                RealUroven = Cell.Уровень;
                                var N0 = Cell.N.Split('.');
                                var G = Cell.Группа.Split('.');
                                var U = Cell.Уровень.Split('.');
                                if (U[0] == Urov && G[0] == Group.ToString() && N0[0] == N.ToString())
                                {
                                    SortedDB.Add(Cell);
                                    DB.Remove(Cell);
                                    NextCell = true;

                                    List<NTD> FilterDB = new List<NTD>();
                                    var NewNextCell = true;
                                    while (NewNextCell)
                                    {
                                        NewNextCell = false;
                                        foreach (NTD FilterCell in DB)
                                        {
                                            if (FilterCell.Группа == RealGroup && FilterCell.Уровень == RealUroven)
                                            {
                                                FilterDB.Add(Cell);
                                                DB.Remove(Cell);
                                                NewNextCell = true;
                                                break;
                                            }
                                        }
                                    }
                                    for (int NewN = 1; NewN < 50; NewN++)
                                    {
                                        foreach (NTD OtherCell in FilterDB)
                                        {
                                            if (OtherCell.Уровень.Contains(N.ToString() + "."))
                                            {
                                                SortedDB.Add(OtherCell);
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
            return SortedDB;
        }

        public void CheckColumnButton_Click(MainForm Main)
        {
            try
            {
                CheckNTDColumnForm f = new CheckNTDColumnForm(Main, this);
                f.Owner = Main;
                f.ShowDialog();
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void NTDDateUtvergdeniaTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.NTDDateUtvergdeniaTip.Text == "дата")
                Main.NTDDateUtvergdenia.Visible = true;
            else Main.NTDDateUtvergdenia.Visible = false;
        }

        internal void NTDDateAktualizTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.NTDDateAktualizTip.Text == "дата")
                Main.NTDDateAktualiz.Visible = true;
            else Main.NTDDateAktualiz.Visible = false;
        }

        internal void NTDSrokDeisrviaTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.NTDSrokDeisrviaTip.Text == "дата")
                Main.NTDSrokDeisrvia.Visible = true;
            else Main.NTDSrokDeisrvia.Visible = false;
        }

        internal void NTDCellMergeButton_Click(MainForm Main)
        {
            UserSettings.NTDCellMerge = !UserSettings.NTDCellMerge;
            Main.NTDGridView.OptionsView.AllowCellMerge = UserSettings.NTDCellMerge;
            if (UserSettings.NTDCellMerge)
                Main.NTDCellMergeButton.Image = global::LIMS.Properties.Resources.freezetoprow_32x32;
            else Main.NTDCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            UserSettings.SaveSettings();
        }

        internal void splitContainerControl2_Panel1_SizeChanged(object sender)
        {
            try
            {
                var SplitCon = sender as DevExpress.XtraEditors.SplitGroupPanel;
                UserSettings.NTDSplitWidth = SplitCon.Width;
                UserSettings.SaveSettings();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
