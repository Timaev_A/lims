﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using static LIMS.NumberToString.Число;

namespace LIMS
{
    public class VOTab
    {
        public bool NewVO = false;
        public bool CopyVO = false;
        List<int> orderList = new List<int>();
        private bool ChangeWidth = true;
        WordDocument WordDoc1;
        WordDocument WordDoc2;
        WordDocument WordDoc3;
        WordDocument WordDoc4;
        WordDocument WordDoc5;
        WordDocument WordDoc6;
        MainForm MF;
        bool arhiv = false;
        bool real = true;
        internal bool HistoryOfChange = false;
        string SelectedRowId = "";
        DevExpress.Data.ColumnSortOrder SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
        public void GetVO(MainForm Main)
        {
            try
            {
                new Thread(GetVOThread).Start(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void GetVOThread(object MainF)
        {
            try
            {
                var Main = MainF as MainForm;
                MF = Main;
                using (DbDataContext db = new DbDataContext())
                {
                    List<VO> ВсеЗначения = new List<VO>();
                    try
                    {
                        if (!UserSettings.Developer)
                        {
                            if (!arhiv && real) ВсеЗначения = db.VO.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null).OrderBy(c => c.Nint).ToList();
                            else if (arhiv && !real) ВсеЗначения = db.VO.Where(c => c.VisibleStatus == 1).OrderBy(c => c.Nint).ToList();
                            else if (arhiv && real) ВсеЗначения = db.VO.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null || c.VisibleStatus == 1).OrderBy(c => c.Nint).ToList();
                            else ВсеЗначения = null;
                        }
                        else ВсеЗначения = db.VO.OrderBy(c => c.Nint).ToList();
                    }
                    catch
                    {
                        LogErrors Log = new LogErrors("Ошибка соединения с сервером - ВИ");
                        Main.Invoke(new System.Action(() => MessageBox.Show("Сервер не отвечает!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    }

                    if (HistoryOfChange) ВсеЗначения = AddHistory(ВсеЗначения);
                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.VOGridView.FocusedRowChanged -= Main.VOGridView_FocusedRowChanged;
                        Main.VOGrid.DataSource = ВсеЗначения;
                        if (!UserSettings.Developer)
                        {
                            Main.VOGridView.Columns[0].Visible = false;
                            Main.VOGridView.Columns[17].Visible = false;
                            Main.VOGridView.Columns[18].Visible = false;
                            Main.VOGridView.Columns[19].Visible = false;
                            Main.VOGridView.Columns[20].Visible = false;
                            Main.VOGridView.Columns[21].Visible = false;
                        }
                    }));
                    if (orderList.Count < 1)
                    {
                        Main.Invoke(new System.Action(() =>
                        {
                            for (int i = 0; i < Main.VOGridView.Columns.Count; i++)
                                orderList.Add(Main.VOGridView.Columns[i].VisibleIndex);
                        }));
                    }
                    for (int i = 0; i < Main.VOGridView.Columns.Count; i++)
                        Main.Invoke(new System.Action(() => Main.VOGridView.Columns[i].VisibleIndex = orderList[i]));
                    if (UserSettings.VOColumns != null)
                    {
                        var Visibiliti = UserSettings.VOColumns.Split(',');
                        for (int i = 0; i < Visibiliti.Length; i++)
                        {
                            Main.Invoke(new System.Action(() => Main.VOGridView.Columns[i + 1].Visible = (Visibiliti[i] == "1")));
                        }
                    }

                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.VOGridView.FocusedRowChanged += Main.VOGridView_FocusedRowChanged;
                        ColumnView View = Main.VOGridView;
                        GridColumn column = View.Columns["Id"];
                        if (column != null)
                        {
                            int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                            if (rhFound != GridControl.InvalidRowHandle)
                            {
                                View.FocusedRowHandle = rhFound;
                                View.FocusedColumn = column;
                            }
                        }
                    }));

                    Main.Invoke(new System.Action(() =>
                    {
                        Main.VOGridView.Columns[1].Caption = "№ п/п";
                        Main.VOGridView.Columns[2].Caption = "Наименование";
                        Main.VOGridView.Columns[3].Caption = "Тип, марка";
                        Main.VOGridView.Columns[4].Caption = "Заводской номер";
                        Main.VOGridView.Columns[5].Caption = "Изготовитель (страна, наименование организации)";
                        Main.VOGridView.Columns[6].Caption = "Год выпуска";
                        Main.VOGridView.Columns[7].Caption = "Год ввода в эксплуатацию";
                        Main.VOGridView.Columns[8].Caption = "Инвентарный номер";
                        Main.VOGridView.Columns[9].Caption = "Назначение";
                        Main.VOGridView.Columns[10].Caption = "Право собственности";
                        Main.VOGridView.Columns[11].Caption = "Место установки или хранения";
                        Main.VOGridView.Columns[12].Caption = "Дата проверки технического состояния";
                        Main.VOGridView.Columns[13].Caption = "Дата следующей проверки";
                        Main.VOGridView.Columns[14].Caption = "Дата последнего ТО";
                        Main.VOGridView.Columns[15].Caption = "Дата следующего ТО";
                        Main.VOGridView.Columns[16].Caption = "Примечание";
                        Main.VOGridView.Columns[11].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.VOGridView.Columns[11].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.VOGridView.Columns[12].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.VOGridView.Columns[12].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.VOGridView.Columns[14].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.VOGridView.Columns[14].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.VOGridView.Columns[15].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.VOGridView.Columns[15].DisplayFormat.FormatString = "dd/MM/yyyy";
                    }));
                    if (UserSettings.VOColumnWidth != "" && UserSettings.VOColumnWidth != null)
                    {
                        ChangeWidth = false;
                        var ColumnWidth = UserSettings.VOColumnWidth.Split(',');
                        for (int i = 1; i < 17; i++)
                        {
                            Main.Invoke(new System.Action(() => Main.VOGridView.Columns[i].Width = Convert.ToInt32(ColumnWidth[i - 1])));
                        }
                        ChangeWidth = true;
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        Main.VOGridView.OptionsView.RowAutoHeight = true;
                        Main.VOGridView.OptionsView.ColumnAutoWidth = UserSettings.VOColumnAutoWidth;
                        //Wrap Text
                        DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
                        MemoEdit.WordWrap = true;
                        Main.VOGridView.OptionsView.RowAutoHeight = true;
                        Main.VOGridView.OptionsView.AllowCellMerge = UserSettings.VOCellMerge;
                        foreach (GridColumn My_GridColumn in Main.VOGridView.Columns)
                        {
                            My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                            My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            My_GridColumn.ColumnEdit = MemoEdit;

                        }
                        Main.VOSaveButton.Enabled = false;
                        Main.VOEditCheckBox.Checked = false;
                    }));
                    db.Dispose();
                }

                Main.Invoke(new System.Action(() =>
                {
                    //Main.VOGridView.FocusedRowChanged -= Main.VOGridView_FocusedRowChanged;
                    ColumnView View = Main.VOGridView;
                    GridColumn column = View.Columns["Id"];
                    if (column != null)
                    {
                        int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                        if (rhFound != GridControl.InvalidRowHandle)
                        {
                            View.FocusedRowHandle = rhFound;
                            View.FocusedColumn = column;
                        }
                    }
                    //Main.VOGridView.FocusedRowChanged += Main.VOGridView_FocusedRowChanged;
                }));
            }
            catch (Exception ex)
            {
                ChangeWidth = true;
                LogErrors Log = new LogErrors(ex.ToString());
                MF.Invoke(new System.Action(() => MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
            }
        }

        private List<VO> AddHistory(List<VO> всеЗначения)
        {
            try
            {
                List<VO> NewList = new List<VO>();
                using (DbDataContext db = new DbDataContext())
                {
                    foreach (VO StrokeVO in всеЗначения)
                    {
                        var VOHistory = db.VO.Where(c => c.Owner == StrokeVO.Id && (c.VisibleStatus == 100 || c.VisibleStatus == 200)).OrderBy(c => c.ДатаСозданияЗаписи).ToList();
                        if (VOHistory != null && VOHistory.Count > 0)
                        {
                            var i = 0;
                            foreach (VO OtherVO in VOHistory)
                            {
                                i++;
                                if (i > 1)
                                {
                                    OtherVO.N = "";
                                    OtherVO.Наименование = "";
                                    //OtherVO.ТипМарка = "";
                                    //OtherVO.Изготовитель = "";
                                    //OtherVO.ЗаводскойНомер = "";
                                    //OtherVO.ГодВыпуска = "";
                                    //OtherVO.ГодВводаВЭксплуатациюИнвентарныйНомер = "";
                                    //OtherVO.ИнвентарныйНомер = "";
                                }
                                NewList.Add(OtherVO);
                            }
                        }
                        else NewList.Add(StrokeVO);
                    }
                    db.Dispose();
                }
                return NewList;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return всеЗначения;
            }
        }

        public void VOCopyButton_Click(MainForm Main)
        {
            try
            {
                if (Main.VOID.Text != "")
                {
                    Main.VOEditCheckBox.Enabled = false;
                    NewVO = false;
                    Main.VONewButton.Text = "Добавить";
                    Main.VONewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    ActivateEditVO(true, Main);
                    NewVO = false;
                    CopyVO = true;
                    Main.VOSaveButton.Enabled = true;
                    Main.VOEditCheckBox.Checked = false;
                }
                else
                {
                    MessageBox.Show("Не выбран элемент!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void VODeleteButton_Click(MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    if (Main.VOID.Text != "")
                    {
                        DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить \"" + Main.VOName.Text + "\"?", "Удаление", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            var Строка3 = db.VO.Where(c => c.Id == Convert.ToInt32(Main.VOID.Text)).FirstOrDefault();
                            if (Строка3 != null)
                            {
                                Строка3.UserName = UserSettings.User;
                                Строка3.ДатаСозданияЗаписи = DateTime.Now;
                                Строка3.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                //db.VO.DeleteOnSubmit(Строка3);
                                db.SubmitChanges();
                            }
                            DevExpress.XtraGrid.Views.Grid.GridView gv = Main.VOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                            if (gv.FocusedRowHandle > 0)
                            {
                                var Stroke = (VO)gv.GetRow(gv.FocusedRowHandle - 1);
                                if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                            }
                            ClearControlsVO(Main);
                            GetVO(Main);
                            ActivateEditVO(false, Main);
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не выбрана строка для удаления!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        protected void ClearControlsVO(MainForm Main)
        {
            foreach (Control c in Main.VOPanel.Controls)
            {
                if (c is TextBox)
                {
                    var TB = c as TextBox;
                    TB.Text = "";
                }
                if (c is NumericUpDown)
                {
                    var NUB = c as NumericUpDown;
                    NUB.Text = "";
                }
                if (c is ComboBox)
                {
                    var CB = c as ComboBox;
                    CB.Text = "";
                }
                if (c is DateTimePicker)
                {
                    var DTP = c as DateTimePicker;
                    DTP.Text = "";
                    DTP.Checked = false;
                }
                Main.VOID.Text = "";
            }
        }

        private void ActivateEditVO(bool EditableFields, MainForm Main)
        {
            try
            {
                if (EditableFields)
                {
                    foreach (Control c in Main.VOPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = false;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.ReadOnly = false;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = true;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = true;
                            DTP.Checked = false;
                        }
                    }
                    Main.VOSaveButton.Enabled = true;
                }
                else
                {
                    foreach (Control c in Main.VOPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = true;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.ReadOnly = true;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = false;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = false;
                            DTP.Checked = false;
                        }
                        Main.VOEditCheckBox.Enabled = true;
                    }
                    NewVO = false;
                    Main.VONewButton.Text = "Добавить";
                    Main.VONewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.VOSaveButton.Enabled = false;
                    Main.VOEditCheckBox.Checked = false;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void VOEditCheckBox_CheckedChanged(MainForm Main)
        {
            if (!NewVO && Main.VOID.Text != "")
            {
                ActivateEditVO(Main.VOEditCheckBox.Checked, Main);
            }
            else
            {
                Main.VOEditCheckBox.Checked = false;
            }
        }

        public void VOSaveButton_Click(MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    try
                    {
                        var t = Convert.ToInt32(Main.VON.Text);
                    }
                    catch
                    {
                        MessageBox.Show("Не корректное значения в поле \"№ п/п\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        db.Dispose();
                        return;
                    }
                    //Редактирование
                    if (!NewVO && !CopyVO)
                    {
                        if (Main.VOID.Text != "")
                        {
                            var EditID = Convert.ToInt32(Main.VOID.Text);
                            var Stroka = db.VO.Where(c => c.Id == EditID).FirstOrDefault();
                            Stroka.Id = EditID;
                            try
                            {
                                var IntN = Main.VON.Text.Split('.');
                                Stroka.Nint = Convert.ToInt32(IntN[0]);
                            }
                            catch { }
                            Stroka.N = Main.VON.Text;
                            Stroka.Наименование = Main.VOName.Text;
                            Stroka.ТипМарка = Main.VOTipMarka.Text;
                            Stroka.ЗаводскойНомер = Main.VOZavodNomer.Text;
                            Stroka.Изготовитель = Main.VOIzgotovitel.Text;
                            Stroka.ГодВыпуска = Main.VOGodVypuska.Text;
                            Stroka.ГодВводаВЭксплуатациюИнвентарныйНомер = Main.VOGodVvodaVEkspltatechiu.Text;
                            Stroka.ИнвентарныйНомер = Main.VOInventarNomer.Text;
                            Stroka.Назначение = Main.VONaznachenie.Text;
                            Stroka.ПравоСобственности = Main.VOPravoSobstvennosti.Text;
                            Stroka.МестоУстановкиИлиХранения = Main.VOMestoUstanovkiIHranenia.Text;
                            if (Main.VODateProverkiTehnicheskogoSostoianiaTip.Text != "нет")
                                Stroka.ДатаПроверкиТехническогоСостояния = DateTime.Parse(Main.VODateProverkiTehnicheskogoSostoiania.Text).Date;
                            else Stroka.ДатаПроверкиТехническогоСостояния = null;
                            if (Main.VODateSledProverkiTip.Text != "нет")
                            {
                                if (Main.VODateSledProverkiTip.Text == "дата")
                                    Stroka.ДатаСледующейПроверки = DateTime.Parse(Main.VODateSledProverkiDate.Text).Date;
                                else Stroka.ДатаСледующейПроверки = Stroka.ДатаПроверкиТехническогоСостояния.HasValue ? Stroka.ДатаПроверкиТехническогоСостояния.Value.AddMonths(Convert.ToInt32(Main.VODateSledProverkiKol.Text)).Date : Stroka.ДатаСледующейПроверки;
                            }
                            else Stroka.ДатаСледующейПроверки = null;
                            //Stroka.ДатаПроверкиТехническогоСостояния = Main.VODateProverkiTehnicheskogoSostoiania.Checked ? DateTime.Parse(Main.VODateProverkiTehnicheskogoSostoiania.Text).Date : Stroka.ДатаПроверкиТехническогоСостояния;
                            //Stroka.ДатаСледующейПроверки = Main.VODateSledProverkiTip.Text == "дата" ? Main.VODateSledProverkiDate.Checked ? DateTime.Parse(Main.VODateSledProverkiDate.Text).Date : Stroka.ДатаСледующейПроверки : Stroka.ДатаПроверкиТехническогоСостояния.HasValue ? Stroka.ДатаПроверкиТехническогоСостояния.Value.AddMonths(Convert.ToInt32(Main.VODateSledProverkiKol.Text)).Date : Stroka.ДатаСледующейПроверки;                            
                            if (Main.VODataPoslTOTip.Text != "нет")
                                Stroka.ПровДатаПослТО = DateTime.Parse(Main.VODataPoslTO.Text).Date;
                            else Stroka.ПровДатаПослТО = null;
                            if (Main.VOSledProvTOTip.Text != "нет")
                            {
                                if (Main.VOSledProvTOTip.Text == "дата")
                                    Stroka.ДатаСледПроведенияТО = DateTime.Parse(Main.VOSledProvTODate.Text).Date;
                                else
                                    Stroka.ДатаСледПроведенияТО = Stroka.ПровДатаПослТО.HasValue ? Stroka.ПровДатаПослТО.Value.AddMonths(Convert.ToInt32(Main.VOSledProvTOKol.Text)).Date : Stroka.ДатаСледПроведенияТО;
                            }
                            else Stroka.ДатаСледПроведенияТО = null;
                            //Stroka.ПровДатаПослТО = Main.VODataPoslTO.Checked ? DateTime.Parse(Main.VODataPoslTO.Text).Date : Stroka.ПровДатаПослТО;
                            //Stroka.ДатаСледПроведенияТО = Main.VOSledProvTOTip.Text == "дата" ? Main.VOSledProvTODate.Checked ? DateTime.Parse(Main.VOSledProvTODate.Text).Date : Stroka.ДатаСледПроведенияТО : Stroka.ПровДатаПослТО.HasValue ? Stroka.ПровДатаПослТО.Value.AddMonths(Convert.ToInt32(Main.VOSledProvTOKol.Text)).Date : Stroka.ДатаСледПроведенияТО ;                            
                            Stroka.Примечание = Main.VOPrimechanie.Text;
                            Stroka.VisibleStatus = Stroka.VisibleStatus;
                            Stroka.ДатаСозданияЗаписи = DateTime.Now;
                            Stroka.UserName = UserSettings.User;
                            Stroka.Owner = null;
                            HistoryVO(Stroka, 200);
                            db.SubmitChanges();
                            GetVO(Main);
                            int rowHandle = Main.VOGridView.LocateByValue("Id", int.Parse(Main.VOID.Text));
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                Main.VOGridView.FocusedRowHandle = rowHandle;
                        }
                    }
                    else
                    {
                        //Добавление                                            
                        var NextID = db.VO != null && db.VO.Count() > 0 ? db.VO.Max(c => c.Id) + 1 : 1;
                        DateTime? ДатаПроверкиТехническогоСостояния = null;
                        if (Main.VODateProverkiTehnicheskogoSostoianiaTip.Text != "нет") ДатаПроверкиТехническогоСостояния = DateTime.Parse(Main.VODateProverkiTehnicheskogoSostoiania.Text).Date;
                        DateTime? ДатаСледующейПроверки = null;
                        if (Main.VODateSledProverkiTip.Text != "нет")
                        {
                            if (Main.VODateSledProverkiTip.Text == "дата")
                                ДатаСледующейПроверки = DateTime.Parse(Main.VODateSledProverkiDate.Text).Date;
                            else
                                if (ДатаПроверкиТехническогоСостояния.HasValue) ДатаСледующейПроверки = Main.VODateProverkiTehnicheskogoSostoiania.Value.AddMonths(Convert.ToInt32(Main.VODateSledProverkiKol.Text)).Date;
                        }
                        DateTime? ПровДатаПослТО = null;
                        if (Main.VODataPoslTOTip.Text != "нет") ПровДатаПослТО = DateTime.Parse(Main.VODataPoslTO.Text);
                        DateTime? ДатаСледПроведенияТО = null;
                        if (Main.VOSledProvTOTip.Text != "нет")
                        {
                            if (Main.VOSledProvTOTip.Text == "дата")
                                ДатаСледПроведенияТО = DateTime.Parse(Main.VOSledProvTODate.Text).Date;
                            else
                                if (ПровДатаПослТО.HasValue) ДатаСледПроведенияТО = Main.VODataPoslTO.Value.AddMonths(Convert.ToInt32(Main.VOSledProvTOKol.Text)).Date;
                        }
                        int NINT = 0;
                        try
                        {
                            var IntN = Main.VON.Text.Split('.');
                            NINT = Convert.ToInt32(IntN[0]);
                        }
                        catch { }
                        db.VO.InsertOnSubmit(new VO
                        {
                            Id = NextID,
                            Nint = NINT,
                            N = Main.VON.Text,
                            Наименование = Main.VOName.Text,
                            ТипМарка = Main.VOTipMarka.Text,
                            ЗаводскойНомер = Main.VOZavodNomer.Text,
                            Изготовитель = Main.VOIzgotovitel.Text,
                            ГодВыпуска = Main.VOGodVypuska.Text,
                            ГодВводаВЭксплуатациюИнвентарныйНомер = Main.VOGodVvodaVEkspltatechiu.Text,
                            ИнвентарныйНомер = Main.VOInventarNomer.Text,
                            Назначение = Main.VONaznachenie.Text,
                            ПравоСобственности = Main.VOPravoSobstvennosti.Text,
                            МестоУстановкиИлиХранения = Main.VOMestoUstanovkiIHranenia.Text,
                            ДатаПроверкиТехническогоСостояния = ДатаПроверкиТехническогоСостояния,
                            ДатаСледующейПроверки = ДатаСледующейПроверки,
                            ПровДатаПослТО = ПровДатаПослТО,
                            ДатаСледПроведенияТО = ДатаСледПроведенияТО,
                            Примечание = Main.VOPrimechanie.Text,
                            VisibleStatus = 0,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = null
                        });
                        db.VO.InsertOnSubmit(new VO
                        {
                            Id = NextID + 1,
                            Nint = NINT,
                            N = Main.VON.Text,
                            Наименование = Main.VOName.Text,
                            ТипМарка = Main.VOTipMarka.Text,
                            ЗаводскойНомер = Main.VOZavodNomer.Text,
                            Изготовитель = Main.VOIzgotovitel.Text,
                            ГодВыпуска = Main.VOGodVypuska.Text,
                            ГодВводаВЭксплуатациюИнвентарныйНомер = Main.VOGodVvodaVEkspltatechiu.Text,
                            ИнвентарныйНомер = Main.VOInventarNomer.Text,
                            Назначение = Main.VONaznachenie.Text,
                            ПравоСобственности = Main.VOPravoSobstvennosti.Text,
                            МестоУстановкиИлиХранения = Main.VOMestoUstanovkiIHranenia.Text,
                            ДатаПроверкиТехническогоСостояния = ДатаПроверкиТехническогоСостояния,
                            ДатаСледующейПроверки = ДатаСледующейПроверки,
                            ПровДатаПослТО = ПровДатаПослТО,
                            ДатаСледПроведенияТО = ДатаСледПроведенияТО,
                            Примечание = Main.VOPrimechanie.Text,
                            VisibleStatus = 100,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = NextID
                        });
                        db.SubmitChanges();
                        NewVOButton(Main);
                        GetVO(Main);
                        ActivateEditVO(false, Main);
                        Main.VOSaveButton.Enabled = false;
                    }
                    NewVO = false;
                    CopyVO = false;
                    db.Dispose();
                    Main.VOEditCheckBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void HistoryVO(VO stroka, int Kod)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.VO.InsertOnSubmit(new VO
                    {
                        Id = db.VO != null && db.VO.Count() > 0 ? db.VO.Max(c => c.Id) + 1 : 1,
                        N = stroka.N,
                        Nint = stroka.Nint,
                        Наименование = stroka.Наименование,
                        ТипМарка = stroka.ТипМарка,
                        ЗаводскойНомер = stroka.ЗаводскойНомер,
                        Изготовитель = stroka.Изготовитель,
                        ГодВыпуска = stroka.ГодВыпуска,
                        ГодВводаВЭксплуатациюИнвентарныйНомер = stroka.ГодВводаВЭксплуатациюИнвентарныйНомер,
                        ИнвентарныйНомер = stroka.ИнвентарныйНомер,
                        Назначение = stroka.Назначение,
                        МестоУстановкиИлиХранения = stroka.МестоУстановкиИлиХранения,
                        ДатаПроверкиТехническогоСостояния = stroka.ДатаПроверкиТехническогоСостояния,
                        ДатаСледующейПроверки = stroka.ДатаСледующейПроверки,
                        ПровДатаПослТО = stroka.ПровДатаПослТО,
                        ДатаСледПроведенияТО = stroka.ДатаСледПроведенияТО,
                        Примечание = stroka.Примечание,
                        VisibleStatus = Kod,
                        ДатаСозданияЗаписи = DateTime.Now,
                        UserName = UserSettings.User,
                        Owner = stroka.Id
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void NewVOButton(MainForm Main)
        {
            try
            {
                if (NewVO)
                {
                    NewVO = false;
                    Main.VONewButton.Text = "Добавить";
                    Main.VONewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.VOSaveButton.Enabled = false;
                }
                else
                {
                    NewVO = true;
                    ClearControlsVO(Main);
                    Main.VONewButton.Text = "Отменить";
                    Main.VONewButton.Image = global::LIMS.Properties.Resources.cancel_16x16;
                    Main.VOSaveButton.Enabled = true;
                }
                ActivateEditVO(Main.VONewButton.Text == "Отменить", Main);
                Main.VOEditCheckBox.Enabled = Main.VONewButton.Text != "Отменить";
                Main.VOEditCheckBox.Checked = false;
                Main.VOSledProvTOTip.Text = "дата";
                Main.VODateSledProverkiTip.Text = "дата";
                Main.VODateProverkiTehnicheskogoSostoianiaTip.Text = "дата";
                Main.VODataPoslTOTip.Text = "дата";
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void VOGridClick(MainForm Main)
        {
            try
            {
                if (Main.VOGridView.DataRowCount > 0)
                {
                    DevExpress.XtraGrid.Views.Grid.GridView gv = Main.VOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    var Stroke = (VO)gv.GetRow(gv.FocusedRowHandle);
                    if (Stroke == null) return;
                    SelectedRowId = Stroke.Id.ToString();
                    Main.VOID.Text = Stroke.Id.ToString();
                    Main.VON.Text = Stroke.N.ToString();
                    Main.VOName.Text = Stroke.Наименование;
                    Main.VOTipMarka.Text = Stroke.ТипМарка;
                    Main.VOZavodNomer.Text = Stroke.ЗаводскойНомер;
                    Main.VOIzgotovitel.Text = Stroke.Изготовитель;
                    Main.VOGodVypuska.Text = Stroke.ГодВыпуска;
                    Main.VOGodVvodaVEkspltatechiu.Text = Stroke.ГодВводаВЭксплуатациюИнвентарныйНомер;
                    Main.VOInventarNomer.Text = Stroke.ИнвентарныйНомер;
                    Main.VONaznachenie.Text = Stroke.Назначение;
                    Main.VOPravoSobstvennosti.Text = Stroke.ПравоСобственности;
                    Main.VOMestoUstanovkiIHranenia.Text = Stroke.МестоУстановкиИлиХранения;
                    Main.VODateSledProverkiDate.Text = Stroke.ДатаСледующейПроверки.ToString();
                    Main.VODateProverkiTehnicheskogoSostoiania.Text = Stroke.ДатаПроверкиТехническогоСостояния.ToString();
                    Main.VODataPoslTO.Text = Stroke.ПровДатаПослТО.ToString();
                    Main.VOSledProvTODate.Text = Stroke.ДатаСледПроведенияТО.ToString();
                    Main.VOPrimechanie.Text = Stroke.Примечание;
                    ActivateEditVO(false, Main);
                    Main.VOSledProvTOTip.Text = Stroke.ДатаСледПроведенияТО.HasValue ? "дата" : "нет";
                    Main.VODateSledProverkiTip.Text = Stroke.ДатаСледующейПроверки.HasValue ? "дата" : "нет";
                    Main.VODateProverkiTehnicheskogoSostoianiaTip.Text = Stroke.ДатаПроверкиТехническогоСостояния.HasValue ? "дата" : "нет";
                    Main.VODataPoslTOTip.Text = Stroke.ПровДатаПослТО.HasValue ? "дата" : "нет";
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void CheckColumnButton_Click(MainForm Main)
        {
            try
            {
                CheckVOColumnForm f = new CheckVOColumnForm(Main, this);
                f.Owner = Main;
                f.ShowDialog();
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        internal void VODateSledProverkiTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.VODateSledProverkiTip.Text == "дата")
            {
                Main.VODateSledProverkiDate.Visible = true;
                Main.VODateSledProverkiKol.Visible = false;
            }
            else if (Main.VODateSledProverkiTip.Text == "нет")
            {
                Main.VODateSledProverkiDate.Visible = false;
                Main.VODateSledProverkiKol.Visible = false;
            }
            else
            {
                Main.VODateSledProverkiDate.Visible = false;
                Main.VODateSledProverkiKol.Visible = true;
            }
        }

        internal void VOSledProvTOTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.VOSledProvTOTip.Text == "дата")
            {
                Main.VOSledProvTODate.Visible = true;
                Main.VOSledProvTOKol.Visible = false;
            }
            else if (Main.VOSledProvTOTip.Text == "нет")
            {
                Main.VOSledProvTODate.Visible = false;
                Main.VOSledProvTOKol.Visible = false;
            }
            else
            {
                Main.VOSledProvTODate.Visible = false;
                Main.VOSledProvTOKol.Visible = true;
                Main.VOSledProvTOKol.Value = 12;
            }
        }

        internal void VOGridView_ColumnWidthChanged(MainForm MF)
        {
            try
            {
                if (ChangeWidth)
                {
                    UserSettings.VOColumnWidth = "";
                    for (int i = 1; i < 17; i++)
                    {
                        if (UserSettings.VOColumnWidth != "")
                            UserSettings.VOColumnWidth = UserSettings.VOColumnWidth + ",";
                        UserSettings.VOColumnWidth = UserSettings.VOColumnWidth + MF.VOGridView.Columns[i].Width.ToString();
                    }
                    UserSettings.SaveSettings();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void AutoWidthColumn(MainForm MF)
        {
            if (UserSettings.VOColumnAutoWidth)
            {
                UserSettings.VOColumnAutoWidth = false;
            }
            else UserSettings.VOColumnAutoWidth = true;
            GetVO(MF);
            UserSettings.SaveSettings();
        }

        internal void VOPrint(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc1 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Вспомогательное оборудование.dotx");
                    var RowCount = MF.VOGridView.RowCount;
                    if (RowCount > 1) WordDoc1.AddTableCell(2, RowCount - 1);
                    int Cell = 1;
                    for (int rowHandle = 0; rowHandle < RowCount; rowHandle++)
                    {
                        Cell++;
                        if (MF.VOGridView.IsDataRow(rowHandle) || Cell >= 39)
                        {
                            try
                            {
                                var SelectedRow = (VO)MF.VOGridView.GetRow(rowHandle);
                                WordDoc1.AddTableValue(2, Cell, 1, SelectedRow.N.ToString());
                                WordDoc1.AddTableValue(2, Cell, 2, SelectedRow.Наименование);
                                WordDoc1.AddTableValue(2, Cell, 3, SelectedRow.ТипМарка);
                                WordDoc1.AddTableValue(2, Cell, 4, SelectedRow.ЗаводскойНомер);
                                WordDoc1.AddTableValue(2, Cell, 5, SelectedRow.Изготовитель);
                                WordDoc1.AddTableValue(2, Cell, 6, SelectedRow.ГодВыпуска);
                                WordDoc1.AddTableValue(2, Cell, 7, SelectedRow.ГодВводаВЭксплуатациюИнвентарныйНомер);
                                WordDoc1.AddTableValue(2, Cell, 8, SelectedRow.ИнвентарныйНомер);
                                WordDoc1.AddTableValue(2, Cell, 9, SelectedRow.Назначение);
                                WordDoc1.AddTableValue(2, Cell, 10, SelectedRow.ПравоСобственности);
                                WordDoc1.AddTableValue(2, Cell, 11, SelectedRow.МестоУстановкиИлиХранения);
                                WordDoc1.AddTableValue(2, Cell, 12, SelectedRow.ДатаПроверкиТехническогоСостояния.HasValue ? SelectedRow.ДатаПроверкиТехническогоСостояния.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 13, SelectedRow.ДатаСледующейПроверки.HasValue ? SelectedRow.ДатаСледующейПроверки.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 14, SelectedRow.ПровДатаПослТО.HasValue ? SelectedRow.ПровДатаПослТО.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 15, SelectedRow.ДатаСледПроведенияТО.HasValue ? SelectedRow.ДатаСледПроведенияТО.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 16, SelectedRow.Примечание);
                            }
                            catch { }
                        }
                    }
                    if (UserSettings.VOColumns != null)
                    {
                        var Visibiliti = UserSettings.VOColumns.Split(',');
                        for (int i = Visibiliti.Length - 1; i > 0; i--)
                        {
                            if (Visibiliti[i] != "1")
                                WordDoc1.DeleteTableColumn(2, i + 1);
                        }
                    }
                    WordDoc1.TableResize(2);
                    StringBuilder rezult = new StringBuilder();
                    var result = Пропись((uint)(Cell - 1), new КлассБезТысяч(), rezult);
                    WordDoc1.ReplaceString("{КолвоДокументов}", (Cell - 1).ToString() + "(" + rezult + ")");
                    WordDoc1.ReplaceString("{Дата}", DateTime.Now.ToShortDateString() + "г.");
                    WordDoc1.Visible = true;
                    WordDoc1.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc1.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void VOReturnFromArhivButton_Click(int id, MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.VO.Where(c => c.Id == id).FirstOrDefault();
                    Stroka.VisibleStatus = 0;
                    Stroka.ДатаСозданияЗаписи = DateTime.Now;
                    Stroka.UserName = UserSettings.User;
                    HistoryVO(Stroka, 400);
                    db.SubmitChanges();
                    db.Dispose();
                    if (!real)
                    {
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.VOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        if (gv.FocusedRowHandle > 0)
                        {
                            var Stroke = (VO)gv.GetRow(gv.FocusedRowHandle - 1);
                            if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                        }
                    }
                    GetVO(Main);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void VOArhivButton_Click(int id, MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.VO.Where(c => c.Id == id).FirstOrDefault();
                    Stroka.VisibleStatus = 1;
                    Stroka.ДатаСозданияЗаписи = DateTime.Now;
                    Stroka.UserName = UserSettings.User;
                    HistoryVO(Stroka, 401);
                    db.SubmitChanges();
                    db.Dispose();
                    if (!arhiv)
                    {
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.VOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        if (gv.FocusedRowHandle > 0)
                        {
                            var Stroke = (VO)gv.GetRow(gv.FocusedRowHandle - 1);
                            if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                        }
                    }
                    GetVO(Main);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void VOArhivButton_Click(MainForm Main)
        {
            if (Main.VOArhivButton.ContextMenuStrip != null)
            {
                Main.VOArhivButton.ContextMenuStrip.Close();
                Main.VOArhivButton.ContextMenuStrip = null;
                return;
            }
            MF = Main;
            ContextMenuStrip ContextMS = new ContextMenuStrip();
            ContextMS.Items.Add("&Показать рабочее оборудование", null, VORealVisible);
            if (real)
            {
                ((ToolStripMenuItem)ContextMS.Items[0]).Checked = true;
            }
            else
            {
                ((ToolStripMenuItem)ContextMS.Items[0]).Checked = false;
            }
            ContextMS.Items.Add("&Архив", null, VOArhivVisible);
            if (arhiv)
            {
                ((ToolStripMenuItem)ContextMS.Items[1]).Checked = true;
            }
            else
            {
                ((ToolStripMenuItem)ContextMS.Items[1]).Checked = false;
            }
            ContextMS.Items.Add("&Показать изменения", null, VOHistoryVisible);
            if (HistoryOfChange)
            {
                ((ToolStripMenuItem)ContextMS.Items[2]).Checked = true;
            }
            else
            {
                ((ToolStripMenuItem)ContextMS.Items[2]).Checked = false;
            }
            ContextMS.Items.Add("&Закрыть", global::LIMS.Properties.Resources.delete_16x16, ContextMenuStripClose_Click);
            ContextMS.AutoClose = false;
            Main.VOArhivButton.ContextMenuStrip = ContextMS;
            ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);
        }

        private void VOHistoryVisible(object sender, EventArgs e)
        {
            HistoryOfChange = HistoryOfChange ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = HistoryOfChange;
            GetVO(MF);
        }

        public void ContextMenuStripClose_Click(object sender, EventArgs e)
        {
            var TSMI = sender as ToolStripMenuItem;
            var CMS = TSMI.Owner as ContextMenuStrip;
            CMS.AutoClose = true;
            MF.VOArhivButton.ContextMenuStrip = null;
            CMS.Close();
        }

        private void VORealVisible(object sender, EventArgs e)
        {
            real = real ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = real;
            GetVO(MF);
        }

        private void VOArhivVisible(object sender, EventArgs e)
        {
            arhiv = arhiv ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = arhiv;
            GetVO(MF);
        }

        internal void VODateProverkiTehnicheskogoSostoianiaTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.VODateProverkiTehnicheskogoSostoianiaTip.Text == "дата")
            {
                Main.VODateProverkiTehnicheskogoSostoiania.Visible = true;
            }
            else
            {
                Main.VODateProverkiTehnicheskogoSostoiania.Visible = false;
            }
        }

        internal void VODataPoslTOTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.VODataPoslTOTip.Text == "дата")
            {
                Main.VODataPoslTO.Visible = true;
            }
            else
            {
                Main.VODataPoslTO.Visible = false;
            }
        }

        internal void VOCellMergeButton_Click(MainForm Main)
        {
            UserSettings.VOCellMerge = !UserSettings.VOCellMerge;
            Main.VOGridView.OptionsView.AllowCellMerge = UserSettings.VOCellMerge;
            if (UserSettings.VOCellMerge)
                Main.VOCellMergeButton.Image = global::LIMS.Properties.Resources.freezetoprow_32x32;
            else Main.VOCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            UserSettings.SaveSettings();
        }

        internal void splitContainerControl5_Panel1_SizeChanged(object sender)
        {
            try
            {
                var SplitCon = sender as DevExpress.XtraEditors.SplitGroupPanel;
                UserSettings.VOSplitWidth = SplitCon.Width;
                UserSettings.SaveSettings();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void VOPrintGrafickAttestachii(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc2 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\График проверки ВО.dotx");
                    List<VO> ВсеЗначения = new List<VO>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.VO.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null) && c.ДатаСледующейПроверки.HasValue && c.ДатаСледующейПроверки.Value.Year == GlobalStatic.BuferInt).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - VО");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc2.AddTableCell(2, RowCount - 1);
                    int Cell = 2;
                    foreach (var VO in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                var Mount = VO.ДатаСледующейПроверки.Value.Month;
                                var Day = VO.ДатаСледующейПроверки.Value.Day;
                                WordDoc2.AddTableValue(2, Cell, 1, VO.Наименование + ((VO.ТипМарка == "-") ? "" : " " + VO.ТипМарка));
                                WordDoc2.AddTableValue(2, Cell, 2, VO.ИнвентарныйНомер);
                                WordDoc2.AddTableValue(2, Cell, 2 + Mount, Day.ToString());
                                //WordDoc.AddTableValue(2, Cell, 15, IO.Примечание);
                            }
                            catch { }
                        }
                    }
                    WordDoc2.TableResize(2);
                    WordDoc2.ReplaceString("{ГОД}", GlobalStatic.BuferInt.ToString());
                    WordDoc2.Visible = true;
                    WordDoc2.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc2.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void VOPrintOsnachinnost(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc3 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Оснащенность ВО.dotx");
                    List<VO> ВсеЗначения = new List<VO>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.VO.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null)).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - VО");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc3.AddTableCell(1, RowCount - 1);
                    int Cell = 2;
                    foreach (var VO in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                WordDoc3.AddTableValue(1, Cell, 1, (Cell - 2).ToString());
                                WordDoc3.AddTableValue(1, Cell, 2, VO.Наименование + " " + VO.ТипМарка + ((VO.ЗаводскойНомер == "б/н" || VO.ЗаводскойНомер == "-" || VO.ЗаводскойНомер == "") ? "" : " зав. №" + VO.ЗаводскойНомер));
                                WordDoc3.AddTableValue(1, Cell, 3, VO.Изготовитель);
                                WordDoc3.AddTableValue(1, Cell, 4, VO.ГодВводаВЭксплуатациюИнвентарныйНомер);
                                WordDoc3.AddTableValue(1, Cell, 5, VO.Назначение);
                                WordDoc3.AddTableValue(1, Cell, 6, VO.МестоУстановкиИлиХранения);
                                WordDoc3.AddTableValue(1, Cell, 7, VO.ПравоСобственности);
                                WordDoc3.AddTableValue(1, Cell, 8, VO.Примечание);
                                WordDoc3.BoltText(VO.Наименование + " " + VO.ТипМарка);
                            }
                            catch { }
                        }
                    }
                    //WordDoc.TableResize(1);
                    WordDoc3.ReplaceString("{ГОД}", DateTime.Now.Year.ToString());
                    WordDoc3.Visible = true;
                    WordDoc3.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc3.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void VOGridView_StartSorting(MainForm Main)
        {
            try
            {
                var GV = Main.VOGridView;
                if (GV.SortInfo.Count > 0)
                {
                    for (int i = GV.SortInfo.Count - 1; i >= 0; i--)
                    {
                        if (GV.SortInfo[i].Column.FieldName != "Nint")
                        {
                            if (GV.SortInfo[i].Column.FieldName == "N")
                            {
                                GV.ClearSorting();
                                GV.Columns["Nint"].SortOrder = SortOrder;
                                if (SortOrder == DevExpress.Data.ColumnSortOrder.Ascending)
                                    SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
                                else
                                    SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                                //GV.SortInfo[i].Column = SIGridView.Columns[24];
                                //GV.SortInfo[i].Column.SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
                            }
                            else
                                SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        internal void VOPerechen(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc4 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\ПЕРЕЧЕНЬ ВО.dotx");
                    List<VO> ВсеЗначения = new List<VO>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.VO.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null)).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - VO");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc4.AddTableCell(1, RowCount - 1);
                    int Cell = 1;
                    foreach (var VO in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                WordDoc4.AddTableValue(1, Cell, 2, VO.Наименование);
                                WordDoc4.AddTableValue(1, Cell, 3, VO.ТипМарка);
                                WordDoc4.AddTableValue(1, Cell, 4, (VO.ЗаводскойНомер.Contains("б/н") || VO.ЗаводскойНомер == "") ? VO.ЗаводскойНомер : ("№ " + VO.ЗаводскойНомер));
                            }
                            catch { }
                        }
                    }
                    StringBuilder rezult = new StringBuilder();
                    var result = Пропись((uint)(RowCount), new КлассБезТысяч(), rezult);
                    WordDoc4.ReplaceString("{колво}", RowCount.ToString() + " (" + rezult + ")");
                    WordDoc4.ReplaceString("{Дата}", DateTime.Now.ToShortDateString());
                    WordDoc4.TableResize(1);
                    WordDoc4.Visible = true;
                    WordDoc4.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc4.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void VOPerechenDocumentacii(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc5 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\ПЕРЕЧЕНЬ документации на ВО.dotx");
                    List<VO> ВсеЗначения = new List<VO>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.VO.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null)).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - VO");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc5.AddTableCell(1, RowCount - 1);
                    int Cell = 1;
                    foreach (var VO in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                WordDoc5.AddTableValue(1, Cell, 2, VO.Наименование);
                                WordDoc5.AddTableValue(1, Cell, 3, VO.ТипМарка);
                                WordDoc5.AddTableValue(1, Cell, 4, (VO.ЗаводскойНомер.Contains("б/н") || VO.ЗаводскойНомер == "") ? VO.ЗаводскойНомер : ("№ " + VO.ЗаводскойНомер));
                            }
                            catch { }
                        }
                    }
                    StringBuilder rezult = new StringBuilder();
                    var result = Пропись((uint)(RowCount), new КлассБезТысяч(), rezult);
                    WordDoc5.ReplaceString("{колво}", RowCount.ToString() + " (" + rezult + ")");
                    WordDoc5.ReplaceString("{Дата}", DateTime.Now.ToShortDateString());
                    WordDoc5.TableResize(1);
                    WordDoc5.Visible = true;
                    WordDoc5.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc5.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        private string GetPerechenRabot(string Name)
        {
            if (Name.ToUpper().Contains("БАНЯ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности, согласно эксплуатационной документации на баню водяную, исправности электрического шнура, вилки, исправности заземления, правильности подключения к электросети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка:\r\n-поверхностей плат элементов терморегулирования; разъемов плат;\r\n-механическая очистка поверхности нагревательных элементов;\r\n-чистка поверхности оборудования от загрязнений.\r\n4.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("БОМБА"))
            {
                return "1.Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр оборудования:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на Бомбы Рейда;\r\n-проверка целостности резьбовых соединений;\r\n-проверка соответствия конструкции Бомбы Рейда требованиям ГОСТ 1756 - 2000 приложение А.\r\n3.Очистка поверхности оборудования от загрязнений.";
            }
            if (Name.ToUpper().Contains("АКВАДИСТИЛЛЯТОР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка:\r\n-внутренней поверхности испарителя, электронагревателей, поплавка датчика,-верхней части конденсатора через люк, сняв маскировочную крышку и крышку люка.\r\n4.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("УСТРОЙСТВО ДЛЯ СУШКИ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, держателя предохранителя, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Проверка наличия теплового потока с воздуховодов.\r\n4 Очистка:\r\n-поверхностей плат элементов терморегулирования; разъемов плат;\r\n-управления нагрева и работы вентилятора\r\n-внутренней и внешней поверхности от пыли.\r\n5.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("ВОДОЛЕЙ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, отсутствие перегибов трубок, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Замена сменной кассеты фильтра(при необходимости).\r\n4.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("ЭКСТРАКТОР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка поверхности экстрактора.\r\n4.Смазка подшипника, замена резиновой муфты(при необходимости).\r\n5.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("КОЛБОНАГРЕВАТЕЛЬ") && (Name.ToUpper().Contains("ЛАБ") || Name.ToUpper().Contains("LAB")))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка:\r\n-поверхностей плат элементов терморегулирования, разъемов плат, клавиш, регулятора нагрева.\r\n- поверхности корпусов - стаканов и нагревательного элемента.\r\n4.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("КОЛБОНАГРЕВАТЕЛЬ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка:\r\n-поверхностей плат элементов терморегулирования, разъемов плат, регулятора нагрева;\r\n-поверхности нагревательного элемента.\r\n4.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("ХОЛОДИЛЬНИК ДЛЯ ХИМИКАТОВ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка внешней и внутренней поверхности холодильника.\r\n4.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("ВАКУУМНАСОС"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка системы, мембран и вентиля.\r\n4.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации";
            }
            if (Name.ToUpper().Contains("ЭЛЕКТРОПЛИТКА"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка:\r\n-разъемов плат элементов терморегулирования и рабочей поверхности нагревательного элемента.\r\n4.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("ФИЛЬТРОВАНИЯ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат,\r\n-целостности корпуса, отсутствие перегибов трубок\r\n- проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка внешней и внутренней поверхности бани.";
            }
            if (Name.ToUpper().Contains("СМЕСИТЕЛЬНАЯ СИСТЕМА"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка поверхности оборудования.\r\n4.Проверка герметичности\r\n- резьбовых соединений;\r\n-шлангов.\r\n5.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("КОМПРЕССОР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка:\r\n-впускного и нагнетательного клапана;\r\n-фильтрующего элемента воздушного фильтра\r\n- охлаждающих ребер.\r\n4.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации";
            }
            if (Name.ToUpper().Contains("ПРОБООТБОРНИК") && Name.ToUpper().Contains("СКЛЯНКА"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр оборудования:\r\n-визуальная проверка целостности корпуса, крышки;\r\n-проверка целостности резьбовых соединений.\r\n3.Очистка поверхности оборудования от загрязнений.";
            }
            if (Name.ToUpper().Contains("ПРОБООТБОРНИК"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр оборудования:\r\n-визуальная проверка комплектности согласно эксплуатационной документации;\r\n-проверка целостности резьбовых соединений.\r\n3.Очистка поверхности оборудования от загрязнений.";
            }
            if (Name.ToUpper().Contains("СУШ")) //СУШКИ ЛАБОРАТОРНОЙ ПОСУДЫ
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, держателя предохранителя, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Проверка наличия теплового потока с воздуховодов.\r\n4 Очистка:\r\n-поверхностей плат элементов терморегулирования; разъемов плат;\r\n-управления нагрева и работы вентилятора\r\n-внутренней и внешней поверхности от пыли.\r\n5.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            return String.Empty;
        }

        private string GetDolgnost(string Name)
        {           
            
                return "";
            
        }

        internal void VOGrafikTO(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc6 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\График ТО ВО.dotx");
                    List<VO> ВсеЗначения = new List<VO>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.VO.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null) && c.ДатаСледПроведенияТО.HasValue && c.ДатаСледПроведенияТО.Value.Year == GlobalStatic.BuferInt).OrderBy(c => c.Наименование).ThenBy(c => c.ТипМарка).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - VO");
                        }
                        db.Dispose();
                    }

                    int Cell = 1;
                    string Name = "";
                    int StartName = 2;
                    string Data = "";
                    int StartData = Cell + 1;
                    string Perechen = "";
                    int StartPerechen = Cell + 1;
                    string Dolgnost = "";
                    foreach (var VO in ВсеЗначения)
                    {
                        try
                        {
                            if (Name != (VO.Наименование + " " + VO.ТипМарка) || Data != (VO.ДатаСледПроведенияТО.HasValue ? VO.ДатаСледПроведенияТО.Value.ToShortDateString() : ""))
                            {
                                Cell++;
                                if (Cell > 2) WordDoc6.AddTableCell(1, 1);
                                WordDoc6.AddTableValue(1, Cell, 2, VO.Наименование + " " + VO.ТипМарка);
                                Name = VO.Наименование + " " + VO.ТипМарка;

                                WordDoc6.AddTableValue(1, Cell, 3, (VO.ЗаводскойНомер));
                                WordDoc6.AddTableValue(1, Cell, 4, VO.ДатаСледПроведенияТО.HasValue ? VO.ДатаСледПроведенияТО.Value.ToShortDateString() : "");
                                WordDoc6.AddTableValue(1, Cell, 5, GetPerechenRabot(VO.Наименование + " " + VO.ТипМарка));
                                WordDoc6.AddTableValue(1, Cell, 6, GetDolgnost(VO.Наименование + " " + VO.ТипМарка));
                            }
                            else
                            {
                                if (!WordDoc6.TableValue(1, Cell, 3).Contains(VO.ЗаводскойНомер))
                                    WordDoc6.AddTableValue(1, Cell, 3, (WordDoc6.TableValue(1, Cell, 3) + VO.ЗаводскойНомер));
                                if (!WordDoc6.TableValue(1, Cell, 4).Contains(VO.ДатаСледПроведенияТО.HasValue ? VO.ДатаСледПроведенияТО.Value.ToShortDateString() : ""))
                                    WordDoc6.AddTableValue(1, Cell, 4, (WordDoc6.TableValue(1, Cell, 4) + (VO.ДатаСледПроведенияТО.HasValue ? VO.ДатаСледПроведенияТО.Value.ToShortDateString() : "")));
                                WordDoc6.AddTableValue(1, Cell, 5, GetPerechenRabot(VO.Наименование + " " + VO.ТипМарка));
                                WordDoc6.AddTableValue(1, Cell, 6, GetDolgnost(VO.Наименование + " " + VO.ТипМарка));
                            }
                            Data = VO.ДатаСледПроведенияТО.HasValue ? VO.ДатаСледПроведенияТО.Value.ToShortDateString() : "";
                        }
                        catch { }
                    }

                    Name = "";
                    StartData = 2;
                    Perechen = "";
                    for (int i = 2; i < Cell + 1; i++)
                    {
                        if (Name != WordDoc6.TableValue(1, i, 2))
                        {
                            if (StartName < i - 1)
                            {
                                WordDoc6.MergeCells(1, StartName, i - 1, 2);
                            }
                            StartName = i;
                            Name = WordDoc6.TableValue(1, i, 2);
                        }
                        else
                        {
                            WordDoc6.AddTableValue(1, i, 2, "");
                        }

                        if (Perechen != WordDoc6.TableValue(1, i, 5))
                        {
                            if (StartPerechen < i - 1)
                            {
                                WordDoc6.MergeCells(1, StartPerechen, i - 1, 5);
                                WordDoc6.MergeCells(1, StartPerechen, i - 1, 6);
                            }
                            if (StartData < i - 1)
                                WordDoc6.MergeCells(1, StartData, i - 1, 4);

                            Data = WordDoc6.TableValue(1, i, 4);
                            Perechen = WordDoc6.TableValue(1, i, 5);
                            Dolgnost = WordDoc6.TableValue(1, i, 6);
                            StartData = i;
                            StartPerechen = i;
                        }
                        else
                        {
                            if (Data != WordDoc6.TableValue(1, i, 4))
                            {
                                if (StartData < i - 1)
                                    WordDoc6.MergeCells(1, StartData, i - 1, 4);
                                Data = WordDoc6.TableValue(1, i, 4);
                                StartData = i;
                            }
                            else
                            {
                                WordDoc6.AddTableValue(1, i, 4, "");
                            }

                            WordDoc6.AddTableValue(1, i, 5, "");
                            WordDoc6.AddTableValue(1, i, 6, "");
                        }
                    }

                    if (StartName < Cell - 1)
                        WordDoc6.MergeCells(1, StartName, Cell - 1, 2);
                    if (StartData < Cell - 1)
                        WordDoc6.MergeCells(1, StartData, Cell - 1, 4);
                    if (StartPerechen < Cell - 1)
                    {
                        WordDoc6.MergeCells(1, StartPerechen, Cell - 1, 5);
                        WordDoc6.MergeCells(1, StartPerechen, Cell - 1, 6);
                    }
                    WordDoc6.ReplaceString("{Год}", GlobalStatic.BuferInt.ToString());
                    WordDoc6.TableResize(1);
                    WordDoc6.Visible = true;
                    WordDoc6.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc6.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }
    }
}
