﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using static LIMS.NumberToString.Число;

namespace LIMS
{
    public class ReactivTab
    {
        public bool NewReactiv = false;
        public bool CopyReactiv = false;
        List<int> orderList = new List<int>();
        public bool ChangeWidth = true;
        WordDocument WordDoc1;        
        MainForm MF;
        bool ReactivVizibleSpisatChecked = false;
        bool ReactivVizibleNullChecked = false;
        bool ReactivVisibleAllChecked = true;
        string SelectedRowId = "";
        DevExpress.Data.ColumnSortOrder SortOrder = DevExpress.Data.ColumnSortOrder.Descending;

        public void GetReactiv(MainForm Main)
        {
            try
            {
                new Thread(GetReactivThread).Start(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void GetReactivThread(object MainF)
        {
            try
            {
                var Main = MainF as MainForm;
                MF = Main;
                using (DbDataContext db = new DbDataContext())
                {
                    List<Reactiv> ВсеЗначения = new List<Reactiv>();
                    try
                    {
                        if (!UserSettings.Developer)
                        {
                            if (ReactivVisibleAllChecked)
                            {
                                if (ReactivVizibleSpisatChecked && ReactivVizibleNullChecked)
                                {
                                    ВсеЗначения = db.Reactiv.Where(c => c.VisibleStatus == null || c.VisibleStatus < 2).OrderBy(c => c.N).ToList();
                                }
                                else
                                {
                                    if (ReactivVizibleSpisatChecked && !ReactivVizibleNullChecked)
                                    {
                                        ВсеЗначения = db.Reactiv.Where(c => (c.Остаток != null && c.Остаток != 0) && (c.VisibleStatus == null || c.VisibleStatus < 2)).OrderBy(c => c.N).ToList();
                                    }
                                    else
                                    {
                                        if (!ReactivVizibleSpisatChecked && ReactivVizibleNullChecked)
                                        {
                                            ВсеЗначения = db.Reactiv.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0).OrderBy(c => c.N).ToList();
                                        }
                                        else ВсеЗначения = db.Reactiv.Where(c => c.Остаток != null && c.Остаток != 0 && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderBy(c => c.N).ToList();
                                    }
                                }
                            }
                            else
                            {
                                if (ReactivVizibleSpisatChecked && ReactivVizibleNullChecked)
                                {
                                    ВсеЗначения = db.Reactiv.OrderBy(c => c.N).Where(c => (c.Остаток == null || c.Остаток == 0) && c.VisibleStatus == 1).ToList();
                                }
                                else
                                {
                                    if (ReactivVizibleSpisatChecked && !ReactivVizibleNullChecked)
                                    {
                                        ВсеЗначения = db.Reactiv.Where(c => c.VisibleStatus == 1 && c.Остаток != null && c.Остаток != 0).OrderBy(c => c.N).ToList();
                                    }
                                    else
                                    {
                                        if (!ReactivVizibleSpisatChecked && ReactivVizibleNullChecked)
                                        {
                                            ВсеЗначения = db.Reactiv.Where(c => (c.Остаток == null || c.Остаток == 0) && (c.VisibleStatus == 1 || c.VisibleStatus == 0 || c.VisibleStatus == null)).OrderBy(c => c.N).ToList();
                                        }
                                        else ВсеЗначения = null;
                                    }
                                }
                            }
                        }
                        else ВсеЗначения = db.Reactiv.OrderBy(c => c.N).ToList();
                    }
                    catch
                    {
                        LogErrors Log = new LogErrors("Ошибка соединения с сервером - Реактив");
                        Main.Invoke(new System.Action(() => MessageBox.Show("Сервер не отвечает!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    }


                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.ReactivGridView.FocusedRowChanged -= Main.ReactivGridView_FocusedRowChanged;
                        Main.ReactivGrid.DataSource = ВсеЗначения;
                        if (!UserSettings.Developer)
                        {
                            Main.ReactivGridView.Columns[0].Visible = false;
                            Main.ReactivGridView.Columns[13].Visible = false;
                            Main.ReactivGridView.Columns[14].Visible = false;
                            Main.ReactivGridView.Columns[15].Visible = false;
                            Main.ReactivGridView.Columns[16].Visible = false;
                            Main.ReactivGridView.Columns[17].Visible = false;
                        }
                    }));

                    if (orderList.Count < 1)
                    {
                        Main.Invoke(new System.Action(() =>
                        {
                            for (int i = 0; i < Main.ReactivGridView.Columns.Count; i++)
                                orderList.Add(Main.ReactivGridView.Columns[i].VisibleIndex);
                        }));
                    }

                    Main.Invoke(new System.Action(() =>
                    {
                        for (int i = 0; i < Main.ReactivGridView.Columns.Count; i++)
                            Main.ReactivGridView.Columns[i].VisibleIndex = orderList[i];
                    }));
                    if (UserSettings.ReactivColumns != null)
                    {
                        var Visibiliti = UserSettings.ReactivColumns.Split(',');
                        for (int i = 0; i < Visibiliti.Length; i++)
                        {
                            Main.Invoke(new System.Action(() => Main.ReactivGridView.Columns[i + 1].Visible = (Visibiliti[i] == "1")));
                        }
                    }

                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.ReactivGridView.FocusedRowChanged += Main.ReactivGridView_FocusedRowChanged;
                        ColumnView View = Main.ReactivGridView;
                        GridColumn column = View.Columns["Id"];
                        if (column != null)
                        {
                            int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                            if (rhFound != GridControl.InvalidRowHandle)
                            {
                                View.FocusedRowHandle = rhFound;
                                View.FocusedColumn = column;
                            }
                        }
                    }));

                    Main.Invoke(new System.Action(() =>
                    {
                        Main.ReactivGridView.Columns[1].Caption = "№ п/п";
                        Main.ReactivGridView.Columns[2].Caption = "Наименование реактива";
                        Main.ReactivGridView.Columns[3].Caption = "Квалификация";
                        Main.ReactivGridView.Columns[4].Caption = "НД на реактив";
                        Main.ReactivGridView.Columns[5].Caption = "Номер партии";
                        Main.ReactivGridView.Columns[6].Caption = "Дата изготовления";
                        Main.ReactivGridView.Columns[7].Caption = "Срок годности";
                        Main.ReactivGridView.Columns[8].Caption = "Дата получения";
                        Main.ReactivGridView.Columns[10].Caption = "Ед. изм.";
                        Main.ReactivGridView.Columns[9].Caption = "Количество (остаток)";
                        Main.ReactivGridView.Columns[11].Caption = "Назначение";
                        Main.ReactivGridView.Columns[12].Caption = "Продление срока годности";
                        Main.ReactivGridView.Columns[6].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.ReactivGridView.Columns[6].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.ReactivGridView.Columns[7].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.ReactivGridView.Columns[7].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.ReactivGridView.Columns[8].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.ReactivGridView.Columns[8].DisplayFormat.FormatString = "dd/MM/yyyy";
                    }));
                    if (UserSettings.ReactivColumnWidth != "" && UserSettings.ReactivColumnWidth != null)
                    {
                        ChangeWidth = false;
                        var ColumnWidth = UserSettings.ReactivColumnWidth.Split(',');
                        for (int i = 1; i < 13; i++)
                        {
                            Main.Invoke(new System.Action(() => Main.ReactivGridView.Columns[i].Width = Convert.ToInt32(ColumnWidth[i - 1])));
                        }
                        ChangeWidth = true;
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        Main.ReactivGridView.OptionsView.RowAutoHeight = true;
                        Main.ReactivGridView.OptionsView.ColumnAutoWidth = UserSettings.ReactivColumnAutoWidth;
                        Main.ReactivGridView.OptionsView.RowAutoHeight = true;
                        Main.ReactivGridView.OptionsView.AllowCellMerge = UserSettings.ReactivCellMerge;
                        //Wrap Text
                        DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
                        MemoEdit.WordWrap = true;

                        foreach (GridColumn My_GridColumn in Main.ReactivGridView.Columns)
                        {
                            My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                            My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            My_GridColumn.ColumnEdit = MemoEdit;
                        }

                        Main.ReactivSaveButton.Enabled = false;
                        Main.ReactivEditCheckBox.Checked = false;
                    }));

                    db.Dispose();
                }

                Main.Invoke(new System.Action(() =>
                {
                    //Main.ReactivGridView.FocusedRowChanged -= Main.ReactivGridView_FocusedRowChanged;
                    ColumnView View = Main.ReactivGridView;
                    GridColumn column = View.Columns["Id"];
                    if (column != null)
                    {
                        int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                        if (rhFound != GridControl.InvalidRowHandle)
                        {
                            View.FocusedRowHandle = rhFound;
                            View.FocusedColumn = column;
                        }
                    }
                    //Main.ReactivGridView.FocusedRowChanged += Main.ReactivGridView_FocusedRowChanged;
                }));
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MF.Invoke(new System.Action(() => MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
            }
        }

        public void ReactivCopyButton_Click(MainForm Main)
        {
            try
            {
                if (Main.ReactivID.Text != "")
                {
                    Main.ReactivEditCheckBox.Enabled = false;
                    NewReactiv = false;
                    Main.ReactivNewButton.Text = "Добавить";
                    Main.ReactivNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    ActivateEditReactiv(true, Main);
                    NewReactiv = false;
                    CopyReactiv = true;
                    Main.ReactivSaveButton.Enabled = true;
                    Main.ReactivEditCheckBox.Checked = false;
                    Main.ReactivKol_vo.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Не выбран элемент!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void ActivateEditReactiv(bool EditableFields, MainForm Main)
        {
            try
            {
                if (EditableFields)
                {
                    foreach (Control c in Main.ReactivPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = false;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = true;
                            DTP.Checked = false;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = true;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.ReadOnly = false;
                            NUD.Enabled = true;
                        }
                    }
                    Main.ReactivSaveButton.Enabled = true;
                }
                else
                {
                    foreach (Control c in Main.ReactivPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = true;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = false;
                            DTP.Checked = false;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = false;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.ReadOnly = true;
                            NUD.Enabled = false;
                        }
                    }
                    NewReactiv = false;
                    Main.ReactivNewButton.Text = "Добавить";
                    Main.ReactivNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.ReactivSaveButton.Enabled = false;
                    Main.ReactivEditCheckBox.Checked = false;
                    Main.ReactivEditCheckBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void ReactivEditCheckBox_CheckedChanged(MainForm Main)
        {
            if (!NewReactiv && Main.ReactivID.Text != "")
            {
                ActivateEditReactiv(Main.ReactivEditCheckBox.Checked, Main);
            }
            else
            {
                Main.ReactivEditCheckBox.Checked = false;
            }
        }

        public void ReactivGrid_Click(MainForm Main)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = Main.ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (Reactiv)gv.GetRow(gv.FocusedRowHandle);            
            if (Stroke == null) return;
            SelectedRowId = Stroke.Id.ToString();
            Main.ReactivID.Text = Stroke.Id.ToString();
            Main.ReactivN.Text = Stroke.N.ToString();
            Main.ReactivName.Text = Stroke.НаименованиеРеактива;
            Main.ReactivClass.Text = Stroke.Классификация;
            Main.ReactivND.Text = Stroke.НДНаРеактив;
            Main.ReactivNPartii.Text = Stroke.НомерПартии;
            Main.ReactivDateIzgot.Text = Stroke.ДатаИзготовления.HasValue ? Stroke.ДатаИзготовления.Value.ToShortDateString() : "";
            Main.ReactivSrokGodnostiDate.Text = Stroke.СрокГодностиDate.HasValue ? Stroke.СрокГодностиDate.Value.ToShortDateString() : "";
            Main.ReactivSrokGodnostiText.Text = Stroke.СрокГодности;
            Main.ReactivDateGet.Text = Stroke.ДатаПолучения.HasValue ? Stroke.ДатаПолучения.Value.ToShortDateString() : "";
            Main.ReactivEdIzm.Text = Stroke.ЕдИзм;
            Main.ReactivKol_vo.Value = Convert.ToDecimal(Stroke.Остаток);
            Main.ReactivNaznachenie.Text = Stroke.Назначение;
            Main.ReactivProdlenieSroka.Text = Stroke.ПродлениеСрока;
            Main.ReactivSrokGodnoctiTip.Text = Stroke.СрокГодностиDate.HasValue ? "дата" : "др.";
            Main.ReactivDateIzgotTip.Text = Stroke.ДатаИзготовления.HasValue ? "дата" : "нет";
            Main.ReactivDateGetTip.Text = Stroke.ДатаПолучения.HasValue ? "дата" : "нет";
            Main.ReactivEditCheckBox.Checked = false;
            ActivateEditReactiv(false, Main);
            Main.ReactivEditCheckBox.Enabled = true;
        }

        public void ReactivDeleteButton_Click(MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {                    
                    //Удаление                
                    if (Main.ReactivID.Text != "")
                    {
                        DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить \"" + Main.ReactivName.Text + "\"?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            var Строка3 = db.Reactiv.Where(c => c.Id == Convert.ToInt32(Main.ReactivID.Text)).FirstOrDefault();
                            if (Строка3 != null)
                            {
                                Строка3.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                Строка3.ДатаСозданияЗаписи = DateTime.Now;
                                Строка3.UserName = UserSettings.User;

                                //Удаление акта проверки пригодности
                                var PP = db.ReactivAktProverkiPrigodnosti.Where(c => c.ReactivID == Строка3.Id).ToList();
                                foreach (var ReactivPrihod in PP)
                                {
                                    ReactivPrihod.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                    ReactivPrihod.UserName = UserSettings.User;
                                    ReactivPrihod.ДатаСозданияЗаписи = DateTime.Now;

                                    var TablePP = db.ReactivAktProverkiPrigodnostiTable.Where(c => c.MainID == ReactivPrihod.Id).ToList();
                                    foreach (var TableStroke in TablePP)
                                    {
                                        TableStroke.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                        TableStroke.UserName = UserSettings.User;
                                        TableStroke.ДатаСозданияЗаписи = DateTime.Now;
                                    }
                                    var Obrazech = db.ReactivAktProverkiPrigodnostiObrazech.Where(c => c.MainID == ReactivPrihod.Id).ToList();
                                    foreach (var ObrazechStroke in Obrazech)
                                    {
                                        ObrazechStroke.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                        ObrazechStroke.UserName = UserSettings.User;
                                        ObrazechStroke.ДатаСозданияЗаписи = DateTime.Now;
                                    }
                                    var Vipolnil = db.ReactivAktProverkiPrigodnostiVipolnil.Where(c => c.MainID == ReactivPrihod.Id).ToList();
                                    foreach (var VipolnilStroke in Vipolnil)
                                    {
                                        VipolnilStroke.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                        VipolnilStroke.UserName = UserSettings.User;
                                        VipolnilStroke.ДатаСозданияЗаписи = DateTime.Now;
                                    }
                                }

                                //Удаление акта входного контроля
                                var Del = db.ReactivAktVhodnogoKontrolya.Where(c => c.ReactivID == Строка3.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                                foreach (var DelP in Del)
                                {
                                    DelP.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                    DelP.UserName = UserSettings.User;
                                    DelP.ДатаСозданияЗаписи = DateTime.Now;

                                    var Table = db.ReactivAktVhodnogoKontrolyaTable.Where(c => c.MainTableId == DelP.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                                    foreach (var TableStroke in Table)
                                    {
                                        TableStroke.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                        TableStroke.UserName = UserSettings.User;
                                        TableStroke.ДатаСозданияЗаписи = DateTime.Now;
                                    }
                                    var Other = db.ReactivAktVhodnogoKontrolyaOther.Where(c => c.MainTableId == DelP.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                                    foreach (var OtherStroke in Other)
                                    {
                                        OtherStroke.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                        OtherStroke.UserName = UserSettings.User;
                                        OtherStroke.ДатаСозданияЗаписи = DateTime.Now;
                                    }
                                    var Komissiya = db.ReactivAktVhodnogoKontrolyaKomissiya.Where(c => c.MainTableId == DelP.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                                    foreach (var KomissiyaStroke in Komissiya)
                                    {
                                        KomissiyaStroke.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                        KomissiyaStroke.UserName = UserSettings.User;
                                        KomissiyaStroke.ДатаСозданияЗаписи = DateTime.Now;
                                    }
                                }

                                //Удаление журнала учета реактива
                                var Uchet = db.ReactivUchet.Where(c => c.ReactivID == Строка3.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                                foreach (var UchetOne in Uchet)
                                {
                                    UchetOne.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                    UchetOne.UserName = UserSettings.User;
                                    UchetOne.ДатаСозданияЗаписи = DateTime.Now;
                                }
                            }
                            db.SubmitChanges();                            
                            DevExpress.XtraGrid.Views.Grid.GridView gv = Main.ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                            if (gv.FocusedRowHandle > 0)
                            {
                               var Stroke = (Reactiv)gv.GetRow(gv.FocusedRowHandle - 1);
                               if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                            }
                            ClearControlsReactiv(Main);
                            GetReactiv(Main);
                            ActivateEditReactiv(false, Main);                            
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не выбрана строка для удаления!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        protected void ClearControlsReactiv(MainForm Main)
        {
            foreach (Control c in Main.ReactivPanel.Controls)
            {
                if (c is TextBox)
                {
                    var TB = c as TextBox;
                    TB.Text = "";
                }
                if (c is DateTimePicker)
                {
                    var DTP = c as DateTimePicker;
                    DTP.Text = "";
                    DTP.Checked = false;
                }
                if (c is ComboBox)
                {
                    var CB = c as ComboBox;
                    CB.Text = "";
                }
                if (c is NumericUpDown)
                {
                    var NUD = c as NumericUpDown;
                    NUD.Text = "";
                }
                Main.ReactivID.Text = "";
            }
        }

        public void SrokGodnoctiTip_SelectedIndexChanged_1(MainForm Main)
        {
            if (Main.ReactivSrokGodnoctiTip.Text == "дата")
            {
                Main.ReactivSrokGodnostiDate.Visible = true;
                Main.ReactivSrokGodnostiMounth.Visible = false;
                Main.ReactivSrokGodnostiText.Visible = false;
            }
            else if (Main.ReactivSrokGodnoctiTip.Text == "др.")
            {
                Main.ReactivSrokGodnostiDate.Visible = false;
                Main.ReactivSrokGodnostiMounth.Visible = false;
                Main.ReactivSrokGodnostiText.Visible = true;
            }
            else
            {
                Main.ReactivSrokGodnostiText.Visible = false;
                Main.ReactivSrokGodnostiDate.Visible = false;
                Main.ReactivSrokGodnostiMounth.Visible = true;
            }
        }


        public void NewReactivButton(MainForm Main)
        {
            try
            {
                if (NewReactiv)
                {
                    NewReactiv = false;
                    Main.ReactivNewButton.Text = "Добавить";
                    Main.ReactivNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.ReactivSaveButton.Enabled = false;
                }
                else
                {
                    NewReactiv = true;
                    ClearControlsReactiv(Main);
                    Main.ReactivNewButton.Text = "Отменить";
                    Main.ReactivNewButton.Image = global::LIMS.Properties.Resources.cancel_16x16;
                    Main.ReactivSaveButton.Enabled = true;
                    Main.ReactivEditCheckBox.Enabled = false;
                    Main.ReactivSrokGodnoctiTip.Text = "дата";
                    Main.ReactivDateGetTip.Text = "дата";
                    Main.ReactivDateIzgotTip.Text = "дата";
                }
                Main.ReactivKol_vo.Value = 0;
                ActivateEditReactiv(Main.ReactivNewButton.Text == "Отменить", Main);
                Main.ReactivEditCheckBox.Checked = false;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void ReactivSaveButton_Click(MainForm Main)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gv = MF.ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;                
                try
                {
                    var t = Convert.ToInt32(Main.ReactivN.Text);
                }
                catch
                {
                    MessageBox.Show("Не корректное значения в поле \"№ п/п\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                using (DbDataContext db = new DbDataContext())
                {
                    //Редактирование
                    if (!NewReactiv && !CopyReactiv)
                    {

                        if (Main.ReactivID.Text != "")
                        {
                            var EditID = Convert.ToInt32(Main.ReactivID.Text);
                            var Stroka = db.Reactiv.Where(c => c.Id == EditID).FirstOrDefault();
                            Stroka.Id = EditID;
                            Stroka.N = Convert.ToInt32(Main.ReactivN.Text);
                            Stroka.НаименованиеРеактива = Main.ReactivName.Text;
                            Stroka.Классификация = Main.ReactivClass.Text;
                            Stroka.НДНаРеактив = Main.ReactivND.Text;
                            Stroka.НомерПартии = Main.ReactivNPartii.Text;
                            if (Main.ReactivDateIzgotTip.Text != "нет")
                                Stroka.ДатаИзготовления = Main.ReactivDateIzgot.Value.Date;
                            else Stroka.ДатаИзготовления = null;
                            if (Main.ReactivSrokGodnoctiTip.Text != "др.")
                            {
                                if (Main.ReactivSrokGodnoctiTip.Text == "дата")
                                {
                                    Stroka.СрокГодностиDate = Main.ReactivSrokGodnostiDate.Value.Date;
                                    Stroka.СрокГодности = Main.ReactivSrokGodnostiDate.Value.ToShortDateString();
                                }
                                else if (Stroka.ДатаИзготовления.HasValue)
                                {
                                    Stroka.СрокГодностиDate = Stroka.ДатаИзготовления.Value.AddMonths(Convert.ToInt32(Main.ReactivSrokGodnostiMounth.Text == "" ? "0" : Main.ReactivSrokGodnostiMounth.Text)).Date;
                                    Stroka.СрокГодности = Stroka.СрокГодностиDate.Value.ToShortDateString();
                                }
                            }
                            else
                            {
                                Stroka.СрокГодностиDate = null;
                                Stroka.СрокГодности = Main.ReactivSrokGodnostiText.Text;
                            }
                            if (Main.ReactivDateGetTip.Text != "нет")
                                Stroka.ДатаПолучения = Main.ReactivDateGet.Value.Date;
                            else
                                Stroka.ДатаПолучения = null;
                            Stroka.ЕдИзм = Main.ReactivEdIzm.Text;
                            Stroka.Остаток = Main.ReactivKol_vo.Value;
                            Stroka.Назначение = Main.ReactivNaznachenie.Text;
                            Stroka.ПродлениеСрока = Main.ReactivProdlenieSroka.Text;
                            //Stroka.VisibleStatus = 0;
                            Stroka.ДатаСозданияЗаписи = DateTime.Now;
                            Stroka.UserName = UserSettings.User;
                            Stroka.Owner = null;
                            HistoryReactiv(Stroka, 200);
                            db.SubmitChanges();
                            GetReactiv(Main);
                            //Выбор стороки таблицы
                            int rowHandle = Main.ReactivGridView.LocateByValue("Id", int.Parse(Main.ReactivID.Text));
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                Main.ReactivGridView.FocusedRowHandle = rowHandle;
                        }
                    }
                    else
                    {
                        //Добавление                        
                        var NextID = db.Reactiv != null && db.Reactiv.Count() > 0 ? db.Reactiv.Max(c => c.Id) + 1 : 1;
                        DateTime? ДатаИзготовления = null;
                        if (Main.ReactivDateIzgotTip.Text != "нет") ДатаИзготовления = Main.ReactivDateIzgot.Value.Date;
                        DateTime? ДатаПолучения = null;
                        if (Main.ReactivDateGetTip.Text != "нет") ДатаПолучения = Main.ReactivDateGet.Value.Date;
                        string SG = null;
                        DateTime? SGDate = null;
                        if (Main.ReactivSrokGodnoctiTip.Text != "нет")
                        {
                            if (Main.ReactivSrokGodnoctiTip.Text == "дата")
                            {
                                SGDate = Main.ReactivSrokGodnostiDate.Value.Date;
                                SG = Main.ReactivSrokGodnostiDate.Value.Date.ToShortDateString();
                            }
                            else
                            {
                                if (ДатаИзготовления.HasValue)
                                {
                                    SGDate = Main.ReactivDateIzgot.Value.AddMonths(Convert.ToInt32(Main.ReactivSrokGodnostiMounth.Text == "" ? "0" : Main.ReactivSrokGodnostiMounth.Text)).Date;
                                    SG = Main.ReactivDateIzgot.Value.AddMonths(Convert.ToInt32(Main.ReactivSrokGodnostiMounth.Text == "" ? "0" : Main.ReactivSrokGodnostiMounth.Text)).Date.ToShortDateString();
                                }
                            }
                        }
                        db.Reactiv.InsertOnSubmit(new Reactiv
                        {
                            Id = NextID,
                            N = Convert.ToInt32(Main.ReactivN.Text),
                            НаименованиеРеактива = Main.ReactivName.Text,
                            Классификация = Main.ReactivClass.Text,
                            НДНаРеактив = Main.ReactivND.Text,
                            НомерПартии = Main.ReactivNPartii.Text,
                            ЕдИзм = Main.ReactivEdIzm.Text,
                            Назначение = Main.ReactivNaznachenie.Text,
                            ДатаИзготовления = ДатаИзготовления,
                            ДатаПолучения = ДатаПолучения,
                            СрокГодности = SG,
                            СрокГодностиDate = SGDate,
                            Остаток = Main.ReactivKol_vo.Value,
                            ПродлениеСрока = Main.ReactivProdlenieSroka.Text,
                            VisibleStatus = 0,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = null
                        });
                        db.Reactiv.InsertOnSubmit(new Reactiv
                        {
                            Id = NextID + 1,
                            N = Convert.ToInt32(Main.ReactivN.Text),
                            НаименованиеРеактива = Main.ReactivName.Text,
                            Классификация = Main.ReactivClass.Text,
                            НДНаРеактив = Main.ReactivND.Text,
                            НомерПартии = Main.ReactivNPartii.Text,
                            ЕдИзм = Main.ReactivEdIzm.Text,
                            Назначение = Main.ReactivNaznachenie.Text,
                            ДатаИзготовления = ДатаИзготовления,
                            ДатаПолучения = ДатаПолучения,
                            СрокГодности = SG,
                            СрокГодностиDate = SGDate,
                            Остаток = Main.ReactivKol_vo.Value,
                            ПродлениеСрока = Main.ReactivProdlenieSroka.Text,
                            VisibleStatus = 100,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = NextID
                        });
                        db.SubmitChanges();
                        NewReactivButton(Main);
                        GetReactiv(Main);
                        ActivateEditReactiv(false, Main);
                        Main.ReactivSaveButton.Enabled = false;

                        var result = MessageBox.Show("Оформить приход и создать акт входного контроля?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            var Strok = db.Reactiv.Where(c => c.Id == NextID).FirstOrDefault();
                            if (Strok != null)
                            {
                                ReactivPrihodForm f = new ReactivPrihodForm(Main, Strok, true);
                                f.Owner = Main;
                                Main.Visible = false;
                                f.ShowDialog();
                                Main.Visible = true;
                                GetReactiv(Main);
                                f.Dispose();
                            }
                        }
                    }
                    NewReactiv = false;
                    CopyReactiv = false;
                    Main.ReactivEditCheckBox.Enabled = true;
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void HistoryReactiv(Reactiv stroka, int Kod)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.Reactiv.InsertOnSubmit(new Reactiv
                    {
                        Id = db.Reactiv != null && db.Reactiv.Count() > 0 ? db.Reactiv.Max(c => c.Id) + 1 : 1,
                        N = stroka.N,
                        НаименованиеРеактива = stroka.НаименованиеРеактива,
                        Классификация = stroka.Классификация,
                        НДНаРеактив = stroka.НДНаРеактив,
                        НомерПартии = stroka.НомерПартии,
                        ЕдИзм = stroka.ЕдИзм,
                        Назначение = stroka.Назначение,
                        ДатаИзготовления = stroka.ДатаИзготовления,
                        ДатаПолучения = stroka.ДатаПолучения,
                        СрокГодности = stroka.СрокГодности,
                        Остаток = stroka.Остаток,
                        ПродлениеСрока = stroka.ПродлениеСрока,
                        VisibleStatus = Kod,
                        ДатаСозданияЗаписи = DateTime.Now,
                        UserName = UserSettings.User,
                        Owner = stroka.Id
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void ReactivGridView_ColumnWidthChanged(MainForm MF)
        {
            try
            {
                if (ChangeWidth)
                {
                    UserSettings.ReactivColumnWidth = "";
                    for (int i = 1; i < 13; i++)
                    {
                        if (UserSettings.ReactivColumnWidth != "")
                            UserSettings.ReactivColumnWidth = UserSettings.ReactivColumnWidth + ",";
                        UserSettings.ReactivColumnWidth = UserSettings.ReactivColumnWidth + MF.ReactivGridView.Columns[i].Width.ToString();
                    }
                    UserSettings.SaveSettings();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CheckColumnButton_Click(MainForm Main)
        {
            try
            {
                CheckReactivColumnForm f = new CheckReactivColumnForm(Main, this);
                f.Owner = Main;
                f.ShowDialog();
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void AutoWidthColumn(MainForm MF)
        {
            if (UserSettings.ReactivColumnAutoWidth)
            {
                UserSettings.ReactivColumnAutoWidth = false;
            }
            else UserSettings.ReactivColumnAutoWidth = true;
            GetReactiv(MF);
            UserSettings.SaveSettings();
        }

        internal void ReactivWiewAktProverkiPrigodnostiClick(MainForm Main)
        {
            try
            {
                ReactivListAktProverkiPrigodnostiForm f = new ReactivListAktProverkiPrigodnostiForm(Main);
                f.Owner = Main;
                Main.Visible = false;
                f.ShowDialog();
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void ReactivWiewAktVhodnogoKontrolaClick(MainForm Main)
        {
            try
            {
                ReactivListAktVhodnogoKontrolaForm f = new ReactivListAktVhodnogoKontrolaForm(Main);
                f.Owner = Main;
                Main.Visible = false;
                f.ShowDialog();
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void ReactivWiewAktVhodnogoKontrolaClick(MainForm Main, string ReactivName)
        {
            try
            {
                ReactivListAktVhodnogoKontrolaForm f = new ReactivListAktVhodnogoKontrolaForm(Main, ReactivName);
                f.Owner = Main;
                Main.Visible = false;
                f.ShowDialog();
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void ReactivWiewAktProverkiPrigodnostiClickOneReactiv(MainForm Main, string ReactivName)
        {
            try
            {
                ReactivListAktProverkiPrigodnostiForm f = new ReactivListAktProverkiPrigodnostiForm(Main, ReactivName);
                f.Owner = Main;
                Main.Visible = false;
                f.ShowDialog();
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void ReactivSpisatButton_Click(MainForm Main, int ID)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    //Списание               
                    if (Main.ReactivID.Text != "")
                    {
                        var Строка3 = db.Reactiv.Where(c => c.Id == ID).FirstOrDefault();
                        if (Строка3.VisibleStatus == null || Строка3.VisibleStatus == 0)
                        {
                            DialogResult dialogResult = MessageBox.Show("Вы действительно хотите списать " + Main.ReactivName.Text + " ?", "Списание", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dialogResult == DialogResult.Yes)
                            {
                                if (Строка3 != null)
                                {
                                    Строка3.VisibleStatus = 1;
                                    Строка3.ДатаСозданияЗаписи = DateTime.Now;
                                    Строка3.UserName = UserSettings.User;
                                    HistoryReactiv(Строка3, 401);
                                    db.SubmitChanges();
                                }
                                if(!ReactivVizibleSpisatChecked)
                                {
                                    DevExpress.XtraGrid.Views.Grid.GridView gv = Main.ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                                    if(gv.FocusedRowHandle > 0)
                                    {
                                        var Stroke = (Reactiv)gv.GetRow(gv.FocusedRowHandle - 1);
                                        if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                                    }  
                                }
                                ClearControlsReactiv(Main);
                                GetReactiv(Main);
                                ActivateEditReactiv(false, Main);                                
                            }
                            else if (dialogResult == DialogResult.No)
                            {
                                return;
                            }
                        }
                        else
                        {
                            //Отмена списания
                            DialogResult dialogResult = MessageBox.Show("Отменить списание " + Main.ReactivName.Text + " ?", "Отмена списания", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dialogResult == DialogResult.Yes)
                            {
                                if (Строка3 != null)
                                {
                                    Строка3.VisibleStatus = null;
                                    Строка3.ДатаСозданияЗаписи = DateTime.Now;
                                    Строка3.UserName = UserSettings.User;
                                    HistoryReactiv(Строка3, 400);
                                    db.SubmitChanges();
                                }
                                if (!ReactivVisibleAllChecked)
                                {
                                    DevExpress.XtraGrid.Views.Grid.GridView gv = Main.ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                                    if (gv.FocusedRowHandle > 0)
                                    {
                                        var Stroke = (Reactiv)gv.GetRow(gv.FocusedRowHandle - 1);
                                        if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                                    }
                                }
                                GetReactiv(Main);
                                ActivateEditReactiv(false, Main);
                                ClearControlsReactiv(Main);
                            }
                            else if (dialogResult == DialogResult.No)
                            {
                                return;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не выбран реактив!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void ReactivRashodButton_Click(MainForm Main, Reactiv Strok)
        {
            try
            {
                ReactivRashodForm f = new ReactivRashodForm(Main, Strok);
                f.Owner = Main;
                f.ShowDialog();
                GetReactiv(Main);
                ActivateEditReactiv(false, Main);
                ClearControlsReactiv(Main);
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void ReactivVisibleSpisatButton_Click(MainForm Main)
        {
            if (Main.ReactivVisibleSpisatButton.ContextMenuStrip != null)
            {
                Main.ReactivVisibleSpisatButton.ContextMenuStrip.Close();
                Main.ReactivVisibleSpisatButton.ContextMenuStrip = null;
                return;
            }
            MF = Main;
            ContextMenuStrip ContextMS = new ContextMenuStrip();
            ContextMS.Items.Add("&Показывать все, кроме списанного и с нулевым остатком", null, ReactivVisibleAllChecked_click);
            ContextMS.Items.Add("&Показывать списанное", null, ReactivVizibleSpisatChecked_Click);
            ContextMS.Items.Add("&Показывать реактивы с нулевым остатком", null, ReactivVizibleNullChecked_Click);
            ContextMS.Items.Add("&Закрыть", global::LIMS.Properties.Resources.delete_16x16, ContextMenuStripClose_Click);
            if (ReactivVisibleAllChecked) ((ToolStripMenuItem)ContextMS.Items[0]).Checked = true;
            else ((ToolStripMenuItem)ContextMS.Items[0]).Checked = false;
            if (ReactivVizibleSpisatChecked) ((ToolStripMenuItem)ContextMS.Items[1]).Checked = true;
            else ((ToolStripMenuItem)ContextMS.Items[1]).Checked = false;
            if (ReactivVizibleNullChecked) ((ToolStripMenuItem)ContextMS.Items[2]).Checked = true;
            else ((ToolStripMenuItem)ContextMS.Items[2]).Checked = false;
            ContextMS.AutoClose = false;
            Main.ReactivVisibleSpisatButton.ContextMenuStrip = ContextMS;
            ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);
        }

        public void ContextMenuStripClose_Click(object sender, EventArgs e)
        {
            var TSMI = sender as ToolStripMenuItem;
            var CMS = TSMI.Owner as ContextMenuStrip;
            CMS.AutoClose = true;
            MF.ReactivVisibleSpisatButton.ContextMenuStrip = null;
            CMS.Close();
        }

        private void ReactivVizibleNullChecked_Click(object sender, EventArgs e)
        {
            ReactivVizibleNullChecked = ReactivVizibleNullChecked ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = ReactivVizibleNullChecked;
            GetReactiv(MF);
        }

        private void ReactivVizibleSpisatChecked_Click(object sender, EventArgs e)
        {
            ReactivVizibleSpisatChecked = ReactivVizibleSpisatChecked ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = ReactivVizibleSpisatChecked;
            GetReactiv(MF);
        }

        private void ReactivVisibleAllChecked_click(object sender, EventArgs e)
        {
            ReactivVisibleAllChecked = ReactivVisibleAllChecked ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = ReactivVisibleAllChecked;
            GetReactiv(MF);
        }

        internal void ReactivPrihodButton_Click(MainForm Main, Reactiv Strok)
        {
            try
            {
                ReactivPrihodForm f = new ReactivPrihodForm(Main, Strok);
                f.Owner = Main;
                Main.Visible = false;
                f.ShowDialog();
                Main.Visible = true;
                GetReactiv(Main);
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void ReactivUchetClick(MainForm Main, int id = -5)
        {
            try
            {
                ReactivUchetReactivovForm f;
                if (id >= 0)
                {
                    f = new ReactivUchetReactivovForm(Main, id);
                }
                else
                {
                    f = new ReactivUchetReactivovForm(Main);
                }
                f.Owner = Main;
                Main.Visible = false;
                f.ShowDialog();
                Main.Visible = true;
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void ReactivDateIzgotTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.ReactivDateIzgotTip.Text == "дата")
                Main.ReactivDateIzgot.Visible = true;
            else Main.ReactivDateIzgot.Visible = false;
        }

        internal void ReactivDateGetTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.ReactivDateGetTip.Text == "дата")
                Main.ReactivDateGet.Visible = true;
            else Main.ReactivDateGet.Visible = false;
        }

        internal void ReactivPrintTread2(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    WordDoc1 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Реактив.dotx");
                    MF = e.Argument as MainForm;
                    var RowCount = MF.ReactivGridView.RowCount;
                    if (RowCount > 1) WordDoc1.AddTableCell(2, RowCount - 1);
                    int Cell = 1;
                    for (int rowHandle = 0; rowHandle < RowCount; rowHandle++)
                    {
                        Cell++;
                        if (MF.ReactivGridView.IsDataRow(rowHandle) || Cell >= 39)
                        {
                            try
                            {
                                var SelectedRow = (Reactiv)MF.ReactivGridView.GetRow(rowHandle);
                                WordDoc1.AddTableValue(2, Cell, 1, SelectedRow.N.ToString());
                                WordDoc1.AddTableValue(2, Cell, 2, SelectedRow.НаименованиеРеактива);
                                WordDoc1.AddTableValue(2, Cell, 3, SelectedRow.Классификация);
                                WordDoc1.AddTableValue(2, Cell, 4, SelectedRow.НДНаРеактив);
                                WordDoc1.AddTableValue(2, Cell, 5, SelectedRow.НомерПартии);
                                WordDoc1.AddTableValue(2, Cell, 6, SelectedRow.ДатаИзготовления.HasValue ? SelectedRow.ДатаИзготовления.Value.ToShortDateString() : string.Empty);
                                WordDoc1.AddTableValue(2, Cell, 7, SelectedRow.СрокГодности);
                                WordDoc1.AddTableValue(2, Cell, 8, SelectedRow.ДатаПолучения.HasValue ? SelectedRow.ДатаПолучения.Value.ToShortDateString() : string.Empty);
                                WordDoc1.AddTableValue(2, Cell, 9, SelectedRow.Остаток.HasValue ? SelectedRow.Остаток.Value.ToString() : string.Empty);
                                WordDoc1.AddTableValue(2, Cell, 10, SelectedRow.ЕдИзм);
                                WordDoc1.AddTableValue(2, Cell, 11, SelectedRow.Назначение);
                                WordDoc1.AddTableValue(2, Cell, 12, SelectedRow.ПродлениеСрока);
                            }
                            catch { }
                        }
                    }
                    if (UserSettings.ReactivColumns != null)
                    {
                        var Visibiliti = UserSettings.ReactivColumns.Split(',');
                        for (int i = Visibiliti.Length - 1; i > 0; i--)
                        {
                            if (Visibiliti[i] != "1")
                                WordDoc1.DeleteTableColumn(2, i + 1);
                        }
                    }
                    WordDoc1.TableResize(2);
                    StringBuilder rezult = new StringBuilder();
                    var result = Пропись((uint)(Cell - 1), new КлассБезТысяч(), rezult);
                    WordDoc1.ReplaceString("{КолвоДокументов}", (Cell - 1).ToString() + "(" + rezult + ")");
                    WordDoc1.ReplaceString("{Дата}", DateTime.Now.ToShortDateString() + "г.");
                    WordDoc1.Visible = true;
                    WordDoc1.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc1.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            } while (REPEAT);
        }

        internal void ReactivCellMergeButton_Click(MainForm Main)
        {
            UserSettings.ReactivCellMerge = !UserSettings.ReactivCellMerge;
            Main.ReactivGridView.OptionsView.AllowCellMerge = UserSettings.ReactivCellMerge;
            if (UserSettings.ReactivCellMerge)
                Main.ReactivCellMergeButton.Image = global::LIMS.Properties.Resources.freezetoprow_32x32;
            else Main.ReactivCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            UserSettings.SaveSettings();
        }

        internal void splitContainerControl6_Panel1_SizeChanged(object sender)
        {
            try
            {
                var SplitCon = sender as DevExpress.XtraEditors.SplitGroupPanel;
                UserSettings.ReactivSplitWidth = SplitCon.Width;
                UserSettings.SaveSettings();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void ReactivGridView_StartSorting(MainForm Main)
        {
            try
            {
                var GV = Main.ReactivGridView;
                if (GV.SortInfo.Count > 0)
                {
                    for (int i = GV.SortInfo.Count - 1; i >= 0; i--)
                    {
                        if (GV.SortInfo[i].Column.FieldName != "СрокГодностиDate")
                        {
                            if (GV.SortInfo[i].Column.FieldName == "СрокГодности")
                            {
                                GV.ClearSorting();
                                GV.Columns["СрокГодностиDate"].SortOrder = SortOrder;
                                if (SortOrder == DevExpress.Data.ColumnSortOrder.Ascending)
                                    SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
                                else
                                    SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                            }
                            else
                                SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}
