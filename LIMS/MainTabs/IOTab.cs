﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using static LIMS.NumberToString.Число;

namespace LIMS
{
    public class IOTab
    {
        public bool NewIO = false;
        public bool CopyIO = false;
        List<int> orderList = new List<int>();
        private bool ChangeWidth = true;
        WordDocument WordDoc1;
        WordDocument WordDoc2;
        WordDocument WordDoc3;
        WordDocument WordDoc4;
        WordDocument WordDoc5;
        WordDocument WordDoc6;
        MainForm MF;
        bool arhiv = false;
        bool real = true;
        internal bool HistoryOfChange = false;
        string SelectedRowId = "";
        DevExpress.Data.ColumnSortOrder SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
        public void GetIO(MainForm Main)
        {
            try
            {
                new Thread(GetIOThread).Start(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void GetIOThread(object MainF)
        {
            try
            {
                var Main = MainF as MainForm;
                MF = Main;
                using (DbDataContext db = new DbDataContext())
                {
                    List<IO> ВсеЗначения = new List<IO>();
                    try
                    {
                        if (!UserSettings.Developer)
                        {
                            if (!arhiv && real) ВсеЗначения = db.IO.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null).OrderBy(c => c.Nint).ToList();
                            else if (arhiv && !real) ВсеЗначения = db.IO.Where(c => c.VisibleStatus == 1).OrderBy(c => c.Nint).ToList();
                            else if (arhiv && real) ВсеЗначения = db.IO.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null || c.VisibleStatus == 1).OrderBy(c => c.Nint).ToList();
                            else ВсеЗначения = null;
                        }
                        else ВсеЗначения = db.IO.OrderBy(c => c.Nint).ToList();
                    }
                    catch
                    {
                        LogErrors Log = new LogErrors("Ошибка соединения с сервером - ИО");
                        Main.Invoke(new System.Action(() => MessageBox.Show("Сервер не отвечает!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    }

                    if (HistoryOfChange) ВсеЗначения = AddHistory(ВсеЗначения);
                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.IOGridView.FocusedRowChanged -= Main.IOGridView_FocusedRowChanged;
                        Main.IOGrid.DataSource = ВсеЗначения;
                        if (!UserSettings.Developer)
                        {
                            Main.IOGridView.Columns[0].Visible = false;
                            Main.IOGridView.Columns[20].Visible = false;
                            Main.IOGridView.Columns[21].Visible = false;
                            Main.IOGridView.Columns[22].Visible = false;
                            Main.IOGridView.Columns[23].Visible = false;
                            Main.IOGridView.Columns[24].Visible = false;
                        }
                    }));
                    if (orderList.Count < 1)
                    {
                        Main.Invoke(new System.Action(() =>
                        {
                            for (int i = 0; i < Main.IOGridView.Columns.Count; i++)
                                orderList.Add(Main.IOGridView.Columns[i].VisibleIndex);
                        }));
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        for (int i = 0; i < Main.IOGridView.Columns.Count; i++)
                            Main.IOGridView.Columns[i].VisibleIndex = orderList[i];
                    }));
                    if (UserSettings.IOColumns != null)
                    {
                        var Visibiliti = UserSettings.IOColumns.Split(',');
                        for (int i = 0; i < Visibiliti.Length; i++)
                        {
                            Main.Invoke(new System.Action(() => Main.IOGridView.Columns[i + 1].Visible = (Visibiliti[i] == "1")));
                        }
                    }

                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.IOGridView.FocusedRowChanged += Main.IOGridView_FocusedRowChanged;
                        ColumnView View = Main.IOGridView;
                        GridColumn column = View.Columns["Id"];
                        if (column != null)
                        {
                            int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                            if (rhFound != GridControl.InvalidRowHandle)
                            {
                                View.FocusedRowHandle = rhFound;
                                View.FocusedColumn = column;
                            }
                        }
                    }));

                    Main.Invoke(new System.Action(() =>
                    {
                        Main.IOGridView.Columns[1].Caption = "№ п/п";
                        Main.IOGridView.Columns[2].Caption = "Наименование определяемых характеристик продукции";
                        Main.IOGridView.Columns[3].Caption = "Наименование ИО";
                        Main.IOGridView.Columns[4].Caption = "Тип, марка";
                        Main.IOGridView.Columns[5].Caption = "Заводской номер";
                        Main.IOGridView.Columns[6].Caption = "Инвентарный номер";
                        Main.IOGridView.Columns[7].Caption = "Изготовитель (предприятие)";
                        Main.IOGridView.Columns[8].Caption = "Год выпуска";
                        Main.IOGridView.Columns[9].Caption = "Основные технические характеристики, рабочие диапазоны";
                        Main.IOGridView.Columns[10].Caption = "Год ввода в эксплуатацию";
                        Main.IOGridView.Columns[11].Caption = "Аттестат №";
                        Main.IOGridView.Columns[12].Caption = "Дата аттестации";
                        Main.IOGridView.Columns[13].Caption = "Периодичность";
                        Main.IOGridView.Columns[14].Caption = "Дата следующей аттестации";
                        Main.IOGridView.Columns[15].Caption = "Право собственности";
                        Main.IOGridView.Columns[16].Caption = "Место установки или хранения";
                        Main.IOGridView.Columns[17].Caption = "Дата последнего ТО";
                        Main.IOGridView.Columns[18].Caption = "Дата следующего ТО";
                        Main.IOGridView.Columns[19].Caption = "Примечание";
                        Main.IOGridView.Columns[12].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.IOGridView.Columns[12].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.IOGridView.Columns[14].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.IOGridView.Columns[14].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.IOGridView.Columns[17].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.IOGridView.Columns[17].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.IOGridView.Columns[18].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.IOGridView.Columns[18].DisplayFormat.FormatString = "dd/MM/yyyy";
                    }));
                    if (UserSettings.IOColumnWidth != "" && UserSettings.IOColumnWidth != null)
                    {
                        ChangeWidth = false;
                        var ColumnWidth = UserSettings.IOColumnWidth.Split(',');
                        for (int i = 1; i < 20; i++)
                        {
                            Main.Invoke(new System.Action(() => Main.IOGridView.Columns[i].Width = Convert.ToInt32(ColumnWidth[i - 1])));
                        }
                        ChangeWidth = true;
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        Main.IOGridView.OptionsView.RowAutoHeight = true;
                        Main.IOGridView.OptionsView.ColumnAutoWidth = UserSettings.IOColumnAutoWidth;
                        //Wrap Text
                        DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
                        MemoEdit.WordWrap = true;
                        Main.IOGridView.OptionsView.RowAutoHeight = true;
                        Main.IOGridView.OptionsView.AllowCellMerge = UserSettings.IOCellMerge;
                        foreach (GridColumn My_GridColumn in Main.IOGridView.Columns)
                        {
                            My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                            My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            My_GridColumn.ColumnEdit = MemoEdit;
                        }
                        Main.IOSaveButton.Enabled = false;
                        Main.IOEditCheckBox.Checked = false;
                        Main.IODateSledAttistachiiDate.Checked = false;
                    }));
                    db.Dispose();
                }

                Main.Invoke(new System.Action(() =>
                {
                    //Main.IOGridView.FocusedRowChanged -= Main.IOGridView_FocusedRowChanged;
                    ColumnView View = Main.IOGridView;
                    GridColumn column = View.Columns["Id"];
                    if (column != null)
                    {
                        int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                        if (rhFound != GridControl.InvalidRowHandle)
                        {
                            View.FocusedRowHandle = rhFound;
                            View.FocusedColumn = column;
                        }
                    }
                    //Main.IOGridView.FocusedRowChanged += Main.IOGridView_FocusedRowChanged;
                }));
            }
            catch (Exception ex)
            {
                ChangeWidth = true;
                LogErrors Log = new LogErrors(ex.ToString());
                MF.Invoke(new System.Action(() => MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
            }
        }

        private List<IO> AddHistory(List<IO> всеЗначения)
        {
            try
            {
                List<IO> NewList = new List<IO>();
                using (DbDataContext db = new DbDataContext())
                {
                    foreach (IO StrokeIO in всеЗначения)
                    {
                        var IOHistory = db.IO.Where(c => c.Owner == StrokeIO.Id && (c.VisibleStatus == 100 || c.VisibleStatus == 200)).OrderBy(c => c.ДатаСозданияЗаписи).ToList();
                        if (IOHistory != null && IOHistory.Count > 0)
                        {
                            var i = 0;
                            foreach (IO OtherIO in IOHistory)
                            {
                                i++;
                                if (i > 1)
                                {
                                    OtherIO.N = "";
                                    OtherIO.НаименованиеВидовИспмтанийИВмдовХарактеристикПродукции = "";
                                    OtherIO.НаименованиеИспытательготоОборудования = "";                                    
                                }
                                NewList.Add(OtherIO);
                            }
                        }
                        else NewList.Add(StrokeIO);
                    }
                    db.Dispose();
                }
                return NewList;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return всеЗначения;
            }
        }

        public void IOCopyButton_Click(MainForm Main)
        {
            try
            {
                if (Main.IOID.Text != "")
                {
                    Main.IOEditCheckBox.Enabled = false;
                    NewIO = false;
                    Main.IONewButton.Text = "Добавить";
                    Main.IONewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    ActivateEditIO(true, Main);
                    NewIO = false;
                    CopyIO = true;
                    Main.IOSaveButton.Enabled = true;
                    Main.IOEditCheckBox.Checked = false;
                }
                else
                {
                    MessageBox.Show("Не выбран элемент!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void IODeleteButton_Click(MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    if (Main.IOID.Text != "")
                    {
                        DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить \"" + Main.IOName.Text + "\"?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            var Строка3 = db.IO.Where(c => c.Id == Convert.ToInt32(Main.IOID.Text)).FirstOrDefault();
                            if (Строка3 != null)
                            {
                                Строка3.UserName = UserSettings.User;
                                Строка3.ДатаСозданияЗаписи = DateTime.Now;
                                Строка3.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                db.SubmitChanges();
                            }
                            DevExpress.XtraGrid.Views.Grid.GridView gv = Main.IOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                            if (gv.FocusedRowHandle > 0)
                            {
                                var Stroke = (IO)gv.GetRow(gv.FocusedRowHandle - 1);
                                if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                            }
                            ClearControlsIO(Main);
                            GetIO(Main);
                            ActivateEditIO(false, Main);
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не выбрана строка для удаления!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        protected void ClearControlsIO(MainForm Main)
        {
            foreach (Control c in Main.IOPanel.Controls)
            {
                if (c is TextBox)
                {
                    var TB = c as TextBox;
                    TB.Text = "";
                }
                if (c is NumericUpDown)
                {
                    var NUB = c as NumericUpDown;
                    NUB.Text = "";
                }
                if (c is ComboBox)
                {
                    var CB = c as ComboBox;
                    CB.Text = "";
                }
                if (c is DateTimePicker)
                {
                    var DTP = c as DateTimePicker;
                    DTP.Text = "";
                    DTP.Checked = false;
                }
                Main.IOID.Text = "";
            }
        }

        private void ActivateEditIO(bool EditableFields, MainForm Main)
        {
            try
            {
                if (EditableFields)
                {
                    foreach (Control c in Main.IOPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = false;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.ReadOnly = false;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = true;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = true;
                            DTP.Checked = false;
                        }
                    }
                    Main.IOSaveButton.Enabled = true;
                }
                else
                {
                    foreach (Control c in Main.IOPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = true;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.ReadOnly = true;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = false;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = false;
                            DTP.Checked = false;
                        }
                        Main.IOEditCheckBox.Enabled = true;
                    }
                    NewIO = false;
                    Main.IONewButton.Text = "Добавить";
                    Main.IONewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.IOSaveButton.Enabled = false;
                    Main.IOEditCheckBox.Checked = false;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void NewIOButton(MainForm Main)
        {
            try
            {
                if (NewIO)
                {
                    NewIO = false;
                    Main.IONewButton.Text = "Добавить";
                    Main.IONewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.IOSaveButton.Enabled = false;
                }
                else
                {
                    NewIO = true;
                    ClearControlsIO(Main);
                    Main.IONewButton.Text = "Отменить";
                    Main.IONewButton.Image = global::LIMS.Properties.Resources.cancel_16x16;
                    Main.IOSaveButton.Enabled = true;
                }
                ActivateEditIO(Main.IONewButton.Text == "Отменить", Main);
                Main.IOEditCheckBox.Enabled = Main.IONewButton.Text != "Отменить";
                Main.IOEditCheckBox.Checked = false;
                Main.IODataSledTOTip.Text = "дата";
                Main.IODateSledAttistachiiTip.Text = "дата";
                Main.IODataPosledTOTip.Text = "дата";
                Main.IODataINAttestachiiTip.Text = "дата";
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void IOEditCheckBox_CheckedChanged(MainForm Main)
        {
            if (!NewIO && Main.IOID.Text != "")
            {
                ActivateEditIO(Main.IOEditCheckBox.Checked, Main);
            }
            else
            {
                Main.IOEditCheckBox.Checked = false;
            }
        }

        internal void IOSaveButton_Click(MainForm Main)
        {
            try
            {
                try
                {
                    var t = Convert.ToInt32(Main.ION.Text);
                }
                catch
                {
                    MessageBox.Show("Не корректное значения в поле \"№ п/п\"", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                using (DbDataContext db = new DbDataContext())
                {
                    //Редактирование
                    if (!NewIO && !CopyIO)
                    {
                        if (Main.IOID.Text != "")
                        {
                            var EditID = Convert.ToInt32(Main.IOID.Text);
                            var Stroka = db.IO.Where(c => c.Id == EditID).FirstOrDefault();
                            Stroka.Id = EditID;
                            try
                            {
                                var IntN = Main.ION.Text.Split('.');
                                Stroka.Nint = Convert.ToInt32(IntN[0]);
                            }
                            catch { }
                            Stroka.N = Main.ION.Text;
                            Stroka.НаименованиеВидовИспмтанийИВмдовХарактеристикПродукции = Main.IONameVidovIspitanii.Text;
                            Stroka.НаименованиеИспытательготоОборудования = Main.IOName.Text;
                            Stroka.ТипМарка = Main.IOTipMarka.Text;
                            Stroka.Заводской = Main.IOZavodskoy.Text;
                            Stroka.ИнвентарныйНомер = Main.IOInventarNomer.Text;
                            Stroka.Изготовитель = Main.IOIzgotovitel.Text;
                            Stroka.ГодВыпуска = Main.IOGodVyp.Text;
                            Stroka.ОсновныеТехническиеХарактеристики = Main.IOTehnichHaracter.Text;
                            Stroka.ГодВводаВЭксплуатацию = Main.IOGodVvodaVEkspluatachiu.Text;
                            Stroka.НомерДокументаОбАттестации = Main.IONomerDokumentaObAttes.Text;
                            if (Main.IODataINAttestachiiTip.Text != "нет")
                                Stroka.ДатаАттестации = DateTime.Parse(Main.IODataINAttestachii.Text);
                            else Stroka.ДатаАттестации = null;
                            //Stroka.ДатаАттестации = Main.IODataINAttestachiiTip.Text != "нет" ? Main.IODataINAttestachii.Checked ? DateTime.Parse(Main.IODataINAttestachii.Text) : Stroka.ДатаАттестации : null ;
                            Stroka.Периодичность = Main.IOPeriodich.Text;
                            if (Main.IODateSledAttistachiiTip.Text != "нет")
                            {
                                if (Main.IODateSledAttistachiiTip.Text == "дата")
                                    Stroka.ДатаСледАттестации = DateTime.Parse(Main.IODateSledAttistachiiDate.Text).Date;
                                else
                                    Stroka.ДатаСледАттестации = Stroka.ДатаАттестации.HasValue ? Stroka.ДатаАттестации.Value.AddMonths(Convert.ToInt32(Main.IODateSledAttistachiiKol.Text)).Date : Stroka.ДатаСледАттестации;
                            }
                            else Stroka.ДатаСледАттестации = null;
                            Stroka.МестоУстановкиИлиХранения = Main.IOMestoUstanovkiIHranenia.Text;
                            Stroka.ПравоСобственности = Main.IOPravoSobstvennosti.Text;
                            if (Main.IODataPosledTOTip.Text != "нет")
                                Stroka.ПровДатаПослТО = DateTime.Parse(Main.IODataPosledTO.Text).Date;
                            else Stroka.ПровДатаПослТО = null;
                            if (Main.IODataSledTOTip.Text != "нет")
                            {
                                if (Main.IODataSledTOTip.Text == "дата")
                                    Stroka.ДатаСледПроведенияТО = DateTime.Parse(Main.IODataSledTODate.Text).Date;
                                else Stroka.ДатаСледПроведенияТО = Stroka.ПровДатаПослТО.HasValue ? Stroka.ПровДатаПослТО.Value.AddMonths(Convert.ToInt32(Main.IODataSledTOKol.Text)).Date : Stroka.ДатаСледПроведенияТО;
                            }
                            else Stroka.ДатаСледПроведенияТО = null;
                            Stroka.Примечание = Main.IOPrimechanie.Text;
                            Stroka.VisibleStatus = Stroka.VisibleStatus;
                            Stroka.ДатаСозданияЗаписи = DateTime.Now;
                            Stroka.UserName = UserSettings.User;
                            Stroka.Owner = null;
                            HistoryIO(Stroka, 200);
                            db.SubmitChanges();
                            GetIO(Main);
                            int rowHandle = Main.IOGridView.LocateByValue("Id", int.Parse(Main.IOID.Text));
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                Main.IOGridView.FocusedRowHandle = rowHandle;
                        }

                    }
                    else
                    {
                        //Добавление                        
                        var NextID = db.IO != null && db.IO.Count() > 0 ? db.IO.Max(c => c.Id) + 1 : 1;
                        DateTime? ДатаАттестации = null;
                        if (Main.IODataINAttestachiiTip.Text != "нет")
                        {                            
                            ДатаАттестации = DateTime.Parse(Main.IODataINAttestachii.Text).Date;
                        }
                        DateTime? ДатаСледАттестации = null;
                        if (Main.IODateSledAttistachiiTip.Text == "дата")
                        {                            
                            ДатаСледАттестации = DateTime.Parse(Main.IODateSledAttistachiiDate.Text).Date;
                        }
                        else if (Main.IODateSledAttistachiiTip.Text != "нет")
                        {
                            if (ДатаАттестации != null) ДатаСледАттестации = Main.IODataINAttestachii.Value.AddMonths(Convert.ToInt32(Main.IODateSledAttistachiiKol.Text)).Date;
                        }
                        DateTime? ПровДатаПослТО = null;
                        if (Main.IODataPosledTOTip.Text != "нет")
                        {                            
                            ПровДатаПослТО = DateTime.Parse(Main.IODataPosledTO.Text).Date;
                        }
                        DateTime? ДатаСледПроведенияТО = null;
                        if (Main.IODataSledTOTip.Text == "дата")
                        {                            
                            ДатаСледПроведенияТО = DateTime.Parse(Main.IODataSledTODate.Text).Date;
                        }
                        else if (Main.IODataSledTOTip.Text != "нет")
                        {
                            if (ПровДатаПослТО != null) ДатаСледПроведенияТО = Main.IODataPosledTO.Value.AddMonths(Convert.ToInt32(Main.IODataSledTOKol.Text)).Date;
                        }
                        int NINT = 0;
                        try
                        {
                            var IntN = Main.ION.Text.Split('.');
                            NINT = Convert.ToInt32(IntN[0]);
                        }
                        catch { }
                        db.IO.InsertOnSubmit(new IO
                        {
                            Id = NextID,
                            Nint = NINT,
                            N = Main.ION.Text,
                            НаименованиеВидовИспмтанийИВмдовХарактеристикПродукции = Main.IONameVidovIspitanii.Text,
                            НаименованиеИспытательготоОборудования = Main.IOName.Text,
                            ТипМарка = Main.IOTipMarka.Text,
                            Заводской = Main.IOZavodskoy.Text,
                            ИнвентарныйНомер = Main.IOInventarNomer.Text,
                            Изготовитель = Main.IOIzgotovitel.Text,
                            ГодВыпуска = Main.IOGodVyp.Text,
                            ОсновныеТехническиеХарактеристики = Main.IOTehnichHaracter.Text,
                            ГодВводаВЭксплуатацию = Main.IOGodVvodaVEkspluatachiu.Text,
                            НомерДокументаОбАттестации = Main.IONomerDokumentaObAttes.Text,
                            ДатаАттестации = ДатаАттестации,
                            Периодичность = Main.IOPeriodich.Text,
                            ДатаСледАттестации = ДатаСледАттестации,
                            ПравоСобственности = Main.IOPravoSobstvennosti.Text,
                            МестоУстановкиИлиХранения = Main.IOMestoUstanovkiIHranenia.Text,
                            ПровДатаПослТО = ПровДатаПослТО,
                            ДатаСледПроведенияТО = ДатаСледПроведенияТО,
                            Примечание = Main.IOPrimechanie.Text,
                            VisibleStatus = 0,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = null
                        });
                        //История
                        db.IO.InsertOnSubmit(new IO
                        {
                            Id = NextID + 1,
                            Nint = NINT,
                            N = Main.ION.Text,
                            НаименованиеВидовИспмтанийИВмдовХарактеристикПродукции = Main.IONameVidovIspitanii.Text,
                            НаименованиеИспытательготоОборудования = Main.IOName.Text,
                            ТипМарка = Main.IOTipMarka.Text,
                            Заводской = Main.IOZavodskoy.Text,
                            ИнвентарныйНомер = Main.IOInventarNomer.Text,
                            Изготовитель = Main.IOIzgotovitel.Text,
                            ГодВыпуска = Main.IOGodVyp.Text,
                            ОсновныеТехническиеХарактеристики = Main.IOTehnichHaracter.Text,
                            ГодВводаВЭксплуатацию = Main.IOGodVvodaVEkspluatachiu.Text,
                            НомерДокументаОбАттестации = Main.IONomerDokumentaObAttes.Text,
                            ДатаАттестации = ДатаАттестации,
                            Периодичность = Main.IOPeriodich.Text,
                            ДатаСледАттестации = ДатаСледАттестации,
                            ПравоСобственности = Main.IOPravoSobstvennosti.Text,
                            МестоУстановкиИлиХранения = Main.IOMestoUstanovkiIHranenia.Text,
                            ПровДатаПослТО = ПровДатаПослТО,
                            ДатаСледПроведенияТО = ДатаСледПроведенияТО,
                            Примечание = Main.IOPrimechanie.Text,
                            VisibleStatus = 100,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = NextID,
                        });
                        db.SubmitChanges();
                        NewIOButton(Main);
                        GetIO(Main);
                        ActivateEditIO(false, Main);
                        Main.IOSaveButton.Enabled = false;
                    }
                    Main.IODateSledAttistachiiDate.Checked = false;
                    NewIO = false;
                    CopyIO = false;
                    db.Dispose();
                    Main.IOEditCheckBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void HistoryIO(IO stroka, int Kod)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.IO.InsertOnSubmit(new IO
                    {
                        Id = db.IO != null && db.IO.Count() > 0 ? db.IO.Max(c => c.Id) + 1 : 1,
                        N = stroka.N,
                        Nint = stroka.Nint,
                        НаименованиеВидовИспмтанийИВмдовХарактеристикПродукции = stroka.НаименованиеВидовИспмтанийИВмдовХарактеристикПродукции,
                        НаименованиеИспытательготоОборудования = stroka.НаименованиеИспытательготоОборудования,
                        ТипМарка = stroka.ТипМарка,
                        Заводской = stroka.Заводской,
                        ИнвентарныйНомер = stroka.ИнвентарныйНомер,
                        Изготовитель = stroka.Изготовитель,
                        ГодВыпуска = stroka.ГодВыпуска,
                        ОсновныеТехническиеХарактеристики = stroka.ОсновныеТехническиеХарактеристики,
                        ГодВводаВЭксплуатацию = stroka.ГодВводаВЭксплуатацию,
                        НомерДокументаОбАттестации = stroka.НомерДокументаОбАттестации,
                        ДатаАттестации = stroka.ДатаАттестации,
                        Периодичность = stroka.Периодичность,
                        ДатаСледАттестации = stroka.ДатаСледАттестации,
                        МестоУстановкиИлиХранения = stroka.МестоУстановкиИлиХранения,
                        ПровДатаПослТО = stroka.ПровДатаПослТО,
                        ДатаСледПроведенияТО = stroka.ДатаСледПроведенияТО,
                        Примечание = stroka.Примечание,
                        VisibleStatus = Kod,
                        ДатаСозданияЗаписи = DateTime.Now,
                        UserName = UserSettings.User,
                        Owner = stroka.Id,
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void IOGrid_Click(MainForm Main)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gv = Main.IOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                var Stroke = (IO)gv.GetRow(gv.FocusedRowHandle);
                if (Stroke == null) return;
                SelectedRowId = Stroke.Id.ToString();
                Main.IOID.Text = Stroke.Id.ToString();
                Main.ION.Text = Stroke.N.ToString();
                Main.IONameVidovIspitanii.Text = Stroke.НаименованиеВидовИспмтанийИВмдовХарактеристикПродукции;
                Main.IOName.Text = Stroke.НаименованиеИспытательготоОборудования;
                Main.IOTipMarka.Text = Stroke.ТипМарка;
                Main.IOZavodskoy.Text = Stroke.Заводской;
                Main.IOInventarNomer.Text = Stroke.ИнвентарныйНомер;
                Main.IOIzgotovitel.Text = Stroke.Изготовитель;
                Main.IOGodVyp.Text = Stroke.ГодВыпуска;
                Main.IOTehnichHaracter.Text = Stroke.ОсновныеТехническиеХарактеристики;
                Main.IOGodVvodaVEkspluatachiu.Text = Stroke.ГодВводаВЭксплуатацию;
                Main.IONomerDokumentaObAttes.Text = Stroke.НомерДокументаОбАттестации;
                Main.IODataINAttestachii.Text = Stroke.ДатаАттестации.ToString();
                Main.IOPeriodich.Text = Stroke.Периодичность;
                Main.IODateSledAttistachiiDate.Text = Stroke.ДатаСледАттестации.ToString();
                Main.IOPravoSobstvennosti.Text = Stroke.ПравоСобственности;
                Main.IOMestoUstanovkiIHranenia.Text = Stroke.МестоУстановкиИлиХранения;
                Main.IODataPosledTO.Text = Stroke.ПровДатаПослТО.ToString();
                Main.IODataSledTODate.Text = Stroke.ДатаСледПроведенияТО.ToString();
                Main.IOPrimechanie.Text = Stroke.Примечание;
                Main.IODateSledAttistachiiDate.Checked = false;
                ActivateEditIO(false, Main);
                if (Stroke.ДатаСледПроведенияТО.HasValue)
                    Main.IODataSledTOTip.Text = "дата";
                else Main.IODataSledTOTip.Text = "нет";
                if (Stroke.ДатаСледАттестации.HasValue)
                    Main.IODateSledAttistachiiTip.Text = "дата";
                else Main.IODateSledAttistachiiTip.Text = "нет";
                if (Stroke.ДатаАттестации.HasValue)
                    Main.IODataINAttestachiiTip.Text = "дата";
                else Main.IODataINAttestachiiTip.Text = "нет";
                if (Stroke.ПровДатаПослТО.HasValue)
                    Main.IODataPosledTOTip.Text = "дата";
                else Main.IODataPosledTOTip.Text = "нет";
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void CheckColumnButton_Click(MainForm Main)
        {
            try
            {
                CheckIOColumnForm f = new CheckIOColumnForm(Main, this);
                f.Owner = Main;
                f.ShowDialog();
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        internal void IODateSledAttistachiiTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.IODateSledAttistachiiTip.Text == "дата")
            {
                Main.IODateSledAttistachiiDate.Visible = true;
                Main.IODateSledAttistachiiKol.Visible = false;
            }
            else if (Main.IODateSledAttistachiiTip.Text == "нет")
            {
                Main.IODateSledAttistachiiDate.Visible = false;
                Main.IODateSledAttistachiiKol.Visible = false;
            }
            else
            {
                Main.IODateSledAttistachiiDate.Visible = false;
                Main.IODateSledAttistachiiKol.Visible = true;
            }
        }

        internal void IOGridView_ColumnWidthChanged(MainForm MF)
        {
            try
            {
                if (ChangeWidth)
                {
                    UserSettings.IOColumnWidth = "";
                    for (int i = 1; i < 20; i++)
                    {
                        if (UserSettings.IOColumnWidth != "")
                            UserSettings.IOColumnWidth = UserSettings.IOColumnWidth + ",";
                        UserSettings.IOColumnWidth = UserSettings.IOColumnWidth + MF.IOGridView.Columns[i].Width.ToString();
                    }
                    UserSettings.SaveSettings();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void IODataSledTOTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.IODataSledTOTip.Text == "дата")
            {
                Main.IODataSledTODate.Visible = true;
                Main.IODataSledTOKol.Visible = false;
            }
            else if (Main.IODataSledTOTip.Text == "нет")
            {
                Main.IODataSledTODate.Visible = false;
                Main.IODataSledTOKol.Visible = false;
            }
            else
            {
                Main.IODataSledTODate.Visible = false;
                Main.IODataSledTOKol.Visible = true;
                Main.IODataSledTOKol.Value = 12;
            }
        }

        internal void AutoWidthColumn(MainForm MF)
        {
            if (UserSettings.IOColumnAutoWidth)
            {
                UserSettings.IOColumnAutoWidth = false;
            }
            else UserSettings.IOColumnAutoWidth = true;
            GetIO(MF);
            UserSettings.SaveSettings();
        }

        internal void IOPrint(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc1 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Испытательное оборудование.dotx");
                    var RowCount = MF.IOGridView.RowCount;
                    if (RowCount > 1) WordDoc1.AddTableCell(2, RowCount - 1);
                    int Cell = 1;
                    for (int rowHandle = 0; rowHandle < RowCount; rowHandle++)
                    {
                        Cell++;
                        if (MF.IOGridView.IsDataRow(rowHandle) || Cell >= 39)
                        {
                            try
                            {
                                var SelectedRow = (IO)MF.IOGridView.GetRow(rowHandle);
                                WordDoc1.AddTableValue(2, Cell, 1, SelectedRow.N.ToString());
                                WordDoc1.AddTableValue(2, Cell, 2, SelectedRow.НаименованиеВидовИспмтанийИВмдовХарактеристикПродукции);
                                WordDoc1.AddTableValue(2, Cell, 3, SelectedRow.НаименованиеИспытательготоОборудования);
                                WordDoc1.AddTableValue(2, Cell, 4, SelectedRow.ТипМарка);
                                WordDoc1.AddTableValue(2, Cell, 5, SelectedRow.Заводской);
                                WordDoc1.AddTableValue(2, Cell, 6, SelectedRow.ИнвентарныйНомер);
                                WordDoc1.AddTableValue(2, Cell, 7, SelectedRow.Изготовитель);
                                WordDoc1.AddTableValue(2, Cell, 8, SelectedRow.ГодВыпуска);
                                WordDoc1.AddTableValue(2, Cell, 9, SelectedRow.ОсновныеТехническиеХарактеристики);
                                WordDoc1.AddTableValue(2, Cell, 10, SelectedRow.ГодВводаВЭксплуатацию);
                                WordDoc1.AddTableValue(2, Cell, 11, SelectedRow.НомерДокументаОбАттестации);
                                WordDoc1.AddTableValue(2, Cell, 12, SelectedRow.ДатаАттестации.HasValue ? SelectedRow.ДатаАттестации.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 13, SelectedRow.Периодичность);
                                WordDoc1.AddTableValue(2, Cell, 14, SelectedRow.ДатаСледАттестации.HasValue ? SelectedRow.ДатаСледАттестации.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 15, SelectedRow.ПравоСобственности);
                                WordDoc1.AddTableValue(2, Cell, 16, SelectedRow.МестоУстановкиИлиХранения);
                                WordDoc1.AddTableValue(2, Cell, 17, SelectedRow.ПровДатаПослТО.HasValue ? SelectedRow.ПровДатаПослТО.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 18, SelectedRow.ДатаСледПроведенияТО.HasValue ? SelectedRow.ДатаСледПроведенияТО.Value.ToShortDateString() : "");
                                WordDoc1.AddTableValue(2, Cell, 19, SelectedRow.Примечание);
                            }
                            catch { }
                        }
                    }
                    if (UserSettings.IOColumns != null)
                    {
                        var Visibiliti = UserSettings.IOColumns.Split(',');
                        for (int i = Visibiliti.Length - 1; i > 0; i--)
                        {
                            if (Visibiliti[i] != "1")
                                WordDoc1.DeleteTableColumn(2, i + 1);
                        }
                    }
                    WordDoc1.TableResize(2);
                    StringBuilder rezult = new StringBuilder();
                    var result = Пропись((uint)(Cell - 1), new КлассБезТысяч(), rezult);
                    WordDoc1.ReplaceString("{КолвоДокументов}", (Cell - 1).ToString() + "(" + rezult + ")");
                    WordDoc1.ReplaceString("{Дата}", DateTime.Now.ToShortDateString() + "г.");
                    WordDoc1.Visible = true;
                    WordDoc1.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc1.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void IOReturnFromArhivButton_Click(int id, MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.IO.Where(c => c.Id == id).FirstOrDefault();
                    Stroka.VisibleStatus = 0;
                    Stroka.ДатаСозданияЗаписи = DateTime.Now;
                    Stroka.UserName = UserSettings.User;
                    HistoryIO(Stroka, 400);
                    db.SubmitChanges();
                    db.Dispose();
                    if (!real)
                    {
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.IOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        if (gv.FocusedRowHandle > 0)
                        {
                            var Stroke = (IO)gv.GetRow(gv.FocusedRowHandle - 1);
                            if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                        }
                    }
                    GetIO(Main);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void IOArhivButton_Click(int id, MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.IO.Where(c => c.Id == id).FirstOrDefault();
                    Stroka.VisibleStatus = 1;
                    Stroka.ДатаСозданияЗаписи = DateTime.Now;
                    Stroka.UserName = UserSettings.User;
                    HistoryIO(Stroka, 401);
                    db.SubmitChanges();
                    db.Dispose();
                    if (!arhiv)
                    {
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.IOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        if (gv.FocusedRowHandle > 0)
                        {
                            var Stroke = (IO)gv.GetRow(gv.FocusedRowHandle - 1);
                            if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                        }
                    }
                    GetIO(Main);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void IOArhivButton_Click(MainForm Main)
        {
            if (Main.IOArhivButton.ContextMenuStrip != null)
            {
                Main.IOArhivButton.ContextMenuStrip.Close();
                Main.IOArhivButton.ContextMenuStrip = null;
                return;
            }
            MF = Main;
            ContextMenuStrip ContextMS = new ContextMenuStrip();
            ContextMS.Items.Add("&Показать рабочее оборудование", null, IORealVisible);
            if (real)
            {
                ((ToolStripMenuItem)ContextMS.Items[0]).Checked = true;
            }
            else
            {
                ((ToolStripMenuItem)ContextMS.Items[0]).Checked = false;
            }
            ContextMS.Items.Add("&Архив", null, IOArhivVisible);
            if (arhiv)
            {
                ((ToolStripMenuItem)ContextMS.Items[1]).Checked = true;
            }
            else
            {
                ((ToolStripMenuItem)ContextMS.Items[1]).Checked = false;
            }
            ContextMS.Items.Add("&Показать изменения", null, IOHistoryVisible);
            if (HistoryOfChange)
            {
                ((ToolStripMenuItem)ContextMS.Items[2]).Checked = true;
            }
            else
            {
                ((ToolStripMenuItem)ContextMS.Items[2]).Checked = false;
            }
            ContextMS.Items.Add("&Закрыть", global::LIMS.Properties.Resources.delete_16x16, ContextMenuStripClose_Click);
            ContextMS.AutoClose = false;
            Main.IOArhivButton.ContextMenuStrip = ContextMS;
            ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);
        }

        private void IOHistoryVisible(object sender, EventArgs e)
        {
            HistoryOfChange = HistoryOfChange ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = HistoryOfChange;
            GetIO(MF);
        }

        public void ContextMenuStripClose_Click(object sender, EventArgs e)
        {
            var TSMI = sender as ToolStripMenuItem;
            var CMS = TSMI.Owner as ContextMenuStrip;
            CMS.AutoClose = true;
            MF.IOArhivButton.ContextMenuStrip = null;
            CMS.Close();
        }

        private void IORealVisible(object sender, EventArgs e)
        {
            real = real ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = real;
            GetIO(MF);
        }

        private void IOArhivVisible(object sender, EventArgs e)
        {
            arhiv = arhiv ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = arhiv;
            GetIO(MF);
        }

        internal void IODataINAttestachiiTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.IODataINAttestachiiTip.Text == "дата")
            {
                Main.IODataINAttestachii.Visible = true;
            }
            else
            {
                Main.IODataINAttestachii.Visible = false;
            }
        }

        internal void IODataPosledTOTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.IODataPosledTOTip.Text == "дата")
            {
                Main.IODataPosledTO.Visible = true;
            }
            else
            {
                Main.IODataPosledTO.Visible = false;
            }
        }

        internal void IOPeriodich_SelectedIndexChanged(MainForm mainForm)
        {
            try
            {
                var Srok = mainForm.IOPeriodich.Text.Split(' ');
                mainForm.IODateSledAttistachiiKol.Value = Convert.ToInt32(Srok[0]);
            }
            catch { }
        }

        internal void IOCellMergeButton_Click(MainForm Main)
        {
            UserSettings.IOCellMerge = !UserSettings.IOCellMerge;
            Main.IOGridView.OptionsView.AllowCellMerge = UserSettings.IOCellMerge;
            if (UserSettings.IOCellMerge)
                Main.IOCellMergeButton.Image = global::LIMS.Properties.Resources.freezetoprow_32x32;
            else Main.IOCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            UserSettings.SaveSettings();
        }

        internal void splitContainerControl3_Panel1_SizeChanged(object sender)
        {
            try
            {
                var SplitCon = sender as DevExpress.XtraEditors.SplitGroupPanel;
                UserSettings.IOSplitWidth = SplitCon.Width;
                UserSettings.SaveSettings();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void PrintGrafickAttestachii(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc2 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\График аттестации ИО.dotx");
                    List<IO> ВсеЗначения = new List<IO>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.IO.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null) && c.ДатаСледАттестации.HasValue && c.ДатаСледАттестации.Value.Year == GlobalStatic.BuferInt).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - ИО");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc2.AddTableCell(2, RowCount - 1);
                    int Cell = 2;
                    foreach (var IO in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                var Mount = IO.ДатаСледАттестации.Value.Month;
                                var Day = IO.ДатаСледАттестации.Value.Day;
                                WordDoc2.AddTableValue(2, Cell, 1, IO.НаименованиеИспытательготоОборудования + ((IO.ТипМарка == "-") ? "" : " " + IO.ТипМарка));
                                WordDoc2.AddTableValue(2, Cell, 2, IO.Заводской);
                                WordDoc2.AddTableValue(2, Cell, 2 + Mount, Day.ToString());
                                //WordDoc.AddTableValue(2, Cell, 15, IO.Примечание);
                            }
                            catch { }
                        }
                    }
                    WordDoc2.TableResize(2);
                    WordDoc2.ReplaceString("{ГОД}", GlobalStatic.BuferInt.ToString());
                    WordDoc2.Visible = true;
                    WordDoc2.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc2.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void IOPrintOsnachinnost(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc3 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Оснащенность ИО.dotx");
                    List<IO> ВсеЗначения = new List<IO>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.IO.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null)).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - IО");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc3.AddTableCell(1, RowCount - 1);
                    int Cell = 2;
                    int NextPage = 3;
                    int StartTehnich = 3;
                    int StartPrimechanie = 3;
                    string TextTehnich = "";
                    string TextPrimechanie = "";
                    foreach (var IO in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                WordDoc3.AddTableValue(1, Cell, 1, (Cell - 2).ToString());
                                if (Cell == NextPage)
                                {
                                    NextPage = NextPage + 7;
                                    WordDoc3.AddTableValue(1, Cell, 2, Cell == 3 ? "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nНефть" : "\r\n\r\n\r\nНефть");
                                }
                                else
                                {
                                    WordDoc3.AddTableValue(1, Cell, 2, "\r\n\r\n\r\n\r\n\r\n");
                                }
                                WordDoc3.AddTableValue(1, Cell, 3, IO.НаименованиеВидовИспмтанийИВмдовХарактеристикПродукции);
                                WordDoc3.AddTableValue(1, Cell, 4, IO.НаименованиеИспытательготоОборудования + " " + IO.ТипМарка + ((IO.Заводской == "б/н" || IO.Заводской == "-" || IO.Заводской == "") ? "" : " зав. №" + IO.Заводской) + ((IO.ИнвентарныйНомер == "б/н" || IO.ИнвентарныйНомер == "-" || IO.ИнвентарныйНомер == "") ? "" : " инв. №" + IO.ИнвентарныйНомер));
                                WordDoc3.AddTableValue(1, Cell, 5, IO.Изготовитель);
                                WordDoc3.AddTableValue(1, Cell, 6, IO.ОсновныеТехническиеХарактеристики);
                                WordDoc3.AddTableValue(1, Cell, 7, IO.ГодВводаВЭксплуатацию);
                                WordDoc3.AddTableValue(1, Cell, 8, "Аттестат " + IO.НомерДокументаОбАттестации + " от " + IO.ДатаАттестации + "г., " + IO.Периодичность);
                                WordDoc3.AddTableValue(1, Cell, 9, IO.ПравоСобственности);
                                WordDoc3.AddTableValue(1, Cell, 10, IO.МестоУстановкиИлиХранения);
                                WordDoc3.AddTableValue(1, Cell, 11, IO.Примечание);
                                WordDoc3.BoltText(IO.НаименованиеИспытательготоОборудования + " " + IO.ТипМарка + ((IO.Заводской == "б/н" || IO.Заводской == "-" || IO.Заводской == "") ? "" : " зав. №" + IO.Заводской));

                                if (TextTehnich != IO.ОсновныеТехническиеХарактеристики || IO.ОсновныеТехническиеХарактеристики == "")
                                {
                                    if (Cell - StartTehnich > 1)
                                    {
                                        WordDoc3.MergeCells(1, StartTehnich, Cell - 1, 6);
                                    }
                                    StartTehnich = Cell;
                                    TextTehnich = IO.ОсновныеТехническиеХарактеристики;
                                }
                                else WordDoc3.AddTableValue(1, Cell, 6, "");
                                if (TextPrimechanie != IO.Примечание || IO.Примечание == "")
                                {
                                    if (Cell - StartPrimechanie > 1)
                                    {
                                        WordDoc3.MergeCells(1, StartPrimechanie, Cell - 1, 11);
                                    }
                                    StartPrimechanie = Cell;
                                    TextPrimechanie = IO.Примечание;
                                }
                                else WordDoc3.AddTableValue(1, Cell, 11, "");
                            }
                            catch { }
                        }
                    }
                    WordDoc3.MergeCells(1, 3, Cell, 2);

                    //WordDoc.TableResize(1);
                    WordDoc3.ReplaceString("{ГОД}", DateTime.Now.Year.ToString());
                    WordDoc3.Visible = true;
                    WordDoc3.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc3.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void IOGridView_StartSorting(MainForm Main)
        {
            try
            {
                var GV = Main.IOGridView;
                if (GV.SortInfo.Count > 0)
                {
                    for (int i = GV.SortInfo.Count - 1; i >= 0; i--)
                    {
                        if (GV.SortInfo[i].Column.FieldName != "Nint")
                        {
                            if (GV.SortInfo[i].Column.FieldName == "N")
                            {
                                GV.ClearSorting();
                                GV.Columns["Nint"].SortOrder = SortOrder;
                                if (SortOrder == DevExpress.Data.ColumnSortOrder.Ascending)
                                    SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
                                else
                                    SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                                //GV.SortInfo[i].Column = SIGridView.Columns[24];
                                //GV.SortInfo[i].Column.SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
                            }
                            else
                                SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        internal void IOPrintPerechenDocumentacii(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc5 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\ПЕРЕЧЕНЬ документации на ИО.dotx");
                    List<IO> ВсеЗначения = new List<IO>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.IO.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null)).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - IO");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc5.AddTableCell(1, RowCount - 1);
                    int Cell = 1;
                    int StartCell1 = Cell + 1;
                    int EndCell1 = Cell + 1;
                    int StartCell2 = Cell + 1;
                    int EndCell2 = Cell + 1;
                    string StartValue1 = "";
                    string StartValue2 = "";
                    foreach (var IO in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                WordDoc5.AddTableValue(1, Cell, 1, (Cell - 1).ToString() + ".");
                                WordDoc5.AddTableValue(1, Cell, 2, IO.НаименованиеИспытательготоОборудования + " " + IO.ТипМарка);
                                WordDoc5.AddTableValue(1, Cell, 3, IO.Заводской == "" ? "" : "№ " + IO.Заводской);
                                if (StartValue2 == IO.ОсновныеТехническиеХарактеристики)
                                {
                                    EndCell2 = Cell;
                                }
                                else
                                {
                                    StartValue2 = IO.ОсновныеТехническиеХарактеристики;
                                    WordDoc5.AddTableValue(1, Cell, 4, StartValue2);
                                    if (StartCell2 != EndCell2) WordDoc5.MergeCells(1, StartCell2, EndCell2, 4);
                                    StartCell2 = Cell;
                                    EndCell2 = Cell;
                                }

                                if (StartValue1 == (IO.Примечание.Contains("Рабочий диапазон:") ? IO.Примечание.Replace("Рабочий диапазон:", String.Empty) : IO.Примечание))
                                {
                                    EndCell1 = Cell;
                                }
                                else
                                {
                                    StartValue1 = IO.Примечание.Contains("Рабочий диапазон:") ? IO.Примечание.Replace("Рабочий диапазон:", String.Empty) : IO.Примечание;
                                    WordDoc5.AddTableValue(1, Cell, 5, StartValue1);
                                    if (StartCell1 != EndCell1) WordDoc5.MergeCells(1, StartCell1, EndCell1, 5);
                                    StartCell1 = Cell;
                                    EndCell1 = Cell;
                                }
                            }
                            catch { }
                        }
                    }
                    StringBuilder rezult = new StringBuilder();
                    var result = Пропись((uint)(RowCount), new КлассБезТысяч(), rezult);
                    WordDoc5.ReplaceString("{колво}", RowCount.ToString() + " (" + rezult + ")");
                    WordDoc5.ReplaceString("{Дата}", DateTime.Now.ToShortDateString());
                    //WordDoc5.TableResize(1);
                    WordDoc5.Visible = true;
                    WordDoc5.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc5.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void IOPrintGrafikTO(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc6 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\График ТО ИО.dotx");
                    List<IO> ВсеЗначения = new List<IO>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.IO.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null) && c.ДатаСледПроведенияТО.HasValue && c.ДатаСледПроведенияТО.Value.Year == GlobalStatic.BuferInt).OrderBy(c => c.НаименованиеИспытательготоОборудования).ThenBy(c => c.ТипМарка).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - IO");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    int Cell = 1;
                    string Name = "";
                    int StartName = 2;
                    string Data = "";
                    int StartData = Cell + 1;
                    string Perechen = "";
                    int StartPerechen = Cell + 1;
                    string Dolgnost = "";
                    foreach (var IO in ВсеЗначения)
                    {
                        try
                        {
                            if (Name != (IO.НаименованиеИспытательготоОборудования + " " + IO.ТипМарка) || Data != (IO.ДатаСледПроведенияТО.HasValue ? IO.ДатаСледПроведенияТО.Value.ToShortDateString() : ""))
                            {
                                Cell++;
                                if (Cell > 2) WordDoc6.AddTableCell(1, 1);
                                WordDoc6.AddTableValue(1, Cell, 2, IO.НаименованиеИспытательготоОборудования + " " + IO.ТипМарка);
                                Name = IO.НаименованиеИспытательготоОборудования + " " + IO.ТипМарка;

                                WordDoc6.AddTableValue(1, Cell, 3, (IO.Заводской));

                                WordDoc6.AddTableValue(1, Cell, 4, IO.ДатаСледПроведенияТО.HasValue ? IO.ДатаСледПроведенияТО.Value.ToShortDateString() : "");
                                WordDoc6.AddTableValue(1, Cell, 5, GetPerechenRabot(IO.НаименованиеИспытательготоОборудования + " " + IO.ТипМарка));
                                WordDoc6.AddTableValue(1, Cell, 6, GetDolgnost(IO.НаименованиеИспытательготоОборудования + " " + IO.ТипМарка));
                            }
                            else
                            {
                                if (!WordDoc6.TableValue(1, Cell, 3).Contains(IO.Заводской))
                                    WordDoc6.AddTableValue(1, Cell, 3, (WordDoc6.TableValue(1, Cell, 3) + IO.Заводской));
                                if (!WordDoc6.TableValue(1, Cell, 4).Contains(IO.ДатаСледПроведенияТО.HasValue ? IO.ДатаСледПроведенияТО.Value.ToShortDateString() : ""))
                                    WordDoc6.AddTableValue(1, Cell, 4, (WordDoc6.TableValue(1, Cell, 4) + (IO.ДатаСледПроведенияТО.HasValue ? IO.ДатаСледПроведенияТО.Value.ToShortDateString() : "")));
                                WordDoc6.AddTableValue(1, Cell, 5, GetPerechenRabot(IO.НаименованиеИспытательготоОборудования + " " + IO.ТипМарка));
                                WordDoc6.AddTableValue(1, Cell, 6, GetDolgnost(IO.НаименованиеИспытательготоОборудования + " " + IO.ТипМарка));
                            }
                            Data = IO.ДатаСледПроведенияТО.HasValue ? IO.ДатаСледПроведенияТО.Value.ToShortDateString() : "";
                        }
                        catch { }
                    }

                    Name = "";
                    StartData = 2;
                    Perechen = "";
                    for (int i = 2; i < Cell + 1; i++)
                    {
                        if (Name != WordDoc6.TableValue(1, i, 2))
                        {
                            if (StartName < i - 1)
                            {
                                WordDoc6.MergeCells(1, StartName, i - 1, 2);
                            }
                            StartName = i;
                            Name = WordDoc6.TableValue(1, i, 2);
                        }
                        else
                        {
                            WordDoc6.AddTableValue(1, i, 2, "");
                        }

                        if (Perechen != WordDoc6.TableValue(1, i, 5))
                        {
                            if (StartPerechen < i - 1)
                            {
                                WordDoc6.MergeCells(1, StartPerechen, i - 1, 5);
                                WordDoc6.MergeCells(1, StartPerechen, i - 1, 6);
                            }
                            Perechen = WordDoc6.TableValue(1, i, 5);
                            Dolgnost = WordDoc6.TableValue(1, i, 6);
                            StartPerechen = i;
                        }
                        else
                        {
                            WordDoc6.AddTableValue(1, i, 5, "");
                            WordDoc6.AddTableValue(1, i, 6, "");
                        }
                    }

                    if (StartName < Cell - 1)
                        WordDoc6.MergeCells(1, StartName, Cell - 1, 2);
                    if (StartPerechen < Cell - 1)
                    {
                        WordDoc6.MergeCells(1, StartPerechen, Cell - 1, 5);
                        WordDoc6.MergeCells(1, StartPerechen, Cell - 1, 6);
                    }
                    WordDoc6.ReplaceString("{Год}", GlobalStatic.BuferInt.ToString());
                    WordDoc6.TableResize(1);
                    WordDoc6.Visible = true;
                    WordDoc6.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc6.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        private string GetDolgnost(string Name)
        {
            if (Name.ToUpper().Contains("МАССОВОЙ ДОЛИ") || Name.ToUpper().Contains("НАСЫЩЕННЫХ ПАРОВ"))
            {
                return "Лаборант хим. анализа ;\r\nИнженер хим. анализа .";
            }
            else
            {
                return "Слесарь КИП и А\r\n или\r\nЭлектромонтер\r\n.";
            }
        }

        internal void IOPrintPerechen(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc4 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\ПЕРЕЧЕНЬ ИО.dotx");
                    List<IO> ВсеЗначения = new List<IO>();
                    using (DbDataContext db = new DbDataContext())
                    {
                        try
                        {
                            ВсеЗначения = db.IO.Where(c => (c.VisibleStatus == 0 || c.VisibleStatus == null)).OrderBy(c => c.Nint).ToList();
                        }
                        catch
                        {
                            LogErrors Log = new LogErrors("Ошибка соединения с сервером - IO");
                        }
                        db.Dispose();
                    }

                    var RowCount = ВсеЗначения.Count;
                    if (RowCount > 1) WordDoc4.AddTableCell(1, RowCount - 1);
                    int Cell = 1;
                    int StartCell1 = Cell + 1;
                    int EndCell1 = Cell + 1;
                    int StartCell2 = Cell + 1;
                    int EndCell2 = Cell + 1;
                    string StartValue1 = "";
                    string StartValue2 = "";
                    foreach (var IO in ВсеЗначения)
                    {
                        Cell++;
                        {
                            try
                            {
                                WordDoc4.AddTableValue(1, Cell, 1, (Cell - 1).ToString() + ".");
                                WordDoc4.AddTableValue(1, Cell, 2, IO.НаименованиеИспытательготоОборудования + " " + IO.ТипМарка);
                                WordDoc4.AddTableValue(1, Cell, 3, IO.Заводской == "" ? "" : "№ " + IO.Заводской);
                                if (StartValue2 == IO.ОсновныеТехническиеХарактеристики)
                                {
                                    EndCell2 = Cell;
                                }
                                else
                                {
                                    StartValue2 = IO.ОсновныеТехническиеХарактеристики;
                                    WordDoc4.AddTableValue(1, Cell, 4, StartValue2);
                                    if (StartCell2 != EndCell2) WordDoc4.MergeCells(1, StartCell2, EndCell2, 4);
                                    StartCell2 = Cell;
                                    EndCell2 = Cell;
                                }

                                if (StartValue1 == (IO.Примечание.Contains("Рабочий диапазон:") ? IO.Примечание.Replace("Рабочий диапазон:", String.Empty) : IO.Примечание))
                                {
                                    EndCell1 = Cell;
                                }
                                else
                                {
                                    StartValue1 = IO.Примечание.Contains("Рабочий диапазон:") ? IO.Примечание.Replace("Рабочий диапазон:", String.Empty) : IO.Примечание;
                                    WordDoc4.AddTableValue(1, Cell, 5, StartValue1);
                                    if (StartCell1 != EndCell1) WordDoc4.MergeCells(1, StartCell1, EndCell1, 5);
                                    StartCell1 = Cell;
                                    EndCell1 = Cell;
                                }
                            }
                            catch { }
                        }
                    }
                    StringBuilder rezult = new StringBuilder();
                    var result = Пропись((uint)(RowCount), new КлассБезТысяч(), rezult);
                    WordDoc4.ReplaceString("{колво}", RowCount.ToString() + " (" + rezult + ")");
                    WordDoc4.ReplaceString("{Дата}", DateTime.Now.ToShortDateString());
                    //WordDoc4.TableResize(1);
                    WordDoc4.Visible = true;
                    WordDoc4.wordApplication.Activate();
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        WordDoc4.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        private string GetPerechenRabot(string Name)
        {
            if (Name.ToUpper().Contains("ЭЛЕКТРОПЕЧЬ НИЗКОТЕМПЕРАТУРН"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности, согласно эксплуатационной документации, исправности электрического шнура, вилки, исправности заземления, правильности подключения к электросети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней аттестации);\r\n3.Регулировка плотного прилегания дверцы шкафа.\r\n4.Проверка состояния контактных соединений электроцепи(при необходимости подтянуть контакты).\r\n5.Очистка:\r\n-вентиляционных отверстий;\r\n-камеры и наружных поверхностей электропечи.\r\n5.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("ТЕРМОСТАТ ЖИДКОСТНЫЙ") && Name.ToUpper().Contains("ИССЛЕДОВАНИЯ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности, согласно эксплуатационной документации, исправности электрического шнура, вилки, исправности заземления, правильности подключения к электросети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней аттестации);\r\n3.Очистка поверхностей элементов блока терморегулирования.\r\n4.Замена дистиллированной воды в ванне термостата.\r\n5.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("ТЕРМОСТАТ ЖИДКОСТНЫЙ") && Name.ToUpper().Contains("НИЗКОТЕМПЕРАТУРНЫЙ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности, согласно эксплуатационной документации, исправности электрического шнура, вилки, исправности заземления, правильности подключения к электросети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней аттестации).\r\n3.Очистка поверхностей элементов блока терморегулирования.\r\n4.Замена дистиллированной воды в ванне термостата.\r\n5.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("ТЕРМОСТАТ") && Name.ToUpper().Contains("КРИОСТАТ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности, согласно эксплуатационной документации, исправности электрического шнура, вилки, исправности заземления, правильности подключения к электросети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней аттестации);\r\n-отсутствие перегибов трубок.\r\n3.Очистка:\r\n-поверхности конденсатора терморегулирования;\r\n-стенок бани и внутренних частей циркулятора.\r\n4.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("ТЕРМОСТАТ") && Name.ToUpper().Contains("ПЛОТНОСТ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности, согласно эксплуатационной документации, исправности электрического шнура, вилки, исправности заземления, правильности подключения к электросети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней аттестации).\r\n3.Очистка поверхностей элементов блока терморегулирования.\r\n4.Замена дистиллированной воды в ванне термостата.\r\n5.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("АППАРАТ") && Name.ToUpper().Contains("РАЗГОНКИ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности, согласно эксплуатационной документации, исправности электрического шнура, вилки, исправности заземления, правильности подключения к электросети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней аттестации);\r\n3.Проверить прочность крепления органов управления, четкость их фиксации, плавность хода, состояние гальванических и лакокрасочных покрытий, крепление деталей и узлов, отсутствие трещин, сколов на деталях из стекла и пластмасс.\r\n4.Внутренняя и внешняя чистка аппарата от пыли.\r\n5.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("АППАРАТ") && Name.ToUpper().Contains("МАССОВОЙ ДОЛ"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр с целью определения целостности аппарата, проверка герметичности всех соединений.\r\n3.Промывка всех стеклянных частей хромовой смесью.";
            }
            if (Name.ToUpper().Contains("ХОЛОДИЛЬНИК"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат, исправности электрического шнура, заземления, правильности подключения к электрической сети, целостности кнопок управления, исправности световой индикации;\r\n-проверка наличия бирки на аппарате(наименования оборудования, заводского номера, даты выпуска, дата последней проверки технического состояния).\r\n3.Очистка внешней и внутренней поверхности холодильника.\r\n4.Проверка возможности включения аппарата, работоспособности органов управления, функционирование индикации в соответствии с порядком, установленным в эксплуатационной документации.";
            }
            if (Name.ToUpper().Contains("АППАРАТ") && Name.ToUpper().Contains("НАСЫЩЕННЫХ ПАР"))
            {
                return "1. Проверка наличия эксплуатационно-технической документации.\r\n2.Внешний осмотр оборудования:\r\n-визуальная проверка комплектности согласно эксплуатационной документации на аппарат;\r\n-проверка целостности резьбовых соединений;\r\n-проверка соответствия конструкции аппарата требованиям ГОСТ 1756 - 2000 приложение А.\r\n3.Очистка поверхности аппарата от загрязнений.";
            }
            return String.Empty;
        }
    }
}
