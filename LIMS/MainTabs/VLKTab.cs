﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraTab;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using static LIMS.FiltrVLKProbiClass;

namespace LIMS
{
    public class VLKTab
    {
        WordDocument WordDoc1;
        WordDocument WordDoc2;
        WordDocument WordDoc3;
        ExcelDocument ExcelDoc1;
        ExcelDocument ExcelDoc2;
        MainForm MF;
        internal int VisiblePages;
        List<int> orderList = new List<int>();
        SelectedRow SelectedRowId = new SelectedRow();
        int KolvoZaprosov = 0;
        DevExpress.Data.ColumnSortOrder SortOrderProbi = DevExpress.Data.ColumnSortOrder.Ascending;
        DevExpress.Data.ColumnSortOrder SortOrderProbiOperativnii = DevExpress.Data.ColumnSortOrder.Descending;
        internal DevExpress.Data.ColumnSortOrder SortOrderProbiStabilnosti = DevExpress.Data.ColumnSortOrder.Descending;
        internal void UpdateGSOGrid(MainForm Main)
        {
            try
            {
                KolvoZaprosov = 0;
                var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');
                if (Names[0] == "MainGSOTab")
                {
                    Main.GSOGridView.CustomDrawCell -= new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(Main.GSOGridView_CustomDrawCell);
                    GetVLKLoadList(Main);
                    GetVLK(Main);
                }
                else if (Names[0] == "Пробы")
                {
                    Main.GSOGridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(Main.GSOGridView_CustomDrawCell);
                    GetProbiLoadList(Main, 1);
                    GetProbi(Main, Convert.ToInt32(Names[1]), 1);
                }
                else if (Names[0] == "Оперативный")
                {
                    Main.GSOGridView.CustomDrawCell -= new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(Main.GSOGridView_CustomDrawCell);
                    GetOperativniiKontrolLoadList(Main);
                    GetOperativniiKontrol(Main);
                }
                else if (Names[0] == "Стабильности")
                {
                    Main.GSOGridView.CustomDrawCell -= new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(Main.GSOGridView_CustomDrawCell);
                    GetKontrolStabilnostiLoadList(Main);
                    GetKontrolStabilnosti(Main);
                }
                else if (Names[0] == "ПробыОперативный")
                {
                    Main.GSOGridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(Main.GSOGridView_CustomDrawCell);
                    GetProbiLoadList(Main, 2);
                    GetProbi(Main, Convert.ToInt32(Names[1]), 2, Convert.ToInt32(Names[2]));
                }
                else if (Names[0] == "ПробыСтабильности")
                {
                    Main.GSOGridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(Main.GSOGridView_CustomDrawCell);
                    GetProbiStabilnostiLoadList(Main);
                    GetProbiStabilnosti(Main, Convert.ToInt32(Names[1]), Convert.ToInt32(Names[2]));
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GetOperativniiKontrol(MainForm Main)
        {
            try
            {
                Main.VLKCellMergeButton.Image = UserSettings.VLKOperativniiCellMerge ? global::LIMS.Properties.Resources.freezetoprow_32x32 : global::LIMS.Properties.Resources.gridmergecells_32x32;
                Main.VLKCellMergeButton.Visible = true;
                Main.AddAnalizProbiButton.Visible = false;
                Main.VLKPrintButton.Visible = false;
                Main.DiagramButton.Visible = false;
                Main.FiltrButton.Visible = false;
                new Thread(GetOperativniiKontrolThread).Start(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void GetOperativniiKontrolThread(object MainF)
        {
            var Main = MainF as MainForm;
            try
            {
                MF = Main;
                using (DbDataContext db = new DbDataContext())
                {
                    //db.CommandTimeout = 1440;
                    var Operativnii = new List<OperativniiKontrol>();
                    var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');
                    try
                    {
                        if (Names.Length > 1)
                        {
                            if (!UserSettings.Developer)
                            {
                                Operativnii = db.OperativniiKontrol.Where(c => c.AnalizID == Convert.ToInt32(Names[1]) && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1)).OrderByDescending(c => c.CreateDate).ThenByDescending(c => c.Book).ToList();
                            }
                            else
                            {
                                Operativnii = db.OperativniiKontrol.Where(c => c.AnalizID == Convert.ToInt32(Names[1])).OrderByDescending(c => c.CreateDate).ThenByDescending(c => c.Book).ToList();
                            }
                        }
                    }
                    catch
                    {
                        LogErrors Log = new LogErrors("Ошибка соединения с сервером - VLK");
                        if (KolvoZaprosov == 3) Main.Invoke(new System.Action(() => MessageBox.Show("Сервер не отвечает!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        GetOperativniiKontrolLoadList(Main, Operativnii);
                    }));
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                //MF.Invoke(new System.Action(() => MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));                
            }
            finally
            {
                try
                {
                    DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    if ((gv.GetRow(0) == null || gv.GetRow(0).GetType().Name != "OperativniiKontrol") && KolvoZaprosov < 3)
                    {
                        new Thread(GetOperativniiKontrolThread).Start(MainF);
                        KolvoZaprosov++;
                    }
                }
                catch { }
            }
        }

        private void GetOperativniiKontrolLoadList(MainForm Main, List<OperativniiKontrol> Operativnii = null)
        {
            Main.GSOGridView.FocusedRowChanged -= Main.GSOGridView_FocusedRowChanged;
            Main.GSOGrid.BeginUpdate();
            try
            {
                ClearGrid(Main);
                if (Operativnii == null) Operativnii = new List<OperativniiKontrol>();
                Main.GSOGrid.DataSource = Operativnii;
            }
            finally
            {
                Main.GSOGrid.EndUpdate();
            }
            Main.GSOGridView.Columns[0].Visible = false;
            Main.GSOGridView.Columns[1].Visible = false;
            Main.GSOGridView.Columns[6].Visible = false;
            Main.GSOGridView.Columns[7].Visible = false;
            Main.GSOGridView.Columns[8].Visible = false;
            Main.GSOGridView.Columns[9].Visible = false;
            Main.GSOGridView.Columns[10].Visible = false;
            Main.GSOGridView.Columns[2].Caption = "№";
            Main.GSOGridView.Columns[3].Caption = "Дата создания";
            Main.GSOGridView.Columns[4].Caption = "Дата закрытия";
            Main.GSOGridView.Columns[5].Caption = "Количество измерений";
            Main.GSOGridView.Columns[6].Caption = "Аттестованное значение";
            Main.GSOGridView.Columns[3].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            Main.GSOGridView.Columns[3].DisplayFormat.FormatString = "dd/MM/yyyy";
            Main.GSOGridView.Columns[4].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            Main.GSOGridView.Columns[4].DisplayFormat.FormatString = "dd/MM/yyyy";
            Main.GSOGridView.OptionsView.ShowColumnHeaders = true;
            Main.GSOGridView.OptionsView.AllowCellMerge = UserSettings.VLKOperativniiCellMerge;

            //Wrap Text
            DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
            MemoEdit.WordWrap = true;
            Main.GSOGridView.OptionsView.RowAutoHeight = true;
            foreach (GridColumn My_GridColumn in Main.GSOGridView.Columns)
            {
                My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                My_GridColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                My_GridColumn.ColumnEdit = MemoEdit;
            }

            ColumnView View = Main.GSOGridView;
            GridColumn column = View.Columns["Id"];
            if (column != null)
            {
                int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId.GetRowID(Main.GSOSpisokTab.SelectedTabPage.Name));
                if (rhFound != GridControl.InvalidRowHandle)
                {
                    View.FocusedRowHandle = rhFound;
                    View.FocusedColumn = column;
                }
            }
            Main.GSOGridView.FocusedRowChanged += Main.GSOGridView_FocusedRowChanged;
        }

        private void GetKontrolStabilnosti(MainForm Main)
        {
            try
            {
                Main.VLKCellMergeButton.Image = UserSettings.VLKStabilnostCellMerge ? global::LIMS.Properties.Resources.freezetoprow_32x32 : global::LIMS.Properties.Resources.gridmergecells_32x32;
                Main.VLKCellMergeButton.Visible = true;
                Main.AddAnalizProbiButton.Visible = false;
                Main.VLKPrintButton.Visible = false;
                Main.DiagramButton.Visible = false;
                Main.FiltrButton.Visible = false;
                new Thread(GetKontrolStabilnostiThread).Start(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void GetKontrolStabilnostiThread(object MainF)
        {
            var Main = MainF as MainForm;
            try
            {
                MF = Main;
                using (DbDataContext db = new DbDataContext())
                {
                    var Stabilnosti = new List<KontrolStabilnosti>();
                    var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');
                    try
                    {
                        if (Names.Length > 1)
                        {
                            if (!UserSettings.Developer)
                            {
                                Stabilnosti = db.KontrolStabilnosti.Where(c => c.AnalizID == Convert.ToInt32(Names[1]) && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1)).OrderByDescending(c => c.CreateDate).ThenByDescending(c => c.Book).ToList();
                            }
                            else Stabilnosti = db.KontrolStabilnosti.Where(c => c.AnalizID == Convert.ToInt32(Names[1])).OrderByDescending(c => c.CreateDate).ThenByDescending(c => c.Book).ToList();
                        }
                    }
                    catch
                    {
                        LogErrors Log = new LogErrors("Ошибка соединения с сервером - VLK");
                        if (KolvoZaprosov == 3) Main.Invoke(new System.Action(() => MessageBox.Show("Сервер не отвечает!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        GetKontrolStabilnostiLoadList(Main, Stabilnosti);
                    }));
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MF.Invoke(new System.Action(() => MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
            }
            finally
            {
                try
                {
                    DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    if ((gv.GetRow(0) == null || gv.GetRow(0).GetType().Name != "KontrolStabilnosti") && KolvoZaprosov < 3)
                    {
                        new Thread(GetKontrolStabilnostiThread).Start(MainF);
                        KolvoZaprosov++;
                    }
                }
                catch { }
            }
        }

        private void GetKontrolStabilnostiLoadList(MainForm Main, List<KontrolStabilnosti> Stabilnosti = null)
        {
            Main.GSOGridView.FocusedRowChanged -= Main.GSOGridView_FocusedRowChanged;
            Main.GSOGrid.BeginUpdate();
            try
            {
                ClearGrid(Main);
                if (Stabilnosti == null) Stabilnosti = new List<KontrolStabilnosti>();
                Main.GSOGrid.DataSource = Stabilnosti;
            }
            finally
            {
                Main.GSOGrid.EndUpdate();
            }
            Main.GSOGridView.Columns[0].Visible = false;
            Main.GSOGridView.Columns[1].Visible = false;
            Main.GSOGridView.Columns[7].Visible = false;
            Main.GSOGridView.Columns[8].Visible = false;
            Main.GSOGridView.Columns[9].Visible = false;
            Main.GSOGridView.Columns[10].Visible = false;
            Main.GSOGridView.Columns[2].Caption = "№";
            Main.GSOGridView.Columns[3].Caption = "Дата создания";
            Main.GSOGridView.Columns[4].Caption = "Дата закрытия";
            Main.GSOGridView.Columns[5].Caption = "Количество измерений";
            Main.GSOGridView.Columns[6].Caption = "Аттестованное значение";
            Main.GSOGridView.Columns[3].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            Main.GSOGridView.Columns[3].DisplayFormat.FormatString = "dd/MM/yyyy";
            Main.GSOGridView.Columns[4].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            Main.GSOGridView.Columns[4].DisplayFormat.FormatString = "dd/MM/yyyy";
            Main.GSOGridView.OptionsView.RowAutoHeight = true;
            Main.GSOGridView.OptionsView.ColumnAutoWidth = true;
            Main.GSOGridView.OptionsView.ShowColumnHeaders = true;
            Main.GSOGridView.OptionsView.ShowBands = false;
            //Wrap Text
            DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
            MemoEdit.WordWrap = true;
            Main.GSOGridView.OptionsView.AllowCellMerge = UserSettings.VLKStabilnostCellMerge;
            Main.GSOGridView.OptionsView.ShowColumnHeaders = true;
            foreach (GridColumn My_GridColumn in Main.GSOGridView.Columns)
            {
                My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                My_GridColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                My_GridColumn.ColumnEdit = MemoEdit;
            }

            ColumnView View = Main.GSOGridView;
            GridColumn column = View.Columns["Id"];
            if (column != null)
            {
                int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId.GetRowID(Main.GSOSpisokTab.SelectedTabPage.Name));
                if (rhFound != GridControl.InvalidRowHandle)
                {
                    View.FocusedRowHandle = rhFound;
                    View.FocusedColumn = column;
                }
            }
            Main.GSOGridView.FocusedRowChanged += Main.GSOGridView_FocusedRowChanged;
        }

        public void GetVLK(MainForm Main)
        {
            try
            {
                Main.VLKCellMergeButton.Visible = false;
                Main.AddAnalizProbiButton.Visible = UserSettings.VLKAnalizAdd;
                Main.VLKPrintButton.Visible = false;
                Main.DiagramButton.Visible = false;
                Main.FiltrButton.Visible = false;
                new Thread(GetVLKThread).Start(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void GetVLKThread(object MainF)
        {
            var Main = MainF as MainForm;
            try
            {
                MF = Main;
                using (DbDataContext db = new DbDataContext())
                {
                    var ВсеЗначения = new List<Analiz>();
                    try
                    {
                        if (!UserSettings.Developer)
                        {
                            ВсеЗначения = db.Analiz.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0).OrderBy(c => c.НазваниеМетодаАнализа).ToList();
                        }
                        else ВсеЗначения = db.Analiz.OrderBy(c => c.НазваниеМетодаАнализа).ToList();
                    }
                    catch
                    {
                        LogErrors Log = new LogErrors("Ошибка соединения с сервером - VLK");
                        if (KolvoZaprosov == 3) Main.Invoke(new System.Action(() => MessageBox.Show("Сервер не отвечает!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    }
                    List<AnalizVLK> AnalizMain = new List<AnalizVLK>();
                    if (ВсеЗначения != null && ВсеЗначения.Count() > 0)
                    {
                        foreach (var i in ВсеЗначения)
                        {
                            AnalizMain.Add(new AnalizVLK
                            {
                                Id = i.Id,
                                Анализ = i.НД + " " + i.НазваниеМетодаАнализа,
                            });
                        }
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        GetVLKLoadList(Main, AnalizMain);
                    }));
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MF.Invoke(new System.Action(() => MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
            }
            finally
            {
                try
                {
                    DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    if ((gv.GetRow(0) == null || gv.GetRow(0).GetType().Name != "AnalizVLK") && KolvoZaprosov < 3)
                    {
                        new Thread(GetVLKThread).Start(MainF);
                        KolvoZaprosov++;
                    }
                }
                catch { }
            }
        }

        private void GetVLKLoadList(MainForm Main, List<AnalizVLK> AnalizMain = null)
        {
            Main.GSOGridView.FocusedRowChanged -= Main.GSOGridView_FocusedRowChanged;
            Main.GSOGrid.BeginUpdate();
            try
            {
                ClearGrid(Main);
                if (AnalizMain == null) AnalizMain = new List<AnalizVLK>();
                Main.GSOGrid.DataSource = AnalizMain;
            }
            finally
            {
                Main.GSOGrid.EndUpdate();
            }
            Main.GSOGridView.Columns[0].Visible = false;
            Main.GSOGridView.OptionsView.ShowColumnHeaders = false;
            Main.GSOGridView.OptionsView.AllowCellMerge = false;
            //Wrap Text
            DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
            MemoEdit.WordWrap = true;
            Main.GSOGridView.OptionsView.RowAutoHeight = true;
            foreach (GridColumn My_GridColumn in Main.GSOGridView.Columns)
            {
                My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                My_GridColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
                My_GridColumn.ColumnEdit = MemoEdit;
            }

            ColumnView View = Main.GSOGridView;
            GridColumn column = View.Columns["Id"];
            if (column != null)
            {
                int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId.GetRowID(Main.GSOSpisokTab.SelectedTabPage.Name));
                if (rhFound != GridControl.InvalidRowHandle)
                {
                    View.FocusedRowHandle = rhFound;
                    View.FocusedColumn = column;
                }
            }
            Main.GSOGridView.FocusedRowChanged += Main.GSOGridView_FocusedRowChanged;
        }

        private static void ClearGrid(MainForm Main, bool CreateEmptyBand = true)
        {
            Main.GSOGridView.ColumnFilterChanged -= Main.GSOGridView_ColumnFilterChanged;
            Main.GSOGridView.Columns.Clear();
            Main.GSOGridView.Bands.Clear();
            if (CreateEmptyBand)
            {
                Main.GSOGridView.Bands.Add(new GridBand());
            }
            Main.GSOGrid.DataSource = null;
            Main.GSOGridView.OptionsView.RowAutoHeight = true;
            Main.GSOGridView.OptionsView.ColumnAutoWidth = true;
            Main.GSOGridView.OptionsView.ShowBands = !CreateEmptyBand;
            Main.GSOGridView.OptionsView.AllowCellMerge = true;
            Main.GSOGridView.ColumnFilterChanged += Main.GSOGridView_ColumnFilterChanged;
        }

        internal void AddAnalizButton_Click(MainForm Main)
        {
            AddAnalizForm f = new AddAnalizForm(Main);
            f.Owner = Main;
            f.ShowDialog();
            UpdateGSOGrid(Main);
            f.Dispose();
        }

        internal void AddProbiButton_Click(MainForm Main)
        {
            var AnalizID = GetAnalizID(Main);
            AddProbiForm f = new AddProbiForm(Main, AnalizID);
            f.Owner = Main;
            f.ShowDialog();
            UpdateGSOGrid(Main);
            f.Dispose();
        }

        private int GetAnalizID(MainForm Main)
        {
            try
            {
                var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');
                if (Names.Length == 1)
                {
                    return -1;
                }
                else return Convert.ToInt32(Names[1]);
            }
            catch { return -1; }
        }

        public void AnalizDeleteButton_Click(MainForm Main, int DelAnalizID)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var DelAnaliz = db.Analiz.Where(c => c.Id == DelAnalizID).FirstOrDefault();
                    DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить \"" + DelAnaliz.НД + " " + DelAnaliz.НазваниеМетодаАнализа + "\"?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        try
                        {

                            var Строка3 = db.Analiz.Where(c => c.Id == DelAnaliz.Id).FirstOrDefault();
                            if (Строка3 != null)
                            {
                                Строка3.UserName = UserSettings.User;
                                Строка3.ДатаСозданияЗаписи = DateTime.Now;
                                Строка3.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                db.SubmitChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                            {
                                try
                                {
                                    var Строка3 = db.Analiz.Where(c => c.Id == DelAnaliz.Id).FirstOrDefault();
                                    if (Строка3 != null)
                                    {
                                        Строка3.UserName = UserSettings.User;
                                        Строка3.ДатаСозданияЗаписи = DateTime.Now;
                                        Строка3.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                        db.SubmitChanges();
                                    }
                                }
                                catch (ChangeConflictException)
                                {
                                    LogErrors Log2 = new LogErrors(ex.ToString());
                                    MessageBox.Show("Конфликт при сохранении!\nПопробуйте повторить операцию.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                    return;
                                }
                            }
                            LogErrors Log = new LogErrors(ex.ToString());
                        }

                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        var StrokeFocus = (AnalizVLK)gv.GetRow(gv.FocusedRowHandle - 1);
                        if (StrokeFocus != null)
                            SelectedRowId.AddNewRowID(Main.GSOSpisokTab.SelectedTabPage.Name, StrokeFocus.Id.ToString());
                        UpdateGSOGrid(Main);
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        return;
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void AnalizEditClick(MainForm Main, int AnalizId)
        {
            AddAnalizForm f = new AddAnalizForm(Main, AnalizId);
            f.Text = "Изменить";
            f.Owner = Main;
            f.ShowDialog();
            UpdateGSOGrid(Main);
            f.Dispose();
        }

        internal void GSOGrid_DoubleClick(MainForm Main)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');
                if (Names.Length == 1)
                {
                    switch (Main.GSOSpisokTab.SelectedTabPage.Name)
                    {
                        case "MainGSOTab":
                            var Stroke = (AnalizVLK)gv.GetRow(gv.FocusedRowHandle);
                            if (Stroke != null)
                            {
                                using (DbDataContext db = new DbDataContext())
                                {
                                    var Analiz = db.Analiz.Where(c => c.Id == Stroke.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                                    if (UserSettings.VLKProbi)
                                    {
                                        var NaME = "Пробы" + "&" + Stroke.Id;
                                        var Text2 = Analiz.ShortName.Length > 40 ? Analiz.ShortName.Substring(0, 40) + "..." : Analiz.ShortName;
                                        AddTab(Main, "Шифрование " + "\"" + Text2 + "\"", NaME);
                                    }
                                    if (UserSettings.VLKOperaivnii)
                                    {
                                        var NaME2 = "Оперативный" + "&" + Analiz.Id;
                                        AddTab(Main, Analiz.ShortName.Length > 40 ? "Оперативный контроль \"" + Analiz.ShortName.Substring(0, 40) + "...\"" : "Оперативный контроль \"" + Analiz.ShortName + "\"", NaME2, false);
                                    }
                                    if (UserSettings.VLKStabilnosti)
                                    {
                                        var NaME3 = "Стабильности" + "&" + Analiz.Id;
                                        AddTab(Main, Analiz.ShortName.Length > 40 ? "Контроль стабильности \"" + Analiz.ShortName.Substring(0, 40) + "...\"" : "Контроль стабильности \"" + Analiz.ShortName + "\"", NaME3, false);
                                    }
                                    db.Dispose();
                                }
                            }
                            break;
                    }
                }
                else if ((Names[0] == "Пробы" || Names[0] == "ПробыОперативный") && UserSettings.VLKProbiEdit)
                {
                    var Stroke = (Probi)gv.GetRow(gv.FocusedRowHandle);
                    var AnalizID = GetAnalizID(Main);
                    AddProbiForm f = new AddProbiForm(Main, Stroke);
                    f.Owner = Main;
                    f.ShowDialog();
                    UpdateGSOGrid(Main);
                    f.Dispose();
                }
                else if (Names[0] == "ПробыСтабильности" && UserSettings.VLKProbiEdit)
                {
                    var Stroka = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle);
                    var Stroke = new Probi();
                    using (DbDataContext db = new DbDataContext())
                    {
                        Stroke = db.Probi.Where(c => c.Id == Stroka.Id).FirstOrDefault();
                        db.Dispose();
                    }
                    var AnalizID = GetAnalizID(Main);
                    AddProbiForm f = new AddProbiForm(Main, Stroke);
                    f.Owner = Main;
                    f.ShowDialog();
                    UpdateGSOGrid(Main);
                    f.Dispose();
                }
                else if (Names[0] == "Оперативный")
                {
                    using (DbDataContext db = new DbDataContext())
                    {
                        var Stroke = (OperativniiKontrol)gv.GetRow(gv.FocusedRowHandle);
                        var Analiz = db.Analiz.Where(c => c.Id == Convert.ToInt32(Names[1])).FirstOrDefault();
                        if (Analiz == null) return;
                        if (Stroke == null) return;
                        var NaME = "ПробыОперативный" + "&" + Names[1] + "&" + Stroke.Id;
                        var Text2 = Analiz.ShortName.Length > 40 ? Analiz.ShortName.Substring(0, 40) + "..." : Analiz.ShortName;
                        AddTab(Main, "Журнал опер.контр. №" + Stroke.Book + " \"" + Text2 + "\"", NaME);
                        db.Dispose();
                    }
                }
                else if (Names[0] == "Стабильности")
                {
                    using (DbDataContext db = new DbDataContext())
                    {
                        var Stroke = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
                        var Analiz = db.Analiz.Where(c => c.Id == Convert.ToInt32(Names[1])).FirstOrDefault();
                        if (Analiz == null) return;
                        if (Stroke == null) return;
                        var NaME = "ПробыСтабильности" + "&" + Names[1] + "&" + Stroke.Id;
                        var Text2 = Analiz.ShortName.Length > 40 ? Analiz.ShortName.Substring(0, 40) + "..." : Analiz.ShortName;
                        AddTab(Main, "Журнал контр. стаб. №" + Stroke.Book + " \"" + Text2 + "\"", NaME);
                        db.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void AddTab(MainForm Main, string Text, string Name, bool select = true)
        {
            var TabPageNew = new XtraTabPage();
            TabPageNew.Name = Name;
            TabPageNew.ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;
            TabPageNew.Text = Text;
            foreach (XtraTabPage pages in Main.GSOSpisokTab.TabPages)
            {
                if (pages.Name == Name)
                {
                    if (select)
                    {
                        Main.GSOSpisokTab.SelectedTabPage = pages;
                        UpdateGSOGrid(Main);
                    }
                    return;
                }
            }
            Main.GSOSpisokTab.TabPages.Add(TabPageNew);
            if (select)
            {
                Main.GSOSpisokTab.SelectedTabPage = TabPageNew;
                UpdateGSOGrid(Main);
            }
        }

        private void GetProbi(MainForm Main, int AnalizID, int VisibleColumnsTip = 0, int OperKontrolId = -1)
        {
            try
            {
                Main.VLKCellMergeButton.Visible = true;
                Main.AddAnalizProbiButton.Visible = UserSettings.VLKProbiAdd;
                Main.VLKPrintButton.Visible = VisibleColumnsTip == 1 ? UserSettings.VLKProbiPrint : UserSettings.VLKOperaivniiPrint;
                Main.DiagramButton.Visible = false;
                Main.FiltrButton.Visible = (VisibleColumnsTip == 1 || VisibleColumnsTip == 2);
                if (VisibleColumnsTip == 1)
                    Main.VLKCellMergeButton.Image = UserSettings.VLKProbiCellMerge ? global::LIMS.Properties.Resources.freezetoprow_32x32 : global::LIMS.Properties.Resources.gridmergecells_32x32;
                else Main.VLKCellMergeButton.Image = UserSettings.VLKOperativniiProbiCellMerge ? global::LIMS.Properties.Resources.freezetoprow_32x32 : global::LIMS.Properties.Resources.gridmergecells_32x32;
                var ObjData = new GSOUpdateClass { Main = Main, AnalizID = AnalizID, ColumnTip = VisibleColumnsTip, OperControlID = OperKontrolId };
                new Thread(GetProbiThread).Start(ObjData);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void GetProbiThread(object ObjData)
        {
            var GSOData = ObjData as GSOUpdateClass;
            var Main = GSOData.Main;
            try
            {
                MF = GSOData.Main;
                using (DbDataContext db = new DbDataContext())
                {
                    List<Probi> ВсеЗначения = null;
                    try
                    {
                        if (!UserSettings.Developer)
                        {
                            if (GSOData.ColumnTip == 1)
                            {
                                ВсеЗначения = db.Probi.Where(c => c.AnalizID == GSOData.AnalizID && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderBy(c => c.Дата).ToList();
                            }
                            else
                            {
                                if (GSOData.ColumnTip == 2)
                                {

                                    ВсеЗначения = db.Probi.Where(c => c.AnalizID == GSOData.AnalizID && c.OperativniiKontrolID == GSOData.OperControlID && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderBy(c => c.Дата).ToList();
                                    foreach (var Stroke in ВсеЗначения)
                                    {
                                        if (Stroke.X1.HasValue && Stroke.X2.HasValue)
                                        {
                                            //VLKStatic.CalculatePredel(Stroke);
                                            //if (!Stroke.НормативКонтроляК.HasValue)
                                            //{
                                            VLKStatic.CalculatePredel(Stroke);
                                            Stroke.НормативКонтроляК = VLKStatic.Al;
                                            //}
                                            if (Stroke.KK.Value <= Stroke.НормативКонтроляК.Value && (-1) * Stroke.KK.Value <= Stroke.НормативКонтроляК.Value)
                                                Stroke.ЗаключениеК = "уд.";
                                            else Stroke.ЗаключениеК = "неуд.";
                                        }
                                    }
                                    db.SubmitChanges();
                                }
                                else
                                {
                                    if (GSOData.ColumnTip == 3)
                                        ВсеЗначения = db.Probi.Where(c => c.AnalizID == GSOData.AnalizID && c.KontrolStabilnostiID == GSOData.OperControlID && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderBy(c => c.Дата).ToList();
                                }
                            }
                        }
                        else //для разработчика
                        {
                            #region
                            if (GSOData.ColumnTip == 1)
                            {
                                ВсеЗначения = db.Probi.Where(c => c.AnalizID == GSOData.AnalizID).OrderBy(c => c.Дата).ToList();
                            }
                            else
                            {
                                if (GSOData.ColumnTip == 2)
                                {
                                    ВсеЗначения = db.Probi.Where(c => c.AnalizID == GSOData.AnalizID && c.OperativniiKontrolID == GSOData.OperControlID).OrderBy(c => c.Дата).ToList();
                                    foreach (var Stroke in ВсеЗначения)
                                    {
                                        if (Stroke.X1.HasValue && Stroke.X2.HasValue)
                                        {
                                            if (!Stroke.НормативКонтроляК.HasValue)
                                            {
                                                VLKStatic.CalculatePredel(Stroke);
                                                Stroke.НормативКонтроляК = VLKStatic.Al;
                                            }
                                            if (Stroke.KK.Value <= Stroke.НормативКонтроляК.Value && (-1) * Stroke.KK.Value <= Stroke.НормативКонтроляК.Value)
                                                Stroke.ЗаключениеК = "уд.";
                                            else Stroke.ЗаключениеК = "неуд.";
                                        }
                                    }
                                    db.SubmitChanges();
                                }
                                else
                                {
                                    if (GSOData.ColumnTip == 3)
                                        ВсеЗначения = db.Probi.Where(c => c.AnalizID == GSOData.AnalizID && c.KontrolStabilnostiID == GSOData.OperControlID).OrderBy(c => c.Дата).ToList();
                                }
                            }
                            if (GSOData.OperControlID == -1) ВсеЗначения = ВсеЗначения.OrderByDescending(c => c.Дата).ToList();
                            #endregion
                        }

                        if (GSOData.OperControlID == -1)
                        {
                            ВсеЗначения = SortTableByShifr(ВсеЗначения, SortOrderProbi == DevExpress.Data.ColumnSortOrder.Descending);
                        }
                        else
                        {
                            ВсеЗначения = SortTableByShifr(ВсеЗначения, SortOrderProbiOperativnii == DevExpress.Data.ColumnSortOrder.Ascending);
                        }
                    }
                    catch
                    {
                        LogErrors Log = new LogErrors("Ошибка соединения с сервером - VLK");
                        if (KolvoZaprosov == 3) Main.Invoke(new System.Action(() => MessageBox.Show("Сервер не отвечает!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        GetProbiLoadList(Main, GSOData.ColumnTip, ВсеЗначения);
                    }));
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MF.Invoke(new System.Action(() => MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
            }
            finally
            {
                if (GSOData.ColumnTip == 1)
                {
                    MF.Invoke(new System.Action(() => SetFiltr(GSOData.Main, GSOData.AnalizID.ToString())));
                }
                else
                {
                    MF.Invoke(new System.Action(() => SetFiltr(GSOData.Main, GSOData.AnalizID.ToString(), GSOData.OperControlID)));
                }
                try
                {
                    DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    if ((gv.GetRow(0) == null || gv.GetRow(0).GetType().Name != "Probi") && KolvoZaprosov < 3)
                    {
                        new Thread(GetProbiThread).Start(ObjData);
                        KolvoZaprosov++;
                    }
                }
                catch { }
            }
        }

        private List<Probi> SortTableByShifr(List<Probi> всеЗначения, bool turn)
        {
            try
            {
                var SortedList = new List<Probi>();
                var number = new List<int>();
                var Year = new List<int>();
                foreach (var probi in всеЗначения)
                {
                    try
                    {
                        var Names = probi.Шифр.Split('-');
                        if (!number.Contains(Convert.ToInt32(Names[1]))) number.Add(Convert.ToInt32(Names[1]));
                        if (!Year.Contains(Convert.ToInt32(Names[2]))) Year.Add(Convert.ToInt32(Names[2]));
                    }
                    catch (Exception ex) { LogErrors Log = new LogErrors(ex.ToString()); }
                }
                if (turn)
                {
                    number = number.OrderByDescending(p => p).ToList();
                    Year = Year.OrderByDescending(p => p).ToList();
                }
                else
                {
                    number = number.OrderBy(p => p).ToList();
                    Year = Year.OrderBy(p => p).ToList();
                }
                try
                {
                    foreach (var Y in Year)
                    {
                        foreach (var N in number)
                        {
                            for (int i = всеЗначения.Count - 1; i >= 0; i--)
                            {
                                var Names = всеЗначения[i].Шифр.Split('-');
                                if (Names[1] == N.ToString() && Names[2] == Y.ToString())
                                {
                                    SortedList.Add(всеЗначения[i]);
                                    всеЗначения.Remove(всеЗначения[i]);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogErrors Log = new LogErrors(ex.ToString());
                }

                if (всеЗначения.Count > 0)
                {
                    foreach (var LIST in всеЗначения)
                    {
                        SortedList.Add(LIST);
                    }
                }
                return SortedList;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                return всеЗначения;
            }
        }

        private void GetProbiLoadList(MainForm Main, int GSODataColumnTip, List<Probi> ВсеЗначения = null)
        {
            Main.GSOGridView.ColumnFilterChanged -= Main.GSOGridView_ColumnFilterChanged;
            Main.GSOGridView.FocusedRowChanged -= Main.GSOGridView_FocusedRowChanged;
            Main.GSOGrid.BeginUpdate();
            try
            {
                ClearGrid(Main);
                if (ВсеЗначения == null) ВсеЗначения = new List<Probi>();
                Main.GSOGrid.DataSource = ВсеЗначения;
            }
            finally
            {
                Main.GSOGrid.EndUpdate();
            }
            Main.GSOGridView.Columns[0].Visible = false;
            Main.GSOGridView.Columns[15].Visible = false;
            Main.GSOGridView.Columns[16].Visible = false;
            Main.GSOGridView.Columns[17].Visible = false;
            Main.GSOGridView.Columns[18].Visible = false;
            Main.GSOGridView.Columns[19].Visible = false;
            Main.GSOGridView.Columns[20].Visible = false;
            Main.GSOGridView.Columns[21].Visible = false;
            Main.GSOGridView.Columns[22].Visible = false;
            Main.GSOGridView.Columns[23].Visible = false;
            Main.GSOGridView.Columns[24].Visible = false;
            Main.GSOGridView.Columns[1].Caption = "Дата";
            Main.GSOGridView.Columns[2].Caption = "Исполнитель";
            Main.GSOGridView.Columns[3].Caption = "Шифр пробы, анализируемой для целей контроля";
            Main.GSOGridView.Columns[4].Caption = "Контролируемый объект";
            Main.GSOGridView.Columns[5].Caption = "Определяемый компонент с указанием НД на методику анализа";
            Main.GSOGridView.Columns[6].Caption = "Аттестованное значение ОК, С";
            Main.GSOGridView.Columns[7].Caption = "Лицо, выдавшее пробу (должность, ФИО)";
            Main.GSOGridView.Columns[8].Caption = "Лицо, получившее пробу (должность, ФИО)";
            Main.GSOGridView.Columns[9].Caption = "Результат анализа Х1";
            Main.GSOGridView.Columns[10].Caption = "Результат анализа Х2";
            Main.GSOGridView.Columns[11].Caption = "Результат анализа X";
            Main.GSOGridView.Columns[12].Caption = "Оценка повторяемости r";
            Main.GSOGridView.Columns[13].Caption = "Оценка внутрилабораторной прецизионнности R";
            Main.GSOGridView.Columns[14].Caption = "Оценка погрешности K";
            Main.GSOGridView.Columns[16].Visible = false;
            if (GSODataColumnTip == 1)
            {
                Main.GSOGridView.Columns[9].Visible = false;    //X1
                Main.GSOGridView.Columns[10].Visible = false;   //X2
                Main.GSOGridView.Columns[11].Visible = false;   //X
                Main.GSOGridView.Columns[12].Visible = false;
                Main.GSOGridView.Columns[13].Visible = false;
                Main.GSOGridView.Columns[14].Visible = false;
                Main.GSOGridView.OptionsView.AllowCellMerge = UserSettings.VLKProbiCellMerge;
            }
            else if (GSODataColumnTip == 2)
            {
                Main.GSOGridView.Columns[7].Visible = false;
                Main.GSOGridView.Columns[8].Visible = false;
                Main.GSOGridView.Columns[9].Visible = false;
                Main.GSOGridView.Columns[10].Visible = false;
                Main.GSOGridView.Columns[12].Visible = false;
                Main.GSOGridView.Columns[13].Visible = false;
                Main.GSOGridView.Columns[15].Visible = true;
                Main.GSOGridView.Columns[16].Visible = true;
                Main.GSOGridView.Columns[11].Caption = "Результат контрольного измерения X";
                Main.GSOGridView.Columns[14].Caption = "Оценка погрешности K=X-C";
                Main.GSOGridView.Columns[15].Caption = "Норматив контроля K";
                Main.GSOGridView.Columns[16].Caption = "Заключение";
                var VIndex = Main.GSOGridView.Columns[6].VisibleIndex;
                Main.GSOGridView.Columns[6].VisibleIndex = Main.GSOGridView.Columns[11].VisibleIndex;
                Main.GSOGridView.Columns[11].VisibleIndex = VIndex;
                Main.GSOGridView.OptionsView.AllowCellMerge = UserSettings.VLKOperativniiProbiCellMerge;
            }
            Main.GSOGridView.Columns[1].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            Main.GSOGridView.Columns[1].DisplayFormat.FormatString = "dd/MM/yyyy";

            Main.GSOGridView.OptionsView.ShowColumnHeaders = true;
            Main.GSOGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;

            //Wrap Text
            DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
            MemoEdit.WordWrap = true;
            MemoEdit.AutoHeight = true;
            Main.GSOGridView.OptionsView.RowAutoHeight = true;
            foreach (GridColumn My_GridColumn in Main.GSOGridView.Columns)
            {
                My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                My_GridColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                My_GridColumn.ColumnEdit = MemoEdit;
            }

            ColumnView View = Main.GSOGridView;
            GridColumn column = View.Columns["Id"];
            if (column != null)
            {
                int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId.GetRowID(Main.GSOSpisokTab.SelectedTabPage.Name));
                if (rhFound != GridControl.InvalidRowHandle)
                {
                    View.FocusedRowHandle = rhFound;
                    View.FocusedColumn = column;
                }
            }
            Main.GSOGridView.FocusedRowChanged += Main.GSOGridView_FocusedRowChanged;
        }

        private void GetProbiStabilnosti(MainForm Main, int AnalizID, int OperKontrolId = -1)
        {
            try
            {
                Main.VLKCellMergeButton.Image = UserSettings.VLKStabilnostProbiCellMerge ? global::LIMS.Properties.Resources.freezetoprow_32x32 : global::LIMS.Properties.Resources.gridmergecells_32x32;
                Main.VLKCellMergeButton.Visible = true;
                Main.AddAnalizProbiButton.Visible = UserSettings.VLKProbiAdd;
                Main.VLKPrintButton.Visible = UserSettings.VLKStabilnostiPrint || UserSettings.VLKStabilnostiProtokolOchenka;
                Main.DiagramButton.Visible = UserSettings.VLKStabilnostiDiagrams;
                Main.FiltrButton.Visible = false;
                var ObjData = new GSOUpdateClass { Main = Main, AnalizID = AnalizID, ColumnTip = 0, OperControlID = OperKontrolId };
                new Thread(GetProbiStabilnostiThread).Start(ObjData);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void GetProbiStabilnostiThread(object ObjData)
        {
            var GSOData = ObjData as GSOUpdateClass;
            MF = GSOData.Main;
            var Main = GSOData.Main;
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var ВсеЗначения = new List<Probi>();
                    try
                    {
                        if (!UserSettings.Developer)
                        {
                            ВсеЗначения = db.Probi.Where(c => c.AnalizID == GSOData.AnalizID && c.KontrolStabilnostiID == GSOData.OperControlID && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderBy(c => c.Дата).ToList();
                        }
                        else
                        {
                            ВсеЗначения = db.Probi.Where(c => c.AnalizID == GSOData.AnalizID && c.KontrolStabilnostiID == GSOData.OperControlID).OrderBy(c => c.Дата).ToList();
                        }
                    }
                    catch
                    {
                        LogErrors Log = new LogErrors("Ошибка соединения с сервером - VLK");
                        if (KolvoZaprosov == 3) Main.Invoke(new System.Action(() => MessageBox.Show("Сервер не отвечает!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    }

                    ВсеЗначения = SortTableByShifr(ВсеЗначения, SortOrderProbiStabilnosti == DevExpress.Data.ColumnSortOrder.Ascending);

                    foreach (var str in ВсеЗначения)
                    {
                        if (str.X1.HasValue && str.X2.HasValue)
                        {
                            VLKStatic.CalculatePredel(str);
                            break;
                        }
                    }
                    var StabilnostiModel = ConvertToKontrolStabilnostiModel(db, ВсеЗначения);
                    if (StabilnostiModel == null || StabilnostiModel.Count == 0) return;
                    int Cell = 0;
                    foreach (var Select in StabilnostiModel)
                    {
                        if (!Select.X1.HasValue && !Select.X2.HasValue) continue;
                        Cell++;
                        ////Одна точка вышла за предел действия
                        Select._VivodPovtoriamost = Select._rSmall.Value > VLKStatic.Povt1 ? "cверх предела действия" : "-";
                        Select._VivodPrechizionnost = (Select._RBig.HasValue ? Select._RBig.Value : 0) > VLKStatic.Prech1 ? "cверх предела действия" : "-";
                        Select._VivodTochnost = Math.Abs(Select._KK.Value) > VLKStatic.Pogr1 ? "cверх предела действия" : "-";
                        Select._Rezultat = "-";

                        ////9 точек подряд находятся выше средней линии или  9 точек подряд находятся по одну сторону от средней линии
                        if (Cell > 8)
                        {
                            int ValueNull = 0; //Повторяемость
                            var AddText = true;
                            var Text4Add = "9 точек подряд находятся выше средней линии";
                            for (int k = Cell - 1; k >= Cell - 9; k--)
                            {
                                while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                    ValueNull++;
                                if (k - ValueNull < 0) break;
                            }
                            int NullableStrokes = ValueNull;

                            for (int k = Cell - 9; k < Cell + ValueNull; k++)
                            {
                                if (!StabilnostiModel[k].X1.HasValue && !StabilnostiModel[k].X2.HasValue) continue;
                                if (StabilnostiModel[k]._rSmall.Value <= VLKStatic.Povt3)
                                {
                                    AddText = false;
                                    break;
                                }
                            }
                            if (AddText)
                            {
                                for (int k = Cell - 9; k < Cell; k++)
                                {
                                    while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                        NullableStrokes--;
                                    if (StabilnostiModel[k - NullableStrokes]._rSmall.Value > VLKStatic.Povt3)
                                    {
                                        if (!StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost.Contains(Text4Add))
                                            StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost = StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost + ", " + Text4Add;
                                    }
                                }
                            }

                            if (Cell != StabilnostiModel.Count) //Прецизионность
                            {
                                NullableStrokes = 0;
                                ValueNull = 0;
                                AddText = true;
                                for (int k = Cell - 1; k >= Cell - 9; k--)
                                {
                                    while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                        ValueNull++;
                                    if (k - ValueNull < 0) break;
                                }
                                NullableStrokes = ValueNull;

                                for (int k = Cell - 9; k < Cell + ValueNull; k++)
                                {
                                    if (!StabilnostiModel[k].X1.HasValue && !StabilnostiModel[k].X2.HasValue) continue;
                                    if (StabilnostiModel[k]._RBig.Value <= VLKStatic.Prech3)
                                    {
                                        AddText = false;
                                        break;
                                    }
                                }
                                if (AddText)
                                {

                                    for (int k = Cell - 9; k < Cell; k++)
                                    {
                                        while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                            NullableStrokes--;
                                        if (StabilnostiModel[k - NullableStrokes]._RBig.Value > VLKStatic.Prech3)
                                        {
                                            if (!StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost.Contains(Text4Add))
                                                StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost = StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost + ", " + Text4Add;
                                        }
                                    }
                                }
                            }

                            AddText = true;         //Погрешность
                            NullableStrokes = 0;
                            ValueNull = 0;
                            int simbol = 0;
                            bool? minus = null;
                            for (int k = Cell - 1; k >= Cell - 9; k--)
                            {
                                if (simbol == 0 && StabilnostiModel[k - ValueNull].X1.HasValue && StabilnostiModel[k - ValueNull].X2.HasValue)
                                    simbol = StabilnostiModel[k - ValueNull]._KK.Value > 0 ? 1 : -1;
                                while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                    ValueNull++;
                                if (k - ValueNull < 0) break;
                            }
                            NullableStrokes = ValueNull;

                            for (int k = Cell - 9; k < Cell + ValueNull; k++)
                            {
                                if (!StabilnostiModel[k].X1.HasValue && !StabilnostiModel[k].X2.HasValue) continue;
                                if (StabilnostiModel[k]._KK.Value * simbol <= 0) 
                                {
                                    AddText = false;
                                    break;
                                }
                            }
                            if (AddText)
                            {
                                if(simbol == -1)
                                    Text4Add = "9 точек подряд находятся ниже средней линии";
                                for (int k = Cell - 9; k < Cell; k++)
                                {
                                    while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                        NullableStrokes--;
                                    if (StabilnostiModel[k - NullableStrokes]._KK.Value * simbol > 0)
                                    {
                                        if (!StabilnostiModel[k - NullableStrokes]._VivodTochnost.Contains(Text4Add))
                                            StabilnostiModel[k - NullableStrokes]._VivodTochnost = StabilnostiModel[k - NullableStrokes]._VivodTochnost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodTochnost + ", " + Text4Add;
                                    }
                                }
                            }
                        }

                        //6 возрастающих или убывающих точек подряд
                        if (Cell > 5)                                       //Повторяемость
                        {
                            var Text4Add = "6 возрастающих точек подряд";
                            var AddText = true;
                            int ValueNull = 0;
                            for (int k = Cell - 1; k >= Cell - 6; k--)
                            {
                                while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                    ValueNull++;
                                if (k - ValueNull < 0) break;
                            }
                            int NullableStrokes = ValueNull;
                            double? Value1 = null;
                            double? Value2 = null;
                            for (int k = Cell - 6; k < Cell - 1 + ValueNull; k++)
                            {
                                if (!StabilnostiModel[k].X1.HasValue && !StabilnostiModel[k].X2.HasValue) continue;
                                Value1 = StabilnostiModel[k]._rSmall.Value;
                                int g = 0;
                                while (!StabilnostiModel[k + 1 + g].X1.HasValue && !StabilnostiModel[k + 1 + g].X2.HasValue)
                                    g++;
                                Value2 = StabilnostiModel[k + 1 + g]._rSmall.Value;
                                if (Value1 >= Value2)
                                {
                                    AddText = false;
                                    break;
                                }
                            }
                            if (AddText)
                            {
                                for (int k = Cell - 6; k < Cell; k++)
                                {
                                    while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                        NullableStrokes--;
                                    if (!StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost.Contains(Text4Add))
                                        StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost = StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost + ", " + Text4Add;
                                }
                            }

                            if (Cell != StabilnostiModel.Count)         //Прецизионность
                            {
                                AddText = true;
                                ValueNull = 0;
                                for (int k = Cell - 1; k >= Cell - 6; k--)
                                {
                                    while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                        ValueNull++;
                                    if (k - ValueNull < 0) break;
                                }
                                NullableStrokes = ValueNull;
                                Value1 = null;
                                Value2 = null;

                                for (int k = Cell - 6; k < Cell - 1 + ValueNull; k++)
                                {
                                    if (!StabilnostiModel[k].X1.HasValue && !StabilnostiModel[k].X2.HasValue) continue;
                                    Value1 = StabilnostiModel[k]._RBig.HasValue ? StabilnostiModel[k]._RBig.Value : 0;
                                    int g = 0;
                                    while (!StabilnostiModel[k + 1 + g].X1.HasValue && !StabilnostiModel[k + 1 + g].X2.HasValue)
                                        g++;
                                    Value2 = StabilnostiModel[k + 1 + g]._RBig.HasValue ? StabilnostiModel[k + 1 + g]._RBig.Value : 0;
                                    if (Value1 >= Value2)
                                    {
                                        AddText = false;
                                        break;
                                    }
                                }
                                if (AddText)
                                {
                                    for (int k = Cell - 6; k < Cell; k++)
                                    {
                                        while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                            NullableStrokes--;
                                        if (!StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost.Contains(Text4Add))
                                            StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost = StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost + ", " + Text4Add;
                                    }
                                }
                            }

                            AddText = true;                          //Погрешность
                            ValueNull = 0;
                            bool? Vozrastauchii = null;
                            for (int k = Cell - 1; k >= Cell - 6; k--)
                            {
                                while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                    ValueNull++;
                                if (k - ValueNull < 0) break;
                            }
                            NullableStrokes = ValueNull;
                            Value1 = null;
                            Value2 = null;

                            for (int k = Cell - 6; k < Cell - 1 + ValueNull; k++)
                            {
                                if (!StabilnostiModel[k].X1.HasValue && !StabilnostiModel[k].X2.HasValue) continue;
                                Value1 = StabilnostiModel[k]._KK.Value;
                                int g = 0;
                                while (!StabilnostiModel[k + 1 + g].X1.HasValue && !StabilnostiModel[k + 1 + g].X2.HasValue)
                                    g++;
                                Value2 = StabilnostiModel[k + 1 + g]._KK.Value;

                                if (Value1 <= Value2 && (Vozrastauchii == null || !Vozrastauchii.Value))
                                {
                                    if (Vozrastauchii != null)
                                    {
                                        AddText = false;
                                        break;
                                    }
                                    else
                                        Vozrastauchii = true;
                                }
                                else
                                {
                                    if (Value1 >= Value2 && (Vozrastauchii == null || Vozrastauchii.Value))
                                    {
                                        if (Vozrastauchii != null)
                                        {
                                            AddText = false;
                                            break;
                                        }
                                        else
                                            Vozrastauchii = false;
                                    }
                                }
                            }
                            if (AddText)
                            {
                                if (Vozrastauchii.Value)
                                    Text4Add = "6 возрастающих точек подряд";
                                else
                                    Text4Add = "6 убывающих точек подряд";

                                for (int k = Cell - 6; k < Cell; k++)
                                {
                                    while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                        NullableStrokes--;
                                    if (!StabilnostiModel[k - NullableStrokes]._VivodTochnost.Contains(Text4Add))
                                        StabilnostiModel[k - NullableStrokes]._VivodTochnost = StabilnostiModel[k - NullableStrokes]._VivodTochnost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodTochnost + ", " + Text4Add;
                                }
                            }
                        }

                        //2 из 3 последовательных точек находятся выше предела предупреждения
                        if (Cell > 2)
                        {
                            var Text4Add = "2 из 3 последовательных точек выше предела предупреждения";         //Повторяемость
                            int KolVoTochek = 0;
                            int ValueNull = 0;
                            for (int k = Cell - 1; k >= Cell - 3; k--)
                            {
                                while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                    ValueNull++;
                                if (k - ValueNull < 0) break;
                            }
                            int NullableStrokes = ValueNull;

                            for (int k = Cell - 3; k < Cell + ValueNull; k++)
                            {
                                if (!StabilnostiModel[k].X1.HasValue && !StabilnostiModel[k].X2.HasValue) continue;
                                if (StabilnostiModel[k]._rSmall.Value > VLKStatic.Povt2)
                                {
                                    KolVoTochek++;
                                }
                            }
                            if (KolVoTochek > 1)
                            {
                                for (int k = Cell - 3; k < Cell; k++)
                                {
                                    while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                        NullableStrokes--;
                                    if (StabilnostiModel[k]._rSmall.Value > VLKStatic.Povt2)
                                    {
                                        if (!StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost.Contains(Text4Add))
                                            StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost = StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodPovtoriamost + ", " + Text4Add;
                                    }
                                }
                            }

                            if (Cell != StabilnostiModel.Count)         //Прецизионность
                            {
                                KolVoTochek = 0;
                                ValueNull = 0;
                                for (int k = Cell - 1; k >= Cell - 3; k--)
                                {
                                    while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                        ValueNull++;
                                    if (k - ValueNull < 0) break;
                                }
                                NullableStrokes = ValueNull;

                                for (int k = Cell - 3; k < Cell + ValueNull; k++)
                                {
                                    if (!StabilnostiModel[k].X1.HasValue || !StabilnostiModel[k].X2.HasValue || !StabilnostiModel[k]._RBig.HasValue) continue;
                                    if (StabilnostiModel[k]._RBig.Value > VLKStatic.Prech2)
                                    {
                                        KolVoTochek++;
                                    }
                                }
                                if (KolVoTochek > 1)
                                {
                                    for (int k = Cell - 3; k < Cell; k++)
                                    {
                                        while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                            NullableStrokes--;
                                        if (StabilnostiModel[k]._RBig.Value > VLKStatic.Prech2)
                                        {
                                            if (!StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost.Contains(Text4Add))
                                                StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost = StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost + ", " + Text4Add;
                                        }
                                    }
                                }
                            }

                            KolVoTochek = 0;                      //Погрешность
                            ValueNull = 0;
                            for (int k = Cell - 1; k >= Cell - 3; k--)
                            {
                                while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                    ValueNull++;
                                if (k - ValueNull < 0) break;
                            }
                            NullableStrokes = ValueNull;

                            for (int k = Cell - 3; k < Cell + ValueNull; k++)
                            {
                                if (!StabilnostiModel[k].X1.HasValue || !StabilnostiModel[k].X2.HasValue || !StabilnostiModel[k]._KK.HasValue) continue;
                                if (Math.Abs(StabilnostiModel[k]._KK.Value) > VLKStatic.Pogr2)
                                {
                                    KolVoTochek++;
                                }
                            }
                            if (KolVoTochek > 1)
                            {
                                for (int k = Cell - 3; k < Cell; k++)
                                {
                                    while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                        NullableStrokes--;

                                    if (Math.Abs(StabilnostiModel[k]._KK.Value) > VLKStatic.Pogr2)
                                    {
                                        if (!StabilnostiModel[k - NullableStrokes]._VivodTochnost.Contains(Text4Add))
                                            StabilnostiModel[k - NullableStrokes]._VivodTochnost = StabilnostiModel[k - NullableStrokes]._VivodTochnost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodTochnost + ", " + Text4Add;
                                    }
                                }
                            }
                        }

                        //4 из 5 последовательных точек находятся выше половинной границы верхней или нижней зоны предупреждения
                        if (Cell > 4)
                        {
                            var Text4Add = "4 из 5 последовательных точек выше половинной границы зоны предупреждения";
                            int ValueNull = 0;
                            int KolVoTochek = 0;
                            for (int k = Cell - 1; k >= Cell - 5; k--)                                        //Повторяемость
                            {
                                while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                    ValueNull++;
                                if (k - ValueNull < 0) break;
                            }
                            int NullableStrokes = ValueNull;

                            for (int k = Cell - 5; k < Cell + ValueNull; k++)
                            {
                                if (!StabilnostiModel[k].X1.HasValue && !StabilnostiModel[k].X2.HasValue) continue;
                                if (StabilnostiModel[k]._rSmall.Value > VLKStatic.Povt3 + (VLKStatic.Povt2 - VLKStatic.Povt3) / 2)
                                {
                                    KolVoTochek++;
                                }
                            }
                            if (KolVoTochek > 3)
                            {
                                for (int k = Cell - 5; k < Cell; k++)
                                {
                                    while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                        NullableStrokes--;
                                    if (StabilnostiModel[k - NullableStrokes]._rSmall.Value > VLKStatic.Povt3 + (VLKStatic.Povt2 - VLKStatic.Povt3) / 2)
                                    {
                                        if (!StabilnostiModel[k - NullableStrokes]._VivodTochnost.Contains(Text4Add))
                                            StabilnostiModel[k - NullableStrokes]._VivodTochnost = StabilnostiModel[k - NullableStrokes]._VivodTochnost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodTochnost + ", " + Text4Add;
                                    }
                                }
                            }

                            if (Cell != StabilnostiModel.Count)                     //Прецизионность
                            {
                                KolVoTochek = 0;
                                ValueNull = 0;
                                for (int k = Cell - 1; k >= Cell - 5; k--)
                                {
                                    while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                        ValueNull++;
                                    if (k - ValueNull < 0) break;
                                }
                                NullableStrokes = ValueNull;

                                for (int k = Cell - 5; k < Cell + ValueNull; k++)
                                {
                                    if (!StabilnostiModel[k].X1.HasValue || !StabilnostiModel[k].X2.HasValue || !StabilnostiModel[k]._RBig.HasValue) continue;
                                    if (StabilnostiModel[k]._RBig.Value > VLKStatic.Prech3 + (VLKStatic.Prech2 - VLKStatic.Prech3) / 2)
                                    {
                                        KolVoTochek++;
                                    }
                                }
                                if (KolVoTochek > 3)
                                {
                                    for (int k = Cell - 5; k < Cell; k++)
                                    {
                                        while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                            NullableStrokes--;
                                        if (StabilnostiModel[k - NullableStrokes]._RBig.Value > VLKStatic.Prech3 + (VLKStatic.Prech2 - VLKStatic.Prech3) / 2)
                                        {
                                            if (!StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost.Contains(Text4Add))
                                                StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost = StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodPrechizionnost + ", " + Text4Add;
                                        }
                                    }
                                }
                            }

                            KolVoTochek = 0;
                            ValueNull = 0;
                            for (int k = Cell - 1; k >= Cell - 5; k--)                   //Погрешность
                            {
                                while (k - ValueNull >= 0 && !StabilnostiModel[k - ValueNull].X1.HasValue && !StabilnostiModel[k - ValueNull].X2.HasValue)
                                    ValueNull++;
                                if (k - ValueNull < 0) break;
                            }
                            NullableStrokes = ValueNull;

                            for (int k = Cell - 5; k < Cell + ValueNull; k++)
                            {
                                if (!StabilnostiModel[k].X1.HasValue && !StabilnostiModel[k].X2.HasValue) continue;
                                if (Math.Abs(StabilnostiModel[k]._KK.Value) > VLKStatic.Prech2 / 2)
                                {
                                    KolVoTochek++;
                                }
                            }
                            if (KolVoTochek > 3)
                            {
                                for (int k = Cell - 5; k < Cell; k++)
                                {
                                    while (!StabilnostiModel[k - NullableStrokes].X1.HasValue && !StabilnostiModel[k - NullableStrokes].X2.HasValue)
                                        NullableStrokes--;
                                    if (Math.Abs(StabilnostiModel[k - NullableStrokes]._KK.Value) > VLKStatic.Prech2 / 2)
                                    {
                                        if (!StabilnostiModel[k - NullableStrokes]._VivodTochnost.Contains(Text4Add))
                                            StabilnostiModel[k - NullableStrokes]._VivodTochnost = StabilnostiModel[k - NullableStrokes]._VivodTochnost == "-" ? Text4Add : StabilnostiModel[k - NullableStrokes]._VivodTochnost + ", " + Text4Add;
                                    }
                                }
                            }
                        }
                    }

                    Main.Invoke(new System.Action(() =>
                    {
                        GetProbiStabilnostiLoadList(Main, StabilnostiModel);
                    }));
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                //Main.Invoke(new System.Action(() => MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));                
            }
            finally
            {
                try
                {
                    DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    if ((gv.GetRow(0) == null || gv.GetRow(0).GetType().Name != "KontrolStabilnostiModel") && KolvoZaprosov < 3)
                    {
                        new Thread(GetProbiStabilnostiThread).Start(ObjData);
                        KolvoZaprosov++;
                    }
                }
                catch { }
            }
        }

        private void GetProbiStabilnostiLoadList(MainForm Main, List<KontrolStabilnostiModel> StabilnostiModel = null)
        {
            Main.GSOGridView.ColumnFilterChanged -= Main.GSOGridView_ColumnFilterChanged;
            Main.GSOGridView.FocusedRowChanged -= Main.GSOGridView_FocusedRowChanged;
            Main.GSOGrid.BeginUpdate();
            try
            {
                ClearGrid(Main, false);
                if (StabilnostiModel == null) StabilnostiModel = new List<KontrolStabilnostiModel>();
                Main.GSOGrid.DataSource = StabilnostiModel;
            }
            finally
            {
                Main.GSOGrid.EndUpdate();
            }
            Main.GSOGridView.Columns[0].Visible = false;
            Main.GSOGridView.Columns[1].Caption = "№";
            Main.GSOGridView.Columns[1].Visible = true;
            Main.GSOGridView.Columns[2].Caption = "Дата";
            Main.GSOGridView.Columns[2].Visible = true;
            Main.GSOGridView.Columns[3].Caption = "Шифр пробы";
            Main.GSOGridView.Columns[3].Visible = true;
            Main.GSOGridView.Columns[4].Caption = "1-го";
            Main.GSOGridView.Columns[4].Visible = true;
            Main.GSOGridView.Columns[5].Caption = "2-го";
            Main.GSOGridView.Columns[5].Visible = true;
            Main.GSOGridView.Columns[6].Caption = "Результат контрольного измерения X";
            Main.GSOGridView.Columns[6].Visible = true;
            Main.GSOGridView.Columns[7].Caption = "Аттестованное значение ОК, С";
            Main.GSOGridView.Columns[7].Visible = true;
            Main.GSOGridView.Columns[8].Caption = "для контроля повторяемости";
            Main.GSOGridView.Columns[8].Visible = true;
            Main.GSOGridView.Columns[9].Caption = "для контроля внутрилабораторной прецизионности";
            Main.GSOGridView.Columns[9].Visible = true;
            Main.GSOGridView.Columns[10].Caption = "для контроля точности";
            Main.GSOGridView.Columns[10].Visible = true;
            Main.GSOGridView.Columns[11].Caption = "для контроля повторяемости";
            Main.GSOGridView.Columns[11].Visible = true;
            Main.GSOGridView.Columns[12].Caption = "для контроля внутрилабораторной прецизионности";
            Main.GSOGridView.Columns[12].Visible = true;
            Main.GSOGridView.Columns[13].Caption = "для контроля точности";
            Main.GSOGridView.Columns[13].Visible = true;
            Main.GSOGridView.Columns[14].Caption = "Результат интерпретации данных контрольных карт, требующие корректирующих/ предупреждающих действий"; //с целью обеспечить стабильность процедуры анализа рабочих проб";
            Main.GSOGridView.Columns[14].Visible = true;
            Main.GSOGridView.Columns[14].MinWidth = 200;
            Main.GSOGridView.Columns[15].Visible = false;
            Main.GSOGridView.Columns[16].Visible = false;
            Main.GSOGridView.Columns[17].Visible = false;
            Main.GSOGridView.Columns[2].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            Main.GSOGridView.Columns[2].DisplayFormat.FormatString = "dd/MM/yyyy";
            Main.GSOGridView.OptionsView.ShowColumnHeaders = false;
            Main.GSOGridView.OptionsView.AllowCellMerge = UserSettings.VLKStabilnostProbiCellMerge; // объединяет похожие строки
                                                                                                    //Wrap Text
            DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
            MemoEdit.WordWrap = true;
            Main.GSOGridView.OptionsView.RowAutoHeight = true;
            foreach (GridColumn My_GridColumn in Main.GSOGridView.Columns)
            {
                My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                My_GridColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                My_GridColumn.ColumnEdit = MemoEdit;
            }
            foreach (BandedGridColumn Colum in Main.GSOGridView.Columns)
            {
                if (!Colum.Visible || Colum.AbsoluteIndex == 5 || Colum.AbsoluteIndex == 9 || Colum.AbsoluteIndex == 10 || Colum.AbsoluteIndex == 12 || Colum.AbsoluteIndex == 13) continue;

                var Band = new GridBand();
                if (Colum.AbsoluteIndex == 4)
                {
                    Band.Caption = "Результат контрольного определения";
                    SetTextOptions(Band, false);
                    var Band1 = new GridBand();
                    var Band2 = new GridBand();
                    Band1.Columns.Add(Main.GSOGridView.Columns[4]);
                    Band2.Columns.Add(Main.GSOGridView.Columns[5]);
                    SetTextOptions(Band1);
                    SetTextOptions(Band2);
                    Band.Children.Add(Band1);
                    Band.Children.Add(Band2);
                }
                else
                {
                    if (Colum.AbsoluteIndex == 8)
                    {
                        Band.Caption = "Результаты контрольной процедуры";
                        SetTextOptions(Band, false);
                        var Band1 = new GridBand();
                        var Band2 = new GridBand();
                        var Band3 = new GridBand();
                        Band1.Columns.Add(Main.GSOGridView.Columns[8]);
                        Band2.Columns.Add(Main.GSOGridView.Columns[9]);
                        Band3.Columns.Add(Main.GSOGridView.Columns[10]);
                        SetTextOptions(Band1);
                        SetTextOptions(Band2);
                        SetTextOptions(Band3);
                        Band.Children.Add(Band1);
                        Band.Children.Add(Band2);
                        Band.Children.Add(Band3);
                    }
                    else
                    {
                        if (Colum.AbsoluteIndex == 11)
                        {
                            Band.Caption = "Выводы о несоответствии результата контрольной процедуры пределу действия или предупреждения";
                            SetTextOptions(Band, false);
                            var Band1 = new GridBand();
                            var Band2 = new GridBand();
                            var Band3 = new GridBand();
                            Band1.Columns.Add(Main.GSOGridView.Columns[11]);
                            Band2.Columns.Add(Main.GSOGridView.Columns[12]);
                            Band3.Columns.Add(Main.GSOGridView.Columns[13]);
                            SetTextOptions(Band1);
                            SetTextOptions(Band2);
                            SetTextOptions(Band3);
                            Band.Children.Add(Band1);
                            Band.Children.Add(Band2);
                            Band.Children.Add(Band3);
                        }
                        else
                        {
                            Band.Columns.Add(Colum);
                            SetTextOptions(Band);
                        }
                    }
                }
                Main.GSOGridView.Bands.Add(Band);
            }

            ColumnView View = Main.GSOGridView;
            GridColumn column = View.Columns["Id"];
            if (column != null)
            {
                int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId.GetRowID(Main.GSOSpisokTab.SelectedTabPage.Name));
                if (rhFound != GridControl.InvalidRowHandle)
                {
                    View.FocusedRowHandle = rhFound;
                    View.FocusedColumn = column;
                }
            }
            Main.GSOGridView.FocusedRowChanged += Main.GSOGridView_FocusedRowChanged;
        }

        private static void SetTextOptions(GridBand Band, bool SetCaption = true)
        {
            Band.AppearanceHeader.Options.UseTextOptions = true;
            Band.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            Band.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            Band.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            if (SetCaption) Band.Caption = Band.Columns[0].Caption;
        }

        private List<KontrolStabilnostiModel> ConvertToKontrolStabilnostiModel(DbDataContext db, List<Probi> всеЗначения)
        {
            var StabilnostModel = new List<KontrolStabilnostiModel>();
            int i = 0;
            foreach (var Stroke in всеЗначения)
            {
                i++;
                StabilnostModel.Add(new KontrolStabilnostiModel
                {
                    _N = i,
                    Id = Stroke.Id,
                    _AnalizID = Stroke.AnalizID,
                    _KontrolStabilnostiID = Stroke.KontrolStabilnostiID,
                    _KK = Stroke.KK,
                    _OperativniiKontrolID = Stroke.OperativniiKontrolID,
                    _RBig = Stroke.RBig,
                    _rSmall = Stroke.rSmall,
                    X = Stroke.X,
                    X1 = Stroke.X1,
                    X2 = Stroke.X2,
                    //NewX = NewClassX,
                    _АттестованноеЗначение = Stroke.АттестованноеЗначение,
                    _Дата = Stroke.Дата,
                    _Шифр = Stroke.Шифр,
                    _VivodPovtoriamost = "",
                    _VivodPrechizionnost = "",
                    _VivodTochnost = "",
                    _Rezultat = ""
                });
            }
            return StabilnostModel;
        }

        internal void ProbiEditClick(MainForm Main)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = new Probi();
            try
            {
                Stroke = (Probi)gv.GetRow(gv.FocusedRowHandle);
            }
            catch
            {
                try
                {
                    var Stroka = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle);
                    using (DbDataContext db = new DbDataContext())
                    {
                        Stroke = db.Probi.Where(c => c.Id == Stroka.Id).FirstOrDefault();
                        db.Dispose();
                    }
                }
                catch { }
            }
            AddProbiForm f = new AddProbiForm(Main, Stroke);
            f.Owner = Main;
            f.ShowDialog();
            UpdateGSOGrid(Main);
            f.Dispose();
        }

        internal void ProbiAddDataButton_Click(MainForm Main)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = new Probi();
            try
            {
                Stroke = (Probi)gv.GetRow(gv.FocusedRowHandle);
            }
            catch
            {
                try
                {
                    var Stroka = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle);
                    using (DbDataContext db = new DbDataContext())
                    {
                        Stroke = db.Probi.Where(c => c.Id == Stroka.Id).FirstOrDefault();
                        db.Dispose();
                    }
                }
                catch { }
            }
            var AnalizID = GetAnalizID(Main);
            AddProbiDataForm f = new AddProbiDataForm(Main, Stroke);
            f.Owner = Main;
            f.ShowDialog();
            UpdateGSOGrid(Main);
            f.Dispose();
        }

        internal void ProbiDeleteButton_Click(MainForm Main)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            string RowID = "";
            var Stroke = new Probi();
            try
            {
                Stroke = (Probi)gv.GetRow(gv.FocusedRowHandle);
                var StrokeAnaliz = (Probi)gv.GetRow(gv.FocusedRowHandle - 1);
                if (StrokeAnaliz != null)
                    RowID = StrokeAnaliz.Id.ToString();
            }
            catch
            {
                try
                {
                    var Stroka = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle);
                    using (DbDataContext db = new DbDataContext())
                    {
                        Stroke = db.Probi.Where(c => c.Id == Stroka.Id).FirstOrDefault();
                        db.Dispose();
                    }
                    var StrokeAnaliz = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle - 1);
                    if (StrokeAnaliz != null)
                        RowID = StrokeAnaliz.Id.ToString();
                }
                catch { }
            }
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Proba = db.Probi.Where(c => c.Id == Stroke.Id && (c.VisibleStatus == 0 || c.VisibleStatus == null || c.VisibleStatus == 1)).FirstOrDefault();
                    if (Proba != null)
                    {
                        DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить \"" + Proba.Шифр + "\"?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            Proba.VisibleStatus = Proba.Owner == null ? 300 : 301;
                            Proba.ДатаСозданияЗаписи = DateTime.Now;
                            Proba.UserName = UserSettings.User;
                            if (Proba.OperativniiKontrolID != null && Proba.OperativniiKontrolID != 0)
                            {
                                var Operkontrol = db.OperativniiKontrol.Where(c => c.Id == Proba.OperativniiKontrolID).FirstOrDefault();
                                if (Operkontrol != null)
                                {
                                    Operkontrol.Колво = Operkontrol.Колво.Value > 0 ? Operkontrol.Колво.Value - 1 : 0;
                                    Operkontrol.ДатаСозданияЗаписи = DateTime.Now;
                                    Operkontrol.UserName = UserSettings.User;
                                    var NextID = db.OperativniiKontrol != null && db.OperativniiKontrol.Count() > 0 ? db.OperativniiKontrol.Max(c => c.Id) + 1 : 1;
                                    db.OperativniiKontrol.InsertOnSubmit(new OperativniiKontrol
                                    {
                                        Id = NextID,
                                        Book = Operkontrol.Book,
                                        AnalizID = Operkontrol.AnalizID,
                                        AttestovannoeZnachenie = Operkontrol.AttestovannoeZnachenie,
                                        CreateDate = Operkontrol.CreateDate,
                                        EndDate = Operkontrol.EndDate,
                                        Owner = Operkontrol.Id,
                                        UserName = UserSettings.User,
                                        VisibleStatus = 200,
                                        ДатаСозданияЗаписи = DateTime.Now,
                                        Колво = Operkontrol.Колво
                                    });
                                }
                            }
                            if (Proba.KontrolStabilnostiID != null && Proba.KontrolStabilnostiID != 0)
                            {
                                var KontrolStabil = db.KontrolStabilnosti.Where(c => c.Id == Proba.KontrolStabilnostiID).FirstOrDefault();
                                if (KontrolStabil != null)
                                {
                                    KontrolStabil.Колво = KontrolStabil.Колво.Value > 0 ? KontrolStabil.Колво.Value - 1 : 0;
                                    KontrolStabil.ДатаСозданияЗаписи = DateTime.Now;
                                    KontrolStabil.UserName = UserSettings.User;
                                    var NextID = db.KontrolStabilnosti != null && db.KontrolStabilnosti.Count() > 0 ? db.KontrolStabilnosti.Max(c => c.Id) + 1 : 1;
                                    db.KontrolStabilnosti.InsertOnSubmit(new KontrolStabilnosti
                                    {
                                        Id = NextID,
                                        Book = KontrolStabil.Book,
                                        AnalizID = KontrolStabil.AnalizID,
                                        AttestovannoeZnachenie = KontrolStabil.AttestovannoeZnachenie,
                                        CreateDate = KontrolStabil.CreateDate,
                                        EndDate = KontrolStabil.EndDate,
                                        Owner = KontrolStabil.Id,
                                        UserName = UserSettings.User,
                                        VisibleStatus = 200,
                                        ДатаСозданияЗаписи = DateTime.Now,
                                        Колво = KontrolStabil.Колво,
                                    });
                                }
                            }
                            db.SubmitChanges();
                            SelectedRowId.AddNewRowID(Main.GSOSpisokTab.SelectedTabPage.Name, RowID);
                            UpdateGSOGrid(Main);
                        }
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                //MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void OperayivniiKontrol_Click(MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');
                    var AnalizID = GetAnalizID(Main);
                    var SelectAnalizShortName = db.Analiz.Where(c => c.Id == AnalizID && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault().ShortName;
                    var NaME = "Оперативный" + "&" + AnalizID;
                    AddTab(Main, SelectAnalizShortName.Length > 40 ? "Оперативный контроль \"" + SelectAnalizShortName.Substring(0, 40) + "...\"" : "Оперативный контроль \"" + SelectAnalizShortName + "\"", NaME, true);
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void CloseOperativniiKontrolBookClick(MainForm Main, OperativniiKontrol Stroke)
        {
            try
            {
                ChangeDateGSOBookForm f = new ChangeDateGSOBookForm(Main, Stroke, false, true);
                f.Owner = Main;
                f.ShowDialog();
                UpdateGSOGrid(Main);
                f.Dispose();                
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void CloseKontrolStabilnostiBookClick(MainForm Main, KontrolStabilnosti Stroke)
        {
            try
            {
                ChangeDateGSOBookForm f = new ChangeDateGSOBookForm(Main, Stroke, false, true);
                f.Owner = Main;
                f.ShowDialog();
                UpdateGSOGrid(Main);
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void OperativniiKontrolDeleteButton_Click(MainForm Main, int id)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var OperKontrol = db.OperativniiKontrol.Where(c => c.Id == id).FirstOrDefault();
                    DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить журнал оперативного контроля №" + OperKontrol.Book.Value.ToString() + "?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        OperKontrol.VisibleStatus = OperKontrol.Owner == null ? 300 : 301;
                        OperKontrol.ДатаСозданияЗаписи = DateTime.Now;
                        OperKontrol.UserName = UserSettings.User;
                        db.SubmitChanges();
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        var StrokeAnaliz = (OperativniiKontrol)gv.GetRow(gv.FocusedRowHandle - 1);
                        if (StrokeAnaliz != null)
                            SelectedRowId.AddNewRowID(Main.GSOSpisokTab.SelectedTabPage.Name, StrokeAnaliz.Id.ToString());
                        UpdateGSOGrid(Main);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                //MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void KontrolStabilnostiDeleteButton_Click(MainForm Main, int id)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var KontrolStabil = db.KontrolStabilnosti.Where(c => c.Id == id).FirstOrDefault();
                    DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить журнал контроля стабильности №" + KontrolStabil.Book.Value.ToString() + "?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        KontrolStabil.VisibleStatus = KontrolStabil.Owner == null ? 300 : 301;
                        KontrolStabil.ДатаСозданияЗаписи = DateTime.Now;
                        KontrolStabil.UserName = UserSettings.User;
                        db.SubmitChanges();
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        var StrokeFocus = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle - 1);
                        if (StrokeFocus != null)
                            SelectedRowId.AddNewRowID(Main.GSOSpisokTab.SelectedTabPage.Name, StrokeFocus.Id.ToString());
                        UpdateGSOGrid(Main);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void ChangeDateOperativniiKontrol_Click(MainForm Main, OperativniiKontrol Stroke)
        {
            try
            {
                ChangeDateGSOBookForm f = new ChangeDateGSOBookForm(Main, Stroke);
                f.Owner = Main;
                f.ShowDialog();
                f.Dispose();
                UpdateGSOGrid(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void ChangeDateKontrolStabilnosti_Click(MainForm Main, KontrolStabilnosti Stroke)
        {
            try
            {
                ChangeDateGSOBookForm f = new ChangeDateGSOBookForm(Main, Stroke);
                f.Owner = Main;
                f.ShowDialog();
                f.Dispose();
                UpdateGSOGrid(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void ViewOperativniiKontrolProbiClick(MainForm Main, OperativniiKontrol Stroke)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Analiz = db.Analiz.Where(c => c.Id == Stroke.AnalizID).FirstOrDefault();
                    var GetProbi = db.Probi.Where(c => c.AnalizID == Stroke.AnalizID && c.OperativniiKontrolID == Stroke.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                    var NaME = "ПробыОперативный" + "&" + Stroke.AnalizID + "&" + GetProbi.OperativniiKontrolID;
                    var Text2 = Analiz.ShortName.Length > 40 ? Analiz.ShortName.Substring(0, 40) + "..." : Analiz.ShortName;
                    AddTab(Main, "Журнал опер. контр. №" + Stroke.Book + " \"" + Text2 + "\"", NaME);
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void ViewKontrolStabilnostiProbiClick(MainForm Main, KontrolStabilnosti Stroke)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Analiz = db.Analiz.Where(c => c.Id == Stroke.AnalizID).FirstOrDefault();
                    var GetProbi = db.Probi.Where(c => c.AnalizID == Stroke.AnalizID && c.KontrolStabilnostiID == Stroke.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                    var NaME = "ПробыСтабильности" + "&" + Stroke.AnalizID + "&" + GetProbi.KontrolStabilnostiID;
                    var Text2 = Analiz.ShortName.Length > 40 ? Analiz.ShortName.Substring(0, 40) + "..." : Analiz.ShortName;
                    AddTab(Main, "Журнал контр. стаб. №" + Stroke.Book + " \"" + Text2 + "\"", NaME);
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void PrintOchenkaKKSHClick(MainForm MainForm, KontrolStabilnosti stroke, int VisiblePages = 12)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    using (DbDataContext db = new DbDataContext())
                    {
                        MF = MainForm;
                        var Probi = db.Probi.Where(c => c.KontrolStabilnostiID == stroke.Id && (c.VisibleStatus == null || c.VisibleStatus < 2)).ToList();
                        var Analiz = db.Analiz.Where(c => c.Id == stroke.AnalizID).FirstOrDefault();
                        int s = 0;
                        foreach (var Pr in Probi)
                        {
                            if (Pr.X1.HasValue && Pr.X2.HasValue)
                                break;
                            s++;
                        }
                        if ((s + 1 == Probi.Count) && !Probi[s].X1.HasValue && !Probi[s].X2.HasValue)
                            return;
                        VLKStatic.CalculatePredel(Probi[s]);
                        var AnalizTable = db.AnalizTable.Where(c => c.AnalizID == stroke.AnalizID && (c.VisibleStatus == null || c.VisibleStatus < 2)).ToList();
                        ExcelDoc1 = new ExcelDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Оценка ККШ.xltx");
                        _Worksheet Page2 = ExcelDoc1.excelWorkbook.Worksheets[2];
                        ExcelDoc1.excelWorksheet.Range["E4"].Value2 = Analiz.КонтролируемыйОбъект;
                        ExcelDoc1.excelWorksheet.Range["E5"].Value2 = Analiz.ОпределяемыйКомпонент;
                        ExcelDoc1.excelWorksheet.Range["E6"].Value2 = Analiz.НД;
                        Page2.Range["D12"].Value2 = Analiz.НД;
                        ExcelDoc1.excelWorksheet.Range["E7"].Value2 = Analiz.НазваниеМетодаАнализа;
                        Page2.Range["E15"].Value2 = Analiz.НазваниеМетодаАнализа;
                        ExcelDoc1.excelWorksheet.Range["E8"].Value2 = Analiz.ЕдиницаИзмерения;
                        Page2.Range["A26"].Value2 = Analiz.ЕдиницаИзмерения;
                        Page2.Range["C26"].Value2 = Analiz.ЕдиницаИзмерения;
                        Page2.Range["H26"].Value2 = Analiz.ЕдиницаИзмерения;
                        Page2.Range["K26"].Value2 = Analiz.ЕдиницаИзмерения;
                        foreach (var Select in AnalizTable)
                        {
                            var newOt = Select.Down.HasValue ? Select.Down.Value : 0;
                            var newDo = Select.Up.HasValue ? Select.Up.Value : 9999999999999999999;
                            if (newOt < stroke.AttestovannoeZnachenie && newDo >= stroke.AttestovannoeZnachenie)
                            {
                                ExcelDoc1.excelWorksheet.Range["E9"].Value2 = Select.Down.HasValue ? Select.Down.Value.ToString() : "";
                                ExcelDoc1.excelWorksheet.Range["F9"].Value2 = Select.Up.HasValue ? Select.Up.Value.ToString() : "";
                                var OtSvishe = "";
                                OtSvishe = Select.Down.HasValue ? "свыше " + Select.Down.Value.ToString() : "";
                                OtSvishe = OtSvishe + (Select.Up.HasValue ? " до " + Select.Up.Value.ToString() : "");
                                Page2.Range["A27"].Value2 = OtSvishe;
                            }
                        }
                        ExcelDoc1.excelWorksheet.Range["E11"].Value2 = stroke.CreateDate.HasValue ? stroke.CreateDate.Value.ToShortDateString() : "";
                        ExcelDoc1.excelWorksheet.Range["F11"].Value2 = stroke.EndDate.HasValue ? stroke.EndDate.Value.ToShortDateString() : "";
                        Page2.Range["F19"].Value2 = stroke.CreateDate.HasValue ? stroke.CreateDate.Value.ToShortDateString() : "";
                        Page2.Range["H19"].Value2 = stroke.EndDate.HasValue ? stroke.EndDate.Value.ToShortDateString() : "";
                        var N1 = VLKStatic.GetRoundNumber(Probi[0].АттестованноеЗначение.Value);
                        var N2 = VLKStatic.GetRoundNumber(Probi[0].X1.Value);
                        var N3 = VLKStatic.GetRoundNumber(Probi[0].X2.Value);
                        var RoundNumber = N1 >= N2 ? N1 >= N3 ? N1 : N3 : N2 >= N3 ? N2 : N3;
                        ExcelDoc1.excelWorksheet.Range["J15"].Value2 = RoundNumber;
                        if (Probi.Count > 1)
                        {
                            for (int i = 1; i < Probi.Count; i++)
                            {
                                ExcelDoc1.excelWorksheet.Rows["18:18"].Insert(XlInsertShiftDirection.xlShiftDown, XlInsertFormatOrigin.xlFormatFromLeftOrAbove);
                                ExcelDoc1.excelWorksheet.Range["A19", "K19"].Copy(ExcelDoc1.excelWorksheet.Range["A18", "K18"]);
                            }
                        }
                        int Row = 0;
                        double R = 0;
                        double K = 0;
                        //var NumberFormat = GetNumberFormat(VLKStatic.NumberRound);
                        ExcelDoc1.excelWorksheet.Range["E10"].NumberFormat = GetNumberFormat(N1); //Аттестованное значение
                        foreach (var One in Probi) //Таблица
                        {
                            R = R + (Probi[Row].RBig.HasValue ? Probi[Row].RBig.Value : 0);
                            K = K + (Probi[Row].KK.HasValue ? Probi[Row].KK.Value : 0);
                            ExcelDoc1.excelWorksheet.Range["A" + (18 + Row).ToString()].Value2 = Row + 1;
                            ExcelDoc1.excelWorksheet.Range["B" + (18 + Row).ToString()].NumberFormat = Probi[Row].АттестованноеЗначение.HasValue ? GetNumberFormat(VLKStatic.GetRoundNumber(Probi[Row].АттестованноеЗначение.Value)) : "0";
                            ExcelDoc1.excelWorksheet.Range["B" + (18 + Row).ToString()].Value2 = Probi[Row].АттестованноеЗначение.HasValue ? Probi[Row].АттестованноеЗначение.Value : 0;
                            ExcelDoc1.excelWorksheet.Range["C" + (18 + Row).ToString()].NumberFormat = Probi[Row].X1.HasValue ? GetNumberFormat(VLKStatic.GetRoundNumber(Probi[Row].X1.Value)) : "0";
                            ExcelDoc1.excelWorksheet.Range["C" + (18 + Row).ToString()].Value2 = Probi[Row].X1.HasValue ? Probi[Row].X1.Value : 0;
                            ExcelDoc1.excelWorksheet.Range["D" + (18 + Row).ToString()].NumberFormat = Probi[Row].X2.HasValue ? GetNumberFormat(VLKStatic.GetRoundNumber(Probi[Row].X2.Value)) : "0";
                            ExcelDoc1.excelWorksheet.Range["D" + (18 + Row).ToString()].Value2 = Probi[Row].X2.HasValue ? Probi[Row].X2.Value : 0;
                            Row++;
                        }
                        ExcelDoc1.excelWorksheet.Range["G" + (17 + Row).ToString()].Value2 = null;
                        ExcelDoc1.excelWorksheet.Range["F" + (21 + Row).ToString()].Value2 = Probi.Count - 1;
                        ExcelDoc1.excelWorksheet.Range["F" + (25 + Row).ToString()].Value2 = Probi.Count;
                        ExcelDoc1.excelWorksheet.Range["F" + (30 + Row).ToString()].Value2 = Probi.Count;
                        ExcelDoc1.excelWorksheet.Range["F" + (36 + Row).ToString()].Value2 = Probi.Count;
                        var tTabl = KriteriiStyudenta(Probi.Count - 1);
                        ExcelDoc1.excelWorksheet.Range["H" + (36 + Row).ToString()].Value2 = tTabl;

                        ExcelDoc1.excelWorksheet.Range["D" + (21 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        Page2.Range["C29"].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        ExcelDoc1.excelWorksheet.Range["D" + (21 + Row).ToString()].Value2 = Math.Round(R / (1.128 * Probi.Count - 1), RoundNumber + 1, MidpointRounding.AwayFromZero);
                        ExcelDoc1.excelWorksheet.Range["D" + (25 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        ExcelDoc1.excelWorksheet.Range["D" + (25 + Row).ToString()].Value2 = Math.Round(K / Probi.Count, RoundNumber + 1, MidpointRounding.AwayFromZero);
                        ExcelDoc1.excelWorksheet.Range["J" + (18 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 2);
                        ExcelDoc1.excelWorksheet.Range["J" + (18 + Row).ToString()].FormulaR1C1 = "=ROUND(SUM(R[-" + Probi.Count + "]C:R[-1]C),R15C10 + 2)";
                        ExcelDoc1.excelWorksheet.Range["D" + (30 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);

                        ExcelDoc1.excelWorksheet.Range["F" + (48 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        ExcelDoc1.excelWorksheet.Range["D" + (48 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        ExcelDoc1.excelWorksheet.Range["D" + (43 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        string Acl = "";
                        string Al = "";
                        if ((double)ExcelDoc1.excelWorksheet.Range["D" + (36 + Row).ToString()].Value2 > tTabl)
                        {
                            Acl = "Δсл,н = " + ExcelDoc1.excelWorksheet.Range["F" + (48 + Row).ToString()].Value2.ToString() + "         Δсл,в = " + ExcelDoc1.excelWorksheet.Range["D" + (48 + Row).ToString()].Value2.ToString();
                            ExcelDoc1.excelWorksheet.Rows[(42 + Row).ToString() + ":" + (44 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                            Row = Row - 4;
                        }
                        else
                        {
                            Acl = Math.Round((double)((decimal)ExcelDoc1.excelWorksheet.Range["D" + (43 + Row).ToString()].Value2), VLKStatic.NumberRound, MidpointRounding.AwayFromZero).ToString();
                            ExcelDoc1.excelWorksheet.Rows[(45 + Row).ToString() + ":" + (49 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                            Row = Row - 6;

                        }
                        ExcelDoc1.excelWorksheet.Range["C" + (54 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        ExcelDoc1.excelWorksheet.Range["F" + (59 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        ExcelDoc1.excelWorksheet.Range["E" + (63 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        ExcelDoc1.excelWorksheet.Range["E" + (71 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        ExcelDoc1.excelWorksheet.Range["C" + (71 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        if (1 / 3 < (double)ExcelDoc1.excelWorksheet.Range["C" + (54 + Row).ToString()].Value2)
                        {
                            Al = Math.Round((double)((decimal)ExcelDoc1.excelWorksheet.Range["E" + (63 + Row).ToString()].Value2), VLKStatic.NumberRound, MidpointRounding.AwayFromZero).ToString();
                            ExcelDoc1.excelWorksheet.Rows[(65 + Row).ToString() + ":" + (72 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                        }
                        else
                        {
                            Al = "Δл,н = " + ExcelDoc1.excelWorksheet.Range["E" + (71 + Row).ToString()].Value2.ToString() + "         Δл,в = " + ExcelDoc1.excelWorksheet.Range["C" + (71 + Row).ToString()].Value2.ToString();
                            ExcelDoc1.excelWorksheet.Rows[(57 + Row).ToString() + ":" + (64 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                        }
                        //ПротоколККШ 
                        Page2.Range["H29"].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        Page2.Range["H29"].Value2 = Acl;
                        Page2.Range["K29"].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        Page2.Range["K29"].Value2 = Al;
                        Page2.Range["H29"].NumberFormat = GetNumberFormat(VLKStatic.GetRoundNumber(VLKStatic.ORl));
                        Page2.Range["C27"].Value2 = VLKStatic.ORl;
                        Page2.Range["H29"].NumberFormat = GetNumberFormat(VLKStatic.GetRoundNumber(VLKStatic.Acl));
                        Page2.Range["H27"].Value2 = VLKStatic.Acl;
                        Page2.Range["H29"].NumberFormat = GetNumberFormat(VLKStatic.GetRoundNumber(VLKStatic.Al));
                        Page2.Range["K27"].Value2 = VLKStatic.Al;
                        ExcelDoc1.excelWorksheet.Columns["I:L"].Hidden = true;

                        if (VisiblePages == 2) ExcelDoc1.excelWorksheet.Visible = XlSheetVisibility.xlSheetHidden;
                        if (VisiblePages == 1) Page2.Visible = XlSheetVisibility.xlSheetHidden;

                        if (UserSettings.Protect)
                        {
                            var Use = db.Users.Where(c => c.UserName == UserSettings.User && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1 || c.VisibleStatus == 2)).FirstOrDefault();
                            if (Use != null && Use.Password != null && Use.UseDocProtect)
                            {
                                ExcelDoc2.excelWorksheet.Protect(Crypt.Decrypt(Use.Password, true), true, true, true);
                                Page2.Protect(Crypt.Decrypt(Use.Password, true), true, true, true);
                            }
                        }

                        db.Dispose();
                        ExcelDoc1.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        ExcelDoc1.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        private string GetNumberFormat(int numberRound)
        {
            var Format = "0.";
            while (numberRound > 0)
            {
                numberRound--;
                Format = Format + "0";
            }
            return Format;
        }

        private double KriteriiStyudenta(int coint)
        {
            switch (coint)
            {
                case 1:
                    return 12.71;
                case 2:
                    return 4.3;
                case 3:
                    return 3.18;
                case 4:
                    return 2.78;
                case 5:
                    return 2.57;
                case 6:
                    return 2.45;
                case 7:
                    return 2.37;
                case 8:
                    return 2.31;
                case 9:
                    return 2.26;
                case 10:
                    return 2.23;
                case 11:
                    return 2.20;
                case 12:
                    return 2.18;
                case 13:
                    return 2.16;
                case 14:
                    return 2.15;
                case 15:
                    return 2.14;
                case 16:
                    return 2.12;
                case 17:
                    return 2.11;
                case 18:
                    return 2.10;
                case 19:
                    return 2.09;
                case 20:
                    return 2.09;
                case 21:
                    return 2.08;
                case 22:
                    return 2.07;
                case 23:
                    return 2.07;
                case 24:
                    return 2.06;
                case 25:
                    return 2.06;
                case 26:
                    return 2.06;
                case 27:
                    return 2.05;
                case 28:
                    return 2.05;
                default:
                    if (coint > 28 && coint <= 33) return 2.04;
                    else if (coint > 33 && coint <= 37) return 2.03;
                    else if (coint > 37 && coint <= 44) return 2.02;
                    else if (coint > 44 && coint <= 53) return 2.01;
                    else if (coint > 53 && coint <= 69) return 2.00;
                    else if (coint > 69 && coint <= 95) return 1.99;
                    else if (coint > 95 && coint <= 155) return 1.98;
                    else if (coint > 155 && coint <= 400) return 1.97;
                    else if (coint > 400) return 1.96;
                    break;
            }
            return 0;
        }

        internal void KontrolStabilnosti_Click(MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');
                    var AnalizID = GetAnalizID(Main);
                    var SelectAnalizShortName = db.Analiz.Where(c => c.Id == AnalizID && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault().ShortName;
                    var NaME = "Стабильности" + "&" + AnalizID;
                    AddTab(Main, SelectAnalizShortName.Length > 40 ? "Контроль стабильности \"" + SelectAnalizShortName.Substring(0, 40) + "...\"" : "Контроль стабильности \"" + SelectAnalizShortName + "\"", NaME, true);
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void PredeliView_Click(MainForm Main, Probi Stroke)
        {
            try
            {
                if (Stroke.X1.HasValue && Stroke.X2.HasValue)
                {
                    PredeliForm f = new PredeliForm(Stroke);
                    f.Owner = Main;
                    f.Show();
                }
                else
                    MessageBox.Show("Отсутствуют результаты измерения!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void KontrolPovtorayamostiClick(KontrolStabilnosti Stroke, Probi Stroka)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    List<Probi> Probs = null;
                    if (Stroke != null)
                        Probs = db.Probi.Where(c => c.KontrolStabilnostiID == Stroke.Id && c.X.HasValue && (c.VisibleStatus == null || c.VisibleStatus < 2)).OrderBy(c => c.Дата).ToList();
                    else Probs = db.Probi.Where(c => c.KontrolStabilnostiID == Stroka.KontrolStabilnostiID && c.X.HasValue && (c.VisibleStatus == null || c.VisibleStatus < 2)).OrderBy(c => c.Дата).ToList();
                    if (Probs == null)
                    {
                        MessageBox.Show("Журнал пустой!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    DiagrammaPovtoryamostForm f = new DiagrammaPovtoryamostForm(Probs);
                    f.Show();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void KontrolPrechizionnostiClick(KontrolStabilnosti Stroke, Probi Stroka)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    List<Probi> Probs = null;
                    if (Stroke != null)
                        Probs = db.Probi.Where(c => c.KontrolStabilnostiID == Stroke.Id && c.X.HasValue && (c.VisibleStatus == null || c.VisibleStatus < 2)).OrderBy(c => c.Дата).ToList();
                    else Probs = db.Probi.Where(c => c.KontrolStabilnostiID == Stroka.KontrolStabilnostiID && c.X.HasValue && (c.VisibleStatus == null || c.VisibleStatus < 2)).OrderBy(c => c.Дата).ToList();
                    if (Probs == null)
                    {
                        MessageBox.Show("Журнал пустой!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    DiagrammaPrechezionnostForm f = new DiagrammaPrechezionnostForm(Probs);
                    f.Show();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void KontrolPogreshnostiClick(KontrolStabilnosti Stroke, Probi Stroka)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    List<Probi> Probs = null;
                    if (Stroke != null)
                        Probs = db.Probi.Where(c => c.KontrolStabilnostiID == Stroke.Id && c.X.HasValue && (c.VisibleStatus == null || c.VisibleStatus < 2)).OrderBy(c => c.Дата).ToList();
                    else Probs = db.Probi.Where(c => c.KontrolStabilnostiID == Stroka.KontrolStabilnostiID && c.X.HasValue && (c.VisibleStatus == null || c.VisibleStatus < 2)).OrderBy(c => c.Дата).ToList();
                    if (Probs == null)
                    {
                        MessageBox.Show("Журнал пустой!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    DiagrammaPogreshnostiForm f = new DiagrammaPogreshnostiForm(Probs);
                    f.Show();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void PrintKontrolStabilnostiClick(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => { if (1 > MF.GSOGridView.RowCount) return; }));
                    DevExpress.XtraGrid.Views.Grid.GridView gv = MF.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    KontrolStabilnosti Stroka = null;
                    Probi Stroke = null;
                    List<Probi> ProbsDB = null;
                    KontrolStabilnosti Stabilnosti = null;
                    try
                    {
                        Stroka = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
                    }
                    catch { }
                    try
                    {
                        Stroke = (Probi)gv.GetRow(gv.FocusedRowHandle);
                    }
                    catch
                    {
                        try
                        {
                            var Strok = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle);
                            using (DbDataContext db = new DbDataContext())
                            {
                                Stroke = db.Probi.Where(c => c.Id == Strok.Id).FirstOrDefault();
                                db.Dispose();
                            }
                        }
                        catch { }
                    }
                    if (Stroka == null && Stroke == null) return;
                    using (DbDataContext db = new DbDataContext())
                    {
                        if (Stroka != null)
                        {
                            ProbsDB = db.Probi.Where(c => c.KontrolStabilnostiID == Stroka.Id && (c.VisibleStatus == null || c.VisibleStatus < 2)).OrderBy(c => c.Дата).ToList();
                            Stabilnosti = db.KontrolStabilnosti.Where(c => c.Id == Stroka.Id).FirstOrDefault();
                        }
                        else
                        {
                            ProbsDB = db.Probi.Where(c => c.KontrolStabilnostiID == Stroke.KontrolStabilnostiID && (c.VisibleStatus == null || c.VisibleStatus < 2)).OrderBy(c => c.Дата).ToList();
                            Stabilnosti = db.KontrolStabilnosti.Where(c => c.Id == Stroke.KontrolStabilnostiID).FirstOrDefault();
                        }

                        if (ProbsDB == null)
                        {
                            MessageBox.Show("Журнал пустой!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        //Убираем строки с нулевыми данными
                        List<Probi> Probs = new List<Probi>();
                        foreach (var Pr in ProbsDB)
                        {
                            if (Pr.X1.HasValue && Pr.X2.HasValue)
                            {
                                Probs.Add(Pr);
                            }
                        }
                        if (Probs == null || Probs.Count == 0)
                        {
                            MessageBox.Show("Нет результатов измерений!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        var Analiz = db.Analiz.Where(c => c.Id == Probs[0].AnalizID).FirstOrDefault();
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = true));
                        MF.Invoke(new System.Action(() => MF.TopMost = false));
                        WordDoc1 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Журнал контроля стабильности.dotx");
                        WordDoc1.AddTableValue(1, 1, 2, Analiz.КонтролируемыйОбъект);
                        WordDoc1.AddTableValue(1, 2, 2, Analiz.ОпределяемыйКомпонент);
                        WordDoc1.AddTableValue(1, 3, 2, Analiz.НазваниеМетодаАнализа);
                        WordDoc1.AddTableValue(1, 4, 2, Analiz.ЕдиницаИзмерения);
                        var Period = Stabilnosti.CreateDate.HasValue ? Stabilnosti.CreateDate.Value.ToShortDateString() : "";
                        if (Stabilnosti.EndDate.HasValue) Period = Period + " - " + Stabilnosti.EndDate.Value.ToShortDateString();
                        WordDoc1.AddTableValue(1, 5, 2, Period);
                        WordDoc1.AddTableValue(1, 6, 2, VLKStatic.GetRoundNumber(Stabilnosti.AttestovannoeZnachenie.Value) > 0 ? Stabilnosti.AttestovannoeZnachenie.Value.ToString() : VLKStatic.AddZero(Stabilnosti.AttestovannoeZnachenie.Value, 1));
                        int s = 0;
                        foreach (var Pr in Probs)
                        {
                            if (Pr.X1.HasValue && Pr.X2.HasValue)
                                break;
                            s++;
                        }
                        if ((s + 1 == Probs.Count) && !Probs[s].X1.HasValue && !Probs[s].X2.HasValue)
                            return;
                        VLKStatic.CalculatePredel(Probs[s]);
                        WordDoc1.ReplaceString("{ПовторПР}", VLKStatic.AddZero(VLKStatic.Povt2.ToString()));
                        WordDoc1.ReplaceString("{ПовторД}", VLKStatic.AddZero(VLKStatic.Povt1.ToString()));
                        WordDoc1.ReplaceString("{ПовторСР}", VLKStatic.AddZero(VLKStatic.Povt3.ToString()));
                        WordDoc1.ReplaceString("{ПрецизионПР}", VLKStatic.AddZero(VLKStatic.Prech2.ToString()));
                        WordDoc1.ReplaceString("{ПрецизионД}", VLKStatic.AddZero(VLKStatic.Prech1.ToString()));
                        WordDoc1.ReplaceString("{ПрецизионСР}", VLKStatic.AddZero(VLKStatic.Prech3.ToString()));
                        WordDoc1.ReplaceString("{ТСРВ}", VLKStatic.AddZero(VLKStatic.Pogr1.ToString()));
                        WordDoc1.ReplaceString("{ТСРН}", "-" + VLKStatic.AddZero(VLKStatic.Pogr1.ToString()));
                        WordDoc1.ReplaceString("{ТДВ}", VLKStatic.AddZero(VLKStatic.Pogr2.ToString()));
                        WordDoc1.ReplaceString("{ТДН}", "-" + VLKStatic.AddZero(VLKStatic.Pogr2.ToString()));
                        int w = 0;
                        foreach (var sel in Probs)
                        {
                            if (sel.X1.HasValue && sel.X2.HasValue)
                            {
                                w++;
                            }
                        }
                        if (w > 1) WordDoc1.AddTableCell(1, w - 1);
                        int Cell = 0;
                        //WordDoc.Visible = true;
                        foreach (var Select in Probs)
                        {
                            if (Select.X1.HasValue && Select.X2.HasValue)
                            {
                                Cell++;
                                WordDoc1.AddTableValue(1, Cell + 15, 1, Cell.ToString());
                                WordDoc1.AddTableValue(1, Cell + 15, 2, Select.Дата.Value.ToShortDateString());
                                WordDoc1.AddTableValue(1, Cell + 15, 3, Select.Шифр);
                                WordDoc1.AddTableValue(1, Cell + 15, 4, Select.X1.HasValue ? VLKStatic.AddZero(Select.X1.Value.ToString()) : "");
                                WordDoc1.AddTableValue(1, Cell + 15, 5, Select.X2.HasValue ? VLKStatic.AddZero(Select.X2.Value.ToString()) : "");
                                WordDoc1.AddTableValue(1, Cell + 15, 6, Select.X.HasValue ? VLKStatic.AddZero(Select.X.Value.ToString()) : "");
                                WordDoc1.AddTableValue(1, Cell + 15, 7, Select.rSmall.HasValue ? VLKStatic.AddZero(Select.rSmall.Value.ToString()) : "");
                                WordDoc1.AddTableValue(1, Cell + 15, 8, Select.RBig.HasValue ? VLKStatic.AddZero(Select.RBig.Value.ToString()) : "");
                                WordDoc1.AddTableValue(1, Cell + 15, 9, Select.KK.HasValue ? VLKStatic.AddZero(Select.KK.Value.ToString()) : "");
                                WordDoc1.AddTableValue(1, Cell + 15, 13, "-");

                                //Одна точка вышла за предел действия
                                WordDoc1.AddTableValue(1, Cell + 15, 10, Select.rSmall.Value > VLKStatic.Povt1 ? "cверх предела действия" : "-");
                                WordDoc1.AddTableValue(1, Cell + 15, 11, (Select.RBig.HasValue ? Select.RBig.Value : 0) > VLKStatic.Prech1 ? "cверх предела действия" : "-");
                                WordDoc1.AddTableValue(1, Cell + 15, 12, Math.Abs(Select.KK.Value) > VLKStatic.Pogr1 ? "cверх предела действия" : "-");

                                //9 точек подряд находятся выше средней линии или  9 точек подряд находятся по одну сторону от средней линии
                                if (Cell > 8)
                                {
                                    var AddText = true;
                                    var Text4Add = "9 точек подряд находятся выше средней линии";
                                    for (int k = Cell - 9; k < Cell; k++) //Повторяемость
                                    {
                                        if (Probs[k].rSmall.Value <= VLKStatic.Povt3)
                                        {
                                            AddText = false;
                                            break;
                                        }
                                    }
                                    if (AddText)
                                    {
                                        for (int k = Cell - 8; k <= Cell; k++)
                                        {
                                            var Value = WordDoc1.TableValue(1, k + 15, 10);
                                            if (!Value.Contains(Text4Add))
                                                WordDoc1.AddTableValue(1, k + 15, 10, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                        }
                                    }

                                    if (Cell != Probs.Count) //Прецизионность
                                    {
                                        AddText = true;
                                        for (int k = Cell - 9; k < Cell; k++)
                                        {
                                            if (Probs[k].RBig.Value <= VLKStatic.Prech3)
                                            {
                                                AddText = false;
                                                break;
                                            }
                                        }
                                        if (AddText)
                                        {
                                            for (int k = Cell - 8; k <= Cell; k++)
                                            {
                                                var Value = WordDoc1.TableValue(1, k + 15, 11);
                                                if (!Value.Contains(Text4Add))
                                                    WordDoc1.AddTableValue(1, k + 15, 11, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                            }
                                        }
                                    }

                                    AddText = true;
                                    int simbol = Probs[(int)Cell - 1].KK.Value > 0 ? 1 : -1;
                                    for (int k = Cell - 9; k < Cell; k++) //Погрешность
                                    {
                                        if (Probs[k].KK.Value * simbol <= 0)
                                        {
                                            AddText = false;
                                            break;
                                        }
                                    }
                                    if (AddText)
                                    {
                                        for (int k = Cell - 8; k <= Cell; k++)
                                        {
                                            var Value = WordDoc1.TableValue(1, k + 15, 12);
                                            if (!Value.Contains(Text4Add))
                                                WordDoc1.AddTableValue(1, k + 15, 12, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                        }
                                    }
                                }

                                //6 возрастающих или убывающих точек подряд
                                if (Cell > 5)
                                {
                                    var Text4Add = "6 возрастающих точек подряд";
                                    var AddText = true;
                                    for (int k = Cell - 6; k < Cell - 1; k++) //Повторяемость
                                    {
                                        if (Probs[k].rSmall.Value >= Probs[k + 1].rSmall.Value)
                                        {
                                            AddText = false;
                                            break;
                                        }
                                    }
                                    if (AddText)
                                    {
                                        for (int k = Cell - 5; k <= Cell; k++)
                                        {
                                            var Value = WordDoc1.TableValue(1, k + 15, 10);
                                            if (!Value.Contains(Text4Add))
                                                WordDoc1.AddTableValue(1, k + 15, 10, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                        }
                                    }

                                    if (Cell != Probs.Count) //Прецизионность
                                    {
                                        AddText = true;
                                        for (int k = Cell - 6; k < Cell - 1; k++)
                                        {
                                            if (Probs[k].RBig.Value >= Probs[k + 1].RBig.Value)
                                            {
                                                AddText = false;
                                                break;
                                            }
                                        }
                                        if (AddText)
                                        {
                                            for (int k = Cell - 5; k <= Cell; k++)
                                            {
                                                var Value = WordDoc1.TableValue(1, k + 15, 11);
                                                if (!Value.Contains(Text4Add))
                                                    WordDoc1.AddTableValue(1, k + 15, 11, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                            }
                                        }
                                    }

                                    AddText = true;
                                    bool? Vozrastauchii = null;
                                    for (int k = Cell - 6; k < Cell - 1; k++) //Погрешность
                                    {
                                        if (Probs[k].KK.Value <= Probs[k + 1].KK.Value && (Vozrastauchii == null || !Vozrastauchii.Value))
                                        {
                                            if (Vozrastauchii != null)
                                            {
                                                AddText = false;
                                                break;
                                            }
                                            else
                                                Vozrastauchii = true;
                                        }
                                        else
                                        {
                                            if (Probs[k].KK.Value >= Probs[k + 1].KK.Value && (Vozrastauchii == null || Vozrastauchii.Value))
                                            {
                                                if (Vozrastauchii != null)
                                                {
                                                    AddText = false;
                                                    break;
                                                }
                                                else
                                                    Vozrastauchii = false;
                                            }
                                        }
                                    }
                                    if (AddText)
                                    {
                                        for (int k = Cell - 5; k <= Cell; k++)
                                        {
                                            var Value = WordDoc1.TableValue(1, k + 15, 12);
                                            if (Vozrastauchii.Value)
                                            {
                                                Text4Add = "6 возрастающих точек подряд";
                                                if (!Value.Contains(Text4Add))
                                                    WordDoc1.AddTableValue(1, k + 15, 12, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                            }
                                            else
                                            {
                                                Text4Add = "6 убывающих точек подряд";
                                                if (!Value.Contains(Text4Add))
                                                    WordDoc1.AddTableValue(1, k + 15, 12, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                            }
                                        }
                                    }

                                    //2 из 3 последовательных точек находятся выше предела предупреждения
                                    if (Cell > 2)
                                    {
                                        Text4Add = "2 из 3 последовательных точек выше предела предупреждения";
                                        int KolVoTochek = 0;
                                        for (int k = Cell - 3; k < Cell; k++) //Повторяемость
                                        {
                                            if (Probs[k].rSmall.Value > VLKStatic.Povt2)
                                            {
                                                KolVoTochek++;
                                            }
                                        }
                                        if (KolVoTochek > 1)
                                        {
                                            for (int k = Cell - 2; k <= Cell; k++)
                                            {
                                                if (Probs[k - 1].rSmall.Value > VLKStatic.Povt2)
                                                {
                                                    var Value = WordDoc1.TableValue(1, k + 15, 10);
                                                    if (!Value.Contains(Text4Add))
                                                        WordDoc1.AddTableValue(1, k + 15, 10, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                                }
                                            }
                                        }

                                        if (Cell != Probs.Count) //Прецизионность
                                        {
                                            KolVoTochek = 0;
                                            for (int k = Cell - 3; k < Cell; k++)
                                            {
                                                if (Probs[k].RBig.Value > VLKStatic.Prech2)
                                                {
                                                    KolVoTochek++;
                                                }
                                            }
                                            if (KolVoTochek > 1)
                                            {
                                                for (int k = Cell - 2; k <= Cell; k++)
                                                {
                                                    if (Probs[k - 1].RBig.Value > VLKStatic.Prech2)
                                                    {
                                                        var Value = WordDoc1.TableValue(1, k + 15, 11);
                                                        if (!Value.Contains(Text4Add))
                                                            WordDoc1.AddTableValue(1, k + 15, 11, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                                    }
                                                }
                                            }
                                        }

                                        KolVoTochek = 0;
                                        for (int k = Cell - 3; k < Cell; k++) //Погрешность
                                        {
                                            if (Math.Abs(Probs[k].KK.Value) > VLKStatic.Pogr2)
                                            {
                                                KolVoTochek++;
                                            }
                                        }
                                        if (KolVoTochek > 1)
                                        {
                                            for (int k = Cell - 2; k <= Cell; k++)
                                            {
                                                if (Math.Abs(Probs[k - 1].KK.Value) > VLKStatic.Pogr2)
                                                {
                                                    var Value = WordDoc1.TableValue(1, k + 15, 12);
                                                    if (!Value.Contains(Text4Add))
                                                        WordDoc1.AddTableValue(1, k + 15, 12, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                                }
                                            }
                                        }
                                    }

                                    //4 из 5 последовательных точек находятся выше половинной границы верхней или нижней зоны предупреждения
                                    if (Cell > 4)
                                    {
                                        Text4Add = "4 из 5 последовательных точек выше половинной границы зоны предупреждения";
                                        int KolVoTochek = 0;
                                        for (int k = Cell - 5; k < Cell; k++) //Повторяемость
                                        {
                                            if (Probs[k].rSmall.Value > VLKStatic.Povt3 + (VLKStatic.Povt2 - VLKStatic.Povt3) / 2)
                                            {
                                                KolVoTochek++;
                                            }
                                        }
                                        if (KolVoTochek > 3)
                                        {
                                            for (int k = Cell - 4; k <= Cell; k++)
                                            {
                                                if (Probs[k - 1].rSmall.Value > VLKStatic.Povt3 + (VLKStatic.Povt2 - VLKStatic.Povt3) / 2)
                                                {
                                                    var Value = WordDoc1.TableValue(1, k + 15, 10);
                                                    if (!Value.Contains(Text4Add))
                                                        WordDoc1.AddTableValue(1, k + 15, 10, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                                }
                                            }
                                        }

                                        if (Cell != Probs.Count) //Прецизионность
                                        {
                                            KolVoTochek = 0;
                                            for (int k = Cell - 5; k < Cell; k++)
                                            {
                                                if (Probs[k].RBig.Value > VLKStatic.Prech3 + (VLKStatic.Prech2 - VLKStatic.Prech3) / 2)
                                                {
                                                    KolVoTochek++;
                                                }
                                            }
                                            if (KolVoTochek > 3)
                                            {
                                                for (int k = Cell - 4; k <= Cell; k++)
                                                {
                                                    if (Probs[k - 1].RBig.Value > VLKStatic.Prech3 + (VLKStatic.Prech2 - VLKStatic.Prech3) / 2)
                                                    {
                                                        var Value = WordDoc1.TableValue(1, k + 15, 11);
                                                        if (!Value.Contains(Text4Add))
                                                            WordDoc1.AddTableValue(1, k + 15, 11, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                                    }
                                                }
                                            }
                                        }

                                        KolVoTochek = 0;
                                        for (int k = Cell - 5; k < Cell; k++) //Погрешность
                                        {
                                            if (Math.Abs(Probs[k].KK.Value) > VLKStatic.Prech2 / 2)
                                            {
                                                KolVoTochek++;
                                            }
                                        }
                                        if (KolVoTochek > 3)
                                        {
                                            for (int k = Cell - 4; k <= Cell; k++)
                                            {
                                                if (Math.Abs(Probs[k - 1].KK.Value) > VLKStatic.Prech2 / 2)
                                                {
                                                    var Value = WordDoc1.TableValue(1, k + 15, 11);
                                                    if (!Value.Contains(Text4Add))
                                                        WordDoc1.AddTableValue(1, k + 15, 12, Value.Contains("-") ? Text4Add : Value.Remove(Value.IndexOf('\r')) + ", " + Text4Add);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        db.Dispose();
                        MF.Invoke(new System.Action(() => { WordDoc1.Visible = true; }));
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc1.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void PrintOperativniiKontrolClick(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {   
                    MF = e.Argument as MainForm;
                    DevExpress.XtraGrid.Views.Grid.GridView gv = MF.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    var Stroka = new OperativniiKontrol();

                    using (DbDataContext db = new DbDataContext())
                    {
                        int RowCount = 0;
                        try
                        {
                            RowCount = gv.RowCount;
                        }
                        catch
                        {
                        }
                        if (RowCount == 0)
                        {
                            MessageBox.Show("Отсутствуют измерения в данном журнале", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        try
                        {
                            Stroka = (OperativniiKontrol)gv.GetRow(gv.FocusedRowHandle);
                        }
                        catch
                        {
                            try
                            {
                                var Stroke = (Probi)gv.GetRow(gv.FocusedRowHandle);
                                Stroka = db.OperativniiKontrol.Where(c => c.Id == Stroke.OperativniiKontrolID).FirstOrDefault();
                            }
                            catch { }
                        }
                        if (Stroka == null) return;
                        MF.Invoke(new System.Action(() => MF.TopMost = false));
                        List<Probi> Probs = new List<LIMS.Probi>();
                        if (gv.GetRow(gv.FocusedRowHandle) is Probi)
                        {
                            int Cells = 1;
                            for (int rowHandle = 0; rowHandle < RowCount; rowHandle++)
                            {
                                Cells++;
                                if (MF.ReactivGridView.IsDataRow(rowHandle) || Cells >= 39)
                                {                                    
                                    Probs.Add((Probi)MF.GSOGridView.GetRow(rowHandle));                                      
                                }
                            }
                        }
                        else
                        {
                            Probs = db.Probi.Where(c => c.OperativniiKontrolID == Stroka.Id && (c.VisibleStatus == null || c.VisibleStatus < 2) && c.X1.HasValue && c.X2.HasValue).OrderBy(c => c.Дата).ToList();
                        }

                            if (Probs == null)
                            {
                                MessageBox.Show("Отсутствуют измерения в данном журнале", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            WordDoc2 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Журнал оперативного контроля.dotx");
                            int w = 0;
                            foreach (var Stroke in Probs)
                            {
                                if (Stroke.X1.HasValue && Stroke.X2.HasValue)
                                    w++;
                            }
                            if (w > 1) WordDoc2.AddTableCell(1, w - 1);

                            int s = 0;
                            foreach (var Pr in Probs)
                            {
                                if (Pr.X1.HasValue && Pr.X2.HasValue)
                                    break;
                                s++;
                            }
                            if ((s + 1 == Probs.Count) && !Probs[s].X1.HasValue && !Probs[s].X2.HasValue)
                                return;
                            
                            int Cell = 0;
                            foreach (var Stroke in Probs)
                            {
                                VLKStatic.CalculatePredel(Stroke);
                                if (Stroke.X1.HasValue && Stroke.X2.HasValue)
                                {
                                    Cell++;
                                    WordDoc2.AddTableValue(1, Cell + 2, 1, Stroke.Дата.HasValue ? Stroke.Дата.Value.ToShortDateString() : "");
                                    WordDoc2.AddTableValue(1, Cell + 2, 2, Stroke.Исполнитель);
                                    WordDoc2.AddTableValue(1, Cell + 2, 3, Stroke.Шифр);
                                    WordDoc2.AddTableValue(1, Cell + 2, 4, Stroke.КонтролируемыйОбъект);
                                    WordDoc2.AddTableValue(1, Cell + 2, 5, Stroke.ОпределяемыйКомпонент);
                                    WordDoc2.AddTableValue(1, Cell + 2, 6, Stroke.X.HasValue ? Stroke.X.Value.ToString() : "");
                                    WordDoc2.AddTableValue(1, Cell + 2, 7, Stroke.АттестованноеЗначение.HasValue ? Stroke.АттестованноеЗначение.Value.ToString() : "");
                                    WordDoc2.AddTableValue(1, Cell + 2, 8, Stroke.KK.HasValue ? Stroke.KK.Value.ToString() : "");
                                    WordDoc2.AddTableValue(1, Cell + 2, 9, VLKStatic.Al.ToString());
                                    if (Stroke.KK.Value <= VLKStatic.Al && (-1) * Stroke.KK.Value <= VLKStatic.Al)
                                        WordDoc2.AddTableValue(1, Cell + 2, 10, "уд.");
                                    else WordDoc2.AddTableValue(1, Cell + 2, 10, "неуд.");
                                }                            
                        }
                        db.Dispose();
                        MF.Invoke(new System.Action(() => { WordDoc2.Visible = true; }));
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc2.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void PrintOchenkaKKSHClick(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => { if (1 > MF.GSOGridView.RowCount) return; }));
                    DevExpress.XtraGrid.Views.Grid.GridView gv = MF.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    using (DbDataContext db = new DbDataContext())
                    {
                        var stroke = new KontrolStabilnosti();
                        try
                        {
                            stroke = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
                        }
                        catch
                        {
                            try
                            {
                                var stroka = (KontrolStabilnostiModel)gv.GetRow(0);
                                stroke = db.KontrolStabilnosti.Where(c => c.Id == stroka._KontrolStabilnostiID).FirstOrDefault();
                            }
                            catch { }
                        }
                        var Probi = db.Probi.Where(c => c.KontrolStabilnostiID == stroke.Id && c.X.HasValue && (c.VisibleStatus == null || c.VisibleStatus < 2)).ToList();
                        if (Probi == null || Probi.Count == 0)
                        {
                            MessageBox.Show("Отсутствуют измерения в данном журнале", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        var Analiz = db.Analiz.Where(c => c.Id == stroke.AnalizID).FirstOrDefault();
                        int s = 0;
                        foreach (var Pr in Probi)
                        {
                            if (Pr.X1.HasValue && Pr.X2.HasValue)
                                break;
                            s++;
                        }
                        if ((s + 1 == Probi.Count) && !Probi[s].X1.HasValue && !Probi[s].X2.HasValue)
                            return;
                        VLKStatic.CalculatePredel(Probi[s]);
                        ExcelDoc2 = new ExcelDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Оценка ККШ.xltx");
                        _Worksheet Page2 = ExcelDoc2.excelWorkbook.Worksheets[2];
                        //Шапка
                        ExcelDoc2.excelWorksheet.Range["F4"].Value2 = Analiz.КонтролируемыйОбъект;
                        ExcelDoc2.excelWorksheet.Range["F5"].Value2 = Analiz.ОпределяемыйКомпонент;
                        ExcelDoc2.excelWorksheet.Range["F6"].Value2 = Analiz.НД;
                        ExcelDoc2.excelWorksheet.Range["F7"].Value2 = Analiz.НазваниеМетодаАнализа;
                        ExcelDoc2.excelWorksheet.Range["F8"].Value2 = Analiz.ЕдиницаИзмерения;
                        ExcelDoc2.excelWorksheet.Range["F11"].Value2 = stroke.EndDate.HasValue ? "c " + stroke.CreateDate.Value.ToShortDateString() + " по " + stroke.EndDate.Value.ToShortDateString() : stroke.CreateDate.HasValue ? "c " + stroke.CreateDate.Value.ToShortDateString() : "";

                        Page2.Range["A20"].Value2 = Analiz.ЕдиницаИзмерения;
                        Page2.Range["C20"].Value2 = Analiz.ЕдиницаИзмерения;
                        Page2.Range["H20"].Value2 = Analiz.ЕдиницаИзмерения;
                        Page2.Range["K20"].Value2 = Analiz.ЕдиницаИзмерения;
                        Page2.Range["H15"].Value2 = stroke.EndDate.HasValue ? "c " + stroke.CreateDate.Value.ToShortDateString() + " по " + stroke.EndDate.Value.ToShortDateString() : stroke.CreateDate.HasValue ? "c " + stroke.CreateDate.Value.ToShortDateString() : "";
                        try
                        {
                            var AnalizCalculateProfile = db.Analiz.Where(c => c.Id == stroke.AnalizID).FirstOrDefault().CalculateProfile;
                            if (AnalizCalculateProfile == null || AnalizCalculateProfile == "")
                            {
                                var AnalizTable = db.AnalizTable.Where(c => c.AnalizID == stroke.AnalizID && (c.VisibleStatus == null || c.VisibleStatus < 2)).ToList();
                                foreach (var Select in AnalizTable)
                                {
                                    var newOt = Select.Down.HasValue ? Select.Down.Value : 0;
                                    var newDo = Select.Up.HasValue ? Select.Up.Value : 9999999999999999999;
                                    if (newOt < stroke.AttestovannoeZnachenie && newDo >= stroke.AttestovannoeZnachenie)
                                    {
                                        var OtSvishe = "";
                                        if (Select.Down.HasValue && Select.Up.HasValue)
                                        {
                                            OtSvishe = "свыше " + Select.Down.Value.ToString() + " до " + Select.Up.Value.ToString();
                                        }
                                        else if (!Select.Down.HasValue && Select.Up.HasValue)
                                            OtSvishe = "менее " + Select.Up.Value.ToString();
                                        else if (Select.Down.HasValue && !Select.Up.HasValue)
                                            OtSvishe = "более " + Select.Up.Value.ToString();
                                        ExcelDoc2.excelWorksheet.Range["F9"].Value2 = OtSvishe;
                                        Page2.Range["A21"].Value2 = OtSvishe;
                                        Page2.Range["A23"].Value2 = OtSvishe;
                                    }
                                }
                            }
                            else
                            {
                                switch (AnalizCalculateProfile)
                                {
                                    case "Сера":
                                        ExcelDoc2.excelWorksheet.Range["F9"].Value2 = "свыше 0,1 до 5";
                                        Page2.Range["A21"].Value2 = "свыше 0,1 до 5";
                                        Page2.Range["A23"].Value2 = "свыше 0,1 до 5";
                                        break;
                                    case "Вязкость":
                                        ExcelDoc2.excelWorksheet.Range["F9"].Value2 = "свыше 1 до 100";
                                        Page2.Range["A21"].Value2 = "свыше 1 до 100";
                                        Page2.Range["A23"].Value2 = "свыше 1 до 100";
                                        break;
                                    case "Парафин":
                                        ExcelDoc2.excelWorksheet.Range["F9"].Value2 = "свыше 1,5 до 6";
                                        Page2.Range["A21"].Value2 = "свыше 1,6 до 6";
                                        Page2.Range["A23"].Value2 = "свыше 1,6 до 6";
                                        break;
                                    case "Хлорорганика":
                                        ExcelDoc2.excelWorksheet.Range["F9"].Value2 = "свыше 0,1 до 10";
                                        Page2.Range["A21"].Value2 = "свыше 0,1 до 10";
                                        Page2.Range["A23"].Value2 = "свыше 0,1 до 10";
                                        break;
                                }
                            }
                        }
                        catch { }

                        var N1 = VLKStatic.GetRoundNumber(Probi[0].АттестованноеЗначение.Value);
                        var N2 = VLKStatic.GetRoundNumber(Probi[0].X1.Value);
                        var N3 = VLKStatic.GetRoundNumber(Probi[0].X2.Value);
                        var RoundNumber = N1 >= N2 ? N1 >= N3 ? N1 : N3 : N2 >= N3 ? N2 : N3;
                        if (RoundNumber < 2) RoundNumber = 2;

                        var RN1 = VLKStatic.GetRoundNumber(VLKStatic.ORl);
                        var RN2 = VLKStatic.GetRoundNumber(VLKStatic.Acl);
                        var RN3 = VLKStatic.GetRoundNumber(VLKStatic.Al);
                        var RoundNumber2 = RN1 >= RN2 ? RN1 >= RN3 ? RN1 : RN3 : RN2 >= RN3 ? RN2 : RN3;
                        if (RoundNumber2 < 3) RoundNumber2 = 3;

                        ExcelDoc2.excelWorksheet.Range["J15"].Value2 = RoundNumber;
                        if (Probi.Count > 1)  //добавление строк в таблицу
                        {
                            for (int i = 1; i < Probi.Count; i++)
                            {
                                ExcelDoc2.excelWorksheet.Rows["18:18"].Insert(XlInsertShiftDirection.xlShiftDown, XlInsertFormatOrigin.xlFormatFromLeftOrAbove);
                                ExcelDoc2.excelWorksheet.Range["A19", "K19"].Copy(ExcelDoc2.excelWorksheet.Range["A18", "K18"]);
                            }
                        }
                        int Row = 0;
                        double R = 0;
                        double K = 0;

                        ExcelDoc2.excelWorksheet.Range["F10"].NumberFormat = GetNumberFormat(N1 < RoundNumber ? N1 + 1 : RoundNumber); //Аттестованное значение
                        ExcelDoc2.excelWorksheet.Range["F10"].Value2 = Probi[0].АттестованноеЗначение.HasValue ? Probi[0].АттестованноеЗначение.Value : 0;
                        ExcelDoc2.excelWorksheet.Range["I15"].Value2 = Probi.Count;
                        foreach (var One in Probi) //Таблица
                        {
                            R = R + (Probi[Row].RBig.HasValue ? Probi[Row].RBig.Value : 0);
                            K = K + (Probi[Row].KK.HasValue ? Probi[Row].KK.Value : 0);
                            ExcelDoc2.excelWorksheet.Range["A" + (18 + Row).ToString()].Value2 = Row + 1;
                            ExcelDoc2.excelWorksheet.Range["B" + (18 + Row).ToString()].NumberFormat = Probi[Row].АттестованноеЗначение.HasValue ? GetNumberFormat(N1 < RoundNumber ? N1 + 1 : RoundNumber/*VLKStatic.GetRoundNumber(Probi[Row].АттестованноеЗначение.Value)*/) : "0";
                            ExcelDoc2.excelWorksheet.Range["B" + (18 + Row).ToString()].Value2 = Probi[Row].АттестованноеЗначение.HasValue ? Probi[Row].АттестованноеЗначение.Value : 0;
                            ExcelDoc2.excelWorksheet.Range["C" + (18 + Row).ToString()].NumberFormat = Probi[Row].X1.HasValue ? GetNumberFormat(RoundNumber/*VLKStatic.GetRoundNumber(Probi[Row].X1.Value)*/) : "0";
                            ExcelDoc2.excelWorksheet.Range["C" + (18 + Row).ToString()].Value2 = Probi[Row].X1.HasValue ? Probi[Row].X1.Value : 0;
                            ExcelDoc2.excelWorksheet.Range["D" + (18 + Row).ToString()].NumberFormat = Probi[Row].X2.HasValue ? GetNumberFormat(RoundNumber/*VLKStatic.GetRoundNumber(Probi[Row].X2.Value)*/) : "0";
                            ExcelDoc2.excelWorksheet.Range["D" + (18 + Row).ToString()].Value2 = Probi[Row].X2.HasValue ? Probi[Row].X2.Value : 0;
                            ExcelDoc2.excelWorksheet.Range["E" + (18 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber);
                            ExcelDoc2.excelWorksheet.Range["F" + (18 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber);
                            ExcelDoc2.excelWorksheet.Range["G" + (18 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber);
                            ExcelDoc2.excelWorksheet.Range["H" + (18 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber);
                            Row++;
                        }
                        ExcelDoc2.excelWorksheet.Range["G" + (17 + Row).ToString()].Value2 = "";
                        //Вычисления
                        ExcelDoc2.excelWorksheet.Range["J" + (18 + Row).ToString()].FormulaR1C1 = "=ROUND(SUM(R[-" + Row.ToString() + "]C:R[-1]C),R15C10 + 1)";
                        ExcelDoc2.excelWorksheet.Range["H" + (21 + Row).ToString()].FormulaR1C1 = "=ROUND(SUM(R[-" + (3 + Row).ToString() + "]C[-1]:R[-4]C[-1])/(1.128*" + Row.ToString() + "),R15C10 + 1)";
                        ExcelDoc2.excelWorksheet.Range["H" + (22 + Row).ToString()].FormulaR1C1 = "=ROUND(SUM(R[-" + (4 + Row).ToString() + "]C[0]:R[-5]C[0])/(1.128*" + Row.ToString() + "),R15C10 + 1)";
                        ExcelDoc2.excelWorksheet.Range["H" + (21 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        ExcelDoc2.excelWorksheet.Range["H" + (22 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        ExcelDoc2.excelWorksheet.Range["H" + (23 + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        //Округления
                        ExcelDoc2.excelWorksheet.Range["I" + (27 + Row).ToString()].Value2 = RoundNumber + 1;
                        for (int i = 0; i < 10; i++)
                        {
                            ExcelDoc2.excelWorksheet.Range["H" + (30 + i + Row).ToString()].NumberFormat = GetNumberFormat(RoundNumber + 1);
                        }
                        //Удаление лишнего
                        var T = ExcelDoc2.excelWorksheet.Range["H" + (24 + Row).ToString()].Value2;
                        var Ttabl = ExcelDoc2.excelWorksheet.Range["H" + (25 + Row).ToString()].Value2;
                        var yslovie2 = ExcelDoc2.excelWorksheet.Range["H" + (33 + Row).ToString()].Value2;
                        if (T <= Ttabl)
                        {
                            ExcelDoc2.excelWorksheet.Rows[(29 + Row).ToString() + ":" + (29 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                            ExcelDoc2.excelWorksheet.Rows[(30 + Row).ToString() + ":" + (31 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                            Page2.Range["H23"].Value2 = ExcelDoc2.excelWorksheet.Range["H" + (29 + Row).ToString()].Value2;
                            if (yslovie2 == "выполняется")
                            {
                                ExcelDoc2.excelWorksheet.Rows[(32 + Row).ToString() + ":" + (36 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                                Page2.Range["K23"].Value2 = ExcelDoc2.excelWorksheet.Range["H" + (31 + Row).ToString()].Value2;
                            }
                            else
                            {
                                ExcelDoc2.excelWorksheet.Rows[(33 + Row).ToString() + ":" + (36 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                                ExcelDoc2.excelWorksheet.Rows[(31 + Row).ToString() + ":" + (31 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                                Page2.Range["K23"].Value2 = ExcelDoc2.excelWorksheet.Range["H" + (31 + Row).ToString()].Value2;
                            }
                        }
                        else
                        {
                            ExcelDoc2.excelWorksheet.Rows[(28 + Row).ToString() + ":" + (28 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                            ExcelDoc2.excelWorksheet.Rows[(29 + Row).ToString() + ":" + (29 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                            Page2.Range["H23"].Value2 = "Δсл,н = " + VLKStatic.AddZero(Math.Round(ExcelDoc2.excelWorksheet.Range["H" + (29 + Row).ToString()].Value2, RoundNumber2), RoundNumber2) + "          Δсл,в = " + VLKStatic.AddZero(Math.Round(ExcelDoc2.excelWorksheet.Range["H" + (30 + Row).ToString()].Value2, RoundNumber2), RoundNumber2);
                            if (yslovie2 == "выполняется")
                            {
                                ExcelDoc2.excelWorksheet.Rows[(36 + Row).ToString() + ":" + (37 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                                ExcelDoc2.excelWorksheet.Rows[(32 + Row).ToString() + ":" + (33 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                            }
                            else
                            {
                                ExcelDoc2.excelWorksheet.Rows[(32 + Row).ToString() + ":" + (35 + Row).ToString()].Delete(XlDeleteShiftDirection.xlShiftUp);
                            }
                            Page2.Range["K23"].Value2 = "Δл,н = " + VLKStatic.AddZero(Math.Round(ExcelDoc2.excelWorksheet.Range["H" + (32 + Row).ToString()].Value2, RoundNumber2), RoundNumber2) + "              Δл,в = " + VLKStatic.AddZero(Math.Round(ExcelDoc2.excelWorksheet.Range["H" + (33 + Row).ToString()].Value2, RoundNumber2), RoundNumber2);
                        }                        
                        //ПротоколККШ                    
                        Page2.Range["H11"].Value2 = Analiz.НД;
                        Page2.Range["H12"].Value2 = Analiz.НД;
                        Page2.Range["H13"].Value2 = Analiz.НазваниеМетодаАнализа;

                        //Верхняя строка                   
                        Page2.Range["C21"].Value2 = VLKStatic.ORl;
                        Page2.Range["C21"].NumberFormat = GetNumberFormat(RoundNumber2);
                        Page2.Range["H21"].Value2 = VLKStatic.Acl;
                        Page2.Range["H21"].NumberFormat = GetNumberFormat(RoundNumber2);
                        Page2.Range["K21"].Value2 = VLKStatic.Al;
                        Page2.Range["K21"].NumberFormat = GetNumberFormat(RoundNumber2);
                        //Нижняя строка                    
                        Page2.Range["H23"].NumberFormat = GetNumberFormat(RoundNumber2);
                        Page2.Range["K23"].NumberFormat = GetNumberFormat(RoundNumber2);
                        Page2.Range["C23"].NumberFormat = GetNumberFormat(RoundNumber2);
                        Page2.Range["B27"].Value2 = VLKStatic.ORl;
                        Page2.Range["B27"].NumberFormat = GetNumberFormat(RoundNumber2);
                        Page2.Range["F27"].Value2 = Convert.ToChar(177).ToString() + " " + VLKStatic.AddZero(Math.Round(VLKStatic.Acl, RoundNumber2), RoundNumber2) + ";";
                        Page2.Range["F27"].NumberFormat = GetNumberFormat(RoundNumber2);
                        Page2.Range["H27"].Value2 = Convert.ToChar(177).ToString() + " " + VLKStatic.AddZero(Math.Round(VLKStatic.Al, RoundNumber2), RoundNumber2);
                        Page2.Range["H27"].NumberFormat = GetNumberFormat(RoundNumber2);
                        ExcelDoc2.excelWorksheet.Columns["I:L"].Hidden = true;
                        ExcelDoc2.excelWorksheet.Range["F4"].Value2 = Analiz.КонтролируемыйОбъект;
                        ExcelDoc2.excelWorkbook.Worksheets[1].Select();
                        if (VisiblePages == 2)
                        {
                            ExcelDoc2.excelWorksheet.Visible = XlSheetVisibility.xlSheetHidden;
                        }
                        if (VisiblePages == 1)
                        {
                            Page2.Visible = XlSheetVisibility.xlSheetHidden;
                        }

                        if (UserSettings.Protect)
                        {
                            var Use = db.Users.Where(c => c.UserName == UserSettings.User && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1 || c.VisibleStatus == 2)).FirstOrDefault();
                            if (Use != null && Use.Password != null && Use.UseDocProtect)
                            {
                                ExcelDoc2.excelWorksheet.Protect(Crypt.Decrypt(Use.Password, true), true, true, true);
                                Page2.Protect(Crypt.Decrypt(Use.Password, true), true, true, true);
                            }
                        }
                        db.Dispose();
                        MF.Invoke(new System.Action(() => ExcelDoc2.Visible = true));
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        MF.Invoke(new System.Action(() => { ExcelDoc2.Close(); }));
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void GSOGridView_StartSorting(MainForm Main)
        {
            try
            {
                var GV = Main.GSOGridView;
                if (GV.SortInfo.Count > 0)
                {
                    for (int i = GV.SortInfo.Count - 1; i >= 0; i--)
                    {
                        var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');

                        if (Names[0] == "Пробы")
                        {
                            GV.ClearSorting();
                            if (SortOrderProbi == DevExpress.Data.ColumnSortOrder.Ascending)
                                SortOrderProbi = DevExpress.Data.ColumnSortOrder.Descending;
                            else
                                SortOrderProbi = DevExpress.Data.ColumnSortOrder.Ascending;
                        }
                        else if (Names[0] == "ПробыОперативный")
                        {
                            GV.ClearSorting();
                            if (SortOrderProbiOperativnii == DevExpress.Data.ColumnSortOrder.Ascending)
                                SortOrderProbiOperativnii = DevExpress.Data.ColumnSortOrder.Descending;
                            else
                                SortOrderProbiOperativnii = DevExpress.Data.ColumnSortOrder.Ascending;
                        }
                        //else if (Names[0] == "ПробыСтабильности")
                        //{
                        //    GV.ClearSorting();
                        //    if (SortOrderProbiStabilnosti == DevExpress.Data.ColumnSortOrder.Ascending)
                        //        SortOrderProbiStabilnosti = DevExpress.Data.ColumnSortOrder.Descending;
                        //    else
                        //        SortOrderProbiStabilnosti = DevExpress.Data.ColumnSortOrder.Ascending;
                        //}                                         
                    }
                    UpdateGSOGrid(Main);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
        
        internal void VLKCellMergeButton_Click(MainForm Main)
        {
            try
            {
                var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');
                if (Names[0] == "Пробы")
                {
                    UserSettings.VLKProbiCellMerge = !UserSettings.VLKProbiCellMerge;
                    Main.GSOGridView.OptionsView.AllowCellMerge = UserSettings.VLKProbiCellMerge;
                    if (UserSettings.VLKProbiCellMerge)
                        Main.VLKCellMergeButton.Image = global::LIMS.Properties.Resources.freezetoprow_32x32;
                    else Main.VLKCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
                }
                else if (Names[0] == "Оперативный")
                {
                    UserSettings.VLKOperativniiCellMerge = !UserSettings.VLKOperativniiCellMerge;
                    Main.GSOGridView.OptionsView.AllowCellMerge = UserSettings.VLKOperativniiCellMerge;
                    if (UserSettings.VLKOperativniiCellMerge)
                        Main.VLKCellMergeButton.Image = global::LIMS.Properties.Resources.freezetoprow_32x32;
                    else Main.VLKCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
                }
                else if (Names[0] == "Стабильности")
                {
                    UserSettings.VLKStabilnostCellMerge = !UserSettings.VLKStabilnostCellMerge;
                    Main.GSOGridView.OptionsView.AllowCellMerge = UserSettings.VLKStabilnostCellMerge;
                    if (UserSettings.VLKStabilnostCellMerge)
                        Main.VLKCellMergeButton.Image = global::LIMS.Properties.Resources.freezetoprow_32x32;
                    else Main.VLKCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
                }
                else if (Names[0] == "ПробыОперативный")
                {
                    UserSettings.VLKOperativniiProbiCellMerge = !UserSettings.VLKOperativniiProbiCellMerge;
                    Main.GSOGridView.OptionsView.AllowCellMerge = UserSettings.VLKOperativniiProbiCellMerge;
                    if (UserSettings.VLKOperativniiProbiCellMerge)
                        Main.VLKCellMergeButton.Image = global::LIMS.Properties.Resources.freezetoprow_32x32;
                    else Main.VLKCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
                }
                else if (Names[0] == "ПробыСтабильности")
                {
                    UserSettings.VLKStabilnostProbiCellMerge = !UserSettings.VLKStabilnostProbiCellMerge;
                    Main.GSOGridView.OptionsView.AllowCellMerge = UserSettings.VLKStabilnostProbiCellMerge;
                    if (UserSettings.VLKStabilnostProbiCellMerge)
                        Main.VLKCellMergeButton.Image = global::LIMS.Properties.Resources.freezetoprow_32x32;
                    else Main.VLKCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
                }
                UserSettings.SaveSettings();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void PrintProbiClick(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    DevExpress.XtraGrid.Views.Grid.GridView gv = MF.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    var Stroka = new Probi();

                    using (DbDataContext db = new DbDataContext())
                    {
                        int RowCount = 0;
                        try
                        {
                            RowCount = gv.RowCount;
                        }
                        catch
                        { }
                        if (RowCount == 0) return;
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = true));
                        MF.Invoke(new System.Action(() => MF.TopMost = false));

                        WordDoc3 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Журнал шифрования.dotx");
                        if (RowCount > 1) WordDoc3.AddTableCell(1, RowCount - 1);
                        VLKStatic.CalculatePredel((Probi)gv.GetRow(gv.FocusedRowHandle));
                        int Cell = 1;
                        for (int rowHandle = 0; rowHandle < RowCount; rowHandle++)
                        {
                            Cell++;
                            if (MF.ReactivGridView.IsDataRow(rowHandle) || Cell >= 39)
                            {
                                try
                                {
                                    var SelectedRow = (Probi)MF.GSOGridView.GetRow(rowHandle);
                                    WordDoc3.AddTableValue(1, Cell + 1, 1, SelectedRow.Дата.Value.ToShortDateString());
                                    WordDoc3.AddTableValue(1, Cell + 1, 2, SelectedRow.Шифр);
                                    WordDoc3.AddTableValue(1, Cell + 1, 3, SelectedRow.КонтролируемыйОбъект);
                                    WordDoc3.AddTableValue(1, Cell + 1, 4, SelectedRow.ОпределяемыйКомпонент);
                                    WordDoc3.AddTableValue(1, Cell + 1, 5, SelectedRow.АттестованноеЗначение.Value.ToString());
                                    WordDoc3.AddTableValue(1, Cell + 1, 6, SelectedRow.Выдал);
                                    WordDoc3.AddTableValue(1, Cell + 1, 7, SelectedRow.Получил);
                                }
                                catch { }
                            }
                        }
                        db.Dispose();
                        MF.Invoke(new System.Action(() => { WordDoc3.Visible = true; }));
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc3.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        internal void GSOGridView_FocusedRowChanged(MainForm Main)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = Main.GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            try
            {
                var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');
                if (Names[0] == "MainGSOTab")
                {
                    var StrokeAnaliz = (AnalizVLK)gv.GetRow(gv.FocusedRowHandle);
                    if (StrokeAnaliz != null)
                        SelectedRowId.AddNewRowID(Main.GSOSpisokTab.SelectedTabPage.Name, StrokeAnaliz.Id.ToString());
                }
                else if (Names[0] == "Пробы")
                {
                    var StrokeAnaliz = (Probi)gv.GetRow(gv.FocusedRowHandle);
                    if (StrokeAnaliz != null)
                        SelectedRowId.AddNewRowID(Main.GSOSpisokTab.SelectedTabPage.Name, StrokeAnaliz.Id.ToString());
                }
                else if (Names[0] == "Оперативный")
                {
                    var StrokeAnaliz = (OperativniiKontrol)gv.GetRow(gv.FocusedRowHandle);
                    if (StrokeAnaliz != null)
                        SelectedRowId.AddNewRowID(Main.GSOSpisokTab.SelectedTabPage.Name, StrokeAnaliz.Id.ToString());
                }
                else if (Names[0] == "Стабильности")
                {
                    var StrokeAnaliz = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
                    if (StrokeAnaliz != null)
                        SelectedRowId.AddNewRowID(Main.GSOSpisokTab.SelectedTabPage.Name, StrokeAnaliz.Id.ToString());
                }
                else if (Names[0] == "ПробыОперативный")
                {
                    var StrokeAnaliz = (Probi)gv.GetRow(gv.FocusedRowHandle);
                    if (StrokeAnaliz != null)
                        SelectedRowId.AddNewRowID(Main.GSOSpisokTab.SelectedTabPage.Name, StrokeAnaliz.Id.ToString());
                }
                else if (Names[0] == "ПробыСтабильности")
                {
                    var StrokeAnaliz = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle);
                    if (StrokeAnaliz != null)
                        SelectedRowId.AddNewRowID(Main.GSOSpisokTab.SelectedTabPage.Name, StrokeAnaliz.Id.ToString());
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                UpdateGSOGrid(Main);
                //MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void CloseAllTabsButton_Click(MainForm Main)
        {
            try
            {
                for (int i = Main.GSOSpisokTab.TabPages.Count - 1; i > 0; i--)
                {
                    if (Main.GSOSpisokTab.TabPages[i].Name != "MainGSOTab")
                    {
                        Main.GSOSpisokTab.TabPages[i].Dispose();
                    }
                }
                UpdateGSOGrid(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void FiltrButton_Click(MainForm Main)
        {
            try
            {
                Main.GSOGridView.ColumnFilterChanged -= Main.GSOGridView_ColumnFilterChanged;
                var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');
                if (Names.Length > 1)
                {
                    if (Names[0] == "Пробы")
                    {
                        FiltrVLKProbiForm f = new FiltrVLKProbiForm(Main, Names[1]);
                        f.Owner = Main;
                        f.ShowDialog();
                        f.Dispose();
                        SetFiltr(Main, Names[1]);
                    }
                    if (Names[0] == "ПробыОперативный")
                    {
                        FiltrVLKProbiForm f = new FiltrVLKProbiForm(Main, Names[1], Convert.ToInt32(Names[2]));
                        f.Owner = Main;
                        f.ShowDialog();
                        f.Dispose();
                        SetFiltr(Main, Names[1], Convert.ToInt32(Names[2]));
                    }                    
                }                
                Main.GSOGridView.ColumnFilterChanged += Main.GSOGridView_ColumnFilterChanged;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void GSOGridView_ColumnFilterChanged(MainForm Main)
        {
            try
            {
                var Names = Main.GSOSpisokTab.SelectedTabPage.Name.Split('&');
                if (Names.Length > 1 && Names[0] == "Пробы")
                {
                    AddNewFiltr(new FiltrVLKProbi { AnalizID = Names[1], EndDate = null, ShifrDoN = null, ShifrDoYear = null, ShifrOtN = null, ShifrOtYear = null, StartDate = null });
                }
                if (Names.Length > 1 && Names[0] == "ПробыОперативный")
                {
                    AddNewFiltr(new FiltrVLKProbi { AnalizID = Names[1], OperKontrolID = Convert.ToInt32(Names[2]), EndDate = null, ShifrDoN = null, ShifrDoYear = null, ShifrOtN = null, ShifrOtYear = null, StartDate = null });
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }
    }
}
