﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace LIMS
{
    public class PersonalTab
    {
        MainForm MF;
        public bool NewPersonal = false;
        public bool CopyPersonal = false;
        List<int> orderList = new List<int>();
        public bool ChangeWidth = true;
        WordDocument WordDoc1;
        bool VisibleAll = true;
        bool VivibleUvolennie = false;
        string SelectedRowId = "";

        public void GetPersonal(MainForm Main)
        {
            try
            {
                new Thread(GetPersonalThread).Start(Main);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void GetPersonalThread(object MainF)
        {
            try
            {
                var Main = MainF as MainForm;
                MF = Main;
                using (DbDataContext db = new DbDataContext())
                {
                    List<Personal> ВсеЗначения = new List<Personal>();
                    try
                    {
                        if (!UserSettings.Developer)
                        {
                            if (VisibleAll && VivibleUvolennie) ВсеЗначения = db.Personal.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null || c.VisibleStatus == 1).OrderBy(c => c.N).ToList();
                            else if (VisibleAll && !VivibleUvolennie) ВсеЗначения = db.Personal.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null).OrderBy(c => c.N).ToList();
                            else if (!VisibleAll && VivibleUvolennie) ВсеЗначения = db.Personal.Where(c => c.VisibleStatus == 1).OrderBy(c => c.N).ToList();
                            else ВсеЗначения = null;
                        }
                        else ВсеЗначения = db.Personal.OrderBy(c => c.N).ToList();
                    }
                    catch
                    {
                        LogErrors Log = new LogErrors("Ошибка соединения с сервером - Персонал");
                        Main.Invoke(new System.Action(() => MessageBox.Show("Сервер не отвечает!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.PersonalGridView.FocusedRowChanged -= Main.PersonalGridView_FocusedRowChanged;
                        Main.PersonalGrid.DataSource = ВсеЗначения;
                        if (!UserSettings.Developer)
                        {
                            Main.PersonalGridView.Columns[0].Visible = false;
                            Main.PersonalGridView.Columns[16].Visible = false;
                            Main.PersonalGridView.Columns[17].Visible = false;
                            Main.PersonalGridView.Columns[18].Visible = false;
                            Main.PersonalGridView.Columns[19].Visible = false;
                            Main.PersonalGridView.Columns[20].Visible = false;
                        }
                    }));
                    if (orderList.Count < 1)
                    {
                        Main.Invoke(new System.Action(() =>
                        {
                            for (int i = 0; i < Main.PersonalGridView.Columns.Count; i++)
                                orderList.Add(Main.PersonalGridView.Columns[i].VisibleIndex);
                        }));
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        for (int i = 0; i < Main.PersonalGridView.Columns.Count; i++)
                            Main.PersonalGridView.Columns[i].VisibleIndex = orderList[i];
                    }));
                    if (UserSettings.PersonalColumns != null)
                    {
                        var Visibiliti = UserSettings.PersonalColumns.Split(',');
                        for (int i = 0; i < Visibiliti.Length; i++)
                        {
                            Main.Invoke(new System.Action(() =>
                            Main.PersonalGridView.Columns[i + 1].Visible = (Visibiliti[i] == "1")));
                        }
                    }

                    Main.Invoke(new System.Action(() =>
                    {
                        //Main.PersonalGridView.FocusedRowChanged += Main.PersonalGridView_FocusedRowChanged;
                        ColumnView View = Main.PersonalGridView;
                        GridColumn column = View.Columns["Id"];
                        if (column != null)
                        {
                            int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                            if (rhFound != GridControl.InvalidRowHandle)
                            {
                                View.FocusedRowHandle = rhFound;
                                View.FocusedColumn = column;
                            }
                        }
                    }));

                    Main.Invoke(new System.Action(() =>
                    {
                        Main.PersonalGridView.Columns[1].Caption = "№ п/п";
                        Main.PersonalGridView.Columns[2].Caption = "Фамилия Имя Отечество сотрудника";
                        Main.PersonalGridView.Columns[3].Caption = "Должность";
                        Main.PersonalGridView.Columns[4].Caption = "Образование";
                        Main.PersonalGridView.Columns[5].Caption = "Образование (полное)";
                        Main.PersonalGridView.Columns[6].Caption = "Практический опыт";
                        Main.PersonalGridView.Columns[7].Caption = "Повышение калификации по основной профессии";
                        Main.PersonalGridView.Columns[8].Caption = "Срок подтверждения (повышения) квалификации по основной профессии";
                        Main.PersonalGridView.Columns[9].Caption = "Обучение на лаборанта газоанализаторщика";
                        Main.PersonalGridView.Columns[10].Caption = "Срок подтверждения квалификации лаборанта-газоанализаторщика";
                        Main.PersonalGridView.Columns[11].Caption = "Аттестация";
                        Main.PersonalGridView.Columns[12].Caption = "Срок аттестации";
                        Main.PersonalGridView.Columns[13].Caption = "Проверка знаний по ОТ и ПБ";
                        Main.PersonalGridView.Columns[14].Caption = "Срок проверки знаний по ОТ и ПБ";
                        Main.PersonalGridView.Columns[15].Caption = "Дополнительно";
                        Main.PersonalGridView.Columns[8].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.PersonalGridView.Columns[8].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.PersonalGridView.Columns[9].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.PersonalGridView.Columns[9].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.PersonalGridView.Columns[10].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.PersonalGridView.Columns[10].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.PersonalGridView.Columns[11].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.PersonalGridView.Columns[11].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.PersonalGridView.Columns[12].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.PersonalGridView.Columns[12].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.PersonalGridView.Columns[13].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.PersonalGridView.Columns[13].DisplayFormat.FormatString = "dd/MM/yyyy";
                        Main.PersonalGridView.Columns[14].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        Main.PersonalGridView.Columns[14].DisplayFormat.FormatString = "dd/MM/yyyy";
                    }));
                    if (UserSettings.PersonalColumnWidth != "" && UserSettings.PersonalColumnWidth != null)
                    {
                        ChangeWidth = false;
                        var ColumnWidth = UserSettings.PersonalColumnWidth.Split(',');
                        for (int i = 1; i < 16; i++)
                        {
                            Main.Invoke(new System.Action(() => Main.PersonalGridView.Columns[i].Width = Convert.ToInt32(ColumnWidth[i - 1])));
                        }
                        ChangeWidth = true;
                    }
                    Main.Invoke(new System.Action(() =>
                    {
                        Main.PersonalGridView.OptionsView.RowAutoHeight = true;
                        Main.PersonalGridView.OptionsView.ColumnAutoWidth = UserSettings.PersonalColumnAutoWidth;
                        DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
                        MemoEdit.WordWrap = true;
                        Main.PersonalGridView.OptionsView.RowAutoHeight = true;
                        Main.PersonalGridView.OptionsView.AllowCellMerge = UserSettings.PersonalCellMerge;
                        foreach (GridColumn My_GridColumn in Main.PersonalGridView.Columns)
                        {
                            My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                            My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                            My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            My_GridColumn.ColumnEdit = MemoEdit;
                        }
                        Main.PersonalSaveButton.Enabled = false;
                        Main.PersonalEditCheckBox.Checked = false;
                    }));
                    db.Dispose();
                }

                Main.Invoke(new System.Action(() =>
                {
                    //Main.PersonalGridView.FocusedRowChanged -= Main.PersonalGridView_FocusedRowChanged;
                    ColumnView View = Main.PersonalGridView;
                    GridColumn column = View.Columns["Id"];
                    if (column != null)
                    {
                        int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                        if (rhFound != GridControl.InvalidRowHandle)
                        {
                            View.FocusedRowHandle = rhFound;
                            View.FocusedColumn = column;
                        }
                    }
                    //Main.PersonalGridView.FocusedRowChanged += Main.PersonalGridView_FocusedRowChanged;
                }));
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MF.Invoke(new System.Action(() => MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
            }
        }

        internal void UpdateOpit()
        {
            Thread.Sleep(5000);
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Personal = db.Personal.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1).ToList();
                    var NewOpit = "";
                    foreach(var pers in Personal)
                    {
                        if(pers.OpitCalculate != null)
                        {
                            var Yea = 0;                           
                            var Mounts = ((DateTime.Now.Year - pers.OpitCalculate.Value.Year) * 12) + DateTime.Now.Month - pers.OpitCalculate.Value.Month;
                            while(Mounts > 11)
                            {
                                Mounts = Mounts - 12;
                                Yea = Yea++;
                            }
                            if(Yea > 0)
                            {
                                if(Yea < 2)
                                {
                                    NewOpit = "1 год";
                                } 
                                else
                                {
                                    if(Yea > 1 && Yea < 5)
                                    {
                                        NewOpit = Yea.ToString() + " год";
                                    }
                                    else
                                    {
                                        NewOpit = Yea.ToString() + " лет";
                                    }
                                }
                            }
                            if(Mounts > 0)
                            {
                                NewOpit = NewOpit + "";
                                if (Mounts < 2)
                                {
                                    NewOpit = NewOpit + "1 месяц";
                                }
                                else
                                {
                                    if (Mounts > 1 && Mounts < 5)
                                    {
                                        NewOpit = NewOpit + Mounts.ToString() + " месяца";
                                    }
                                    else
                                    {
                                        NewOpit = NewOpit + Mounts.ToString() + " месяцев";
                                    }
                                }
                            }
                            if(pers.Практический_опыт == NewOpit)
                            pers.Практический_опыт = NewOpit;
                        }
                    }
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void PersonalCopyButton_Click(MainForm Main)
        {
            try
            {
                if (Main.PersonalID.Text != "")
                {
                    Main.PersonalEditCheckBox.Enabled = false;
                    NewPersonal = false;
                    Main.PersonalNewButton.Text = "Добавить";
                    Main.PersonalNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    ActivateEditPersonal(true, Main);
                    NewPersonal = false;
                    CopyPersonal = true;
                    Main.PersonalSaveButton.Enabled = true;
                    Main.PersonalEditCheckBox.Checked = false;
                }
                else
                {
                    MessageBox.Show("Не выбран элемент!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public void PersonalGridControl_Click(MainForm Main)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gv = Main.PersonalGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                var Stroke = (Personal)gv.GetRow(gv.FocusedRowHandle);
                if (Stroke == null) return;
                SelectedRowId = Stroke.Id.ToString();
                Main.PersonalN.Text = Stroke.N.ToString();
                Main.PersonalID.Text = Stroke.Id.ToString();
                Main.PersonalName.Text = Stroke.ФИО;
                Main.PersonalDolgnost.Text = Stroke.Должность;
                Main.PersonalObrasovaniePoln.Text = Stroke.Образование__полное_;
                Main.PersonalObrasovanie.Text = Stroke.Образование;
                if (Stroke.Практический_опыт != null)
                {
                    var Opit = Stroke.Практический_опыт.Split(' ');
                    Main.PersonalOpitNumber.Value = Convert.ToInt32(Opit[0]);
                    if (Opit.Length > 1) { Main.PersonalOpitLet.Text = Opit[1]; }
                }
                Main.PersonalPovishKvalif.Text = Stroke.Повыш_квал;
                Main.PersonalSrokPodvergKvalif.Text = Stroke.Срок_подтверждения.ToString();
                Main.PersonalGas.Text = Stroke.Обучение_на_лаборанта.ToString();
                Main.PersonalSrokGasDate.Text = Stroke.Срок_подтверждения_лаборанта.ToString();
                Main.PersonalAttestechia.Text = Stroke.Аттестация.ToString();
                Main.PersonalSrokAttestachiiDate.Text = Stroke.Срок_аттестации.ToString();
                Main.PersonalZnaniaOTiPB.Text = Stroke.Проверка_знаний_по_ОТ_и_ТБ.ToString();
                Main.PersonalSrokZnaniiOTiPBDate.Text = Stroke.Срок_проверки_знаний_по_ОТ_и_ТБ.ToString();
                Main.PersonalDopolnitelno.Text = Stroke.Дополнительно;
                Main.PersonalEditCheckBox.Checked = false;
                ActivateEditPersonal(false, Main);
                Main.PersonalZnaniaOTiPBTip.Text = Stroke.Проверка_знаний_по_ОТ_и_ТБ.HasValue ? "дата" : "нет";
                Main.PersonalSrokZnaniiOTiPBTip.Text = Stroke.Срок_проверки_знаний_по_ОТ_и_ТБ.HasValue ? "дата" : "нет";
                Main.PersonalGasTip.Text = Stroke.Обучение_на_лаборанта.HasValue ? "дата" : "нет";
                Main.PersonalSrokGasTip.Text = Stroke.Срок_подтверждения_лаборанта.HasValue ? "дата" : "нет";
                Main.PersonalAttestechiaTip.Text = Stroke.Аттестация.HasValue ? "дата" : "нет";
                Main.PersonalSrokAttestachiiTip.Text = Stroke.Срок_аттестации.HasValue ? "дата" : "нет";
                Main.PersonalSrokPodvergKvalifTip.Text = Stroke.Срок_подтверждения.HasValue ? "дата" : "нет";
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void ActivateEditPersonal(bool EditableFields, MainForm Main)
        {
            try
            {
                if (EditableFields)
                {
                    foreach (Control c in Main.PersonalPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = false;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = true;
                            DTP.Checked = false;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = true;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.Enabled = true;
                        }
                    }
                    Main.PersonalSaveButton.Enabled = true;
                }
                else
                {
                    foreach (Control c in Main.PersonalPanel.Controls)
                    {
                        if (c is TextBox)
                        {
                            var TB = c as TextBox;
                            TB.ReadOnly = true;
                        }
                        if (c is DateTimePicker)
                        {
                            var DTP = c as DateTimePicker;
                            DTP.Enabled = false;
                            DTP.Checked = false;
                        }
                        if (c is ComboBox)
                        {
                            var CB = c as ComboBox;
                            CB.Enabled = false;
                        }
                        if (c is NumericUpDown)
                        {
                            var NUD = c as NumericUpDown;
                            NUD.Enabled = false;
                        }
                    }
                    NewPersonal = false;
                    Main.PersonalNewButton.Text = "Добавить";
                    Main.PersonalNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.PersonalSaveButton.Enabled = false;
                    Main.PersonalEditCheckBox.Checked = false;
                    Main.PersonalEditCheckBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        internal void PersonalVostanovitMenu_Click(MainForm Main, int id)
        {
            MF = Main;
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.Personal.Where(c => c.Id == id).FirstOrDefault();
                    Stroka.VisibleStatus = 0;
                    Stroka.ДатаСозданияЗаписи = DateTime.Now;
                    Stroka.UserName = UserSettings.User;
                    HistoryPersonal(Stroka, 400);
                    db.SubmitChanges();
                    db.Dispose();
                    if (!VisibleAll)
                    {
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.PersonalGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        if (gv.FocusedRowHandle > 0)
                        {
                            var Stroke = (Personal)gv.GetRow(gv.FocusedRowHandle - 1);
                            if (Stroke != null) SelectedRowId = Stroke.Id.ToString();  
                        }
                    }
                    GetPersonal(MF);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void PersonalUvolitMenu_Click(MainForm Main, int id)
        {
            MF = Main;
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.Personal.Where(c => c.Id == id).FirstOrDefault();
                    Stroka.VisibleStatus = 1;
                    Stroka.ДатаСозданияЗаписи = DateTime.Now;
                    Stroka.UserName = UserSettings.User;
                    HistoryPersonal(Stroka, 401);
                    db.SubmitChanges();
                    db.Dispose();
                    if (!VivibleUvolennie)
                    {
                        DevExpress.XtraGrid.Views.Grid.GridView gv = Main.PersonalGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                        if (gv.FocusedRowHandle > 0)
                        {
                            var Stroke = (Personal)gv.GetRow(gv.FocusedRowHandle - 1);
                            if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                        }
                    }
                    GetPersonal(MF);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void PersonalEditCheckBox_CheckedChanged(MainForm Main)
        {
            if (!NewPersonal && Main.PersonalID.Text != "")
            {
                ActivateEditPersonal(Main.PersonalEditCheckBox.Checked, Main);
            }
            else
            {
                Main.PersonalEditCheckBox.Checked = false;
            }
        }

        internal void PersonalPrint(object sender, DoWorkEventArgs e)
        {
            bool REPEAT = true;
            int ttt = 0;
            do
            {
                REPEAT = false;
                ttt++;
                try
                {
                    MF = e.Argument as MainForm;
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                    WordDoc1 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Персонал.dotx");
                    //WordDoc.SaveAs(System.Windows.Forms.Application.StartupPath + "//Temp/МастерЛист.doc");
                    var RowCount = MF.PersonalGridView.RowCount;
                    if (RowCount > 1) WordDoc1.AddTableCell(2, RowCount - 1);
                    int Cell = 1;
                    for (int rowHandle = 0; rowHandle < RowCount; rowHandle++)
                    {
                        Cell++;
                        if (MF.PersonalGridView.IsDataRow(rowHandle) || Cell >= 39)
                        {
                            try
                            {
                                var SelectedRow = (Personal)MF.PersonalGridView.GetRow(rowHandle);
                                WordDoc1.AddTableValue(2, Cell, 1, SelectedRow.N.ToString());
                                WordDoc1.AddTableValue(2, Cell, 2, SelectedRow.ФИО);
                                WordDoc1.AddTableValue(2, Cell, 3, SelectedRow.Должность);
                                WordDoc1.AddTableValue(2, Cell, 4, SelectedRow.Образование);
                                WordDoc1.AddTableValue(2, Cell, 5, SelectedRow.Образование__полное_);
                                WordDoc1.AddTableValue(2, Cell, 6, SelectedRow.Практический_опыт);
                                WordDoc1.AddTableValue(2, Cell, 7, SelectedRow.Повыш_квал);
                                WordDoc1.AddTableValue(2, Cell, 8, SelectedRow.Срок_подтверждения == null ? "" : SelectedRow.Срок_подтверждения.Value.ToShortDateString());
                                WordDoc1.AddTableValue(2, Cell, 9, SelectedRow.Обучение_на_лаборанта == null ? "" : SelectedRow.Обучение_на_лаборанта.Value.ToShortDateString());
                                WordDoc1.AddTableValue(2, Cell, 10, SelectedRow.Срок_подтверждения_лаборанта == null ? "" : SelectedRow.Срок_подтверждения_лаборанта.Value.ToShortDateString());
                                WordDoc1.AddTableValue(2, Cell, 11, SelectedRow.Аттестация == null ? "" : SelectedRow.Аттестация.Value.ToShortDateString());
                                WordDoc1.AddTableValue(2, Cell, 12, SelectedRow.Срок_аттестации == null ? "" : SelectedRow.Срок_аттестации.Value.ToShortDateString());
                                WordDoc1.AddTableValue(2, Cell, 13, SelectedRow.Проверка_знаний_по_ОТ_и_ТБ == null ? "" : SelectedRow.Проверка_знаний_по_ОТ_и_ТБ.Value.ToShortDateString());
                                WordDoc1.AddTableValue(2, Cell, 14, SelectedRow.Срок_проверки_знаний_по_ОТ_и_ТБ == null ? "" : SelectedRow.Срок_проверки_знаний_по_ОТ_и_ТБ.Value.ToShortDateString());
                                WordDoc1.AddTableValue(2, Cell, 15, SelectedRow.Дополнительно);
                            }
                            catch { }
                        }
                    }
                    if (UserSettings.PersonalColumns != null)
                    {
                        var Visibiliti = UserSettings.PersonalColumns.Split(',');
                        for (int i = Visibiliti.Length - 1; i > 0; i--)
                        {
                            if (Visibiliti[i] != "1")
                                WordDoc1.DeleteTableColumn(2, i + 1);
                        }
                    }
                    WordDoc1.TableResize(2);
                    WordDoc1.Visible = true;
                    WordDoc1.wordApplication.Activate();
                    MF.Invoke(new System.Action(() => MF.TopMost = false));
                }
                catch (Exception ex)
                {
                    try
                    {
                        LogErrors Log = new LogErrors(ex.ToString());
                        //MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        WordDoc1.Close();
                        if (ttt < 3)
                        {
                            REPEAT = true;
                            MF.Invoke(new System.Action(() => Thread.Sleep(1000)));
                        }
                        else
                        {
                            MF.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                        }
                    }
                    catch { }
                }
                finally
                {
                    try
                    {
                        MF.Invoke(new System.Action(() => MF.Loader.Visible = false));
                    }
                    catch { }
                }
            }
            while (REPEAT);
        }

        public void NewPersonalButton(MainForm Main)
        {
            try
            {
                if (NewPersonal)
                {
                    NewPersonal = false;
                    Main.PersonalNewButton.Text = "Добавить";
                    Main.PersonalNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
                    Main.PersonalSaveButton.Enabled = false;
                }
                else
                {
                    NewPersonal = true;
                    ClearControlsPersonal(Main);
                    Main.PersonalNewButton.Text = "Отменить";
                    Main.PersonalNewButton.Image = global::LIMS.Properties.Resources.cancel_16x16;
                    Main.PersonalSaveButton.Enabled = true;
                    Main.PersonalEditCheckBox.Enabled = true;
                }
                ActivateEditPersonal(Main.PersonalNewButton.Text == "Отменить", Main);
                Main.PersonalEditCheckBox.Checked = false;
                Main.PersonalEditCheckBox.Enabled = false;
                Main.PersonalSrokZnaniiOTiPBTip.Text = "дата";
                Main.PersonalSrokGasTip.Text = "дата";
                Main.PersonalSrokAttestachiiTip.Text = "дата";
                Main.PersonalZnaniaOTiPBTip.Text = "дата";
                Main.PersonalGasTip.Text = "дата";
                Main.PersonalAttestechiaTip.Text = "дата";
                Main.PersonalSrokPodvergKvalifTip.Text = "дата";
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void ClearControlsPersonal(MainForm Main)
        {
            foreach (Control c in Main.PersonalPanel.Controls)
            {
                if (c is TextBox)
                {
                    var TB = c as TextBox;
                    TB.Text = "";
                }
                if (c is DateTimePicker)
                {
                    var DTP = c as DateTimePicker;
                    DTP.Text = "";
                    DTP.Checked = false;
                }
                if (c is ComboBox)
                {
                    var CB = c as ComboBox;
                    CB.Text = "";
                }
                if (c is NumericUpDown)
                {
                    var NUD = c as NumericUpDown;
                    NUD.Text = "";
                }
                Main.PersonalID.Text = "";
            }
        }


        public void PersonalSaveButton_Click(MainForm Main)
        {
            try
            {
                try
                {
                    var t = Convert.ToInt32(Main.PersonalN.Text);
                }
                catch
                {
                    MessageBox.Show("Не корректное значения в поле \"№ п/п\"", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                using (DbDataContext db = new DbDataContext())
                {
                    if (!NewPersonal && !CopyPersonal)
                    {
                        if (Main.PersonalID.Text != "")
                        {
                            var EditID = Convert.ToInt32(Main.PersonalID.Text);
                            var Stroka = db.Personal.Where(c => c.Id == EditID).FirstOrDefault();
                            Stroka.Id = EditID;
                            Stroka.N = Convert.ToInt32(Main.PersonalN.Text);
                            Stroka.ФИО = Main.PersonalName.Text;
                            Stroka.Должность = Main.PersonalDolgnost.Text;
                            Stroka.Образование = Main.PersonalObrasovanie.Text;
                            Stroka.Образование__полное_ = Main.PersonalObrasovaniePoln.Text;
                            Stroka.Практический_опыт = Main.PersonalOpitNumber.Value.ToString() + " " + Main.PersonalOpitLet.Text;
                            Stroka.OpitCalculate = CalculateOpit(Stroka.Практический_опыт);
                            Stroka.Повыш_квал = Main.PersonalPovishKvalif.Text;
                            if (Main.PersonalSrokPodvergKvalifTip.Text != "нет")
                                Stroka.Срок_подтверждения = DateTime.Parse(Main.PersonalSrokPodvergKvalif.Text).Date;
                            else Stroka.Срок_подтверждения = null;
                            if (Main.PersonalGasTip.Text != "нет")
                                Stroka.Обучение_на_лаборанта = DateTime.Parse(Main.PersonalGas.Text).Date;
                            else Stroka.Обучение_на_лаборанта = null;
                            if (Main.PersonalSrokGasTip.Text != "нет")
                            {
                                if (Main.PersonalSrokGasTip.Text == "дата")
                                    Stroka.Срок_подтверждения_лаборанта = Main.PersonalSrokGasDate.Value.Date;
                                else
                                    if (Stroka.Обучение_на_лаборанта.HasValue) Stroka.Срок_подтверждения_лаборанта = Stroka.Обучение_на_лаборанта.Value.AddMonths(Convert.ToInt32(Main.PersonalSrokGasKol.Text)).Date;
                            }
                            else Stroka.Срок_подтверждения_лаборанта = null;
                            if (Main.PersonalAttestechiaTip.Text != "нет")
                                Stroka.Аттестация = DateTime.Parse(Main.PersonalAttestechia.Text).Date;
                            else
                                Stroka.Аттестация = null;
                            if (Main.PersonalSrokAttestachiiTip.Text != "нет")
                            {
                                if (Main.PersonalSrokAttestachiiTip.Text == "дата")
                                    Stroka.Срок_аттестации = Main.PersonalSrokAttestachiiDate.Value.Date;
                                else
                                    if (Stroka.Аттестация.HasValue) Stroka.Срок_аттестации = Stroka.Аттестация.Value.AddMonths(Convert.ToInt32(Main.PersonalSrokAttestachiiKol.Text)).Date;
                            }
                            else Stroka.Срок_аттестации = null;
                            if (Main.PersonalZnaniaOTiPBTip.Text != "нет")
                                Stroka.Проверка_знаний_по_ОТ_и_ТБ = DateTime.Parse(Main.PersonalZnaniaOTiPB.Text).Date;
                            else Stroka.Проверка_знаний_по_ОТ_и_ТБ = null;
                            if (Main.PersonalSrokZnaniiOTiPBTip.Text != "нет")
                            {
                                if (Main.PersonalSrokZnaniiOTiPBTip.Text == "дата")
                                    Stroka.Срок_проверки_знаний_по_ОТ_и_ТБ = Main.PersonalSrokZnaniiOTiPBDate.Value.Date;
                                else if (Stroka.Проверка_знаний_по_ОТ_и_ТБ.HasValue) Stroka.Срок_проверки_знаний_по_ОТ_и_ТБ = Stroka.Проверка_знаний_по_ОТ_и_ТБ.Value.AddMonths(Convert.ToInt32(Main.PersonalSrokZnaniiOTiPBKol.Text)).Date;
                            }
                            else Stroka.Срок_проверки_знаний_по_ОТ_и_ТБ = null;
                            Stroka.Дополнительно = Main.PersonalDopolnitelno.Text;
                            //Stroka.VisibleStatus = 0;
                            Stroka.ДатаСозданияЗаписи = DateTime.Now;
                            Stroka.UserName = UserSettings.User;
                            Stroka.Owner = null;
                            HistoryPersonal(Stroka, 200);
                            db.SubmitChanges();
                            GetPersonal(Main);
                            //Выбор стороки таблицы
                            int rowHandle = Main.PersonalGridView.LocateByValue("Id", int.Parse(Main.PersonalID.Text));
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                Main.PersonalGridView.FocusedRowHandle = rowHandle;
                        }

                    }
                    else
                    {
                        //Добавление                         
                        var NextID = db.Personal != null && db.Personal.Count() > 0 ? db.Personal.Max(c => c.Id) + 1 : 1;
                        DateTime? SP = null;
                        if (Main.PersonalSrokPodvergKvalifTip.Text != "нет") SP = DateTime.Parse(Main.PersonalSrokPodvergKvalif.Text);
                        DateTime? G = null;
                        if (Main.PersonalGasTip.Text != "нет") G = DateTime.Parse(Main.PersonalGas.Text);
                        DateTime? SG = null;
                        if (Main.PersonalSrokGasTip.Text != "нет")
                        {
                            if (Main.PersonalSrokGasTip.Text == "дата")
                                SG = Main.PersonalSrokGasDate.Value.Date;
                            else
                                if (G.HasValue) SG = G.Value.AddMonths(Convert.ToInt32(Main.PersonalSrokGasKol.Text)).Date;
                        }
                        DateTime? A = null;
                        if (Main.PersonalAttestechiaTip.Text != "нет") A = DateTime.Parse(Main.PersonalAttestechia.Text);
                        DateTime? SA = null;
                        if (Main.PersonalSrokAttestachiiTip.Text != "нет")
                        {
                            if (Main.PersonalSrokAttestachiiTip.Text == "дата")
                                SA = Main.PersonalSrokAttestachiiDate.Value.Date;
                            else
                                if (A.HasValue) SA = A.Value.AddMonths(Convert.ToInt32(Main.PersonalSrokAttestachiiKol.Text)).Date;
                        }

                        DateTime? ZO = null;
                        if (Main.PersonalZnaniaOTiPBTip.Text != "нет") ZO = DateTime.Parse(Main.PersonalZnaniaOTiPB.Text);
                        DateTime? SZO = null;
                        if (Main.PersonalSrokZnaniiOTiPBTip.Text != "нет")
                        {
                            if (Main.PersonalSrokZnaniiOTiPBTip.Text == "дата")
                                SZO = Main.PersonalSrokZnaniiOTiPBDate.Value.Date;
                            else
                                if (ZO.HasValue) SZO = ZO.Value.AddMonths(Convert.ToInt32(Main.PersonalSrokZnaniiOTiPBKol.Text)).Date;
                        }
                        db.Personal.InsertOnSubmit(new Personal
                        {
                            Id = NextID,
                            N = Convert.ToInt32(Main.PersonalN.Text),
                            ФИО = Main.PersonalName.Text,
                            Должность = Main.PersonalDolgnost.Text,
                            Образование = Main.PersonalObrasovanie.Text,
                            Образование__полное_ = Main.PersonalObrasovaniePoln.Text,
                            Практический_опыт = Main.PersonalOpitNumber.Value.ToString() + " " + Main.PersonalOpitLet.Text,
                            OpitCalculate = CalculateOpit(Main.PersonalOpitNumber.Value.ToString() + " " + Main.PersonalOpitLet.Text),
                            Повыш_квал = Main.PersonalPovishKvalif.Text,
                            Срок_подтверждения = SP,
                            Обучение_на_лаборанта = G,
                            Срок_подтверждения_лаборанта = SG,
                            Аттестация = A,
                            Срок_аттестации = SA,
                            Проверка_знаний_по_ОТ_и_ТБ = ZO,
                            Срок_проверки_знаний_по_ОТ_и_ТБ = SZO,
                            Дополнительно = Main.PersonalDopolnitelno.Text,
                            VisibleStatus = 0,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = null
                        });
                        db.Personal.InsertOnSubmit(new Personal
                        {
                            Id = NextID + 1,
                            N = Convert.ToInt32(Main.PersonalN.Text),
                            ФИО = Main.PersonalName.Text,
                            Должность = Main.PersonalDolgnost.Text,
                            Образование = Main.PersonalObrasovanie.Text,
                            Образование__полное_ = Main.PersonalObrasovaniePoln.Text,
                            Практический_опыт = Main.PersonalOpitNumber.Value.ToString() + " " + Main.PersonalOpitLet.Text,
                            OpitCalculate = CalculateOpit(Main.PersonalOpitNumber.Value.ToString() + " " + Main.PersonalOpitLet.Text),
                            Повыш_квал = Main.PersonalPovishKvalif.Text,
                            Срок_подтверждения = SP,
                            Обучение_на_лаборанта = G,
                            Срок_подтверждения_лаборанта = SG,
                            Аттестация = A,
                            Срок_аттестации = SA,
                            Проверка_знаний_по_ОТ_и_ТБ = ZO,
                            Срок_проверки_знаний_по_ОТ_и_ТБ = SZO,
                            Дополнительно = Main.PersonalDopolnitelno.Text,
                            VisibleStatus = 100,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = NextID
                        });
                        db.SubmitChanges();
                        NewPersonalButton(Main);
                        GetPersonal(Main);
                        ActivateEditPersonal(false, Main);
                        Main.PersonalSaveButton.Enabled = false;
                    }
                    NewPersonal = false;
                    CopyPersonal = false;
                    db.Dispose();
                    Main.PersonalEditCheckBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private DateTime? CalculateOpit(string практический_опыт)
        {
            try
            {
                DateTime? Opit = null;
                var opits = практический_опыт.Split(' ');
                if (opits.Length > 0)
                {
                    if (opits.Length > 1)
                    {
                        if (opits[1].Contains("год") || opits[1].Contains("лет"))
                        {
                            try
                            {
                                Opit = Opit == null ? DateTime.Now.AddYears((-1) * Convert.ToInt32(opits[0])) : Opit.Value.AddYears((-1) * Convert.ToInt32(opits[0]));
                            }
                            catch { }
                        }
                    }
                    else
                    {
                        if (opits[1].Contains("мес"))
                        {
                            try
                            {
                                Opit = Opit == null ? DateTime.Now.AddMonths((-1) * Convert.ToInt32(opits[0])) : Opit.Value.AddMonths((-1) * Convert.ToInt32(opits[0]));
                            }
                            catch { }
                        }
                        else
                        {
                            try
                            {
                                Opit = Opit == null ? DateTime.Now.AddYears((-1) * Convert.ToInt32(opits[0])) : Opit.Value.AddYears((-1) * Convert.ToInt32(opits[0]));
                            }
                            catch { }
                        }
                    }
                    if (opits.Length > 2)
                    {
                        try
                        {
                            Opit = Opit == null ? DateTime.Now.AddMonths((-1) * Convert.ToInt32(opits[2])) : Opit.Value.AddMonths((-1) * Convert.ToInt32(opits[2]));
                        }
                        catch { }
                    }
                }               
                else return null;
                return Opit;
            }
            catch (Exception ex)
            {                
                LogErrors Log = new LogErrors(ex.ToString());
                return null;
            }
        }

        private void HistoryPersonal(Personal stroka, int Kod)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.Personal.InsertOnSubmit(new Personal
                    {
                        Id = db.Personal != null && db.Personal.Count() > 0 ? db.Personal.Max(c => c.Id) + 1 : 1,
                        N = stroka.N,
                        ФИО = stroka.ФИО,
                        Должность = stroka.Должность,
                        Образование = stroka.Образование,
                        Образование__полное_ = stroka.Образование__полное_,
                        Практический_опыт = stroka.Практический_опыт,
                        Повыш_квал = stroka.Повыш_квал,
                        Срок_подтверждения = stroka.Срок_подтверждения,
                        Обучение_на_лаборанта = stroka.Обучение_на_лаборанта,
                        Срок_подтверждения_лаборанта = stroka.Срок_подтверждения_лаборанта,
                        Аттестация = stroka.Аттестация,
                        Срок_аттестации = stroka.Срок_аттестации,
                        Проверка_знаний_по_ОТ_и_ТБ = stroka.Проверка_знаний_по_ОТ_и_ТБ,
                        Срок_проверки_знаний_по_ОТ_и_ТБ = stroka.Срок_проверки_знаний_по_ОТ_и_ТБ,
                        Дополнительно = stroka.Дополнительно,
                        VisibleStatus = Kod,
                        ДатаСозданияЗаписи = DateTime.Now,
                        UserName = UserSettings.User,
                        Owner = stroka.Id,
                        OpitCalculate = stroka.OpitCalculate
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void PersonalGridView_ColumnWidthChanged(MainForm MF)
        {
            try
            {
                if (ChangeWidth)
                {
                    UserSettings.PersonalColumnWidth = "";
                    for (int i = 1; i < 16; i++)
                    {
                        if (UserSettings.PersonalColumnWidth != "")
                            UserSettings.PersonalColumnWidth = UserSettings.PersonalColumnWidth + ",";
                        UserSettings.PersonalColumnWidth = UserSettings.PersonalColumnWidth + MF.PersonalGridView.Columns[i].Width.ToString();
                    }
                    UserSettings.SaveSettings();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void PersonalDeleteButton_Click(MainForm Main)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    //Удаление                
                    if (Main.PersonalID.Text != "")
                    {
                        DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить \"" + Main.PersonalName.Text + "\"?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            var Строка3 = db.Personal.Where(c => c.Id == Convert.ToInt32(Main.PersonalID.Text)).FirstOrDefault();
                            if (Строка3 != null)
                            {
                                Строка3.VisibleStatus = Строка3.Owner == null ? 300 : 301;
                                Строка3.ДатаСозданияЗаписи = DateTime.Now;
                                Строка3.UserName = UserSettings.User;
                                //db.Personal.DeleteOnSubmit(Строка3);
                                db.SubmitChanges();
                            }
                            DevExpress.XtraGrid.Views.Grid.GridView gv = Main.PersonalGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                            if (gv.FocusedRowHandle > 0)
                            {
                                var Stroke = (Personal)gv.GetRow(gv.FocusedRowHandle - 1);
                                if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                            }
                            ClearControlsPersonal(Main);
                            GetPersonal(Main);
                            ActivateEditPersonal(false, Main);                            
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не выбрана строка для удаления!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }


        public void CheckColumnButton_Click(MainForm Main)
        {
            try
            {
                CheckPersonalColumnForm f = new CheckPersonalColumnForm(Main, this);
                f.Owner = Main;
                f.ShowDialog();
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        internal void PersonalSrokGasTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.PersonalSrokGasTip.Text == "дата")
            {
                Main.PersonalSrokGasDate.Visible = true;
                Main.PersonalSrokGasKol.Visible = false;
            }
            else if (Main.PersonalSrokGasTip.Text == "нет")
            {
                Main.PersonalSrokGasDate.Visible = false;
                Main.PersonalSrokGasKol.Visible = false;
            }
            else
            {
                Main.PersonalSrokGasDate.Visible = false;
                Main.PersonalSrokGasKol.Visible = true;
            }
        }

        internal void PersonalSrokAttestachiiTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.PersonalSrokAttestachiiTip.Text == "дата")
            {
                Main.PersonalSrokAttestachiiDate.Visible = true;
                Main.PersonalSrokAttestachiiKol.Visible = false;
            }
            else if (Main.PersonalSrokAttestachiiTip.Text == "нет")
            {
                Main.PersonalSrokAttestachiiDate.Visible = false;
                Main.PersonalSrokAttestachiiKol.Visible = false;
            }
            else
            {
                Main.PersonalSrokAttestachiiDate.Visible = false;
                Main.PersonalSrokAttestachiiKol.Visible = true;
            }
        }

        internal void PersonalSrokZnaniiOTiPBTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.PersonalSrokZnaniiOTiPBTip.Text == "дата")
            {
                Main.PersonalSrokZnaniiOTiPBDate.Visible = true;
                Main.PersonalSrokZnaniiOTiPBKol.Visible = false;
            }
            else if (Main.PersonalSrokZnaniiOTiPBTip.Text == "нет")
            {
                Main.PersonalSrokZnaniiOTiPBDate.Visible = false;
                Main.PersonalSrokZnaniiOTiPBKol.Visible = false;
            }
            else
            {
                Main.PersonalSrokZnaniiOTiPBDate.Visible = false;
                Main.PersonalSrokZnaniiOTiPBKol.Visible = true;
            }
        }

        internal void AutoWidthColumn(MainForm MF)
        {
            if (UserSettings.PersonalColumnAutoWidth)
            {
                UserSettings.PersonalColumnAutoWidth = false;
            }
            else UserSettings.PersonalColumnAutoWidth = true;
            GetPersonal(MF);
            UserSettings.SaveSettings();
        }

        internal void PersonalUvolenButton_Click(MainForm Main)
        {
            if (Main.PersonalUvolenButton.ContextMenuStrip != null)
            {
                Main.PersonalUvolenButton.ContextMenuStrip.Close();
                Main.PersonalUvolenButton.ContextMenuStrip = null;
                return;
            }
            MF = Main;
            ContextMenuStrip ContextMS = new ContextMenuStrip();
            ContextMS.Items.Add("&Показать текущий персонал", null, VisibleAll_click);
            ContextMS.Items.Add("&Показать уволенный персонал", null, VivibleUvolennie_Click);
            ContextMS.Items.Add("&Закрыть", global::LIMS.Properties.Resources.delete_16x16, ContextMenuStripClose_Click);
            if (VisibleAll) ((ToolStripMenuItem)ContextMS.Items[0]).Checked = true;
            else ((ToolStripMenuItem)ContextMS.Items[0]).Checked = false;
            if (VivibleUvolennie) ((ToolStripMenuItem)ContextMS.Items[1]).Checked = true;
            else ((ToolStripMenuItem)ContextMS.Items[1]).Checked = false;
            ContextMS.AutoClose = false;
            Main.PersonalUvolenButton.ContextMenuStrip = ContextMS;
            ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);
        }

        public void ContextMenuStripClose_Click(object sender, EventArgs e)
        {
            var TSMI = sender as ToolStripMenuItem;
            var CMS = TSMI.Owner as ContextMenuStrip;
            CMS.AutoClose = true;
            MF.PersonalUvolenButton.ContextMenuStrip = null;
            CMS.Close();
        }

        private void VivibleUvolennie_Click(object sender, EventArgs e)
        {
            VivibleUvolennie = VivibleUvolennie ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = VivibleUvolennie;
            GetPersonal(MF);
        }

        private void VisibleAll_click(object sender, EventArgs e)
        {
            VisibleAll = VisibleAll ? false : true;
            var TSMI = sender as ToolStripMenuItem;
            TSMI.Checked = VisibleAll;
            GetPersonal(MF);
        }

        internal void PersonalSrokPodvergKvalifTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.PersonalSrokPodvergKvalifTip.Text == "дата")
                Main.PersonalSrokPodvergKvalif.Visible = true;
            else Main.PersonalSrokPodvergKvalif.Visible = false;
        }

        internal void PersonalGasTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.PersonalGasTip.Text == "дата")
                Main.PersonalGas.Visible = true;
            else Main.PersonalGas.Visible = false;
        }

        internal void PersonalAttestechiaTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.PersonalAttestechiaTip.Text == "дата")
                Main.PersonalAttestechia.Visible = true;
            else Main.PersonalAttestechia.Visible = false;
        }

        internal void PersonalZnaniaOTiPBTip_SelectedIndexChanged(MainForm Main)
        {
            if (Main.PersonalZnaniaOTiPBTip.Text == "дата")
                Main.PersonalZnaniaOTiPB.Visible = true;
            else Main.PersonalZnaniaOTiPB.Visible = false;
        }

        internal void PersonalCellMergeButton_Click(MainForm Main)
        {
            UserSettings.PersonalCellMerge = !UserSettings.PersonalCellMerge;
            Main.PersonalGridView.OptionsView.AllowCellMerge = UserSettings.PersonalCellMerge;
            if (UserSettings.PersonalCellMerge)
                Main.PersonalCellMergeButton.Image = global::LIMS.Properties.Resources.freezetoprow_32x32;
            else Main.PersonalCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            UserSettings.SaveSettings();
        }

        internal void splitContainerControl1_Panel1_SizeChanged(object sender)
        {
            try
            {
                var SplitCon = sender as DevExpress.XtraEditors.SplitGroupPanel;
                UserSettings.PersonalSplitWidth = SplitCon.Width;
                UserSettings.SaveSettings();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
