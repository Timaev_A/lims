﻿using System;
using System.Linq;
using System.Threading;
namespace LIMS
{
    class LogErrors
    {
        private string Error_ = "";
        private string User_ = "";
        public LogErrors(string Error, string User = "")
        {
            Error_ = Error;
            User_ = User;
            new Thread(SendLog).Start();
        }

        private void SendLog()
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var NextID = db.Logs != null && db.Logs.Count() > 0 ? db.Logs.Max(c => c.Id) + 1 : 1;
                    db.Logs.InsertOnSubmit(new Logs
                    {
                        Id = NextID,
                        Message = Error_,
                        ДатаИВремя = DateTime.Now.ToString(),
                        Пользователь = User_
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch
            {
            }
        }
    }
}

