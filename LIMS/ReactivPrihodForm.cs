﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class ReactivPrihodForm : DevExpress.XtraEditors.XtraForm
    {
        MainForm Main;
        bool NewReact = false;
        int? ReactivID;
        public ReactivPrihodForm(MainForm MF, Reactiv Stroke, bool NewReactiv = false)
        {
            InitializeComponent();
            ReactivID = Stroke.Id;
            ID.Text = Stroke.Id.ToString();
            ReactivName.Text = Stroke.НаименованиеРеактива;
            Partia.Text = "Партия: " + Stroke.НомерПартии;
            DateIzgot.Text = Stroke.ДатаИзготовления.HasValue ? "Дата изготовления: " + Stroke.ДатаИзготовления.Value.ToShortDateString() : "";
            var now = DateTime.Now.AddDays(-10);
            Ot.Value = new DateTime(now.Year, now.Month, 1, 0, 0, 0);
            Do.Value = new DateTime(now.Year, now.Month + 1, 1, 0, 0, 0).AddDays(-1);
            if (NewReactiv) Kolvo.Value = Stroke.Остаток.HasValue ? Stroke.Остаток.Value : 0;
            NewReact = NewReactiv;
            Main = MF;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.Reactiv.Where(c => c.Id == Convert.ToInt32(ID.Text) && (c.VisibleStatus == null || c.VisibleStatus < 4)).FirstOrDefault();
                    if (Stroka.Остаток.HasValue && Kolvo.Value > 0)
                    {
                        if (!NewReact) Stroka.Остаток = Stroka.Остаток + Kolvo.Value;
                        else Stroka.Остаток = Kolvo.Value;
                        var result = MessageBox.Show("Pеактив: " + Stroka.НаименованиеРеактива + "\rПартия: " + Stroka.НомерПартии + "\rПриход: " + Kolvo.Value.ToString() + " " + Stroka.ЕдИзм + "\rОстаток: " + Stroka.Остаток.Value.ToString() + " " + Stroka.ЕдИзм, "Принять?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (result == DialogResult.OK)
                        {
                            ReactivUchet ReactivUCHET;
                            if (Stroka.ДатаИзготовления.HasValue)
                                ReactivUCHET = db.ReactivUchet.Where(c => c.НаименованиеРеактива == Stroka.НаименованиеРеактива && c.Партия == Stroka.НомерПартии && c.Квалификация == Stroka.Классификация && c.ДатаИзготовления == Stroka.ДатаИзготовления && c.От == Ot.Value && c.До == Do.Value && (c.VisibleStatus == null || c.VisibleStatus < 3)).FirstOrDefault();
                            else ReactivUCHET = db.ReactivUchet.Where(c => c.НаименованиеРеактива == Stroka.НаименованиеРеактива && c.Партия == Stroka.НомерПартии && c.Квалификация == Stroka.Классификация && c.ДатаИзготовления == null && c.От == Ot.Value && c.До == Do.Value && (c.VisibleStatus == null || c.VisibleStatus < 3)).FirstOrDefault();
                            var NextID = db.ReactivUchet != null && db.ReactivUchet.Count() > 0 ? db.ReactivUchet.Max(c => c.Id) + 1 : 1;
                            if (ReactivUCHET == null)
                            {
                                db.ReactivUchet.InsertOnSubmit(new ReactivUchet
                                {
                                    Id = NextID,
                                    MainID = Convert.ToInt32(ID.Text),
                                    НаименованиеРеактива = ReactivName.Text,
                                    Квалификация = Stroka.Классификация,
                                    Партия = Stroka.НомерПартии,
                                    ДатаИзготовления = Stroka.ДатаИзготовления,
                                    От = Ot.Value,
                                    До = Do.Value,
                                    Приход = Kolvo.Value,
                                    Расход = 0,
                                    Остаток = Stroka.Остаток,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    VisibleStatus = 0,
                                    Owner = null,
                                    ReactivID = ReactivID,
                                });
                                db.ReactivUchet.InsertOnSubmit(new ReactivUchet
                                {
                                    Id = NextID + 1,
                                    MainID = Convert.ToInt32(ID.Text),
                                    НаименованиеРеактива = ReactivName.Text,
                                    Квалификация = Stroka.Классификация,
                                    Партия = Stroka.НомерПартии,
                                    ДатаИзготовления = Stroka.ДатаИзготовления,
                                    От = Ot.Value,
                                    До = Do.Value,
                                    Приход = Kolvo.Value,
                                    Расход = 0,
                                    Остаток = Stroka.Остаток,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    VisibleStatus = 100,
                                    Owner = NextID,
                                    ReactivID = ReactivID,
                                });
                            }
                            else
                            {
                                ReactivUCHET.Приход = ReactivUCHET.Приход + Kolvo.Value;
                                ReactivUCHET.Расход = ReactivUCHET.Расход;
                                ReactivUCHET.Остаток = Stroka.Остаток;
                                ReactivUCHET.ReactivID = ReactivID;
                                ReactivUCHET.UserName = UserSettings.User;
                                ReactivUCHET.ДатаСозданияЗаписи = DateTime.Now;
                                ReactivUCHET.VisibleStatus = 0;
                                db.ReactivUchet.InsertOnSubmit(new ReactivUchet
                                {
                                    Id = NextID,
                                    MainID = ReactivUCHET.ReactivID,
                                    НаименованиеРеактива = ReactivUCHET.НаименованиеРеактива,
                                    Квалификация = ReactivUCHET.Квалификация,
                                    Партия = ReactivUCHET.Партия,
                                    ДатаИзготовления = ReactivUCHET.ДатаИзготовления,
                                    От = ReactivUCHET.От,
                                    До = ReactivUCHET.До,
                                    Приход = ReactivUCHET.Приход,
                                    Расход = ReactivUCHET.Расход,
                                    Остаток = ReactivUCHET.Остаток,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    VisibleStatus = 200,
                                    Owner = ReactivUCHET.Id,
                                    ReactivID = ReactivUCHET.ReactivID,
                                });
                            }
                            db.SubmitChanges();
                            var result2 = MessageBox.Show("Создать акт входного контроля?", "Создание акта...", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result2 == DialogResult.Yes)
                            {
                                ReactivPrintAktvVhodnogoKontrolaForm f = new ReactivPrintAktvVhodnogoKontrolaForm(Main, Convert.ToInt32(ID.Text), true);
                                f.Owner = Main;
                                Main.Visible = false;
                                this.Visible = false;
                                f.ShowDialog();
                                this.Visible = false;
                                Main.Visible = true;
                                f.Dispose();
                            }
                            this.Close();
                        }
                        else return;
                    }
                    else
                    {
                        return;//MessageBox.Show("Остаток реактива " + Stroka.НаименованиеРеактива + " меньше, чем вы собираетесь списывать!");
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Ot_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void ReactivPrihodForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
