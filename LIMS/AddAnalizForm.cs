﻿using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;

namespace LIMS
{
    public partial class AddAnalizForm : DevExpress.XtraEditors.XtraForm
    {
        int AnalizID = -1;
        int Higth = 0;
        public AddAnalizForm(MainForm Main)
        {
            InitializeComponent();
            List<AnalizTable> Data = new List<AnalizTable>();
            Data.Add(new AnalizTable { });
            AnalizGrid.DataSource = Data;
            AnalizGridView.Columns[1].Caption = "От";
            AnalizGridView.Columns[2].Caption = "До";
            AnalizGridView.Columns[3].Caption = "r";
            AnalizGridView.Columns[4].Caption = "R";
            AnalizGridView.Columns[5].Caption = "\u2206";
            AnalizGridView.Columns[6].Caption = "Кол-во измерений";
            AnalizGridView.Columns[0].Visible = false;
            AnalizGridView.Columns[7].Visible = false;
            AnalizGridView.Columns[8].Visible = false;
            AnalizGridView.Columns[9].Visible = false;
            AnalizGridView.Columns[10].Visible = false;
            AnalizGridView.Columns[11].Visible = false;
            AnalizGridView.Columns[12].Visible = false;
            CotextMenuEvent(Main);
        }
        public AddAnalizForm(MainForm Main, int AnalizIDIN)
        {
            InitializeComponent();
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var EditAnaliz = db.Analiz.Where(c => c.Id == AnalizIDIN && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                    if (EditAnaliz != null)
                    {
                        AnalizID = EditAnaliz.Id;
                        ND.Text = EditAnaliz.НД;
                        AnalizEdIzm.Text = EditAnaliz.ЕдиницаИзмерения;
                        AnalizName.Text = EditAnaliz.НазваниеМетодаАнализа;
                        AnalizOpredelyaimiiKomponent.Text = EditAnaliz.ОпределяемыйКомпонент;
                        AnalizKontroliruemiiObekt.Text = EditAnaliz.КонтролируемыйОбъект;
                        AnalizShifr.Text = EditAnaliz.Шифр;
                        ShortName.Text = EditAnaliz.ShortName;
                        CalculateProfile.Text = EditAnaliz.CalculateProfile;
                    }
                    if (EditAnaliz.CalculateProfile == null || EditAnaliz.CalculateProfile == "")
                    {
                        CheckCalculate.Checked = false;
                        CheckTabl.Checked = true;
                    }
                    else
                    {
                        CheckCalculate.Checked = true;
                        CheckTabl.Checked = false;
                    }
                    var AnalizTable = db.AnalizTable.Where(c => c.AnalizID == AnalizIDIN && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderBy(c => c.Id).ToList();
                    if (AnalizTable != null && AnalizTable.Count > 0)
                    {
                        AnalizGrid.DataSource = AnalizTable;
                    }
                    else
                    {
                        List<AnalizTable> Data = new List<AnalizTable>();
                        Data.Add(new AnalizTable { });
                        AnalizGrid.DataSource = Data;
                    }
                    AnalizGridView.Columns[1].Caption = "От";
                    AnalizGridView.Columns[2].Caption = "До";
                    AnalizGridView.Columns[3].Caption = "r";
                    AnalizGridView.Columns[4].Caption = "R";
                    AnalizGridView.Columns[5].Caption = "\u2206";
                    AnalizGridView.Columns[6].Caption = "Кол-во измерений";
                    AnalizGridView.Columns[0].Visible = false;
                    AnalizGridView.Columns[7].Visible = false;
                    AnalizGridView.Columns[8].Visible = false;
                    AnalizGridView.Columns[9].Visible = false;
                    AnalizGridView.Columns[10].Visible = false;
                    AnalizGridView.Columns[11].Visible = false;
                    AnalizGridView.Columns[12].Visible = false;
                    db.Dispose();
                    CotextMenuEvent(Main);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CotextMenuEvent(MainForm Main)
        {
            foreach (Control C in panelControl1.Controls)
            {
                if (C is TextBox || C is ComboBox)
                {
                    C.ContextMenuStrip = Main.ContextMenuStripMain;
                }
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    if (CheckCalculate.Checked)
                        if (CalculateProfile.Text == "")
                        {
                            MessageBox.Show("Не корректные данные!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    if (AnalizID == -1) //Создание
                    {
                        if (AnalizName.Text != "")
                        {
                            var NextID = db.Analiz != null && db.Analiz.Count() > 0 ? db.Analiz.Max(c => c.Id) + 1 : 1;
                            db.Analiz.InsertOnSubmit(new Analiz
                            {
                                Id = NextID,
                                НазваниеМетодаАнализа = AnalizName.Text,
                                НД = ND.Text,
                                ЕдиницаИзмерения = AnalizEdIzm.Text,
                                КонтролируемыйОбъект = AnalizKontroliruemiiObekt.Text,
                                ОпределяемыйКомпонент = AnalizOpredelyaimiiKomponent.Text,
                                Шифр = AnalizShifr.Text,
                                UserName = UserSettings.User,
                                Owner = null,
                                VisibleStatus = 0,
                                ДатаСозданияЗаписи = DateTime.Now,
                                ShortName = ShortName.Text,
                                CalculateProfile = CalculateProfile.Text,
                            });
                            //История
                            db.Analiz.InsertOnSubmit(new Analiz
                            {
                                Id = NextID + 1,
                                НазваниеМетодаАнализа = AnalizName.Text,
                                НД = ND.Text,
                                ЕдиницаИзмерения = AnalizEdIzm.Text,
                                КонтролируемыйОбъект = AnalizKontroliruemiiObekt.Text,
                                ОпределяемыйКомпонент = AnalizOpredelyaimiiKomponent.Text,
                                ShortName = ShortName.Text,
                                Шифр = AnalizShifr.Text,
                                UserName = UserSettings.User,
                                Owner = NextID,
                                VisibleStatus = 100,
                                ДатаСозданияЗаписи = DateTime.Now,
                                CalculateProfile = CalculateProfile.Text,
                            });
                            //Таблица
                            var NextIDTable = db.AnalizTable != null && db.AnalizTable.Count() > 0 ? db.AnalizTable.Max(c => c.Id) + 1 : 0;
                            var NewDataTable = (List<AnalizTable>)AnalizGridView.DataSource;
                            var TablDontDel = new List<AnalizTable>();
                            foreach (var NewStroke in NewDataTable)
                            {
                                db.AnalizTable.InsertOnSubmit(new AnalizTable
                                {
                                    Id = NextIDTable,
                                    AnalizID = NextID,
                                    Owner = null,
                                    Down = NewStroke.Down,
                                    Up = NewStroke.Up,
                                    Q = NewStroke.Q,
                                    RBig = NewStroke.RBig,
                                    RSmall = NewStroke.RSmall,
                                    КолВоПроб = NewStroke.КолВоПроб,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    VisibleStatus = 0,
                                });
                                NextIDTable++;
                                db.AnalizTable.InsertOnSubmit(new AnalizTable
                                {
                                    Id = NextIDTable,
                                    AnalizID = NextID + 1,
                                    Owner = NextIDTable - 1,
                                    Down = NewStroke.Down,
                                    Up = NewStroke.Up,
                                    Q = NewStroke.Q,
                                    RBig = NewStroke.RBig,
                                    RSmall = NewStroke.RSmall,
                                    КолВоПроб = NewStroke.КолВоПроб,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    VisibleStatus = 100,
                                });
                                NextIDTable++;
                            }
                        }
                        else MessageBox.Show("Не задано \"Название метода анализа\"!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else // изменение
                    {
                        if (AnalizName.Text != "")
                        {
                            var AnalizStroke = db.Analiz.Where(c => c.Id == AnalizID && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                            var AnalizTable = db.AnalizTable.Where(c => c.AnalizID == AnalizID && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderBy(c => c.Id).ToList();
                            var NextID = db.Analiz != null && db.Analiz.Count() > 0 ? db.Analiz.Max(c => c.Id) + 1 : 1;
                            if (AnalizStroke != null)
                            {
                                AnalizStroke.НазваниеМетодаАнализа = AnalizName.Text;
                                AnalizStroke.НД = ND.Text;
                                AnalizStroke.ЕдиницаИзмерения = AnalizEdIzm.Text;
                                AnalizStroke.КонтролируемыйОбъект = AnalizKontroliruemiiObekt.Text;
                                AnalizStroke.ОпределяемыйКомпонент = AnalizOpredelyaimiiKomponent.Text;
                                AnalizStroke.Шифр = AnalizShifr.Text;
                                AnalizStroke.ShortName = ShortName.Text;
                                AnalizStroke.UserName = UserSettings.User;
                                AnalizStroke.Owner = null;
                                AnalizStroke.VisibleStatus = 0;
                                AnalizStroke.ДатаСозданияЗаписи = DateTime.Now;
                                AnalizStroke.CalculateProfile = CalculateProfile.Text;
                                //История
                                db.Analiz.InsertOnSubmit(new Analiz
                                {
                                    Id = NextID,
                                    НазваниеМетодаАнализа = AnalizName.Text,
                                    НД = ND.Text,
                                    ЕдиницаИзмерения = AnalizEdIzm.Text,
                                    КонтролируемыйОбъект = AnalizKontroliruemiiObekt.Text,
                                    ОпределяемыйКомпонент = AnalizOpredelyaimiiKomponent.Text,
                                    Шифр = AnalizShifr.Text,
                                    ShortName = ShortName.Text,
                                    UserName = UserSettings.User,
                                    Owner = AnalizStroke.Id,
                                    VisibleStatus = 200,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    CalculateProfile = CalculateProfile.Text,
                                });
                                //Таблица
                                var NextIDTable = db.AnalizTable != null && db.AnalizTable.Count() > 0 ? db.AnalizTable.Max(c => c.Id) + 1 : 0;
                                var NewDataTable = (List<AnalizTable>)AnalizGridView.DataSource;
                                var TablDontDel = new List<AnalizTable>();
                                foreach (var NewStroke in NewDataTable)
                                {
                                    //Изменение
                                    if (AnalizTable != null && AnalizTable.Contains(NewStroke))
                                    {
                                        TablDontDel.Add(NewStroke);
                                        var TItem = db.AnalizTable.Where(c => c.Id == NewStroke.Id).FirstOrDefault();
                                        TItem.Down = NewStroke.Down;
                                        TItem.Up = NewStroke.Up;
                                        TItem.Q = NewStroke.Q;
                                        TItem.RBig = NewStroke.RBig;
                                        TItem.RSmall = NewStroke.RSmall;
                                        TItem.КолВоПроб = NewStroke.КолВоПроб;
                                        TItem.UserName = UserSettings.User;
                                        TItem.ДатаСозданияЗаписи = DateTime.Now;
                                        TItem.VisibleStatus = 0;

                                        db.AnalizTable.InsertOnSubmit(new AnalizTable
                                        {
                                            Id = NextIDTable,
                                            AnalizID = NewStroke.AnalizID,
                                            Owner = NewStroke.Id,
                                            Down = NewStroke.Down,
                                            Up = NewStroke.Down,
                                            Q = NewStroke.Q,
                                            RBig = NewStroke.RBig,
                                            RSmall = NewStroke.RSmall,
                                            КолВоПроб = NewStroke.КолВоПроб,
                                            UserName = UserSettings.User,
                                            ДатаСозданияЗаписи = DateTime.Now,
                                            VisibleStatus = 200,
                                        });
                                        NextIDTable++;
                                    }
                                    else //Создание новой
                                    {
                                        db.AnalizTable.InsertOnSubmit(new AnalizTable
                                        {
                                            Id = NextIDTable,
                                            AnalizID = AnalizID,
                                            Owner = null,
                                            Down = NewStroke.Down,
                                            Up = NewStroke.Up,
                                            Q = NewStroke.Q,
                                            RBig = NewStroke.RBig,
                                            RSmall = NewStroke.RSmall,
                                            КолВоПроб = NewStroke.КолВоПроб,
                                            UserName = UserSettings.User,
                                            ДатаСозданияЗаписи = DateTime.Now,
                                            VisibleStatus = 0,
                                        });
                                        NextIDTable++;
                                        db.AnalizTable.InsertOnSubmit(new AnalizTable
                                        {
                                            Id = NextIDTable,
                                            AnalizID = NextID,
                                            Owner = NextIDTable - 1,
                                            Down = NewStroke.Down,
                                            Up = NewStroke.Up,
                                            Q = NewStroke.Q,
                                            RBig = NewStroke.RBig,
                                            RSmall = NewStroke.RSmall,
                                            КолВоПроб = NewStroke.КолВоПроб,
                                            UserName = UserSettings.User,
                                            ДатаСозданияЗаписи = DateTime.Now,
                                            VisibleStatus = 100,
                                        });
                                        NextIDTable++;
                                    }
                                }
                                //Удаление оставшихся
                                if (AnalizTable != null)
                                {
                                    foreach (var Stroke in AnalizTable)
                                    {
                                        if (TablDontDel == null || !TablDontDel.Contains(Stroke))
                                        {
                                            Stroke.VisibleStatus = Stroke.Owner == null ? 300 : 301;
                                            Stroke.ДатаСозданияЗаписи = DateTime.Now;
                                            Stroke.UserName = UserSettings.User;
                                        }
                                    }
                                }
                            }
                            else MessageBox.Show("Ошибка сохранения!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else MessageBox.Show("Не задано \"Название метода анализа\"!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    db.SubmitChanges();
                    db.Dispose();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddAnalizForm_Load(object sender, EventArgs e)
        {            
        }

        private void AnalizGridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            GridView view = sender as GridView;
            DevExpress.XtraGrid.Views.Grid.GridView gv = AnalizGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (AnalizTable)gv.GetRow(gv.FocusedRowHandle);
            e.Menu.Items.Clear();

            DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Добавить строку",
            view.OptionsView.AllowCellMerge, null, new EventHandler(AnalizTableAddButton_Click));
            e.Menu.Items.Add(checkItem5);

            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
            {
                int rowHandle = e.HitInfo.RowHandle;

                DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                view.OptionsView.AllowCellMerge, null, new EventHandler(AnalizTableDeleteButton_Click));
                checkItem3.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem3);
            }
        }

        class RowInfo
        {
            public RowInfo(GridView view, int rowHandle)
            {
                this.RowHandle = rowHandle;
                this.View = view;
            }
            public GridView View;
            public int RowHandle;
        }

        private void AnalizTableAddButton_Click(object sender, EventArgs e)
        {
            var data = ((List<AnalizTable>)AnalizGridView.DataSource);
            data.Add(new AnalizTable { });
            AnalizGrid.DataSource = data;
            AnalizGrid.RefreshDataSource();
        }

        private void AnalizTableDeleteButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = AnalizGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (AnalizTable)gv.GetRow(gv.FocusedRowHandle);
            var data = ((List<AnalizTable>)AnalizGridView.DataSource);
            data.Remove(Stroke);
            AnalizGrid.DataSource = data;
            AnalizGrid.RefreshDataSource();
        }

        private void AnalizName_TextChanged(object sender, EventArgs e)
        {
            if (AnalizID <= 0)
                ShortName.Text = AnalizName.Text;
        }

        private void CheckCalculate_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckCalculate.Checked)
            {
                CalculateProfile.Visible = true;
                AnalizContainerControl.SplitterPosition = 295;
                Higth = Size.Height;
                Size = new System.Drawing.Size(Size.Width, 295 + 72);
            }
            else
            {
                CalculateProfile.Visible = false;
                AnalizContainerControl.SplitterPosition = 264;
                Size = new System.Drawing.Size(Size.Width, Higth);
            }
        }

        private void EnterKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void AddAnalizForm_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
