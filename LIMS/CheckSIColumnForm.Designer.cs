﻿namespace LIMS
{
    partial class CheckSIColumnForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckSIColumnForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.CheckEditPravoSobstvennosti = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditPrimechanie = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateSledTO = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDiapozomIzmerenii = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditInventarniiN = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDatePoslednegoTO = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditGodVvodaVEksplyatachiu = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditIzgotovitel = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditN = new DevExpress.XtraEditors.CheckEdit();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.CancelButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEditMestoUstanovkiIHranenia = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateSledPoverki = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditSrokDeistvia = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDatePoverki = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditSvidetelstvoOPoverke = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditClassTochnosti = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditGodVipuska = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditZavN = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditTip = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditName = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditNameHaracteristic = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPravoSobstvennosti.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrimechanie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateSledTO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDiapozomIzmerenii.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditInventarniiN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDatePoslednegoTO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGodVvodaVEksplyatachiu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditIzgotovitel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditMestoUstanovkiIHranenia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateSledPoverki.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokDeistvia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDatePoverki.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSvidetelstvoOPoverke.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditClassTochnosti.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGodVipuska.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditZavN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditTip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNameHaracteristic.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.CheckEditPravoSobstvennosti);
            this.panelControl1.Controls.Add(this.CheckEditPrimechanie);
            this.panelControl1.Controls.Add(this.CheckEditDateSledTO);
            this.panelControl1.Controls.Add(this.CheckEditDiapozomIzmerenii);
            this.panelControl1.Controls.Add(this.CheckEditInventarniiN);
            this.panelControl1.Controls.Add(this.CheckEditDatePoslednegoTO);
            this.panelControl1.Controls.Add(this.CheckEditGodVvodaVEksplyatachiu);
            this.panelControl1.Controls.Add(this.CheckEditIzgotovitel);
            this.panelControl1.Controls.Add(this.CheckEditN);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelButton1);
            this.panelControl1.Controls.Add(this.CheckEditMestoUstanovkiIHranenia);
            this.panelControl1.Controls.Add(this.CheckEditDateSledPoverki);
            this.panelControl1.Controls.Add(this.CheckEditSrokDeistvia);
            this.panelControl1.Controls.Add(this.CheckEditDatePoverki);
            this.panelControl1.Controls.Add(this.CheckEditSvidetelstvoOPoverke);
            this.panelControl1.Controls.Add(this.CheckEditClassTochnosti);
            this.panelControl1.Controls.Add(this.CheckEditGodVipuska);
            this.panelControl1.Controls.Add(this.CheckEditZavN);
            this.panelControl1.Controls.Add(this.CheckEditTip);
            this.panelControl1.Controls.Add(this.CheckEditName);
            this.panelControl1.Controls.Add(this.CheckEditNameHaracteristic);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(506, 292);
            this.panelControl1.TabIndex = 0;
            // 
            // CheckEditPravoSobstvennosti
            // 
            this.CheckEditPravoSobstvennosti.EditValue = true;
            this.CheckEditPravoSobstvennosti.Location = new System.Drawing.Point(295, 137);
            this.CheckEditPravoSobstvennosti.Name = "CheckEditPravoSobstvennosti";
            this.CheckEditPravoSobstvennosti.Properties.Caption = "Право собственности";
            this.CheckEditPravoSobstvennosti.Size = new System.Drawing.Size(171, 19);
            this.CheckEditPravoSobstvennosti.TabIndex = 22;
            // 
            // CheckEditPrimechanie
            // 
            this.CheckEditPrimechanie.EditValue = true;
            this.CheckEditPrimechanie.Location = new System.Drawing.Point(295, 237);
            this.CheckEditPrimechanie.Name = "CheckEditPrimechanie";
            this.CheckEditPrimechanie.Properties.Caption = "Примечание";
            this.CheckEditPrimechanie.Size = new System.Drawing.Size(157, 19);
            this.CheckEditPrimechanie.TabIndex = 19;
            this.CheckEditPrimechanie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDateSledTO
            // 
            this.CheckEditDateSledTO.EditValue = true;
            this.CheckEditDateSledTO.Location = new System.Drawing.Point(295, 212);
            this.CheckEditDateSledTO.Name = "CheckEditDateSledTO";
            this.CheckEditDateSledTO.Properties.Caption = "Дата следующего ТО";
            this.CheckEditDateSledTO.Size = new System.Drawing.Size(157, 19);
            this.CheckEditDateSledTO.TabIndex = 18;
            this.CheckEditDateSledTO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDiapozomIzmerenii
            // 
            this.CheckEditDiapozomIzmerenii.EditValue = true;
            this.CheckEditDiapozomIzmerenii.Location = new System.Drawing.Point(12, 237);
            this.CheckEditDiapozomIzmerenii.Name = "CheckEditDiapozomIzmerenii";
            this.CheckEditDiapozomIzmerenii.Properties.Caption = "Диапазон измерений";
            this.CheckEditDiapozomIzmerenii.Size = new System.Drawing.Size(217, 19);
            this.CheckEditDiapozomIzmerenii.TabIndex = 10;
            this.CheckEditDiapozomIzmerenii.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditInventarniiN
            // 
            this.CheckEditInventarniiN.EditValue = true;
            this.CheckEditInventarniiN.Location = new System.Drawing.Point(12, 212);
            this.CheckEditInventarniiN.Name = "CheckEditInventarniiN";
            this.CheckEditInventarniiN.Properties.Caption = "Инвентарный номер";
            this.CheckEditInventarniiN.Size = new System.Drawing.Size(217, 19);
            this.CheckEditInventarniiN.TabIndex = 9;
            this.CheckEditInventarniiN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDatePoslednegoTO
            // 
            this.CheckEditDatePoslednegoTO.EditValue = true;
            this.CheckEditDatePoslednegoTO.Location = new System.Drawing.Point(295, 187);
            this.CheckEditDatePoslednegoTO.Name = "CheckEditDatePoslednegoTO";
            this.CheckEditDatePoslednegoTO.Properties.Caption = "Дата последнего ТО";
            this.CheckEditDatePoslednegoTO.Size = new System.Drawing.Size(171, 19);
            this.CheckEditDatePoslednegoTO.TabIndex = 17;
            this.CheckEditDatePoslednegoTO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditGodVvodaVEksplyatachiu
            // 
            this.CheckEditGodVvodaVEksplyatachiu.EditValue = true;
            this.CheckEditGodVvodaVEksplyatachiu.Location = new System.Drawing.Point(12, 187);
            this.CheckEditGodVvodaVEksplyatachiu.Name = "CheckEditGodVvodaVEksplyatachiu";
            this.CheckEditGodVvodaVEksplyatachiu.Properties.Caption = "Год ввода в эксплуатацию";
            this.CheckEditGodVvodaVEksplyatachiu.Size = new System.Drawing.Size(217, 19);
            this.CheckEditGodVvodaVEksplyatachiu.TabIndex = 8;
            this.CheckEditGodVvodaVEksplyatachiu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditIzgotovitel
            // 
            this.CheckEditIzgotovitel.EditValue = true;
            this.CheckEditIzgotovitel.Location = new System.Drawing.Point(12, 162);
            this.CheckEditIzgotovitel.Name = "CheckEditIzgotovitel";
            this.CheckEditIzgotovitel.Properties.Caption = "Изготовитель";
            this.CheckEditIzgotovitel.Size = new System.Drawing.Size(217, 19);
            this.CheckEditIzgotovitel.TabIndex = 7;
            this.CheckEditIzgotovitel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditN
            // 
            this.CheckEditN.EditValue = true;
            this.CheckEditN.Location = new System.Drawing.Point(12, 12);
            this.CheckEditN.Name = "CheckEditN";
            this.CheckEditN.Properties.Caption = "№ п/п";
            this.CheckEditN.Size = new System.Drawing.Size(112, 19);
            this.CheckEditN.TabIndex = 1;
            this.CheckEditN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(190, 262);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 20;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton1
            // 
            this.CancelButton1.Location = new System.Drawing.Point(271, 262);
            this.CancelButton1.Name = "CancelButton1";
            this.CancelButton1.Size = new System.Drawing.Size(75, 23);
            this.CancelButton1.TabIndex = 21;
            this.CancelButton1.Text = "Отмена";
            this.CancelButton1.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CheckEditMestoUstanovkiIHranenia
            // 
            this.CheckEditMestoUstanovkiIHranenia.EditValue = true;
            this.CheckEditMestoUstanovkiIHranenia.Location = new System.Drawing.Point(295, 162);
            this.CheckEditMestoUstanovkiIHranenia.Name = "CheckEditMestoUstanovkiIHranenia";
            this.CheckEditMestoUstanovkiIHranenia.Properties.Caption = "Место установки или хранения";
            this.CheckEditMestoUstanovkiIHranenia.Size = new System.Drawing.Size(184, 19);
            this.CheckEditMestoUstanovkiIHranenia.TabIndex = 16;
            this.CheckEditMestoUstanovkiIHranenia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDateSledPoverki
            // 
            this.CheckEditDateSledPoverki.EditValue = true;
            this.CheckEditDateSledPoverki.Location = new System.Drawing.Point(295, 112);
            this.CheckEditDateSledPoverki.Name = "CheckEditDateSledPoverki";
            this.CheckEditDateSledPoverki.Properties.Caption = "Дата следующей поверки";
            this.CheckEditDateSledPoverki.Size = new System.Drawing.Size(171, 19);
            this.CheckEditDateSledPoverki.TabIndex = 15;
            this.CheckEditDateSledPoverki.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditSrokDeistvia
            // 
            this.CheckEditSrokDeistvia.EditValue = true;
            this.CheckEditSrokDeistvia.Location = new System.Drawing.Point(295, 87);
            this.CheckEditSrokDeistvia.Name = "CheckEditSrokDeistvia";
            this.CheckEditSrokDeistvia.Properties.Caption = "Срок действия";
            this.CheckEditSrokDeistvia.Size = new System.Drawing.Size(109, 19);
            this.CheckEditSrokDeistvia.TabIndex = 14;
            this.CheckEditSrokDeistvia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDatePoverki
            // 
            this.CheckEditDatePoverki.EditValue = true;
            this.CheckEditDatePoverki.Location = new System.Drawing.Point(295, 62);
            this.CheckEditDatePoverki.Name = "CheckEditDatePoverki";
            this.CheckEditDatePoverki.Properties.Caption = "Дата поверки";
            this.CheckEditDatePoverki.Size = new System.Drawing.Size(109, 19);
            this.CheckEditDatePoverki.TabIndex = 13;
            this.CheckEditDatePoverki.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditSvidetelstvoOPoverke
            // 
            this.CheckEditSvidetelstvoOPoverke.EditValue = true;
            this.CheckEditSvidetelstvoOPoverke.Location = new System.Drawing.Point(295, 37);
            this.CheckEditSvidetelstvoOPoverke.Name = "CheckEditSvidetelstvoOPoverke";
            this.CheckEditSvidetelstvoOPoverke.Properties.Caption = "Свидетельство о поверке СИ, номер";
            this.CheckEditSvidetelstvoOPoverke.Size = new System.Drawing.Size(212, 19);
            this.CheckEditSvidetelstvoOPoverke.TabIndex = 12;
            this.CheckEditSvidetelstvoOPoverke.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditClassTochnosti
            // 
            this.CheckEditClassTochnosti.EditValue = true;
            this.CheckEditClassTochnosti.Location = new System.Drawing.Point(295, 12);
            this.CheckEditClassTochnosti.Name = "CheckEditClassTochnosti";
            this.CheckEditClassTochnosti.Properties.Caption = "Класс точности, погрешность";
            this.CheckEditClassTochnosti.Size = new System.Drawing.Size(193, 19);
            this.CheckEditClassTochnosti.TabIndex = 11;
            this.CheckEditClassTochnosti.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditGodVipuska
            // 
            this.CheckEditGodVipuska.EditValue = true;
            this.CheckEditGodVipuska.Location = new System.Drawing.Point(12, 137);
            this.CheckEditGodVipuska.Name = "CheckEditGodVipuska";
            this.CheckEditGodVipuska.Properties.Caption = "Год выпуска";
            this.CheckEditGodVipuska.Size = new System.Drawing.Size(127, 19);
            this.CheckEditGodVipuska.TabIndex = 6;
            this.CheckEditGodVipuska.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditZavN
            // 
            this.CheckEditZavN.EditValue = true;
            this.CheckEditZavN.Location = new System.Drawing.Point(12, 112);
            this.CheckEditZavN.Name = "CheckEditZavN";
            this.CheckEditZavN.Properties.Caption = "Заводской номер";
            this.CheckEditZavN.Size = new System.Drawing.Size(145, 19);
            this.CheckEditZavN.TabIndex = 5;
            this.CheckEditZavN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditTip
            // 
            this.CheckEditTip.EditValue = true;
            this.CheckEditTip.Location = new System.Drawing.Point(12, 87);
            this.CheckEditTip.Name = "CheckEditTip";
            this.CheckEditTip.Properties.Caption = "Тип";
            this.CheckEditTip.Size = new System.Drawing.Size(112, 19);
            this.CheckEditTip.TabIndex = 4;
            this.CheckEditTip.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditName
            // 
            this.CheckEditName.EditValue = true;
            this.CheckEditName.Location = new System.Drawing.Point(12, 62);
            this.CheckEditName.Name = "CheckEditName";
            this.CheckEditName.Properties.Caption = "Наименование СИ";
            this.CheckEditName.Size = new System.Drawing.Size(112, 19);
            this.CheckEditName.TabIndex = 3;
            this.CheckEditName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditNameHaracteristic
            // 
            this.CheckEditNameHaracteristic.EditValue = true;
            this.CheckEditNameHaracteristic.Location = new System.Drawing.Point(12, 37);
            this.CheckEditNameHaracteristic.Name = "CheckEditNameHaracteristic";
            this.CheckEditNameHaracteristic.Properties.Caption = "Наименование опр-мых характеристик продукции";
            this.CheckEditNameHaracteristic.Size = new System.Drawing.Size(277, 19);
            this.CheckEditNameHaracteristic.TabIndex = 2;
            this.CheckEditNameHaracteristic.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckSIColumnForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 292);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "CheckSIColumnForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ СИ";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckSIColumnForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPravoSobstvennosti.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrimechanie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateSledTO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDiapozomIzmerenii.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditInventarniiN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDatePoslednegoTO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGodVvodaVEksplyatachiu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditIzgotovitel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditMestoUstanovkiIHranenia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateSledPoverki.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokDeistvia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDatePoverki.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSvidetelstvoOPoverke.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditClassTochnosti.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGodVipuska.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditZavN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditTip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNameHaracteristic.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit CheckEditNameHaracteristic;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateSledPoverki;
        private DevExpress.XtraEditors.CheckEdit CheckEditSrokDeistvia;
        private DevExpress.XtraEditors.CheckEdit CheckEditDatePoverki;
        private DevExpress.XtraEditors.CheckEdit CheckEditSvidetelstvoOPoverke;
        private DevExpress.XtraEditors.CheckEdit CheckEditClassTochnosti;
        private DevExpress.XtraEditors.CheckEdit CheckEditGodVipuska;
        private DevExpress.XtraEditors.CheckEdit CheckEditZavN;
        private DevExpress.XtraEditors.CheckEdit CheckEditTip;
        private DevExpress.XtraEditors.CheckEdit CheckEditName;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.SimpleButton CancelButton1;
        private DevExpress.XtraEditors.CheckEdit CheckEditMestoUstanovkiIHranenia;
        private DevExpress.XtraEditors.CheckEdit CheckEditN;
        private DevExpress.XtraEditors.CheckEdit CheckEditIzgotovitel;
        private DevExpress.XtraEditors.CheckEdit CheckEditGodVvodaVEksplyatachiu;
        private DevExpress.XtraEditors.CheckEdit CheckEditDatePoslednegoTO;
        private DevExpress.XtraEditors.CheckEdit CheckEditInventarniiN;
        private DevExpress.XtraEditors.CheckEdit CheckEditDiapozomIzmerenii;
        private DevExpress.XtraEditors.CheckEdit CheckEditPrimechanie;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateSledTO;
        private DevExpress.XtraEditors.CheckEdit CheckEditPravoSobstvennosti;
    }
}