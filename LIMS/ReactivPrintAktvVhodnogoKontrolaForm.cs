﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class ReactivPrintAktvVhodnogoKontrolaForm : DevExpress.XtraEditors.XtraForm
    {
        MainForm Main;
        WordDocument WordDoc1 = new WordDocument();
        WordDocument WordDoc2 = new WordDocument();
        bool Changed = false;
        int? ReactivID;
        public ReactivPrintAktvVhodnogoKontrolaForm(MainForm MF, int id, bool Prihod = false)
        {
            try
            {
                InitializeComponent();
                ReactivID = id;
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroke = db.Reactiv.Where(c => c.Id == id && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                    if (Stroke != null)
                    {
                        Main = MF;
                        Komissia1.DataSource = GlobalStatic.GetPersonal();
                        Komissia2.DataSource = GlobalStatic.GetPersonal();
                        Komissia3.DataSource = GlobalStatic.GetPersonal();
                        Komissia4.DataSource = GlobalStatic.GetPersonal();
                        Komissia1.Text = "";
                        Komissia2.Text = "";

                        ReactivName.Text = Stroke.НаименованиеРеактива;
                        Class.Text = Stroke.Классификация;
                        ND.Text = Stroke.НДНаРеактив;
                        Partia.Text = Stroke.НомерПартии;
                        DateIzgot.Text = Stroke.ДатаИзготовления.HasValue ? Stroke.ДатаИзготовления.Value.ToShortDateString() : "";
                        if (Stroke.СрокГодностиDate != null && Stroke.ДатаИзготовления != null)
                        {
                            var days = (Stroke.СрокГодностиDate.Value - Stroke.ДатаИзготовления.Value).Days;
                            if (days < 370)
                            {
                                if (360 < days) SrokGodnosti.Text = "1 год";
                                else SrokGodnosti.Text = Math.Round((double)days / 30, 0) + " месяцев";
                            }
                            else
                            {
                                if (Math.Round((double)days / 365, 1) < 5)
                                {
                                    SrokGodnosti.Text = Math.Round((double)days / 365, 1).ToString() + " года";
                                }
                                else SrokGodnosti.Text = Math.Round((double)days / 365, 1).ToString() + " лет";
                            }
                        }
                        DateGet.Text = Stroke.ДатаПолучения.HasValue ? Stroke.ДатаПолучения.Value.ToShortDateString() : "";
                        //OtherDate1.Text = DateTime.Now.Date.ToString().Substring(0, 10);
                        //OtherDate2.Text = DateTime.Now.Date.ToString().Substring(0, 10);
                        //OtherDate3.Text = DateTime.Now.Date.ToString().Substring(0, 10);
                        //OtherDate4.Text = DateTime.Now.Date.ToString().Substring(0, 10);
                        //OtherDate5.Text = DateTime.Now.Date.ToString().Substring(0, 10);
                        //OtherDate6.Text = DateTime.Now.Date.ToString().Substring(0, 10);
                        //OtherDate7.Text = DateTime.Now.Date.ToString().Substring(0, 10);
                        GodenDo.Text = Stroke.СрокГодности;
                        AktCreateDate.Value = DateTime.Now.Date;
                        TextWasChanged();
                    }
                    db.Dispose();
                }
                Changed = Prihod;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public ReactivPrintAktvVhodnogoKontrolaForm(MainForm MF, ReactivAktVhodnogoKontrolya Stroke, bool Prihod = false)
        {
            try
            {
                InitializeComponent();
                using (DbDataContext db = new DbDataContext())
                {
                    if (Stroke.ReactivID.HasValue)
                    {
                        ReactivID = Stroke.ReactivID.Value;
                    }
                    else
                    {
                        var ReactivThis = db.Reactiv.Where(c => c.НаименованиеРеактива == Stroke.НаименованиеРеактива && c.Классификация == Stroke.Классификация && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                        if (ReactivThis != null) ReactivID = ReactivThis.Id; else ReactivID = null;
                    }
                    db.Dispose();
                }
                TextWasChanged();
                Main = MF;
                Komissia1.DataSource = GlobalStatic.GetPersonal();
                Komissia2.DataSource = GlobalStatic.GetPersonal();
                Komissia3.DataSource = GlobalStatic.GetPersonal();
                Komissia4.DataSource = GlobalStatic.GetPersonal();
                Komissia1.Text = "";
                Komissia2.Text = "";
                AktCreateDate.Value = Stroke.ДатаСозданияАкта.Value.Date;
                ID.Text = Stroke.Id.ToString();
                ReactivName.Text = Stroke.НаименованиеРеактива;
                Class.Text = Stroke.Классификация;
                ND.Text = Stroke.НДНаРеактив;
                Partia.Text = Stroke.Партия;
                DateIzgot.Text = Stroke.ДатаИзготовления.HasValue ? Stroke.ДатаИзготовления.Value.ToShortDateString() : "";
                SrokGodnosti.Text = Stroke.СрокГодности;
                DateGet.Text = Stroke.ДатаПолучения.HasValue ? Stroke.ДатаПолучения.Value.ToShortDateString() : "";
                GodenDo.Text = Stroke.ГоденДо.HasValue ? Stroke.ГоденДо.Value.ToShortDateString() : "";
                
                using (DbDataContext db = new DbDataContext())
                {
                    var Other = db.ReactivAktVhodnogoKontrolyaOther.Where(c => c.MainTableId == Stroke.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                    if (Other != null && Other.Count > 0)
                    {
                        OtherText1.Text = Other[0].Текст;
                        OtherNumber1.Text = Other[0].Число;
                        OtherEdIzm1.Text = Other[0].ЕдиницаИзмерения;
                        OtherDate1.Text = Other[0].Дата;
                        if (Other.Count > 1)
                        {
                            OtherText2.Text = Other[1].Текст;
                            OtherNumber2.Text = Other[1].Число;
                            OtherEdIzm2.Text = Other[1].ЕдиницаИзмерения;
                            OtherDate2.Text = Other[1].Дата;
                            OtherText2.Visible = true;
                            OtherDate2.Visible = true;
                            OtherEdIzm2.Visible = true;
                            OtherNumber2.Visible = true;
                            if (Other.Count > 2)
                            {
                                OtherText3.Text = Other[2].Текст;
                                OtherNumber3.Text = Other[2].Число;
                                OtherEdIzm3.Text = Other[2].ЕдиницаИзмерения;
                                OtherDate3.Text = Other[2].Дата;
                                OtherText3.Visible = true;
                                OtherDate3.Visible = true;
                                OtherEdIzm3.Visible = true;
                                OtherNumber3.Visible = true;
                                if (Other.Count > 3)
                                {
                                    OtherText4.Text = Other[3].Текст;
                                    OtherNumber4.Text = Other[3].Число;
                                    OtherEdIzm4.Text = Other[3].ЕдиницаИзмерения;
                                    OtherDate4.Text = Other[3].Дата;
                                    OtherText4.Visible = true;
                                    OtherDate4.Visible = true;
                                    OtherEdIzm4.Visible = true;
                                    OtherNumber4.Visible = true;
                                    if (Other.Count > 4)
                                    {
                                        OtherText5.Text = Other[4].Текст;
                                        OtherNumber5.Text = Other[4].Число;
                                        OtherEdIzm5.Text = Other[4].ЕдиницаИзмерения;
                                        OtherDate5.Text = Other[4].Дата;
                                        OtherText5.Visible = true;
                                        OtherDate5.Visible = true;
                                        OtherEdIzm5.Visible = true;
                                        OtherNumber5.Visible = true;
                                        if (Other.Count > 5)
                                        {
                                            OtherText6.Text = Other[5].Текст;
                                            OtherNumber6.Text = Other[5].Число;
                                            OtherEdIzm6.Text = Other[5].ЕдиницаИзмерения;
                                            OtherDate6.Text = Other[5].Дата;
                                            OtherText6.Visible = true;
                                            OtherDate6.Visible = true;
                                            OtherEdIzm6.Visible = true;
                                            OtherNumber6.Visible = true;
                                            if (Other.Count > 6)
                                            {
                                                OtherText7.Text = Other[6].Текст;
                                                OtherNumber7.Text = Other[6].Число;
                                                OtherEdIzm7.Text = Other[6].ЕдиницаИзмерения;
                                                OtherDate7.Text = Other[6].Дата;
                                                OtherText7.Visible = true;
                                                OtherDate7.Visible = true;
                                                OtherEdIzm7.Visible = true;
                                                OtherNumber7.Visible = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    var Table = db.ReactivAktVhodnogoKontrolyaTable.Where(c => c.MainTableId == Stroke.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                    if (Table != null && Table.Count > 0)
                    {
                        if (Table.Count > 0)
                        {
                            ReactivAktVhodnoiGridView.Rows.Add(Table.Count);
                        }
                        for (int rows = 0; rows < Table.Count; rows++)
                        {
                            ReactivAktVhodnoiGridView.Rows[rows].Cells[0].Value = Table[rows].ДатаКонтроля;
                            ReactivAktVhodnoiGridView.Rows[rows].Cells[1].Value = Table[rows].ГСО;
                            ReactivAktVhodnoiGridView.Rows[rows].Cells[2].Value = Table[rows].АттестованноеЗначениеГСО;
                            ReactivAktVhodnoiGridView.Rows[rows].Cells[3].Value = Table[rows].ДатаВыпуска;
                            ReactivAktVhodnoiGridView.Rows[rows].Cells[4].Value = Table[rows].СрокГодностиГСО;
                            ReactivAktVhodnoiGridView.Rows[rows].Cells[5].Value = Table[rows].РезультатИзмерений;
                            ReactivAktVhodnoiGridView.Rows[rows].Cells[6].Value = Table[rows].РезультатКонтроля;
                            ReactivAktVhodnoiGridView.Rows[rows].Cells[7].Value = Table[rows].НормативКонтроля;
                        }
                    }

                    var Komissia = db.ReactivAktVhodnogoKontrolyaKomissiya.Where(c => c.MainTableId == Stroke.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                    if (Komissia != null && Komissia.Count > 0)
                    {
                        Komissia1Name.Text = Komissia[0].Должность;
                        Komissia1.Text = Komissia[0].ФИО;
                        if (Komissia.Count > 1)
                        {
                            Komissia2Name.Text = Komissia[1].Должность;
                            Komissia2.Text = Komissia[1].ФИО;
                            if (Komissia.Count > 2)
                            {
                                Komissia3Name.Text = Komissia[2].Должность;
                                Komissia3.Text = Komissia[2].ФИО;
                                if (Komissia.Count > 3)
                                {
                                    Komissia4Name.Text = Komissia[3].Должность;
                                    Komissia4.Text = Komissia[3].ФИО;
                                }
                            }
                        }
                    }
                    db.SubmitChanges();
                    db.Dispose();
                }
                TextWasChanged();
                Changed = Prihod;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void TextWasChanged()
        {
            foreach (Control x in ReactivAktVhodnPanel.Controls)
            {
                if (x is TextBox || x is ComboBox)
                {
                    x.TextChanged += X_TextChanged;
                }
            }
        }

        private void X_TextChanged(object sender, EventArgs e)
        {
            Changed = true;
        }

        private void ViewButton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveAkt();
                this.TopMost = false;
                WordDoc1 = new WordDocument(Application.StartupPath + @"\\Templates\\Шаблон Входной контроль.dotx");
                EditTable(ref WordDoc1);
                Replace(ref WordDoc1);
                //WordDoc.SaveAs(Application.StartupPath + "//Temp/Шаблон Входной контроль.doc");
                WordDoc1.Visible = true;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WordDoc1.Close();
            }
        }

        private void EditTable(ref WordDocument WordDoc)
        {
            List<string> GridData = new List<string>();
            foreach (DataGridViewRow row in ReactivAktVhodnoiGridView.Rows)
            {
                var Rows = ReactivAktVhodnoiGridView.Rows.Count;
                if (Rows - 1 > row.Index)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        string value = cell.Value == null ? "" : cell.Value.ToString();
                        GridData.Add(value);
                    }
                }
            }

            bool LastRowNull = true;
            do
            {
                var count = GridData.Count;
                if (count > 0)
                {
                    for (int i = count - 1; i > count - 9; i--)
                    {
                        LastRowNull = LastRowNull == true ? GridData[i] == "" : false;
                    }
                    if (LastRowNull)
                    {
                        for (int i = count - 1; i > count - 9; i--)
                        {
                            GridData.RemoveAt(i);
                        }
                    }
                }
                else LastRowNull = false;
            }
            while (LastRowNull);
            var GSO = "";
            var GSOZnachenie = "";
            var DateVipuska = "";
            var SrokGodnosti = "";
            if (GridData.Count / 8 > 1)
            {
                WordDoc.AddTableCell(2, GridData.Count / 8 - 1);
                for (int i = 1; i < GridData.Count / 8 + 1; i++)
                {
                    if (GSO != "" && GSOZnachenie != "" && DateVipuska != "" && SrokGodnosti != "")
                    {
                        GSO = GSO + ", ";
                        GSOZnachenie = GSOZnachenie + ", ";
                        DateVipuska = DateVipuska + ", ";
                        SrokGodnosti = SrokGodnosti + ", ";
                    }
                    GSO = GSO + i + ") " + GridData[(i - 1) * 8 + 1];
                    GSOZnachenie = GSOZnachenie + i + ") " + GridData[(i - 1) * 8 + 2];
                    DateVipuska = DateVipuska + i + ") " + GridData[(i - 1) * 8 + 3];
                    SrokGodnosti = SrokGodnosti + i + ") " + GridData[(i - 1) * 8 + 4];
                }
                GSO = GSO + ".";
                GSOZnachenie = GSOZnachenie + ".";
                DateVipuska = DateVipuska + ".";
                SrokGodnosti = SrokGodnosti + ".";
            }
            else
            {
                if (GridData.Count > 5)
                {
                    GSO = GridData[1];
                    GSOZnachenie = GridData[2];
                    DateVipuska = GridData[3];
                    SrokGodnosti = GridData[4];
                }
            }
            WordDoc.ReplaceString("{ГСО}", GSO);
            WordDoc.ReplaceString("{Аттес Знач}", GSOZnachenie);
            WordDoc.ReplaceString("{Дата выпуска}", DateVipuska == "" ? "" : DateVipuska + "г.");
            WordDoc.ReplaceString("{Срок годности ГСО}", SrokGodnosti);
            for (int Row = 2; Row < GridData.Count / 8 + 2; Row++)
            {
                for (int Cell = 1; Cell < 9; Cell++)
                {
                    var CellT = 0;
                    if (Cell == 4 || Cell == 5) continue;
                    if (Cell < 4) CellT = Cell;
                    else CellT = Cell - 2;
                    WordDoc.AddTableValue(2, Row, CellT, GridData[(Row - 2) * 8 + (Cell - 1)]);
                }
            }
        }

        private void Replace(ref WordDocument WordDoc)
        {
            string ReactivNameTr = "";
            Invoke(new System.Action(() => ReactivNameTr = ReactivName.Text));
            string ClassTr = "";
            Invoke(new System.Action(() => ClassTr = Class.Text));
            string НД = "";
            Invoke(new System.Action(() => НД = ND.Text));
            string Партия = "";
            Invoke(new System.Action(() => Партия = Partia.Text));
            string Датаизготовления = "";
            Invoke(new System.Action(() => Датаизготовления = DateIzgot.Text == "" ? "" : DateIzgot.Text + "г."));
            string Срокгодности = "";
            Invoke(new System.Action(() => Срокгодности = SrokGodnosti.Text));
            string Датаполучения = "";
            Invoke(new System.Action(() => Датаполучения = DateGet.Text == "" ? "" : DateGet.Text + "г."));
            string Годендо = "";
            Invoke(new System.Action(() => Годендо = GodenDo.Text));
            string Другиепоказатели1 = "";
            Invoke(new System.Action(() => Другиепоказатели1 = OtherText1.Text + " " + OtherNumber1.Text + " "));
            WordDoc.ReplaceString("{Наименование}", ReactivNameTr);
            WordDoc.ReplaceString("{класс}", ClassTr);
            WordDoc.ReplaceString("{НД}", НД);
            WordDoc.ReplaceString("{Партия}", Партия);
            WordDoc.ReplaceString("{Дата изготовления}", Датаизготовления);
            WordDoc.ReplaceString("{Срок годности}", Срокгодности);
            WordDoc.ReplaceString("{Дата получения}", Датаполучения);
            WordDoc.ReplaceString("{Годен до}", Годендо);
            WordDoc.ReplaceString("{Другие показатели1}", Другиепоказатели1);
            string OtherEdIzm1Text = "";
            Invoke(new System.Action(() => OtherEdIzm1Text = OtherEdIzm1.Text));
            switch (OtherEdIzm1Text)
            {
                case "мм2/с":
                    WordDoc.ReplaceString("&&&{1}м3&$&{1}", " ");
                    break;
                case "г/см3":
                    WordDoc.ReplaceString("мм2/с&&&{1}", "г/с");
                    WordDoc.ReplaceString("&$&{1}", " ");
                    break;
                case "мг/дм3":
                    WordDoc.ReplaceString("мм2/с&&&{1}", "мг/д");
                    WordDoc.ReplaceString("&$&{1}", " ");
                    break;
                default:
                    WordDoc.ReplaceString("мм2/с&&&{1}м3&$&{1}", OtherEdIzm1Text + " ");
                    break;
            }
            string OtherDate1Text = "";
            Invoke(new System.Action(() => OtherDate1Text = OtherDate1.Text));
            WordDoc.ReplaceString("{Дата1}", OtherDate1Text == "" ? "" : "(" + OtherDate1Text + ")");
            bool OtherText2Visible = true;
            Invoke(new System.Action(() => OtherText2Visible = OtherText2.Visible));
            if (OtherText2Visible == true)
            {
                string OtherText2Text = "";
                Invoke(new System.Action(() => OtherText2Text = OtherText2.Text));
                string OtherNumber2Text = "";
                Invoke(new System.Action(() => OtherNumber2Text = OtherNumber2.Text));
                WordDoc.ReplaceString("{Другие показатели2}", ",\r" + OtherText2Text + " " + OtherNumber2Text + " ");
                string OtherEdIzm2Text = "";
                Invoke(new System.Action(() => OtherEdIzm2Text = OtherEdIzm2.Text));
                switch (OtherEdIzm2Text)
                {
                    case "мм2/с":
                        WordDoc.ReplaceString("&&&{2}м3&$&{2}", " ");
                        break;
                    case "г/см3":
                        WordDoc.ReplaceString("мм2/с&&&{2}", "г/с");
                        WordDoc.ReplaceString("&$&{2}", " ");
                        break;
                    case "мг/дм3":
                        WordDoc.ReplaceString("мм2/с&&&{2}", "мг/д");
                        WordDoc.ReplaceString("&$&{2}", " ");
                        break;
                    default:
                        WordDoc.ReplaceString("мм2/с&&&{2}м3&$&{2}", OtherEdIzm2Text + " ");
                        break;
                }
                string OtherDate2Text = "";
                Invoke(new System.Action(() => OtherDate2Text = OtherDate2.Text));
                WordDoc.ReplaceString("{Дата2}", "(" + OtherDate2Text + ")");
            }
            else
            {
                WordDoc.ReplaceString("{Другие показатели2}мм2/с&&&{2}м3&$&{2}{Дата2}", "");
            }
            bool OtherText3Visible = true;
            Invoke(new System.Action(() => OtherText3Visible = OtherText3.Visible));
            if (OtherText3Visible == true)
            {
                string OtherText3Text = "";
                Invoke(new System.Action(() => OtherText3Text = OtherText3.Text));
                string OtherNumber3Text = "";
                Invoke(new System.Action(() => OtherNumber3Text = OtherNumber3.Text));
                WordDoc.ReplaceString("{Другие показатели3}", ",\r" + OtherText3Text + " " + OtherNumber3Text + " ");
                string OtherEdIzm3Text = "";
                Invoke(new System.Action(() => OtherEdIzm3Text = OtherEdIzm3.Text));
                switch (OtherEdIzm3Text)
                {
                    case "мм2/с":
                        WordDoc.ReplaceString("&&&{3}м3&$&{3}", " ");
                        break;
                    case "г/см3":
                        WordDoc.ReplaceString("мм2/с&&&{3}", "г/с");
                        WordDoc.ReplaceString("&$&{3}", " ");
                        break;
                    case "мг/дм3":
                        WordDoc.ReplaceString("мм2/с&&&{3}", "мг/д");
                        WordDoc.ReplaceString("&$&{3}", " ");
                        break;
                    default:
                        WordDoc.ReplaceString("мм2/с&&&{3}м3&$&{3}", OtherEdIzm3Text + " ");
                        break;
                }
                string OtherDate3Text = "";
                Invoke(new System.Action(() => OtherDate3Text = OtherDate3.Text));
                WordDoc.ReplaceString("{Дата3}", "(" + OtherDate3Text + ")");
            }
            else
            {
                WordDoc.ReplaceString("{Другие показатели3}мм2/с&&&{3}м3&$&{3}{Дата3}", "");
            }
            bool OtherText4Visible = true;
            Invoke(new System.Action(() => OtherText4Visible = OtherText4.Visible));
            if (OtherText4Visible == true)
            {
                string OtherText4Text = "";
                Invoke(new System.Action(() => OtherText4Text = OtherText4.Text));
                string OtherNumber4Text = "";
                Invoke(new System.Action(() => OtherNumber4Text = OtherNumber4.Text));
                WordDoc.ReplaceString("{Другие показатели4}", ",\r" + OtherText4Text + " " + OtherNumber4Text + " ");
                string OtherEdIzm4Text = "";
                Invoke(new System.Action(() => OtherEdIzm4Text = OtherEdIzm4.Text));
                switch (OtherEdIzm4Text)
                {
                    case "мм2/с":
                        WordDoc.ReplaceString("&&&{4}м3&$&{4}", " ");
                        break;
                    case "г/см3":
                        WordDoc.ReplaceString("мм2/с&&&{4}", "г/с");
                        WordDoc.ReplaceString("&$&{4}", " ");
                        break;
                    case "мг/дм3":
                        WordDoc.ReplaceString("мм2/с&&&{4}", "мг/д");
                        WordDoc.ReplaceString("&$&{4}", " ");
                        break;
                    default:
                        WordDoc.ReplaceString("мм2/с&&&{4}м3&$&{4}", OtherEdIzm4Text + " ");
                        break;
                }
                string OtherDate4Text = "";
                Invoke(new System.Action(() => OtherDate4Text = OtherDate4.Text));
                WordDoc.ReplaceString("{Дата4}", "(" + OtherDate4Text + ")");
            }
            else
            {
                WordDoc.ReplaceString("{Другие показатели4}мм2/с&&&{4}м3&$&{4}{Дата4}", "");
            }
            bool OtherText5Visible = true;
            Invoke(new System.Action(() => OtherText5Visible = OtherText5.Visible));
            if (OtherText5Visible == true)
            {
                string OtherText5Text = "";
                Invoke(new System.Action(() => OtherText5Text = OtherText5.Text));
                string OtherNumber5Text = "";
                Invoke(new System.Action(() => OtherNumber5Text = OtherNumber5.Text));
                WordDoc.ReplaceString("{Другие показатели5}", ",\r" + OtherText5Text + " " + OtherNumber5Text + " ");
                string OtherEdIzm5Text = "";
                Invoke(new System.Action(() => OtherEdIzm5Text = OtherEdIzm5.Text));
                switch (OtherEdIzm5Text)
                {
                    case "мм2/с":
                        WordDoc.ReplaceString("&&&{5}м3&$&{5}", " ");
                        break;
                    case "г/см3":
                        WordDoc.ReplaceString("мм2/с&&&{5}", "г/с");
                        WordDoc.ReplaceString("&$&{5}", " ");
                        break;
                    case "мг/дм3":
                        WordDoc.ReplaceString("мм2/с&&&{5}", "мг/д");
                        WordDoc.ReplaceString("&$&{5}", " ");
                        break;
                    default:
                        WordDoc.ReplaceString("мм2/с&&&{5}м3&$&{5}", OtherEdIzm5Text + " ");
                        break;
                }
                string OtherDate5Text = "";
                Invoke(new System.Action(() => OtherDate5Text = OtherDate5.Text));
                WordDoc.ReplaceString("{Дата5}", "(" + OtherDate5Text + ")");
            }
            else
            {
                WordDoc.ReplaceString("{Другие показатели5}мм2/с&&&{5}м3&$&{5}{Дата5}", "");
            }
            string Komissia1NameText = "";
            Invoke(new System.Action(() => Komissia1NameText = Komissia1Name.Text));
            string Komissia1Text = "";
            Invoke(new System.Action(() => Komissia1Text = Komissia1.Text));
            string Komissia2NameText = "";
            Invoke(new System.Action(() => Komissia2NameText = Komissia2Name.Text));
            string Komissia2Text = "";
            Invoke(new System.Action(() => Komissia2Text = Komissia2.Text));
            string Komissia3NameText = "";
            Invoke(new System.Action(() => Komissia3NameText = Komissia3Name.Text));
            string Komissia3Text = "";
            Invoke(new System.Action(() => Komissia3Text = Komissia3.Text));
            string Komissia4NameText = "";
            Invoke(new System.Action(() => Komissia4NameText = Komissia4Name.Text));
            string Komissia4Text = "";
            Invoke(new System.Action(() => Komissia4Text = Komissia4.Text));
            WordDoc.ReplaceString("{Начальник1}", Komissia1NameText);
            WordDoc.ReplaceString("{Начальник2}", Komissia1Text);
            WordDoc.ReplaceString("{Инженер1}", Komissia2NameText);
            WordDoc.ReplaceString("{Инженер2}", Komissia2Text);
            WordDoc.ReplaceString("{Лаборант11}", Komissia3NameText);
            WordDoc.ReplaceString("{Лаборант12}", Komissia3Text);
            WordDoc.ReplaceString("{Лаборант21}", Komissia4NameText);
            WordDoc.ReplaceString("{Лаборант22}", Komissia4Text);
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void PrintButton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveAkt();
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                PrintButton.Enabled = false;
                SaveButton.Enabled = false;
                Loader.Visible = true;
                worker.DoWork += Print;
                worker.RunWorkerAsync();
            }
            catch { }
        }

        private void Print(object sender, DoWorkEventArgs e)
        {
            try
            {
                Invoke(new System.Action(() => TopMost = false));
                WordDoc2 = new WordDocument(Application.StartupPath + @"\\Templates\\Входной контроль.dotx");
                EditTable(ref WordDoc2);
                Replace(ref WordDoc2);
                WordDoc2.Visible = true;
                WordDoc2.wordApplication.Activate();
                Invoke(new System.Action(() => Main.TopMost = false));
                //WordDoc.SaveAs(Application.StartupPath + "//Temp/Шаблон Входной контроль2.doc");                              
                Changed = false;
            }
            catch (Exception ex)
            {
                try
                {
                    LogErrors Log = new LogErrors(ex.ToString());
                    Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    WordDoc2.Close();
                }
                catch { }
            }
            finally
            {
                try
                {
                    Invoke(new System.Action(() => Loader.Visible = false));
                    Invoke(new System.Action(() => PrintButton.Enabled = true));
                    Invoke(new System.Action(() => SaveButton.Enabled = true));
                }
                catch { }
            }
        }

        private void AddOtherButton_Click(object sender, EventArgs e)
        {
            if (OtherText2.Visible == false)
            {
                OtherText2.Visible = true;
                OtherDate2.Visible = true;
                OtherEdIzm2.Visible = true;
                OtherNumber2.Visible = true;
                return;
            }
            if (OtherText3.Visible == false)
            {
                OtherText3.Visible = true;
                OtherDate3.Visible = true;
                OtherEdIzm3.Visible = true;
                OtherNumber3.Visible = true;
                return;
            }
            if (OtherText4.Visible == false)
            {
                OtherText4.Visible = true;
                OtherDate4.Visible = true;
                OtherEdIzm4.Visible = true;
                OtherNumber4.Visible = true;
                return;
            }
            if (OtherText5.Visible == false)
            {
                OtherText5.Visible = true;
                OtherDate5.Visible = true;
                OtherEdIzm5.Visible = true;
                OtherNumber5.Visible = true;
                return;
            }
            if (OtherText6.Visible == false)
            {
                OtherText6.Visible = true;
                OtherDate6.Visible = true;
                OtherEdIzm6.Visible = true;
                OtherNumber6.Visible = true;
                return;
            }
            if (OtherText7.Visible == false)
            {
                OtherText7.Visible = true;
                OtherDate7.Visible = true;
                OtherEdIzm7.Visible = true;
                OtherNumber7.Visible = true;
                return;
            }
        }

        private void DeleteOtherButton_Click(object sender, EventArgs e)
        {
            if (OtherText7.Visible == true)
            {
                OtherText7.Visible = false;
                OtherDate7.Visible = false;
                OtherEdIzm7.Visible = false;
                OtherNumber7.Visible = false;
                return;
            }
            if (OtherText6.Visible == true)
            {
                OtherText6.Visible = false;
                OtherDate6.Visible = false;
                OtherEdIzm6.Visible = false;
                OtherNumber6.Visible = false;
                return;
            }
            if (OtherText5.Visible == true)
            {
                OtherText5.Visible = false;
                OtherDate5.Visible = false;
                OtherEdIzm5.Visible = false;
                OtherNumber5.Visible = false;
                return;
            }
            if (OtherText4.Visible == true)
            {
                OtherText4.Visible = false;
                OtherDate4.Visible = false;
                OtherEdIzm4.Visible = false;
                OtherNumber4.Visible = false;
                return;
            }
            if (OtherText3.Visible == true)
            {
                OtherText3.Visible = false;
                OtherDate3.Visible = false;
                OtherEdIzm3.Visible = false;
                OtherNumber3.Visible = false;
                return;
            }
            if (OtherText2.Visible == true)
            {
                OtherText2.Visible = false;
                OtherDate2.Visible = false;
                OtherEdIzm2.Visible = false;
                OtherNumber2.Visible = false;
                return;
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            SaveAkt(true);
        }

        private void SaveAkt(bool ShowMessage = false)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    if (ID.Text != "") //Редактирование
                    {
                        //Основные даннные
                        var Stroka = db.ReactivAktVhodnogoKontrolya.Where(c => c.Id == Convert.ToInt32(ID.Text)).FirstOrDefault();
                        var IDStroka = db.ReactivAktVhodnogoKontrolya != null && db.ReactivAktVhodnogoKontrolya.Count() > 0 ? db.ReactivAktVhodnogoKontrolya.Max(c => c.Id) + 1 : 1;
                        if (Stroka != null)
                        {
                            Stroka.Id = Convert.ToInt32(ID.Text);
                            Stroka.ДатаСозданияАкта = AktCreateDate.Value.Date;
                            Stroka.НаименованиеРеактива = ReactivName.Text;
                            Stroka.Классификация = Class.Text;
                            Stroka.НДНаРеактив = ND.Text;
                            Stroka.Партия = Partia.Text;
                            try { Stroka.ДатаИзготовления = DateTime.Parse(DateIzgot.Text).Date; } catch { Stroka.ДатаИзготовления = null; }
                            Stroka.СрокГодности = SrokGodnosti.Text;
                            try { Stroka.ДатаПолучения = DateTime.Parse(DateGet.Text).Date; } catch { Stroka.ДатаПолучения = null; }
                            try { Stroka.ГоденДо = DateTime.Parse(GodenDo.Text).Date; } catch { Stroka.ГоденДо = null; }
                            Stroka.VisibleStatus = 0;
                            Stroka.UserName = UserSettings.User;
                            Stroka.ДатаСозданияЗаписи = DateTime.Now;
                            Stroka.ReactivID = ReactivID;
                            Stroka.Owner = null;
                            HistoryReactivAktVhodnogoKontrolya(Stroka, 200, IDStroka);
                        }
                        else
                        {
                            MessageBox.Show("Ошибка сохранения акта", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        //Изменение другие показатели
                        var IDOther = db.ReactivAktVhodnogoKontrolyaOther != null && db.ReactivAktVhodnogoKontrolyaOther.Count() > 0 ? db.ReactivAktVhodnogoKontrolyaOther.Max(c => c.Id) + 1 : 1;
                        var Other = db.ReactivAktVhodnogoKontrolyaOther.Where(c => c.MainTableId == Stroka.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                        if (OtherText1.Text != "" || OtherNumber1.Text != "" || OtherEdIzm1.Text != "")
                        {
                            if (Other.Count > 0)
                            {
                                Other[0].Текст = OtherText1.Text;
                                Other[0].Число = OtherNumber1.Text;
                                Other[0].ЕдиницаИзмерения = OtherEdIzm1.Text;
                                Other[0].Дата = OtherDate1.Text;
                                Other[0].VisibleStatus = 0;
                                Other[0].UserName = UserSettings.User;
                                Other[0].ДатаСозданияЗаписи = DateTime.Now;
                                Other[0].Owner = null;
                                HistoryReactivAktVhodnogoKontrolyaOther(Other[0], 200, IDOther, IDStroka);
                                IDOther++;
                            }
                            else
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = Stroka.Id,
                                    Текст = OtherText1.Text,
                                    Число = OtherNumber1.Text,
                                    ЕдиницаИзмерения = OtherEdIzm1.Text,
                                    Дата = OtherDate1.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null
                                });
                                IDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = IDStroka,
                                    Текст = OtherText1.Text,
                                    Число = OtherNumber1.Text,
                                    ЕдиницаИзмерения = OtherEdIzm1.Text,
                                    Дата = OtherDate1.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = IDOther - 1,
                                });
                                IDOther++;
                            }
                        }
                        else
                        {
                            if (Other.Count > 0)
                            {
                                for (int i = 0; i < Other.Count; i++)
                                {
                                    Other[i].VisibleStatus = Other[i].Owner == null ? 300 : 301;
                                    Other[i].UserName = UserSettings.User;
                                    Other[i].ДатаСозданияЗаписи = DateTime.Now;
                                    //db.ReactivAktVhodnogoKontrolyaOther.DeleteOnSubmit(Other[i]);
                                }
                            }
                        }
                        if (OtherText2.Visible == true)
                        {
                            if (Other.Count > 1)
                            {
                                Other[1].Текст = OtherText2.Text;
                                Other[1].Число = OtherNumber2.Text;
                                Other[1].ЕдиницаИзмерения = OtherEdIzm2.Text;
                                Other[1].Дата = OtherDate2.Text;
                                Other[1].VisibleStatus = 0;
                                Other[1].UserName = UserSettings.User;
                                Other[1].ДатаСозданияЗаписи = DateTime.Now;
                                Other[1].Owner = null;
                                HistoryReactivAktVhodnogoKontrolyaOther(Other[1], 200, IDOther, IDStroka);
                                IDOther++;
                            }
                            else
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = Stroka.Id,
                                    Текст = OtherText2.Text,
                                    Число = OtherNumber2.Text,
                                    ЕдиницаИзмерения = OtherEdIzm2.Text,
                                    Дата = OtherDate2.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                IDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = IDStroka,
                                    Текст = OtherText2.Text,
                                    Число = OtherNumber2.Text,
                                    ЕдиницаИзмерения = OtherEdIzm2.Text,
                                    Дата = OtherDate2.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = IDOther - 1,
                                });
                                IDOther++;
                            }
                        }
                        else
                        {
                            if (Other.Count > 1)
                            {
                                Other[1].VisibleStatus = Other[1].Owner == null ? 300 : 301;
                                Other[1].UserName = UserSettings.User;
                                Other[1].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktVhodnogoKontrolyaOther.DeleteOnSubmit(Other[1]);
                            }
                        }
                        if (OtherText3.Visible == true)
                        {
                            if (Other.Count > 2)
                            {
                                Other[2].Текст = OtherText3.Text;
                                Other[2].Число = OtherNumber3.Text;
                                Other[2].ЕдиницаИзмерения = OtherEdIzm3.Text;
                                Other[2].Дата = OtherDate3.Text;
                                Other[2].VisibleStatus = 0;
                                Other[2].UserName = UserSettings.User;
                                Other[2].ДатаСозданияЗаписи = DateTime.Now;
                                Other[2].Owner = null;
                                HistoryReactivAktVhodnogoKontrolyaOther(Other[2], 200, IDOther, IDStroka);
                                IDOther++;
                            }
                            else
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = Stroka.Id,
                                    Текст = OtherText3.Text,
                                    Число = OtherNumber3.Text,
                                    ЕдиницаИзмерения = OtherEdIzm3.Text,
                                    Дата = OtherDate3.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                IDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = IDStroka,
                                    Текст = OtherText3.Text,
                                    Число = OtherNumber3.Text,
                                    ЕдиницаИзмерения = OtherEdIzm3.Text,
                                    Дата = OtherDate3.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = IDOther - 1,
                                });
                                IDOther++;
                            }
                        }
                        else
                        {
                            if (Other.Count > 2)
                            {
                                Other[2].VisibleStatus = Other[2].Owner == null ? 300 : 301;
                                Other[2].UserName = UserSettings.User;
                                Other[2].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktVhodnogoKontrolyaOther.DeleteOnSubmit(Other[2]);
                            }
                        }
                        if (OtherText4.Visible == true)
                        {
                            if (Other.Count > 3)
                            {
                                Other[3].Текст = OtherText4.Text;
                                Other[3].Число = OtherNumber4.Text;
                                Other[3].ЕдиницаИзмерения = OtherEdIzm4.Text;
                                Other[3].Дата = OtherDate4.Text;
                                Other[3].VisibleStatus = 0;
                                Other[3].UserName = UserSettings.User;
                                Other[3].ДатаСозданияЗаписи = DateTime.Now;
                                Other[3].Owner = null;
                                HistoryReactivAktVhodnogoKontrolyaOther(Other[3], 200, IDOther, IDStroka);
                                IDOther++;
                            }
                            else
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = Stroka.Id,
                                    Текст = OtherText4.Text,
                                    Число = OtherNumber4.Text,
                                    ЕдиницаИзмерения = OtherEdIzm4.Text,
                                    Дата = OtherDate4.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                IDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = IDStroka,
                                    Текст = OtherText4.Text,
                                    Число = OtherNumber4.Text,
                                    ЕдиницаИзмерения = OtherEdIzm4.Text,
                                    Дата = OtherDate4.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = IDOther - 1,
                                });
                                IDOther++;
                            }
                        }
                        else
                        {
                            if (Other.Count > 3)
                            {
                                Other[3].VisibleStatus = Other[3].Owner == null ? 300 : 301;
                                Other[3].UserName = UserSettings.User;
                                Other[3].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktVhodnogoKontrolyaOther.DeleteOnSubmit(Other[3]);
                            }
                        }
                        if (OtherText5.Visible == true)
                        {
                            if (Other.Count > 4)
                            {
                                Other[4].Текст = OtherText5.Text;
                                Other[4].Число = OtherNumber5.Text;
                                Other[4].ЕдиницаИзмерения = OtherEdIzm5.Text;
                                Other[4].Дата = OtherDate5.Text;
                                Other[4].VisibleStatus = 0;
                                Other[4].UserName = UserSettings.User;
                                Other[4].ДатаСозданияЗаписи = DateTime.Now;
                                Other[4].Owner = null;
                                HistoryReactivAktVhodnogoKontrolyaOther(Other[4], 200, IDOther, IDStroka);
                                IDOther++;
                            }
                            else
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = Stroka.Id,
                                    Текст = OtherText5.Text,
                                    Число = OtherNumber5.Text,
                                    ЕдиницаИзмерения = OtherEdIzm5.Text,
                                    Дата = OtherDate5.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null
                                });
                                IDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = IDStroka,
                                    Текст = OtherText5.Text,
                                    Число = OtherNumber5.Text,
                                    ЕдиницаИзмерения = OtherEdIzm5.Text,
                                    Дата = OtherDate5.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = IDOther - 1,
                                });
                                IDOther++;
                            }
                        }
                        else
                        {
                            if (Other.Count > 4)
                            {
                                Other[4].VisibleStatus = Other[4].Owner == null ? 300 : 301;
                                Other[4].UserName = UserSettings.User;
                                Other[4].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktVhodnogoKontrolyaOther.DeleteOnSubmit(Other[4]);
                            }
                        }
                        if (OtherText6.Visible == true)
                        {
                            if (Other.Count > 5)
                            {
                                Other[5].Текст = OtherText6.Text;
                                Other[5].Число = OtherNumber6.Text;
                                Other[5].ЕдиницаИзмерения = OtherEdIzm6.Text;
                                Other[5].Дата = OtherDate6.Text;
                                Other[5].VisibleStatus = 0;
                                Other[5].UserName = UserSettings.User;
                                Other[5].ДатаСозданияЗаписи = DateTime.Now;
                                Other[5].Owner = null;
                                HistoryReactivAktVhodnogoKontrolyaOther(Other[5], 200, IDOther, IDStroka);
                                IDOther++;
                            }
                            else
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = Stroka.Id,
                                    Текст = OtherText6.Text,
                                    Число = OtherNumber6.Text,
                                    ЕдиницаИзмерения = OtherEdIzm6.Text,
                                    Дата = OtherDate6.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                IDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = IDStroka,
                                    Текст = OtherText6.Text,
                                    Число = OtherNumber6.Text,
                                    ЕдиницаИзмерения = OtherEdIzm6.Text,
                                    Дата = OtherDate6.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = IDOther - 1,
                                });
                                IDOther++;
                            }
                        }
                        else
                        {
                            if (Other.Count > 5)
                            {
                                Other[5].VisibleStatus = Other[5].Owner == null ? 300 : 301;
                                Other[5].UserName = UserSettings.User;
                                Other[5].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktVhodnogoKontrolyaOther.DeleteOnSubmit(Other[5]);
                            }
                        }
                        if (OtherText7.Visible == true)
                        {
                            if (Other.Count > 6)
                            {
                                Other[6].Текст = OtherText7.Text;
                                Other[6].Число = OtherNumber7.Text;
                                Other[6].ЕдиницаИзмерения = OtherEdIzm7.Text;
                                Other[6].Дата = OtherDate7.Text;
                                Other[6].VisibleStatus = 0;
                                Other[6].UserName = UserSettings.User;
                                Other[6].ДатаСозданияЗаписи = DateTime.Now;
                                Other[6].Owner = null;
                                HistoryReactivAktVhodnogoKontrolyaOther(Other[6], 200, IDOther, IDStroka);
                                IDOther++;
                            }
                            else
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = Stroka.Id,
                                    Текст = OtherText7.Text,
                                    Число = OtherNumber7.Text,
                                    ЕдиницаИзмерения = OtherEdIzm7.Text,
                                    Дата = OtherDate7.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null
                                });
                                IDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = IDOther,
                                    MainTableId = IDStroka,
                                    Текст = OtherText7.Text,
                                    Число = OtherNumber7.Text,
                                    ЕдиницаИзмерения = OtherEdIzm7.Text,
                                    Дата = OtherDate7.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = IDOther - 1,
                                });
                                IDOther++;
                            }
                        }
                        else
                        {
                            if (Other.Count > 6)
                            {
                                Other[6].VisibleStatus = Other[6].Owner == null ? 300 : 301;
                                Other[6].UserName = UserSettings.User;
                                Other[6].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktVhodnogoKontrolyaOther.DeleteOnSubmit(Other[6]);
                            }
                        }
                        //Изменение табличной части
                        var Table = db.ReactivAktVhodnogoKontrolyaTable.Where(c => c.MainTableId == Stroka.Id && (c.VisibleStatus == 0 || c.VisibleStatus == null)).ToList();
                        List<string> GridData = GetGridData();

                        if (GridData.Count > 0)
                        {
                            int n = 0;
                            var IDTable = db.ReactivAktVhodnogoKontrolyaTable != null && db.ReactivAktVhodnogoKontrolyaTable.Count() > 0 ? db.ReactivAktVhodnogoKontrolyaTable.Max(c => c.Id + 1) : 1;
                            while (GridData.Count > 8 * n)
                            {
                                if (Table.Count < n + 1) //добавляем новый
                                {
                                    db.ReactivAktVhodnogoKontrolyaTable.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaTable
                                    {
                                        Id = IDTable,
                                        MainTableId = Stroka.Id,
                                        ДатаКонтроля = GridData[8 * n],
                                        ГСО = GridData[8 * n + 1],
                                        АттестованноеЗначениеГСО = GridData[8 * n + 2],
                                        ДатаВыпуска = GridData[8 * n + 3],
                                        СрокГодностиГСО = GridData[8 * n + 4],
                                        РезультатИзмерений = GridData[8 * n + 5],
                                        РезультатКонтроля = GridData[8 * n + 6],
                                        НормативКонтроля = GridData[8 * n + 7],
                                        VisibleStatus = 0,
                                        UserName = UserSettings.User,
                                        ДатаСозданияЗаписи = DateTime.Now,
                                        Owner = null
                                    });
                                    IDTable++;
                                    db.ReactivAktVhodnogoKontrolyaTable.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaTable
                                    {
                                        Id = IDTable,
                                        MainTableId = IDStroka,
                                        ДатаКонтроля = GridData[8 * n],
                                        ГСО = GridData[8 * n + 1],
                                        АттестованноеЗначениеГСО = GridData[8 * n + 2],
                                        ДатаВыпуска = GridData[8 * n + 3],
                                        СрокГодностиГСО = GridData[8 * n + 4],
                                        РезультатИзмерений = GridData[8 * n + 5],
                                        РезультатКонтроля = GridData[8 * n + 6],
                                        НормативКонтроля = GridData[8 * n + 7],
                                        VisibleStatus = 100,
                                        UserName = UserSettings.User,
                                        ДатаСозданияЗаписи = DateTime.Now,
                                        Owner = IDTable - 1,
                                    });
                                    IDTable++;
                                }
                                else //Изменяем имеющийся
                                {
                                    Table[n].ДатаКонтроля = GridData[8 * n];
                                    Table[n].ГСО = GridData[8 * n + 1];
                                    Table[n].АттестованноеЗначениеГСО = GridData[8 * n + 2];
                                    Table[n].ДатаВыпуска = GridData[8 * n + 3];
                                    Table[n].СрокГодностиГСО = GridData[8 * n + 4];
                                    Table[n].РезультатИзмерений = GridData[8 * n + 5];
                                    Table[n].РезультатКонтроля = GridData[8 * n + 6];
                                    Table[n].НормативКонтроля = GridData[8 * n + 7];
                                    Table[n].VisibleStatus = 0;
                                    Table[n].UserName = UserSettings.User;
                                    Table[n].ДатаСозданияЗаписи = DateTime.Now;
                                    Table[n].Owner = null;
                                    HistiryReactivAktVhodnogoKontrolyaTable(Table[n], 200, IDTable, IDStroka);
                                    IDTable++;
                                }
                                n++;
                            }
                            if (Table.Count > GridData.Count / 8)
                            {
                                for (int i = GridData.Count / 8; i < Table.Count; i++)
                                {
                                    Table[i].VisibleStatus = Table[i].Owner == null ? 300 : 301;
                                    Table[i].UserName = UserSettings.User;
                                    Table[i].ДатаСозданияЗаписи = DateTime.Now;
                                    //db.ReactivAktVhodnogoKontrolyaTable.DeleteOnSubmit(Table[i]);
                                }
                            }
                        }
                        else
                        {
                            if (Table.Count > 0)
                            {
                                for (int i = 0; i < Table.Count; i++)
                                {
                                    Table[i].VisibleStatus = Table[i].Owner == null ? 300 : 301;
                                    Table[i].UserName = UserSettings.User;
                                    Table[i].ДатаСозданияЗаписи = DateTime.Now;
                                    //db.ReactivAktVhodnogoKontrolyaTable.DeleteOnSubmit(Table[i]);
                                }
                            }
                        }
                        //Изменение комиссии
                        var Komissia = db.ReactivAktVhodnogoKontrolyaKomissiya.Where(c => c.MainTableId == Stroka.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                        var IDKomissia = db.ReactivAktVhodnogoKontrolyaKomissiya != null && db.ReactivAktVhodnogoKontrolyaKomissiya.Count() > 0 ? db.ReactivAktVhodnogoKontrolyaKomissiya.Max(c => c.Id + 1) : 1;
                        if (Komissia1Name.Text != "" && Komissia1.Text != "")
                        {
                            if (Komissia.Count > 0)
                            {
                                Komissia[0].MainTableId = Komissia[0].MainTableId;
                                Komissia[0].Должность = Komissia1Name.Text;
                                Komissia[0].ФИО = Komissia1.Text;
                                Komissia[0].VisibleStatus = 0;
                                Komissia[0].UserName = UserSettings.User;
                                Komissia[0].ДатаСозданияЗаписи = DateTime.Now;
                                Komissia[0].Owner = null;
                                HistoryReactivAktVhodnogoKontrolyaKomissiya(Komissia[0], 200, IDKomissia, IDStroka);
                                IDKomissia++;
                            }
                            else
                            {
                                db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                                {
                                    Id = IDKomissia,
                                    MainTableId = Stroka.Id,
                                    Должность = Komissia1Name.Text,
                                    ФИО = Komissia1.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                IDKomissia++;
                                db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                                {
                                    Id = IDKomissia,
                                    MainTableId = IDStroka,
                                    Должность = Komissia1Name.Text,
                                    ФИО = Komissia1.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = IDKomissia - 1,
                                });
                                IDKomissia++;
                            }
                        }
                        else
                        {
                            if (Komissia.Count > 0)
                            {
                                for (int i = 0; i < Komissia.Count; i++)
                                {
                                    Komissia[i].VisibleStatus = Komissia[i].Owner == null ? 300 : 301;
                                    Komissia[i].UserName = UserSettings.User;
                                    Komissia[i].ДатаСозданияЗаписи = DateTime.Now;
                                    //db.ReactivAktVhodnogoKontrolyaKomissiya.DeleteOnSubmit(Komissia[i]);
                                }
                            }
                        }
                        if (Komissia2Name.Text != "" && Komissia2.Text != "")
                        {
                            if (Komissia.Count > 1)
                            {
                                Komissia[1].Должность = Komissia2Name.Text;
                                Komissia[1].ФИО = Komissia2.Text;
                                Komissia[1].VisibleStatus = 0;
                                Komissia[1].UserName = UserSettings.User;
                                Komissia[1].ДатаСозданияЗаписи = DateTime.Now;
                                Komissia[1].Owner = null;
                                HistoryReactivAktVhodnogoKontrolyaKomissiya(Komissia[1], 200, IDKomissia, IDStroka);
                                IDKomissia++;
                            }
                            else
                            {
                                db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                                {
                                    Id = IDKomissia,
                                    MainTableId = Stroka.Id,
                                    Должность = Komissia2Name.Text,
                                    ФИО = Komissia2.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                IDKomissia++;
                                db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                                {
                                    Id = IDKomissia,
                                    MainTableId = IDStroka,
                                    Должность = Komissia2Name.Text,
                                    ФИО = Komissia2.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = IDKomissia - 1,
                                });
                                IDKomissia++;
                            }
                        }
                        else
                        {
                            if (Komissia.Count > 1)
                            {
                                Komissia[1].VisibleStatus = Komissia[1].Owner == null ? 300 : 301;
                                Komissia[1].UserName = UserSettings.User;
                                Komissia[1].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktVhodnogoKontrolyaKomissiya.DeleteOnSubmit(Komissia[1]);
                            }
                        }
                        if (Komissia3Name.Text != "" && Komissia3.Text != "")
                        {
                            if (Komissia.Count > 2)
                            {
                                Komissia[2].Должность = Komissia3Name.Text;
                                Komissia[2].ФИО = Komissia3.Text;
                                Komissia[2].VisibleStatus = 0;
                                Komissia[2].UserName = UserSettings.User;
                                Komissia[2].ДатаСозданияЗаписи = DateTime.Now;
                                Komissia[2].Owner = null;
                                HistoryReactivAktVhodnogoKontrolyaKomissiya(Komissia[2], 200, IDKomissia, IDStroka);
                                IDKomissia++;
                            }
                            else
                            {
                                db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                                {
                                    Id = IDKomissia,
                                    MainTableId = Stroka.Id,
                                    Должность = Komissia3Name.Text,
                                    ФИО = Komissia3.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                IDKomissia++;
                                db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                                {
                                    Id = IDKomissia,
                                    MainTableId = IDStroka,
                                    Должность = Komissia3Name.Text,
                                    ФИО = Komissia3.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = IDKomissia - 1,
                                });
                                IDKomissia++;
                            }
                        }
                        else
                        {
                            if (Komissia.Count > 2)
                            {
                                Komissia[2].VisibleStatus = Komissia[2].Owner == null ? 300 : 301;
                                Komissia[2].UserName = UserSettings.User;
                                Komissia[2].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktVhodnogoKontrolyaKomissiya.DeleteOnSubmit(Komissia[2]);
                            }
                        }
                        if (Komissia4Name.Text != "" && Komissia4.Text != "")
                        {
                            if (Komissia.Count > 3)
                            {
                                Komissia[3].Должность = Komissia4Name.Text;
                                Komissia[3].ФИО = Komissia4.Text;
                                Komissia[3].VisibleStatus = 0;
                                Komissia[3].UserName = UserSettings.User;
                                Komissia[3].ДатаСозданияЗаписи = DateTime.Now;
                                Komissia[3].Owner = null;
                                HistoryReactivAktVhodnogoKontrolyaKomissiya(Komissia[3], 200, IDKomissia, IDStroka);
                                IDKomissia++;
                            }
                            else
                            {
                                db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                                {
                                    Id = IDKomissia,
                                    MainTableId = Stroka.Id,
                                    Должность = Komissia4Name.Text,
                                    ФИО = Komissia4.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null
                                });
                                IDKomissia++;
                                db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                                {
                                    Id = IDKomissia,
                                    MainTableId = IDStroka,
                                    Должность = Komissia4Name.Text,
                                    ФИО = Komissia4.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = IDKomissia - 1,
                                });
                                IDKomissia++;
                            }
                        }
                        else
                        {
                            if (Komissia.Count > 3)
                            {
                                Komissia[3].VisibleStatus = Komissia[3].Owner == null ? 300 : 301;
                                Komissia[3].UserName = UserSettings.User;
                                Komissia[3].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktVhodnogoKontrolyaKomissiya.DeleteOnSubmit(Komissia[3]);
                            }
                        }
                    }
                    else //Добавить новый акт
                    {
                        //Основные данные
                        var NextID = db.ReactivAktVhodnogoKontrolya != null && db.ReactivAktVhodnogoKontrolya.Count() > 0 ? db.ReactivAktVhodnogoKontrolya.Max(c => c.Id) + 1 : 1;
                        DateTime? ДатаИзготовления = null;
                        DateTime? ДатаПолучения = null;
                        DateTime? ГоденДо = null;
                        try { ДатаИзготовления = DateTime.Parse(DateIzgot.Text).Date; } catch { }
                        try { ДатаПолучения = DateTime.Parse(DateGet.Text).Date; } catch { }
                        try { ГоденДо = DateTime.Parse(GodenDo.Text).Date; } catch { }
                        db.ReactivAktVhodnogoKontrolya.InsertOnSubmit(new ReactivAktVhodnogoKontrolya
                        {
                            Id = NextID,
                            ДатаСозданияАкта = DateTime.Now.Date,
                            НаименованиеРеактива = ReactivName.Text,
                            Классификация = Class.Text,
                            НДНаРеактив = ND.Text,
                            Партия = Partia.Text,
                            ДатаИзготовления = ДатаИзготовления,
                            СрокГодности = SrokGodnosti.Text,
                            ДатаПолучения = ДатаПолучения,
                            ГоденДо = ГоденДо,
                            VisibleStatus = 0,
                            UserName = UserSettings.User,
                            ДатаСозданияЗаписи = DateTime.Now,
                            ReactivID = ReactivID,
                            Owner = null,
                        });
                        db.ReactivAktVhodnogoKontrolya.InsertOnSubmit(new ReactivAktVhodnogoKontrolya
                        {
                            Id = NextID + 1,
                            ДатаСозданияАкта = DateTime.Now.Date,
                            НаименованиеРеактива = ReactivName.Text,
                            Классификация = Class.Text,
                            НДНаРеактив = ND.Text,
                            Партия = Partia.Text,
                            ДатаИзготовления = ДатаИзготовления,
                            СрокГодности = SrokGodnosti.Text,
                            ДатаПолучения = ДатаПолучения,
                            ГоденДо = ГоденДо,
                            VisibleStatus = 100,
                            UserName = UserSettings.User,
                            ДатаСозданияЗаписи = DateTime.Now,
                            ReactivID = ReactivID,
                            Owner = NextID,
                        });

                        //Другие показатели
                        if (OtherText1.Text != "" || OtherNumber1.Text != "" || OtherEdIzm1.Text != "")
                        {
                            var NextIDOther = db.ReactivAktVhodnogoKontrolyaOther != null && db.ReactivAktVhodnogoKontrolyaOther.Count() > 0 ? db.ReactivAktVhodnogoKontrolyaOther.Max(c => c.Id) + 1 : 1;
                            db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                            {
                                Id = NextIDOther,
                                MainTableId = NextID,
                                Текст = OtherText1.Text,
                                Число = OtherNumber1.Text,
                                ЕдиницаИзмерения = OtherEdIzm1.Text,
                                Дата = OtherDate1.Text,
                                VisibleStatus = 0,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = null,
                            });
                            NextIDOther++;
                            db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                            {
                                Id = NextIDOther,
                                MainTableId = NextID + 1,
                                Текст = OtherText1.Text,
                                Число = OtherNumber1.Text,
                                ЕдиницаИзмерения = OtherEdIzm1.Text,
                                Дата = OtherDate1.Text,
                                VisibleStatus = 100,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = NextIDOther - 1,
                            });
                            NextIDOther++;
                            if (OtherText2.Visible == true)
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID,
                                    Текст = OtherText2.Text,
                                    Число = OtherNumber2.Text,
                                    ЕдиницаИзмерения = OtherEdIzm2.Text,
                                    Дата = OtherDate2.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                NextIDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID + 1,
                                    Текст = OtherText2.Text,
                                    Число = OtherNumber2.Text,
                                    ЕдиницаИзмерения = OtherEdIzm2.Text,
                                    Дата = OtherDate2.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDOther - 1,
                                });
                                NextIDOther++;
                            }
                            if (OtherText3.Visible == true)
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID,
                                    Текст = OtherText3.Text,
                                    Число = OtherNumber3.Text,
                                    ЕдиницаИзмерения = OtherEdIzm3.Text,
                                    Дата = OtherDate3.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null
                                });
                                NextIDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID + 1,
                                    Текст = OtherText3.Text,
                                    Число = OtherNumber3.Text,
                                    ЕдиницаИзмерения = OtherEdIzm3.Text,
                                    Дата = OtherDate3.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDOther - 1,
                                });
                                NextIDOther++;
                            }
                            if (OtherText4.Visible == true)
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID,
                                    Текст = OtherText4.Text,
                                    Число = OtherNumber4.Text,
                                    ЕдиницаИзмерения = OtherEdIzm4.Text,
                                    Дата = OtherDate4.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null
                                });
                                NextIDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID + 1,
                                    Текст = OtherText4.Text,
                                    Число = OtherNumber4.Text,
                                    ЕдиницаИзмерения = OtherEdIzm4.Text,
                                    Дата = OtherDate4.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDOther - 1,
                                });
                                NextIDOther++;
                            }
                            if (OtherText5.Visible == true)
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID,
                                    Текст = OtherText5.Text,
                                    Число = OtherNumber5.Text,
                                    ЕдиницаИзмерения = OtherEdIzm5.Text,
                                    Дата = OtherDate5.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null
                                });
                                NextIDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID + 1,
                                    Текст = OtherText5.Text,
                                    Число = OtherNumber5.Text,
                                    ЕдиницаИзмерения = OtherEdIzm5.Text,
                                    Дата = OtherDate5.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDOther - 1,
                                });
                                NextIDOther++;
                            }
                            if (OtherText6.Visible == true)
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID,
                                    Текст = OtherText6.Text,
                                    Число = OtherNumber6.Text,
                                    ЕдиницаИзмерения = OtherEdIzm6.Text,
                                    Дата = OtherDate6.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null
                                });
                                NextIDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID + 1,
                                    Текст = OtherText6.Text,
                                    Число = OtherNumber6.Text,
                                    ЕдиницаИзмерения = OtherEdIzm6.Text,
                                    Дата = OtherDate6.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDOther - 1,
                                });
                                NextIDOther++;
                            }
                            if (OtherText7.Visible == true)
                            {
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID,
                                    Текст = OtherText7.Text,
                                    Число = OtherNumber7.Text,
                                    ЕдиницаИзмерения = OtherEdIzm7.Text,
                                    Дата = OtherDate7.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                NextIDOther++;
                                db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                                {
                                    Id = NextIDOther,
                                    MainTableId = NextID + 1,
                                    Текст = OtherText7.Text,
                                    Число = OtherNumber7.Text,
                                    ЕдиницаИзмерения = OtherEdIzm7.Text,
                                    Дата = OtherDate7.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDOther - 1,
                                });
                            }
                        }
                        //Добавление табличной части
                        List<string> GridData = GetGridData();

                        var NextIDTable = db.ReactivAktVhodnogoKontrolyaTable != null && db.ReactivAktVhodnogoKontrolyaTable.Count() > 0 ? db.ReactivAktVhodnogoKontrolyaTable.Max(c => c.Id) + 1 : 0;
                        if (GridData.Count > 0)
                        {
                            int n = 0;
                            while (GridData.Count > 8 * n)
                            {
                                db.ReactivAktVhodnogoKontrolyaTable.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaTable
                                {
                                    Id = NextIDTable,
                                    MainTableId = NextID,
                                    ДатаКонтроля = GridData[8 * n],
                                    ГСО = GridData[8 * n + 1],
                                    АттестованноеЗначениеГСО = GridData[8 * n + 2],
                                    ДатаВыпуска = GridData[8 * n + 3],
                                    СрокГодностиГСО = GridData[8 * n + 4],
                                    РезультатИзмерений = GridData[8 * n + 5],
                                    РезультатКонтроля = GridData[8 * n + 6],
                                    НормативКонтроля = GridData[8 * n + 7],
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                NextIDTable++;
                                db.ReactivAktVhodnogoKontrolyaTable.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaTable
                                {
                                    Id = NextIDTable,
                                    MainTableId = NextID + 1,
                                    ДатаКонтроля = GridData[8 * n],
                                    ГСО = GridData[8 * n + 1],
                                    АттестованноеЗначениеГСО = GridData[8 * n + 2],
                                    ДатаВыпуска = GridData[8 * n + 3],
                                    СрокГодностиГСО = GridData[8 * n + 4],
                                    РезультатИзмерений = GridData[8 * n + 5],
                                    РезультатКонтроля = GridData[8 * n + 6],
                                    НормативКонтроля = GridData[8 * n + 7],
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDTable - 1,
                                });
                                NextIDTable++;
                                n++;
                            }
                        }
                        //Добавление комиссии
                        var NextIDKomissia = db.ReactivAktVhodnogoKontrolyaKomissiya != null && db.ReactivAktVhodnogoKontrolyaKomissiya.Count() > 0 ? db.ReactivAktVhodnogoKontrolyaKomissiya.Max(c => c.Id) + 1 : 1;
                        if (Komissia2Name.Text != "" && Komissia2.Text != "")
                        {
                            db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                            {
                                Id = NextIDKomissia,
                                MainTableId = NextID,
                                Должность = Komissia1Name.Text,
                                ФИО = Komissia1.Text,
                                VisibleStatus = 0,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = null,
                            });
                            NextIDKomissia++;
                            db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                            {
                                Id = NextIDKomissia,
                                MainTableId = NextID + 1,
                                Должность = Komissia1Name.Text,
                                ФИО = Komissia1.Text,
                                VisibleStatus = 100,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = NextIDKomissia - 1,
                            });
                            NextIDKomissia++;
                        }
                        if (Komissia2Name.Text != "" && Komissia2.Text != "")
                        {
                            db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                            {
                                Id = NextIDKomissia,
                                MainTableId = NextID,
                                Должность = Komissia2Name.Text,
                                ФИО = Komissia2.Text,
                                VisibleStatus = 0,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = null,
                            });
                            NextIDKomissia++;
                            db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                            {
                                Id = NextIDKomissia,
                                MainTableId = NextID + 1,
                                Должность = Komissia2Name.Text,
                                ФИО = Komissia2.Text,
                                VisibleStatus = 100,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = NextIDKomissia - 1,
                            });
                            NextIDKomissia++;
                        }
                        if (Komissia3Name.Text != "" && Komissia3.Text != "")
                        {
                            db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                            {
                                Id = NextIDKomissia,
                                MainTableId = NextID,
                                Должность = Komissia3Name.Text,
                                ФИО = Komissia3.Text,
                                VisibleStatus = 0,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = null,
                            });
                            NextIDKomissia++;
                            db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                            {
                                Id = NextIDKomissia,
                                MainTableId = NextID + 1,
                                Должность = Komissia3Name.Text,
                                ФИО = Komissia3.Text,
                                VisibleStatus = 100,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = NextIDKomissia - 1,
                            });
                            NextIDKomissia++;
                        }
                        if (Komissia4Name.Text != "" && Komissia4.Text != "")
                        {
                            db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                            {
                                Id = NextIDKomissia,
                                MainTableId = NextID,
                                Должность = Komissia4Name.Text,
                                ФИО = Komissia4.Text,
                                VisibleStatus = 0,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = null,
                            });
                            NextIDKomissia++;
                            db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                            {
                                Id = NextIDKomissia,
                                MainTableId = NextID + 1,
                                Должность = Komissia4Name.Text,
                                ФИО = Komissia4.Text,
                                VisibleStatus = 100,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = NextIDKomissia - 1,
                            });
                            NextIDKomissia++;
                        }
                        ID.Text = NextID.ToString();
                    }
                    db.SubmitChanges();
                    db.Dispose();
                }
                if (ShowMessage) MessageBox.Show("Акт входного контроля сохранён!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Changed = false;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void HistoryReactivAktVhodnogoKontrolyaKomissiya(ReactivAktVhodnogoKontrolyaKomissiya stroka, int Kod, int ID, int MainID)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.ReactivAktVhodnogoKontrolyaKomissiya.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaKomissiya
                    {
                        Id = ID,
                        MainTableId = MainID,
                        Должность = stroka.Должность,
                        ФИО = stroka.ФИО,
                        VisibleStatus = Kod,
                        UserName = UserSettings.User,
                        ДатаСозданияЗаписи = DateTime.Now,
                        Owner = stroka.Id,
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void HistiryReactivAktVhodnogoKontrolyaTable(ReactivAktVhodnogoKontrolyaTable stroka, int Kod, int ID, int MainID)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.ReactivAktVhodnogoKontrolyaTable.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaTable
                    {
                        Id = ID,
                        MainTableId = MainID,
                        ДатаКонтроля = stroka.ДатаКонтроля,
                        ГСО = stroka.ГСО,
                        АттестованноеЗначениеГСО = stroka.АттестованноеЗначениеГСО,
                        ДатаВыпуска = stroka.ДатаВыпуска,
                        СрокГодностиГСО = stroka.СрокГодностиГСО,
                        РезультатИзмерений = stroka.РезультатИзмерений,
                        РезультатКонтроля = stroka.РезультатКонтроля,
                        НормативКонтроля = stroka.НормативКонтроля,
                        VisibleStatus = Kod,
                        UserName = UserSettings.User,
                        ДатаСозданияЗаписи = DateTime.Now,
                        Owner = stroka.Id
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void HistoryReactivAktVhodnogoKontrolyaOther(ReactivAktVhodnogoKontrolyaOther stroka, int Kod, int ID, int MainID)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.ReactivAktVhodnogoKontrolyaOther.InsertOnSubmit(new ReactivAktVhodnogoKontrolyaOther
                    {
                        Id = ID,
                        MainTableId = MainID,
                        Текст = stroka.Текст,
                        Число = stroka.Число,
                        ЕдиницаИзмерения = stroka.ЕдиницаИзмерения,
                        Дата = stroka.Дата,
                        VisibleStatus = Kod,
                        ДатаСозданияЗаписи = DateTime.Now,
                        UserName = UserSettings.User,
                        Owner = stroka.Id,
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void HistoryReactivAktVhodnogoKontrolya(ReactivAktVhodnogoKontrolya stroka, int Kod, int MainID)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.ReactivAktVhodnogoKontrolya.InsertOnSubmit(new ReactivAktVhodnogoKontrolya
                    {
                        Id = MainID,
                        ДатаСозданияАкта = stroka.ДатаСозданияАкта,
                        НаименованиеРеактива = stroka.НаименованиеРеактива,
                        Классификация = stroka.Классификация,
                        НДНаРеактив = stroka.НДНаРеактив,
                        Партия = stroka.Партия,
                        ДатаИзготовления = stroka.ДатаИзготовления,
                        СрокГодности = stroka.СрокГодности,
                        ДатаПолучения = stroka.ДатаПолучения,
                        ГоденДо = stroka.ГоденДо,
                        VisibleStatus = Kod,
                        ДатаСозданияЗаписи = DateTime.Now,
                        UserName = UserSettings.User,
                        Owner = stroka.Id,
                        ReactivID = stroka.ReactivID,
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<string> GetGridData()
        {
            List<string> GridData = new List<string>();
            foreach (DataGridViewRow row in ReactivAktVhodnoiGridView.Rows)
            {
                var Rows = ReactivAktVhodnoiGridView.Rows.Count;
                if (Rows - 1 > row.Index)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        string value = cell.Value == null ? "" : cell.Value.ToString();
                        GridData.Add(value);
                    }
                }
            }
            bool LastRowNull = true;
            do
            {
                var count = GridData.Count;
                if (count > 0)
                {
                    for (int i = count - 1; i > count - 9; i--)
                    {
                        LastRowNull = LastRowNull == true ? GridData[i] == "" : false;
                    }
                    if (LastRowNull)
                    {
                        for (int i = count - 1; i > count - 9; i--)
                        {
                            GridData.RemoveAt(i);
                        }
                    }
                }
                else LastRowNull = false;
            }
            while (LastRowNull);
            return GridData;
        }

        private void ReactivPrintAktvVhodnogoKontrolaForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Changed)
            {
                DialogResult dialogResult = MessageBox.Show("Сохранить изменения?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    SaveAkt();
                }
            }
            Owner.Visible = true;
            Close();
        }

        private void ReactivPrintAktvVhodnogoKontrolaForm_Load(object sender, EventArgs e)
        {
            ContextMenuEvent();
        }

        private void ContextMenuEvent()
        {
            foreach (Control C in ReactivAktVhodnPanel.Controls)
            {
                if (C is TextBox || C is ComboBox)
                {
                    C.ContextMenuStrip = Main.ContextMenuStripMain;
                }
            }
        }


        private void ReactivAktVhodnoiGridView_Enter(object sender, EventArgs e)
        {
            Changed = true;
        }

        private void ReactivAktVhodnoiGridView_MouseClick(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //{   
            //    ContextMenu m = new ContextMenu();
            //    m.MenuItems.Add(new MenuItem("Cut"));
            //    m.MenuItems.Add(new MenuItem("Copy"));
            //    m.MenuItems.Add(new MenuItem("Paste"));

            //    int currentMouseOverRow = ReactivAktVhodnoiGridView.HitTest(e.X, e.Y).RowIndex;
            //    int currentMouseOverColumn = ReactivAktVhodnoiGridView.HitTest(e.X, e.Y).ColumnIndex;
            //    if (currentMouseOverRow >= 0 && currentMouseOverColumn >= 0)
            //    {
            //        m.MenuItems.Add(new MenuItem(string.Format("Do something to row {0}", currentMouseOverRow.ToString())));
            //    }
            //    m.Show(ReactivAktVhodnoiGridView, new Point(e.X, e.Y));
            //}
        }

        private void ReactivName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    SaveButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void ReactivPrintAktvVhodnogoKontrolaForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        //void ReactivAktVhodnoiGridView_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e)
        //{
        //    e.ContextMenu = ContextMenuStrip1;
        //}        
    }
}
