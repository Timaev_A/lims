﻿using System.Windows.Forms;

namespace LIMS
{
    partial class ReactivPrintAktvVhodnogoKontrolaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReactivPrintAktvVhodnogoKontrolaForm));
            this.ReactivAktVhodnPanel = new DevExpress.XtraEditors.PanelControl();
            this.label8 = new System.Windows.Forms.Label();
            this.AktCreateDate = new System.Windows.Forms.DateTimePicker();
            this.Komissia2 = new System.Windows.Forms.ComboBox();
            this.Komissia1 = new System.Windows.Forms.ComboBox();
            this.Komissia4 = new System.Windows.Forms.ComboBox();
            this.Komissia3 = new System.Windows.Forms.ComboBox();
            this.ID = new System.Windows.Forms.Label();
            this.SaveButton = new DevExpress.XtraEditors.SimpleButton();
            this.DeleteOtherButton = new DevExpress.XtraEditors.SimpleButton();
            this.AddOtherButton = new DevExpress.XtraEditors.SimpleButton();
            this.OtherText7 = new System.Windows.Forms.ComboBox();
            this.OtherDate7 = new System.Windows.Forms.TextBox();
            this.OtherEdIzm7 = new System.Windows.Forms.ComboBox();
            this.OtherNumber7 = new System.Windows.Forms.TextBox();
            this.OtherText6 = new System.Windows.Forms.ComboBox();
            this.OtherDate6 = new System.Windows.Forms.TextBox();
            this.OtherEdIzm6 = new System.Windows.Forms.ComboBox();
            this.OtherNumber6 = new System.Windows.Forms.TextBox();
            this.OtherText5 = new System.Windows.Forms.ComboBox();
            this.OtherDate5 = new System.Windows.Forms.TextBox();
            this.OtherEdIzm5 = new System.Windows.Forms.ComboBox();
            this.OtherNumber5 = new System.Windows.Forms.TextBox();
            this.OtherText4 = new System.Windows.Forms.ComboBox();
            this.OtherDate4 = new System.Windows.Forms.TextBox();
            this.OtherEdIzm4 = new System.Windows.Forms.ComboBox();
            this.OtherNumber4 = new System.Windows.Forms.TextBox();
            this.OtherText3 = new System.Windows.Forms.ComboBox();
            this.OtherDate3 = new System.Windows.Forms.TextBox();
            this.OtherEdIzm3 = new System.Windows.Forms.ComboBox();
            this.OtherNumber3 = new System.Windows.Forms.TextBox();
            this.OtherText2 = new System.Windows.Forms.ComboBox();
            this.OtherDate2 = new System.Windows.Forms.TextBox();
            this.OtherEdIzm2 = new System.Windows.Forms.ComboBox();
            this.OtherNumber2 = new System.Windows.Forms.TextBox();
            this.OtherText1 = new System.Windows.Forms.ComboBox();
            this.OtherDate1 = new System.Windows.Forms.TextBox();
            this.OtherEdIzm1 = new System.Windows.Forms.ComboBox();
            this.OtherNumber1 = new System.Windows.Forms.TextBox();
            this.ReactivAktVhodnoiGridView = new System.Windows.Forms.DataGridView();
            this.DateKontrolyaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GSOColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttestovannoeGSOColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataVipuska = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SrokGodnostiGSOColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResultIzmColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PezultKontrolyaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NormativKontrolyaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Komissia2Name = new System.Windows.Forms.TextBox();
            this.Komissia1Name = new System.Windows.Forms.TextBox();
            this.PrintButton = new DevExpress.XtraEditors.SimpleButton();
            this.Komissia4Name = new System.Windows.Forms.ComboBox();
            this.Komissia3Name = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.GodenDo = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.DateGet = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SrokGodnosti = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DateIzgot = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Partia = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ND = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Class = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ReactivName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ViewButton = new DevExpress.XtraEditors.SimpleButton();
            this.CancelClickButton = new DevExpress.XtraEditors.SimpleButton();
            this.Loader = new DevExpress.XtraWaitForm.ProgressPanel();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivAktVhodnPanel)).BeginInit();
            this.ReactivAktVhodnPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivAktVhodnoiGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ReactivAktVhodnPanel
            // 
            this.ReactivAktVhodnPanel.AutoSize = true;
            this.ReactivAktVhodnPanel.Controls.Add(this.label8);
            this.ReactivAktVhodnPanel.Controls.Add(this.AktCreateDate);
            this.ReactivAktVhodnPanel.Controls.Add(this.Komissia2);
            this.ReactivAktVhodnPanel.Controls.Add(this.Komissia1);
            this.ReactivAktVhodnPanel.Controls.Add(this.Komissia4);
            this.ReactivAktVhodnPanel.Controls.Add(this.Komissia3);
            this.ReactivAktVhodnPanel.Controls.Add(this.ID);
            this.ReactivAktVhodnPanel.Controls.Add(this.SaveButton);
            this.ReactivAktVhodnPanel.Controls.Add(this.DeleteOtherButton);
            this.ReactivAktVhodnPanel.Controls.Add(this.AddOtherButton);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherText7);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherDate7);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherEdIzm7);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherNumber7);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherText6);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherDate6);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherEdIzm6);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherNumber6);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherText5);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherDate5);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherEdIzm5);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherNumber5);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherText4);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherDate4);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherEdIzm4);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherNumber4);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherText3);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherDate3);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherEdIzm3);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherNumber3);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherText2);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherDate2);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherEdIzm2);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherNumber2);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherText1);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherDate1);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherEdIzm1);
            this.ReactivAktVhodnPanel.Controls.Add(this.OtherNumber1);
            this.ReactivAktVhodnPanel.Controls.Add(this.ReactivAktVhodnoiGridView);
            this.ReactivAktVhodnPanel.Controls.Add(this.Komissia2Name);
            this.ReactivAktVhodnPanel.Controls.Add(this.Komissia1Name);
            this.ReactivAktVhodnPanel.Controls.Add(this.PrintButton);
            this.ReactivAktVhodnPanel.Controls.Add(this.Komissia4Name);
            this.ReactivAktVhodnPanel.Controls.Add(this.Komissia3Name);
            this.ReactivAktVhodnPanel.Controls.Add(this.label17);
            this.ReactivAktVhodnPanel.Controls.Add(this.label20);
            this.ReactivAktVhodnPanel.Controls.Add(this.label22);
            this.ReactivAktVhodnPanel.Controls.Add(this.GodenDo);
            this.ReactivAktVhodnPanel.Controls.Add(this.label23);
            this.ReactivAktVhodnPanel.Controls.Add(this.label16);
            this.ReactivAktVhodnPanel.Controls.Add(this.DateGet);
            this.ReactivAktVhodnPanel.Controls.Add(this.label7);
            this.ReactivAktVhodnPanel.Controls.Add(this.SrokGodnosti);
            this.ReactivAktVhodnPanel.Controls.Add(this.label6);
            this.ReactivAktVhodnPanel.Controls.Add(this.DateIzgot);
            this.ReactivAktVhodnPanel.Controls.Add(this.label5);
            this.ReactivAktVhodnPanel.Controls.Add(this.Partia);
            this.ReactivAktVhodnPanel.Controls.Add(this.label4);
            this.ReactivAktVhodnPanel.Controls.Add(this.ND);
            this.ReactivAktVhodnPanel.Controls.Add(this.label3);
            this.ReactivAktVhodnPanel.Controls.Add(this.Class);
            this.ReactivAktVhodnPanel.Controls.Add(this.label2);
            this.ReactivAktVhodnPanel.Controls.Add(this.ReactivName);
            this.ReactivAktVhodnPanel.Controls.Add(this.label1);
            this.ReactivAktVhodnPanel.Controls.Add(this.ViewButton);
            this.ReactivAktVhodnPanel.Controls.Add(this.CancelClickButton);
            this.ReactivAktVhodnPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReactivAktVhodnPanel.Location = new System.Drawing.Point(0, 0);
            this.ReactivAktVhodnPanel.Name = "ReactivAktVhodnPanel";
            this.ReactivAktVhodnPanel.Size = new System.Drawing.Size(777, 578);
            this.ReactivAktVhodnPanel.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 232);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 13);
            this.label8.TabIndex = 101;
            this.label8.Text = "Дата создания акта:";
            // 
            // AktCreateDate
            // 
            this.AktCreateDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.AktCreateDate.Location = new System.Drawing.Point(146, 229);
            this.AktCreateDate.Name = "AktCreateDate";
            this.AktCreateDate.Size = new System.Drawing.Size(200, 21);
            this.AktCreateDate.TabIndex = 9;
            this.AktCreateDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // Komissia2
            // 
            this.Komissia2.FormattingEnabled = true;
            this.Komissia2.Location = new System.Drawing.Point(440, 463);
            this.Komissia2.Name = "Komissia2";
            this.Komissia2.Size = new System.Drawing.Size(131, 21);
            this.Komissia2.TabIndex = 44;
            this.Komissia2.Text = "";
            this.Komissia2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // Komissia1
            // 
            this.Komissia1.FormattingEnabled = true;
            this.Komissia1.Location = new System.Drawing.Point(440, 436);
            this.Komissia1.Name = "Komissia1";
            this.Komissia1.Size = new System.Drawing.Size(131, 21);
            this.Komissia1.TabIndex = 42;
            this.Komissia1.Text = "";
            this.Komissia1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // Komissia4
            // 
            this.Komissia4.FormattingEnabled = true;
            this.Komissia4.Location = new System.Drawing.Point(440, 517);
            this.Komissia4.Name = "Komissia4";
            this.Komissia4.Size = new System.Drawing.Size(131, 21);
            this.Komissia4.TabIndex = 48;
            this.Komissia4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // Komissia3
            // 
            this.Komissia3.FormattingEnabled = true;
            this.Komissia3.Location = new System.Drawing.Point(440, 490);
            this.Komissia3.Name = "Komissia3";
            this.Komissia3.Size = new System.Drawing.Size(131, 21);
            this.Komissia3.TabIndex = 46;
            this.Komissia3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Location = new System.Drawing.Point(30, 493);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(0, 13);
            this.ID.TabIndex = 94;
            this.ID.Visible = false;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(364, 544);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 50;
            this.SaveButton.Text = "Сохранить";
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // DeleteOtherButton
            // 
            this.DeleteOtherButton.ImageOptions.Image = global::LIMS.Properties.Resources.cancel_16x16;
            this.DeleteOtherButton.Location = new System.Drawing.Point(665, 7);
            this.DeleteOtherButton.Name = "DeleteOtherButton";
            this.DeleteOtherButton.Size = new System.Drawing.Size(25, 25);
            this.DeleteOtherButton.TabIndex = 11;
            this.DeleteOtherButton.Click += new System.EventHandler(this.DeleteOtherButton_Click);
            // 
            // AddOtherButton
            // 
            this.AddOtherButton.ImageOptions.Image = global::LIMS.Properties.Resources.add_16x16;
            this.AddOtherButton.Location = new System.Drawing.Point(634, 7);
            this.AddOtherButton.Name = "AddOtherButton";
            this.AddOtherButton.Size = new System.Drawing.Size(25, 25);
            this.AddOtherButton.TabIndex = 10;
            this.AddOtherButton.Click += new System.EventHandler(this.AddOtherButton_Click);
            // 
            // OtherText7
            // 
            this.OtherText7.FormattingEnabled = true;
            this.OtherText7.Items.AddRange(new object[] {
            "Плотность при 20°С",
            "Содержание воды"});
            this.OtherText7.Location = new System.Drawing.Point(364, 201);
            this.OtherText7.Name = "OtherText7";
            this.OtherText7.Size = new System.Drawing.Size(163, 21);
            this.OtherText7.TabIndex = 36;
            this.OtherText7.Visible = false;
            this.OtherText7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherDate7
            // 
            this.OtherDate7.Location = new System.Drawing.Point(683, 201);
            this.OtherDate7.Name = "OtherDate7";
            this.OtherDate7.Size = new System.Drawing.Size(82, 21);
            this.OtherDate7.TabIndex = 39;
            this.OtherDate7.Visible = false;
            this.OtherDate7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherEdIzm7
            // 
            this.OtherEdIzm7.FormattingEnabled = true;
            this.OtherEdIzm7.Items.AddRange(new object[] {
            "мг/дм3",
            "ppm",
            "мм2/с",
            "кПа",
            "г/см3",
            "%",
            "°С "});
            this.OtherEdIzm7.Location = new System.Drawing.Point(617, 201);
            this.OtherEdIzm7.Name = "OtherEdIzm7";
            this.OtherEdIzm7.Size = new System.Drawing.Size(60, 21);
            this.OtherEdIzm7.TabIndex = 38;
            this.OtherEdIzm7.Visible = false;
            this.OtherEdIzm7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherNumber7
            // 
            this.OtherNumber7.Location = new System.Drawing.Point(533, 201);
            this.OtherNumber7.Name = "OtherNumber7";
            this.OtherNumber7.Size = new System.Drawing.Size(78, 21);
            this.OtherNumber7.TabIndex = 37;
            this.OtherNumber7.Visible = false;
            this.OtherNumber7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherText6
            // 
            this.OtherText6.FormattingEnabled = true;
            this.OtherText6.Items.AddRange(new object[] {
            "Плотность при 20°С",
            "Содержание воды"});
            this.OtherText6.Location = new System.Drawing.Point(364, 174);
            this.OtherText6.Name = "OtherText6";
            this.OtherText6.Size = new System.Drawing.Size(163, 21);
            this.OtherText6.TabIndex = 32;
            this.OtherText6.Visible = false;
            this.OtherText6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherDate6
            // 
            this.OtherDate6.Location = new System.Drawing.Point(683, 174);
            this.OtherDate6.Name = "OtherDate6";
            this.OtherDate6.Size = new System.Drawing.Size(82, 21);
            this.OtherDate6.TabIndex = 35;
            this.OtherDate6.Visible = false;
            this.OtherDate6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherEdIzm6
            // 
            this.OtherEdIzm6.FormattingEnabled = true;
            this.OtherEdIzm6.Items.AddRange(new object[] {
            "мг/дм3",
            "ppm",
            "мм2/с",
            "кПа",
            "г/см3",
            "%",
            "°С "});
            this.OtherEdIzm6.Location = new System.Drawing.Point(617, 174);
            this.OtherEdIzm6.Name = "OtherEdIzm6";
            this.OtherEdIzm6.Size = new System.Drawing.Size(60, 21);
            this.OtherEdIzm6.TabIndex = 34;
            this.OtherEdIzm6.Visible = false;
            this.OtherEdIzm6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherNumber6
            // 
            this.OtherNumber6.Location = new System.Drawing.Point(533, 174);
            this.OtherNumber6.Name = "OtherNumber6";
            this.OtherNumber6.Size = new System.Drawing.Size(78, 21);
            this.OtherNumber6.TabIndex = 33;
            this.OtherNumber6.Visible = false;
            this.OtherNumber6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherText5
            // 
            this.OtherText5.FormattingEnabled = true;
            this.OtherText5.Items.AddRange(new object[] {
            "Плотность при 20°С",
            "Содержание воды"});
            this.OtherText5.Location = new System.Drawing.Point(364, 147);
            this.OtherText5.Name = "OtherText5";
            this.OtherText5.Size = new System.Drawing.Size(163, 21);
            this.OtherText5.TabIndex = 28;
            this.OtherText5.Visible = false;
            this.OtherText5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherDate5
            // 
            this.OtherDate5.Location = new System.Drawing.Point(683, 147);
            this.OtherDate5.Name = "OtherDate5";
            this.OtherDate5.Size = new System.Drawing.Size(82, 21);
            this.OtherDate5.TabIndex = 31;
            this.OtherDate5.Visible = false;
            this.OtherDate5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherEdIzm5
            // 
            this.OtherEdIzm5.FormattingEnabled = true;
            this.OtherEdIzm5.Items.AddRange(new object[] {
            "мг/дм3",
            "ppm",
            "мм2/с",
            "кПа",
            "г/см3",
            "%",
            "°С "});
            this.OtherEdIzm5.Location = new System.Drawing.Point(617, 147);
            this.OtherEdIzm5.Name = "OtherEdIzm5";
            this.OtherEdIzm5.Size = new System.Drawing.Size(60, 21);
            this.OtherEdIzm5.TabIndex = 30;
            this.OtherEdIzm5.Visible = false;
            this.OtherEdIzm5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherNumber5
            // 
            this.OtherNumber5.Location = new System.Drawing.Point(533, 147);
            this.OtherNumber5.Name = "OtherNumber5";
            this.OtherNumber5.Size = new System.Drawing.Size(78, 21);
            this.OtherNumber5.TabIndex = 29;
            this.OtherNumber5.Visible = false;
            this.OtherNumber5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherText4
            // 
            this.OtherText4.FormattingEnabled = true;
            this.OtherText4.Items.AddRange(new object[] {
            "Плотность при 20°С",
            "Содержание воды"});
            this.OtherText4.Location = new System.Drawing.Point(364, 120);
            this.OtherText4.Name = "OtherText4";
            this.OtherText4.Size = new System.Drawing.Size(163, 21);
            this.OtherText4.TabIndex = 24;
            this.OtherText4.Visible = false;
            this.OtherText4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherDate4
            // 
            this.OtherDate4.Location = new System.Drawing.Point(683, 120);
            this.OtherDate4.Name = "OtherDate4";
            this.OtherDate4.Size = new System.Drawing.Size(82, 21);
            this.OtherDate4.TabIndex = 27;
            this.OtherDate4.Visible = false;
            this.OtherDate4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherEdIzm4
            // 
            this.OtherEdIzm4.FormattingEnabled = true;
            this.OtherEdIzm4.Items.AddRange(new object[] {
            "мг/дм3",
            "ppm",
            "мм2/с",
            "кПа",
            "г/см3",
            "%",
            "°С "});
            this.OtherEdIzm4.Location = new System.Drawing.Point(617, 120);
            this.OtherEdIzm4.Name = "OtherEdIzm4";
            this.OtherEdIzm4.Size = new System.Drawing.Size(60, 21);
            this.OtherEdIzm4.TabIndex = 26;
            this.OtherEdIzm4.Visible = false;
            this.OtherEdIzm4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherNumber4
            // 
            this.OtherNumber4.Location = new System.Drawing.Point(533, 120);
            this.OtherNumber4.Name = "OtherNumber4";
            this.OtherNumber4.Size = new System.Drawing.Size(78, 21);
            this.OtherNumber4.TabIndex = 25;
            this.OtherNumber4.Visible = false;
            this.OtherNumber4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherText3
            // 
            this.OtherText3.FormattingEnabled = true;
            this.OtherText3.Items.AddRange(new object[] {
            "Плотность при 20°С",
            "Содержание воды"});
            this.OtherText3.Location = new System.Drawing.Point(364, 93);
            this.OtherText3.Name = "OtherText3";
            this.OtherText3.Size = new System.Drawing.Size(163, 21);
            this.OtherText3.TabIndex = 20;
            this.OtherText3.Visible = false;
            this.OtherText3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherDate3
            // 
            this.OtherDate3.Location = new System.Drawing.Point(683, 93);
            this.OtherDate3.Name = "OtherDate3";
            this.OtherDate3.Size = new System.Drawing.Size(82, 21);
            this.OtherDate3.TabIndex = 23;
            this.OtherDate3.Visible = false;
            this.OtherDate3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherEdIzm3
            // 
            this.OtherEdIzm3.FormattingEnabled = true;
            this.OtherEdIzm3.Items.AddRange(new object[] {
            "мг/дм3",
            "ppm",
            "мм2/с",
            "кПа",
            "г/см3",
            "%",
            "°С "});
            this.OtherEdIzm3.Location = new System.Drawing.Point(617, 93);
            this.OtherEdIzm3.Name = "OtherEdIzm3";
            this.OtherEdIzm3.Size = new System.Drawing.Size(60, 21);
            this.OtherEdIzm3.TabIndex = 22;
            this.OtherEdIzm3.Visible = false;
            this.OtherEdIzm3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherNumber3
            // 
            this.OtherNumber3.Location = new System.Drawing.Point(533, 93);
            this.OtherNumber3.Name = "OtherNumber3";
            this.OtherNumber3.Size = new System.Drawing.Size(78, 21);
            this.OtherNumber3.TabIndex = 21;
            this.OtherNumber3.Visible = false;
            this.OtherNumber3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherText2
            // 
            this.OtherText2.FormattingEnabled = true;
            this.OtherText2.Items.AddRange(new object[] {
            "Плотность при 20°С",
            "Содержание воды"});
            this.OtherText2.Location = new System.Drawing.Point(364, 66);
            this.OtherText2.Name = "OtherText2";
            this.OtherText2.Size = new System.Drawing.Size(163, 21);
            this.OtherText2.TabIndex = 16;
            this.OtherText2.Visible = false;
            this.OtherText2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherDate2
            // 
            this.OtherDate2.Location = new System.Drawing.Point(683, 66);
            this.OtherDate2.Name = "OtherDate2";
            this.OtherDate2.Size = new System.Drawing.Size(82, 21);
            this.OtherDate2.TabIndex = 19;
            this.OtherDate2.Visible = false;
            this.OtherDate2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherEdIzm2
            // 
            this.OtherEdIzm2.FormattingEnabled = true;
            this.OtherEdIzm2.Items.AddRange(new object[] {
            "мг/дм3",
            "ppm",
            "мм2/с",
            "кПа",
            "г/см3",
            "%",
            "°С "});
            this.OtherEdIzm2.Location = new System.Drawing.Point(617, 66);
            this.OtherEdIzm2.Name = "OtherEdIzm2";
            this.OtherEdIzm2.Size = new System.Drawing.Size(60, 21);
            this.OtherEdIzm2.TabIndex = 18;
            this.OtherEdIzm2.Visible = false;
            this.OtherEdIzm2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherNumber2
            // 
            this.OtherNumber2.Location = new System.Drawing.Point(533, 66);
            this.OtherNumber2.Name = "OtherNumber2";
            this.OtherNumber2.Size = new System.Drawing.Size(78, 21);
            this.OtherNumber2.TabIndex = 17;
            this.OtherNumber2.Visible = false;
            this.OtherNumber2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherText1
            // 
            this.OtherText1.FormattingEnabled = true;
            this.OtherText1.Items.AddRange(new object[] {
            "Плотность при 20°С",
            "Содержание воды"});
            this.OtherText1.Location = new System.Drawing.Point(364, 39);
            this.OtherText1.Name = "OtherText1";
            this.OtherText1.Size = new System.Drawing.Size(163, 21);
            this.OtherText1.TabIndex = 12;
            this.OtherText1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherDate1
            // 
            this.OtherDate1.Location = new System.Drawing.Point(683, 39);
            this.OtherDate1.Name = "OtherDate1";
            this.OtherDate1.Size = new System.Drawing.Size(82, 21);
            this.OtherDate1.TabIndex = 15;
            this.OtherDate1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherEdIzm1
            // 
            this.OtherEdIzm1.FormattingEnabled = true;
            this.OtherEdIzm1.Items.AddRange(new object[] {
            "мг/дм3",
            "ppm",
            "мм2/с",
            "кПа",
            "г/см3",
            "%",
            "°С "});
            this.OtherEdIzm1.Location = new System.Drawing.Point(617, 39);
            this.OtherEdIzm1.Name = "OtherEdIzm1";
            this.OtherEdIzm1.Size = new System.Drawing.Size(60, 21);
            this.OtherEdIzm1.TabIndex = 14;
            this.OtherEdIzm1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // OtherNumber1
            // 
            this.OtherNumber1.Location = new System.Drawing.Point(533, 39);
            this.OtherNumber1.Name = "OtherNumber1";
            this.OtherNumber1.Size = new System.Drawing.Size(78, 21);
            this.OtherNumber1.TabIndex = 13;
            this.OtherNumber1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // ReactivAktVhodnoiGridView
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReactivAktVhodnoiGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.ReactivAktVhodnoiGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ReactivAktVhodnoiGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.ReactivAktVhodnoiGridView.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReactivAktVhodnoiGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.ReactivAktVhodnoiGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ReactivAktVhodnoiGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DateKontrolyaColumn,
            this.GSOColumn,
            this.AttestovannoeGSOColumn,
            this.DataVipuska,
            this.SrokGodnostiGSOColumn,
            this.ResultIzmColumn,
            this.PezultKontrolyaColumn,
            this.NormativKontrolyaColumn});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReactivAktVhodnoiGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.ReactivAktVhodnoiGridView.Location = new System.Drawing.Point(8, 256);
            this.ReactivAktVhodnoiGridView.Name = "ReactivAktVhodnoiGridView";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReactivAktVhodnoiGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReactivAktVhodnoiGridView.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.ReactivAktVhodnoiGridView.Size = new System.Drawing.Size(757, 152);
            this.ReactivAktVhodnoiGridView.TabIndex = 40;
            this.ReactivAktVhodnoiGridView.Enter += new System.EventHandler(this.ReactivAktVhodnoiGridView_Enter);
            this.ReactivAktVhodnoiGridView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ReactivAktVhodnoiGridView_MouseClick);
            // 
            // DateKontrolyaColumn
            // 
            this.DateKontrolyaColumn.HeaderText = "Дата контроля";
            this.DateKontrolyaColumn.Name = "DateKontrolyaColumn";
            this.DateKontrolyaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GSOColumn
            // 
            this.GSOColumn.HeaderText = "ГСО";
            this.GSOColumn.Name = "GSOColumn";
            this.GSOColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AttestovannoeGSOColumn
            // 
            this.AttestovannoeGSOColumn.HeaderText = "Аттестованное значение ГСО";
            this.AttestovannoeGSOColumn.Name = "AttestovannoeGSOColumn";
            this.AttestovannoeGSOColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DataVipuska
            // 
            this.DataVipuska.HeaderText = "Дата выпуска";
            this.DataVipuska.Name = "DataVipuska";
            this.DataVipuska.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SrokGodnostiGSOColumn
            // 
            this.SrokGodnostiGSOColumn.HeaderText = "Срок годности ГСО";
            this.SrokGodnostiGSOColumn.Name = "SrokGodnostiGSOColumn";
            this.SrokGodnostiGSOColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ResultIzmColumn
            // 
            this.ResultIzmColumn.HeaderText = "Результат измерений";
            this.ResultIzmColumn.Name = "ResultIzmColumn";
            this.ResultIzmColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PezultKontrolyaColumn
            // 
            this.PezultKontrolyaColumn.HeaderText = "Результат контроля";
            this.PezultKontrolyaColumn.Name = "PezultKontrolyaColumn";
            this.PezultKontrolyaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NormativKontrolyaColumn
            // 
            this.NormativKontrolyaColumn.HeaderText = "Норматив контроля";
            this.NormativKontrolyaColumn.Name = "NormativKontrolyaColumn";
            this.NormativKontrolyaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Komissia2Name
            // 
            this.Komissia2Name.Location = new System.Drawing.Point(227, 463);
            this.Komissia2Name.Name = "Komissia2Name";
            this.Komissia2Name.Size = new System.Drawing.Size(207, 21);
            this.Komissia2Name.TabIndex = 43;
            this.Komissia2Name.Text = "Начальник ХАЛ";
            this.Komissia2Name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // Komissia1Name
            // 
            this.Komissia1Name.Location = new System.Drawing.Point(226, 436);
            this.Komissia1Name.Name = "Komissia1Name";
            this.Komissia1Name.Size = new System.Drawing.Size(208, 21);
            this.Komissia1Name.TabIndex = 41;
            this.Komissia1Name.Text = "Начальник ХАЛ";
            this.Komissia1Name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // PrintButton
            // 
            this.PrintButton.Location = new System.Drawing.Point(283, 544);
            this.PrintButton.Name = "PrintButton";
            this.PrintButton.Size = new System.Drawing.Size(75, 23);
            this.PrintButton.TabIndex = 49;
            this.PrintButton.Text = "Печать";
            this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // Komissia4Name
            // 
            this.Komissia4Name.FormattingEnabled = true;
            this.Komissia4Name.Items.AddRange(new object[] {
            "Лаборант химического анализа"});
            this.Komissia4Name.Location = new System.Drawing.Point(227, 517);
            this.Komissia4Name.Name = "Komissia4Name";
            this.Komissia4Name.Size = new System.Drawing.Size(207, 21);
            this.Komissia4Name.TabIndex = 47;
            this.Komissia4Name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // Komissia3Name
            // 
            this.Komissia3Name.FormattingEnabled = true;
            this.Komissia3Name.Items.AddRange(new object[] {
            "Лаборант химического анализа"});
            this.Komissia3Name.Location = new System.Drawing.Point(227, 490);
            this.Komissia3Name.Name = "Komissia3Name";
            this.Komissia3Name.Size = new System.Drawing.Size(207, 21);
            this.Komissia3Name.TabIndex = 45;
            this.Komissia3Name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(348, 411);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(129, 16);
            this.label17.TabIndex = 56;
            this.label17.Text = "Комиссия в составе:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(251, 498);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 13);
            this.label20.TabIndex = 52;
            this.label20.Text = "Партия:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(240, 439);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(89, 13);
            this.label22.TabIndex = 48;
            this.label22.Text = "Начальник ХАЛ:";
            // 
            // GodenDo
            // 
            this.GodenDo.Location = new System.Drawing.Point(146, 201);
            this.GodenDo.Name = "GodenDo";
            this.GodenDo.Size = new System.Drawing.Size(200, 21);
            this.GodenDo.TabIndex = 8;
            this.GodenDo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(82, 204);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 13);
            this.label23.TabIndex = 46;
            this.label23.Text = "Годен до:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(517, 12);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(110, 13);
            this.label16.TabIndex = 44;
            this.label16.Text = "Другие показатели:";
            // 
            // DateGet
            // 
            this.DateGet.Location = new System.Drawing.Point(146, 174);
            this.DateGet.Name = "DateGet";
            this.DateGet.Size = new System.Drawing.Size(200, 21);
            this.DateGet.TabIndex = 7;
            this.DateGet.Text = "     ";
            this.DateGet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(46, 177);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Дата получения:";
            // 
            // SrokGodnosti
            // 
            this.SrokGodnosti.Location = new System.Drawing.Point(146, 147);
            this.SrokGodnosti.Name = "SrokGodnosti";
            this.SrokGodnosti.Size = new System.Drawing.Size(200, 21);
            this.SrokGodnosti.TabIndex = 6;
            this.SrokGodnosti.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(54, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Срок годности:";
            // 
            // DateIzgot
            // 
            this.DateIzgot.Location = new System.Drawing.Point(146, 120);
            this.DateIzgot.Name = "DateIzgot";
            this.DateIzgot.Size = new System.Drawing.Size(200, 21);
            this.DateIzgot.TabIndex = 5;
            this.DateIzgot.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Дата изготовления:";
            // 
            // Partia
            // 
            this.Partia.Location = new System.Drawing.Point(146, 93);
            this.Partia.Name = "Partia";
            this.Partia.Size = new System.Drawing.Size(200, 21);
            this.Partia.TabIndex = 4;
            this.Partia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(92, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Партия:";
            // 
            // ND
            // 
            this.ND.Location = new System.Drawing.Point(146, 66);
            this.ND.Name = "ND";
            this.ND.Size = new System.Drawing.Size(200, 21);
            this.ND.TabIndex = 3;
            this.ND.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "НД на реактив:";
            // 
            // Class
            // 
            this.Class.Location = new System.Drawing.Point(146, 39);
            this.Class.Name = "Class";
            this.Class.Size = new System.Drawing.Size(200, 21);
            this.Class.TabIndex = 2;
            this.Class.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Классификация:";
            // 
            // ReactivName
            // 
            this.ReactivName.Location = new System.Drawing.Point(146, 12);
            this.ReactivName.Name = "ReactivName";
            this.ReactivName.Size = new System.Drawing.Size(200, 21);
            this.ReactivName.TabIndex = 1;
            this.ReactivName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivName_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Наименование реактива:";
            // 
            // ViewButton
            // 
            this.ViewButton.Enabled = false;
            this.ViewButton.Location = new System.Drawing.Point(634, 463);
            this.ViewButton.Name = "ViewButton";
            this.ViewButton.Size = new System.Drawing.Size(75, 23);
            this.ViewButton.TabIndex = 13;
            this.ViewButton.Text = "Просмотр";
            this.ViewButton.Visible = false;
            this.ViewButton.Click += new System.EventHandler(this.ViewButton_Click);
            // 
            // CancelClickButton
            // 
            this.CancelClickButton.Location = new System.Drawing.Point(445, 544);
            this.CancelClickButton.Name = "CancelClickButton";
            this.CancelClickButton.Size = new System.Drawing.Size(75, 23);
            this.CancelClickButton.TabIndex = 51;
            this.CancelClickButton.Text = "Отмена";
            this.CancelClickButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // Loader
            // 
            this.Loader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Loader.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Loader.Appearance.Options.UseBackColor = true;
            this.Loader.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Loader.AppearanceCaption.Options.UseFont = true;
            this.Loader.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Loader.AppearanceDescription.Options.UseFont = true;
            this.Loader.BarAnimationElementThickness = 2;
            this.Loader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.Loader.Caption = "Подождите";
            this.Loader.Description = "Формирование документа ...";
            this.Loader.Location = new System.Drawing.Point(273, 271);
            this.Loader.Name = "Loader";
            this.Loader.Size = new System.Drawing.Size(246, 66);
            this.Loader.TabIndex = 95;
            this.Loader.Visible = false;
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(32, 19);
            // 
            // ReactivPrintAktvVhodnogoKontrolaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 578);
            this.Controls.Add(this.Loader);
            this.Controls.Add(this.ReactivAktVhodnPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "ReactivPrintAktvVhodnogoKontrolaForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Акт входного контроля";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ReactivPrintAktvVhodnogoKontrolaForm_FormClosed);
            this.Load += new System.EventHandler(this.ReactivPrintAktvVhodnogoKontrolaForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivPrintAktvVhodnogoKontrolaForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ReactivAktVhodnPanel)).EndInit();
            this.ReactivAktVhodnPanel.ResumeLayout(false);
            this.ReactivAktVhodnPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivAktVhodnoiGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl ReactivAktVhodnPanel;
        private DevExpress.XtraEditors.SimpleButton ViewButton;
        private DevExpress.XtraEditors.SimpleButton CancelClickButton;
        private System.Windows.Forms.TextBox DateIzgot;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Partia;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ND;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Class;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ReactivName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Komissia4Name;
        private System.Windows.Forms.ComboBox Komissia3Name;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox GodenDo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox DateGet;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox SrokGodnosti;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.SimpleButton PrintButton;
        private System.Windows.Forms.TextBox Komissia2Name;
        private System.Windows.Forms.TextBox Komissia1Name;
        private System.Windows.Forms.DataGridView ReactivAktVhodnoiGridView;
        private System.Windows.Forms.ComboBox OtherText1;
        private System.Windows.Forms.TextBox OtherDate1;
        private System.Windows.Forms.ComboBox OtherEdIzm1;
        private System.Windows.Forms.TextBox OtherNumber1;
        private System.Windows.Forms.ComboBox OtherText7;
        private System.Windows.Forms.TextBox OtherDate7;
        private System.Windows.Forms.ComboBox OtherEdIzm7;
        private System.Windows.Forms.TextBox OtherNumber7;
        private System.Windows.Forms.ComboBox OtherText6;
        private System.Windows.Forms.TextBox OtherDate6;
        private System.Windows.Forms.ComboBox OtherEdIzm6;
        private System.Windows.Forms.TextBox OtherNumber6;
        private System.Windows.Forms.ComboBox OtherText5;
        private System.Windows.Forms.TextBox OtherDate5;
        private System.Windows.Forms.ComboBox OtherEdIzm5;
        private System.Windows.Forms.TextBox OtherNumber5;
        private System.Windows.Forms.ComboBox OtherText4;
        private System.Windows.Forms.TextBox OtherDate4;
        private System.Windows.Forms.ComboBox OtherEdIzm4;
        private System.Windows.Forms.TextBox OtherNumber4;
        private System.Windows.Forms.ComboBox OtherText3;
        private System.Windows.Forms.TextBox OtherDate3;
        private System.Windows.Forms.ComboBox OtherEdIzm3;
        private System.Windows.Forms.TextBox OtherNumber3;
        private System.Windows.Forms.ComboBox OtherText2;
        private System.Windows.Forms.TextBox OtherDate2;
        private System.Windows.Forms.ComboBox OtherEdIzm2;
        private System.Windows.Forms.TextBox OtherNumber2;
        private DevExpress.XtraEditors.SimpleButton AddOtherButton;
        private DevExpress.XtraEditors.SimpleButton DeleteOtherButton;
        private DevExpress.XtraEditors.SimpleButton SaveButton;
        private System.Windows.Forms.Label ID;
        private DevExpress.XtraWaitForm.ProgressPanel Loader;
        private System.Windows.Forms.ComboBox Komissia2;
        private System.Windows.Forms.ComboBox Komissia1;
        private System.Windows.Forms.ComboBox Komissia4;
        private System.Windows.Forms.ComboBox Komissia3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker AktCreateDate;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private DataGridViewTextBoxColumn DateKontrolyaColumn;
        private DataGridViewTextBoxColumn GSOColumn;
        private DataGridViewTextBoxColumn AttestovannoeGSOColumn;
        private DataGridViewTextBoxColumn DataVipuska;
        private DataGridViewTextBoxColumn SrokGodnostiGSOColumn;
        private DataGridViewTextBoxColumn ResultIzmColumn;
        private DataGridViewTextBoxColumn PezultKontrolyaColumn;
        private DataGridViewTextBoxColumn NormativKontrolyaColumn;
    }
}