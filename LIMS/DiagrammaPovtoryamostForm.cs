﻿using DevExpress.XtraCharts;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class DiagrammaPovtoryamostForm : DevExpress.XtraEditors.XtraForm
    {
        public DiagrammaPovtoryamostForm(List<Probi> Probs)
        {
            InitializeComponent();
            try
            {
                int s = 0;
                foreach (var Pr in Probs)
                {
                    if (Pr.X1.HasValue && Pr.X2.HasValue)
                        break;
                    s++;
                }
                if ((s + 1 == Probs.Count) && !Probs[s].X1.HasValue && !Probs[s].X2.HasValue)
                    return;
                VLKStatic.CalculatePredel(Probs[s]);
                ((XYDiagram)ChartControl.Diagram).AxisX.VisualRange.MinValue = 1;
                ((XYDiagram)ChartControl.Diagram).AxisX.VisualRange.MaxValue = Probs.Count;
                ((XYDiagram)ChartControl.Diagram).AxisX.VisualRange.Auto = true;
                ((XYDiagram)ChartControl.Diagram).AxisX.WholeRange.MinValue = 1;
                ((XYDiagram)ChartControl.Diagram).AxisX.WholeRange.MaxValue = Probs.Count;
                ((XYDiagram)ChartControl.Diagram).AxisX.WholeRange.Auto = true;
                int i = 0;
                ChartControl.Titles[0].Text = "Предел действия - " + VLKStatic.AddZero(VLKStatic.Povt1) + ";    Предел предупреждения - " + VLKStatic.AddZero(VLKStatic.Povt2) + ";    Средняя линия - " + VLKStatic.AddZero(VLKStatic.Povt3);
                int? ID = -1;
                foreach (Probi Stroke in Probs)
                {
                    i++;
                    ChartControl.Series[0].Points.Add(new SeriesPoint(i, Stroke.rSmall.HasValue ? Stroke.rSmall.Value : 0));
                    ChartControl.Series[1].Points.Add(new SeriesPoint(i, VLKStatic.Povt1));
                    ChartControl.Series[2].Points.Add(new SeriesPoint(i, VLKStatic.Povt2));
                    ChartControl.Series[3].Points.Add(new SeriesPoint(i, VLKStatic.Povt3));
                    ((XYDiagram)ChartControl.Diagram).AxisX.CustomLabels.Add(new CustomAxisLabel { AxisValue = i, Name = i.ToString() });
                    ID = Stroke.AnalizID;
                }
                using (DbDataContext db = new DbDataContext())
                {
                    var Analiz = db.Analiz.Where(c => c.Id == ID).FirstOrDefault();
                    if (Analiz.НазваниеМетодаАнализа.Contains("содержания вод"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля повторяемости\rпри измерении содержания воды по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("механических примес"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля повторяемости\rпри измерении м.к. мех. примесей по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("хлористых сол"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля повторяемости\rпри измерении м.к. хлористых солей по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("плотност"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля повторяемости\rпри измерении плотности по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("насыщенных пар"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля повторяемости\rпри измерении давления насыщенных паров по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("спектрометр"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля повторяемости\rпри измерении массовой доли серы по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("сероводород"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля повторяемости\rпри определении массовой доли сероводорода по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("фракц"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля повторяемости\rпри определении фракционного состава по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("хлорорганическ"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля повторяемости\rпри измерении массовой доли хлорорганических соединений по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("парафин"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля повторяемости\rпри измерении массовой доли парафина по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("вязкост"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля повторяемости\rпри измерении вязкости по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PrintButton_Click(object sender, EventArgs e)
        {
            PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
            pcl.Component = ChartControl;
            ChartControl.OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Zoom;
            pcl.RtfReportHeader = "Химико-аналитическая лаборатория \r";
            pcl.RtfReportFooter = "\rВыполнено с использованием АРМЛАБ";
            pcl.Landscape = true;
            pcl.ShowPreview();
        }

        private void ExcelButton_Click(object sender, EventArgs e)
        {
            try
            {
                PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
                pcl.Component = ChartControl;
                ChartControl.OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Zoom;
                pcl.RtfReportHeader = "Химико-аналитическая лаборатория \r";
                pcl.RtfReportFooter = "\rВыполнено с использованием АРМЛАБ";
                pcl.Landscape = true;
                System.IO.Directory.CreateDirectory(System.Windows.Forms.Application.StartupPath + @"\\Temp");
                var path = System.Windows.Forms.Application.StartupPath + @"\\Temp\\Контроль повторяемости " + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString() + ".xlsx";
                pcl.ExportToXlsx(path);
                FileInfo file = new FileInfo(path);
                if (file.Exists)
                {
                    System.Diagnostics.Process.Start(path);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DiagrammaPovtoryamostForm_Load(object sender, EventArgs e)
        {
        }

        private void DiagrammaPovtoryamostForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
