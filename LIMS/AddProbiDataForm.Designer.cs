﻿namespace LIMS
{
    partial class AddProbiDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddProbiDataForm));
            this.CancelClickButton = new DevExpress.XtraEditors.SimpleButton();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.Shodimost = new System.Windows.Forms.NumericUpDown();
            this.XCP = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.X2 = new System.Windows.Forms.NumericUpDown();
            this.X1 = new System.Windows.Forms.NumericUpDown();
            this.bdrtg35f = new System.Windows.Forms.Label();
            this.sfdyxbd = new System.Windows.Forms.Label();
            this.Shifr = new System.Windows.Forms.Label();
            this.Date = new System.Windows.Forms.Label();
            this.ID = new System.Windows.Forms.Label();
            this.CalculateX12 = new DevExpress.XtraEditors.SimpleButton();
            this.CalculateXcpShod = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Shodimost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XCP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.X2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.X1)).BeginInit();
            this.SuspendLayout();
            // 
            // CancelClickButton
            // 
            this.CancelClickButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelClickButton.Location = new System.Drawing.Point(137, 176);
            this.CancelClickButton.Name = "CancelClickButton";
            this.CancelClickButton.Size = new System.Drawing.Size(75, 26);
            this.CancelClickButton.TabIndex = 6;
            this.CancelClickButton.Text = "Отмена";
            this.CancelClickButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(48, 176);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 26);
            this.OKButton.TabIndex = 5;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.CalculateXcpShod);
            this.panelControl1.Controls.Add(this.CalculateX12);
            this.panelControl1.Controls.Add(this.Shodimost);
            this.panelControl1.Controls.Add(this.XCP);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.X2);
            this.panelControl1.Controls.Add(this.X1);
            this.panelControl1.Controls.Add(this.bdrtg35f);
            this.panelControl1.Controls.Add(this.sfdyxbd);
            this.panelControl1.Controls.Add(this.Shifr);
            this.panelControl1.Controls.Add(this.Date);
            this.panelControl1.Controls.Add(this.ID);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelClickButton);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(264, 213);
            this.panelControl1.TabIndex = 0;
            // 
            // Shodimost
            // 
            this.Shodimost.DecimalPlaces = 7;
            this.Shodimost.Location = new System.Drawing.Point(64, 149);
            this.Shodimost.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.Shodimost.Name = "Shodimost";
            this.Shodimost.Size = new System.Drawing.Size(112, 21);
            this.Shodimost.TabIndex = 4;
            this.Shodimost.ThousandsSeparator = true;
            this.Shodimost.ValueChanged += new System.EventHandler(this.Shodimost_ValueChanged);
            // 
            // XCP
            // 
            this.XCP.DecimalPlaces = 7;
            this.XCP.Location = new System.Drawing.Point(64, 122);
            this.XCP.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.XCP.Name = "XCP";
            this.XCP.Size = new System.Drawing.Size(112, 21);
            this.XCP.TabIndex = 3;
            this.XCP.ThousandsSeparator = true;
            this.XCP.ValueChanged += new System.EventHandler(this.XCP_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "X1-X2:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Xср:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(48, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 30;
            this.label5.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 14);
            this.label2.TabIndex = 28;
            this.label2.Text = "Шифр:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(18, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 14);
            this.label1.TabIndex = 27;
            this.label1.Text = "Дата:";
            // 
            // X2
            // 
            this.X2.DecimalPlaces = 7;
            this.X2.Location = new System.Drawing.Point(64, 95);
            this.X2.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.X2.Name = "X2";
            this.X2.Size = new System.Drawing.Size(112, 21);
            this.X2.TabIndex = 2;
            this.X2.ThousandsSeparator = true;
            this.X2.ValueChanged += new System.EventHandler(this.X2_ValueChanged);
            this.X2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.X1_KeyDown);
            // 
            // X1
            // 
            this.X1.DecimalPlaces = 7;
            this.X1.Location = new System.Drawing.Point(64, 68);
            this.X1.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.X1.Name = "X1";
            this.X1.Size = new System.Drawing.Size(112, 21);
            this.X1.TabIndex = 1;
            this.X1.ThousandsSeparator = true;
            this.X1.ValueChanged += new System.EventHandler(this.X1_ValueChanged);
            this.X1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.X1_KeyDown);
            // 
            // bdrtg35f
            // 
            this.bdrtg35f.AutoSize = true;
            this.bdrtg35f.Location = new System.Drawing.Point(32, 97);
            this.bdrtg35f.Name = "bdrtg35f";
            this.bdrtg35f.Size = new System.Drawing.Size(23, 13);
            this.bdrtg35f.TabIndex = 24;
            this.bdrtg35f.Text = "X2:";
            // 
            // sfdyxbd
            // 
            this.sfdyxbd.AutoSize = true;
            this.sfdyxbd.Location = new System.Drawing.Point(32, 70);
            this.sfdyxbd.Name = "sfdyxbd";
            this.sfdyxbd.Size = new System.Drawing.Size(23, 13);
            this.sfdyxbd.TabIndex = 23;
            this.sfdyxbd.Text = "X1:";
            // 
            // Shifr
            // 
            this.Shifr.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Shifr.Location = new System.Drawing.Point(61, 41);
            this.Shifr.Name = "Shifr";
            this.Shifr.Size = new System.Drawing.Size(116, 16);
            this.Shifr.TabIndex = 16;
            this.Shifr.Text = "Shifr";
            // 
            // Date
            // 
            this.Date.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Date.Location = new System.Drawing.Point(61, 14);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(116, 16);
            this.Date.TabIndex = 15;
            this.Date.Text = "Date";
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Location = new System.Drawing.Point(48, 84);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(0, 13);
            this.ID.TabIndex = 1;
            this.ID.Visible = false;
            // 
            // CalculateX12
            // 
            this.CalculateX12.Appearance.Options.UseTextOptions = true;
            this.CalculateX12.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.CalculateX12.Location = new System.Drawing.Point(182, 70);
            this.CalculateX12.Name = "CalculateX12";
            this.CalculateX12.Size = new System.Drawing.Size(74, 46);
            this.CalculateX12.TabIndex = 34;
            this.CalculateX12.Text = "Расчитать Х1 и Х2";
            this.CalculateX12.Click += new System.EventHandler(this.CalculateX12_Click);
            // 
            // CalculateXcpShod
            // 
            this.CalculateXcpShod.Appearance.Options.UseTextOptions = true;
            this.CalculateXcpShod.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.CalculateXcpShod.Location = new System.Drawing.Point(182, 124);
            this.CalculateXcpShod.Name = "CalculateXcpShod";
            this.CalculateXcpShod.Size = new System.Drawing.Size(74, 46);
            this.CalculateXcpShod.TabIndex = 35;
            this.CalculateXcpShod.Text = "Расчитать Хcp и Х1-Х2";
            this.CalculateXcpShod.Click += new System.EventHandler(this.CalculateXcpShod_Click);
            // 
            // AddProbiDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelClickButton;
            this.ClientSize = new System.Drawing.Size(264, 213);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddProbiDataForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Результаты";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.AddProbiDataForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AddProbiDataForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Shodimost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XCP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.X2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.X1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton CancelClickButton;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Label ID;
        private System.Windows.Forms.Label Date;
        private System.Windows.Forms.Label Shifr;
        private System.Windows.Forms.Label bdrtg35f;
        private System.Windows.Forms.Label sfdyxbd;
        private System.Windows.Forms.NumericUpDown X2;
        private System.Windows.Forms.NumericUpDown X1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown Shodimost;
        private System.Windows.Forms.NumericUpDown XCP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.SimpleButton CalculateXcpShod;
        private DevExpress.XtraEditors.SimpleButton CalculateX12;
    }
}