﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class ReactivRashodForm : DevExpress.XtraEditors.XtraForm
    {
        int? ReactivID;
        public ReactivRashodForm(MainForm MF, Reactiv Stroke)
        {
            InitializeComponent();
            ReactivID = Stroke.Id;
            ID.Text = Stroke.Id.ToString();
            ReactivName.Text = Stroke.НаименованиеРеактива;
            Partia.Text = "Партия: " + Stroke.НомерПартии;
            DateIzgot.Text = Stroke.ДатаИзготовления.HasValue ? "Дата изготовления: " + Stroke.ДатаИзготовления.Value.ToShortDateString() : "";
            var now = DateTime.Now.AddDays(-10);
            Ot.Value = new DateTime(now.Year, now.Month, 1, 0, 0, 0);
            Do.Value = new DateTime(now.Year, now.Month + 1, 1, 0, 0, 0).AddDays(-1);
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Kolvo.Value <= 0) return;
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.Reactiv.Where(c => c.Id == Convert.ToInt32(ID.Text)).FirstOrDefault();
                    if (Stroka.Остаток >= Kolvo.Value && Stroka.Остаток.HasValue)
                    {
                        Stroka.Остаток = Stroka.Остаток - Kolvo.Value;
                        var result = MessageBox.Show("Pеактив: " + Stroka.НаименованиеРеактива + "\rПартия: " + Stroka.НомерПартии + "\rРасход: " + ((decimal)Kolvo.Value).ToString() + " " + Stroka.ЕдИзм + "\rОстаток: " + Stroka.Остаток.Value.ToString() + " " + Stroka.ЕдИзм, "Принять?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (result == DialogResult.OK)
                        {
                            ReactivUchet ReactivUCHET;
                            if (Stroka.ДатаИзготовления.HasValue)
                                ReactivUCHET = db.ReactivUchet.Where(c => c.НаименованиеРеактива == Stroka.НаименованиеРеактива && c.Партия == Stroka.НомерПартии && c.Квалификация == Stroka.Классификация && c.ДатаИзготовления == Stroka.ДатаИзготовления && c.От == Ot.Value && c.До == Do.Value && (c.VisibleStatus == null || c.VisibleStatus < 3)).FirstOrDefault();
                            else ReactivUCHET = db.ReactivUchet.Where(c => c.НаименованиеРеактива == Stroka.НаименованиеРеактива && c.Партия == Stroka.НомерПартии && c.Квалификация == Stroka.Классификация && c.ДатаИзготовления == null && c.От == Ot.Value && c.До == Do.Value && (c.VisibleStatus == null || c.VisibleStatus < 3)).FirstOrDefault();
                            var NextID = db.ReactivUchet != null && db.ReactivUchet.Count() > 0 ? db.ReactivUchet.Max(c => c.Id) + 1 : 1;
                            if (ReactivUCHET == null)
                            {
                                db.ReactivUchet.InsertOnSubmit(new ReactivUchet
                                {
                                    Id = NextID,
                                    MainID = Convert.ToInt32(ID.Text),
                                    НаименованиеРеактива = ReactivName.Text,
                                    Квалификация = Stroka.Классификация,
                                    Партия = Stroka.НомерПартии,
                                    ДатаИзготовления = Stroka.ДатаИзготовления,
                                    От = Ot.Value,
                                    До = Do.Value,
                                    Приход = 0,
                                    Расход = Kolvo.Value,
                                    Остаток = Stroka.Остаток,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    ReactivID = ReactivID,
                                    Owner = null
                                });
                                db.ReactivUchet.InsertOnSubmit(new ReactivUchet
                                {
                                    Id = NextID + 1,
                                    MainID = Convert.ToInt32(ID.Text),
                                    НаименованиеРеактива = ReactivName.Text,
                                    Квалификация = Stroka.Классификация,
                                    Партия = Stroka.НомерПартии,
                                    ДатаИзготовления = Stroka.ДатаИзготовления,
                                    От = Ot.Value,
                                    До = Do.Value,
                                    Приход = 0,
                                    Расход = Kolvo.Value,
                                    Остаток = Stroka.Остаток,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    ReactivID = ReactivID,
                                    Owner = NextID
                                });
                            }
                            else
                            {
                                ReactivUCHET.MainID = ReactivID;
                                ReactivUCHET.ДатаСозданияЗаписи = DateTime.Now;
                                ReactivUCHET.UserName = UserSettings.User;
                                ReactivUCHET.ReactivID = ReactivID;
                                ReactivUCHET.Приход = ReactivUCHET.Приход;
                                ReactivUCHET.Расход = ReactivUCHET.Расход + Kolvo.Value;
                                ReactivUCHET.Остаток = Stroka.Остаток;
                            }
                            db.SubmitChanges();
                            this.Close();
                        }
                        else return;
                    }
                    else
                    {
                        MessageBox.Show("Остаток реактива " + Stroka.НаименованиеРеактива + " меньше, чем вы собираетесь списывать!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Ot_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void ReactivRashodForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
