﻿namespace LIMS
{
    partial class CheckIOColumnForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckIOColumnForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.CheckEditPravoSobstvennosti = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.CheckEditPrimeshanie = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateSledTO = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditTehnichHarakteristiki = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDatePoslednegoTO = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditGogVipuska = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditIzgotovitel = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditN = new DevExpress.XtraEditors.CheckEdit();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.CancelButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEditMestoUstanovkiIHranenia = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateSledAttestachii = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditPeriodichnost = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDataAttestachii = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditNDokumentaAttestachii = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditGodVvodaVEkspluatachiu = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditInventarniiN = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditZavN = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditTip = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditName = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditNameHaracteristis = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPravoSobstvennosti.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrimeshanie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateSledTO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditTehnichHarakteristiki.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDatePoslednegoTO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGogVipuska.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditIzgotovitel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditMestoUstanovkiIHranenia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateSledAttestachii.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPeriodichnost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDataAttestachii.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNDokumentaAttestachii.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGodVvodaVEkspluatachiu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditInventarniiN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditZavN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditTip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNameHaracteristis.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.CheckEditPravoSobstvennosti);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.CheckEditPrimeshanie);
            this.panelControl1.Controls.Add(this.CheckEditDateSledTO);
            this.panelControl1.Controls.Add(this.CheckEditTehnichHarakteristiki);
            this.panelControl1.Controls.Add(this.CheckEditDatePoslednegoTO);
            this.panelControl1.Controls.Add(this.CheckEditGogVipuska);
            this.panelControl1.Controls.Add(this.CheckEditIzgotovitel);
            this.panelControl1.Controls.Add(this.CheckEditN);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelButton1);
            this.panelControl1.Controls.Add(this.CheckEditMestoUstanovkiIHranenia);
            this.panelControl1.Controls.Add(this.CheckEditDateSledAttestachii);
            this.panelControl1.Controls.Add(this.CheckEditPeriodichnost);
            this.panelControl1.Controls.Add(this.CheckEditDataAttestachii);
            this.panelControl1.Controls.Add(this.CheckEditNDokumentaAttestachii);
            this.panelControl1.Controls.Add(this.CheckEditGodVvodaVEkspluatachiu);
            this.panelControl1.Controls.Add(this.CheckEditInventarniiN);
            this.panelControl1.Controls.Add(this.CheckEditZavN);
            this.panelControl1.Controls.Add(this.CheckEditTip);
            this.panelControl1.Controls.Add(this.CheckEditName);
            this.panelControl1.Controls.Add(this.CheckEditNameHaracteristis);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(426, 270);
            this.panelControl1.TabIndex = 0;
            // 
            // CheckEditPravoSobstvennosti
            // 
            this.CheckEditPravoSobstvennosti.EditValue = true;
            this.CheckEditPravoSobstvennosti.Location = new System.Drawing.Point(231, 112);
            this.CheckEditPravoSobstvennosti.Name = "CheckEditPravoSobstvennosti";
            this.CheckEditPravoSobstvennosti.Properties.Caption = "Право собственности";
            this.CheckEditPravoSobstvennosti.Size = new System.Drawing.Size(190, 19);
            this.CheckEditPravoSobstvennosti.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 227);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "теристики, рабочие диапазоны";
            // 
            // CheckEditPrimeshanie
            // 
            this.CheckEditPrimeshanie.EditValue = true;
            this.CheckEditPrimeshanie.Location = new System.Drawing.Point(231, 210);
            this.CheckEditPrimeshanie.Name = "CheckEditPrimeshanie";
            this.CheckEditPrimeshanie.Properties.Caption = "Примечание";
            this.CheckEditPrimeshanie.Size = new System.Drawing.Size(172, 19);
            this.CheckEditPrimeshanie.TabIndex = 18;
            this.CheckEditPrimeshanie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDateSledTO
            // 
            this.CheckEditDateSledTO.EditValue = true;
            this.CheckEditDateSledTO.Location = new System.Drawing.Point(231, 187);
            this.CheckEditDateSledTO.Name = "CheckEditDateSledTO";
            this.CheckEditDateSledTO.Properties.Caption = "Дата следующего ТО";
            this.CheckEditDateSledTO.Size = new System.Drawing.Size(172, 19);
            this.CheckEditDateSledTO.TabIndex = 17;
            this.CheckEditDateSledTO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditTehnichHarakteristiki
            // 
            this.CheckEditTehnichHarakteristiki.EditValue = true;
            this.CheckEditTehnichHarakteristiki.Location = new System.Drawing.Point(12, 210);
            this.CheckEditTehnichHarakteristiki.Name = "CheckEditTehnichHarakteristiki";
            this.CheckEditTehnichHarakteristiki.Properties.Caption = "Основные технические харак-";
            this.CheckEditTehnichHarakteristiki.Size = new System.Drawing.Size(217, 19);
            this.CheckEditTehnichHarakteristiki.TabIndex = 9;
            this.CheckEditTehnichHarakteristiki.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDatePoslednegoTO
            // 
            this.CheckEditDatePoslednegoTO.EditValue = true;
            this.CheckEditDatePoslednegoTO.Location = new System.Drawing.Point(231, 162);
            this.CheckEditDatePoslednegoTO.Name = "CheckEditDatePoslednegoTO";
            this.CheckEditDatePoslednegoTO.Properties.Caption = "Дата последнего ТО";
            this.CheckEditDatePoslednegoTO.Size = new System.Drawing.Size(174, 19);
            this.CheckEditDatePoslednegoTO.TabIndex = 16;
            this.CheckEditDatePoslednegoTO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditGogVipuska
            // 
            this.CheckEditGogVipuska.EditValue = true;
            this.CheckEditGogVipuska.Location = new System.Drawing.Point(12, 187);
            this.CheckEditGogVipuska.Name = "CheckEditGogVipuska";
            this.CheckEditGogVipuska.Properties.Caption = "Год выпуска";
            this.CheckEditGogVipuska.Size = new System.Drawing.Size(217, 19);
            this.CheckEditGogVipuska.TabIndex = 8;
            this.CheckEditGogVipuska.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditIzgotovitel
            // 
            this.CheckEditIzgotovitel.EditValue = true;
            this.CheckEditIzgotovitel.Location = new System.Drawing.Point(12, 162);
            this.CheckEditIzgotovitel.Name = "CheckEditIzgotovitel";
            this.CheckEditIzgotovitel.Properties.Caption = "Изготовитель";
            this.CheckEditIzgotovitel.Size = new System.Drawing.Size(217, 19);
            this.CheckEditIzgotovitel.TabIndex = 7;
            this.CheckEditIzgotovitel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditN
            // 
            this.CheckEditN.EditValue = true;
            this.CheckEditN.Location = new System.Drawing.Point(12, 12);
            this.CheckEditN.Name = "CheckEditN";
            this.CheckEditN.Properties.Caption = "№ п/п";
            this.CheckEditN.Size = new System.Drawing.Size(112, 19);
            this.CheckEditN.TabIndex = 1;
            this.CheckEditN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(231, 239);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 19;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton1
            // 
            this.CancelButton1.Location = new System.Drawing.Point(314, 239);
            this.CancelButton1.Name = "CancelButton1";
            this.CancelButton1.Size = new System.Drawing.Size(75, 23);
            this.CancelButton1.TabIndex = 20;
            this.CancelButton1.Text = "Отмена";
            this.CancelButton1.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CheckEditMestoUstanovkiIHranenia
            // 
            this.CheckEditMestoUstanovkiIHranenia.EditValue = true;
            this.CheckEditMestoUstanovkiIHranenia.Location = new System.Drawing.Point(231, 137);
            this.CheckEditMestoUstanovkiIHranenia.Name = "CheckEditMestoUstanovkiIHranenia";
            this.CheckEditMestoUstanovkiIHranenia.Properties.Caption = "Место установки или хранения";
            this.CheckEditMestoUstanovkiIHranenia.Size = new System.Drawing.Size(190, 19);
            this.CheckEditMestoUstanovkiIHranenia.TabIndex = 15;
            this.CheckEditMestoUstanovkiIHranenia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDateSledAttestachii
            // 
            this.CheckEditDateSledAttestachii.EditValue = true;
            this.CheckEditDateSledAttestachii.Location = new System.Drawing.Point(231, 87);
            this.CheckEditDateSledAttestachii.Name = "CheckEditDateSledAttestachii";
            this.CheckEditDateSledAttestachii.Properties.Caption = "Дата следующей аттестации";
            this.CheckEditDateSledAttestachii.Size = new System.Drawing.Size(181, 19);
            this.CheckEditDateSledAttestachii.TabIndex = 14;
            this.CheckEditDateSledAttestachii.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditPeriodichnost
            // 
            this.CheckEditPeriodichnost.EditValue = true;
            this.CheckEditPeriodichnost.Location = new System.Drawing.Point(231, 62);
            this.CheckEditPeriodichnost.Name = "CheckEditPeriodichnost";
            this.CheckEditPeriodichnost.Properties.Caption = "Периодичность";
            this.CheckEditPeriodichnost.Size = new System.Drawing.Size(141, 19);
            this.CheckEditPeriodichnost.TabIndex = 13;
            this.CheckEditPeriodichnost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDataAttestachii
            // 
            this.CheckEditDataAttestachii.EditValue = true;
            this.CheckEditDataAttestachii.Location = new System.Drawing.Point(231, 37);
            this.CheckEditDataAttestachii.Name = "CheckEditDataAttestachii";
            this.CheckEditDataAttestachii.Properties.Caption = "Дата аттестации";
            this.CheckEditDataAttestachii.Size = new System.Drawing.Size(109, 19);
            this.CheckEditDataAttestachii.TabIndex = 12;
            this.CheckEditDataAttestachii.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditNDokumentaAttestachii
            // 
            this.CheckEditNDokumentaAttestachii.EditValue = true;
            this.CheckEditNDokumentaAttestachii.Location = new System.Drawing.Point(231, 12);
            this.CheckEditNDokumentaAttestachii.Name = "CheckEditNDokumentaAttestachii";
            this.CheckEditNDokumentaAttestachii.Properties.Caption = "№ документа об аттестации ИО";
            this.CheckEditNDokumentaAttestachii.Size = new System.Drawing.Size(190, 19);
            this.CheckEditNDokumentaAttestachii.TabIndex = 11;
            this.CheckEditNDokumentaAttestachii.CheckedChanged += new System.EventHandler(this.CheckEditDateGet_CheckedChanged);
            this.CheckEditNDokumentaAttestachii.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditGodVvodaVEkspluatachiu
            // 
            this.CheckEditGodVvodaVEkspluatachiu.EditValue = true;
            this.CheckEditGodVvodaVEkspluatachiu.Location = new System.Drawing.Point(12, 243);
            this.CheckEditGodVvodaVEkspluatachiu.Name = "CheckEditGodVvodaVEkspluatachiu";
            this.CheckEditGodVvodaVEkspluatachiu.Properties.Caption = "Год ввода в эксплуатацию";
            this.CheckEditGodVvodaVEkspluatachiu.Size = new System.Drawing.Size(181, 19);
            this.CheckEditGodVvodaVEkspluatachiu.TabIndex = 10;
            this.CheckEditGodVvodaVEkspluatachiu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditInventarniiN
            // 
            this.CheckEditInventarniiN.EditValue = true;
            this.CheckEditInventarniiN.Location = new System.Drawing.Point(12, 137);
            this.CheckEditInventarniiN.Name = "CheckEditInventarniiN";
            this.CheckEditInventarniiN.Properties.Caption = "Инвентарный номер";
            this.CheckEditInventarniiN.Size = new System.Drawing.Size(127, 19);
            this.CheckEditInventarniiN.TabIndex = 6;
            this.CheckEditInventarniiN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditZavN
            // 
            this.CheckEditZavN.EditValue = true;
            this.CheckEditZavN.Location = new System.Drawing.Point(12, 112);
            this.CheckEditZavN.Name = "CheckEditZavN";
            this.CheckEditZavN.Properties.Caption = "Заводской номер";
            this.CheckEditZavN.Size = new System.Drawing.Size(145, 19);
            this.CheckEditZavN.TabIndex = 5;
            this.CheckEditZavN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditTip
            // 
            this.CheckEditTip.EditValue = true;
            this.CheckEditTip.Location = new System.Drawing.Point(12, 87);
            this.CheckEditTip.Name = "CheckEditTip";
            this.CheckEditTip.Properties.Caption = "Тип, марка";
            this.CheckEditTip.Size = new System.Drawing.Size(112, 19);
            this.CheckEditTip.TabIndex = 4;
            this.CheckEditTip.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditName
            // 
            this.CheckEditName.EditValue = true;
            this.CheckEditName.Location = new System.Drawing.Point(12, 62);
            this.CheckEditName.Name = "CheckEditName";
            this.CheckEditName.Properties.Caption = "Наименование ИО";
            this.CheckEditName.Size = new System.Drawing.Size(112, 19);
            this.CheckEditName.TabIndex = 3;
            this.CheckEditName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditNameHaracteristis
            // 
            this.CheckEditNameHaracteristis.EditValue = true;
            this.CheckEditNameHaracteristis.Location = new System.Drawing.Point(12, 37);
            this.CheckEditNameHaracteristis.Name = "CheckEditNameHaracteristis";
            this.CheckEditNameHaracteristis.Properties.Caption = "Наим. опред-мых характер. прод.";
            this.CheckEditNameHaracteristis.Size = new System.Drawing.Size(215, 19);
            this.CheckEditNameHaracteristis.TabIndex = 2;
            this.CheckEditNameHaracteristis.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckIOColumnForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 270);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "CheckIOColumnForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ ИО";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckIOColumnForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPravoSobstvennosti.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrimeshanie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateSledTO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditTehnichHarakteristiki.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDatePoslednegoTO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGogVipuska.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditIzgotovitel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditMestoUstanovkiIHranenia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateSledAttestachii.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPeriodichnost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDataAttestachii.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNDokumentaAttestachii.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGodVvodaVEkspluatachiu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditInventarniiN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditZavN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditTip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNameHaracteristis.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit CheckEditNameHaracteristis;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateSledAttestachii;
        private DevExpress.XtraEditors.CheckEdit CheckEditPeriodichnost;
        private DevExpress.XtraEditors.CheckEdit CheckEditDataAttestachii;
        private DevExpress.XtraEditors.CheckEdit CheckEditNDokumentaAttestachii;
        private DevExpress.XtraEditors.CheckEdit CheckEditGodVvodaVEkspluatachiu;
        private DevExpress.XtraEditors.CheckEdit CheckEditInventarniiN;
        private DevExpress.XtraEditors.CheckEdit CheckEditZavN;
        private DevExpress.XtraEditors.CheckEdit CheckEditTip;
        private DevExpress.XtraEditors.CheckEdit CheckEditName;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.SimpleButton CancelButton1;
        private DevExpress.XtraEditors.CheckEdit CheckEditMestoUstanovkiIHranenia;
        private DevExpress.XtraEditors.CheckEdit CheckEditN;
        private DevExpress.XtraEditors.CheckEdit CheckEditIzgotovitel;
        private DevExpress.XtraEditors.CheckEdit CheckEditGogVipuska;
        private DevExpress.XtraEditors.CheckEdit CheckEditDatePoslednegoTO;
        private DevExpress.XtraEditors.CheckEdit CheckEditTehnichHarakteristiki;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateSledTO;
        private DevExpress.XtraEditors.CheckEdit CheckEditPrimeshanie;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit CheckEditPravoSobstvennosti;
    }
}