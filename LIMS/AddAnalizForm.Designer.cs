﻿namespace LIMS
{
    partial class AddAnalizForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddAnalizForm));
            this.EditIdControl = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.CancelButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.AnalizName = new System.Windows.Forms.TextBox();
            this.AnalizOpredelyaimiiKomponent = new System.Windows.Forms.TextBox();
            this.AnalizShifr = new System.Windows.Forms.TextBox();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.AnalizKontroliruemiiObekt = new System.Windows.Forms.TextBox();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.ND = new System.Windows.Forms.TextBox();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.AnalizEdIzm = new System.Windows.Forms.ComboBox();
            this.AnalizGrid = new DevExpress.XtraGrid.GridControl();
            this.AnalizGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.AnalizContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.CalculateProfile = new System.Windows.Forms.ComboBox();
            this.CheckCalculate = new System.Windows.Forms.RadioButton();
            this.CheckTabl = new System.Windows.Forms.RadioButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.ShortName = new System.Windows.Forms.TextBox();
            this.ShortNameДфиуд = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.AnalizGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnalizGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnalizContainerControl)).BeginInit();
            this.AnalizContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // EditIdControl
            // 
            this.EditIdControl.Location = new System.Drawing.Point(17, 42);
            this.EditIdControl.Name = "EditIdControl";
            this.EditIdControl.Size = new System.Drawing.Size(136, 13);
            this.EditIdControl.TabIndex = 0;
            this.EditIdControl.Text = "Название метода анализа:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(50, 113);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(103, 13);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Единица измерения:";
            // 
            // OKButton
            // 
            this.OKButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.OKButton.Location = new System.Drawing.Point(288, 2);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 24);
            this.OKButton.TabIndex = 12;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton1
            // 
            this.CancelButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton1.Dock = System.Windows.Forms.DockStyle.Right;
            this.CancelButton1.Location = new System.Drawing.Point(363, 2);
            this.CancelButton1.Name = "CancelButton1";
            this.CancelButton1.Size = new System.Drawing.Size(74, 24);
            this.CancelButton1.TabIndex = 13;
            this.CancelButton1.Text = "Отмена";
            this.CancelButton1.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 86);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(138, 13);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "Определяемый компонент:";
            // 
            // AnalizName
            // 
            this.AnalizName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AnalizName.Location = new System.Drawing.Point(159, 39);
            this.AnalizName.Multiline = true;
            this.AnalizName.Name = "AnalizName";
            this.AnalizName.Size = new System.Drawing.Size(268, 38);
            this.AnalizName.TabIndex = 2;
            this.AnalizName.TextChanged += new System.EventHandler(this.AnalizName_TextChanged);
            this.AnalizName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterKeyDown);
            // 
            // AnalizOpredelyaimiiKomponent
            // 
            this.AnalizOpredelyaimiiKomponent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AnalizOpredelyaimiiKomponent.Location = new System.Drawing.Point(159, 83);
            this.AnalizOpredelyaimiiKomponent.Name = "AnalizOpredelyaimiiKomponent";
            this.AnalizOpredelyaimiiKomponent.Size = new System.Drawing.Size(268, 21);
            this.AnalizOpredelyaimiiKomponent.TabIndex = 3;
            this.AnalizOpredelyaimiiKomponent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterKeyDown);
            // 
            // AnalizShifr
            // 
            this.AnalizShifr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AnalizShifr.Location = new System.Drawing.Point(159, 164);
            this.AnalizShifr.Name = "AnalizShifr";
            this.AnalizShifr.Size = new System.Drawing.Size(268, 21);
            this.AnalizShifr.TabIndex = 6;
            this.AnalizShifr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterKeyDown);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(119, 167);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(34, 13);
            this.labelControl2.TabIndex = 24;
            this.labelControl2.Text = "Шифр:";
            // 
            // AnalizKontroliruemiiObekt
            // 
            this.AnalizKontroliruemiiObekt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AnalizKontroliruemiiObekt.Location = new System.Drawing.Point(159, 137);
            this.AnalizKontroliruemiiObekt.Name = "AnalizKontroliruemiiObekt";
            this.AnalizKontroliruemiiObekt.Size = new System.Drawing.Size(268, 21);
            this.AnalizKontroliruemiiObekt.TabIndex = 5;
            this.AnalizKontroliruemiiObekt.Text = "Нефть";
            this.AnalizKontroliruemiiObekt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterKeyDown);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(22, 140);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(131, 13);
            this.labelControl3.TabIndex = 26;
            this.labelControl3.Text = "Контролируемый объект:";
            // 
            // ND
            // 
            this.ND.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ND.Location = new System.Drawing.Point(159, 12);
            this.ND.Name = "ND";
            this.ND.Size = new System.Drawing.Size(268, 21);
            this.ND.TabIndex = 1;
            this.ND.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterKeyDown);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(41, 15);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(112, 13);
            this.labelControl5.TabIndex = 28;
            this.labelControl5.Text = "НД на метод анализа:";
            // 
            // AnalizEdIzm
            // 
            this.AnalizEdIzm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AnalizEdIzm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.AnalizEdIzm.FormattingEnabled = true;
            this.AnalizEdIzm.Items.AddRange(new object[] {
            "амп.",
            "кг.",
            "г.",
            "мг.",
            "л.",
            "мл."});
            this.AnalizEdIzm.Location = new System.Drawing.Point(159, 110);
            this.AnalizEdIzm.Name = "AnalizEdIzm";
            this.AnalizEdIzm.Size = new System.Drawing.Size(268, 21);
            this.AnalizEdIzm.TabIndex = 4;
            this.AnalizEdIzm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterKeyDown);
            // 
            // AnalizGrid
            // 
            this.AnalizGrid.AccessibleDescription = "AnalizGrid";
            this.AnalizGrid.AccessibleName = "ReactivGrid";
            this.AnalizGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AnalizGrid.Location = new System.Drawing.Point(0, 0);
            this.AnalizGrid.MainView = this.AnalizGridView;
            this.AnalizGrid.Name = "AnalizGrid";
            this.AnalizGrid.Size = new System.Drawing.Size(439, 300);
            this.AnalizGrid.TabIndex = 999;
            this.AnalizGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.AnalizGridView});
            // 
            // AnalizGridView
            // 
            this.AnalizGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.AnalizGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AnalizGridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.AnalizGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.AnalizGridView.AppearancePrint.EvenRow.Options.UseTextOptions = true;
            this.AnalizGridView.AppearancePrint.EvenRow.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.AnalizGridView.AppearancePrint.EvenRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.AnalizGridView.AppearancePrint.GroupRow.Options.UseTextOptions = true;
            this.AnalizGridView.AppearancePrint.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.AnalizGridView.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.AnalizGridView.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AnalizGridView.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.AnalizGridView.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.AnalizGridView.AppearancePrint.Lines.Options.UseTextOptions = true;
            this.AnalizGridView.AppearancePrint.Lines.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.AnalizGridView.AppearancePrint.OddRow.Options.UseTextOptions = true;
            this.AnalizGridView.AppearancePrint.OddRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.AnalizGridView.AppearancePrint.Preview.Options.UseTextOptions = true;
            this.AnalizGridView.AppearancePrint.Preview.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.AnalizGridView.AppearancePrint.Row.Options.UseTextOptions = true;
            this.AnalizGridView.AppearancePrint.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AnalizGridView.AppearancePrint.Row.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.AnalizGridView.AppearancePrint.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.AnalizGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.AnalizGridView.GridControl = this.AnalizGrid;
            this.AnalizGridView.Name = "AnalizGridView";
            this.AnalizGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.AnalizGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.AnalizGridView.OptionsCustomization.AllowRowSizing = true;
            this.AnalizGridView.OptionsFind.AllowFindPanel = false;
            this.AnalizGridView.OptionsNavigation.AutoFocusNewRow = true;
            this.AnalizGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.AnalizGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.AnalizGridView.OptionsView.RowAutoHeight = true;
            this.AnalizGridView.OptionsView.ShowGroupPanel = false;
            this.AnalizGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.AnalizGridView_PopupMenuShowing);
            // 
            // AnalizContainerControl
            // 
            this.AnalizContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AnalizContainerControl.Horizontal = false;
            this.AnalizContainerControl.Location = new System.Drawing.Point(0, 0);
            this.AnalizContainerControl.Name = "AnalizContainerControl";
            this.AnalizContainerControl.Panel1.Controls.Add(this.panelControl1);
            this.AnalizContainerControl.Panel1.Text = "Panel1";
            this.AnalizContainerControl.Panel2.Controls.Add(this.AnalizGrid);
            this.AnalizContainerControl.Panel2.Text = "Panel2";
            this.AnalizContainerControl.Size = new System.Drawing.Size(439, 569);
            this.AnalizContainerControl.SplitterPosition = 264;
            this.AnalizContainerControl.TabIndex = 75;
            this.AnalizContainerControl.Text = "splitContainerControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.CalculateProfile);
            this.panelControl1.Controls.Add(this.CheckCalculate);
            this.panelControl1.Controls.Add(this.CheckTabl);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.ShortName);
            this.panelControl1.Controls.Add(this.ShortNameДфиуд);
            this.panelControl1.Controls.Add(this.ND);
            this.panelControl1.Controls.Add(this.AnalizEdIzm);
            this.panelControl1.Controls.Add(this.EditIdControl);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.AnalizKontroliruemiiObekt);
            this.panelControl1.Controls.Add(this.AnalizName);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.AnalizOpredelyaimiiKomponent);
            this.panelControl1.Controls.Add(this.AnalizShifr);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(439, 264);
            this.panelControl1.TabIndex = 0;
            // 
            // CalculateProfile
            // 
            this.CalculateProfile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CalculateProfile.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CalculateProfile.FormattingEnabled = true;
            this.CalculateProfile.Items.AddRange(new object[] {
            "Сера",
            "Вязкость",
            "Парафин",
            "Хлорорганика"});
            this.CalculateProfile.Location = new System.Drawing.Point(157, 264);
            this.CalculateProfile.Name = "CalculateProfile";
            this.CalculateProfile.Size = new System.Drawing.Size(268, 21);
            this.CalculateProfile.TabIndex = 10;
            this.CalculateProfile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterKeyDown);
            // 
            // CheckCalculate
            // 
            this.CheckCalculate.AutoSize = true;
            this.CheckCalculate.Location = new System.Drawing.Point(157, 241);
            this.CheckCalculate.Name = "CheckCalculate";
            this.CheckCalculate.Size = new System.Drawing.Size(69, 17);
            this.CheckCalculate.TabIndex = 9;
            this.CheckCalculate.Text = "Формуле";
            this.CheckCalculate.UseVisualStyleBackColor = true;
            this.CheckCalculate.CheckedChanged += new System.EventHandler(this.CheckCalculate_CheckedChanged);
            // 
            // CheckTabl
            // 
            this.CheckTabl.AutoSize = true;
            this.CheckTabl.Checked = true;
            this.CheckTabl.Location = new System.Drawing.Point(157, 218);
            this.CheckTabl.Name = "CheckTabl";
            this.CheckTabl.Size = new System.Drawing.Size(129, 17);
            this.CheckTabl.TabIndex = 8;
            this.CheckTabl.TabStop = true;
            this.CheckTabl.Text = "Табличным даннным";
            this.CheckTabl.UseVisualStyleBackColor = true;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(17, 220);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(134, 13);
            this.labelControl6.TabIndex = 76;
            this.labelControl6.Text = "Расчитать показатели по:";
            // 
            // ShortName
            // 
            this.ShortName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ShortName.Location = new System.Drawing.Point(159, 191);
            this.ShortName.Name = "ShortName";
            this.ShortName.Size = new System.Drawing.Size(268, 21);
            this.ShortName.TabIndex = 7;
            this.ShortName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterKeyDown);
            // 
            // ShortNameДфиуд
            // 
            this.ShortNameДфиуд.Location = new System.Drawing.Point(79, 194);
            this.ShortNameДфиуд.Name = "ShortNameДфиуд";
            this.ShortNameДфиуд.Size = new System.Drawing.Size(74, 13);
            this.ShortNameДфиуд.TabIndex = 74;
            this.ShortNameДфиуд.Text = "Короткое имя:";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.OKButton);
            this.panelControl2.Controls.Add(this.CancelButton1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 569);
            this.panelControl2.MaximumSize = new System.Drawing.Size(0, 28);
            this.panelControl2.MinimumSize = new System.Drawing.Size(0, 28);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(439, 28);
            this.panelControl2.TabIndex = 76;
            // 
            // AddAnalizForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelButton1;
            this.ClientSize = new System.Drawing.Size(439, 597);
            this.Controls.Add(this.AnalizContainerControl);
            this.Controls.Add(this.panelControl2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "AddAnalizForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавить";
            this.Load += new System.EventHandler(this.AddAnalizForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AddAnalizForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.AnalizGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnalizGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnalizContainerControl)).EndInit();
            this.AnalizContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl EditIdControl;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.SimpleButton CancelButton1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.TextBox AnalizName;
        private System.Windows.Forms.TextBox AnalizOpredelyaimiiKomponent;
        private System.Windows.Forms.TextBox AnalizShifr;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.TextBox AnalizKontroliruemiiObekt;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.TextBox ND;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        public System.Windows.Forms.ComboBox AnalizEdIzm;
        public DevExpress.XtraGrid.GridControl AnalizGrid;
        public DevExpress.XtraGrid.Views.Grid.GridView AnalizGridView;
        private DevExpress.XtraEditors.SplitContainerControl AnalizContainerControl;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.TextBox ShortName;
        private DevExpress.XtraEditors.LabelControl ShortNameДфиуд;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private System.Windows.Forms.RadioButton CheckCalculate;
        private System.Windows.Forms.RadioButton CheckTabl;
        public System.Windows.Forms.ComboBox CalculateProfile;
    }
}