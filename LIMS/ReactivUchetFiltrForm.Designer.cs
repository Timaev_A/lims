﻿namespace LIMS
{
    partial class ReactivUchetFiltrForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReactivUchetFiltrForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DateTimeDo = new System.Windows.Forms.DateTimePicker();
            this.DateTimeOt = new System.Windows.Forms.DateTimePicker();
            this.СheckEditSearchByDate = new DevExpress.XtraEditors.CheckEdit();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.CancelClickButton = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEditOstatok = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditRashod = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditPrihod = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDo = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditOt = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateIzgot = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditNPartii = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditClass = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditName = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.СheckEditSearchByDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditOstatok.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRashod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrihod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditOt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateIzgot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNPartii.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditClass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.DateTimeDo);
            this.panelControl1.Controls.Add(this.DateTimeOt);
            this.panelControl1.Controls.Add(this.СheckEditSearchByDate);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelClickButton);
            this.panelControl1.Controls.Add(this.CheckEditOstatok);
            this.panelControl1.Controls.Add(this.CheckEditRashod);
            this.panelControl1.Controls.Add(this.CheckEditPrihod);
            this.panelControl1.Controls.Add(this.CheckEditDo);
            this.panelControl1.Controls.Add(this.CheckEditOt);
            this.panelControl1.Controls.Add(this.CheckEditDateIzgot);
            this.panelControl1.Controls.Add(this.CheckEditNPartii);
            this.panelControl1.Controls.Add(this.CheckEditClass);
            this.panelControl1.Controls.Add(this.CheckEditName);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(324, 253);
            this.panelControl1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(104, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 14);
            this.label3.TabIndex = 19;
            this.label3.Text = "Показывать колонки";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(163, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "До";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "От";
            // 
            // DateTimeDo
            // 
            this.DateTimeDo.Enabled = false;
            this.DateTimeDo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTimeDo.Location = new System.Drawing.Point(190, 37);
            this.DateTimeDo.Name = "DateTimeDo";
            this.DateTimeDo.Size = new System.Drawing.Size(121, 21);
            this.DateTimeDo.TabIndex = 3;
            this.DateTimeDo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // DateTimeOt
            // 
            this.DateTimeOt.Enabled = false;
            this.DateTimeOt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateTimeOt.Location = new System.Drawing.Point(36, 37);
            this.DateTimeOt.Name = "DateTimeOt";
            this.DateTimeOt.Size = new System.Drawing.Size(121, 21);
            this.DateTimeOt.TabIndex = 2;
            this.DateTimeOt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // СheckEditSearchByDate
            // 
            this.СheckEditSearchByDate.Location = new System.Drawing.Point(12, 12);
            this.СheckEditSearchByDate.Name = "СheckEditSearchByDate";
            this.СheckEditSearchByDate.Properties.Caption = "Поиск по дате";
            this.СheckEditSearchByDate.Size = new System.Drawing.Size(109, 19);
            this.СheckEditSearchByDate.TabIndex = 1;
            this.СheckEditSearchByDate.CheckedChanged += new System.EventHandler(this.СheckEditSearchByDate_CheckedChanged);
            this.СheckEditSearchByDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(79, 219);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 13;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelClickButton
            // 
            this.CancelClickButton.Location = new System.Drawing.Point(175, 219);
            this.CancelClickButton.Name = "CancelClickButton";
            this.CancelClickButton.Size = new System.Drawing.Size(75, 23);
            this.CancelClickButton.TabIndex = 12;
            this.CancelClickButton.Text = "Отмена";
            this.CancelClickButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CheckEditOstatok
            // 
            this.CheckEditOstatok.EditValue = true;
            this.CheckEditOstatok.Location = new System.Drawing.Point(199, 194);
            this.CheckEditOstatok.Name = "CheckEditOstatok";
            this.CheckEditOstatok.Properties.Caption = "Остаток";
            this.CheckEditOstatok.Size = new System.Drawing.Size(109, 19);
            this.CheckEditOstatok.TabIndex = 12;
            this.CheckEditOstatok.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // CheckEditRashod
            // 
            this.CheckEditRashod.EditValue = true;
            this.CheckEditRashod.Location = new System.Drawing.Point(199, 169);
            this.CheckEditRashod.Name = "CheckEditRashod";
            this.CheckEditRashod.Properties.Caption = "Расход";
            this.CheckEditRashod.Size = new System.Drawing.Size(109, 19);
            this.CheckEditRashod.TabIndex = 11;
            this.CheckEditRashod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // CheckEditPrihod
            // 
            this.CheckEditPrihod.EditValue = true;
            this.CheckEditPrihod.Location = new System.Drawing.Point(199, 144);
            this.CheckEditPrihod.Name = "CheckEditPrihod";
            this.CheckEditPrihod.Properties.Caption = "Приход";
            this.CheckEditPrihod.Size = new System.Drawing.Size(109, 19);
            this.CheckEditPrihod.TabIndex = 10;
            this.CheckEditPrihod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // CheckEditDo
            // 
            this.CheckEditDo.EditValue = true;
            this.CheckEditDo.Location = new System.Drawing.Point(199, 119);
            this.CheckEditDo.Name = "CheckEditDo";
            this.CheckEditDo.Properties.Caption = "До";
            this.CheckEditDo.Size = new System.Drawing.Size(109, 19);
            this.CheckEditDo.TabIndex = 9;
            this.CheckEditDo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // CheckEditOt
            // 
            this.CheckEditOt.EditValue = true;
            this.CheckEditOt.Location = new System.Drawing.Point(199, 94);
            this.CheckEditOt.Name = "CheckEditOt";
            this.CheckEditOt.Properties.Caption = "От";
            this.CheckEditOt.Size = new System.Drawing.Size(109, 19);
            this.CheckEditOt.TabIndex = 8;
            this.CheckEditOt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // CheckEditDateIzgot
            // 
            this.CheckEditDateIzgot.EditValue = true;
            this.CheckEditDateIzgot.Location = new System.Drawing.Point(66, 169);
            this.CheckEditDateIzgot.Name = "CheckEditDateIzgot";
            this.CheckEditDateIzgot.Properties.Caption = "Дата изготовления";
            this.CheckEditDateIzgot.Size = new System.Drawing.Size(127, 19);
            this.CheckEditDateIzgot.TabIndex = 7;
            this.CheckEditDateIzgot.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // CheckEditNPartii
            // 
            this.CheckEditNPartii.EditValue = true;
            this.CheckEditNPartii.Location = new System.Drawing.Point(66, 144);
            this.CheckEditNPartii.Name = "CheckEditNPartii";
            this.CheckEditNPartii.Properties.Caption = "Номер партии";
            this.CheckEditNPartii.Size = new System.Drawing.Size(112, 19);
            this.CheckEditNPartii.TabIndex = 6;
            this.CheckEditNPartii.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // CheckEditClass
            // 
            this.CheckEditClass.EditValue = true;
            this.CheckEditClass.Location = new System.Drawing.Point(66, 119);
            this.CheckEditClass.Name = "CheckEditClass";
            this.CheckEditClass.Properties.Caption = "Классификация";
            this.CheckEditClass.Size = new System.Drawing.Size(112, 19);
            this.CheckEditClass.TabIndex = 5;
            this.CheckEditClass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // CheckEditName
            // 
            this.CheckEditName.EditValue = true;
            this.CheckEditName.Location = new System.Drawing.Point(66, 94);
            this.CheckEditName.Name = "CheckEditName";
            this.CheckEditName.Properties.Caption = "Наименование";
            this.CheckEditName.Size = new System.Drawing.Size(112, 19);
            this.CheckEditName.TabIndex = 4;
            this.CheckEditName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.СheckEditSearchByDate_KeyDown);
            // 
            // ReactivUchetFiltrForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 253);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "ReactivUchetFiltrForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Фильтр...";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ReactivUchetFiltrForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivUchetFiltrForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.СheckEditSearchByDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditOstatok.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRashod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrihod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditOt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateIzgot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNPartii.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditClass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit CheckEditName;
        private DevExpress.XtraEditors.CheckEdit CheckEditOstatok;
        private DevExpress.XtraEditors.CheckEdit CheckEditRashod;
        private DevExpress.XtraEditors.CheckEdit CheckEditPrihod;
        private DevExpress.XtraEditors.CheckEdit CheckEditDo;
        private DevExpress.XtraEditors.CheckEdit CheckEditOt;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateIzgot;
        private DevExpress.XtraEditors.CheckEdit CheckEditNPartii;
        private DevExpress.XtraEditors.CheckEdit CheckEditClass;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.SimpleButton CancelClickButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker DateTimeDo;
        private System.Windows.Forms.DateTimePicker DateTimeOt;
        private DevExpress.XtraEditors.CheckEdit СheckEditSearchByDate;
    }
}