﻿namespace LIMS
{
    partial class AddChangeUserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddChangeUserForm));
            this.MainPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.AlertsCheckBox = new System.Windows.Forms.CheckBox();
            this.DeveloperCheckBox = new System.Windows.Forms.CheckBox();
            this.DisabledCheckBox = new System.Windows.Forms.CheckBox();
            this.DeleteButton = new DevExpress.XtraEditors.SimpleButton();
            this.UserName = new System.Windows.Forms.ComboBox();
            this.SaveButton = new DevExpress.XtraEditors.SimpleButton();
            this.AdminCheckBox = new System.Windows.Forms.CheckBox();
            this.TipComboBox = new System.Windows.Forms.ComboBox();
            this.Password = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GroupControl = new DevExpress.XtraEditors.GroupControl();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl14 = new DevExpress.XtraEditors.GroupControl();
            this.label3 = new System.Windows.Forms.Label();
            this.StabilnostiCheckBox = new System.Windows.Forms.CheckBox();
            this.StabilnostiEditCheckBox = new System.Windows.Forms.CheckBox();
            this.StabilnostiDiagramsCheckBox = new System.Windows.Forms.CheckBox();
            this.StabilnostiCloseCheckBox = new System.Windows.Forms.CheckBox();
            this.StabilnostiKKSHCheckBox = new System.Windows.Forms.CheckBox();
            this.StabilnostiPrintCheckBox = new System.Windows.Forms.CheckBox();
            this.StabilnostiDeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.groupControl15 = new DevExpress.XtraEditors.GroupControl();
            this.OperativniiCloseCheckBox = new System.Windows.Forms.CheckBox();
            this.OperativniiPrintCheckBox = new System.Windows.Forms.CheckBox();
            this.OperativniiCheckBox = new System.Windows.Forms.CheckBox();
            this.OperativniiDeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.OperativniiEditCheckBox = new System.Windows.Forms.CheckBox();
            this.groupControl16 = new DevExpress.XtraEditors.GroupControl();
            this.ProbiChangeDataCheckBox = new System.Windows.Forms.CheckBox();
            this.ProbiCheckBox = new System.Windows.Forms.CheckBox();
            this.ProbiAddCheckBox = new System.Windows.Forms.CheckBox();
            this.ProbiEditCheckBox = new System.Windows.Forms.CheckBox();
            this.ProbiDeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.ProbiPrintCheckBox = new System.Windows.Forms.CheckBox();
            this.groupControl17 = new DevExpress.XtraEditors.GroupControl();
            this.AnalizCheckBox = new System.Windows.Forms.CheckBox();
            this.AnalizAddCheckBox = new System.Windows.Forms.CheckBox();
            this.AnalizEditCheckBox = new System.Windows.Forms.CheckBox();
            this.AnalizDeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.VLKCheckBox = new System.Windows.Forms.CheckBox();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.VOCheckBox = new System.Windows.Forms.CheckBox();
            this.VOAddCheckBox = new System.Windows.Forms.CheckBox();
            this.VOArhivCheckBox = new System.Windows.Forms.CheckBox();
            this.VOEditCheckBox = new System.Windows.Forms.CheckBox();
            this.VODeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.VOPrintCheckBox = new System.Windows.Forms.CheckBox();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.IOCheckBox = new System.Windows.Forms.CheckBox();
            this.IOAddCheckBox = new System.Windows.Forms.CheckBox();
            this.IOArhivCheckBox = new System.Windows.Forms.CheckBox();
            this.IOEditCheckBox = new System.Windows.Forms.CheckBox();
            this.IODeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.IOPrintCheckBox = new System.Windows.Forms.CheckBox();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.SICheckBox = new System.Windows.Forms.CheckBox();
            this.SIAddCheckBox = new System.Windows.Forms.CheckBox();
            this.SIArhivCheckBox = new System.Windows.Forms.CheckBox();
            this.SIEditCheckBox = new System.Windows.Forms.CheckBox();
            this.SIDeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.SIPrintCheckBox = new System.Windows.Forms.CheckBox();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.NTDCheckBox = new System.Windows.Forms.CheckBox();
            this.NTDAddCheckBox = new System.Windows.Forms.CheckBox();
            this.NTDArhivCheckBox = new System.Windows.Forms.CheckBox();
            this.NTDEditCheckBox = new System.Windows.Forms.CheckBox();
            this.NTDDelCheckBox = new System.Windows.Forms.CheckBox();
            this.NTDPrintCheckBox = new System.Windows.Forms.CheckBox();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.PersonalCheckBox = new System.Windows.Forms.CheckBox();
            this.PersonalAddCheckBox = new System.Windows.Forms.CheckBox();
            this.PersonalUvolnenieCheckBox = new System.Windows.Forms.CheckBox();
            this.PersonalEditCheckBox = new System.Windows.Forms.CheckBox();
            this.PersonalDeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.PersonalPrintCheckBox = new System.Windows.Forms.CheckBox();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.ReactivCheckBox = new System.Windows.Forms.CheckBox();
            this.ReactivAddCheckBox = new System.Windows.Forms.CheckBox();
            this.ReactivEditCheckBox = new System.Windows.Forms.CheckBox();
            this.ReactivRashodPrihodCheckBox = new System.Windows.Forms.CheckBox();
            this.ReactivUchetCheckBox = new System.Windows.Forms.CheckBox();
            this.ReactivVhodnoiKontrolCheckBox = new System.Windows.Forms.CheckBox();
            this.ReactivProverkaPrigodnostiCheckBox = new System.Windows.Forms.CheckBox();
            this.ReactivPrintCheckBox = new System.Windows.Forms.CheckBox();
            this.ReactivDeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.ReactivSpisatCheckBox = new System.Windows.Forms.CheckBox();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.UseDocProtectCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.MainPanelControl)).BeginInit();
            this.MainPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl)).BeginInit();
            this.GroupControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).BeginInit();
            this.groupControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).BeginInit();
            this.groupControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl16)).BeginInit();
            this.groupControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl17)).BeginInit();
            this.groupControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.xtraScrollableControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanelControl
            // 
            this.MainPanelControl.AutoSize = true;
            this.MainPanelControl.Controls.Add(this.UseDocProtectCheckBox);
            this.MainPanelControl.Controls.Add(this.AlertsCheckBox);
            this.MainPanelControl.Controls.Add(this.DeveloperCheckBox);
            this.MainPanelControl.Controls.Add(this.DisabledCheckBox);
            this.MainPanelControl.Controls.Add(this.DeleteButton);
            this.MainPanelControl.Controls.Add(this.UserName);
            this.MainPanelControl.Controls.Add(this.SaveButton);
            this.MainPanelControl.Controls.Add(this.AdminCheckBox);
            this.MainPanelControl.Controls.Add(this.TipComboBox);
            this.MainPanelControl.Controls.Add(this.Password);
            this.MainPanelControl.Controls.Add(this.label2);
            this.MainPanelControl.Controls.Add(this.label1);
            this.MainPanelControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.MainPanelControl.Location = new System.Drawing.Point(0, 0);
            this.MainPanelControl.Name = "MainPanelControl";
            this.MainPanelControl.Size = new System.Drawing.Size(1120, 192);
            this.MainPanelControl.TabIndex = 0;
            // 
            // AlertsCheckBox
            // 
            this.AlertsCheckBox.AutoSize = true;
            this.AlertsCheckBox.Checked = true;
            this.AlertsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AlertsCheckBox.Location = new System.Drawing.Point(101, 140);
            this.AlertsCheckBox.Name = "AlertsCheckBox";
            this.AlertsCheckBox.Size = new System.Drawing.Size(147, 17);
            this.AlertsCheckBox.TabIndex = 6;
            this.AlertsCheckBox.Text = "Включить уведомления";
            this.AlertsCheckBox.UseVisualStyleBackColor = true;
            // 
            // DeveloperCheckBox
            // 
            this.DeveloperCheckBox.AutoSize = true;
            this.DeveloperCheckBox.Location = new System.Drawing.Point(314, 14);
            this.DeveloperCheckBox.Name = "DeveloperCheckBox";
            this.DeveloperCheckBox.Size = new System.Drawing.Size(91, 17);
            this.DeveloperCheckBox.TabIndex = 14;
            this.DeveloperCheckBox.Text = "Разработчик";
            this.DeveloperCheckBox.UseVisualStyleBackColor = true;
            this.DeveloperCheckBox.Visible = false;
            // 
            // DisabledCheckBox
            // 
            this.DisabledCheckBox.AutoSize = true;
            this.DisabledCheckBox.Location = new System.Drawing.Point(101, 117);
            this.DisabledCheckBox.Name = "DisabledCheckBox";
            this.DisabledCheckBox.Size = new System.Drawing.Size(170, 17);
            this.DisabledCheckBox.TabIndex = 5;
            this.DisabledCheckBox.Text = "Отключить учетную запись";
            this.DisabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(196, 163);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(83, 22);
            this.DeleteButton.TabIndex = 8;
            this.DeleteButton.Text = "Удалить";
            this.DeleteButton.Visible = false;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // UserName
            // 
            this.UserName.FormattingEnabled = true;
            this.UserName.Location = new System.Drawing.Point(101, 39);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(178, 21);
            this.UserName.TabIndex = 2;
            this.UserName.SelectedIndexChanged += new System.EventHandler(this.UserName_SelectedIndexChanged);
            // 
            // SaveButton
            // 
            this.SaveButton.AutoSize = true;
            this.SaveButton.Location = new System.Drawing.Point(101, 163);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(64, 22);
            this.SaveButton.TabIndex = 7;
            this.SaveButton.Text = "Сохранить";
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // AdminCheckBox
            // 
            this.AdminCheckBox.AutoSize = true;
            this.AdminCheckBox.Location = new System.Drawing.Point(101, 94);
            this.AdminCheckBox.Name = "AdminCheckBox";
            this.AdminCheckBox.Size = new System.Drawing.Size(105, 17);
            this.AdminCheckBox.TabIndex = 4;
            this.AdminCheckBox.Text = "Администратор";
            this.AdminCheckBox.UseVisualStyleBackColor = true;
            // 
            // TipComboBox
            // 
            this.TipComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.TipComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TipComboBox.FormattingEnabled = true;
            this.TipComboBox.Items.AddRange(new object[] {
            "Добавить",
            "Изменить"});
            this.TipComboBox.Location = new System.Drawing.Point(101, 12);
            this.TipComboBox.Name = "TipComboBox";
            this.TipComboBox.Size = new System.Drawing.Size(178, 21);
            this.TipComboBox.TabIndex = 1;
            this.TipComboBox.SelectedIndexChanged += new System.EventHandler(this.TipComboBox_SelectedIndexChanged);
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(101, 67);
            this.Password.Name = "Password";
            this.Password.PasswordChar = '*';
            this.Password.Size = new System.Drawing.Size(178, 21);
            this.Password.TabIndex = 3;
            this.Password.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Password_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Пароль:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Пользователь:";
            // 
            // GroupControl
            // 
            this.GroupControl.AutoSize = true;
            this.GroupControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GroupControl.Controls.Add(this.groupControl7);
            this.GroupControl.Controls.Add(this.groupControl6);
            this.GroupControl.Controls.Add(this.groupControl5);
            this.GroupControl.Controls.Add(this.groupControl4);
            this.GroupControl.Controls.Add(this.groupControl3);
            this.GroupControl.Controls.Add(this.groupControl2);
            this.GroupControl.Controls.Add(this.groupControl1);
            this.GroupControl.Dock = System.Windows.Forms.DockStyle.Left;
            this.GroupControl.Location = new System.Drawing.Point(0, 0);
            this.GroupControl.Name = "GroupControl";
            this.GroupControl.Size = new System.Drawing.Size(1549, 384);
            this.GroupControl.TabIndex = 8;
            this.GroupControl.Text = "Права";
            // 
            // groupControl7
            // 
            this.groupControl7.AutoSize = true;
            this.groupControl7.Controls.Add(this.groupControl9);
            this.groupControl7.Controls.Add(this.VLKCheckBox);
            this.groupControl7.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl7.Location = new System.Drawing.Point(815, 20);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.ShowCaption = false;
            this.groupControl7.Size = new System.Drawing.Size(732, 362);
            this.groupControl7.TabIndex = 22;
            this.groupControl7.Text = "groupControl7";
            // 
            // groupControl9
            // 
            this.groupControl9.AutoSize = true;
            this.groupControl9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl9.Controls.Add(this.groupControl14);
            this.groupControl9.Controls.Add(this.groupControl15);
            this.groupControl9.Controls.Add(this.groupControl16);
            this.groupControl9.Controls.Add(this.groupControl17);
            this.groupControl9.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl9.Location = new System.Drawing.Point(2, 19);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.ShowCaption = false;
            this.groupControl9.Size = new System.Drawing.Size(728, 341);
            this.groupControl9.TabIndex = 24;
            // 
            // groupControl14
            // 
            this.groupControl14.AutoSize = true;
            this.groupControl14.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl14.Controls.Add(this.label3);
            this.groupControl14.Controls.Add(this.StabilnostiCheckBox);
            this.groupControl14.Controls.Add(this.StabilnostiEditCheckBox);
            this.groupControl14.Controls.Add(this.StabilnostiDiagramsCheckBox);
            this.groupControl14.Controls.Add(this.StabilnostiCloseCheckBox);
            this.groupControl14.Controls.Add(this.StabilnostiKKSHCheckBox);
            this.groupControl14.Controls.Add(this.StabilnostiPrintCheckBox);
            this.groupControl14.Controls.Add(this.StabilnostiDeleteCheckBox);
            this.groupControl14.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl14.Location = new System.Drawing.Point(460, 2);
            this.groupControl14.Name = "groupControl14";
            this.groupControl14.ShowCaption = false;
            this.groupControl14.Size = new System.Drawing.Size(266, 337);
            this.groupControl14.TabIndex = 22;
            this.groupControl14.Text = "groupControl14";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(209, 13);
            this.label3.TabIndex = 70;
            this.label3.Text = "и внутрилабораторной прецизионности";
            // 
            // StabilnostiCheckBox
            // 
            this.StabilnostiCheckBox.AutoSize = true;
            this.StabilnostiCheckBox.Checked = true;
            this.StabilnostiCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.StabilnostiCheckBox.Location = new System.Drawing.Point(5, 5);
            this.StabilnostiCheckBox.Name = "StabilnostiCheckBox";
            this.StabilnostiCheckBox.Size = new System.Drawing.Size(148, 17);
            this.StabilnostiCheckBox.TabIndex = 65;
            this.StabilnostiCheckBox.Text = "Контроль стабильности";
            this.StabilnostiCheckBox.UseVisualStyleBackColor = true;
            this.StabilnostiCheckBox.CheckedChanged += new System.EventHandler(this.StabilnostiCheckBox_CheckedChanged);
            // 
            // StabilnostiEditCheckBox
            // 
            this.StabilnostiEditCheckBox.AutoSize = true;
            this.StabilnostiEditCheckBox.Checked = true;
            this.StabilnostiEditCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.StabilnostiEditCheckBox.Location = new System.Drawing.Point(17, 28);
            this.StabilnostiEditCheckBox.Name = "StabilnostiEditCheckBox";
            this.StabilnostiEditCheckBox.Size = new System.Drawing.Size(80, 17);
            this.StabilnostiEditCheckBox.TabIndex = 66;
            this.StabilnostiEditCheckBox.Text = "Изменение";
            this.StabilnostiEditCheckBox.UseVisualStyleBackColor = true;
            // 
            // StabilnostiDiagramsCheckBox
            // 
            this.StabilnostiDiagramsCheckBox.AutoSize = true;
            this.StabilnostiDiagramsCheckBox.Checked = true;
            this.StabilnostiDiagramsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.StabilnostiDiagramsCheckBox.Location = new System.Drawing.Point(17, 97);
            this.StabilnostiDiagramsCheckBox.Name = "StabilnostiDiagramsCheckBox";
            this.StabilnostiDiagramsCheckBox.Size = new System.Drawing.Size(242, 17);
            this.StabilnostiDiagramsCheckBox.TabIndex = 69;
            this.StabilnostiDiagramsCheckBox.Text = "Диаграммы погрешности, повторяемости  ";
            this.StabilnostiDiagramsCheckBox.UseVisualStyleBackColor = true;
            // 
            // StabilnostiCloseCheckBox
            // 
            this.StabilnostiCloseCheckBox.AutoSize = true;
            this.StabilnostiCloseCheckBox.Checked = true;
            this.StabilnostiCloseCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.StabilnostiCloseCheckBox.Location = new System.Drawing.Point(17, 51);
            this.StabilnostiCloseCheckBox.Name = "StabilnostiCloseCheckBox";
            this.StabilnostiCloseCheckBox.Size = new System.Drawing.Size(76, 17);
            this.StabilnostiCloseCheckBox.TabIndex = 67;
            this.StabilnostiCloseCheckBox.Text = "Закрытие";
            this.StabilnostiCloseCheckBox.UseVisualStyleBackColor = true;
            // 
            // StabilnostiKKSHCheckBox
            // 
            this.StabilnostiKKSHCheckBox.AutoSize = true;
            this.StabilnostiKKSHCheckBox.Checked = true;
            this.StabilnostiKKSHCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.StabilnostiKKSHCheckBox.Location = new System.Drawing.Point(17, 74);
            this.StabilnostiKKSHCheckBox.Name = "StabilnostiKKSHCheckBox";
            this.StabilnostiKKSHCheckBox.Size = new System.Drawing.Size(150, 17);
            this.StabilnostiKKSHCheckBox.TabIndex = 68;
            this.StabilnostiKKSHCheckBox.Text = "Протокол и оценка ККШ";
            this.StabilnostiKKSHCheckBox.UseVisualStyleBackColor = true;
            // 
            // StabilnostiPrintCheckBox
            // 
            this.StabilnostiPrintCheckBox.AutoSize = true;
            this.StabilnostiPrintCheckBox.Checked = true;
            this.StabilnostiPrintCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.StabilnostiPrintCheckBox.Location = new System.Drawing.Point(17, 134);
            this.StabilnostiPrintCheckBox.Name = "StabilnostiPrintCheckBox";
            this.StabilnostiPrintCheckBox.Size = new System.Drawing.Size(63, 17);
            this.StabilnostiPrintCheckBox.TabIndex = 71;
            this.StabilnostiPrintCheckBox.Text = "Печать";
            this.StabilnostiPrintCheckBox.UseVisualStyleBackColor = true;
            // 
            // StabilnostiDeleteCheckBox
            // 
            this.StabilnostiDeleteCheckBox.AutoSize = true;
            this.StabilnostiDeleteCheckBox.Checked = true;
            this.StabilnostiDeleteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.StabilnostiDeleteCheckBox.Location = new System.Drawing.Point(17, 157);
            this.StabilnostiDeleteCheckBox.Name = "StabilnostiDeleteCheckBox";
            this.StabilnostiDeleteCheckBox.Size = new System.Drawing.Size(76, 17);
            this.StabilnostiDeleteCheckBox.TabIndex = 72;
            this.StabilnostiDeleteCheckBox.Text = "Удаление";
            this.StabilnostiDeleteCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupControl15
            // 
            this.groupControl15.AutoSize = true;
            this.groupControl15.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl15.Controls.Add(this.OperativniiCloseCheckBox);
            this.groupControl15.Controls.Add(this.OperativniiPrintCheckBox);
            this.groupControl15.Controls.Add(this.OperativniiCheckBox);
            this.groupControl15.Controls.Add(this.OperativniiDeleteCheckBox);
            this.groupControl15.Controls.Add(this.OperativniiEditCheckBox);
            this.groupControl15.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl15.Location = new System.Drawing.Point(301, 2);
            this.groupControl15.Name = "groupControl15";
            this.groupControl15.ShowCaption = false;
            this.groupControl15.Size = new System.Drawing.Size(159, 337);
            this.groupControl15.TabIndex = 22;
            this.groupControl15.Text = "groupControl15";
            // 
            // OperativniiCloseCheckBox
            // 
            this.OperativniiCloseCheckBox.AutoSize = true;
            this.OperativniiCloseCheckBox.Checked = true;
            this.OperativniiCloseCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OperativniiCloseCheckBox.Location = new System.Drawing.Point(17, 51);
            this.OperativniiCloseCheckBox.Name = "OperativniiCloseCheckBox";
            this.OperativniiCloseCheckBox.Size = new System.Drawing.Size(76, 17);
            this.OperativniiCloseCheckBox.TabIndex = 62;
            this.OperativniiCloseCheckBox.Text = "Закрытие";
            this.OperativniiCloseCheckBox.UseVisualStyleBackColor = true;
            // 
            // OperativniiPrintCheckBox
            // 
            this.OperativniiPrintCheckBox.AutoSize = true;
            this.OperativniiPrintCheckBox.Checked = true;
            this.OperativniiPrintCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OperativniiPrintCheckBox.Location = new System.Drawing.Point(17, 74);
            this.OperativniiPrintCheckBox.Name = "OperativniiPrintCheckBox";
            this.OperativniiPrintCheckBox.Size = new System.Drawing.Size(63, 17);
            this.OperativniiPrintCheckBox.TabIndex = 63;
            this.OperativniiPrintCheckBox.Text = "Печать";
            this.OperativniiPrintCheckBox.UseVisualStyleBackColor = true;
            // 
            // OperativniiCheckBox
            // 
            this.OperativniiCheckBox.AutoSize = true;
            this.OperativniiCheckBox.Checked = true;
            this.OperativniiCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OperativniiCheckBox.Location = new System.Drawing.Point(5, 5);
            this.OperativniiCheckBox.Name = "OperativniiCheckBox";
            this.OperativniiCheckBox.Size = new System.Drawing.Size(147, 17);
            this.OperativniiCheckBox.TabIndex = 60;
            this.OperativniiCheckBox.Text = "Оперативный контроль";
            this.OperativniiCheckBox.UseVisualStyleBackColor = true;
            this.OperativniiCheckBox.CheckedChanged += new System.EventHandler(this.OperativniiCheckBox_CheckedChanged);
            // 
            // OperativniiDeleteCheckBox
            // 
            this.OperativniiDeleteCheckBox.AutoSize = true;
            this.OperativniiDeleteCheckBox.Checked = true;
            this.OperativniiDeleteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OperativniiDeleteCheckBox.Location = new System.Drawing.Point(17, 97);
            this.OperativniiDeleteCheckBox.Name = "OperativniiDeleteCheckBox";
            this.OperativniiDeleteCheckBox.Size = new System.Drawing.Size(76, 17);
            this.OperativniiDeleteCheckBox.TabIndex = 64;
            this.OperativniiDeleteCheckBox.Text = "Удаление";
            this.OperativniiDeleteCheckBox.UseVisualStyleBackColor = true;
            // 
            // OperativniiEditCheckBox
            // 
            this.OperativniiEditCheckBox.AutoSize = true;
            this.OperativniiEditCheckBox.Checked = true;
            this.OperativniiEditCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OperativniiEditCheckBox.Location = new System.Drawing.Point(17, 28);
            this.OperativniiEditCheckBox.Name = "OperativniiEditCheckBox";
            this.OperativniiEditCheckBox.Size = new System.Drawing.Size(80, 17);
            this.OperativniiEditCheckBox.TabIndex = 61;
            this.OperativniiEditCheckBox.Text = "Изменение";
            this.OperativniiEditCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupControl16
            // 
            this.groupControl16.AutoSize = true;
            this.groupControl16.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl16.Controls.Add(this.ProbiChangeDataCheckBox);
            this.groupControl16.Controls.Add(this.ProbiCheckBox);
            this.groupControl16.Controls.Add(this.ProbiAddCheckBox);
            this.groupControl16.Controls.Add(this.ProbiEditCheckBox);
            this.groupControl16.Controls.Add(this.ProbiDeleteCheckBox);
            this.groupControl16.Controls.Add(this.ProbiPrintCheckBox);
            this.groupControl16.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl16.Location = new System.Drawing.Point(114, 2);
            this.groupControl16.Name = "groupControl16";
            this.groupControl16.ShowCaption = false;
            this.groupControl16.Size = new System.Drawing.Size(187, 337);
            this.groupControl16.TabIndex = 21;
            this.groupControl16.Text = "groupControl16";
            // 
            // ProbiChangeDataCheckBox
            // 
            this.ProbiChangeDataCheckBox.AutoSize = true;
            this.ProbiChangeDataCheckBox.Checked = true;
            this.ProbiChangeDataCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ProbiChangeDataCheckBox.Location = new System.Drawing.Point(17, 74);
            this.ProbiChangeDataCheckBox.Name = "ProbiChangeDataCheckBox";
            this.ProbiChangeDataCheckBox.Size = new System.Drawing.Size(163, 17);
            this.ProbiChangeDataCheckBox.TabIndex = 57;
            this.ProbiChangeDataCheckBox.Text = "Ввод результатов анализа";
            this.ProbiChangeDataCheckBox.UseVisualStyleBackColor = true;
            // 
            // ProbiCheckBox
            // 
            this.ProbiCheckBox.AutoSize = true;
            this.ProbiCheckBox.Checked = true;
            this.ProbiCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ProbiCheckBox.Location = new System.Drawing.Point(5, 5);
            this.ProbiCheckBox.Name = "ProbiCheckBox";
            this.ProbiCheckBox.Size = new System.Drawing.Size(92, 17);
            this.ProbiCheckBox.TabIndex = 54;
            this.ProbiCheckBox.Text = "Шифрование";
            this.ProbiCheckBox.UseVisualStyleBackColor = true;
            this.ProbiCheckBox.CheckedChanged += new System.EventHandler(this.ProbiCheckBox_CheckedChanged);
            // 
            // ProbiAddCheckBox
            // 
            this.ProbiAddCheckBox.AutoSize = true;
            this.ProbiAddCheckBox.Checked = true;
            this.ProbiAddCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ProbiAddCheckBox.Location = new System.Drawing.Point(17, 28);
            this.ProbiAddCheckBox.Name = "ProbiAddCheckBox";
            this.ProbiAddCheckBox.Size = new System.Drawing.Size(88, 17);
            this.ProbiAddCheckBox.TabIndex = 55;
            this.ProbiAddCheckBox.Text = "Добавление";
            this.ProbiAddCheckBox.UseVisualStyleBackColor = true;
            // 
            // ProbiEditCheckBox
            // 
            this.ProbiEditCheckBox.AutoSize = true;
            this.ProbiEditCheckBox.Checked = true;
            this.ProbiEditCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ProbiEditCheckBox.Location = new System.Drawing.Point(17, 51);
            this.ProbiEditCheckBox.Name = "ProbiEditCheckBox";
            this.ProbiEditCheckBox.Size = new System.Drawing.Size(80, 17);
            this.ProbiEditCheckBox.TabIndex = 56;
            this.ProbiEditCheckBox.Text = "Изменение";
            this.ProbiEditCheckBox.UseVisualStyleBackColor = true;
            // 
            // ProbiDeleteCheckBox
            // 
            this.ProbiDeleteCheckBox.AutoSize = true;
            this.ProbiDeleteCheckBox.Checked = true;
            this.ProbiDeleteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ProbiDeleteCheckBox.Location = new System.Drawing.Point(17, 120);
            this.ProbiDeleteCheckBox.Name = "ProbiDeleteCheckBox";
            this.ProbiDeleteCheckBox.Size = new System.Drawing.Size(76, 17);
            this.ProbiDeleteCheckBox.TabIndex = 59;
            this.ProbiDeleteCheckBox.Text = "Удаление";
            this.ProbiDeleteCheckBox.UseVisualStyleBackColor = true;
            // 
            // ProbiPrintCheckBox
            // 
            this.ProbiPrintCheckBox.AutoSize = true;
            this.ProbiPrintCheckBox.Checked = true;
            this.ProbiPrintCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ProbiPrintCheckBox.Location = new System.Drawing.Point(17, 97);
            this.ProbiPrintCheckBox.Name = "ProbiPrintCheckBox";
            this.ProbiPrintCheckBox.Size = new System.Drawing.Size(63, 17);
            this.ProbiPrintCheckBox.TabIndex = 58;
            this.ProbiPrintCheckBox.Text = "Печать";
            this.ProbiPrintCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupControl17
            // 
            this.groupControl17.AutoSize = true;
            this.groupControl17.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl17.Controls.Add(this.AnalizCheckBox);
            this.groupControl17.Controls.Add(this.AnalizAddCheckBox);
            this.groupControl17.Controls.Add(this.AnalizEditCheckBox);
            this.groupControl17.Controls.Add(this.AnalizDeleteCheckBox);
            this.groupControl17.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl17.Location = new System.Drawing.Point(2, 2);
            this.groupControl17.Name = "groupControl17";
            this.groupControl17.ShowCaption = false;
            this.groupControl17.Size = new System.Drawing.Size(112, 337);
            this.groupControl17.TabIndex = 20;
            this.groupControl17.Text = "groupControl17";
            // 
            // AnalizCheckBox
            // 
            this.AnalizCheckBox.AutoSize = true;
            this.AnalizCheckBox.Checked = true;
            this.AnalizCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AnalizCheckBox.Location = new System.Drawing.Point(5, 5);
            this.AnalizCheckBox.Name = "AnalizCheckBox";
            this.AnalizCheckBox.Size = new System.Drawing.Size(70, 17);
            this.AnalizCheckBox.TabIndex = 50;
            this.AnalizCheckBox.Text = "Анализы";
            this.AnalizCheckBox.UseVisualStyleBackColor = true;
            this.AnalizCheckBox.CheckedChanged += new System.EventHandler(this.AnalizCheckBox_CheckedChanged);
            // 
            // AnalizAddCheckBox
            // 
            this.AnalizAddCheckBox.AutoSize = true;
            this.AnalizAddCheckBox.Checked = true;
            this.AnalizAddCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AnalizAddCheckBox.Location = new System.Drawing.Point(17, 28);
            this.AnalizAddCheckBox.Name = "AnalizAddCheckBox";
            this.AnalizAddCheckBox.Size = new System.Drawing.Size(88, 17);
            this.AnalizAddCheckBox.TabIndex = 51;
            this.AnalizAddCheckBox.Text = "Добавление";
            this.AnalizAddCheckBox.UseVisualStyleBackColor = true;
            // 
            // AnalizEditCheckBox
            // 
            this.AnalizEditCheckBox.AutoSize = true;
            this.AnalizEditCheckBox.Checked = true;
            this.AnalizEditCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AnalizEditCheckBox.Location = new System.Drawing.Point(17, 51);
            this.AnalizEditCheckBox.Name = "AnalizEditCheckBox";
            this.AnalizEditCheckBox.Size = new System.Drawing.Size(80, 17);
            this.AnalizEditCheckBox.TabIndex = 52;
            this.AnalizEditCheckBox.Text = "Изменение";
            this.AnalizEditCheckBox.UseVisualStyleBackColor = true;
            // 
            // AnalizDeleteCheckBox
            // 
            this.AnalizDeleteCheckBox.AutoSize = true;
            this.AnalizDeleteCheckBox.Checked = true;
            this.AnalizDeleteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AnalizDeleteCheckBox.Location = new System.Drawing.Point(17, 74);
            this.AnalizDeleteCheckBox.Name = "AnalizDeleteCheckBox";
            this.AnalizDeleteCheckBox.Size = new System.Drawing.Size(76, 17);
            this.AnalizDeleteCheckBox.TabIndex = 53;
            this.AnalizDeleteCheckBox.Text = "Удаление";
            this.AnalizDeleteCheckBox.UseVisualStyleBackColor = true;
            // 
            // VLKCheckBox
            // 
            this.VLKCheckBox.AutoSize = true;
            this.VLKCheckBox.Checked = true;
            this.VLKCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.VLKCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.VLKCheckBox.Location = new System.Drawing.Point(2, 2);
            this.VLKCheckBox.Name = "VLKCheckBox";
            this.VLKCheckBox.Size = new System.Drawing.Size(728, 17);
            this.VLKCheckBox.TabIndex = 49;
            this.VLKCheckBox.Text = "ВЛК";
            this.VLKCheckBox.UseVisualStyleBackColor = true;
            this.VLKCheckBox.CheckedChanged += new System.EventHandler(this.VLKCheckBox_CheckedChanged);
            // 
            // groupControl6
            // 
            this.groupControl6.AutoSize = true;
            this.groupControl6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl6.Controls.Add(this.VOCheckBox);
            this.groupControl6.Controls.Add(this.VOAddCheckBox);
            this.groupControl6.Controls.Add(this.VOArhivCheckBox);
            this.groupControl6.Controls.Add(this.VOEditCheckBox);
            this.groupControl6.Controls.Add(this.VODeleteCheckBox);
            this.groupControl6.Controls.Add(this.VOPrintCheckBox);
            this.groupControl6.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl6.Location = new System.Drawing.Point(703, 20);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.ShowCaption = false;
            this.groupControl6.Size = new System.Drawing.Size(112, 362);
            this.groupControl6.TabIndex = 22;
            this.groupControl6.Text = "groupControl6";
            // 
            // VOCheckBox
            // 
            this.VOCheckBox.AutoSize = true;
            this.VOCheckBox.Checked = true;
            this.VOCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.VOCheckBox.Location = new System.Drawing.Point(5, 5);
            this.VOCheckBox.Name = "VOCheckBox";
            this.VOCheckBox.Size = new System.Drawing.Size(40, 17);
            this.VOCheckBox.TabIndex = 43;
            this.VOCheckBox.Text = "ВО";
            this.VOCheckBox.UseVisualStyleBackColor = true;
            this.VOCheckBox.CheckedChanged += new System.EventHandler(this.VOCheckBox_CheckedChanged);
            // 
            // VOAddCheckBox
            // 
            this.VOAddCheckBox.AutoSize = true;
            this.VOAddCheckBox.Checked = true;
            this.VOAddCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.VOAddCheckBox.Location = new System.Drawing.Point(17, 28);
            this.VOAddCheckBox.Name = "VOAddCheckBox";
            this.VOAddCheckBox.Size = new System.Drawing.Size(88, 17);
            this.VOAddCheckBox.TabIndex = 44;
            this.VOAddCheckBox.Text = "Добавление";
            this.VOAddCheckBox.UseVisualStyleBackColor = true;
            // 
            // VOArhivCheckBox
            // 
            this.VOArhivCheckBox.AutoSize = true;
            this.VOArhivCheckBox.Checked = true;
            this.VOArhivCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.VOArhivCheckBox.Location = new System.Drawing.Point(17, 97);
            this.VOArhivCheckBox.Name = "VOArhivCheckBox";
            this.VOArhivCheckBox.Size = new System.Drawing.Size(57, 17);
            this.VOArhivCheckBox.TabIndex = 47;
            this.VOArhivCheckBox.Text = "Архив";
            this.VOArhivCheckBox.UseVisualStyleBackColor = true;
            // 
            // VOEditCheckBox
            // 
            this.VOEditCheckBox.AutoSize = true;
            this.VOEditCheckBox.Checked = true;
            this.VOEditCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.VOEditCheckBox.Location = new System.Drawing.Point(17, 51);
            this.VOEditCheckBox.Name = "VOEditCheckBox";
            this.VOEditCheckBox.Size = new System.Drawing.Size(80, 17);
            this.VOEditCheckBox.TabIndex = 45;
            this.VOEditCheckBox.Text = "Изменение";
            this.VOEditCheckBox.UseVisualStyleBackColor = true;
            // 
            // VODeleteCheckBox
            // 
            this.VODeleteCheckBox.AutoSize = true;
            this.VODeleteCheckBox.Checked = true;
            this.VODeleteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.VODeleteCheckBox.Location = new System.Drawing.Point(17, 120);
            this.VODeleteCheckBox.Name = "VODeleteCheckBox";
            this.VODeleteCheckBox.Size = new System.Drawing.Size(76, 17);
            this.VODeleteCheckBox.TabIndex = 48;
            this.VODeleteCheckBox.Text = "Удаление";
            this.VODeleteCheckBox.UseVisualStyleBackColor = true;
            // 
            // VOPrintCheckBox
            // 
            this.VOPrintCheckBox.AutoSize = true;
            this.VOPrintCheckBox.Checked = true;
            this.VOPrintCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.VOPrintCheckBox.Location = new System.Drawing.Point(17, 74);
            this.VOPrintCheckBox.Name = "VOPrintCheckBox";
            this.VOPrintCheckBox.Size = new System.Drawing.Size(63, 17);
            this.VOPrintCheckBox.TabIndex = 46;
            this.VOPrintCheckBox.Text = "Печать";
            this.VOPrintCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupControl5
            // 
            this.groupControl5.AutoSize = true;
            this.groupControl5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl5.Controls.Add(this.IOCheckBox);
            this.groupControl5.Controls.Add(this.IOAddCheckBox);
            this.groupControl5.Controls.Add(this.IOArhivCheckBox);
            this.groupControl5.Controls.Add(this.IOEditCheckBox);
            this.groupControl5.Controls.Add(this.IODeleteCheckBox);
            this.groupControl5.Controls.Add(this.IOPrintCheckBox);
            this.groupControl5.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl5.Location = new System.Drawing.Point(591, 20);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.ShowCaption = false;
            this.groupControl5.Size = new System.Drawing.Size(112, 362);
            this.groupControl5.TabIndex = 22;
            this.groupControl5.Text = "groupControl5";
            // 
            // IOCheckBox
            // 
            this.IOCheckBox.AutoSize = true;
            this.IOCheckBox.Checked = true;
            this.IOCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IOCheckBox.Location = new System.Drawing.Point(5, 5);
            this.IOCheckBox.Name = "IOCheckBox";
            this.IOCheckBox.Size = new System.Drawing.Size(41, 17);
            this.IOCheckBox.TabIndex = 37;
            this.IOCheckBox.Text = "ИО";
            this.IOCheckBox.UseVisualStyleBackColor = true;
            this.IOCheckBox.CheckedChanged += new System.EventHandler(this.IOCheckBox_CheckedChanged);
            // 
            // IOAddCheckBox
            // 
            this.IOAddCheckBox.AutoSize = true;
            this.IOAddCheckBox.Checked = true;
            this.IOAddCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IOAddCheckBox.Location = new System.Drawing.Point(17, 28);
            this.IOAddCheckBox.Name = "IOAddCheckBox";
            this.IOAddCheckBox.Size = new System.Drawing.Size(88, 17);
            this.IOAddCheckBox.TabIndex = 38;
            this.IOAddCheckBox.Text = "Добавление";
            this.IOAddCheckBox.UseVisualStyleBackColor = true;
            // 
            // IOArhivCheckBox
            // 
            this.IOArhivCheckBox.AutoSize = true;
            this.IOArhivCheckBox.Checked = true;
            this.IOArhivCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IOArhivCheckBox.Location = new System.Drawing.Point(17, 97);
            this.IOArhivCheckBox.Name = "IOArhivCheckBox";
            this.IOArhivCheckBox.Size = new System.Drawing.Size(57, 17);
            this.IOArhivCheckBox.TabIndex = 41;
            this.IOArhivCheckBox.Text = "Архив";
            this.IOArhivCheckBox.UseVisualStyleBackColor = true;
            // 
            // IOEditCheckBox
            // 
            this.IOEditCheckBox.AutoSize = true;
            this.IOEditCheckBox.Checked = true;
            this.IOEditCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IOEditCheckBox.Location = new System.Drawing.Point(17, 51);
            this.IOEditCheckBox.Name = "IOEditCheckBox";
            this.IOEditCheckBox.Size = new System.Drawing.Size(80, 17);
            this.IOEditCheckBox.TabIndex = 39;
            this.IOEditCheckBox.Text = "Изменение";
            this.IOEditCheckBox.UseVisualStyleBackColor = true;
            // 
            // IODeleteCheckBox
            // 
            this.IODeleteCheckBox.AutoSize = true;
            this.IODeleteCheckBox.Checked = true;
            this.IODeleteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IODeleteCheckBox.Location = new System.Drawing.Point(17, 120);
            this.IODeleteCheckBox.Name = "IODeleteCheckBox";
            this.IODeleteCheckBox.Size = new System.Drawing.Size(76, 17);
            this.IODeleteCheckBox.TabIndex = 42;
            this.IODeleteCheckBox.Text = "Удаление";
            this.IODeleteCheckBox.UseVisualStyleBackColor = true;
            // 
            // IOPrintCheckBox
            // 
            this.IOPrintCheckBox.AutoSize = true;
            this.IOPrintCheckBox.Checked = true;
            this.IOPrintCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IOPrintCheckBox.Location = new System.Drawing.Point(17, 74);
            this.IOPrintCheckBox.Name = "IOPrintCheckBox";
            this.IOPrintCheckBox.Size = new System.Drawing.Size(63, 17);
            this.IOPrintCheckBox.TabIndex = 40;
            this.IOPrintCheckBox.Text = "Печать";
            this.IOPrintCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupControl4
            // 
            this.groupControl4.AutoSize = true;
            this.groupControl4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl4.Controls.Add(this.SICheckBox);
            this.groupControl4.Controls.Add(this.SIAddCheckBox);
            this.groupControl4.Controls.Add(this.SIArhivCheckBox);
            this.groupControl4.Controls.Add(this.SIEditCheckBox);
            this.groupControl4.Controls.Add(this.SIDeleteCheckBox);
            this.groupControl4.Controls.Add(this.SIPrintCheckBox);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl4.Location = new System.Drawing.Point(479, 20);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.ShowCaption = false;
            this.groupControl4.Size = new System.Drawing.Size(112, 362);
            this.groupControl4.TabIndex = 22;
            this.groupControl4.Text = "groupControl4";
            // 
            // SICheckBox
            // 
            this.SICheckBox.AutoSize = true;
            this.SICheckBox.Checked = true;
            this.SICheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SICheckBox.Location = new System.Drawing.Point(5, 5);
            this.SICheckBox.Name = "SICheckBox";
            this.SICheckBox.Size = new System.Drawing.Size(40, 17);
            this.SICheckBox.TabIndex = 31;
            this.SICheckBox.Text = "СИ";
            this.SICheckBox.UseVisualStyleBackColor = true;
            this.SICheckBox.CheckedChanged += new System.EventHandler(this.SICheckBox_CheckedChanged);
            // 
            // SIAddCheckBox
            // 
            this.SIAddCheckBox.AutoSize = true;
            this.SIAddCheckBox.Checked = true;
            this.SIAddCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SIAddCheckBox.Location = new System.Drawing.Point(17, 28);
            this.SIAddCheckBox.Name = "SIAddCheckBox";
            this.SIAddCheckBox.Size = new System.Drawing.Size(88, 17);
            this.SIAddCheckBox.TabIndex = 32;
            this.SIAddCheckBox.Text = "Добавление";
            this.SIAddCheckBox.UseVisualStyleBackColor = true;
            // 
            // SIArhivCheckBox
            // 
            this.SIArhivCheckBox.AutoSize = true;
            this.SIArhivCheckBox.Checked = true;
            this.SIArhivCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SIArhivCheckBox.Location = new System.Drawing.Point(17, 97);
            this.SIArhivCheckBox.Name = "SIArhivCheckBox";
            this.SIArhivCheckBox.Size = new System.Drawing.Size(57, 17);
            this.SIArhivCheckBox.TabIndex = 35;
            this.SIArhivCheckBox.Text = "Архив";
            this.SIArhivCheckBox.UseVisualStyleBackColor = true;
            // 
            // SIEditCheckBox
            // 
            this.SIEditCheckBox.AutoSize = true;
            this.SIEditCheckBox.Checked = true;
            this.SIEditCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SIEditCheckBox.Location = new System.Drawing.Point(17, 51);
            this.SIEditCheckBox.Name = "SIEditCheckBox";
            this.SIEditCheckBox.Size = new System.Drawing.Size(80, 17);
            this.SIEditCheckBox.TabIndex = 33;
            this.SIEditCheckBox.Text = "Изменение";
            this.SIEditCheckBox.UseVisualStyleBackColor = true;
            // 
            // SIDeleteCheckBox
            // 
            this.SIDeleteCheckBox.AutoSize = true;
            this.SIDeleteCheckBox.Checked = true;
            this.SIDeleteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SIDeleteCheckBox.Location = new System.Drawing.Point(17, 120);
            this.SIDeleteCheckBox.Name = "SIDeleteCheckBox";
            this.SIDeleteCheckBox.Size = new System.Drawing.Size(76, 17);
            this.SIDeleteCheckBox.TabIndex = 36;
            this.SIDeleteCheckBox.Text = "Удаление";
            this.SIDeleteCheckBox.UseVisualStyleBackColor = true;
            // 
            // SIPrintCheckBox
            // 
            this.SIPrintCheckBox.AutoSize = true;
            this.SIPrintCheckBox.Checked = true;
            this.SIPrintCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SIPrintCheckBox.Location = new System.Drawing.Point(17, 74);
            this.SIPrintCheckBox.Name = "SIPrintCheckBox";
            this.SIPrintCheckBox.Size = new System.Drawing.Size(63, 17);
            this.SIPrintCheckBox.TabIndex = 34;
            this.SIPrintCheckBox.Text = "Печать";
            this.SIPrintCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupControl3
            // 
            this.groupControl3.AutoSize = true;
            this.groupControl3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl3.Controls.Add(this.NTDCheckBox);
            this.groupControl3.Controls.Add(this.NTDAddCheckBox);
            this.groupControl3.Controls.Add(this.NTDArhivCheckBox);
            this.groupControl3.Controls.Add(this.NTDEditCheckBox);
            this.groupControl3.Controls.Add(this.NTDDelCheckBox);
            this.groupControl3.Controls.Add(this.NTDPrintCheckBox);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl3.Location = new System.Drawing.Point(367, 20);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.ShowCaption = false;
            this.groupControl3.Size = new System.Drawing.Size(112, 362);
            this.groupControl3.TabIndex = 22;
            this.groupControl3.Text = "groupControl3";
            // 
            // NTDCheckBox
            // 
            this.NTDCheckBox.AutoSize = true;
            this.NTDCheckBox.Checked = true;
            this.NTDCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.NTDCheckBox.Location = new System.Drawing.Point(5, 5);
            this.NTDCheckBox.Name = "NTDCheckBox";
            this.NTDCheckBox.Size = new System.Drawing.Size(47, 17);
            this.NTDCheckBox.TabIndex = 25;
            this.NTDCheckBox.Text = "НТД";
            this.NTDCheckBox.UseVisualStyleBackColor = true;
            this.NTDCheckBox.CheckedChanged += new System.EventHandler(this.NTDCheckBox_CheckedChanged);
            // 
            // NTDAddCheckBox
            // 
            this.NTDAddCheckBox.AutoSize = true;
            this.NTDAddCheckBox.Checked = true;
            this.NTDAddCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.NTDAddCheckBox.Location = new System.Drawing.Point(17, 28);
            this.NTDAddCheckBox.Name = "NTDAddCheckBox";
            this.NTDAddCheckBox.Size = new System.Drawing.Size(88, 17);
            this.NTDAddCheckBox.TabIndex = 26;
            this.NTDAddCheckBox.Text = "Добавление";
            this.NTDAddCheckBox.UseVisualStyleBackColor = true;
            // 
            // NTDArhivCheckBox
            // 
            this.NTDArhivCheckBox.AutoSize = true;
            this.NTDArhivCheckBox.Checked = true;
            this.NTDArhivCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.NTDArhivCheckBox.Location = new System.Drawing.Point(17, 97);
            this.NTDArhivCheckBox.Name = "NTDArhivCheckBox";
            this.NTDArhivCheckBox.Size = new System.Drawing.Size(57, 17);
            this.NTDArhivCheckBox.TabIndex = 29;
            this.NTDArhivCheckBox.Text = "Архив";
            this.NTDArhivCheckBox.UseVisualStyleBackColor = true;
            // 
            // NTDEditCheckBox
            // 
            this.NTDEditCheckBox.AutoSize = true;
            this.NTDEditCheckBox.Checked = true;
            this.NTDEditCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.NTDEditCheckBox.Location = new System.Drawing.Point(17, 51);
            this.NTDEditCheckBox.Name = "NTDEditCheckBox";
            this.NTDEditCheckBox.Size = new System.Drawing.Size(80, 17);
            this.NTDEditCheckBox.TabIndex = 27;
            this.NTDEditCheckBox.Text = "Изменение";
            this.NTDEditCheckBox.UseVisualStyleBackColor = true;
            // 
            // NTDDelCheckBox
            // 
            this.NTDDelCheckBox.AutoSize = true;
            this.NTDDelCheckBox.Checked = true;
            this.NTDDelCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.NTDDelCheckBox.Location = new System.Drawing.Point(17, 120);
            this.NTDDelCheckBox.Name = "NTDDelCheckBox";
            this.NTDDelCheckBox.Size = new System.Drawing.Size(76, 17);
            this.NTDDelCheckBox.TabIndex = 30;
            this.NTDDelCheckBox.Text = "Удаление";
            this.NTDDelCheckBox.UseVisualStyleBackColor = true;
            // 
            // NTDPrintCheckBox
            // 
            this.NTDPrintCheckBox.AutoSize = true;
            this.NTDPrintCheckBox.Checked = true;
            this.NTDPrintCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.NTDPrintCheckBox.Location = new System.Drawing.Point(17, 74);
            this.NTDPrintCheckBox.Name = "NTDPrintCheckBox";
            this.NTDPrintCheckBox.Size = new System.Drawing.Size(63, 17);
            this.NTDPrintCheckBox.TabIndex = 28;
            this.NTDPrintCheckBox.Text = "Печать";
            this.NTDPrintCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupControl2
            // 
            this.groupControl2.AutoSize = true;
            this.groupControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl2.Controls.Add(this.PersonalCheckBox);
            this.groupControl2.Controls.Add(this.PersonalAddCheckBox);
            this.groupControl2.Controls.Add(this.PersonalUvolnenieCheckBox);
            this.groupControl2.Controls.Add(this.PersonalEditCheckBox);
            this.groupControl2.Controls.Add(this.PersonalDeleteCheckBox);
            this.groupControl2.Controls.Add(this.PersonalPrintCheckBox);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl2.Location = new System.Drawing.Point(255, 20);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.ShowCaption = false;
            this.groupControl2.Size = new System.Drawing.Size(112, 362);
            this.groupControl2.TabIndex = 21;
            this.groupControl2.Text = "groupControl2";
            // 
            // PersonalCheckBox
            // 
            this.PersonalCheckBox.AutoSize = true;
            this.PersonalCheckBox.Checked = true;
            this.PersonalCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PersonalCheckBox.Location = new System.Drawing.Point(5, 5);
            this.PersonalCheckBox.Name = "PersonalCheckBox";
            this.PersonalCheckBox.Size = new System.Drawing.Size(74, 17);
            this.PersonalCheckBox.TabIndex = 19;
            this.PersonalCheckBox.Text = "Персонал";
            this.PersonalCheckBox.UseVisualStyleBackColor = true;
            this.PersonalCheckBox.CheckedChanged += new System.EventHandler(this.PersonalCheckBox_CheckedChanged);
            // 
            // PersonalAddCheckBox
            // 
            this.PersonalAddCheckBox.AutoSize = true;
            this.PersonalAddCheckBox.Checked = true;
            this.PersonalAddCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PersonalAddCheckBox.Location = new System.Drawing.Point(17, 28);
            this.PersonalAddCheckBox.Name = "PersonalAddCheckBox";
            this.PersonalAddCheckBox.Size = new System.Drawing.Size(88, 17);
            this.PersonalAddCheckBox.TabIndex = 20;
            this.PersonalAddCheckBox.Text = "Добавление";
            this.PersonalAddCheckBox.UseVisualStyleBackColor = true;
            // 
            // PersonalUvolnenieCheckBox
            // 
            this.PersonalUvolnenieCheckBox.AutoSize = true;
            this.PersonalUvolnenieCheckBox.Checked = true;
            this.PersonalUvolnenieCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PersonalUvolnenieCheckBox.Location = new System.Drawing.Point(17, 97);
            this.PersonalUvolnenieCheckBox.Name = "PersonalUvolnenieCheckBox";
            this.PersonalUvolnenieCheckBox.Size = new System.Drawing.Size(87, 17);
            this.PersonalUvolnenieCheckBox.TabIndex = 23;
            this.PersonalUvolnenieCheckBox.Text = "Увольнение";
            this.PersonalUvolnenieCheckBox.UseVisualStyleBackColor = true;
            // 
            // PersonalEditCheckBox
            // 
            this.PersonalEditCheckBox.AutoSize = true;
            this.PersonalEditCheckBox.Checked = true;
            this.PersonalEditCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PersonalEditCheckBox.Location = new System.Drawing.Point(17, 51);
            this.PersonalEditCheckBox.Name = "PersonalEditCheckBox";
            this.PersonalEditCheckBox.Size = new System.Drawing.Size(80, 17);
            this.PersonalEditCheckBox.TabIndex = 21;
            this.PersonalEditCheckBox.Text = "Изменение";
            this.PersonalEditCheckBox.UseVisualStyleBackColor = true;
            // 
            // PersonalDeleteCheckBox
            // 
            this.PersonalDeleteCheckBox.AutoSize = true;
            this.PersonalDeleteCheckBox.Checked = true;
            this.PersonalDeleteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PersonalDeleteCheckBox.Location = new System.Drawing.Point(17, 120);
            this.PersonalDeleteCheckBox.Name = "PersonalDeleteCheckBox";
            this.PersonalDeleteCheckBox.Size = new System.Drawing.Size(76, 17);
            this.PersonalDeleteCheckBox.TabIndex = 24;
            this.PersonalDeleteCheckBox.Text = "Удаление";
            this.PersonalDeleteCheckBox.UseVisualStyleBackColor = true;
            // 
            // PersonalPrintCheckBox
            // 
            this.PersonalPrintCheckBox.AutoSize = true;
            this.PersonalPrintCheckBox.Checked = true;
            this.PersonalPrintCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PersonalPrintCheckBox.Location = new System.Drawing.Point(17, 74);
            this.PersonalPrintCheckBox.Name = "PersonalPrintCheckBox";
            this.PersonalPrintCheckBox.Size = new System.Drawing.Size(63, 17);
            this.PersonalPrintCheckBox.TabIndex = 22;
            this.PersonalPrintCheckBox.Text = "Печать";
            this.PersonalPrintCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupControl1
            // 
            this.groupControl1.AutoSize = true;
            this.groupControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupControl1.Controls.Add(this.ReactivCheckBox);
            this.groupControl1.Controls.Add(this.ReactivAddCheckBox);
            this.groupControl1.Controls.Add(this.ReactivEditCheckBox);
            this.groupControl1.Controls.Add(this.ReactivRashodPrihodCheckBox);
            this.groupControl1.Controls.Add(this.ReactivUchetCheckBox);
            this.groupControl1.Controls.Add(this.ReactivVhodnoiKontrolCheckBox);
            this.groupControl1.Controls.Add(this.ReactivProverkaPrigodnostiCheckBox);
            this.groupControl1.Controls.Add(this.ReactivPrintCheckBox);
            this.groupControl1.Controls.Add(this.ReactivDeleteCheckBox);
            this.groupControl1.Controls.Add(this.ReactivSpisatCheckBox);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl1.Location = new System.Drawing.Point(2, 20);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(253, 362);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "groupControl1";
            // 
            // ReactivCheckBox
            // 
            this.ReactivCheckBox.AutoSize = true;
            this.ReactivCheckBox.Checked = true;
            this.ReactivCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ReactivCheckBox.Location = new System.Drawing.Point(5, 5);
            this.ReactivCheckBox.Name = "ReactivCheckBox";
            this.ReactivCheckBox.Size = new System.Drawing.Size(76, 17);
            this.ReactivCheckBox.TabIndex = 9;
            this.ReactivCheckBox.Text = "Реактивы";
            this.ReactivCheckBox.UseVisualStyleBackColor = true;
            this.ReactivCheckBox.CheckedChanged += new System.EventHandler(this.ReactivCheckBox_CheckedChanged);
            // 
            // ReactivAddCheckBox
            // 
            this.ReactivAddCheckBox.AutoSize = true;
            this.ReactivAddCheckBox.Checked = true;
            this.ReactivAddCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ReactivAddCheckBox.Location = new System.Drawing.Point(17, 28);
            this.ReactivAddCheckBox.Name = "ReactivAddCheckBox";
            this.ReactivAddCheckBox.Size = new System.Drawing.Size(88, 17);
            this.ReactivAddCheckBox.TabIndex = 10;
            this.ReactivAddCheckBox.Text = "Добавление";
            this.ReactivAddCheckBox.UseVisualStyleBackColor = true;
            // 
            // ReactivEditCheckBox
            // 
            this.ReactivEditCheckBox.AutoSize = true;
            this.ReactivEditCheckBox.Checked = true;
            this.ReactivEditCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ReactivEditCheckBox.Location = new System.Drawing.Point(17, 51);
            this.ReactivEditCheckBox.Name = "ReactivEditCheckBox";
            this.ReactivEditCheckBox.Size = new System.Drawing.Size(80, 17);
            this.ReactivEditCheckBox.TabIndex = 11;
            this.ReactivEditCheckBox.Text = "Изменение";
            this.ReactivEditCheckBox.UseVisualStyleBackColor = true;
            // 
            // ReactivRashodPrihodCheckBox
            // 
            this.ReactivRashodPrihodCheckBox.AutoSize = true;
            this.ReactivRashodPrihodCheckBox.Checked = true;
            this.ReactivRashodPrihodCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ReactivRashodPrihodCheckBox.Location = new System.Drawing.Point(17, 97);
            this.ReactivRashodPrihodCheckBox.Name = "ReactivRashodPrihodCheckBox";
            this.ReactivRashodPrihodCheckBox.Size = new System.Drawing.Size(104, 17);
            this.ReactivRashodPrihodCheckBox.TabIndex = 13;
            this.ReactivRashodPrihodCheckBox.Text = "Расход/Приход";
            this.ReactivRashodPrihodCheckBox.UseVisualStyleBackColor = true;
            // 
            // ReactivUchetCheckBox
            // 
            this.ReactivUchetCheckBox.AutoSize = true;
            this.ReactivUchetCheckBox.Checked = true;
            this.ReactivUchetCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ReactivUchetCheckBox.Location = new System.Drawing.Point(17, 143);
            this.ReactivUchetCheckBox.Name = "ReactivUchetCheckBox";
            this.ReactivUchetCheckBox.Size = new System.Drawing.Size(156, 17);
            this.ReactivUchetCheckBox.TabIndex = 15;
            this.ReactivUchetCheckBox.Text = "Журнал учета реактивов";
            this.ReactivUchetCheckBox.UseVisualStyleBackColor = true;
            // 
            // ReactivVhodnoiKontrolCheckBox
            // 
            this.ReactivVhodnoiKontrolCheckBox.AutoSize = true;
            this.ReactivVhodnoiKontrolCheckBox.Checked = true;
            this.ReactivVhodnoiKontrolCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ReactivVhodnoiKontrolCheckBox.Location = new System.Drawing.Point(17, 166);
            this.ReactivVhodnoiKontrolCheckBox.Name = "ReactivVhodnoiKontrolCheckBox";
            this.ReactivVhodnoiKontrolCheckBox.Size = new System.Drawing.Size(212, 17);
            this.ReactivVhodnoiKontrolCheckBox.TabIndex = 16;
            this.ReactivVhodnoiKontrolCheckBox.Text = "Акты входного контроля реактивов";
            this.ReactivVhodnoiKontrolCheckBox.UseVisualStyleBackColor = true;
            // 
            // ReactivProverkaPrigodnostiCheckBox
            // 
            this.ReactivProverkaPrigodnostiCheckBox.AutoSize = true;
            this.ReactivProverkaPrigodnostiCheckBox.Checked = true;
            this.ReactivProverkaPrigodnostiCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ReactivProverkaPrigodnostiCheckBox.Location = new System.Drawing.Point(17, 189);
            this.ReactivProverkaPrigodnostiCheckBox.Name = "ReactivProverkaPrigodnostiCheckBox";
            this.ReactivProverkaPrigodnostiCheckBox.Size = new System.Drawing.Size(229, 17);
            this.ReactivProverkaPrigodnostiCheckBox.TabIndex = 17;
            this.ReactivProverkaPrigodnostiCheckBox.Text = "Акты проверки пригодности реактивов";
            this.ReactivProverkaPrigodnostiCheckBox.UseVisualStyleBackColor = true;
            // 
            // ReactivPrintCheckBox
            // 
            this.ReactivPrintCheckBox.AutoSize = true;
            this.ReactivPrintCheckBox.Checked = true;
            this.ReactivPrintCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ReactivPrintCheckBox.Location = new System.Drawing.Point(17, 74);
            this.ReactivPrintCheckBox.Name = "ReactivPrintCheckBox";
            this.ReactivPrintCheckBox.Size = new System.Drawing.Size(63, 17);
            this.ReactivPrintCheckBox.TabIndex = 12;
            this.ReactivPrintCheckBox.Text = "Печать";
            this.ReactivPrintCheckBox.UseVisualStyleBackColor = true;
            // 
            // ReactivDeleteCheckBox
            // 
            this.ReactivDeleteCheckBox.AutoSize = true;
            this.ReactivDeleteCheckBox.Checked = true;
            this.ReactivDeleteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ReactivDeleteCheckBox.Location = new System.Drawing.Point(17, 212);
            this.ReactivDeleteCheckBox.Name = "ReactivDeleteCheckBox";
            this.ReactivDeleteCheckBox.Size = new System.Drawing.Size(76, 17);
            this.ReactivDeleteCheckBox.TabIndex = 18;
            this.ReactivDeleteCheckBox.Text = "Удаление";
            this.ReactivDeleteCheckBox.UseVisualStyleBackColor = true;
            // 
            // ReactivSpisatCheckBox
            // 
            this.ReactivSpisatCheckBox.AutoSize = true;
            this.ReactivSpisatCheckBox.Checked = true;
            this.ReactivSpisatCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ReactivSpisatCheckBox.Location = new System.Drawing.Point(17, 120);
            this.ReactivSpisatCheckBox.Name = "ReactivSpisatCheckBox";
            this.ReactivSpisatCheckBox.Size = new System.Drawing.Size(74, 17);
            this.ReactivSpisatCheckBox.TabIndex = 14;
            this.ReactivSpisatCheckBox.Text = "Списание";
            this.ReactivSpisatCheckBox.UseVisualStyleBackColor = true;
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.GroupControl);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 192);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1120, 401);
            this.xtraScrollableControl1.TabIndex = 9;
            // 
            // UseDocProtectCheckBox
            // 
            this.UseDocProtectCheckBox.AutoSize = true;
            this.UseDocProtectCheckBox.Checked = true;
            this.UseDocProtectCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UseDocProtectCheckBox.Location = new System.Drawing.Point(297, 69);
            this.UseDocProtectCheckBox.Name = "UseDocProtectCheckBox";
            this.UseDocProtectCheckBox.Size = new System.Drawing.Size(182, 17);
            this.UseDocProtectCheckBox.TabIndex = 15;
            this.UseDocProtectCheckBox.Text = "Зачищать паролем документы";
            this.UseDocProtectCheckBox.UseVisualStyleBackColor = true;
            // 
            // AddChangeUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 593);
            this.Controls.Add(this.xtraScrollableControl1);
            this.Controls.Add(this.MainPanelControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "AddChangeUserForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Создание/изменение пользователей";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ReactivListAktVhodnogoKontrolaForm_FormClosed);
            this.Load += new System.EventHandler(this.ReactivListAktVhodnogoKontrolaForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AddChangeUserForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.MainPanelControl)).EndInit();
            this.MainPanelControl.ResumeLayout(false);
            this.MainPanelControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl)).EndInit();
            this.GroupControl.ResumeLayout(false);
            this.GroupControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.groupControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            this.groupControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).EndInit();
            this.groupControl14.ResumeLayout(false);
            this.groupControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).EndInit();
            this.groupControl15.ResumeLayout(false);
            this.groupControl15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl16)).EndInit();
            this.groupControl16.ResumeLayout(false);
            this.groupControl16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl17)).EndInit();
            this.groupControl17.ResumeLayout(false);
            this.groupControl17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.xtraScrollableControl1.ResumeLayout(false);
            this.xtraScrollableControl1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl MainPanelControl;
        private DevExpress.XtraEditors.GroupControl GroupControl;
        private System.Windows.Forms.ComboBox TipComboBox;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox AdminCheckBox;
        private System.Windows.Forms.CheckBox ReactivPrintCheckBox;
        private System.Windows.Forms.CheckBox ReactivProverkaPrigodnostiCheckBox;
        private System.Windows.Forms.CheckBox ReactivVhodnoiKontrolCheckBox;
        private System.Windows.Forms.CheckBox ReactivUchetCheckBox;
        private System.Windows.Forms.CheckBox ReactivRashodPrihodCheckBox;
        private System.Windows.Forms.CheckBox ReactivEditCheckBox;
        private System.Windows.Forms.CheckBox ReactivAddCheckBox;
        private System.Windows.Forms.CheckBox ReactivCheckBox;
        private System.Windows.Forms.CheckBox ReactivSpisatCheckBox;
        private System.Windows.Forms.CheckBox ReactivDeleteCheckBox;
        private System.Windows.Forms.CheckBox PersonalUvolnenieCheckBox;
        private System.Windows.Forms.CheckBox PersonalDeleteCheckBox;
        private System.Windows.Forms.CheckBox PersonalPrintCheckBox;
        private System.Windows.Forms.CheckBox PersonalEditCheckBox;
        private System.Windows.Forms.CheckBox PersonalAddCheckBox;
        private System.Windows.Forms.CheckBox PersonalCheckBox;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private System.Windows.Forms.CheckBox VLKCheckBox;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private System.Windows.Forms.CheckBox VOCheckBox;
        private System.Windows.Forms.CheckBox VOAddCheckBox;
        private System.Windows.Forms.CheckBox VOArhivCheckBox;
        private System.Windows.Forms.CheckBox VOEditCheckBox;
        private System.Windows.Forms.CheckBox VODeleteCheckBox;
        private System.Windows.Forms.CheckBox VOPrintCheckBox;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private System.Windows.Forms.CheckBox IOCheckBox;
        private System.Windows.Forms.CheckBox IOAddCheckBox;
        private System.Windows.Forms.CheckBox IOArhivCheckBox;
        private System.Windows.Forms.CheckBox IOEditCheckBox;
        private System.Windows.Forms.CheckBox IODeleteCheckBox;
        private System.Windows.Forms.CheckBox IOPrintCheckBox;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private System.Windows.Forms.CheckBox SICheckBox;
        private System.Windows.Forms.CheckBox SIAddCheckBox;
        private System.Windows.Forms.CheckBox SIArhivCheckBox;
        private System.Windows.Forms.CheckBox SIEditCheckBox;
        private System.Windows.Forms.CheckBox SIDeleteCheckBox;
        private System.Windows.Forms.CheckBox SIPrintCheckBox;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.CheckBox NTDCheckBox;
        private System.Windows.Forms.CheckBox NTDAddCheckBox;
        private System.Windows.Forms.CheckBox NTDArhivCheckBox;
        private System.Windows.Forms.CheckBox NTDEditCheckBox;
        private System.Windows.Forms.CheckBox NTDDelCheckBox;
        private System.Windows.Forms.CheckBox NTDPrintCheckBox;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraEditors.SimpleButton SaveButton;
        private System.Windows.Forms.ComboBox UserName;
        private DevExpress.XtraEditors.SimpleButton DeleteButton;
        private System.Windows.Forms.CheckBox DisabledCheckBox;
        private System.Windows.Forms.CheckBox DeveloperCheckBox;
        private System.Windows.Forms.CheckBox AlertsCheckBox;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private DevExpress.XtraEditors.GroupControl groupControl14;
        private System.Windows.Forms.CheckBox StabilnostiCheckBox;
        private System.Windows.Forms.CheckBox StabilnostiEditCheckBox;
        private DevExpress.XtraEditors.GroupControl groupControl15;
        private System.Windows.Forms.CheckBox OperativniiCheckBox;
        private System.Windows.Forms.CheckBox OperativniiEditCheckBox;
        private System.Windows.Forms.CheckBox StabilnostiCloseCheckBox;
        private System.Windows.Forms.CheckBox StabilnostiDeleteCheckBox;
        private System.Windows.Forms.CheckBox StabilnostiPrintCheckBox;
        private DevExpress.XtraEditors.GroupControl groupControl16;
        private System.Windows.Forms.CheckBox ProbiCheckBox;
        private System.Windows.Forms.CheckBox ProbiAddCheckBox;
        private System.Windows.Forms.CheckBox ProbiEditCheckBox;
        private System.Windows.Forms.CheckBox ProbiDeleteCheckBox;
        private System.Windows.Forms.CheckBox ProbiPrintCheckBox;
        private DevExpress.XtraEditors.GroupControl groupControl17;
        private System.Windows.Forms.CheckBox AnalizCheckBox;
        private System.Windows.Forms.CheckBox AnalizAddCheckBox;
        private System.Windows.Forms.CheckBox AnalizEditCheckBox;
        private System.Windows.Forms.CheckBox AnalizDeleteCheckBox;
        private System.Windows.Forms.CheckBox ProbiChangeDataCheckBox;
        private System.Windows.Forms.CheckBox StabilnostiDiagramsCheckBox;
        private System.Windows.Forms.CheckBox StabilnostiKKSHCheckBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox OperativniiCloseCheckBox;
        private System.Windows.Forms.CheckBox OperativniiPrintCheckBox;
        private System.Windows.Forms.CheckBox OperativniiDeleteCheckBox;
        private System.Windows.Forms.CheckBox UseDocProtectCheckBox;
    }
}