﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public static class GlobalStatic
    {
        public static string SecurityKey = "*@-+el-teh@bk.ru+-@*";
        public static DateTime? UchetFormOtValue;
        public static DateTime? UchetFormDoValue;
        public static int BuferInt;
        public static string BuferStr;

        public static List<string> GetSkins()
        {
            DevExpress.Skins.SkinContainerCollection skins = DevExpress.Skins.SkinManager.Default.Skins;
            List<string> SkinList = new List<string>();
            for (int i = 0; i < skins.Count; i++)
            {
                SkinList.Add(skins[i].SkinName);
            }
            return SkinList;
        }

        public static List<string> GetPersonal()
        {
            List<string> PersonalName = new List<string>();
            PersonalName.Add("");
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var ВсеЗначения = db.Personal.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0).OrderBy(c => c.ФИО).Select(c => c.ФИО).ToList();
                    if (ВсеЗначения != null && ВсеЗначения.Count() > 0)
                    {
                        foreach (string FullName in ВсеЗначения)
                        {
                            if (FullName == "Developer") continue;
                            var ShortName = "";
                            if (FullName != "")
                            {
                                var Names = FullName.Split(' ');
                                if (Names.Length > 2)
                                {
                                    ShortName = Names[0] + " " + Names[1].Substring(0, 1) + "." + Names[2].Substring(0, 1) + ".";
                                }
                                else
                                {
                                    if (Names.Length > 1) { ShortName = Names[0] + " " + Names[1].Substring(0, 1) + "."; }
                                    else ShortName = Names[0];
                                }
                            }
                            PersonalName.Add(ShortName);
                        }
                        db.Dispose();
                        return PersonalName;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
    }
}
