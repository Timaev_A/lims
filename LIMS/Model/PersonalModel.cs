﻿using System;

namespace LIMS.Model
{
    class PersonalModel
    {
        public int _Id { get; set; }

        public string _N { get; set; }

        public string _ФИО { get; set; }

        public string _Должность { get; set; }

        public string _Образование { get; set; }

        public string _Образование__полное_ { get; set; }

        public string _Практический_опыт { get; set; }

        public string _Повыш_квал { get; set; }

        public DateTime? _Срок_подтверждения { get; set; }

        public DateTime? _Обучение_на_лаборанта { get; set; }

        public DateTime? _Срок_подтверждения_лаборанта { get; set; }

        public DateTime? _Аттестация { get; set; }

        public DateTime? _Срок_аттестации { get; set; }

        public DateTime? _Проверка_знаний_по_ОТ_и_ТБ { get; set; }

        public DateTime? _Срок_проверки_знаний_по_ОТ_и_ТБ { get; set; }

        public string _Дополнительно { get; set; }
    }    
}
