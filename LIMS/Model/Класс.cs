﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIMS.Model
{
    class МодельТаблицыОсновная
    {
        private int _Id { get; set; }

        private string _ВидДокументации { get; set; }

        private string _НаименованиеДокумента { get; set; }

        private string _РегистрационныйНомер { get; set; }

        private string _ДатаУтверждения { get; set; }

        private string _НаличиеКонтрольногоЭкземпляраКолво { get; set; }

        private string _НаличиеКонтрольногоЭкземпляраМестоНахождения { get; set; }

        private string _НаличиеРабочегоЭкземпляраКолво { get; set; }

        private string _НаличиеРабочегоЭкземпляраМестоНахождения { get; set; }

        private string _ДатаПоследнейАктуализации { get; set; }

        private string _СрокДействия { get; set; }

    }
}
