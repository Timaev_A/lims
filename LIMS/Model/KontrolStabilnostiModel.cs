﻿using System;

namespace LIMS
{
    public class KontrolStabilnostiModel
    {
        public int Id { get; set; }
        public int _N { get; set; }
        public DateTime? _Дата { get; set; }
        public string _Шифр { get; set; }
        public double? X1 { get; set; }
        public double? X2 { get; set; }
        public double? X { get; set; }
        public double? _АттестованноеЗначение { get; set; }
        public double? _rSmall { get; set; }
        public double? _RBig { get; set; }
        public double? _KK { get; set; }
        public string _VivodPovtoriamost { get; set; }
        public string _VivodPrechizionnost { get; set; }
        public string _VivodTochnost { get; set; }
        public string _Rezultat { get; set; }
        public int? _OperativniiKontrolID { get; set; }
        public int? _KontrolStabilnostiID { get; set; }
        public int? _AnalizID { get; set; }                     
    }
}
