﻿using System;

namespace LIMS.Model
{
    class NTDModel
    {
        public int _Id { get; set; }

        public string _Уровень { get; set; }

        public string _Группа { get; set; }

        public string _N { get; set; }

        public string _НД { get; set; }

        public string _НаименованиеДокумента { get; set; }

        public string _РегистрационныйНомер { get; set; }

        public DateTime? _ДатаУтверждения { get; set; }

        public string _НаличиеКонтрольногоЭкземпляраКолво { get; set; }

        public string _НаличиеКонтрольногоЭкземпляраМестоНахождения { get; set; }

        public string _НаличиеРабочегоЭкземпляраКолво { get; set; }

        public string _НаличиеРабочегоЭкземпляраМестоНахождения { get; set; }

        public DateTime? _ДатаПоследнейАктуализации { get; set; }

        public DateTime? _СрокДействия { get; set; }

        public string _Примечание { get; set; }        
    }    
}
