﻿using System;

namespace LIMS
{
    public class OperativniiKontrolModel
    {      
        public int _Id { get; set; }
        public DateTime? _Дата { get; set; }
        public string _Исполнитель { get; set; }
        public string _Шифр { get; set; }
        public string _КонтролируемыйОбъект { get; set; }
        public string _ОпределяемыйКомпонент { get; set; }
        public double? _X { get; set; }
        public double? _АттестованноеЗначение { get; set; } 
        public double? _KK { get; set; }
        public double? _НормативКонтроляК { get; set; }
        public string _ЗаключениеК { get; set; }
        public int? _OperativniiKontrolID { get; set; }
        public int? _KontrolStabilnostiID { get; set; }
        public int? _AnalizID { get; set; }
    }
}
