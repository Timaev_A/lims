﻿using System;

namespace LIMS.Model
{
    class ReactivModel
    {
        public int _Id { get; set; }

        public string _N { get; set; }

        public string _НаименованиеРеактива { get; set; }

        public string _Классификация { get; set; }

        public string _НДНаРеактив { get; set; }

        public string _НомерПартии { get; set; }

        public DateTime? _ДатаИзготовления { get; set; }

        public DateTime? _СрокГодности { get; set; }

        public DateTime? _ДатаПолучения { get; set; }

        public string _Остаток { get; set; }

        public string _ЕдИзм { get; set; }

        public string _Назначение { get; set; }

        public string _ПродлениеСрока { get; set; }
    }    
}
