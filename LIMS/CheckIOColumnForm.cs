﻿using System;
using System.Windows.Forms;

namespace LIMS
{
    public partial class CheckIOColumnForm : DevExpress.XtraEditors.XtraForm
    {
        IOTab RT;
        public CheckIOColumnForm(MainForm MF, IOTab IOTab)
        {
            InitializeComponent();
            try
            {
                if (UserSettings.IOColumns != null)
                {
                    var CheckColumn = UserSettings.IOColumns.Split(',');
                    CheckEditN.Checked = CheckColumn[0] == "1";
                    CheckEditNameHaracteristis.Checked = CheckColumn[1] == "1";
                    CheckEditName.Checked = CheckColumn[2] == "1";
                    CheckEditTip.Checked = CheckColumn[3] == "1";
                    CheckEditZavN.Checked = CheckColumn[4] == "1";
                    CheckEditInventarniiN.Checked = CheckColumn[5] == "1";
                    CheckEditIzgotovitel.Checked = CheckColumn[6] == "1";
                    CheckEditGogVipuska.Checked = CheckColumn[7] == "1";
                    CheckEditTehnichHarakteristiki.Checked = CheckColumn[8] == "1";
                    CheckEditGodVvodaVEkspluatachiu.Checked = CheckColumn[9] == "1";
                    CheckEditNDokumentaAttestachii.Checked = CheckColumn[10] == "1";
                    CheckEditDataAttestachii.Checked = CheckColumn[11] == "1";
                    CheckEditPeriodichnost.Checked = CheckColumn[12] == "1";
                    CheckEditDateSledAttestachii.Checked = CheckColumn[13] == "1";
                    CheckEditPravoSobstvennosti.Checked = CheckColumn[14] == "1";
                    CheckEditMestoUstanovkiIHranenia.Checked = CheckColumn[15] == "1";
                    CheckEditDatePoslednegoTO.Checked = CheckColumn[16] == "1";
                    CheckEditDateSledTO.Checked = CheckColumn[17] == "1";
                    CheckEditPrimeshanie.Checked = CheckColumn[18] == "1";
                }
            } catch { }
            RT = IOTab;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            string CheckString = "";
            if (CheckEditN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditNameHaracteristis.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditName.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditTip.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditZavN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditInventarniiN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditIzgotovitel.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditGogVipuska.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditTehnichHarakteristiki.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditGodVvodaVEkspluatachiu.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditNDokumentaAttestachii.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDataAttestachii.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditPeriodichnost.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDateSledAttestachii.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditPravoSobstvennosti.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditMestoUstanovkiIHranenia.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDatePoslednegoTO.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDateSledTO.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditPrimeshanie.Checked == true) { CheckString = CheckString + "1"; } else { CheckString = CheckString + "0"; }
            MainForm main = this.Owner as MainForm;
            if (main != null)
            {
                UserSettings.IOColumns = CheckString;
                UserSettings.SaveSettings();
                RT.GetIO(main);
            }
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CheckEditDateGet_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void CheckEditN_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void CheckIOColumnForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
