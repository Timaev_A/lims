﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace LIMS
{
    public class RW_XML
    {
        internal static string ReadValueOfProperty(string path, string property)
        {
            try
            {
                var NewList = LoadXML(path);
                foreach (var strok in NewList)
                {
                    if (strok.property == property) return strok.value;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
        internal static void AddToFile(string path, SerializerClass el)
        {
            var NewList = LoadXML(path);
            bool Add = true;
            if (!NewList.Contains(el))
            {
                foreach (var INList in NewList)
                {
                    if (INList.property == el.property)
                    {
                        INList.value = el.value;
                        Add = false;
                        break;
                    }
                }
            }
            else Add = false;
            if (Add) NewList.Add(el);
            SaveXML(NewList, path);
        }

        internal static void SaveXML(List<SerializerClass> XMLList, string path)
        {
            try
            {
                bool exists = System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS");
                if (!exists)
                    System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS");

                var xEle = new XElement("configuration", XMLList.Select(i => new XElement(i.property, i.value)));
                xEle.Save(path);
            }
            catch { }
        }

        internal static List<SerializerClass> LoadXML(string path)
        {
            try
            {
                bool exists = System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS");
                if (!exists)
                    System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS");

                XElement xEle = XElement.Load(path);
                List<SerializerClass> XMLList = new List<SerializerClass>();
                foreach (var Elem in xEle.Elements())
                {
                    XMLList.Add(new SerializerClass
                    {
                        property = Elem.Name.ToString(),
                        value = Elem.Value
                    });
                }
                return XMLList;
            }
            catch
            {
                return new List<SerializerClass>();
            }
        }
    }
}
