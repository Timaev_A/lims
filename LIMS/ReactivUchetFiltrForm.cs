﻿using System;
using System.Windows.Forms;

namespace LIMS
{
    public partial class ReactivUchetFiltrForm : DevExpress.XtraEditors.XtraForm
    {
        ReactivUchetReactivovForm UchetForm;
        public ReactivUchetFiltrForm(MainForm MF, ReactivUchetReactivovForm ReactivUchetForm)
        {
            InitializeComponent();
            UchetForm = ReactivUchetForm;            
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            string CheckString = "";
            if (CheckEditName.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditClass.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditNPartii.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDateIzgot.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditOt.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDo.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditPrihod.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditRashod.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditOstatok.Checked == true) { CheckString = CheckString + "1"; } else { CheckString = CheckString + "0"; }
            UchetForm.VisibleColumns = CheckString;
            if (СheckEditSearchByDate.Checked)
            {
                UchetForm.Ot = DateTimeOt.Value;
                UchetForm.Do = DateTimeDo.Value;
            }
            else
            {
                UchetForm.Ot = null;
                UchetForm.Do = null;
            }
            UchetForm.GetReactivUchet();
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ReactivUchetFiltrForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (UchetForm.VisibleColumns != "")
                {
                    var CheckColumn = UchetForm.VisibleColumns.Split(',');
                    CheckEditName.Checked = CheckColumn[0] == "1";
                    CheckEditClass.Checked = CheckColumn[1] == "1";
                    CheckEditNPartii.Checked = CheckColumn[2] == "1";
                    CheckEditDateIzgot.Checked = CheckColumn[3] == "1";
                    CheckEditOt.Checked = CheckColumn[4] == "1";
                    CheckEditDo.Checked = CheckColumn[5] == "1";
                    CheckEditPrihod.Checked = CheckColumn[6] == "1";
                    CheckEditRashod.Checked = CheckColumn[7] == "1";
                    CheckEditOstatok.Checked = CheckColumn[8] == "1";
                }
                if (UchetForm.Ot != null)
                {
                    DateTimeOt.Value = GlobalStatic.UchetFormOtValue.HasValue ? GlobalStatic.UchetFormOtValue.Value : DateTime.Now;
                    СheckEditSearchByDate.Checked = true;
                    DateTimeOt.Enabled = true;
                }
                if (UchetForm.Do != null)
                {
                    DateTimeDo.Value = GlobalStatic.UchetFormDoValue.HasValue ? GlobalStatic.UchetFormDoValue.Value : DateTime.Now;
                    DateTimeDo.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void СheckEditSearchByDate_CheckedChanged(object sender, EventArgs e)
        {
            DateTimeOt.Enabled = СheckEditSearchByDate.Checked;
            DateTimeDo.Enabled = СheckEditSearchByDate.Checked;
        }

        private void СheckEditSearchByDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void ReactivUchetFiltrForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
