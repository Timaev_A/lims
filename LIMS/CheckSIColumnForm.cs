﻿using System;
using System.Windows.Forms;

namespace LIMS
{
    public partial class CheckSIColumnForm : DevExpress.XtraEditors.XtraForm
    {
        SITab RT;
        public CheckSIColumnForm(MainForm MF, SITab SITab)
        {
            InitializeComponent();
            try
            {
                if (UserSettings.SIColumns != null)
                {
                    var CheckColumn = UserSettings.SIColumns.Split(',');
                    CheckEditN.Checked = CheckColumn[0] == "1";
                    CheckEditNameHaracteristic.Checked = CheckColumn[1] == "1";
                    CheckEditName.Checked = CheckColumn[2] == "1";
                    CheckEditTip.Checked = CheckColumn[3] == "1";
                    CheckEditZavN.Checked = CheckColumn[4] == "1";
                    CheckEditGodVipuska.Checked = CheckColumn[5] == "1";
                    CheckEditIzgotovitel.Checked = CheckColumn[6] == "1";
                    CheckEditGodVvodaVEksplyatachiu.Checked = CheckColumn[7] == "1";
                    CheckEditInventarniiN.Checked = CheckColumn[8] == "1";
                    CheckEditDiapozomIzmerenii.Checked = CheckColumn[9] == "1";
                    CheckEditClassTochnosti.Checked = CheckColumn[10] == "1";
                    CheckEditSvidetelstvoOPoverke.Checked = CheckColumn[11] == "1";
                    CheckEditDatePoverki.Checked = CheckColumn[12] == "1";
                    CheckEditSrokDeistvia.Checked = CheckColumn[13] == "1";
                    CheckEditDateSledPoverki.Checked = CheckColumn[14] == "1";
                    CheckEditPravoSobstvennosti.Checked = CheckColumn[15] == "1";
                    CheckEditMestoUstanovkiIHranenia.Checked = CheckColumn[16] == "1";
                    CheckEditDatePoslednegoTO.Checked = CheckColumn[17] == "1";
                    CheckEditDateSledTO.Checked = CheckColumn[18] == "1";
                    CheckEditPrimechanie.Checked = CheckColumn[19] == "1";
                }
            } catch { }
            RT = SITab;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            string CheckString = "";
            if (CheckEditN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditNameHaracteristic.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditName.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditTip.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditZavN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditGodVipuska.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditIzgotovitel.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditGodVvodaVEksplyatachiu.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditInventarniiN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDiapozomIzmerenii.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditClassTochnosti.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditSvidetelstvoOPoverke.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDatePoverki.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditSrokDeistvia.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDateSledPoverki.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditPravoSobstvennosti.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditMestoUstanovkiIHranenia.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDatePoslednegoTO.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDateSledTO.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditPrimechanie.Checked == true) { CheckString = CheckString + "1"; } else { CheckString = CheckString + "0"; }
            MainForm main = this.Owner as MainForm;
            if (main != null)
            {
                UserSettings.SIColumns = CheckString;
                UserSettings.SaveSettings();
                RT.GetSI(main);
            }
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CheckEditN_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void CheckSIColumnForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
