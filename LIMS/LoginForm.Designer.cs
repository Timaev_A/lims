﻿namespace LIMS
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CancelButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.UserBox = new System.Windows.Forms.ComboBox();
            this.PasswordBox = new System.Windows.Forms.TextBox();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.AboutClick = new System.Windows.Forms.PictureBox();
            this.AlertCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AboutClick)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Пользователь:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Пароль:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = global::LIMS.Properties.Resources.zamok;
            this.pictureBox1.Image = global::LIMS.Properties.Resources.zamok;
            this.pictureBox1.Location = new System.Drawing.Point(7, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(92, 98);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // CancelButton2
            // 
            this.CancelButton2.Location = new System.Drawing.Point(281, 88);
            this.CancelButton2.Name = "CancelButton2";
            this.CancelButton2.Size = new System.Drawing.Size(97, 23);
            this.CancelButton2.TabIndex = 2;
            this.CancelButton2.Text = "Отмена";
            this.CancelButton2.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(183, 88);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(92, 23);
            this.OKButton.TabIndex = 0;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.ButtonOK_Click);
            // 
            // UserBox
            // 
            this.UserBox.FormattingEnabled = true;
            this.UserBox.Location = new System.Drawing.Point(183, 11);
            this.UserBox.Name = "UserBox";
            this.UserBox.Size = new System.Drawing.Size(195, 21);
            this.UserBox.TabIndex = 3;
            this.UserBox.SelectedIndexChanged += new System.EventHandler(this.UserBox_SelectedIndexChanged);
            // 
            // PasswordBox
            // 
            this.PasswordBox.Location = new System.Drawing.Point(183, 38);
            this.PasswordBox.Name = "PasswordBox";
            this.PasswordBox.PasswordChar = '*';
            this.PasswordBox.Size = new System.Drawing.Size(195, 21);
            this.PasswordBox.TabIndex = 4;
            this.PasswordBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PasswordBox_KeyDown);
            // 
            // AboutClick
            // 
            this.AboutClick.Image = global::LIMS.Properties.Resources.info_32x32;
            this.AboutClick.Location = new System.Drawing.Point(144, 88);
            this.AboutClick.Name = "AboutClick";
            this.AboutClick.Size = new System.Drawing.Size(33, 23);
            this.AboutClick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AboutClick.TabIndex = 17;
            this.AboutClick.TabStop = false;
            this.AboutClick.Click += new System.EventHandler(this.About_Click);
            // 
            // AlertCheckBox
            // 
            this.AlertCheckBox.AutoSize = true;
            this.AlertCheckBox.Checked = true;
            this.AlertCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AlertCheckBox.Location = new System.Drawing.Point(183, 65);
            this.AlertCheckBox.Name = "AlertCheckBox";
            this.AlertCheckBox.Size = new System.Drawing.Size(143, 17);
            this.AlertCheckBox.TabIndex = 5;
            this.AlertCheckBox.Text = "Включить оповещения";
            this.AlertCheckBox.UseVisualStyleBackColor = true;
            this.AlertCheckBox.CheckedChanged += new System.EventHandler(this.AlertCheckBox_CheckedChanged);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 121);
            this.Controls.Add(this.AlertCheckBox);
            this.Controls.Add(this.AboutClick);
            this.Controls.Add(this.PasswordBox);
            this.Controls.Add(this.UserBox);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.CancelButton2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(50, 50);
            this.Name = "LoginForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Авторизация";
            this.TopMost = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LoginForm_FormClosed);
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoginForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AboutClick)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SimpleButton CancelButton2;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private System.Windows.Forms.ComboBox UserBox;
        private System.Windows.Forms.TextBox PasswordBox;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private System.Windows.Forms.PictureBox AboutClick;
        private System.Windows.Forms.CheckBox AlertCheckBox;
    }
}