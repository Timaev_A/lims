﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public static class VLKStatic
    {
        public static double X = 0;
        public static double rk = 0;
        public static double Rk = 0;
        public static double r = 0;
        public static double R = 0;
        public static double Or = 0;
        public static double OR = 0;
        public static double ORl = 0;
        public static double A = 0;
        public static double Al = 0;
        public static double Acl = 0;
        public static int NumberRound = 0;
        public static double Povt1 = 0;
        public static double Povt2 = 0;
        public static double Povt3 = 0;
        public static double Pogr1 = 0;
        public static double Pogr2 = 0;
        public static double Prech1 = 0;
        public static double Prech2 = 0;
        public static double Prech3 = 0;

        public static void CalculatePredel(Probi Stroka)
        {
            try
            {
                X = 0;
                rk = 0;
                Rk = 0;
                r = 0;
                R = 0;
                Or = 0;
                OR = 0;
                ORl = 0;
                A = 0;
                Al = 0;
                Acl = 0;
                NumberRound = 0;
                Povt1 = 0;
                Povt2 = 0;
                Povt3 = 0;
                Pogr1 = 0;
                Pogr2 = 0;
                Prech1 = 0;
                Prech2 = 0;
                Prech3 = 0;
                if (Stroka != null)
                {
                    X = Stroka.X.HasValue ? Stroka.X.Value : 0;
                    var N1 = GetRoundNumber(Stroka.АттестованноеЗначение.Value);
                    var N2 = Stroka.X1.HasValue ? GetRoundNumber(Stroka.X1.Value) : 0;
                    var N3 = Stroka.X2.HasValue ? GetRoundNumber(Stroka.X2.Value) : 0;
                    NumberRound = N1 >= N2 ? N1 >= N3 ? N1 : N3 : N2 >= N3 ? N2 : N3;
                    if (NumberRound < 2) NumberRound = 2;
                    rk = Stroka.rSmall.HasValue ? Math.Round(Stroka.rSmall.Value, NumberRound, MidpointRounding.AwayFromZero) : 0;
                    Rk = Stroka.RBig.HasValue ? Math.Round(Stroka.RBig.Value, NumberRound, MidpointRounding.AwayFromZero) : 0;
                    using (DbDataContext db = new DbDataContext())
                    {
                        var AnalizCalculateProfile = db.Analiz.Where(c => c.Id == Stroka.AnalizID).FirstOrDefault().CalculateProfile;
                        if (AnalizCalculateProfile == null || AnalizCalculateProfile == "")
                        {
                            var AnalizTable = db.AnalizTable.Where(c => c.AnalizID == Stroka.AnalizID && (c.VisibleStatus == null || c.VisibleStatus <= 2)).ToList();
                            if (AnalizTable != null && AnalizTable.Count > 0)
                            {
                                foreach (AnalizTable Select in AnalizTable)
                                {
                                    var newOt = Select.Down.HasValue ? Select.Down.Value : 0;
                                    var newDo = Select.Up.HasValue ? Select.Up.Value : 9999999999999999999;
                                    if (Povt1 == 0 && newOt < Stroka.АттестованноеЗначение && newDo >= Stroka.АттестованноеЗначение || Povt1 == 0 && newOt == newDo && newDo == Stroka.АттестованноеЗначение)
                                    {
                                        r = Select.RSmall.HasValue ? Select.RSmall.Value : 0;
                                        R = Select.RBig.HasValue ? Select.RBig.Value : 0;
                                        Or = Math.Round((double)((decimal)(r / 2.77)), NumberRound, MidpointRounding.AwayFromZero);
                                        if (Select.RBig.HasValue)
                                            OR = Math.Round((double)((decimal)(R / 2.77)), NumberRound, MidpointRounding.AwayFromZero);
                                        else
                                            OR = Math.Round((double)((decimal)(2 * Or)), NumberRound, MidpointRounding.AwayFromZero);
                                        ORl = Math.Round((double)((decimal)(0.84 * OR)), NumberRound, MidpointRounding.AwayFromZero);
                                        if (Select.Q.HasValue && Select.Q.Value != 0)
                                            A = Select.Q.Value;
                                        else A = Math.Round((double)((decimal)(1.96 * OR)), NumberRound, MidpointRounding.AwayFromZero);
                                        Al = Math.Round((double)((decimal)(0.84 * A)), NumberRound, MidpointRounding.AwayFromZero);
                                    }
                                }
                            }
                        }
                        else
                        {
                            switch (AnalizCalculateProfile)
                            {
                                case "Сера":
                                    r = Math.Round((double)((decimal)(0.02894 * (Stroka.АттестованноеЗначение.Value + 0.1691))), NumberRound - 1, MidpointRounding.AwayFromZero); //0,02894*(Х+0,1691)
                                    R = Math.Round((double)((decimal)(0.1215 * (Stroka.АттестованноеЗначение.Value + 0.05555))), NumberRound - 1, MidpointRounding.AwayFromZero);  //0,1215*(Х+ 0,05555)
                                    if (Stroka.АттестованноеЗначение.Value == 3)
                                    {
                                        Or = 0.032;
                                        OR = 0.134;
                                        ORl = 0.11;
                                        A = 0.26;
                                        Al = 0.22;
                                    }
                                    else
                                    {
                                        if (Stroka.АттестованноеЗначение.Value == 4)
                                        {
                                            Or = 0.043;
                                            OR = 0.177;
                                            ORl = 0.15;
                                            A = 0.35;
                                            Al = 0.29;
                                        }
                                        else
                                        {
                                            if (Stroka.АттестованноеЗначение.Value == 5)
                                            {
                                                Or = 0.054;
                                                OR = 0.22;
                                                ORl = 0.18;
                                                A = 0.43;
                                                Al = 0.36;
                                            }
                                            else
                                            {
                                                r = Math.Round((double)((decimal)(0.02894 * (Stroka.АттестованноеЗначение.Value + 0.1691))), NumberRound - 1, MidpointRounding.AwayFromZero); //0,02894*(Х+0,1691)
                                                R = Math.Round((double)((decimal)(0.1215 * (Stroka.АттестованноеЗначение.Value + 0.05555))), NumberRound - 1, MidpointRounding.AwayFromZero);  //0,1215*(Х+ 0,05555)
                                                Or = Math.Round((double)((decimal)(0.01 * (Stroka.АттестованноеЗначение.Value + 0.1691))), NumberRound - 1, MidpointRounding.AwayFromZero); //0,010*(Х+ 0,01691)
                                                OR = Math.Round((double)((decimal)(0.044 * (Stroka.АттестованноеЗначение.Value + 0.05555))), NumberRound - 1, MidpointRounding.AwayFromZero); //0,044*(Х+ 0,05555)
                                                ORl = Math.Round((double)((decimal)(0.037 * (Stroka.АттестованноеЗначение.Value + 0.05555))), NumberRound - 1, MidpointRounding.AwayFromZero); //0,037*(Х+ 0,05555)
                                                A = Math.Round((double)((decimal)(0.09 * (Stroka.АттестованноеЗначение.Value + 0.05555))), NumberRound - 1, MidpointRounding.AwayFromZero); //0,09*(Х+ 0,05555) 
                                                Al = Math.Round((double)((decimal)(0.08 * (Stroka.АттестованноеЗначение.Value + 0.05555))), NumberRound - 1, MidpointRounding.AwayFromZero); //0,08*(Х+ 0,05555)
                                            }
                                        }
                                    }
                                    break;
                                case "Вязкость":
                                    r = Math.Round((double)((decimal)(0.0035 * Stroka.АттестованноеЗначение.Value)), NumberRound + 1, MidpointRounding.AwayFromZero); //0,0035Х
                                    R = Math.Round((double)((decimal)(0.0072 * Stroka.АттестованноеЗначение.Value)), NumberRound + 1, MidpointRounding.AwayFromZero); //0,0072Х
                                    Or = Math.Round((double)((decimal)(0.00126 * Stroka.АттестованноеЗначение.Value)), NumberRound + 1, MidpointRounding.AwayFromZero); //0,126Х
                                    OR = Math.Round((double)((decimal)(0.0026 * Stroka.АттестованноеЗначение.Value)), NumberRound + 1, MidpointRounding.AwayFromZero); //0,26Х
                                    ORl = Math.Round((double)((decimal)(0.0022 * Stroka.АттестованноеЗначение.Value)), NumberRound, MidpointRounding.AwayFromZero); //0,22Х
                                    A = Math.Round((double)((decimal)(0.0051 * Stroka.АттестованноеЗначение.Value)), NumberRound, MidpointRounding.AwayFromZero); //0,51Х
                                    Al = Math.Round((double)((decimal)(0.0043 * Stroka.АттестованноеЗначение.Value)), NumberRound, MidpointRounding.AwayFromZero); //0,43Х                                    
                                    break;
                                case "Парафин":
                                    if (Stroka.АттестованноеЗначение.Value <= 1.5)
                                    {
                                        r = Math.Round((double)((decimal)(0.19 * Stroka.АттестованноеЗначение.Value)), NumberRound - 1, MidpointRounding.AwayFromZero); //0,19Х
                                        R = Math.Round((double)((decimal)(0.68 * Stroka.АттестованноеЗначение.Value)), NumberRound - 1, MidpointRounding.AwayFromZero); //0,68Х
                                        Or = Math.Round((double)((decimal)(0.069 * Stroka.АттестованноеЗначение.Value)), NumberRound, MidpointRounding.AwayFromZero); //0,069Х
                                        OR = Math.Round((double)((decimal)(0.245 * Stroka.АттестованноеЗначение.Value)), NumberRound, MidpointRounding.AwayFromZero); //0,245Х
                                        ORl = Math.Round((double)((decimal)(0.2 * Stroka.АттестованноеЗначение.Value)), NumberRound - 1, MidpointRounding.AwayFromZero); //0,2Х
                                        A = Math.Round((double)((decimal)(0.48 * Stroka.АттестованноеЗначение.Value)), NumberRound - 1, MidpointRounding.AwayFromZero); //0,48Х
                                        Al = Math.Round((double)((decimal)(0.4 * Stroka.АттестованноеЗначение.Value)), NumberRound - 1, MidpointRounding.AwayFromZero); //0,4Х
                                    }
                                    else if (Stroka.АттестованноеЗначение.Value > 1.5) //&& Stroka.АттестованноеЗначение.Value <= 6
                                    {
                                        r = Math.Round((double)((decimal)(0.18 * Stroka.АттестованноеЗначение.Value)), NumberRound - 1, MidpointRounding.AwayFromZero); //0,18Х
                                        R = Math.Round((double)((decimal)(0.68 * Stroka.АттестованноеЗначение.Value)), NumberRound - 1, MidpointRounding.AwayFromZero); //0,68Х
                                        Or = Math.Round((double)((decimal)(0.065 * Stroka.АттестованноеЗначение.Value)), NumberRound, MidpointRounding.AwayFromZero); //0,065Х
                                        OR = Math.Round((double)((decimal)(0.245 * Stroka.АттестованноеЗначение.Value)), NumberRound, MidpointRounding.AwayFromZero); //0,245Х
                                        ORl = Math.Round((double)((decimal)(0.2 * Stroka.АттестованноеЗначение.Value)), NumberRound - 1, MidpointRounding.AwayFromZero); //0,2Х
                                        A = Math.Round((double)((decimal)(0.48 * Stroka.АттестованноеЗначение.Value)), NumberRound - 1, MidpointRounding.AwayFromZero); //0,48Х
                                        Al = Math.Round((double)((decimal)(0.4 * Stroka.АттестованноеЗначение.Value)), NumberRound - 1, MidpointRounding.AwayFromZero); //0,4Х
                                    }
                                    break;
                                case "Хлорорганика":
                                    r = Math.Round((double)((decimal)(0.32 * Math.Pow((Stroka.АттестованноеЗначение.Value + 0.33), 0.644))), NumberRound - 1, MidpointRounding.AwayFromZero); //0,32(Х+0,33)^0,644
                                    R = Math.Round((double)((decimal)(0.7 * Math.Pow((Stroka.АттестованноеЗначение.Value + 0.33), 0.644))), NumberRound - 1, MidpointRounding.AwayFromZero);  //0,7(Х+0,33)^0,644
                                    Or = Math.Round((double)((decimal)(0.12 * Math.Pow((Stroka.АттестованноеЗначение.Value + 0.32), 0.644))), NumberRound, MidpointRounding.AwayFromZero); //0,12(Х+0,32)^0,644
                                    OR = Math.Round((double)((decimal)(0.25 * Math.Pow((Stroka.АттестованноеЗначение.Value + 0.32), 0.644))), NumberRound, MidpointRounding.AwayFromZero); //0,25(Х+0,32)^0,644)
                                    ORl = Math.Round((double)((decimal)(0.21 * Math.Pow((Stroka.АттестованноеЗначение.Value + 0.32), 0.644))), NumberRound - 1, MidpointRounding.AwayFromZero); //0,21(Х+0,32)^0,644
                                    A = Math.Round((double)((decimal)(0.5 * Math.Pow((Stroka.АттестованноеЗначение.Value + 0.32), 0.644))), NumberRound - 1, MidpointRounding.AwayFromZero); //0,5(Х+0,32)^0,644
                                    Al = Math.Round((double)((decimal)(0.42 * Math.Pow((Stroka.АттестованноеЗначение.Value + 0.32), 0.644))), NumberRound - 1, MidpointRounding.AwayFromZero); //0,42(Х+0,32)^0,644

                                    break;
                            }
                        }
                        Povt1 = Math.Round((double)((decimal)(3.686 * Or)), NumberRound, MidpointRounding.AwayFromZero);
                        Povt2 = Math.Round((double)((decimal)(2.834 * Or)), NumberRound, MidpointRounding.AwayFromZero);
                        Povt3 = Math.Round((double)((decimal)(1.128 * Or)), NumberRound, MidpointRounding.AwayFromZero);
                        Prech1 = Math.Round((double)((decimal)((3.686 * ORl))), NumberRound, MidpointRounding.AwayFromZero);
                        Prech2 = Math.Round((double)((decimal)(2.834 * ORl)), NumberRound, MidpointRounding.AwayFromZero);
                        Prech3 = Math.Round((double)((decimal)(1.128 * ORl)), NumberRound, MidpointRounding.AwayFromZero);

                        Pogr1 = Math.Round((double)((decimal)(Al)), NumberRound, MidpointRounding.AwayFromZero);
                        Pogr2 = Math.Round((double)((decimal)(1.5 * Al)), NumberRound, MidpointRounding.AwayFromZero);
                        //для протокола
                        Acl = Math.Round((double)((decimal)(0.84 * 1.3 * ORl)), NumberRound, MidpointRounding.AwayFromZero);
                        db.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Что то пошло не так!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static int GetRoundNumber(double value)
        {
            try
            {
                int i = 0;
                while (Math.Round(value, i) != value)
                {
                    i++;
                }
                return i;
            }
            catch
            {
                return 0;
            }
        }

        public static string AddZero(string Text, int RoundNumber)
        {
            var Arr = Text.Split(',');
            if (Arr.Length < 2)
            {
                if (Text.Length == 0)
                {
                    Text = "0,";
                }
                else Text = Text + ",";
                for (int i = 0; i < RoundNumber; i++)
                {
                    Text = Text + "0";
                }
            }
            else
            {
                for (int i = Arr[1].Length; i < RoundNumber; i++)
                {
                    Text = Text + "0";
                }
            }
            return Text;
        }

        public static string AddZero(double Value, int RoundNumber)
        {
            return AddZero(Value.ToString(), RoundNumber);
        }

        public static string AddZero(string Text)
        {
            var Arr = Text.Split(',');
            if (Arr.Length < 2)
            {
                if (Text.Length == 0)
                {
                    Text = "0,";
                }
                else Text = Text + ",";
                for (int i = 0; i < NumberRound; i++)
                {
                    Text = Text + "0";
                }
            }
            else
            {
                for (int i = Arr[1].Length; i < NumberRound; i++)
                {
                    Text = Text + "0";
                }
            }
            return Text;
        }

        public static string AddZero(double Value)
        {
            return AddZero(Value.ToString());
        }
    }
}
