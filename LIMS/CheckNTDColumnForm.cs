﻿using System;
using System.Windows.Forms;

namespace LIMS
{
    public partial class CheckNTDColumnForm : DevExpress.XtraEditors.XtraForm
    {
        NTDTab RT;
        public CheckNTDColumnForm(MainForm MF, NTDTab NTDTab)
        {
            InitializeComponent();
            if (UserSettings.NTDColumns != null)
            {
                var CheckColumn = UserSettings.NTDColumns.Split(',');
                CheckEditUroven.Checked = CheckColumn[0] == "1";
                CheckEditGroup.Checked = CheckColumn[1] == "1";
                CheckEditN.Checked = CheckColumn[2] == "1";
                CheckEditND.Checked = CheckColumn[3] == "1";
                CheckEditName.Checked = CheckColumn[4] == "1";
                CheckEditRegistN.Checked = CheckColumn[5] == "1";
                CheckEditDateUtverch.Checked = CheckColumn[6] == "1";
                CheckEditKontroKolvo.Checked = CheckColumn[7] == "1";
                CheckEditKontrolMesto.Checked = CheckColumn[8] == "1";
                CheckEditRabKolvo.Checked = CheckColumn[9] == "1";
                CheckEditRabMesto.Checked = CheckColumn[10] == "1";
                CheckEditDateActualiz.Checked = CheckColumn[11] == "1";
                CheckEditSrokDeistvia.Checked = CheckColumn[12] == "1";
                CheckEditPrimechanie.Checked = CheckColumn[13] == "1";
            }
            RT = NTDTab;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            string CheckString = "";
            if (CheckEditUroven.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditGroup.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditND.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditName.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditRegistN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDateUtverch.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditKontroKolvo.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditKontrolMesto.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditRabKolvo.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditRabMesto.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDateActualiz.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditSrokDeistvia.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditPrimechanie.Checked == true) { CheckString = CheckString + "1"; } else { CheckString = CheckString + "0"; }
            MainForm main = this.Owner as MainForm;
            if (main != null)
            {
                UserSettings.NTDColumns = CheckString;
                UserSettings.SaveSettings();
                RT.GetNTD(main);
            }
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CheckEditUroven_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void CheckNTDColumnForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
