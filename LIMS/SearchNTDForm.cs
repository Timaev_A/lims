﻿using System;
using System.Windows.Forms;

namespace LIMS
{
    public partial class SearchNTDForm : DevExpress.XtraEditors.XtraForm
    {
        MainForm Main;
        public SearchNTDForm(MainForm MF)
        {
            InitializeComponent();
            Main = MF;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {

            //if (CheckEditUroven.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditGroup.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditND.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditName.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditRegistN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditDateUtverch.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditKontroKolvo .Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }          
            //if (CheckEditKontrolMesto.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditRabKolvo.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditRabMesto.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditDateActualiz.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditSrokDeistvia.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            //if (CheckEditPrimechanie.Checked == true) { CheckString = CheckString + "1"; } else { CheckString = CheckString + "0"; }
            //MainForm main = this.Owner as MainForm;
            //if (main != null)
            //{
            //    UserSettings.NTDColumns = CheckString;
            //    UserSettings.SaveSettings();
            //    RT.GetNTD(main);
            //}
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SearchNTDForm_Load(object sender, EventArgs e)
        {

        }

        private void CheckEditUroven_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditUroven.Checked)
            {
                Uroven.Enabled = true;
            }
            else
            {
                Uroven.Enabled = false;
            }
        }

        private void CheckEditGroup_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditGroup.Checked)
            {
                Group.Enabled = true;
            }
            else
            {
                Group.Enabled = false;
            }
        }

        private void CheckEditN_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditN.Checked)
            {
                N.Enabled = true;
            }
            else
            {
                N.Enabled = false;
            }
        }

        private void CheckEditND_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditND.Checked)
            {
                ND.Enabled = true;
            }
            else
            {
                ND.Enabled = false;
            }
        }

        private void CheckEditName_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditName.Checked)
            {
                NTDName.Enabled = true;
            }
            else
            {
                NTDName.Enabled = false;
            }
        }

        private void CheckEditRegistN_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditRegistN.Checked)
            {
                RegistN.Enabled = true;
            }
            else
            {
                RegistN.Enabled = false;
            }
        }

        private void CheckEditDateUtverch_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditDateUtverch.Checked)
            {
                DateUtvergdenia1.Enabled = true;
                DateUtvergdenia2.Enabled = true;
            }
            else
            {
                DateUtvergdenia1.Enabled = false;
                DateUtvergdenia2.Enabled = false;
            }
        }

        private void CheckEditKontroKolvo_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditKontroKolvo.Checked)
            {
                KolvoKontrol.Enabled = true;
            }
            else
            {
                KolvoKontrol.Enabled = false;
            }
        }

        private void CheckEditKontrolMesto_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditKontrolMesto.Checked)
            {
                MestoKontrol.Enabled = true;
            }
            else
            {
                MestoKontrol.Enabled = false;
            }
        }

        private void CheckEditRabKolvo_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditRabKolvo.Checked)
            {
                KolvoRab.Enabled = true;
            }
            else
            {
                KolvoRab.Enabled = false;
            }
        }

        private void CheckEditRabMesto_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditRabMesto.Checked)
            {
                MestoRab.Enabled = true;
            }
            else
            {
                MestoRab.Enabled = false;
            }
        }

        private void CheckEditDateActualiz_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditDateActualiz.Checked)
            {
                DateAktualizasii1.Enabled = true;
                DateAktualizasii2.Enabled = true;
            }
            else
            {
                DateAktualizasii1.Enabled = false;
                DateAktualizasii2.Enabled = false;
            }
        }

        private void CheckEditSrokDeistvia_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditSrokDeistvia.Checked)
            {
                SrokDeistvia1.Enabled = true;
                SrokDeistvia2.Enabled = true;
            }
            else
            {
                SrokDeistvia1.Enabled = false;
                SrokDeistvia2.Enabled = false;
            }
        }

        private void CheckEditPrimechanie_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckEditPrimechanie.Checked)
            {
                Primechanie.Enabled = true;
            }
            else
            {
                Primechanie.Enabled = false;
            }
        }

        private void CheckEditUroven_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void SearchNTDForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
