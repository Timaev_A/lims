﻿namespace LIMS
{
    partial class ReactivUchetReactivovForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReactivUchetReactivovForm));
            this.ReactivUchetGrid = new DevExpress.XtraGrid.GridControl();
            this.ReactivUchetGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Loader = new DevExpress.XtraWaitForm.ProgressPanel();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivUchetGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivUchetGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ReactivUchetGrid
            // 
            this.ReactivUchetGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReactivUchetGrid.Location = new System.Drawing.Point(0, 0);
            this.ReactivUchetGrid.MainView = this.ReactivUchetGridView;
            this.ReactivUchetGrid.Name = "ReactivUchetGrid";
            this.ReactivUchetGrid.Size = new System.Drawing.Size(775, 550);
            this.ReactivUchetGrid.TabIndex = 1;
            this.ReactivUchetGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ReactivUchetGridView});
            this.ReactivUchetGrid.DoubleClick += new System.EventHandler(this.ReactivAktGrid_DoubleClick);
            // 
            // ReactivUchetGridView
            // 
            this.ReactivUchetGridView.Appearance.ColumnFilterButton.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivUchetGridView.Appearance.ColumnFilterButton.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivUchetGridView.Appearance.ColumnFilterButton.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivUchetGridView.Appearance.FixedLine.Options.UseTextOptions = true;
            this.ReactivUchetGridView.Appearance.FixedLine.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivUchetGridView.Appearance.FixedLine.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivUchetGridView.Appearance.FixedLine.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivUchetGridView.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.ReactivUchetGridView.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivUchetGridView.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivUchetGridView.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivUchetGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.ReactivUchetGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivUchetGridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivUchetGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivUchetGridView.Appearance.Row.Options.UseTextOptions = true;
            this.ReactivUchetGridView.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivUchetGridView.Appearance.Row.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivUchetGridView.Appearance.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivUchetGridView.Appearance.VertLine.Options.UseTextOptions = true;
            this.ReactivUchetGridView.Appearance.VertLine.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivUchetGridView.Appearance.VertLine.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivUchetGridView.Appearance.VertLine.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivUchetGridView.Appearance.ViewCaption.Options.UseTextOptions = true;
            this.ReactivUchetGridView.Appearance.ViewCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivUchetGridView.Appearance.ViewCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivUchetGridView.Appearance.ViewCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivUchetGridView.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.ReactivUchetGridView.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivUchetGridView.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivUchetGridView.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivUchetGridView.AppearancePrint.Row.Options.UseTextOptions = true;
            this.ReactivUchetGridView.AppearancePrint.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivUchetGridView.AppearancePrint.Row.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivUchetGridView.AppearancePrint.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivUchetGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.ReactivUchetGridView.GridControl = this.ReactivUchetGrid;
            this.ReactivUchetGridView.Name = "ReactivUchetGridView";
            this.ReactivUchetGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.ReactivUchetGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.ReactivUchetGridView.OptionsBehavior.AllowIncrementalSearch = true;
            this.ReactivUchetGridView.OptionsBehavior.Editable = false;
            this.ReactivUchetGridView.OptionsFind.AlwaysVisible = true;
            this.ReactivUchetGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.ReactivUchetGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.ReactivUchetGridView.OptionsView.RowAutoHeight = true;
            this.ReactivUchetGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.ReactivAktGridView_PopupMenuShowing);
            this.ReactivUchetGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.ReactivUchetGridView_FocusedRowChanged);
            // 
            // Loader
            // 
            this.Loader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Loader.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Loader.Appearance.Options.UseBackColor = true;
            this.Loader.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Loader.AppearanceCaption.Options.UseFont = true;
            this.Loader.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Loader.AppearanceDescription.Options.UseFont = true;
            this.Loader.BarAnimationElementThickness = 2;
            this.Loader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.Loader.Caption = "Подождите";
            this.Loader.Description = "Формирование документа ...";
            this.Loader.Location = new System.Drawing.Point(264, 242);
            this.Loader.Name = "Loader";
            this.Loader.Size = new System.Drawing.Size(246, 66);
            this.Loader.TabIndex = 3;
            this.Loader.Visible = false;
            // 
            // ReactivUchetReactivovForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 550);
            this.Controls.Add(this.Loader);
            this.Controls.Add(this.ReactivUchetGrid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "ReactivUchetReactivovForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Журнал учета хим. реактивов";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ReactivListAktVhodnogoKontrolaForm_FormClosed);
            this.Load += new System.EventHandler(this.ReactivListAktVhodnogoKontrolaForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivUchetReactivovForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ReactivUchetGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivUchetGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraGrid.GridControl ReactivUchetGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView ReactivUchetGridView;
        public DevExpress.XtraWaitForm.ProgressPanel Loader;
    }
}