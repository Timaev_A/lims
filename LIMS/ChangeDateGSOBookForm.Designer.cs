﻿namespace LIMS
{
    partial class ChangeDateGSOBookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeDateGSOBookForm));
            this.CancelClickButton = new DevExpress.XtraEditors.SimpleButton();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.label2 = new System.Windows.Forms.Label();
            this.AnalizName = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ClearDateButton = new System.Windows.Forms.RadioButton();
            this.ChangeDateButton = new System.Windows.Forms.RadioButton();
            this.EndDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DateCreate = new System.Windows.Forms.DateTimePicker();
            this.N = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CancelClickButton
            // 
            this.CancelClickButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelClickButton.Location = new System.Drawing.Point(151, 176);
            this.CancelClickButton.Name = "CancelClickButton";
            this.CancelClickButton.Size = new System.Drawing.Size(75, 26);
            this.CancelClickButton.TabIndex = 6;
            this.CancelClickButton.Text = "Отмена";
            this.CancelClickButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(62, 176);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 26);
            this.OKButton.TabIndex = 5;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.AnalizName);
            this.panelControl1.Controls.Add(this.groupBox1);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.DateCreate);
            this.panelControl1.Controls.Add(this.N);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelClickButton);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(278, 214);
            this.panelControl1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Метод анализа:";
            // 
            // AnalizName
            // 
            this.AnalizName.AutoSize = true;
            this.AnalizName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AnalizName.Location = new System.Drawing.Point(108, 17);
            this.AnalizName.Name = "AnalizName";
            this.AnalizName.Size = new System.Drawing.Size(0, 13);
            this.AnalizName.TabIndex = 33;
            this.AnalizName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ClearDateButton);
            this.groupBox1.Controls.Add(this.ChangeDateButton);
            this.groupBox1.Controls.Add(this.EndDate);
            this.groupBox1.Location = new System.Drawing.Point(12, 94);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(254, 76);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Дата закрытия";
            // 
            // ClearDateButton
            // 
            this.ClearDateButton.AutoSize = true;
            this.ClearDateButton.Checked = true;
            this.ClearDateButton.Location = new System.Drawing.Point(13, 21);
            this.ClearDateButton.Name = "ClearDateButton";
            this.ClearDateButton.Size = new System.Drawing.Size(74, 17);
            this.ClearDateButton.TabIndex = 2;
            this.ClearDateButton.TabStop = true;
            this.ClearDateButton.Text = "Очистить";
            this.ClearDateButton.UseVisualStyleBackColor = true;
            this.ClearDateButton.CheckedChanged += new System.EventHandler(this.ClearDateButton_CheckedChanged);
            this.ClearDateButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCreate_KeyDown);
            // 
            // ChangeDateButton
            // 
            this.ChangeDateButton.AutoSize = true;
            this.ChangeDateButton.Location = new System.Drawing.Point(13, 44);
            this.ChangeDateButton.Name = "ChangeDateButton";
            this.ChangeDateButton.Size = new System.Drawing.Size(126, 17);
            this.ChangeDateButton.TabIndex = 3;
            this.ChangeDateButton.Text = "Добавить/изменить";
            this.ChangeDateButton.UseVisualStyleBackColor = true;
            this.ChangeDateButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCreate_KeyDown);
            // 
            // EndDate
            // 
            this.EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.EndDate.Location = new System.Drawing.Point(145, 43);
            this.EndDate.Name = "EndDate";
            this.EndDate.Size = new System.Drawing.Size(96, 21);
            this.EndDate.TabIndex = 4;
            this.EndDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCreate_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "№ п/п:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Дата создания:";
            // 
            // DateCreate
            // 
            this.DateCreate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateCreate.Location = new System.Drawing.Point(108, 69);
            this.DateCreate.Name = "DateCreate";
            this.DateCreate.Size = new System.Drawing.Size(98, 21);
            this.DateCreate.TabIndex = 1;
            this.DateCreate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCreate_KeyDown);
            // 
            // N
            // 
            this.N.AutoSize = true;
            this.N.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.N.Location = new System.Drawing.Point(108, 44);
            this.N.Name = "N";
            this.N.Size = new System.Drawing.Size(14, 13);
            this.N.TabIndex = 15;
            this.N.Text = "N";
            this.N.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ChangeDateGSOBookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelClickButton;
            this.ClientSize = new System.Drawing.Size(278, 214);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangeDateGSOBookForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Изменение даты";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChangeDateGSOBookForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton CancelClickButton;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Label N;
        private System.Windows.Forms.DateTimePicker EndDate;
        private System.Windows.Forms.DateTimePicker DateCreate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton ChangeDateButton;
        private System.Windows.Forms.RadioButton ClearDateButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label AnalizName;
    }
}