﻿using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using System;
using System.ComponentModel;
using System.Data.Linq;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace LIMS
{
    //статусы
    #region
    //история изменений
    // 0  - Текущая версия
    //100 - Создание
    //200 - Редактирование
    //300 - Удаление
    //400 - Из архива
    //401 - В архив
    #endregion
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        DevExpress.LookAndFeel.DefaultLookAndFeel DESkin = new DevExpress.LookAndFeel.DefaultLookAndFeel();
        public static ReactivTab ReactivTab = new ReactivTab();
        public static PersonalTab PersonalTab = new PersonalTab();
        public static NTDTab NTDTab = new NTDTab();
        public static VOTab VOTab = new VOTab();
        public static IOTab IOTab = new IOTab();
        public static SITab SITab = new SITab();
        public static VLKTab VLKTab = new VLKTab();
        WordDocument WordDoc1;
        public MainForm()
        {
            InitializeComponent();
            //ChangeText();
            UserSettings.Skin = RW_XML.ReadValueOfProperty(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS\\config.xml", "Skin");            
            if(RW_XML.ReadValueOfProperty(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS\\config.xml", "UseSkins") == "true") DESkin.LookAndFeel.SetSkinStyle(UserSettings.Skin);
            LoginForm f = new LoginForm();
            f.ShowDialog();
            f.Dispose();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Visible = false;
            Text = "АРМЛАБ - " + UserSettings.User;
            WindowState = FormWindowState.Maximized;
            splitContainerControl6.SplitterPosition = UserSettings.ReactivSplitWidth.HasValue ? UserSettings.ReactivSplitWidth.Value : 313;
            splitContainerControl1.SplitterPosition = UserSettings.PersonalSplitWidth.HasValue ? UserSettings.PersonalSplitWidth.Value : 304;
            splitContainerControl2.SplitterPosition = UserSettings.NTDSplitWidth.HasValue ? UserSettings.NTDSplitWidth.Value : 336;
            splitContainerControl4.SplitterPosition = UserSettings.SISplitWidth.HasValue ? UserSettings.SISplitWidth.Value : 360;
            splitContainerControl3.SplitterPosition = UserSettings.IOSplitWidth.HasValue ? UserSettings.IOSplitWidth.Value : 369;
            splitContainerControl5.SplitterPosition = UserSettings.VOSplitWidth.HasValue ? UserSettings.VOSplitWidth.Value : 381;
            
            if (UserSettings.Skin != "" && UserSettings.UseSkinsCheckBox && "timaev_as" != Dns.GetHostName())
            {
                DESkin.LookAndFeel.SetSkinStyle(UserSettings.Skin);
            }
            else
            {
                DevExpress.Skins.SkinManager.DisableFormSkins();
                DESkin.LookAndFeel.SetDefaultStyle();
            }
            if (UserSettings.VisibleTabs != "")
            {
                var Tabs = UserSettings.VisibleTabs.Split(',');
                bool UpdateDate = true;
                for (int i = 0; i < 8; i++)
                {
                    if (Tabs[i] == "0")
                    {
                        switch (i)
                        {
                            case 0:
                                TabControlMain.TabPages.Remove(Reactiv);
                                break;
                            case 1:
                                TabControlMain.TabPages.Remove(Personal);
                                break;
                            case 2:
                                TabControlMain.TabPages.Remove(NTD);
                                break;
                            case 3:
                                TabControlMain.TabPages.Remove(SI);
                                break;
                            case 4:
                                TabControlMain.TabPages.Remove(IO);
                                break;
                            case 5:
                                TabControlMain.TabPages.Remove(VO);
                                break;
                            case 6:
                                TabControlMain.TabPages.Remove(VLK);
                                break;
                            case 7:
                                TabControlMain.TabPages.Remove(Nastroiki);
                                break;
                        }
                    }
                    else if (UpdateDate)
                    {
                        switch (i)
                        {
                            case 0:
                                ReactivTab.GetReactiv(this);
                                UpdateDate = false;
                                break;
                            case 1:
                                PersonalTab.GetPersonal(this);
                                UpdateDate = false;
                                break;
                            case 2:
                                NTDTab.GetNTD(this);
                                UpdateDate = false;
                                break;
                            case 3:
                                SITab.GetSI(this);
                                UpdateDate = false;
                                break;
                            case 4:
                                IOTab.GetIO(this);
                                UpdateDate = false;
                                break;
                            case 5:
                                VOTab.GetVO(this);
                                UpdateDate = false;
                                break;
                            case 6:
                                VLKTab.UpdateGSOGrid(this);
                                UpdateDate = false;
                                break;
                        }
                    }
                }
            }
            ContextMenuEvent();
            AddKeyDownEvents();
            SetPrava();
            Visible = true;
            if (UserSettings.Alerts) new Thread(Proverka).Start();
            new Thread(PerechetVLK).Start();
            new Thread(PersonalTab.UpdateOpit).Start();
            new Thread(FiltrVLKProbiClass.LoadFiltr).Start();
            //ChangeText();            
        }

        private void AddKeyDownEvents()
        {
            foreach (Control C in ReactivPanel.Controls)
            {
                if (C is TextBox || C is ComboBox || C is DateTimePicker || C is NumericUpDown)
                {
                    C.KeyDown += Reactiv_KeyDown;
                }
            }
            foreach (Control C in PersonalPanel.Controls)
            {
                if (C is TextBox || C is ComboBox || C is DateTimePicker || C is NumericUpDown)
                {
                    C.KeyDown += Personal_KeyDown;
                }
            }
            foreach (Control C in NTDPanel.Controls)
            {
                if (C is TextBox || C is ComboBox || C is DateTimePicker || C is NumericUpDown)
                {
                    C.KeyDown += NTD_KeyDown;
                }
            }
            foreach (Control C in IOPanel.Controls)
            {
                if (C is TextBox || C is ComboBox || C is DateTimePicker || C is NumericUpDown)
                {
                    C.KeyDown += IO_KeyDown;
                }
            }
            foreach (Control C in VOPanel.Controls)
            {
                if (C is TextBox || C is ComboBox || C is DateTimePicker || C is NumericUpDown)
                {
                    C.KeyDown += VO_KeyDown;
                }
            }
            foreach (Control C in SIPanel.Controls)
            {
                if (C is TextBox || C is ComboBox || C is DateTimePicker || C is NumericUpDown)
                {
                    C.KeyDown += SI_KeyDown;
                }
            }
        }

        private void Reactiv_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ReactivSaveButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }
        private void Personal_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    PersonalSaveButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void NTD_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    NTDSaveButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void IO_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    IOSaveButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void VO_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    VOSaveButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void SI_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    SISaveButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (UserSettings.User == "Developer")
            {
                try
                {
                    using (DbDataContext db = new DbDataContext())
                    {
                        var Dev = db.Users.Where(c => c.UserName == "Developer").FirstOrDefault();
                        if (Dev != null)
                        {
                            db.Users.DeleteOnSubmit(Dev);
                        }
                        var DevS = db.NewSettings.Where(c => c.UserName == "Developer").FirstOrDefault();
                        if (DevS != null)
                        {
                            db.NewSettings.DeleteOnSubmit(DevS);
                        }
                        db.SubmitChanges();
                    }
                }
                catch
                {
                }
            }
            RW_XML.AddToFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS\\config.xml", new SerializerClass { property = "Skin", value = UserSettings.Skin });
            if (UseSkinsCheckBox.Checked)                
                RW_XML.AddToFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS\\config.xml", new SerializerClass { property = "UseSkins", value = "true" });            
            else           
                RW_XML.AddToFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS\\config.xml", new SerializerClass { property = "UseSkins", value = "false" });
                
            Process.GetCurrentProcess().Kill();
        }

        private void SetPrava()
        {
            ReactivDeleteButton.Visible = UserSettings.ReactivDelete;
            ReactivPrintButton.Visible = UserSettings.ReactivPrint;
            ReactivCopyButton.Visible = UserSettings.ReactivAdd;
            ReactivNewButton.Visible = UserSettings.ReactivAdd;
            ReactivEditCheckBox.Visible = UserSettings.ReactivEdit;
            ReactivSaveButton.Visible = UserSettings.ReactivAdd || UserSettings.ReactivEdit;

            PersonalDeleteButton.Visible = UserSettings.PersonalDelete;
            PersonalPrintButton.Visible = UserSettings.PersonalPrint;
            PersonalNewButton.Visible = UserSettings.PersonalAdd;
            PersonalCopyButton.Visible = UserSettings.PersonalAdd;
            PersonalSaveButton.Visible = UserSettings.PersonalAdd || UserSettings.PersonalEdit;
            PersonalEditCheckBox.Visible = UserSettings.PersonalEdit;

            NTDDeleteButton.Visible = UserSettings.NTDDelete;
            NTDPrintButton.Visible = UserSettings.NTDPrint;
            NTDNewButton.Visible = UserSettings.NTDAdd;
            NTDCopyButton.Visible = UserSettings.NTDAdd;
            NTDSaveButton.Visible = UserSettings.NTDAdd || UserSettings.NTDEdit;
            NTDEditCheckBox.Visible = UserSettings.NTDEdit;

            IODeleteButton.Visible = UserSettings.IODelete;
            IOPrintButton.Visible = UserSettings.IOPrint;
            IONewButton.Visible = UserSettings.IOAdd;
            IOCopyButton.Visible = UserSettings.IOAdd;
            IOSaveButton.Visible = UserSettings.IOAdd || UserSettings.IOEdit;
            IOEditCheckBox.Visible = UserSettings.IOEdit;

            VODeleteButton.Visible = UserSettings.VODelete;
            VOPrintButton.Visible = UserSettings.VOPrint;
            VONewButton.Visible = UserSettings.VOAdd;
            VOCopyButton.Visible = UserSettings.VOAdd;
            VOSaveButton.Visible = UserSettings.VOAdd || UserSettings.VOEdit;
            VOEditCheckBox.Visible = UserSettings.VOEdit;

            SIDeleteButton.Visible = UserSettings.SIDelete;
            SIPrintButton.Visible = UserSettings.SIPrint;
            SINewButton.Visible = UserSettings.SIAdd;
            SICopyButton.Visible = UserSettings.SIAdd;
            SISaveButton.Visible = UserSettings.SIAdd || UserSettings.SIEdit;
            SIEditCheckBox.Visible = UserSettings.SIEdit;

            ReactivCellMergeButton.Image = UserSettings.ReactivCellMerge ? global::LIMS.Properties.Resources.freezetoprow_32x32 : global::LIMS.Properties.Resources.gridmergecells_32x32;
            PersonalCellMergeButton.Image = UserSettings.PersonalCellMerge ? global::LIMS.Properties.Resources.freezetoprow_32x32 : global::LIMS.Properties.Resources.gridmergecells_32x32;
            NTDCellMergeButton.Image = UserSettings.NTDCellMerge ? global::LIMS.Properties.Resources.freezetoprow_32x32 : global::LIMS.Properties.Resources.gridmergecells_32x32;
            SICellMergeButton.Image = UserSettings.SICellMerge ? global::LIMS.Properties.Resources.freezetoprow_32x32 : global::LIMS.Properties.Resources.gridmergecells_32x32;
            VOCellMergeButton.Image = UserSettings.VOCellMerge ? global::LIMS.Properties.Resources.freezetoprow_32x32 : global::LIMS.Properties.Resources.gridmergecells_32x32;
            IOCellMergeButton.Image = UserSettings.IOCellMerge ? global::LIMS.Properties.Resources.freezetoprow_32x32 : global::LIMS.Properties.Resources.gridmergecells_32x32;
        }

        void ChangeText()
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var ВсеЗначения = db.Probi.Where(c => c.Шифр.Contains("ДНП")).ToList();
                    
                    foreach (Probi Cel in ВсеЗначения)
                    {
                        try
                        {
                            
                        }
                        catch { }
                    }                    
                    db.SubmitChanges();
                }
            }
            catch
            { }
        }

        private void ContextMenuEvent()
        {
            foreach (Control C in ReactivPanel.Controls)
            {
                if (C is TextBox || C is ComboBox)
                {
                    C.ContextMenuStrip = ContextMenuStripMain;
                }
            }
            foreach (Control C in PersonalPanel.Controls)
            {
                if (C is TextBox || C is ComboBox)
                {
                    C.ContextMenuStrip = ContextMenuStripMain;
                }
            }
            foreach (Control C in NTDPanel.Controls)
            {
                if (C is TextBox || C is ComboBox)
                {
                    C.ContextMenuStrip = ContextMenuStripMain;
                }
            }
            foreach (Control C in IOPanel.Controls)
            {
                if (C is TextBox || C is ComboBox)
                {
                    C.ContextMenuStrip = ContextMenuStripMain;
                }
            }
            foreach (Control C in VOPanel.Controls)
            {
                if (C is TextBox || C is ComboBox)
                {
                    C.ContextMenuStrip = ContextMenuStripMain;
                }
            }
            foreach (Control C in SIPanel.Controls)
            {
                if (C is TextBox || C is ComboBox)
                {
                    C.ContextMenuStrip = ContextMenuStripMain;
                }
            }
        }
        private void PerechetVLK()
        {
            Thread.Sleep(6000);
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    try
                    {
                        var Stabilnosti = db.KontrolStabilnosti.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1).ToList();
                        foreach (var strike in Stabilnosti)
                        {
                            var Probi = db.Probi.Where(c => c.AnalizID == strike.AnalizID && c.KontrolStabilnostiID == strike.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                            if (Probi != null) strike.Колво = Probi.Count;
                        }
                        var Operativnii = db.OperativniiKontrol.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1).ToList();
                        foreach (var strike in Operativnii)
                        {
                            var Probi = db.Probi.Where(c => c.AnalizID == strike.AnalizID && c.OperativniiKontrolID == strike.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                            if (Probi != null) strike.Колво = Probi.Count;
                        }
                        db.SubmitChanges();
                    }
                    catch (Exception ex)
                    {
                        db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                        {
                            try
                            {
                                var Stabilnosti = db.KontrolStabilnosti.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1).ToList();
                                foreach (var strike in Stabilnosti)
                                {
                                    var Probi = db.Probi.Where(c => c.AnalizID == strike.AnalizID && c.KontrolStabilnostiID == strike.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                                    if (Probi != null) strike.Колво = Probi.Count;
                                }
                                var Operativnii = db.OperativniiKontrol.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1).ToList();
                                foreach (var strike in Operativnii)
                                {
                                    var Probi = db.Probi.Where(c => c.AnalizID == strike.AnalizID && c.OperativniiKontrolID == strike.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                                    if (Probi != null) strike.Колво = Probi.Count;
                                }
                                db.SubmitChanges();
                            }
                            catch (ChangeConflictException)
                            {
                                LogErrors Log2 = new LogErrors(ex.ToString());
                                //MessageBox.Show("Конфликт при сохранении!\nПопробуйте повторить операцию.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);          
                                return;
                            }
                        }
                        LogErrors Log = new LogErrors(ex.ToString());
                    }
            db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void TabControlMain_Click(object sender, EventArgs e)
        {
            try
            {
                switch (TabControlMain.SelectedTabPage.Name)
                {
                    case "Reactiv":
                        ReactivTab.GetReactiv(this);
                        break;
                    case "Personal":
                        PersonalTab.GetPersonal(this);
                        break;
                    case "NTD":
                        NTDTab.GetNTD(this);
                        break;
                    case "SI":
                        SITab.GetSI(this);
                        break;
                    case "IO":
                        IOTab.GetIO(this);
                        break;
                    case "VO":
                        VOTab.GetVO(this);
                        break;
                    case "VLK":
                        VLKTab.UpdateGSOGrid(this);
                        break;
                    case "Nastroiki":
                        AddUser.Visible = UserSettings.Admin;
                        UseSkinsCheckBox.Checked = UserSettings.UseSkinsCheckBox;
                        SkinComboBox.Text = UserSettings.Skin;
                        DevExpress.Skins.SkinContainerCollection skins = DevExpress.Skins.SkinManager.Default.Skins;
                        for (int i = 0; i < skins.Count; i++)
                        {
                            SkinComboBox.Items.Add(skins[i].SkinName);
                        }
                        break;
                }
            }
            catch { }
        }

        //Reactiv 
        #region
        private void ReactivCellMergeButton_Click(object sender, EventArgs e)
        {
            ReactivTab.ReactivCellMergeButton_Click(this);
        }
        private void splitContainerControl6_Panel1_SizeChanged(object sender, EventArgs e)
        {
            ReactivTab.splitContainerControl6_Panel1_SizeChanged(sender);
        }
        private void ReactivGridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            GridView view = sender as GridView;
            DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (Reactiv)gv.GetRow(gv.FocusedRowHandle);
            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
            {
                int rowHandle = e.HitInfo.RowHandle;
                e.Menu.Items.Clear();
                if (UserSettings.ReactivPrint)
                {
                    DXMenuCheckItem checkItem10 = new DXMenuCheckItem("&Печать...",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivPrint));
                    checkItem10.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem10);
                }
                if (UserSettings.ReactivVhodnoiKontrol)
                {
                    DXMenuCheckItem checkItem1 = new DXMenuCheckItem("&Создать \"Акт входного контроля\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(OnReactivPrintAktVhodnogoKontrolaRowClick));
                    checkItem1.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem1);
                }
                if (UserSettings.ReactivProverkaPrigodnosti)
                {
                    DXMenuCheckItem checkItem2 = new DXMenuCheckItem("&Создать \"Акт проверки пригодности\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(OnReactivPrintAktProverkiPrigodnostiRowClick));
                    checkItem2.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem2);
                }
                if (UserSettings.ReactivVhodnoiKontrol)
                {
                    DXMenuCheckItem checkItem6 = new DXMenuCheckItem("&Акты входного контроля",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivWiewAktVhodnogoKontrolaClick));
                    checkItem6.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem6);
                }
                if (UserSettings.ReactivProverkaPrigodnosti)
                {
                    DXMenuCheckItem checkItem7 = new DXMenuCheckItem("&Акты проверки пригодности",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivWiewAktProverkiPrigodnostiClick));
                    checkItem7.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem7);
                }
                if (UserSettings.ReactivUschet)
                {
                    DXMenuCheckItem checkItem33 = new DXMenuCheckItem("&Журнал учета хим. реактивов",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivUcet_Click));
                    checkItem33.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem33);
                }
                if (UserSettings.ReactivVhodnoiKontrol)
                {
                    DXMenuCheckItem checkItem8 = new DXMenuCheckItem("&Акты входного контроля для реактива \"" + Stroke.НаименованиеРеактива + "\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivWiewAktVhodnogoKontrolaClickOneReactiv));
                    checkItem8.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem8);
                }
                if (UserSettings.ReactivProverkaPrigodnosti)
                {
                    DXMenuCheckItem checkItem12 = new DXMenuCheckItem("&Акты проверки пригодности для реактива \"" + Stroke.НаименованиеРеактива + "\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivWiewAktProverkiPrigodnostiClickOneReactiv));
                    checkItem12.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem12);
                }

                if (UserSettings.ReactivUschet)
                {
                    DXMenuCheckItem checkItem34 = new DXMenuCheckItem("&Журнал учета хим. реактива \"" + Stroke.НаименованиеРеактива + "\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivOneUcet_Click));
                    checkItem34.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem34);
                }
                DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Подогнать по ширине окна вкл/выкл",
                view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivColumnAvtoWight_Click));
                checkItem5.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem5);
                if (UserSettings.ReactivRashod)
                {
                    DXMenuCheckItem checkItem13 = new DXMenuCheckItem("&Расход",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivRashodButton_Click));
                    checkItem13.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem13);
                    DXMenuCheckItem checkItem25 = new DXMenuCheckItem("&Приход",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivPrihodButton_Click));
                    checkItem25.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem25);
                }
                if (UserSettings.ReactivSpisat)
                {
                    if (Stroke.VisibleStatus == 1)
                    {
                        DXMenuCheckItem checkItem26 = new DXMenuCheckItem("&Отменить списание",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivSpisatButton_Click));
                        checkItem26.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem26);
                    }
                    else
                    {
                        DXMenuCheckItem checkItem23 = new DXMenuCheckItem("&Списать",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivSpisatButton_Click));
                        checkItem23.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem23);
                    }
                }
                if (UserSettings.ReactivDelete)
                {
                    DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivDeleteButton_Click));
                    checkItem3.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem3);
                }
                DXMenuCheckItem checkItem4 = new DXMenuCheckItem("&Обновить",
                view.OptionsView.AllowCellMerge, null, new EventHandler(RefreshReactivButton_Click));
                checkItem4.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem4);
            }
        }
        private void ReactivOneUcet_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (Reactiv)gv.GetRow(gv.FocusedRowHandle);
            ReactivTab.ReactivUchetClick(this, Stroke.Id);
        }
        private void ReactivUcet_Click(object sender, EventArgs e)
        {
            ReactivTab.ReactivUchetClick(this);
        }
        private void ReactivPrihodButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (Reactiv)gv.GetRow(gv.FocusedRowHandle);
            ReactivTab.ReactivPrihodButton_Click(this, Stroke);
        }
        private void ReactivRashodButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var ID = ((Reactiv)gv.GetRow(gv.FocusedRowHandle));
            ReactivTab.ReactivRashodButton_Click(this, ID);
        }
        private void ReactivSpisatButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var ID = ((Reactiv)gv.GetRow(gv.FocusedRowHandle)).Id;
            ReactivTab.ReactivSpisatButton_Click(this, ID);
        }
        private void ReactivWiewAktProverkiPrigodnostiClickOneReactiv(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (Reactiv)gv.GetRow(gv.FocusedRowHandle);
            ReactivTab.ReactivWiewAktProverkiPrigodnostiClickOneReactiv(this, Stroke.НаименованиеРеактива);
        }
        private void ReactivPrint(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += ReactivTab.ReactivPrintTread2;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }
        private void ReactivPrintTread2(object sender, DoWorkEventArgs e)
        {
            try
            {                
                WordDoc1 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Реактив.dotx");

                var RowCount = ReactivGridView.RowCount;
                if (RowCount > 1) WordDoc1.AddTableCell(2, RowCount - 1);
                int Cell = 1;
                for (int rowHandle = 0; rowHandle < RowCount; rowHandle++)
                {
                    Cell++;
                    if (ReactivGridView.IsDataRow(rowHandle) || Cell >= 39)
                    {
                        try
                        {
                            var SelectedRow = (Reactiv)ReactivGridView.GetRow(rowHandle);
                            WordDoc1.AddTableValue(2, Cell, 1, SelectedRow.N.ToString());
                            WordDoc1.AddTableValue(2, Cell, 2, SelectedRow.НаименованиеРеактива);
                            WordDoc1.AddTableValue(2, Cell, 3, SelectedRow.Классификация);
                            WordDoc1.AddTableValue(2, Cell, 4, SelectedRow.НДНаРеактив);
                            WordDoc1.AddTableValue(2, Cell, 5, SelectedRow.НомерПартии);
                            WordDoc1.AddTableValue(2, Cell, 6, SelectedRow.ДатаИзготовления.HasValue ? SelectedRow.ДатаИзготовления.Value.ToShortDateString() : string.Empty);
                            WordDoc1.AddTableValue(2, Cell, 7, SelectedRow.СрокГодности);
                            WordDoc1.AddTableValue(2, Cell, 8, SelectedRow.ДатаПолучения.HasValue ? SelectedRow.ДатаПолучения.Value.ToShortDateString() : string.Empty);
                            WordDoc1.AddTableValue(2, Cell, 9, SelectedRow.Остаток.HasValue ? SelectedRow.Остаток.Value.ToString() : string.Empty);
                            WordDoc1.AddTableValue(2, Cell, 10, SelectedRow.ЕдИзм);
                            WordDoc1.AddTableValue(2, Cell, 11, SelectedRow.Назначение);
                            WordDoc1.AddTableValue(2, Cell, 12, SelectedRow.ПродлениеСрока);
                        } catch { }
                    }
                }
                if (UserSettings.ReactivColumns != null)
                {
                    var Visibiliti = UserSettings.ReactivColumns.Split(',');
                    for (int i = Visibiliti.Length - 1; i > 0; i--)
                    {
                        if (Visibiliti[i] != "1")
                            WordDoc1.DeleteTableColumn(2, i + 1);
                    }
                }
                WordDoc1.TableResize(2);
                WordDoc1.Visible = true;
                WordDoc1.wordApplication.Activate();                
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                this.Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                WordDoc1.Close();                
            }
            finally
            {
                this.Invoke(new System.Action(() => Loader.Visible = false));
            }
        }
        private void ReactivWiewAktVhodnogoKontrolaClickOneReactiv(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (Reactiv)gv.GetRow(gv.FocusedRowHandle);
            ReactivTab.ReactivWiewAktVhodnogoKontrolaClick(this, Stroke.НаименованиеРеактива);
        }
        private void ReactivWiewAktVhodnogoKontrolaClick(object sender, EventArgs e)
        {
            ReactivTab.ReactivWiewAktVhodnogoKontrolaClick(this);
        }
        private void ReactivWiewAktProverkiPrigodnostiClick(object sender, EventArgs e)
        {
            ReactivTab.ReactivWiewAktProverkiPrigodnostiClick(this);
        }
        private void ReactivVisibleSpisatButton_Click(object sender, EventArgs e)
        {
            ReactivTab.ReactivVisibleSpisatButton_Click(this);
        }
        private void ReactivCopyButton_Click(object sender, EventArgs e)
        {
            ReactivTab.ReactivCopyButton_Click(this);
        }
        private void RefreshReactivButton_Click(object sender, EventArgs e)
        {
            ReactivTab.GetReactiv(this);
        }
        private void RefreshPersonalButton_Click(object sender, EventArgs e)
        {
            PersonalTab.GetPersonal(this);
        }
        private void ReactivSaveButton_Click(object sender, EventArgs e)
        {
            ReactivTab.ReactivSaveButton_Click(this);
        }
        private void ReactivNewButton_Click(object sender, EventArgs e)
        {
            ReactivTab.NewReactivButton(this);
        }
        private void SrokGodnoctiTip_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            ReactivTab.SrokGodnoctiTip_SelectedIndexChanged_1(this);
        }
        private void ReactivEditCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ReactivTab.ReactivEditCheckBox_CheckedChanged(this);
        }
        private void ReactivGrid_Click(object sender, EventArgs e)
        {            
        }
        private void ReactivDeleteButton_Click(object sender, EventArgs e)
        {
            ReactivTab.ReactivDeleteButton_Click(this);
        }
        private void PrintButton_Click(object sender, EventArgs e)
        {
            ContextMenuStrip ContextMS = new ContextMenuStrip();
            ContextMS.Items.Add("&Печать...", null, ReactivPrint);
            ContextMS.Items.Add("&Акты входного контроля", null, ReactivWiewAktVhodnogoKontrolaClick);
            ContextMS.Items.Add("&Акты проверки пригодности", null, ReactivWiewAktProverkiPrigodnostiClick);            
            ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);
        }
        private void CheckColumnButton_Click(object sender, EventArgs e)
        {
            ReactivTab.CheckColumnButton_Click(this);
        }
        private void ReactivColumnAvtoWight_Click(object sender, EventArgs e)
        {
            ReactivTab.AutoWidthColumn(this);
        }
        private void OnReactivPrintAktVhodnogoKontrolaRowClick(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                var Stroke = (Reactiv)gv.GetRow(gv.FocusedRowHandle);
                ReactivPrintAktvVhodnogoKontrolaForm f = new ReactivPrintAktvVhodnogoKontrolaForm(this, Stroke.Id);
                f.Owner = this;
                this.Visible = false;
                f.ShowDialog();
                ReactivTab.GetReactiv(this);
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void OnReactivPrintAktProverkiPrigodnostiRowClick(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                var Stroke = (Reactiv)gv.GetRow(gv.FocusedRowHandle);
                ReactivPrintAktvProverkiPrigodnostiForm f = new ReactivPrintAktvProverkiPrigodnostiForm(this, Stroke.Id);
                f.Owner = this;
                Visible = false;
                f.ShowDialog();
                ReactivTab.GetReactiv(this);
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        class RowInfo
        {
            public RowInfo(GridView view, int rowHandle)
            {
                this.RowHandle = rowHandle;
                this.View = view;
            }
            public GridView View;
            public int RowHandle;
        }
        private void ReactivGridView_ColumnWidthChanged(object sender, DevExpress.XtraGrid.Views.Base.ColumnEventArgs e)
        {
            ReactivTab.ReactivGridView_ColumnWidthChanged(this);
        }
        private void ReactivDateIzgotTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReactivTab.ReactivDateIzgotTip_SelectedIndexChanged(this);
        }
        private void ReactivDateGetTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReactivTab.ReactivDateGetTip_SelectedIndexChanged(this);
        }
       

        #endregion
        //Personal
        #region
        private void PersonalUpdateButton_Click(object sender, EventArgs e)
        {
            PersonalTab.GetPersonal(this);
        }
       
        private void splitContainerControl1_Panel1_SizeChanged(object sender, EventArgs e)
        {
            PersonalTab.splitContainerControl1_Panel1_SizeChanged(sender);
        }
        private void PersonalUvolenButton_Click(object sender, EventArgs e)
        {
            PersonalTab.PersonalUvolenButton_Click(this);
        }
        private void PersonalPrintButton_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += PersonalTab.PersonalPrint;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }
        private void PersonalGridControl_Click(object sender, EventArgs e)
        {            
        }
        private void PersonalEditCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            PersonalTab.PersonalEditCheckBox_CheckedChanged(this);
        }
        private void PersonalNewButton_Click(object sender, EventArgs e)
        {
            PersonalTab.NewPersonalButton(this);
        }
        private void PersonalSaveButton_Click(object sender, EventArgs e)
        {
            PersonalTab.PersonalSaveButton_Click(this);
        }
        private void PersonalDeleteButton_Click(object sender, EventArgs e)
        {
            PersonalTab.PersonalDeleteButton_Click(this);
        }
        private void PersonalCopyButton_Click(object sender, EventArgs e)
        {
            PersonalTab.PersonalCopyButton_Click(this);
        }
        private void PersonalColumnCheckButton_Click(object sender, EventArgs e)
        {
            PersonalTab.CheckColumnButton_Click(this);
        }
        private void PersonalSrokGasTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            PersonalTab.PersonalSrokGasTip_SelectedIndexChanged(this);
        }
        private void PersonalSrokAttestachiiTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            PersonalTab.PersonalSrokAttestachiiTip_SelectedIndexChanged(this);
        }
        private void PersonalSrokZnaniiOTiPBTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            PersonalTab.PersonalSrokZnaniiOTiPBTip_SelectedIndexChanged(this);
        }
        private void PersonalColumnAvtoWight_Click(object sender, EventArgs e)
        {
            PersonalTab.AutoWidthColumn(this);
        }
        private void PersonalGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            GridView view = sender as GridView;
            DevExpress.XtraGrid.Views.Grid.GridView gv = PersonalGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (Personal)gv.GetRow(gv.FocusedRowHandle);
            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
            {
                int rowHandle = e.HitInfo.RowHandle;
                e.Menu.Items.Clear();
                if (UserSettings.PersonalPrint)
                {
                    DXMenuCheckItem checkItem1 = new DXMenuCheckItem("&Печать...",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(PersonalPrint));
                    checkItem1.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem1);
                }
                DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Подогнать по ширине окна вкл/выкл",
                view.OptionsView.AllowCellMerge, null, new EventHandler(PersonalColumnAvtoWight_Click));
                checkItem5.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem5);
                if (UserSettings.PersonalArhiv)
                {
                    if (Stroke.VisibleStatus == 0 || Stroke.VisibleStatus == null)
                    {
                        DXMenuCheckItem checkItem8 = new DXMenuCheckItem("&Уволить",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(PersonalUvolitMenu_Click));
                        checkItem8.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem8);
                    }
                    else
                    {
                        DXMenuCheckItem checkItem9 = new DXMenuCheckItem("&Восстановить",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(PersonalVostanovitMenu_Click));
                        checkItem9.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem9);
                    }
                }
                if (UserSettings.PersonalDelete)
                {
                    DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(PersonalDeleteButton_Click));
                    checkItem3.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem3);
                }
                DXMenuCheckItem checkItem4 = new DXMenuCheckItem("&Обновить",
                view.OptionsView.AllowCellMerge, null, new EventHandler(PersonalUpdateButton_Click));
                checkItem4.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem4);
            }
        }
        private void PersonalVostanovitMenu_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = PersonalGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (Personal)gv.GetRow(gv.FocusedRowHandle);
            PersonalTab.PersonalVostanovitMenu_Click(this, Stroke.Id);
        }
        private void PersonalUvolitMenu_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = PersonalGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (Personal)gv.GetRow(gv.FocusedRowHandle);
            PersonalTab.PersonalUvolitMenu_Click(this, Stroke.Id);
        }
        private void PersonalPrint(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += PersonalTab.PersonalPrint;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }
        private void PersonalGridView_ColumnWidthChanged(object sender, DevExpress.XtraGrid.Views.Base.ColumnEventArgs e)
        {
            PersonalTab.PersonalGridView_ColumnWidthChanged(this);
        }
        private void PersonalSrokPodvergKvalifTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            PersonalTab.PersonalSrokPodvergKvalifTip_SelectedIndexChanged(this);
        }
        private void PersonalGasTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            PersonalTab.PersonalGasTip_SelectedIndexChanged(this);
        }
        private void PersonalAttestechiaTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            PersonalTab.PersonalAttestechiaTip_SelectedIndexChanged(this);
        }
        private void PersonalZnaniaOTiPBTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            PersonalTab.PersonalZnaniaOTiPBTip_SelectedIndexChanged(this);
        }
        private void PersonalCellMergeButton_Click(object sender, EventArgs e)
        {
            PersonalTab.PersonalCellMergeButton_Click(this);
        }
        #endregion
        //NTD
        #region
        private void NTDCellMergeButton_Click(object sender, EventArgs e)
        {
            NTDTab.NTDCellMergeButton_Click(this);
        }
        private void splitContainerControl2_Panel1_SizeChanged(object sender, EventArgs e)
        {
            NTDTab.splitContainerControl2_Panel1_SizeChanged(sender);
        }
       
        private void NTDFiltrButton_Click(object sender, EventArgs e)
        {
            NTDTab.NTDFiltrButton_Click(this);
        }
        private void NTDVisibleArhivButton_Click_1(object sender, EventArgs e)
        {
            NTDTab.NTDVisibleArhivButton_Click(this);
        }
        private void NTDUpdateButton_Click(object sender, EventArgs e)
        {
            NTDTab.GetNTD(this);
        }
        private void NTDEditCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            NTDTab.NTDEditCheckBox_CheckedChanged(this);
        }
        private void NTDNewButton_Click(object sender, EventArgs e)
        {
            NTDTab.NewNTDButton(this);
        }
        private void NTDSaveButton_Click(object sender, EventArgs e)
        {
            NTDTab.NTDSaveButton_Click(this);
        }
        private void NTDDeleteButton_Click(object sender, EventArgs e)
        {
            NTDTab.NTDDeleteButton_Click(this);
        }
        private void NTDPrintButton_Click(object sender, EventArgs e)
        {
            ContextMenuStrip ContextMS = new ContextMenuStrip();
            ContextMS.Items.Add("&Печать...", null, NTDPrintSpisok);
            ContextMS.Items.Add("&Печать \"Мастер лист\"", null, OnNTDPrintMasterList);
            ContextMS.Items.Add("&Печать \"Общий перечень НТД\"", null, OnNTDPrintObshiiPerechenNTD);
            ContextMS.Items.Add("&Печать \"Оснащенность лаборатории\"", null, NTDPrintOsnachinnost);
            ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);           
        }
        private void NTDGrid_Click(object sender, EventArgs e)
        {            
        }
        private void NTDCopyButton_Click(object sender, EventArgs e)
        {
            NTDTab.NTDCopyButton_Click(this);
        }
        public void NTDGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            GridView view = sender as GridView;
            DevExpress.XtraGrid.Views.Grid.GridView gv = NTDGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (NTD)gv.GetRow(gv.FocusedRowHandle);
            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
            {
                int rowHandle = e.HitInfo.RowHandle;
                e.Menu.Items.Clear();
                if (UserSettings.IOPrint)
                {
                    DXMenuCheckItem checkItem2 = new DXMenuCheckItem("&Печать...",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(NTDPrintSpisok));
                    checkItem2.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem2);
                    DXMenuCheckItem checkItem1 = new DXMenuCheckItem("&Печать \"Мастер лист\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(OnNTDPrintMasterList));
                    checkItem1.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem1);
                    DXMenuCheckItem checkItem6 = new DXMenuCheckItem("&Печать \"Общий перечень НТД\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(OnNTDPrintObshiiPerechenNTD));
                    checkItem6.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem6);
                    DXMenuCheckItem checkItem7 = new DXMenuCheckItem("&Печать \"Оснащенность лаборатории\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(NTDPrintOsnachinnost));
                    checkItem7.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem7);                    
                }
                DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Подогнать по ширине окна вкл/выкл",
                view.OptionsView.AllowCellMerge, null, new EventHandler(NTDColumnAvtoWight_Click));
                checkItem5.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem5);
                if (UserSettings.NTDArhiv)
                {
                    if (Stroke.VisibleStatus != 1)
                    {
                        DXMenuCheckItem checkItem8 = new DXMenuCheckItem("&В архив",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(NTDArhivButton_Click));
                        checkItem8.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem8);
                    }
                    else
                    {
                        DXMenuCheckItem checkItem18 = new DXMenuCheckItem("&Вернуть из архива",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(NTDReturnFromArhivButton_Click));
                        checkItem18.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem18);
                    }
                }
                if (UserSettings.NTDDelete)
                {
                    DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(NTDDeleteButton_Click));
                    checkItem3.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem3);
                }
                DXMenuCheckItem checkItem4 = new DXMenuCheckItem("&Обновить",
                view.OptionsView.AllowCellMerge, null, new EventHandler(NTDUpdateButton_Click));
                checkItem4.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem4);
            }
        }

        private void NTDPrintOsnachinnost(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += NTDTab.NTDPrintOsnachinnost;
                worker.RunWorkerAsync(this);
            }
            catch { };
        }

        private void NTDReturnFromArhivButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = NTDGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (NTD)gv.GetRow(gv.FocusedRowHandle);
            NTDTab.NTDReturnFromArhivButton_Click(Stroke.Id, this);
        }
        private void NTDArhivButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = NTDGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (NTD)gv.GetRow(gv.FocusedRowHandle);
            NTDTab.NTDArhivButton_Click(Stroke.Id, this);
        }
        private void NTDPrintSpisok(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += NTDTab.NTDPrintSpisok;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }
        private void OnNTDPrintObshiiPerechenNTD(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += NTDTab.PrintObshiiPerechenNTD;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }
        private void NTDColumnAvtoWight_Click(object sender, EventArgs e)
        {
            NTDTab.AutoWidthColumn(this);
        }
        private void OnNTDPrintMasterList(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += NTDTab.PrintMasterList;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }
        private void NTDGridView_ColumnWidthChanged(object sender, DevExpress.XtraGrid.Views.Base.ColumnEventArgs e)
        {
            NTDTab.NTDGridView_ColumnWidthChanged(this);
        }
        private void NTDColumnCheckButton_Click(object sender, EventArgs e)
        {
            NTDTab.CheckColumnButton_Click(this);
        }
        private void NTDDateUtvergdeniaTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            NTDTab.NTDDateUtvergdeniaTip_SelectedIndexChanged(this);
        }
        private void NTDDateAktualizTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            NTDTab.NTDDateAktualizTip_SelectedIndexChanged(this);
        }
        private void NTDSrokDeisrviaTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            NTDTab.NTDSrokDeisrviaTip_SelectedIndexChanged(this);
        }
        #endregion
        //SI
        #region
        private void SICellMergeButton_Click(object sender, EventArgs e)
        {
            SITab.SICellMergeButton_Click(this);
        }
        private void splitContainerControl4_Panel1_SizeChanged(object sender, EventArgs e)
        {
            SITab.splitContainerControl4_Panel1_SizeChanged(sender);
        }
        
        private void SIUpdateButton_Click(object sender, EventArgs e)
        {
            SITab.GetSI(this);
        }
        private void SIPrintButton_Click(object sender, EventArgs e)
        {
            try
            {
                ContextMenuStrip ContextMS = new ContextMenuStrip();
                ContextMS.Items.Add("&Печать...", null, SIPrint);
                ContextMS.Items.Add("&Печать \"График поверки и проверки\"", null, SIPrintGrafickAttestachii);
                ContextMS.Items.Add("&Печать \"Оснащенность лаборатории\"", null, SIPrintOsnachinnost);
                ContextMS.Items.Add("&Печать \"Перечень СИ\"", null, SIPrintPerechen);
                ContextMS.Items.Add("&Печать \"Перечень документации на СИ\"", null, SIPrintPerechenDocumentov);
                ContextMS.Items.Add("&Печать \"График ТО\"", null, SIPrintGrafikTO);
                ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);                
            }
            catch { }            
        }

        private void SIPrintGrafikTO(object sender, EventArgs e)
        {
            try
            {
                SelectYearForm SYF = new SelectYearForm();
                SYF.ShowDialog();
                if (GlobalStatic.BuferInt == 0) return;
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += SITab.SIPrintGrafikTO;
                worker.RunWorkerAsync(this);
            }
            catch { };
        }

        private void SIPrintPerechen(object sender, EventArgs e)
        {      
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += SITab.SIPrintPerechen;
                worker.RunWorkerAsync(this);
            }
            catch { };        
        }

        private void SIPrintPerechenDocumentov(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += SITab.SIPrintPerechenDocumentov;
                worker.RunWorkerAsync(this);
            }
            catch { };
        }

        private void SINewButton_Click(object sender, EventArgs e)
        {
            SITab.NewSIButton(this);
        }
        private void SIEditCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            SITab.SIEditCheckBox_CheckedChanged(this);
        }
        private void SIDeleteButton_Click(object sender, EventArgs e)
        {
            SITab.SIDeleteButton_Click(this);
        }
        private void SISaveButton_Click(object sender, EventArgs e)
        {
            SITab.SISaveButton_Click(this);
        }
        private void SIGrid_Click(object sender, EventArgs e)
        {            
        }
        private void SICopyButton_Click(object sender, EventArgs e)
        {
            SITab.SICopyButton_Click(this);
        }
        private void SIColumnCheckButton_Click(object sender, EventArgs e)
        {
            SITab.CheckColumnButton_Click(this);
        }
        private void SIDateSledPoverkiTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            SITab.SIDateSledPoverkiTip_SelectedIndexChanged(this);
        }
        private void SIDataSledTOTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            SITab.SIDataSledTOTip_SelectedIndexChanged(this);
        }
        private void SIGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            GridView view = sender as GridView;
            DevExpress.XtraGrid.Views.Grid.GridView gv = SIGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (SI)gv.GetRow(gv.FocusedRowHandle);
            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
            {
                int rowHandle = e.HitInfo.RowHandle;
                e.Menu.Items.Clear();
                if (UserSettings.SIPrint)
                {
                    DXMenuCheckItem checkItem7 = new DXMenuCheckItem("&Печать...",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(SIPrint));
                    checkItem7.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem7);
                    DXMenuCheckItem checkItem71 = new DXMenuCheckItem("&Печать \"График поверки и проверки\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(SIPrintGrafickAttestachii));
                    checkItem71.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem71);
                    DXMenuCheckItem checkItem72 = new DXMenuCheckItem("&Печать \"Оснащенность лаборатории\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(SIPrintOsnachinnost));
                    checkItem72.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem72);
                    DXMenuCheckItem checkItem73 = new DXMenuCheckItem("&Печать \"Перечень СИ\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(SIPrintPerechen));
                    checkItem73.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem73);
                    DXMenuCheckItem checkItem74 = new DXMenuCheckItem("&Печать \"Перечень документации на СИ\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(SIPrintPerechenDocumentov));
                    checkItem74.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem74);
                    DXMenuCheckItem checkItem75 = new DXMenuCheckItem("&Печать \"График ТО\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(SIPrintGrafikTO));
                    checkItem75.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem75);
                }
                DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Подогнать по ширине окна вкл/выкл",
                view.OptionsView.AllowCellMerge, null, new EventHandler(SIColumnAvtoWight_Click));
                checkItem5.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem5);
                if(!SITab.HistoryOfChange)
                {
                    if (UserSettings.SIArhiv)
                    {
                        if (Stroke.VisibleStatus != 1)
                        {
                            DXMenuCheckItem checkItem8 = new DXMenuCheckItem("&В архив",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(SIArhivButton_Click));
                            checkItem8.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem8);
                        }
                        else
                        {
                            DXMenuCheckItem checkItem18 = new DXMenuCheckItem("&Вернуть из архива",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(SIReturnFromArhivButton_Click));
                            checkItem18.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem18);
                        }
                    }
                    if (UserSettings.SIDelete)
                    {
                        DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(SIDeleteButton_Click));
                        checkItem3.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem3);
                    }
                }                
                DXMenuCheckItem checkItem4 = new DXMenuCheckItem("&Обновить",
                view.OptionsView.AllowCellMerge, null, new EventHandler(SIUpdateButton_Click));
                checkItem4.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem4);
            }
        }

        private void SIPrintOsnachinnost(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += SITab.SIPrintOsnachinnost;
                worker.RunWorkerAsync(this);
            }
            catch { };
        }

        private void SIPrintGrafickAttestachii(object sender, EventArgs e)
        {
            try
            {
                SelectYearForm SYF = new SelectYearForm();
                SYF.ShowDialog();
                if (GlobalStatic.BuferInt == 0) return;
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += SITab.SIPrintGrafickAttestachii;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }

        private void SIReturnFromArhivButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = SIGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (SI)gv.GetRow(gv.FocusedRowHandle);
            SITab.SIReturnFromArhivButton_Click(Stroke.Id, this);
        }
        private void SIArhivButton_Click_1(object sender, EventArgs e)
        {
            SITab.SIArhivButton_Click_1(this);
        }
        private void SIArhivButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = SIGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (SI)gv.GetRow(gv.FocusedRowHandle);
            SITab.SIArhivButton_Click(Stroke.Id, this);
        }
        private void SIPrint(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += SITab.SIPrint;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }
        private void SIColumnAvtoWight_Click(object sender, EventArgs e)
        {
            SITab.AutoWidthColumn(this);
        }
        private void SIGridView_ColumnWidthChanged(object sender, DevExpress.XtraGrid.Views.Base.ColumnEventArgs e)
        {
            SITab.SIGridView_ColumnWidthChanged(this);
        }
        private void SISrokDeystv_TextChanged(object sender, EventArgs e)
        {
            SITab.SISrokDeystv_TextChanged(this);
        }
        private void SIDataPoverkiTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            SITab.SIDataPoverkiTip_SelectedIndexChanged(this);
        }
        private void SIDataPosledTOTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            SITab.SIDataPosledTOTip_SelectedIndexChanged(this);
        }
        #endregion
        //IO
        #region
        private void IOCellMergeButton_Click(object sender, EventArgs e)
        {
            IOTab.IOCellMergeButton_Click(this);
        }
        private void splitContainerControl3_Panel1_SizeChanged(object sender, EventArgs e)
        {
            IOTab.splitContainerControl3_Panel1_SizeChanged(sender);
        }
       
        private void IOUpdateButton_Click(object sender, EventArgs e)
        {
            IOTab.GetIO(this);
        }
        private void IOPrintButton_Click(object sender, EventArgs e)
        {
            try
            {
                ContextMenuStrip ContextMS = new ContextMenuStrip();
                ContextMS.Items.Add("&Печать...", null, IOPrintSpisok);
                ContextMS.Items.Add("&Печать \"График аттестации\"", null, IOPrintGrafickAttestachii);
                ContextMS.Items.Add("&Печать \"Оснащенность лаборатории\"", null, IOPrintOsnachinnost);
                ContextMS.Items.Add("&Печать \"Перечень ИО\"", null, IOPrintPerechen);
                ContextMS.Items.Add("&Печать \"Перечень документации ИО\"", null, IOPrintPerechenDocumentacii);
                ContextMS.Items.Add("&Печать \"График ТО\"", null, IOPrintGrafikTO);
                ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);
            }
            catch { }
        }

        private void IOPrintGrafikTO(object sender, EventArgs e)
        {
            try
            {
                SelectYearForm SYF = new SelectYearForm();
                SYF.ShowDialog();
                if (GlobalStatic.BuferInt == 0) return;
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += IOTab.IOPrintGrafikTO;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }

        private void IOPrintPerechenDocumentacii(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += IOTab.IOPrintPerechenDocumentacii;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }

        private void IOPrintPerechen(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += IOTab.IOPrintPerechen;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }

        private void IOPrintOsnachinnost(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += IOTab.IOPrintOsnachinnost;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }

        private void IODeleteButton_Click(object sender, EventArgs e)
        {
            IOTab.IODeleteButton_Click(this);
        }
        private void IONewButton_Click(object sender, EventArgs e)
        {
            IOTab.NewIOButton(this);
        }
        private void IOEditCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            IOTab.IOEditCheckBox_CheckedChanged(this);
        }
        private void IOColumnCheckButton_Click(object sender, EventArgs e)
        {
            IOTab.CheckColumnButton_Click(this);
        }
        private void IOSaveButton_Click(object sender, EventArgs e)
        {
            IOTab.IOSaveButton_Click(this);
        }
        private void IOGrid_Click(object sender, EventArgs e)
        {            
        }
        private void IOCopyButtonButton_Click(object sender, EventArgs e)
        {
            IOTab.IOCopyButton_Click(this);
        }
        private void IODateSledAttistachiiTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            IOTab.IODateSledAttistachiiTip_SelectedIndexChanged(this);
        }
        private void IODataSledTOTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            IOTab.IODataSledTOTip_SelectedIndexChanged(this);
        }
        private void IOGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            GridView view = sender as GridView;
            DevExpress.XtraGrid.Views.Grid.GridView gv = IOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (IO)gv.GetRow(gv.FocusedRowHandle);
            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
            {
                int rowHandle = e.HitInfo.RowHandle;
                e.Menu.Items.Clear();
                if (UserSettings.IOPrint)
                {
                    DXMenuCheckItem checkItem7 = new DXMenuCheckItem("&Печать...",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(IOPrintSpisok));
                    checkItem7.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem7);
                    DXMenuCheckItem checkItem71 = new DXMenuCheckItem("&Печать \"График аттестации\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(IOPrintGrafickAttestachii));
                    checkItem71.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem71);
                    DXMenuCheckItem checkItem72 = new DXMenuCheckItem("&Печать \"Оснащенность лаборатории\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(IOPrintOsnachinnost));
                    checkItem72.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem72);
                    DXMenuCheckItem checkItem73 = new DXMenuCheckItem("&Печать \"Перечень ИО\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(IOPrintPerechen));
                    checkItem73.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem73);
                    DXMenuCheckItem checkItem74 = new DXMenuCheckItem("&Печать \"Перечень документации ИО\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(IOPrintPerechenDocumentacii));
                    checkItem74.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem74);
                    DXMenuCheckItem checkItem75 = new DXMenuCheckItem("&Печать \"График ТО\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(IOPrintGrafikTO));
                    checkItem75.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem75);
                }

                DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Подогнать по ширине окна вкл/выкл",
                view.OptionsView.AllowCellMerge, null, new EventHandler(IOColumnAvtoWight_Click));
                checkItem5.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem5);
                if (!IOTab.HistoryOfChange)
                {
                    if (UserSettings.IOArhiv)
                    {
                        if (Stroke.VisibleStatus != 1)
                        {
                            DXMenuCheckItem checkItem8 = new DXMenuCheckItem("&В архив",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(IOArhivButton_Click));
                            checkItem8.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem8);
                        }
                        else
                        {
                            DXMenuCheckItem checkItem18 = new DXMenuCheckItem("&Вернуть из архива",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(IOReturnFromArhivButton_Click));
                            checkItem18.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem18);
                        }
                    }
                    if (UserSettings.IODelete)
                    {
                        DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(IODeleteButton_Click));
                        checkItem3.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem3);
                    }
                }
                DXMenuCheckItem checkItem4 = new DXMenuCheckItem("&Обновить",
                view.OptionsView.AllowCellMerge, null, new EventHandler(IOUpdateButton_Click));
                checkItem4.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem4);
            }
        }

        private void IOReturnFromArhivButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = IOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (IO)gv.GetRow(gv.FocusedRowHandle);
            IOTab.IOReturnFromArhivButton_Click(Stroke.Id, this);
        }
        private void IOArhivButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = IOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (IO)gv.GetRow(gv.FocusedRowHandle);
            IOTab.IOArhivButton_Click(Stroke.Id, this);
        }
        private void IOPrint(object sender, EventArgs e)
        {
            try
            {
                ContextMenuStrip ContextMS = new ContextMenuStrip();
                ContextMS.Items.Add("&Печать...", null, IOPrintSpisok); 
                ContextMS.Items.Add("&Печать \"График аттестации\"", null, IOPrintGrafickAttestachii);                
                ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);                
            }
            catch { }
        }

        private void IOPrintGrafickAttestachii(object sender, EventArgs e)
        {
            try
            {
                SelectYearForm SYF = new SelectYearForm();
                SYF.ShowDialog();
                if (GlobalStatic.BuferInt == 0) return; 

                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += IOTab.PrintGrafickAttestachii;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }

        private void IOPrintSpisok(object sender, EventArgs e)
        {       
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += IOTab.IOPrint;
                worker.RunWorkerAsync(this);
            }
            catch { }       
        }

        private void IOArhivButton_Click_1(object sender, EventArgs e)
        {
            IOTab.IOArhivButton_Click(this);
        }
        private void IOColumnAvtoWight_Click(object sender, EventArgs e)
        {
            IOTab.AutoWidthColumn(this);
        }
        private void IOGridView_ColumnWidthChanged(object sender, DevExpress.XtraGrid.Views.Base.ColumnEventArgs e)
        {
            IOTab.IOGridView_ColumnWidthChanged(this);
        }
        private void IODataINAttestachiiTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            IOTab.IODataINAttestachiiTip_SelectedIndexChanged(this);
        }
        private void IODataPosledTOTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            IOTab.IODataPosledTOTip_SelectedIndexChanged(this);
        }
        private void IOPeriodich_SelectedIndexChanged(object sender, EventArgs e)
        {
            IOTab.IOPeriodich_SelectedIndexChanged(this);
        }
        #endregion
        //VO
        #region
        private void VOCellMergeButton_Click(object sender, EventArgs e)
        {
            VOTab.VOCellMergeButton_Click(this);
        }
        private void splitContainerControl5_Panel1_SizeChanged(object sender, EventArgs e)
        {
            VOTab.splitContainerControl5_Panel1_SizeChanged(sender);
        }
     
        private void VOUpdateButton_Click(object sender, EventArgs e)
        {
            VOTab.GetVO(this);
        }
        private void VODeleteButton_Click(object sender, EventArgs e)
        {
            VOTab.VODeleteButton_Click(this);
        }
        private void VOEditCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            VOTab.VOEditCheckBox_CheckedChanged(this);
        }
        private void VOPrintButton_Click(object sender, EventArgs e)
        {
            try
            {
                ContextMenuStrip ContextMS = new ContextMenuStrip();
                ContextMS.Items.Add("&Печать...", null, VOPrint);
                ContextMS.Items.Add("&Печать \"График проверки\"", null, VOPrintGrafickAttestachii);
                ContextMS.Items.Add("&Печать \"Оснащенность лаборатории\"", null, VOPrintOsnachinnost);
                ContextMS.Items.Add("&Печать \"Перечень ВО\"", null, VOPerechen);
                ContextMS.Items.Add("&Печать \"Перечень документации на ВО\"", null, VOPerechenDocumentacii);
                ContextMS.Items.Add("&Печать \"График ТО\"", null, VOGrafikTO);
                ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);
            }
            catch { }            
        }

        private void VOGrafikTO(object sender, EventArgs e)
        {
            try
            {
                SelectYearForm SYF = new SelectYearForm();
                SYF.ShowDialog();
                if (GlobalStatic.BuferInt == 0) return;
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VOTab.VOGrafikTO;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }

        private void VOPerechenDocumentacii(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VOTab.VOPerechenDocumentacii;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }

        private void VOPerechen(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VOTab.VOPerechen;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }

        private void VOPrintOsnachinnost(object sender, EventArgs e)
        {
            try
            {                 
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VOTab.VOPrintOsnachinnost;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }

        private void VOPrintGrafickAttestachii(object sender, EventArgs e)
        {
            try
            {
                SelectYearForm SYF = new SelectYearForm();
                SYF.ShowDialog();
                if (GlobalStatic.BuferInt == 0) return;
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VOTab.VOPrintGrafickAttestachii;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }

        private void VOColumnCheckButton_Click(object sender, EventArgs e)
        {
            VOTab.CheckColumnButton_Click(this);
        }
        private void VOSaveButton_Click(object sender, EventArgs e)
        {
            VOTab.VOSaveButton_Click(this);
        }
        private void VONewButton_Click(object sender, EventArgs e)
        {
            VOTab.NewVOButton(this);
        }       
        private void VOArhivButton_Click_1(object sender, EventArgs e)
        {
            VOTab.VOArhivButton_Click(this);
        }
        private void VOCopyButton_Click(object sender, EventArgs e)
        {
            VOTab.VOCopyButton_Click(this);
        }
        private void VODateSledProverkiTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            VOTab.VODateSledProverkiTip_SelectedIndexChanged(this);
        }
        private void VOSledProvTOTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            VOTab.VOSledProvTOTip_SelectedIndexChanged(this);
        }
        private void VOGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            GridView view = sender as GridView;
            DevExpress.XtraGrid.Views.Grid.GridView gv = VOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (VO)gv.GetRow(gv.FocusedRowHandle);
            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
            {
                int rowHandle = e.HitInfo.RowHandle;
                e.Menu.Items.Clear();
                if (UserSettings.VOPrint)
                {
                    DXMenuCheckItem checkItem7 = new DXMenuCheckItem("&Печать...",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(VOPrint));
                    checkItem7.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem7);
                    DXMenuCheckItem checkItem71 = new DXMenuCheckItem("&Печать \"График проверки\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(VOPrintGrafickAttestachii));
                    checkItem71.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem71);
                    DXMenuCheckItem checkItem72 = new DXMenuCheckItem("&Печать \"Оснащенность лаборатории\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(VOPrintOsnachinnost));
                    checkItem72.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem72);
                    DXMenuCheckItem checkItem73 = new DXMenuCheckItem("&Печать \"Перечень ВО\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(VOPerechen));
                    checkItem73.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem73);
                    DXMenuCheckItem checkItem74 = new DXMenuCheckItem("&Печать \"Перечень документации на ВО\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(VOPerechenDocumentacii));
                    checkItem74.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem74);
                    DXMenuCheckItem checkItem75 = new DXMenuCheckItem("&Печать \"График ТО\"",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(VOGrafikTO));
                    checkItem75.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem75);
                }
                DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Подогнать по ширине окна вкл/выкл",
                view.OptionsView.AllowCellMerge, null, new EventHandler(VOColumnAvtoWight_Click));
                checkItem5.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem5);
                if (!VOTab.HistoryOfChange)
                {
                    if (UserSettings.VOArhiv)
                    {
                        if (Stroke.VisibleStatus != 1)
                        {
                            DXMenuCheckItem checkItem8 = new DXMenuCheckItem("&В архив",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(VOArhivButton_Click));
                            checkItem8.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem8);
                        }
                        else
                        {
                            DXMenuCheckItem checkItem18 = new DXMenuCheckItem("&Вернуть из архива",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(VOReturnFromArhivButton_Click));
                            checkItem18.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem18);
                        }
                    }
                    if (UserSettings.VODelete)
                    {
                        DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(VODeleteButton_Click));
                        checkItem3.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem3);
                    }
                }
                DXMenuCheckItem checkItem4 = new DXMenuCheckItem("&Обновить",
                view.OptionsView.AllowCellMerge, null, new EventHandler(VOUpdateButton_Click));
                checkItem4.Tag = new RowInfo(view, rowHandle);
                e.Menu.Items.Add(checkItem4);
            }
        }

        private void VOReturnFromArhivButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = VOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (VO)gv.GetRow(gv.FocusedRowHandle);
            VOTab.VOReturnFromArhivButton_Click(Stroke.Id, this);
        }
        private void VOArhivButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = VOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (VO)gv.GetRow(gv.FocusedRowHandle);
            VOTab.VOArhivButton_Click(Stroke.Id, this);
        }
        private void VOPrint(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VOTab.VOPrint;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }
        private void VOColumnAvtoWight_Click(object sender, EventArgs e)
        {
            VOTab.AutoWidthColumn(this);
        }
        private void VOGridView_ColumnWidthChanged(object sender, DevExpress.XtraGrid.Views.Base.ColumnEventArgs e)
        {
            VOTab.VOGridView_ColumnWidthChanged(this);
        }
        private void VODateProverkiTehnicheskogoSostoianiaTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            VOTab.VODateProverkiTehnicheskogoSostoianiaTip_SelectedIndexChanged(this);
        }
        private void VODataPoslTOTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            VOTab.VODataPoslTOTip_SelectedIndexChanged(this);
        }
        #endregion 
        //ContextMenu
        #region
        private void PlusMinus_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, Convert.ToChar(177).ToString());
        }
        private void Copy_Click(object sender, EventArgs e)
        {
            ToolStripItem menuItem = sender as ToolStripItem;
            if (menuItem != null)
            {
                ContextMenuStrip owner = menuItem.Owner as ContextMenuStrip;
                if (owner != null)
                {
                    Control sourceControl = owner.SourceControl;
                    var data = new DataObject();
                    data.SetData(DataFormats.UnicodeText, true, sourceControl.Text);
                    var thread = new Thread(() => Clipboard.SetDataObject(data, true));
                    thread.SetApartmentState(ApartmentState.STA);
                    thread.Start();
                    thread.Join();
                }
            }
        }
        private void Paste_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                ContextMenuPaste(sender, Clipboard.GetText());
            }
        }
        private void Clear_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "", false);
        }
        private static void ContextMenuPaste(object sender, string Content, bool Summ = true)
        {
            try
            {
                ToolStripItem menuItem = sender as ToolStripItem;
                if (menuItem != null)
                {
                    ContextMenuStrip owner = menuItem.Owner as ContextMenuStrip;
                    if (owner != null)
                    {
                        Control sourceControl = owner.SourceControl;
                        if (Summ)
                        {
                            if (sourceControl is TextBox)
                            {
                                var TB = sourceControl as TextBox;
                                var selectionIndex = TB.SelectionStart;
                                if(TB.SelectionLength > 0)                                
                                    TB.Text = TB.Text.Replace(TB.Text.Substring(TB.SelectionStart, TB.SelectionLength), Content);                                
                                else                                
                                    TB.Text = TB.Text.Insert(selectionIndex, Content);
                                TB.SelectionStart = selectionIndex + Content.Length;
                            }
                            if (sourceControl is ComboBox)
                            {
                                var TB = sourceControl as ComboBox;
                                var selectionIndex = TB.SelectionStart;
                                if (TB.SelectionLength > 0)
                                    TB.Text = TB.Text.Replace(TB.Text.Substring(TB.SelectionStart, TB.SelectionLength), Content);
                                else
                                    TB.Text = TB.Text.Insert(selectionIndex, Content);
                                TB.SelectionStart = selectionIndex + Content.Length;
                            }
                        }
                        else sourceControl.Text = Content;
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Paste2_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, Convert.ToChar(178).ToString());
        }
        private void Paste3_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, Convert.ToChar(179).ToString());
        }
        private void PasteProcent_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "%");
        }
        private void PasteGradus_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, Convert.ToChar(176).ToString() + "C");
        }
        private void PasteFaringeit_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, Convert.ToChar(176).ToString() + "F");
        }
        private void Pasteмм2с2_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "мм" + Convert.ToChar(178).ToString() + "/c" + Convert.ToChar(178).ToString());
        }
        private void Pasteмм2с_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "мм" + Convert.ToChar(178).ToString() + "/c".ToString());
        }
        private void PasteКгм3_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "кг/м" + Convert.ToChar(179).ToString());
        }
        private void Pasteмгдм3_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "мг/дм" + Convert.ToChar(179).ToString());
        }
        private void Pasteмгм3_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "мг/м" + Convert.ToChar(179).ToString());
        }
        private void Pastecм3_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "см" + Convert.ToChar(179).ToString());
        }
        private void Pasteдм3_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "дм" + Convert.ToChar(179).ToString());
        }
        private void Pasteм3_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "м" + Convert.ToChar(179).ToString());
        }
        private void PasteмкСмсм_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "мкСм/cм");
        }
        private void Pasteмкгг_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "мкг/г");
        }
        private void Pasteppm_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "ppm");
        }
        private void PasteрН_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, "pH");
        }
        private void Cut_Click(object sender, EventArgs e)
        {
            ToolStripItem menuItem = sender as ToolStripItem;
            if (menuItem != null)
            {
                ContextMenuStrip owner = menuItem.Owner as ContextMenuStrip;
                if (owner != null)
                {
                    Control sourceControl = owner.SourceControl;
                    var data = new DataObject();
                    data.SetData(DataFormats.UnicodeText, true, sourceControl.Text);
                    var thread = new Thread(() => Clipboard.SetDataObject(data, true));
                    thread.SetApartmentState(ApartmentState.STA);
                    thread.Start();
                    thread.Join();
                    sourceControl.Text = "";
                }
            }
        }
        private void Paste0_Click(object sender, EventArgs e)
        {
            ContextMenuPaste(sender, Convert.ToChar(176).ToString());
        }
        #endregion
        //Skins
        #region
        private void SkinComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (UserSettings.UseSkinsCheckBox)
            {
                DESkin.LookAndFeel.SetSkinStyle(SkinComboBox.Text);
                UserSettings.Skin = SkinComboBox.Text;                
                UserSettings.SaveSettings();
            }
        }
        private void UseSkinsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (UseSkinsCheckBox.Checked)
            {
                SkinComboBox.Enabled = true;
                DevExpress.Skins.SkinManager.EnableFormSkins();
                UserSettings.UseSkinsCheckBox = true;
                if (UserSettings.Skin != "")
                {
                    DESkin.LookAndFeel.SetSkinStyle(UserSettings.Skin);
                }
                RW_XML.AddToFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS\\config.xml", new SerializerClass { property = "UseSkins", value = "true" });
            }
            else
            {
                SkinComboBox.Enabled = false;
                DevExpress.Skins.SkinManager.DisableFormSkins();
                DESkin.LookAndFeel.SetDefaultStyle();
                UserSettings.UseSkinsCheckBox = false;
                RW_XML.AddToFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS\\config.xml", new SerializerClass { property = "UseSkins", value = "false" });                
            }
            UserSettings.SaveSettings();
        }
        #endregion
        //Alerts
        #region       
        private void ViewAlertWindow(string Text, int Tip, int Height, int id, int Number)
        {
            AlertForm AF = new AlertForm(Text, Tip, id, Number);
            AF.Location = new System.Drawing.Point(Alert.Width - AF.Size.Width, Height - AF.Size.Height);
            AF.Show();
        }
        private void Proverka()
        {
            Thread.Sleep(2000);
            var screen = System.Windows.Forms.Screen.AllScreens;
            Alert.Width = screen[0].WorkingArea.Width;
            Alert.Height = screen[0].WorkingArea.Height;
            var Height = Alert.Height;
            int FormHigth = 135;
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Reactivi = db.Reactiv.Where(c => c.VisibleStatus == null || c.VisibleStatus < 1).OrderBy(c => c.N).ToList();
                    foreach (var ReactivSrok in Reactivi)
                    {
                        var ReactivSrokGodnosti = db.ReactivAlerts.Where(c => c.ReactivID == ReactivSrok.Id && UserSettings.User == c.UserName).FirstOrDefault();
                        if (ReactivSrokGodnosti == null || ReactivSrokGodnosti.ReactivSrokGodnosti <= DateTime.Now)
                        {
                            if (ReactivSrok.СрокГодности != null && ReactivSrok.СрокГодностиDate <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У реактива " + ReactivSrok.НаименованиеРеактива + "(" + ReactivSrok.Классификация + ") " + ReactivSrok.НДНаРеактив + " партия №" + ReactivSrok.НомерПартии + " истекает срок годности " + ReactivSrok.СрокГодностиDate.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (ReactivSrok.СрокГодностиDate <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У реактива " + ReactivSrok.НаименованиеРеактива + "(" + ReactivSrok.Классификация + ") " + ReactivSrok.НДНаРеактив + " партия №" + ReactivSrok.НомерПартии + " истёк срок годности " + ReactivSrok.СрокГодностиDate.Value.ToShortDateString() + "г.";
                                }
                                else if (ReactivSrok.СрокГодностиDate <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У реактива " + ReactivSrok.НаименованиеРеактива + "(" + ReactivSrok.Классификация + ") " + ReactivSrok.НДНаРеактив + " партия №" + ReactivSrok.НомерПартии + " истекает срок годности " + ReactivSrok.СрокГодностиDate.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, ReactivSrok.Id, 1)));
                            }
                        }
                    }

                    var Personali = db.Personal.Where(c => c.VisibleStatus == null || c.VisibleStatus < 1).OrderBy(c => c.N).ToList();
                    foreach (var PersonalTek in Personali)
                    {
                        var PersonalAlert = db.PersonalAlerts.Where(c => c.PersonalID == PersonalTek.Id && UserSettings.User == c.UserName).FirstOrDefault();
                        if (PersonalAlert == null || PersonalAlert.PersonalSrokAttestachii == null || PersonalAlert.PersonalSrokAttestachii <= DateTime.Now)
                        {
                            if (PersonalTek.Срок_аттестации != null && PersonalTek.Срок_аттестации <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истекает срок аттестации " + PersonalTek.Срок_аттестации.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (PersonalTek.Срок_аттестации <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истёк срок аттестации " + PersonalTek.Срок_аттестации.Value.ToShortDateString() + "г.";
                                }
                                else if (PersonalTek.Срок_аттестации <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истекает срок аттестации " + PersonalTek.Срок_аттестации.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, PersonalTek.Id, 2)));
                            }
                        }
                        if (PersonalAlert == null || PersonalAlert.PersonalSrokPodvergdenia == null || PersonalAlert.PersonalSrokPodvergdenia <= DateTime.Now)
                        {
                            if (PersonalTek.Срок_подтверждения != null && PersonalTek.Срок_подтверждения <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истекает срок подтверждения квалификации по основной профессии " + PersonalTek.Срок_подтверждения.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (PersonalTek.Срок_подтверждения <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истёк срок подтверждения квалификации по основной профессии " + PersonalTek.Срок_подтверждения.Value.ToShortDateString() + "г.";
                                }
                                else if (PersonalTek.Срок_подтверждения <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истекает срок подтверждения квалификации по основной профессии " + PersonalTek.Срок_подтверждения.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, PersonalTek.Id, 3)));
                            }
                        }
                        if (PersonalAlert == null || PersonalAlert.PersonalSrokPodvergdeniaGas == null || PersonalAlert.PersonalSrokPodvergdeniaGas <= DateTime.Now)
                        {
                            if (PersonalTek.Срок_подтверждения_лаборанта != null && PersonalTek.Срок_подтверждения_лаборанта <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истекает срок подтверждения квалификации лаборанта-газоанализаторщика " + PersonalTek.Срок_подтверждения_лаборанта.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (PersonalTek.Срок_подтверждения_лаборанта <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истёк срок подтверждения квалификации лаборанта-газоанализаторщика " + PersonalTek.Срок_подтверждения_лаборанта.Value.ToShortDateString() + "г.";
                                }
                                else if (PersonalTek.Срок_подтверждения_лаборанта <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истекает срок подтверждения квалификации лаборанта-газоанализаторщика " + PersonalTek.Срок_подтверждения_лаборанта.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, PersonalTek.Id, 4)));
                            }
                        }
                        if (PersonalAlert == null || PersonalAlert.PersonalSrokProverkiZnaniiOTiTB == null || PersonalAlert.PersonalSrokProverkiZnaniiOTiTB <= DateTime.Now)
                        {
                            if (PersonalTek.Срок_проверки_знаний_по_ОТ_и_ТБ != null && PersonalTek.Срок_проверки_знаний_по_ОТ_и_ТБ <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истекает срок проверки знаний по ОТ и ТБ " + PersonalTek.Срок_проверки_знаний_по_ОТ_и_ТБ.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (PersonalTek.Срок_проверки_знаний_по_ОТ_и_ТБ <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истёк срок проверки знаний по ОТ и ТБ " + PersonalTek.Срок_проверки_знаний_по_ОТ_и_ТБ.Value.ToShortDateString() + "г.";
                                }
                                else if (PersonalTek.Срок_проверки_знаний_по_ОТ_и_ТБ <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У сотрудника " + PersonalTek.ФИО + " истекает срок проверки знаний по ОТ и ТБ " + PersonalTek.Срок_проверки_знаний_по_ОТ_и_ТБ.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, PersonalTek.Id, 5)));
                            }
                        }
                    }

                    var NTDAll = db.NTD.Where(c => c.VisibleStatus == null || c.VisibleStatus < 1).OrderBy(c => c.N).ToList();
                    foreach (var NTDTek in NTDAll)
                    {
                        var NTDAlert = db.NTDAlerts.Where(c => c.NTDID == NTDTek.Id && UserSettings.User == c.UserName).FirstOrDefault();
                        if (NTDAlert == null || NTDAlert.NTDSrokDeistvia == null || NTDAlert.NTDSrokDeistvia <= DateTime.Now)
                        {
                            if (NTDTek.СрокДействия != null && NTDTek.СрокДействия <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У НТД \"" + (NTDTek.НаименованиеДокумента.Length > 147 ? NTDTek.НаименованиеДокумента.Substring(0, 145) + "..." : NTDTek.НаименованиеДокумента) + "\" истекает срок действия " + NTDTek.СрокДействия.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (NTDTek.СрокДействия <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У НТД \"" + (NTDTek.НаименованиеДокумента.Length > 147 ? NTDTek.НаименованиеДокумента.Substring(0, 145) + "..." : NTDTek.НаименованиеДокумента) + "\" истёк срок действия " + NTDTek.СрокДействия.Value.ToShortDateString() + "г.";
                                }
                                else if (NTDTek.СрокДействия <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У НТД \"" + (NTDTek.НаименованиеДокумента.Length > 147 ? NTDTek.НаименованиеДокумента.Substring(0, 145) + "..." : NTDTek.НаименованиеДокумента) + "\" истекает срок действия " + NTDTek.СрокДействия.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, NTDTek.Id, 6)));
                            }
                        }

                    }

                    var SIAll = db.SI.Where(c => c.VisibleStatus == null || c.VisibleStatus < 1).OrderBy(c => c.Nint).ToList();
                    foreach (var SITek in SIAll)
                    {
                        var SIAlert = db.SIAlerts.Where(c => c.SIID == SITek.Id && UserSettings.User == c.UserName).FirstOrDefault();
                        if (SIAlert == null || SIAlert.SIDataSledPoverki == null || SIAlert.SIDataSledPoverki <= DateTime.Now)
                        {
                            if (SITek.ДатаСледПоверки != null && SITek.ДатаСледПоверки <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У СИ \"" + (SITek.НаименованиеСИ.Length > 147 ? SITek.НаименованиеСИ.Substring(0, 145) + "..." : SITek.НаименованиеСИ) + "\" истекает срок поверки " + SITek.ДатаСледПоверки.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (SITek.ДатаСледПоверки <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У СИ \"" + (SITek.НаименованиеСИ.Length > 147 ? SITek.НаименованиеСИ.Substring(0, 145) + "..." : SITek.НаименованиеСИ) + "\" истёк срок поверки " + SITek.ДатаСледПоверки.Value.ToShortDateString() + "г.";
                                }
                                else if (SITek.ДатаСледПоверки <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У СИ \"" + (SITek.НаименованиеСИ.Length > 147 ? SITek.НаименованиеСИ.Substring(0, 145) + "..." : SITek.НаименованиеСИ) + "\" истекает срок поверки " + SITek.ДатаСледПоверки.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, SITek.Id, 7)));
                            }
                        }
                        if (SIAlert == null || SIAlert.SIDateSledTO == null || SIAlert.SIDateSledTO <= DateTime.Now)
                        {
                            if (SITek.ДатаСледПроведенияТО != null && SITek.ДатаСледПроведенияТО <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У СИ \"" + (SITek.НаименованиеСИ.Length > 147 ? SITek.НаименованиеСИ.Substring(0, 145) + "..." : SITek.НаименованиеСИ) + "\" истекает срок проведения ТО " + SITek.ДатаСледПроведенияТО.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (SITek.ДатаСледПроведенияТО <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У СИ \"" + (SITek.НаименованиеСИ.Length > 147 ? SITek.НаименованиеСИ.Substring(0, 145) + "..." : SITek.НаименованиеСИ) + "\" истёк срок проведения ТО " + SITek.ДатаСледПроведенияТО.Value.ToShortDateString() + "г.";
                                }
                                else if (SITek.ДатаСледПроведенияТО <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У СИ \"" + (SITek.НаименованиеСИ.Length > 147 ? SITek.НаименованиеСИ.Substring(0, 145) + "..." : SITek.НаименованиеСИ) + "\" истекает срок проведения ТО " + SITek.ДатаСледПроведенияТО.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, SITek.Id, 8)));
                            }
                        }
                    }

                    var IOAll = db.IO.Where(c => c.VisibleStatus == null || c.VisibleStatus < 1).OrderBy(c => c.Nint).ToList();
                    foreach (var IOTek in IOAll)
                    {
                        var IOAlert = db.IOAlerts.Where(c => c.IOID == IOTek.Id && UserSettings.User == c.UserName).FirstOrDefault();
                        if (IOAlert == null || IOAlert.IODateSledAttestacii == null || IOAlert.IODateSledAttestacii <= DateTime.Now)
                        {
                            if (IOTek.ДатаСледАттестации != null && IOTek.ДатаСледАттестации <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У ИО \"" + (IOTek.НаименованиеИспытательготоОборудования.Length > 147 ? IOTek.НаименованиеИспытательготоОборудования.Substring(0, 145) + "..." : IOTek.НаименованиеИспытательготоОборудования) + "\" истекает срок поверки " + IOTek.ДатаСледАттестации.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (IOTek.ДатаСледАттестации <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У ИО \"" + (IOTek.НаименованиеИспытательготоОборудования.Length > 147 ? IOTek.НаименованиеИспытательготоОборудования.Substring(0, 145) + "..." : IOTek.НаименованиеИспытательготоОборудования) + "\" истёк срок поверки " + IOTek.ДатаСледАттестации.Value.ToShortDateString() + "г.";
                                }
                                else if (IOTek.ДатаСледАттестации <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У ИО \"" + (IOTek.НаименованиеИспытательготоОборудования.Length > 147 ? IOTek.НаименованиеИспытательготоОборудования.Substring(0, 145) + "..." : IOTek.НаименованиеИспытательготоОборудования) + "\" истекает срок поверки " + IOTek.ДатаСледАттестации.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, IOTek.Id, 9)));
                            }
                        }
                        if (IOAlert == null || IOAlert.IODateSledTO == null || IOAlert.IODateSledTO <= DateTime.Now)
                        {
                            if (IOTek.ДатаСледПроведенияТО != null && IOTek.ДатаСледПроведенияТО <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У ИО \"" + (IOTek.НаименованиеИспытательготоОборудования.Length > 147 ? IOTek.НаименованиеИспытательготоОборудования.Substring(0, 145) + "..." : IOTek.НаименованиеИспытательготоОборудования) + "\" истекает срок проведения ТО " + IOTek.ДатаСледПроведенияТО.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (IOTek.ДатаСледПроведенияТО <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У ИО \"" + (IOTek.НаименованиеИспытательготоОборудования.Length > 147 ? IOTek.НаименованиеИспытательготоОборудования.Substring(0, 145) + "..." : IOTek.НаименованиеИспытательготоОборудования) + "\" истёк срок проведения ТО " + IOTek.ДатаСледПроведенияТО.Value.ToShortDateString() + "г.";
                                }
                                else if (IOTek.ДатаСледПроведенияТО <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У ИО \"" + (IOTek.НаименованиеИспытательготоОборудования.Length > 147 ? IOTek.НаименованиеИспытательготоОборудования.Substring(0, 145) + "..." : IOTek.НаименованиеИспытательготоОборудования) + "\" истекает срок проведения ТО " + IOTek.ДатаСледПроведенияТО.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, IOTek.Id, 10)));
                            }
                        }
                    }

                    var VOAll = db.VO.Where(c => c.VisibleStatus == null || c.VisibleStatus < 1).OrderBy(c => c.Nint).ToList();
                    foreach (var VOTek in VOAll)
                    {
                        var VOAlert = db.VOAlerts.Where(c => c.VOID == VOTek.Id && UserSettings.User == c.UserName).FirstOrDefault();
                        if (VOAlert == null || VOAlert.VODateSledPoverki == null || VOAlert.VODateSledPoverki <= DateTime.Now)
                        {
                            if (VOTek.ДатаСледующейПроверки != null && VOTek.ДатаСледующейПроверки <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У ВО \"" + (VOTek.Наименование.Length > 147 ? VOTek.Наименование.Substring(0, 145) + "..." : VOTek.Наименование) + "\" истекает срок проверки " + VOTek.ДатаСледующейПроверки.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (VOTek.ДатаСледующейПроверки <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У ВО \"" + (VOTek.Наименование.Length > 147 ? VOTek.Наименование.Substring(0, 145) + "..." : VOTek.Наименование) + "\" истёк срок проверки " + VOTek.ДатаСледующейПроверки.Value.ToShortDateString() + "г.";
                                }
                                else if (VOTek.ДатаСледующейПроверки <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У ВО \"" + (VOTek.Наименование.Length > 147 ? VOTek.Наименование.Substring(0, 145) + "..." : VOTek.Наименование) + "\" истекает срок проверки " + VOTek.ДатаСледующейПроверки.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, VOTek.Id,11)));
                            }
                        }
                        if (VOAlert == null || VOAlert.VODateSledTO == null || VOAlert.VODateSledTO <= DateTime.Now)
                        {
                            if (VOTek.ДатаСледПроведенияТО != null && VOTek.ДатаСледПроведенияТО <= DateTime.Now.AddMonths(3))
                            {
                                string AlertMessage = "У ВО \"" + (VOTek.Наименование.Length > 147 ? VOTek.Наименование.Substring(0, 145) + "..." : VOTek.Наименование) + "\" истекает срок проведения ТО " + VOTek.ДатаСледПроведенияТО.Value.ToShortDateString() + "г.";
                                var MessageColor = 0;
                                if (VOTek.ДатаСледПроведенияТО <= DateTime.Now)
                                {
                                    MessageColor = 2;
                                    AlertMessage = "У ВО \"" + (VOTek.Наименование.Length > 147 ? VOTek.Наименование.Substring(0, 145) + "..." : VOTek.Наименование) + "\" истёк срок проведения ТО " + VOTek.ДатаСледПроведенияТО.Value.ToShortDateString() + "г.";
                                }
                                else if (VOTek.ДатаСледПроведенияТО <= DateTime.Now.AddMonths(1))
                                {
                                    MessageColor = 1;
                                    AlertMessage = "У ВО \"" + (VOTek.Наименование.Length > 147 ? VOTek.Наименование.Substring(0, 145) + "..." : VOTek.Наименование) + "\" истекает срок проведения ТО " + VOTek.ДатаСледПроведенияТО.Value.ToShortDateString() + "г.";
                                }

                                while (Alert.WindowsHeights.Count >= (int)(Alert.Height / FormHigth))
                                {
                                    System.Threading.Thread.Sleep(3000);
                                }
                                if (!Alert.TurnONAlerts) return;
                                Height = Alert.Height;
                                while (Height > 0)
                                {
                                    if (!Alert.WindowsHeights.Contains(Height)) { break; }
                                    Height = Height - FormHigth;
                                }
                                lock (Alert.WindowsHeights)
                                    Alert.WindowsHeights.Add(Height);
                                this.Invoke(new Action(() => ViewAlertWindow(AlertMessage, MessageColor, Height, VOTek.Id, 12)));
                            }
                        }
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
        private void AddUser_Click(object sender, EventArgs e)
        {
            try
            {
                var f = new AddChangeUserForm();
                f.Owner = this;
                Visible = false;
                f.ShowDialog();
                Visible = true;
                f.Dispose();
                SetPrava();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }
        //GSO-VLK
        #region
        private void GSOGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            try
            {
                GridView view = sender as GridView;
                DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                var Names = GSOSpisokTab.SelectedTabPage.Name.Split('&');
                if (Names.Length == 1)
                {
                    switch (GSOSpisokTab.SelectedTabPage.Name)
                    {
                        case "MainGSOTab":
                            var Stroke = (AnalizVLK)gv.GetRow(gv.FocusedRowHandle);
                            if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
                            {
                                int rowHandle = e.HitInfo.RowHandle;
                                e.Menu.Items.Clear();
                                if(UserSettings.VLKAnalizEdit)
                                {
                                    DXMenuCheckItem checkItem1 = new DXMenuCheckItem("&Редактировать",
                                    view.OptionsView.AllowCellMerge, null, new EventHandler(AnalizEditClick));
                                    checkItem1.Tag = new RowInfo(view, rowHandle);
                                    e.Menu.Items.Add(checkItem1);
                                }
                                if (UserSettings.VLKAnalizDelete)
                                {
                                    DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                                    view.OptionsView.AllowCellMerge, null, new EventHandler(AnalizDeleteButton_Click));
                                    checkItem3.Tag = new RowInfo(view, rowHandle);
                                    e.Menu.Items.Add(checkItem3);
                                }                                    
                                DXMenuCheckItem checkItem4 = new DXMenuCheckItem("&Обновить",
                                view.OptionsView.AllowCellMerge, null, new EventHandler(AnalizOpdateRowClick));
                                checkItem4.Tag = new RowInfo(view, rowHandle);
                                e.Menu.Items.Add(checkItem4);
                            }
                            break;
                    }
                }
                else if (Names[0] == "Пробы" || Names[0] == "ПробыОперативный")
                {
                    var Stroke = (Probi)gv.GetRow(gv.FocusedRowHandle);
                    if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
                    {
                        int rowHandle = e.HitInfo.RowHandle;
                        e.Menu.Items.Clear();
                        if(UserSettings.VLKProbiAdd)
                        {
                            DXMenuCheckItem checkItem10 = new DXMenuCheckItem("&Добавить контрольную процедуру",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiAddClick));
                            checkItem10.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem10);
                        }
                        if(UserSettings.VLKProbiEdit)
                        {
                            DXMenuCheckItem checkItem1 = new DXMenuCheckItem("&Редактировать",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiEditClick));
                            checkItem1.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem1);
                        }
                        if(UserSettings.VLKProbiChangeData)
                        {
                            DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Записать результат анализа",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiAddDataButton_Click));
                            checkItem5.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem5);
                        }
                        
                        if (Names[0] == "ПробыОперативный" && UserSettings.VLKOperaivniiPrint)
                        {
                            DXMenuCheckItem checkItem18 = new DXMenuCheckItem("&Печать \"Оперативный контроль\"",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(PrintOperativniiKontrolClick));
                            checkItem18.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem18);
                        }
                        if (Stroke.X1 != null && Stroke.X1 != 0 && Stroke.X2 != null && Stroke.X2 != 0)
                        {
                            DXMenuCheckItem checkItem6 = new DXMenuCheckItem("&Пределы",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(PredeliView_Click));
                            checkItem6.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem6);
                        }
                        if (Names[0] == "Пробы")
                        {
                            if (UserSettings.VLKOperaivnii)
                            {
                                DXMenuCheckItem checkItem41 = new DXMenuCheckItem("&Печать...",
                                view.OptionsView.AllowCellMerge, null, new EventHandler(PrintProbiClick));
                                checkItem41.Tag = new RowInfo(view, rowHandle);
                                e.Menu.Items.Add(checkItem41);
                            }                            
                            if (UserSettings.VLKOperaivnii)
                            {
                                DXMenuCheckItem checkItem11 = new DXMenuCheckItem("&Оперативный контроль",
                                view.OptionsView.AllowCellMerge, null, new EventHandler(OperayivniiKontrol_Click));
                                checkItem11.Tag = new RowInfo(view, rowHandle);
                                e.Menu.Items.Add(checkItem11);
                            }
                            if(UserSettings.VLKStabilnosti)
                            {
                                DXMenuCheckItem checkItem12 = new DXMenuCheckItem("&Контроль стабильности",
                                view.OptionsView.AllowCellMerge, null, new EventHandler(KontrolStabilnosti_Click));
                                checkItem12.Tag = new RowInfo(view, rowHandle);
                                e.Menu.Items.Add(checkItem12);
                            }                            
                        }
                        if(UserSettings.VLKProbiDelete)
                        {
                            DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiDeleteButton_Click));
                            checkItem3.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem3);
                        }                        
                        DXMenuCheckItem checkItem4 = new DXMenuCheckItem("&Обновить",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiOpdateRowClick));
                        checkItem4.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem4);
                    }
                }
                else if (Names[0] == "Оперативный")
                {
                    var Stroke = (OperativniiKontrol)gv.GetRow(gv.FocusedRowHandle);
                    if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
                    {
                        int rowHandle = e.HitInfo.RowHandle;
                        e.Menu.Items.Clear();
                        if(UserSettings.VLKOperaivniiPrint)
                        {
                            DXMenuCheckItem checkItem18 = new DXMenuCheckItem("&Печать \"Оперативный контроль\"",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(PrintOperativniiKontrolClick));
                            checkItem18.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem18);
                        }                        
                        DXMenuCheckItem checkItem10 = new DXMenuCheckItem("&Просмотр",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(ViewOperativniiKontrolProbiClick));
                        checkItem10.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem10);
                        if (Stroke.VisibleStatus != 1 && UserSettings.VLKOperaivniiClose)
                        {
                            DXMenuCheckItem checkItem1 = new DXMenuCheckItem("&Закрыть",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(CloseOperativniiKontrolBookClick));
                            checkItem1.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem1);
                        }
                        if(UserSettings.VLKOperaivniiEdit)
                        {
                            DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Изменить дату",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(ChangeDateOperativniiKontrol_Click));
                            checkItem5.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem5);
                        }
                        if(UserSettings.VLKOperaivniiDelete)
                        {
                            DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(OperativniiKontrolDeleteButton_Click));
                            checkItem3.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem3);
                        }                        
                        DXMenuCheckItem checkItem4 = new DXMenuCheckItem("&Обновить",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiOpdateRowClick));
                        checkItem4.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem4);
                    }
                }
                else if (Names[0] == "Стабильности")
                {
                    var Stroke = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
                    if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
                    {
                        int rowHandle = e.HitInfo.RowHandle;
                        e.Menu.Items.Clear();
                        if(UserSettings.VLKStabilnostiDiagrams)
                        {
                            DXMenuCheckItem checkItem15 = new DXMenuCheckItem("&Контроль повторяемости",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(KontrolPovtorayamostiClick));
                            checkItem15.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem15);
                            DXMenuCheckItem checkItem16 = new DXMenuCheckItem("&Контроль внутрилабораторной прецизионности",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(KontrolPrechizionnostiClick));
                            checkItem16.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem16);
                            DXMenuCheckItem checkItem17 = new DXMenuCheckItem("&Контроль погрешности",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(KontrolPogreshnostiClick));
                            checkItem17.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem17);
                        }
                        if(UserSettings.VLKStabilnostiPrint)
                        {
                            DXMenuCheckItem checkItem18 = new DXMenuCheckItem("&Печать \"Контроль стабильности\"",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(PrintKontrolStabilnostiClick));
                            checkItem18.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem18);
                        }
                        if(UserSettings.VLKStabilnostiProtokolOchenka)
                        {
                            DXMenuCheckItem checkItem20 = new DXMenuCheckItem("&Печать \"Оценка ККШ и протокол\"",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(PrintOchenkaKKSHClick));
                            checkItem20.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem20);
                            DXMenuCheckItem checkItem19 = new DXMenuCheckItem("&Печать \"Оценка ККШ\"",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(PrintOchenkaKKSHClick1));
                            checkItem19.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem19);
                            DXMenuCheckItem checkItem21 = new DXMenuCheckItem("&Печать \"Протокол\"",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(PrintOchenkaKKSHClick2));
                            checkItem21.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem21);
                        }                        
                        DXMenuCheckItem checkItem10 = new DXMenuCheckItem("&Просмотр",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(ViewKontrolStabilnostiProbiClick));
                        checkItem10.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem10);
                        if (Stroke.VisibleStatus != 1 && UserSettings.VLKStabilnostiClose)
                        {
                            DXMenuCheckItem checkItem1 = new DXMenuCheckItem("&Закрыть",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(CloseKontrolStabilnostiBookClick));
                            checkItem1.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem1);
                        }
                        if(UserSettings.VLKStabilnostiEdit)
                        {
                            DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Изменить дату",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(ChangeDateKontrolStabilnosti_Click));
                            checkItem5.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem5);
                        }
                        if(UserSettings.VLKStabilnostiDelete)
                        {
                            DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(KontrolStabilnostiDeleteButton_Click));
                            checkItem3.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem3);
                        }                        
                        DXMenuCheckItem checkItem4 = new DXMenuCheckItem("&Обновить",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiOpdateRowClick));
                        checkItem4.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem4);
                    }
                }
                else if (Names[0] == "ПробыСтабильности")
                {
                    var Stroke = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle);
                    if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
                    {
                        int rowHandle = e.HitInfo.RowHandle;
                        e.Menu.Items.Clear();
                        if(UserSettings.VLKProbiAdd)
                        {
                            DXMenuCheckItem checkItem10 = new DXMenuCheckItem("&Добавить контрольную процедуру",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiAddClick));
                            checkItem10.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem10);
                        }
                        if(UserSettings.VLKProbiEdit)
                        {
                            DXMenuCheckItem checkItem1 = new DXMenuCheckItem("&Редактировать",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiEditClick));
                            checkItem1.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem1);
                        }
                        if(UserSettings.VLKProbiChangeData)
                        {
                            DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Записать результат анализа",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiAddDataButton_Click));
                            checkItem5.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem5);
                        }                        
                        if (Stroke.X1 != null && Stroke.X1 != 0 && Stroke.X2 != null && Stroke.X2 != 0)
                        {
                            DXMenuCheckItem checkItem6 = new DXMenuCheckItem("&Пределы",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(PredeliView_Click));
                            checkItem6.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem6);
                        }
                        if (Stroke._KontrolStabilnostiID != null && Stroke._KontrolStabilnostiID != 0)
                        {
                            if(UserSettings.VLKStabilnostiDiagrams)
                            {
                                DXMenuCheckItem checkItem15 = new DXMenuCheckItem("&Контроль повторяемости",
                                view.OptionsView.AllowCellMerge, null, new EventHandler(KontrolPovtorayamostiClick));
                                checkItem15.Tag = new RowInfo(view, rowHandle);
                                e.Menu.Items.Add(checkItem15);
                                DXMenuCheckItem checkItem16 = new DXMenuCheckItem("&Контроль внутрилабораторной прецизионности",
                                view.OptionsView.AllowCellMerge, null, new EventHandler(KontrolPrechizionnostiClick));
                                checkItem16.Tag = new RowInfo(view, rowHandle);
                                e.Menu.Items.Add(checkItem16);
                                DXMenuCheckItem checkItem17 = new DXMenuCheckItem("&Контроль погрешности",
                                view.OptionsView.AllowCellMerge, null, new EventHandler(KontrolPogreshnostiClick));
                                checkItem17.Tag = new RowInfo(view, rowHandle);
                                e.Menu.Items.Add(checkItem17);
                            }
                            if(UserSettings.VLKStabilnostiPrint)
                            {
                                DXMenuCheckItem checkItem18 = new DXMenuCheckItem("&Печать \"Контроль стабильности\"",
                                view.OptionsView.AllowCellMerge, null, new EventHandler(PrintKontrolStabilnostiClick));
                                checkItem18.Tag = new RowInfo(view, rowHandle);
                                e.Menu.Items.Add(checkItem18);
                                DXMenuCheckItem checkItem22 = new DXMenuCheckItem("&Печать \"Оценка и протокол ККШ\"",
                                view.OptionsView.AllowCellMerge, null, new EventHandler(PrintOchenkaKKSHClick));
                                checkItem22.Tag = new RowInfo(view, rowHandle);
                                e.Menu.Items.Add(checkItem22);
                                DXMenuCheckItem checkItem23 = new DXMenuCheckItem("&Печать \"Оценка ККШ\"",
                                view.OptionsView.AllowCellMerge, null, new EventHandler(PrintOchenkaKKSHClick1));
                                checkItem23.Tag = new RowInfo(view, rowHandle);
                                e.Menu.Items.Add(checkItem23);
                            }
                            if(UserSettings.VLKStabilnostiProtokolOchenka)
                            {
                                DXMenuCheckItem checkItem24 = new DXMenuCheckItem("&Печать \"Протокол ККШ\"",
                                view.OptionsView.AllowCellMerge, null, new EventHandler(PrintOchenkaKKSHClick2));
                                checkItem24.Tag = new RowInfo(view, rowHandle);
                                e.Menu.Items.Add(checkItem24);
                            }                            
                        }
                        if(UserSettings.VLKStabilnostiDelete)
                        {
                            DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                            view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiDeleteButton_Click));
                            checkItem3.Tag = new RowInfo(view, rowHandle);
                            e.Menu.Items.Add(checkItem3);
                        }                        
                        DXMenuCheckItem checkItem4 = new DXMenuCheckItem("&Обновить",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(ProbiOpdateRowClick));
                        checkItem4.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem4);
                    }
                }
            }
            catch (Exception ex) { LogErrors Log = new LogErrors(ex.ToString()); }
        }
        private void PrintProbiClick(object sender, EventArgs e)
        {
            PrintProbiClick();
        }
        private void PrintOperativniiKontrolClick()
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VLKTab.PrintOperativniiKontrolClick;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }
        private void PrintOperativniiKontrolClick(object sender, EventArgs e)
        {
            PrintOperativniiKontrolClick();
        }
        private void AddAnalizButton_Click(object sender, EventArgs e)
        {
            var Names = GSOSpisokTab.SelectedTabPage.Name.Split('&');
            if (Names.Length == 1)
            {
                if (GSOSpisokTab.SelectedTabPage.Name == "MainGSOTab")
                {
                    VLKTab.AddAnalizButton_Click(this);
                }
            }
            else if (Names[0] == "Пробы" || Names[0] == "ПробыОперативный" || Names[0] == "ПробыСтабильности")
            {
                VLKTab.AddProbiButton_Click(this);
            }
        }
        private void PrintKontrolStabilnostiClick(object sender, EventArgs e)
        {
            try
            {
                System.ComponentModel.BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VLKTab.PrintKontrolStabilnostiClick;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }
        private void KontrolPogreshnostiClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            KontrolStabilnosti Stroke = null;
            Probi Stroka = null;
            try
            {
                Stroke = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
            }
            catch { }
            try
            {
                Stroka = (Probi)gv.GetRow(gv.FocusedRowHandle);
            }
            catch
            {
                try
                {
                    var Strok = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle);
                    using (DbDataContext db = new DbDataContext())
                    {
                        Stroka = db.Probi.Where(c => c.Id == Strok.Id).FirstOrDefault();
                        db.Dispose();
                    }
                }
                catch { }
            }
            if (Stroke == null && Stroka == null) return;
            VLKTab.KontrolPogreshnostiClick(Stroke, Stroka);
        }
        private void KontrolPrechizionnostiClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            KontrolStabilnosti Stroke = null;
            Probi Stroka = null;
            try
            {
                Stroke = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
            }
            catch { }
            try
            {
                Stroka = (Probi)gv.GetRow(gv.FocusedRowHandle);
            }
            catch
            {
                try
                {
                    var Strok = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle);
                    using (DbDataContext db = new DbDataContext())
                    {
                        Stroka = db.Probi.Where(c => c.Id == Strok.Id).FirstOrDefault();
                        db.Dispose();
                    }
                }
                catch { }
            }
            if (Stroke == null && Stroka == null) return;
            VLKTab.KontrolPrechizionnostiClick(Stroke, Stroka);
        }
        private void KontrolPovtorayamostiClick(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                KontrolStabilnosti Stroke = null;
                Probi Stroka = null;
                try
                {
                    Stroke = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
                }
                catch { }
                try
                {
                    Stroka = (Probi)gv.GetRow(gv.FocusedRowHandle);
                }
                catch
                {
                    try
                    {
                        var Strok = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle);
                        using (DbDataContext db = new DbDataContext())
                        {
                            Stroka = db.Probi.Where(c => c.Id == Strok.Id).FirstOrDefault();
                            db.Dispose();
                        }
                    }
                    catch { }
                }
                if (Stroke == null && Stroka == null) return;
                VLKTab.KontrolPovtorayamostiClick(Stroke, Stroka);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void PredeliView_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = new Probi();
            try
            {
                Stroke = (Probi)gv.GetRow(gv.FocusedRowHandle);
            }
            catch
            {
                try
                {
                    var Stroka = (KontrolStabilnostiModel)gv.GetRow(gv.FocusedRowHandle);
                    using (DbDataContext db = new DbDataContext())
                    {
                        Stroke = db.Probi.Where(c => c.Id == Stroka.Id).FirstOrDefault();
                        db.Dispose();
                    }
                }
                catch { }
            }
            VLKTab.PredeliView_Click(this, Stroke);
        }
        private void KontrolStabilnosti_Click(object sender, EventArgs e)
        {
            VLKTab.KontrolStabilnosti_Click(this);
        }
        private void PrintOchenkaKKSHClick(object sender, EventArgs e)
        {
            try
            {
                VLKTab.VisiblePages = 12;
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VLKTab.PrintOchenkaKKSHClick;
                worker.RunWorkerAsync(this);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }
        private void PrintOchenkaKKSHClick1(object sender, EventArgs e)
        {
            try
            {
                VLKTab.VisiblePages = 1;
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VLKTab.PrintOchenkaKKSHClick;
                worker.RunWorkerAsync(this);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }
        private void PrintOchenkaKKSHClick2(object sender, EventArgs e)
        {
            try
            {
                VLKTab.VisiblePages = 2;
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VLKTab.PrintOchenkaKKSHClick;
                worker.RunWorkerAsync(this);
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }
        private void ViewOperativniiKontrolProbiClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (OperativniiKontrol)gv.GetRow(gv.FocusedRowHandle);
            VLKTab.ViewOperativniiKontrolProbiClick(this, Stroke);
        }
        private void ViewKontrolStabilnostiProbiClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
            VLKTab.ViewKontrolStabilnostiProbiClick(this, Stroke);
        }
        private void ChangeDateOperativniiKontrol_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (OperativniiKontrol)gv.GetRow(gv.FocusedRowHandle);
            VLKTab.ChangeDateOperativniiKontrol_Click(this, Stroke);
        }
        private void ChangeDateKontrolStabilnosti_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
            VLKTab.ChangeDateKontrolStabilnosti_Click(this, Stroke);
        }
        private void OperativniiKontrolDeleteButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (OperativniiKontrol)gv.GetRow(gv.FocusedRowHandle);
            VLKTab.OperativniiKontrolDeleteButton_Click(this, Stroke.Id);
        }
        private void KontrolStabilnostiDeleteButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
            VLKTab.KontrolStabilnostiDeleteButton_Click(this, Stroke.Id);
        }
        private void CloseOperativniiKontrolBookClick(object sender, EventArgs e)
        {
            GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (OperativniiKontrol)gv.GetRow(gv.FocusedRowHandle);
            VLKTab.CloseOperativniiKontrolBookClick(this, Stroke);
        }
        private void CloseKontrolStabilnostiBookClick(object sender, EventArgs e)
        {
            GridView gv = GSOGrid.FocusedView as GridView;
            var Stroke = (KontrolStabilnosti)gv.GetRow(gv.FocusedRowHandle);
            VLKTab.CloseKontrolStabilnostiBookClick(this, Stroke);
        }
        private void OperayivniiKontrol_Click(object sender, EventArgs e)
        {
            VLKTab.OperayivniiKontrol_Click(this);
        }
        private void ProbiAddClick(object sender, EventArgs e)
        {
            VLKTab.AddProbiButton_Click(this);
        }
        private void ProbiOpdateRowClick(object sender, EventArgs e)
        {
            VLKTab.UpdateGSOGrid(this);
        }
        private void ProbiDeleteButton_Click(object sender, EventArgs e)
        {
            VLKTab.ProbiDeleteButton_Click(this);
        }
        private void ProbiAddDataButton_Click(object sender, EventArgs e)
        {
            VLKTab.ProbiAddDataButton_Click(this);
        }
        private void ProbiEditClick(object sender, EventArgs e)
        {
            VLKTab.ProbiEditClick(this);
        }
        private void AnalizEditClick(object sender, EventArgs e)
        {
            GridView gv = GSOGrid.FocusedView as GridView;
            var Stroke = (AnalizVLK)gv.GetRow(gv.FocusedRowHandle);
            VLKTab.AnalizEditClick(this, Stroke.Id);
        }
        private void AnalizOpdateRowClick(object sender, EventArgs e)
        {
            VLKTab.UpdateGSOGrid(this);
        }
        private void AnalizDeleteButton_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GSOGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (AnalizVLK)gv.GetRow(gv.FocusedRowHandle);
            if (Stroke != null) VLKTab.AnalizDeleteButton_Click(this, Stroke.Id);
        }
        private void GSOGrid_DoubleClick(object sender, EventArgs e)
        {
            VLKTab.GSOGrid_DoubleClick(this);
        }
        private void GSOSpisokTab_CloseButtonClick(object sender, EventArgs e)
        {
            ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;
            var Tab = sender as XtraTabControl;            
            XtraTabPage pageToSelect = GSOSpisokTab.TabPages[0];
            Tab.TabPages.Remove(arg.Page as XtraTabPage);
            if (pageToSelect != null)
                GSOSpisokTab.SelectedTabPage = pageToSelect;
            VLKTab.UpdateGSOGrid(this);
        }
        private void GSOSpisokTab_Click(object sender, EventArgs e)
        {
            VLKTab.UpdateGSOGrid(this);
        }
        internal void GSOGridView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            try
            {
                 if (GSOSpisokTab.SelectedTabPage.Name == "MainGSOTab") return;
                var Names = GSOSpisokTab.SelectedTabPage.Name.Split('&');
                if (Names[0] == "Пробы" || Names[0] == "ПробыОперативный" || Names[0] == "ПробыСтабильности")
                {
                    e.Appearance.Options.UseBackColor = true;
                    var X1 = Convert.ToDecimal(GSOGridView.GetRowCellValue(e.RowHandle, GSOGridView.Columns.ColumnByName("colX1")));
                    var X2 = Convert.ToDecimal(GSOGridView.GetRowCellValue(e.RowHandle, GSOGridView.Columns.ColumnByName("colX2")));
                    if (X1 == 0 || X2 == 0)
                    {
                        e.Appearance.BackColor = System.Drawing.Color.Red;
                    }
                }
            }
            catch { }
        }
        private void VLKPrintButton_Click(object sender, EventArgs e)
        {
            var Names = GSOSpisokTab.SelectedTabPage.Name.Split('&');
            if (Names[0] == "ПробыСтабильности")
            {
                ContextMenuStrip ContextMS = new ContextMenuStrip();
                if(UserSettings.VLKStabilnostiPrint)
                {
                    ContextMS.Items.Add("&Печать \"Контроль стабильности\"", null, PrintKontrolStabilnostiClick);
                }
                if(UserSettings.VLKStabilnostiProtokolOchenka)
                {
                    ContextMS.Items.Add("&Печать \"Оценка и протокол ККШ\"", null, PrintOchenkaKKSHClick);
                    ContextMS.Items.Add("&Печать \"Оценка ККШ\"", null, PrintOchenkaKKSHClick1);
                    ContextMS.Items.Add("&Печать \"Протокол ККШ\"", null, PrintOchenkaKKSHClick2);
                }                
                ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);
            }
            else if (Names[0] == "ПробыОперативный")
            {               
                PrintOperativniiKontrolClick();
            }
            else if (Names[0] == "Пробы")
            {
                PrintProbiClick();
            }
        }
        private void PrintProbiClick()
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += VLKTab.PrintProbiClick;
                worker.RunWorkerAsync(this);
            }
            catch { }
        }
        private void VLKUpdateButton_Click(object sender, EventArgs e)
        {
            VLKTab.UpdateGSOGrid(this);
        }
        private void DiagramButton_Click(object sender, EventArgs e)
        {
            var Names = GSOSpisokTab.SelectedTabPage.Name.Split('&');
            if (Names[0] == "ПробыСтабильности")
            {
                ContextMenuStrip ContextMS = new ContextMenuStrip();
                ContextMS.Items.Add("&Контроль повторяемости", null, KontrolPovtorayamostiButtonClick);
                ContextMS.Items.Add("&Контроль внутрилабораторной прецизионности", null, KontrolPresizionnostiButtonClick);
                ContextMS.Items.Add("&Контроль погрешности", null, KontrolPogreshnostiButtonClick);
                ContextMS.Show(Cursor.Position.X, Cursor.Position.Y);
            }
        }
        private void KontrolPogreshnostiButtonClick(object sender, EventArgs e)
        {
            try
            {
                var Names = GSOSpisokTab.SelectedTabPage.Name.Split('&');
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroke = db.KontrolStabilnosti.Where(c => c.Id == Convert.ToInt32(Names[2])).FirstOrDefault();
                    if (Stroke == null) return;
                    VLKTab.KontrolPogreshnostiClick(Stroke, null);
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }
        private void KontrolPresizionnostiButtonClick(object sender, EventArgs e)
        {
            try
            {
                var Names = GSOSpisokTab.SelectedTabPage.Name.Split('&');
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroke = db.KontrolStabilnosti.Where(c => c.Id == Convert.ToInt32(Names[2])).FirstOrDefault();
                    if (Stroke == null) return;
                    VLKTab.KontrolPrechizionnostiClick(Stroke, null);
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }
        internal void GSOGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            VLKTab.GSOGridView_FocusedRowChanged(this);
        }
        private void CloseAllTabsButton_Click(object sender, EventArgs e)
        {
            VLKTab.CloseAllTabsButton_Click(this);
        }
        private void KontrolPovtorayamostiButtonClick(object sender, EventArgs e)
        {
            try
            {
                var Names = GSOSpisokTab.SelectedTabPage.Name.Split('&');
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroke = db.KontrolStabilnosti.Where(c => c.Id == Convert.ToInt32(Names[2])).FirstOrDefault();
                    if (Stroke == null) return;
                    VLKTab.KontrolPovtorayamostiClick(Stroke, null);
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }
        private void VLKCellMergeButton_Click(object sender, EventArgs e)
        {
            VLKTab.VLKCellMergeButton_Click(this);
        }
        private void FiltrButton_Click(object sender, EventArgs e)
        {
            VLKTab.FiltrButton_Click(this);
        }
        #endregion

        internal void GSOGridView_ColumnFilterChanged(object sender, EventArgs e)
        {
            VLKTab.GSOGridView_ColumnFilterChanged(this);
        }

        internal void EmptyEvent(object sender, EventArgs e)
        {            
        }

        private void ReactivGridView_RowClick(object sender, RowClickEventArgs e)
        {
            ReactivTab.ReactivGrid_Click(this);
        }

        private void PersonalGridView_RowClick(object sender, RowClickEventArgs e)
        {
            PersonalTab.PersonalGridControl_Click(this);
        }

        private void NTDGridView_RowClick(object sender, RowClickEventArgs e)
        {
            NTDTab.NTDGrid_Click(this);
        }

        private void SIGridView_RowClick(object sender, RowClickEventArgs e)
        {
            SITab.SIGrid_Click(this);
        }

        private void IOGridView_RowClick(object sender, RowClickEventArgs e)
        {
            IOTab.IOGrid_Click(this);
        }

        private void VOGridView_RowClick(object sender, RowClickEventArgs e)
        {
            VOTab.VOGridClick(this);
        }

        private void SIGridView_StartSorting(object sender, EventArgs e)
        {
            SITab.SIGridView_StartSorting(this);            
        }

        private void IOGridView_StartSorting(object sender, EventArgs e)
        {
            IOTab.IOGridView_StartSorting(this);
        }

        private void VOGridView_StartSorting(object sender, EventArgs e)
        {
            VOTab.VOGridView_StartSorting(this);
        }

        private void ReactivGridView_StartSorting(object sender, EventArgs e)
        {
            ReactivTab.ReactivGridView_StartSorting(this);
        }

        private void GSOGridView_StartSorting(object sender, EventArgs e)
        {
            VLKTab.GSOGridView_StartSorting(this);
        }

        private void GSOGridView_MouseUp(object sender, MouseEventArgs e)
        {
            GridHitInfo hitInfo = GSOGridView.CalcHitInfo(new Point(e.X, e.Y));
            if (hitInfo.InColumn)//клик на заголовок столбца
            {

            }
            else
                if (!hitInfo.InRow)//клик на бэнд
                {
                long sumWidth = 0;                
                try
                {
                    var Names = GSOSpisokTab.SelectedTabPage.Name.Split('&');
                    if (Names.Length > 1 && Names[0] == "ПробыСтабильности")
                    {
                        foreach (DevExpress.XtraGrid.Views.BandedGrid.GridBand b in GSOGridView.Bands)
                        {
                            if (hitInfo.HitPoint.X < sumWidth + b.VisibleWidth &&
                                hitInfo.HitPoint.X >= sumWidth &&
                                hitInfo.HitPoint.Y >= 0 &&
                                hitInfo.HitPoint.Y < GSOGridView.DetailHeight)
                            {
                                if (b.Caption == "Шифр пробы")
                                {
                                    if (VLKTab.SortOrderProbiStabilnosti == DevExpress.Data.ColumnSortOrder.Ascending)
                                        VLKTab.SortOrderProbiStabilnosti = DevExpress.Data.ColumnSortOrder.Descending;
                                    else
                                        VLKTab.SortOrderProbiStabilnosti = DevExpress.Data.ColumnSortOrder.Ascending;
                                }
                                VLKTab.UpdateGSOGrid(this);

                            }
                            sumWidth = sumWidth + b.VisibleWidth;
                        }
                    }
                }
                catch(Exception ex)
                {
                    LogErrors Log = new LogErrors(ex.ToString());
                }
            }
        }
    }
}
