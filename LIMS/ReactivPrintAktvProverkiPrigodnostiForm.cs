﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class ReactivPrintAktvProverkiPrigodnostiForm : DevExpress.XtraEditors.XtraForm
    {
        MainForm Main;
        WordDocument WordDoc1;
        bool Changed = false;
        int? ReactivID;
        public ReactivPrintAktvProverkiPrigodnostiForm(MainForm MF, int id)
        {
            try
            {
                InitializeComponent();

                ReactivID = id;
                var Personals = GlobalStatic.GetPersonal();
                IspolnitelTabl1.DataSource = Personals;
                IspolnitelTabl2.DataSource = Personals;
                FIO.DataSource = Personals;
                FIO.Text = "";
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroke = db.Reactiv.Where(c => c.Id == id).FirstOrDefault();
                    if (Stroke != null)
                    {
                        Main = MF;
                        try
                        {
                            var AktEnd = db.ReactivAktProverkiPrigodnosti.Where(c => c.Дата > new DateTime(DateTime.Now.Year, 1, 1) && c.Дата < new DateTime(DateTime.Now.Year, 12, 31)).OrderByDescending(c => c.Дата).FirstOrDefault();
                            if (AktEnd != null)
                            {
                                N.Text = (Convert.ToInt32(AktEnd.N) + 1).ToString() + "-" + DateTime.Now.Year.ToString().Substring(2);
                            }
                            else
                            {
                                N.Text = "1-" + DateTime.Now.Year.ToString().Substring(2);
                            }
                        } catch(Exception ex) { LogErrors Log = new LogErrors(ex.ToString()); }
                        Ot.Text = DateTime.Now.ToShortDateString() + "г.";
                        ReactivName.Text = Stroke.НаименованиеРеактива;
                        Kvalif.Text = Stroke.Классификация;
                        ND.Text = Stroke.НДНаРеактив;
                        Partia.Text = Stroke.НомерПартии;
                        if (Stroke.ДатаИзготовления.HasValue)
                        {
                            DateIzgotovlenia.Value = Stroke.ДатаИзготовления.Value;
                            DateIzgotovlenia.Checked = true;
                        }
                        DateOkonSrokHran.Text = Stroke.СрокГодностиDate.HasValue ? Stroke.СрокГодностиDate.Value.ToShortDateString() : "";
                        NDiMVI.Text = Stroke.Назначение;
                        var NewSrokGodnosti = Stroke.СрокГодностиDate.HasValue ? Stroke.СрокГодностиDate.Value.AddYears(1).ToShortDateString() : "";
                        if (Stroke.Назначение != "")
                        {
                            var MVI = Stroke.Назначение.Split(' ');
                            if (MVI.Length > 1)
                            {
                                if (MVI[0] == "ГОСТ")
                                {
                                    if (MVI.Length > 2)
                                    {
                                        if (MVI[1] == "Р")
                                        {
                                            Zakluchenie.Text = "реактив годен для применения по " + MVI[0] + " " + MVI[1] + " " + MVI[2] + " до " + NewSrokGodnosti + "г.";
                                        }
                                        else Zakluchenie.Text = "реактив годен для применения по " + MVI[0] + " " + MVI[1] + " до " + NewSrokGodnosti + "г.";
                                    }
                                    else if (MVI[1] == "Р") Zakluchenie.Text = "реактив годен для применения по " + MVI[0] + " " + MVI[1] + "" + MVI[2] + " до " + NewSrokGodnosti + "г.";
                                    else Zakluchenie.Text = "реактив годен для применения по " + MVI[0] + " " + MVI[1] + " до " + NewSrokGodnosti + "г.";
                                }
                                else if (MVI[0] == "МВИ") Zakluchenie.Text = "реактив годен для применения по МВИ до " + NewSrokGodnosti + "г.";
                                else Zakluchenie.Text = "реактив годен для применения до " + NewSrokGodnosti + "г.";
                            }
                        }
                        else Zakluchenie.Text = "реактив годен для применения по до " + NewSrokGodnosti + "г.";
                    }
                    db.Dispose();
                }
                AktCreateDate.Text = DateTime.Now.ToString();
                TextWasChanged();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public ReactivPrintAktvProverkiPrigodnostiForm(MainForm MF, ReactivAktProverkiPrigodnosti Stroke)
        {
            try
            {
                InitializeComponent();
                var Personals = GlobalStatic.GetPersonal();
                IspolnitelTabl1.DataSource = Personals;
                IspolnitelTabl2.DataSource = Personals;
                FIO.DataSource = Personals;
                using (DbDataContext db = new DbDataContext())
                {
                    if (Stroke.ReactivID.HasValue)
                    {
                        ReactivID = Stroke.ReactivID.Value;
                    }
                    else
                    {
                        var ReactivThis = db.Reactiv.Where(c => c.НаименованиеРеактива == Stroke.Наименование && c.Классификация == Stroke.Квалификация && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                        if (ReactivThis != null) ReactivID = ReactivThis.Id; else ReactivID = null;
                    }
                    if (Stroke != null)
                    {
                        ID.Text = Stroke.Id.ToString();
                        Main = MF;
                        AktCreateDate.Text = Stroke.Дата.ToString();
                        N.Text = Stroke.N;
                        Ot.Text = Stroke.Дата.HasValue ? Stroke.Дата.Value.ToShortDateString() + "г." : "";
                        ReactivName.Text = Stroke.Наименование;
                        Kvalif.Text = Stroke.Квалификация;
                        ND.Text = Stroke.ND;
                        Partia.Text = Stroke.НомерПартии;
                        if (Stroke.ДатаИзготовления.HasValue)
                        {
                            DateIzgotovlenia.Value = Stroke.ДатаИзготовления.Value;
                            DateIzgotovlenia.Checked = true;
                        }
                        DateOkonSrokHran.Text = Stroke.ДатаОкончанияСрокаХранения.HasValue ? Stroke.ДатаОкончанияСрокаХранения.Value.ToShortDateString() : "";
                        NDiMVI.Text = Stroke.НДиМВИ;
                        Zakluchenie.Text = Stroke.Заключение;
                        var Obrazech = db.ReactivAktProverkiPrigodnostiObrazech.Where(c => c.MainID == Stroke.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderBy(c => c.Id).ToList();
                        if (Obrazech.Count > 0 && Obrazech != null)
                        {
                            if (Obrazech.Count > 1)
                            {
                                ObrazechDlyaControla2.Text = Obrazech[1].ОбразецДляКонтроля;
                            }
                            if (Obrazech.Count > 0)
                            {
                                ObrazechDlyaControla1.Text = Obrazech[0].ОбразецДляКонтроля;
                            }
                            if (Obrazech.Count > 2)
                            {
                                ObrazechDlyaControla3.Text = Obrazech[2].ОбразецДляКонтроля;
                            }
                        }
                        var Table = db.ReactivAktProverkiPrigodnostiTable.Where(c => c.MainID == Stroke.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderBy(c => c.Id).ToList();
                        if (Table != null && Table.Count > 0)
                        {
                            if (Table.Count > 1)
                            {
                                AktProverkaPrigodnostiGridView.Rows.Add(Table.Count);
                            }
                            for (int rows = 0; rows < Table.Count; rows++)
                            {
                                AktProverkaPrigodnostiGridView.Rows[rows].Cells[0].Value = Table[rows].ДатаПроведенияИзмерения;
                                var DS1 = (List<string>)IspolnitelTabl1.DataSource;
                                if (!DS1.Contains(Table[rows].Исполнитель1))
                                {
                                    DS1.Add(Table[rows].Исполнитель1);
                                    IspolnitelTabl1.DataSource = DS1;
                                }
                                var DS2 = (List<string>)IspolnitelTabl2.DataSource;
                                if (!DS2.Contains(Table[rows].Исполнитель2))
                                {
                                    DS2.Add(Table[rows].Исполнитель2);
                                    IspolnitelTabl1.DataSource = DS2;
                                }
                                AktProverkaPrigodnostiGridView.Rows[rows].Cells[1].Value = Table[rows].Исполнитель1;
                                AktProverkaPrigodnostiGridView.Rows[rows].Cells[2].Value = Table[rows].Исполнитель2;
                                AktProverkaPrigodnostiGridView.Rows[rows].Cells[3].Value = Table[rows].АттестованноеЗначение;
                                AktProverkaPrigodnostiGridView.Rows[rows].Cells[4].Value = Table[rows].РезультатИзмеренийМассовойКонцентрацииКомпонентаВИсходнойПробе;
                                AktProverkaPrigodnostiGridView.Rows[rows].Cells[5].Value = Table[rows].РезультатКонтрольнойПроцедуры;
                                AktProverkaPrigodnostiGridView.Rows[rows].Cells[6].Value = Table[rows].НормативКонтроля;
                            }
                        }
                        var Vipolnil = db.ReactivAktProverkiPrigodnostiVipolnil.Where(c => c.MainID == Stroke.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                        if (Vipolnil != null)
                        {
                            Dolchnost.Text = Vipolnil.Должность;
                            FIO.Text = Vipolnil.ФИО;
                        }
                        else
                        {
                            Dolchnost.Text = "";
                            Dolchnost.Text = "";
                        }
                    }
                    db.Dispose();
                }
                TextWasChanged();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void TextWasChanged()
        {
            foreach (Control x in panelControl1.Controls)
            {
                if (x is TextBox || x is ComboBox)
                {
                    x.TextChanged += X_TextChanged;
                }
            }
        }

        private void X_TextChanged(object sender, EventArgs e)
        {
            Changed = true;
        }

        private void Replace(ref WordDocument WordDoc)
        {
            string NText = "";
            Invoke(new System.Action(() => NText = N.Text));
            string OtText = "";
            Invoke(new System.Action(() => OtText = Ot.Text));
            string ReactivNameText = "";
            Invoke(new System.Action(() => ReactivNameText = ReactivName.Text));
            string KvalifText = "";
            Invoke(new System.Action(() => KvalifText = Kvalif.Text));
            string NDText = "";
            Invoke(new System.Action(() => NDText = ND.Text));
            string DolchnostText = "";
            Invoke(new System.Action(() => DolchnostText = Dolchnost.Text));
            string FIOText = "";
            Invoke(new System.Action(() => FIOText = FIO.Text));
            WordDoc.ReplaceString("{НомерАкта}", NText);
            WordDoc.ReplaceString("{ДатаАкта}", OtText);
            WordDoc.ReplaceString("{НаименованиеРеактива}", ReactivNameText + " " + KvalifText);
            WordDoc.ReplaceString("{НДРеактива}", NDText);
            WordDoc.ReplaceString("{Составил}", DolchnostText);
            WordDoc.ReplaceString("{ФИО}", FIOText);
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PrintButton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveAkt();
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                PrintButton.Enabled = false;
                SaveButton.Enabled = false;
                Loader.Visible = true;
                worker.DoWork += Print;
                worker.RunWorkerAsync();
            }
            catch { }
        }

        private void Print(object sender, DoWorkEventArgs e)
        {
            try
            {
                Invoke(new System.Action(() => Main.TopMost = false));
                WordDoc1 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Акт проверки пригодности.dotx");
                Replace(ref WordDoc1);
                EditTable(ref WordDoc1);
                WordDoc1.TableResize(1);
                WordDoc1.Visible = true;
                WordDoc1.wordApplication.Activate();
                Invoke(new System.Action(() => Main.TopMost = false));
                Changed = false;
            }
            catch (Exception ex)
            {
                try
                {
                    LogErrors Log = new LogErrors(ex.ToString());
                    Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    WordDoc1.Close();
                }
                catch { }
            }
            finally
            {
                try
                {
                    Invoke(new System.Action(() => Loader.Visible = false));
                    Invoke(new System.Action(() => PrintButton.Enabled = true));
                    Invoke(new System.Action(() => SaveButton.Enabled = true));
                }
                catch { }
            }
        }

        private void EditTable(ref WordDocument WordDoc)
        {
            List<string> GridData = GetGridData();
            if (GridData.Count / 7 > 1)
            {
                WordDoc.AddTableCell(1, GridData.Count / 7 - 1);
            }
            if (GridData != null && GridData.Count > 0)
            {
                for (int Row = 2; Row < GridData.Count / 7 + 2; Row++)
                {
                    for (int Cell = 1; Cell < 7; Cell++)
                    {
                        if (Cell == 1) WordDoc.AddTableValue(1, Row, Cell + 4, GridData[(Row - 2) * 7 + (Cell - 1)]);
                        else if (Cell == 2) WordDoc.AddTableValue(1, Row, Cell + 4, GridData[(Row - 2) * 7 + (Cell - 1)] + "\r" + GridData[(Row - 2) * 7 + Cell]);
                        else WordDoc.AddTableValue(1, Row, Cell + 4, GridData[(Row - 2) * 7 + Cell]);
                    }
                }
            }
            string KvalifText = "";
            Invoke(new System.Action(() => KvalifText = Kvalif.Text));
            string ReactivNameText = "";
            Invoke(new System.Action(() => ReactivNameText = ReactivName.Text));
            string NDText = "";
            Invoke(new System.Action(() => NDText = ND.Text));
            string DateOkonSrokHranText = "";
            Invoke(new System.Action(() => DateOkonSrokHranText = DateOkonSrokHran.Text));
            string NDiMVIText = "";
            Invoke(new System.Action(() => NDiMVIText = NDiMVI.Text));
            string ObrazechDlyaControla = "";
            Invoke(new System.Action(() => ObrazechDlyaControla = ObrazechDlyaControla1.Text));
            string ObrazechDlyaControla2Text = "";
            Invoke(new System.Action(() => ObrazechDlyaControla2Text = ObrazechDlyaControla2.Text));
            string ObrazechDlyaControla3Text = "";
            Invoke(new System.Action(() => ObrazechDlyaControla3Text = ObrazechDlyaControla3.Text));
            string DateIzgotovleniaText = "";
            Invoke(new System.Action(() => DateIzgotovleniaText = DateIzgotovlenia.Checked ? (" от " + DateIzgotovlenia.Text + "г.") : ""));
            WordDoc.AddTableValue(1, 2, 1, ReactivNameText + " (" + KvalifText + ") " + NDText + " партия №" + Partia.Text + DateIzgotovleniaText);
            WordDoc.AddTableValue(1, 2, 2, DateOkonSrokHranText);
            WordDoc.AddTableValue(1, 2, 3, NDiMVIText);
            //var ObrazechDlyaControla = ObrazechDlyaControla1.Text;
            ObrazechDlyaControla = ObrazechDlyaControla2Text == "" ? ObrazechDlyaControla : ObrazechDlyaControla + "\r" + ObrazechDlyaControla2Text;
            ObrazechDlyaControla = ObrazechDlyaControla3Text == "" ? ObrazechDlyaControla : ObrazechDlyaControla + "\r" + ObrazechDlyaControla3Text;
            WordDoc.AddTableValue(1, 2, 4, ObrazechDlyaControla);
            string ZakluchenieText = "";
            Invoke(new System.Action(() => ZakluchenieText = Zakluchenie.Text));
            WordDoc.AddTableValue(1, 2, 11, ZakluchenieText);
            if (GridData.Count / 7 > 1)
            {
                for (int Row = 1; Row < 5; Row++)
                {
                    WordDoc.MergeCells(1, 2, GridData.Count / 7 + 1, Row);
                }
                WordDoc.MergeCells(1, 2, GridData.Count / 7 + 1, 11);
            }
        }

        private List<string> GetGridData()
        {
            List<string> GridData = new List<string>();
            foreach (DataGridViewRow row in AktProverkaPrigodnostiGridView.Rows)
            {
                var Rows = AktProverkaPrigodnostiGridView.Rows.Count;
                if (Rows - 1 > row.Index)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        string value = cell.Value == null ? "" : cell.Value.ToString();
                        GridData.Add(value);
                    }
                }
            }

            bool LastRowNull = true;
            do
            {
                var count = GridData.Count;
                if (count > 0)
                {
                    for (int i = count - 1; i > count - 8; i--)
                    {
                        LastRowNull = LastRowNull == true ? GridData[i] == "" : false;
                    }
                    if (LastRowNull)
                    {
                        for (int i = count - 1; i > count - 8; i--)
                        {
                            GridData.RemoveAt(i);
                        }
                    }
                }
                else LastRowNull = false;
            }
            while (LastRowNull);
            return GridData;
        }

        private void ReactivPrintAktvRegistResultForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Changed)
            {
                DialogResult dialogResult = MessageBox.Show("Сохранить изменения?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    SaveAkt();
                }
            }
            Owner.Visible = true;
            Close();
        }

        private void ReactivPrintAktvProverkiPrigodnostiForm_Load(object sender, EventArgs e)
        {
            ContextMenuEvent();
        }

        private void ContextMenuEvent()
        {
            foreach (Control C in panelControl1.Controls)
            {
                if (C is TextBox || C is ComboBox)
                {
                    C.ContextMenuStrip = Main.ContextMenuStripMain;
                }
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            SaveAkt(true);
        }

        private void SaveAkt(bool ShowMessage = false)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    if (ID.Text != "") //Редактирование
                    {
                        // изменение Основные данные
                        var MainID = 0;
                        var HistoryMainID = 0;
                        var Stroka = db.ReactivAktProverkiPrigodnosti.Where(c => c.Id == Convert.ToInt32(ID.Text) && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                        var NextID = db.ReactivAktProverkiPrigodnosti != null && db.ReactivAktProverkiPrigodnosti.Count() > 0 ? db.ReactivAktProverkiPrigodnosti.Max(c => c.Id) + 1 : 1;
                        if (Stroka != null)
                        {
                            Stroka.Id = Convert.ToInt32(ID.Text);
                            DateTime? ДатаСрокаХранения;
                            if (DateOkonSrokHran.Text == "")
                                ДатаСрокаХранения = null;
                            else
                                ДатаСрокаХранения = Convert.ToDateTime(DateOkonSrokHran.Text).Date;

                            Stroka.Дата = AktCreateDate.Value;
                            Stroka.N = N.Text;
                            Stroka.Наименование = ReactivName.Text;
                            Stroka.Квалификация = Kvalif.Text;
                            Stroka.ND = ND.Text;
                            Stroka.НомерПартии = Partia.Text;
                            if (DateIzgotovlenia.Checked)
                            {
                                Stroka.ДатаИзготовления = DateIzgotovlenia.Value;
                            }
                            else Stroka.ДатаИзготовления = null;
                            Stroka.ДатаОкончанияСрокаХранения = ДатаСрокаХранения;
                            Stroka.НДиМВИ = NDiMVI.Text;
                            Stroka.Заключение = Zakluchenie.Text;
                            MainID = Stroka.Id;
                            Stroka.UserName = UserSettings.User;
                            Stroka.VisibleStatus = 0;
                            Stroka.ДатаСозданияЗаписи = DateTime.Now;
                            Stroka.Owner = null;
                            Stroka.ReactivID = ReactivID;
                            HistoryMainID = NextID;
                            HistoryReactivAktProverkiPrigodnosti(Stroka, 200, NextID);
                        }
                        else
                        {
                            var NN = N.Text.Split('-');
                            DateTime? ДатаСрокаХранения;
                            if (DateOkonSrokHran.Text == "")
                                ДатаСрокаХранения = null;
                            else
                                ДатаСрокаХранения = Convert.ToDateTime(DateOkonSrokHran.Text).Date;
                            DateTime? DI = null;
                            if (DateIzgotovlenia.Checked)
                            {
                                DI = DateIzgotovlenia.Value;
                            }
                            db.ReactivAktProverkiPrigodnosti.InsertOnSubmit(new ReactivAktProverkiPrigodnosti
                            {
                                Id = NextID,
                                Дата = AktCreateDate.Value,
                                N = N.Text,
                                Наименование = ReactivName.Text,
                                Квалификация = Kvalif.Text,
                                ND = ND.Text,
                                НомерПартии = Partia.Text,
                                ДатаИзготовления = DI,
                                ДатаОкончанияСрокаХранения = ДатаСрокаХранения,
                                НДиМВИ = NDiMVI.Text,
                                Заключение = Zakluchenie.Text,
                                UserName = UserSettings.User,
                                VisibleStatus = 0,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = null,
                                ReactivID = ReactivID,
                            });
                            db.ReactivAktProverkiPrigodnosti.InsertOnSubmit(new ReactivAktProverkiPrigodnosti
                            {
                                Id = NextID + 1,
                                Дата = AktCreateDate.Value,
                                N = N.Text,
                                Наименование = ReactivName.Text,
                                Квалификация = Kvalif.Text,
                                ND = ND.Text,
                                НомерПартии = Partia.Text,
                                ДатаИзготовления = DI,
                                ДатаОкончанияСрокаХранения = ДатаСрокаХранения,
                                НДиМВИ = NDiMVI.Text,
                                Заключение = Zakluchenie.Text,
                                UserName = UserSettings.User,
                                VisibleStatus = 100,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = NextID,
                                ReactivID = ReactivID
                            });
                            MainID = NextID;
                            HistoryMainID = NextID + 1;
                        }
                        //Изменение Образец для контроля
                        var Obrazech = db.ReactivAktProverkiPrigodnostiObrazech.Where(c => c.MainID == MainID && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderBy(c => c.Id).ToList();
                        var NextIDOther = db.ReactivAktProverkiPrigodnostiObrazech != null && db.ReactivAktProverkiPrigodnostiObrazech.Count() > 0 ? db.ReactivAktProverkiPrigodnostiObrazech.Max(c => c.Id) + 1 : 1;
                        if (ObrazechDlyaControla1.Text != "")
                        {
                            if (Obrazech.Count > 0)
                            {
                                Obrazech[0].Id = Obrazech[0].Id;
                                Obrazech[0].MainID = MainID;
                                Obrazech[0].ОбразецДляКонтроля = ObrazechDlyaControla1.Text;
                                Obrazech[0].UserName = UserSettings.User;
                                Obrazech[0].VisibleStatus = 0;
                                Obrazech[0].ДатаСозданияЗаписи = DateTime.Now;
                                Obrazech[0].Owner = null;
                                HistoryReactivAktProverkiPrigodnostiObrazech(Obrazech[0], 200, NextIDOther, HistoryMainID);
                                NextIDOther++;
                            }
                            else
                            {
                                db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                                {
                                    Id = NextIDOther,
                                    MainID = MainID,
                                    ОбразецДляКонтроля = ObrazechDlyaControla1.Text,
                                    UserName = UserSettings.User,
                                    VisibleStatus = 0,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null
                                });
                                NextIDOther++;
                                db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                                {
                                    Id = NextIDOther,
                                    MainID = HistoryMainID,
                                    ОбразецДляКонтроля = ObrazechDlyaControla1.Text,
                                    UserName = UserSettings.User,
                                    VisibleStatus = 100,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDOther - 1,
                                });
                                NextIDOther++;
                            }
                        }
                        else
                        {
                            if (Obrazech.Count > 0)
                            {
                                Obrazech[0].VisibleStatus = Obrazech[0].Owner == null ? 300 : 301;
                                Obrazech[0].UserName = UserSettings.User;
                                Obrazech[0].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktProverkiPrigodnostiObrazech.DeleteOnSubmit(Obrazech[0]);
                            }
                        }
                        if (ObrazechDlyaControla2.Text != "")
                        {
                            if (Obrazech.Count > 1)
                            {
                                Obrazech[1].Id = Obrazech[1].Id;
                                Obrazech[1].MainID = MainID;
                                Obrazech[1].ОбразецДляКонтроля = ObrazechDlyaControla2.Text;
                                Obrazech[1].UserName = UserSettings.User;
                                Obrazech[1].VisibleStatus = 0;
                                Obrazech[1].ДатаСозданияЗаписи = DateTime.Now;
                                Obrazech[1].Owner = null;
                                HistoryReactivAktProverkiPrigodnostiObrazech(Obrazech[1], 200, NextIDOther, HistoryMainID);
                                NextIDOther++;
                            }
                            else
                            {
                                db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                                {
                                    Id = NextIDOther,
                                    MainID = MainID,
                                    ОбразецДляКонтроля = ObrazechDlyaControla2.Text,
                                    UserName = UserSettings.User,
                                    VisibleStatus = 0,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                NextIDOther++;
                                db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                                {
                                    Id = NextIDOther,
                                    MainID = HistoryMainID,
                                    ОбразецДляКонтроля = ObrazechDlyaControla1.Text,
                                    UserName = UserSettings.User,
                                    VisibleStatus = 100,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDOther - 1,
                                });
                                NextIDOther++;
                            }
                        }
                        else
                        {
                            if (Obrazech.Count > 1)
                            {
                                Obrazech[1].VisibleStatus = Obrazech[1].Owner == null ? 300 : 301;
                                Obrazech[1].UserName = UserSettings.User;
                                Obrazech[1].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktProverkiPrigodnostiObrazech.DeleteOnSubmit(Obrazech[1]);
                            }
                        }
                        if (ObrazechDlyaControla3.Text != "")
                        {
                            if (Obrazech.Count > 2)
                            {
                                Obrazech[2].Id = Obrazech[2].Id;
                                Obrazech[2].MainID = MainID;
                                Obrazech[2].ОбразецДляКонтроля = ObrazechDlyaControla3.Text;
                                Obrazech[2].UserName = UserSettings.User;
                                Obrazech[2].VisibleStatus = 0;
                                Obrazech[2].ДатаСозданияЗаписи = DateTime.Now;
                                Obrazech[2].Owner = null;
                                HistoryReactivAktProverkiPrigodnostiObrazech(Obrazech[2], 200, NextIDOther, HistoryMainID);
                                NextIDOther++;
                            }
                            else
                            {
                                db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                                {
                                    Id = NextIDOther,
                                    MainID = MainID,
                                    ОбразецДляКонтроля = ObrazechDlyaControla3.Text,
                                    UserName = UserSettings.User,
                                    VisibleStatus = 0,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                NextIDOther++;
                                db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                                {
                                    Id = NextIDOther,
                                    MainID = HistoryMainID,
                                    ОбразецДляКонтроля = ObrazechDlyaControla1.Text,
                                    UserName = UserSettings.User,
                                    VisibleStatus = 100,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDOther - 1,
                                });
                                NextIDOther++;
                            }
                        }
                        else
                        {
                            if (Obrazech.Count > 2)
                            {
                                Obrazech[2].VisibleStatus = Obrazech[2].Owner == null ? 300 : 301;
                                Obrazech[2].UserName = UserSettings.User;
                                Obrazech[2].ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktProverkiPrigodnostiObrazech.DeleteOnSubmit(Obrazech[2]);
                            }
                        }
                        //Изменение табличной части
                        List<string> GridData = GetGridData();
                        var NextIDTable = db.ReactivAktProverkiPrigodnostiTable != null && db.ReactivAktProverkiPrigodnostiTable.Count() > 0 ? db.ReactivAktProverkiPrigodnostiTable.Max(c => c.Id) + 1 : 0;
                        var AktTable = db.ReactivAktProverkiPrigodnostiTable.Where(c => c.MainID == MainID && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderBy(c => c.Id).ToList();

                        if (GridData.Count > 0)
                        {
                            int n = 0;
                            while (GridData.Count > 7 * n + 1)
                            {
                                if (AktTable.Count > n)
                                {
                                    AktTable[n].Id = AktTable[n].Id;
                                    AktTable[n].MainID = MainID;
                                    AktTable[n].ДатаПроведенияИзмерения = GridData[7 * n];
                                    AktTable[n].Исполнитель1 = GridData[7 * n + 1];
                                    AktTable[n].Исполнитель2 = GridData[7 * n + 2];
                                    AktTable[n].АттестованноеЗначение = GridData[7 * n + 3];
                                    AktTable[n].РезультатИзмеренийМассовойКонцентрацииКомпонентаВИсходнойПробе = GridData[7 * n + 4];
                                    AktTable[n].РезультатКонтрольнойПроцедуры = GridData[7 * n + 5];
                                    AktTable[n].НормативКонтроля = GridData[7 * n + 6];
                                    AktTable[n].VisibleStatus = 0;
                                    AktTable[n].UserName = UserSettings.User;
                                    AktTable[n].ДатаСозданияЗаписи = DateTime.Now;
                                    AktTable[n].Owner = null;
                                    HistoryReactivAktProverkiPrigodnostiTable(AktTable[n], 200, NextIDTable, HistoryMainID);
                                    NextIDTable++;
                                }
                                else
                                {
                                    db.ReactivAktProverkiPrigodnostiTable.InsertOnSubmit(new ReactivAktProverkiPrigodnostiTable
                                    {
                                        Id = NextIDTable,
                                        MainID = MainID,
                                        ДатаПроведенияИзмерения = GridData[7 * n],
                                        Исполнитель1 = GridData[7 * n + 1],
                                        Исполнитель2 = GridData[7 * n + 2],
                                        АттестованноеЗначение = GridData[7 * n + 3],
                                        РезультатИзмеренийМассовойКонцентрацииКомпонентаВИсходнойПробе = GridData[7 * n + 4],
                                        РезультатКонтрольнойПроцедуры = GridData[7 * n + 5],
                                        НормативКонтроля = GridData[7 * n + 6],
                                        VisibleStatus = 0,
                                        UserName = UserSettings.User,
                                        ДатаСозданияЗаписи = DateTime.Now,
                                        Owner = null,
                                    });
                                    NextIDTable++;
                                    db.ReactivAktProverkiPrigodnostiTable.InsertOnSubmit(new ReactivAktProverkiPrigodnostiTable
                                    {
                                        Id = NextIDTable,
                                        MainID = HistoryMainID,
                                        ДатаПроведенияИзмерения = GridData[7 * n],
                                        Исполнитель1 = GridData[7 * n + 1],
                                        Исполнитель2 = GridData[7 * n + 2],
                                        АттестованноеЗначение = GridData[7 * n + 3],
                                        РезультатИзмеренийМассовойКонцентрацииКомпонентаВИсходнойПробе = GridData[7 * n + 4],
                                        РезультатКонтрольнойПроцедуры = GridData[7 * n + 5],
                                        НормативКонтроля = GridData[7 * n + 6],
                                        VisibleStatus = 100,
                                        UserName = UserSettings.User,
                                        ДатаСозданияЗаписи = DateTime.Now,
                                        Owner = NextIDTable - 1,
                                    });
                                    NextIDTable++;
                                }
                                n++;
                            }
                            if (AktTable.Count > GridData.Count / 7)
                            {
                                for (int i = GridData.Count / 7; AktTable.Count > i; i++)
                                {
                                    AktTable[i].VisibleStatus = AktTable[i].Owner == null ? 300 : 301;
                                    AktTable[i].UserName = UserSettings.User;
                                    AktTable[i].ДатаСозданияЗаписи = DateTime.Now;
                                    //db.ReactivAktProverkiPrigodnostiTable.DeleteOnSubmit(AktTable[i]);
                                }
                            }
                        }
                        //Изменение Vipolnil
                        var NextIDVipolnil = db.ReactivAktProverkiPrigodnostiVipolnil != null && db.ReactivAktProverkiPrigodnostiVipolnil.Count() > 0 ? db.ReactivAktProverkiPrigodnostiVipolnil.Max(c => c.Id) + 1 : 1;
                        var Vipolnil = db.ReactivAktProverkiPrigodnostiVipolnil.Where(c => c.MainID == MainID && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();

                        if (Dolchnost.Text != "" || FIO.Text != "")
                        {
                            if (Vipolnil != null)
                            {
                                Vipolnil.Id = Vipolnil.Id;
                                Vipolnil.MainID = MainID;
                                Vipolnil.Должность = Dolchnost.Text;
                                Vipolnil.ФИО = FIO.Text;
                                Vipolnil.VisibleStatus = 0;
                                Vipolnil.UserName = UserSettings.User;
                                Vipolnil.ДатаСозданияЗаписи = DateTime.Now;
                                Vipolnil.Owner = null;

                                db.ReactivAktProverkiPrigodnostiVipolnil.InsertOnSubmit(new ReactivAktProverkiPrigodnostiVipolnil
                                {
                                    Id = NextIDVipolnil + 1,
                                    MainID = HistoryMainID,
                                    Должность = Vipolnil.Должность,
                                    ФИО = Vipolnil.ФИО,
                                    VisibleStatus = 200,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = Vipolnil.Id,
                                });
                                //NextIDVipolnil++;
                            }
                            else
                            {
                                db.ReactivAktProverkiPrigodnostiVipolnil.InsertOnSubmit(new ReactivAktProverkiPrigodnostiVipolnil
                                {
                                    Id = NextIDVipolnil,
                                    MainID = MainID,
                                    Должность = Dolchnost.Text,
                                    ФИО = FIO.Text,
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                db.ReactivAktProverkiPrigodnostiVipolnil.InsertOnSubmit(new ReactivAktProverkiPrigodnostiVipolnil
                                {
                                    Id = NextIDVipolnil + 1,
                                    MainID = HistoryMainID,
                                    Должность = Dolchnost.Text,
                                    ФИО = FIO.Text,
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDVipolnil,
                                });
                            }
                        }
                        else
                        {
                            if (Vipolnil != null)
                            {
                                Vipolnil.VisibleStatus = Vipolnil.Owner == null ? 300 : 301;
                                Vipolnil.UserName = UserSettings.User;
                                Vipolnil.ДатаСозданияЗаписи = DateTime.Now;
                                //db.ReactivAktProverkiPrigodnostiVipolnil.DeleteOnSubmit(Vipolnil);
                            }
                        }
                    }
                    else //Добавить новый акт
                    {
                        //Основные данные
                        var NextID = db.ReactivAktProverkiPrigodnosti != null && db.ReactivAktProverkiPrigodnosti.Count() > 0 ? db.ReactivAktProverkiPrigodnosti.Max(c => c.Id) + 1 : 1;
                        var NN = N.Text.Split('-');
                        DateTime? ДатаСрокаХранения;
                        if (DateOkonSrokHran.Text == "")
                            ДатаСрокаХранения = null;
                        else
                            ДатаСрокаХранения = Convert.ToDateTime(DateOkonSrokHran.Text).Date;
                        DateTime? DI = null;
                        if (DateIzgotovlenia.Checked)
                        {
                            DI = DateIzgotovlenia.Value;
                        }
                        db.ReactivAktProverkiPrigodnosti.InsertOnSubmit(new ReactivAktProverkiPrigodnosti
                        {
                            Id = NextID,
                            Дата = AktCreateDate.Value,
                            N = N.Text,
                            Наименование = ReactivName.Text,
                            Квалификация = Kvalif.Text,
                            ND = ND.Text,
                            НомерПартии = Partia.Text,
                            ДатаИзготовления = DI,
                            ДатаОкончанияСрокаХранения = ДатаСрокаХранения,
                            НДиМВИ = NDiMVI.Text,
                            Заключение = Zakluchenie.Text,
                            VisibleStatus = 0,
                            UserName = UserSettings.User,
                            ДатаСозданияЗаписи = DateTime.Now,
                            Owner = null,
                            ReactivID = ReactivID,
                        });
                        db.ReactivAktProverkiPrigodnosti.InsertOnSubmit(new ReactivAktProverkiPrigodnosti
                        {
                            Id = NextID + 1,
                            Дата = AktCreateDate.Value,
                            N = N.Text,
                            Наименование = ReactivName.Text,
                            Квалификация = Kvalif.Text,
                            ND = ND.Text,
                            НомерПартии = Partia.Text,
                            ДатаИзготовления = DI,
                            ДатаОкончанияСрокаХранения = ДатаСрокаХранения,
                            НДиМВИ = NDiMVI.Text,
                            Заключение = Zakluchenie.Text,
                            VisibleStatus = 100,
                            UserName = UserSettings.User,
                            ДатаСозданияЗаписи = DateTime.Now,
                            Owner = NextID,
                            ReactivID = ReactivID,
                        });
                        //Образец для контроля
                        var NextIDOther = db.ReactivAktProverkiPrigodnostiObrazech != null && db.ReactivAktProverkiPrigodnostiObrazech.Count() > 0 ? db.ReactivAktProverkiPrigodnostiObrazech.Max(c => c.Id) + 1 : 1;
                        if (ObrazechDlyaControla1.Text != "")
                        {
                            db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                            {
                                Id = NextIDOther,
                                MainID = NextID,
                                ОбразецДляКонтроля = ObrazechDlyaControla1.Text,
                                VisibleStatus = 0,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = null,
                            });
                            NextIDOther++;
                            db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                            {
                                Id = NextIDOther,
                                MainID = NextID + 1,
                                ОбразецДляКонтроля = ObrazechDlyaControla1.Text,
                                VisibleStatus = 100,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = NextIDOther - 1,
                            });
                            NextIDOther++;
                        }
                        if (ObrazechDlyaControla2.Text != "")
                        {
                            db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                            {
                                Id = NextIDOther,
                                MainID = NextID,
                                ОбразецДляКонтроля = ObrazechDlyaControla2.Text,
                                VisibleStatus = 0,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = null,
                            });
                            NextIDOther++;
                            db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                            {
                                Id = NextIDOther,
                                MainID = NextID + 1,
                                ОбразецДляКонтроля = ObrazechDlyaControla2.Text,
                                VisibleStatus = 100,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = NextIDOther - 1,
                            });
                            NextIDOther++;
                        }
                        if (ObrazechDlyaControla3.Text != "")
                        {
                            db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                            {
                                Id = NextIDOther,
                                MainID = NextID,
                                ОбразецДляКонтроля = ObrazechDlyaControla3.Text,
                                VisibleStatus = 0,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = null,
                            });
                            NextIDOther++;
                            db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                            {
                                Id = NextIDOther++,
                                MainID = NextID + 1,
                                ОбразецДляКонтроля = ObrazechDlyaControla3.Text,
                                VisibleStatus = 100,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = NextIDOther - 1,
                            });
                            NextIDOther++;
                        }
                        //Добавление табличной части
                        List<string> GridData = GetGridData();

                        var NextIDTable = db.ReactivAktProverkiPrigodnostiTable != null && db.ReactivAktProverkiPrigodnostiTable.Count() > 0 ? db.ReactivAktProverkiPrigodnostiTable.Max(c => c.Id) + 1 : 0;
                        if (GridData.Count > 0)
                        {
                            int n = 0;
                            while (GridData.Count > 7 * n + 1)
                            {
                                db.ReactivAktProverkiPrigodnostiTable.InsertOnSubmit(new ReactivAktProverkiPrigodnostiTable
                                {
                                    Id = NextIDTable,
                                    MainID = NextID,
                                    ДатаПроведенияИзмерения = GridData[7 * n],
                                    Исполнитель1 = GridData[7 * n + 1],
                                    Исполнитель2 = GridData[7 * n + 2],
                                    АттестованноеЗначение = GridData[7 * n + 3],
                                    РезультатИзмеренийМассовойКонцентрацииКомпонентаВИсходнойПробе = GridData[7 * n + 4],
                                    РезультатКонтрольнойПроцедуры = GridData[7 * n + 5],
                                    НормативКонтроля = GridData[7 * n + 6],
                                    VisibleStatus = 0,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = null,
                                });
                                NextIDTable++;
                                db.ReactivAktProverkiPrigodnostiTable.InsertOnSubmit(new ReactivAktProverkiPrigodnostiTable
                                {
                                    Id = NextIDTable,
                                    MainID = NextID + 1,
                                    ДатаПроведенияИзмерения = GridData[7 * n],
                                    Исполнитель1 = GridData[7 * n + 1],
                                    Исполнитель2 = GridData[7 * n + 2],
                                    АттестованноеЗначение = GridData[7 * n + 3],
                                    РезультатИзмеренийМассовойКонцентрацииКомпонентаВИсходнойПробе = GridData[7 * n + 4],
                                    РезультатКонтрольнойПроцедуры = GridData[7 * n + 5],
                                    НормативКонтроля = GridData[7 * n + 6],
                                    VisibleStatus = 100,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    Owner = NextIDTable - 1,
                                });
                                NextIDTable++;
                                n++;
                            }
                        }
                        //Добавление Vipolnil
                        var NextIDVipolnil = db.ReactivAktProverkiPrigodnostiVipolnil != null && db.ReactivAktProverkiPrigodnostiVipolnil.Count() > 0 ? db.ReactivAktProverkiPrigodnostiVipolnil.Max(c => c.Id) + 1 : 1;
                        if (Dolchnost.Text != "" || FIO.Text != "")
                        {
                            db.ReactivAktProverkiPrigodnostiVipolnil.InsertOnSubmit(new ReactivAktProverkiPrigodnostiVipolnil
                            {
                                Id = NextIDVipolnil,
                                MainID = NextID,
                                Должность = Dolchnost.Text,
                                ФИО = FIO.Text,
                                VisibleStatus = 0,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = null,
                            });
                            db.ReactivAktProverkiPrigodnostiVipolnil.InsertOnSubmit(new ReactivAktProverkiPrigodnostiVipolnil
                            {
                                Id = NextIDVipolnil + 1,
                                MainID = NextID + 1,
                                Должность = Dolchnost.Text,
                                ФИО = FIO.Text,
                                VisibleStatus = 100,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                Owner = NextIDVipolnil,
                            });
                        }
                        ID.Text = NextID.ToString();
                    }
                    var ReactivProdlen = db.Reactiv.Where(c => c.Id == ReactivID).FirstOrDefault();
                    if (ReactivProdlen != null)
                    {
                        var DO = Zakluchenie.Text.Split(' ');
                        var Next = false;
                        var SrokDO = "";
                        foreach (string c in DO)
                        {
                            if (Next)
                            {
                                SrokDO = c;
                                break;
                            }
                            if (c == "до") Next = true;
                        }
                        ReactivProdlen.ПродлениеСрока = "продлен до " + SrokDO + " акт №" + N.Text + " от " + Ot.Text;
                        try
                        {
                            ReactivProdlen.СрокГодностиDate = SrokDO.Length > 9 ? DateTime.Parse(SrokDO.Substring(0, 10)).Date : ReactivProdlen.СрокГодностиDate;
                        } catch (Exception ex) { LogErrors Log = new LogErrors(ex.ToString()); } 
                        ReactivProdlen.СрокГодности = ReactivProdlen.СрокГодностиDate.Value.ToShortDateString();
                    }
                    db.SubmitChanges();
                    db.Dispose();
                }
                if (ShowMessage) MessageBox.Show("Акт пригодности реактива сохранён!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Changed = false;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void HistoryReactivAktProverkiPrigodnostiTable(ReactivAktProverkiPrigodnostiTable stroka, int Kod, int ID, int MainID)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.ReactivAktProverkiPrigodnostiTable.InsertOnSubmit(new ReactivAktProverkiPrigodnostiTable
                    {
                        Id = ID,
                        MainID = MainID,
                        ДатаПроведенияИзмерения = stroka.ДатаПроведенияИзмерения,
                        Исполнитель1 = stroka.Исполнитель1,
                        Исполнитель2 = stroka.Исполнитель2,
                        АттестованноеЗначение = stroka.АттестованноеЗначение,
                        РезультатИзмеренийМассовойКонцентрацииКомпонентаВИсходнойПробе = stroka.РезультатИзмеренийМассовойКонцентрацииКомпонентаВИсходнойПробе,
                        РезультатКонтрольнойПроцедуры = stroka.РезультатКонтрольнойПроцедуры,
                        НормативКонтроля = stroka.НормативКонтроля,
                        UserName = UserSettings.User,
                        VisibleStatus = Kod,
                        ДатаСозданияЗаписи = DateTime.Now,
                        Owner = stroka.Id,
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void HistoryReactivAktProverkiPrigodnostiObrazech(ReactivAktProverkiPrigodnostiObrazech stroka, int Kod, int ID, int MainID)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.ReactivAktProverkiPrigodnostiObrazech.InsertOnSubmit(new ReactivAktProverkiPrigodnostiObrazech
                    {
                        Id = ID,
                        MainID = MainID,
                        ОбразецДляКонтроля = ObrazechDlyaControla1.Text,
                        UserName = UserSettings.User,
                        VisibleStatus = Kod,
                        ДатаСозданияЗаписи = DateTime.Now,
                        Owner = stroka.Id
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void HistoryReactivAktProverkiPrigodnosti(ReactivAktProverkiPrigodnosti stroka, int Kod, int ID)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    db.ReactivAktProverkiPrigodnosti.InsertOnSubmit(new ReactivAktProverkiPrigodnosti
                    {
                        Id = ID,
                        N = stroka.N,
                        Дата = stroka.Дата,
                        Наименование = stroka.Наименование,
                        Квалификация = stroka.Квалификация,
                        ND = stroka.ND,
                        ДатаИзготовления = stroka.ДатаИзготовления,
                        НомерПартии = stroka.НомерПартии,
                        ДатаОкончанияСрокаХранения = stroka.ДатаОкончанияСрокаХранения,
                        НДиМВИ = stroka.НДиМВИ,
                        Заключение = stroka.Заключение,
                        VisibleStatus = Kod,
                        ДатаСозданияЗаписи = DateTime.Now,
                        UserName = UserSettings.User,
                        Owner = stroka.Id,
                        ReactivID = stroka.ReactivID
                    });
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AktProverkaPrigodnostiGridView_Enter(object sender, EventArgs e)
        {
            Changed = true;
        }

        private void N_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    SaveButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void ReactivPrintAktvProverkiPrigodnostiForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
