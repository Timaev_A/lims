﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class ChangeDateGSOBookForm : DevExpress.XtraEditors.XtraForm
    {
        int StrokeID;
        bool CreateDate;
        bool CloseDate;
        bool KontrolStabilnosti = false;
        public ChangeDateGSOBookForm(MainForm MF, OperativniiKontrol Stroke, bool Create = true, bool Close = true)
        {
            InitializeComponent();
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Analiz = db.Analiz.Where(c => c.Id == Stroke.AnalizID).FirstOrDefault();
                    if (Analiz != null)
                        AnalizName.Text = Analiz.НазваниеМетодаАнализа;

                    CreateDate = Create;
                    CloseDate = Close;
                    StrokeID = Stroke.Id;
                    N.Text = Stroke.Book.Value.ToString();

                    DateCreate.Value = Stroke.CreateDate.HasValue ? Stroke.CreateDate.Value : DateTime.Now;
                    EndDate.Value = Stroke.EndDate.HasValue ? Stroke.EndDate.Value : DateTime.Now;
                    DateCreate.Enabled = Create;
                    if (!Close)
                    {
                        ClearDateButton.Enabled = false;
                        ChangeDateButton.Enabled = false;
                        EndDate.Enabled = false;
                    }
                    else
                    {
                        ClearDateButton.Checked = !Stroke.EndDate.HasValue;
                        ChangeDateButton.Checked = Stroke.EndDate.HasValue;
                        EndDate.Enabled = Stroke.EndDate.HasValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public ChangeDateGSOBookForm(MainForm MF, KontrolStabilnosti Stroke, bool Create = true, bool Close = true)
        {
            InitializeComponent();
            try
            {
                KontrolStabilnosti = true;
                using (DbDataContext db = new DbDataContext())
                {
                    var Analiz = db.Analiz.Where(c => c.Id == Stroke.AnalizID).FirstOrDefault();
                    if (Analiz != null)
                        AnalizName.Text = Analiz.НазваниеМетодаАнализа;
                    CreateDate = Create;
                    CloseDate = Close;
                    StrokeID = Stroke.Id;
                    N.Text = Stroke.Book.Value.ToString();

                    DateCreate.Value = Stroke.CreateDate.HasValue ? Stroke.CreateDate.Value : DateTime.Now;
                    EndDate.Value = Stroke.EndDate.HasValue ? Stroke.EndDate.Value : DateTime.Now;
                    DateCreate.Enabled = Create;
                    if (!Close)
                    {
                        ClearDateButton.Enabled = false;
                        ChangeDateButton.Enabled = false;
                        EndDate.Enabled = false;
                    }
                    else
                    {
                        ClearDateButton.Checked = !Stroke.EndDate.HasValue;
                        ChangeDateButton.Checked = Stroke.EndDate.HasValue;
                        EndDate.Enabled = Stroke.EndDate.HasValue;
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    if (!KontrolStabilnosti)
                    {
                        var Stroka = db.OperativniiKontrol.Where(c => c.Id == StrokeID).FirstOrDefault();
                        var NextID = db.OperativniiKontrol != null && db.OperativniiKontrol.Count() > 0 ? db.OperativniiKontrol.Max(c => c.Id) + 1 : 1;
                        if (CreateDate)
                        {
                            Stroka.CreateDate = DateCreate.Value;
                        }
                        if (CloseDate)
                        {
                            if (ClearDateButton.Checked)
                            {
                                Stroka.EndDate = null;
                                Stroka.VisibleStatus = 0;
                            }
                            else
                            {
                                Stroka.EndDate = EndDate.Value;
                                Stroka.VisibleStatus = 1;
                            }
                        }
                        Stroka.VisibleStatus = null;
                        Stroka.UserName = UserSettings.User;
                        Stroka.ДатаСозданияЗаписи = DateTime.Now;
                        //История
                        if (CreateDate && CloseDate)
                        {
                            HistoryChangeDate(db, Stroka, NextID, 200);
                        }
                        else if (!CreateDate && CloseDate && ChangeDateButton.Checked)
                        {
                            HistoryChangeDate(db, Stroka, NextID, 401);
                        }
                        else if (!CreateDate && CloseDate && ClearDateButton.Checked)
                        {
                            HistoryChangeDate(db, Stroka, NextID, 400);
                        }
                        else HistoryChangeDate(db, Stroka, NextID, 200);
                    }
                    else
                    {
                        var Stroka = db.KontrolStabilnosti.Where(c => c.Id == StrokeID).FirstOrDefault();
                        var NextID = db.KontrolStabilnosti != null && db.KontrolStabilnosti.Count() > 0 ? db.KontrolStabilnosti.Max(c => c.Id) + 1 : 1;
                        if (CreateDate)
                        {
                            Stroka.CreateDate = DateCreate.Value;
                        }
                        if (CloseDate)
                        {
                            if (ClearDateButton.Checked)
                            {
                                Stroka.EndDate = null;
                                Stroka.VisibleStatus = 0;
                            }
                            else
                            {
                                Stroka.EndDate = EndDate.Value;
                                Stroka.VisibleStatus = 1;
                            }
                        }
                        Stroka.VisibleStatus = null;
                        Stroka.UserName = UserSettings.User;
                        Stroka.ДатаСозданияЗаписи = DateTime.Now;
                        //История
                        if (CreateDate && CloseDate)
                        {
                            HistoryChangeDate(db, Stroka, NextID, 200);
                        }
                        else if (!CreateDate && CloseDate && ChangeDateButton.Checked)
                        {
                            HistoryChangeDate(db, Stroka, NextID, 401);
                        }
                        else if (!CreateDate && CloseDate && ClearDateButton.Checked)
                        {
                            HistoryChangeDate(db, Stroka, NextID, 400);
                        }
                        else HistoryChangeDate(db, Stroka, NextID, 200);
                    }
                    db.SubmitChanges();
                    db.Dispose();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static void HistoryChangeDate(DbDataContext db, OperativniiKontrol Stroka, int NextID, int Status)
        {
            db.OperativniiKontrol.InsertOnSubmit(new OperativniiKontrol
            {
                Id = NextID,
                AnalizID = Stroka.AnalizID,
                Book = Stroka.Book,
                CreateDate = Stroka.CreateDate,
                EndDate = Stroka.EndDate,
                Owner = Stroka.Id,
                UserName = UserSettings.User,
                VisibleStatus = 200,
                ДатаСозданияЗаписи = DateTime.Now,
                Колво = Stroka.Колво,
            });
        }

        private static void HistoryChangeDate(DbDataContext db, KontrolStabilnosti Stroka, int NextID, int Status)
        {
            db.KontrolStabilnosti.InsertOnSubmit(new KontrolStabilnosti
            {
                Id = NextID,
                AnalizID = Stroka.AnalizID,
                Book = Stroka.Book,
                CreateDate = Stroka.CreateDate,
                EndDate = Stroka.EndDate,
                Owner = Stroka.Id,
                UserName = UserSettings.User,
                VisibleStatus = 200,
                ДатаСозданияЗаписи = DateTime.Now,
                Колво = Stroka.Колво,
                AttestovannoeZnachenie = Stroka.AttestovannoeZnachenie,
            });
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ClearDateButton_CheckedChanged(object sender, EventArgs e)
        {
            EndDate.Enabled = !ClearDateButton.Checked;
        }

        private void DateCreate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void ChangeDateGSOBookForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
