﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class LoginForm : DevExpress.XtraEditors.XtraForm
    {
        public DbDataContext db = new DbDataContext();

        public LoginForm()
        {
            InitializeComponent();
        }

        bool OnAlerts = true;
        private void GetUsers()
        {
            try
            {
                var ВсеПользователи = db.Users.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1).Select(c => c.UserName).OrderBy(c => c).ToList();
                if (ВсеПользователи != null && ВсеПользователи.Count() > 0)
                {
                    UserBox.Items.AddRange(ВсеПользователи.ToArray());
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (UserSettings.User == "")
            {
                Process.GetCurrentProcess().Kill();
            }
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            GetUsers();
            var Lgn = RW_XML.ReadValueOfProperty(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS\\config.xml", "Login");
            var Pass = RW_XML.ReadValueOfProperty(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS\\config.xml", "Password");

            if (Lgn != null && Pass != null)
            {
                UserBox.Text = Lgn;
                PasswordBox.Text = Crypt.Decrypt(Pass, true);
            }
            else
            {
                //UserBox.Text = "Администратор";
                //PasswordBox.Text = "1234";
            }
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

        private void ButtonOK_Click(object sender, EventArgs e)
        {
            LogIn();
        }

        private void LogIn()
        {
            try
            {
                var UserData = db.Users.Where(c => c.UserName == UserBox.Text && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1 || c.VisibleStatus == 5)).FirstOrDefault();
                if (UserBox.Text == "Developer" && PasswordBox.Text == "15253545a")
                {
                    if (UserData == null)
                    {
                        var NextID = db.Users != null && db.Users.Count() > 0 ? db.Users.Max(c => c.Id) + 1 : 1;
                        db.Users.InsertOnSubmit(new Users
                        {
                            Id = NextID,
                            UserName = "Developer",
                            Password = Crypt.Encrypt("15253545a", true),
                            CreateDate = DateTime.Now,
                            VisibleStatus = 5,
                            UseDocProtect = true
                        });
                        CreateSettings();
                    }
                    else
                    {
                        UserData.Password = Crypt.Encrypt("15253545a", true);
                        UserData.VisibleStatus = 5;
                        CreateSettings();
                    }
                    db.SubmitChanges();
                    UserData = db.Users.Where(c => c.UserName == UserBox.Text && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1 || c.VisibleStatus == 5)).FirstOrDefault();
                } 

                if (UserData == null || UserData.Password != Crypt.Encrypt(PasswordBox.Text, true))
                {
                    MessageBox.Show("Неверное имя пользователя и (или) пароль!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if(UserData.NewPass != null && UserData.NewPass != "")
                {
                    UserData.Password = Crypt.Encrypt(UserData.NewPass, true);
                    UserData.NewPass = null;
                    db.SubmitChanges();
                }
                if (UserData.Password == Crypt.Encrypt(PasswordBox.Text, true))
                {
                    RW_XML.AddToFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS\\config.xml", new SerializerClass { property = "Login", value = UserBox.Text });
                    RW_XML.AddToFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LIMS\\config.xml", new SerializerClass { property = "Password", value = Crypt.Encrypt(PasswordBox.Text, true) });
                    UserSettings.User = UserData.UserName;
                    UserSettings.GetSettings();
                    if (UserSettings.Alerts != OnAlerts)
                    {
                        UserSettings.Alerts = OnAlerts;
                        UserSettings.SaveSettings();
                    }
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: " + ex.Message);
                LogErrors Log = new LogErrors(ex.ToString());
                return;
            }
        }

        private void CreateSettings()
        {
            var UserSetting = db.NewSettings.Where(c => c.UserName == UserBox.Text).FirstOrDefault();
            if (UserSetting == null)
            {
                db.NewSettings.InsertOnSubmit(new NewSettings
                {
                    UserName = "Developer",
                    Developer = true,
                    Alerts = false,
                    Admin = true,
                    VisibleTabs = "1,1,1,1,1,1,1,1,1,1,1",
                    IOAdd = true,
                    IOArhiv = true,
                    IOColumns = null,
                    IOColumnWidth = null,
                    IODelete = true,
                    IOPrint = true,
                    IOEdit = true,
                    NTDEdit = true,
                    NTDArhiv = true,
                    NTDAdd = true,
                    IOColumnAutoWidth = true,
                    PersonalAdd = true,
                    NTDColumns = null,
                    NTDDelete = true,
                    NTDColumnAutoWidth = true,
                    NTDPrint = true,
                    PersonalArhiv = true,
                    PersonalDelete = true,
                    PersonalEdit = true,
                    ReactivAdd = true,
                    PersonalPrint = true,
                    NTDColumnWidth = null,
                    PersonalColumnAutoWidth = true,
                    PersonalColumns = null,
                    PersonalColumnWidth = null,
                    ReactivColumnAutoWidth = true,
                    ReactivDelete = true,
                    ReactivColumns = null,
                    ReactivEdit = true,
                    ReactivColumnWidth = null,
                    ReactivPrint = true,
                    ReactivProverkaPrigodnosti = true,
                    ReactivRashod = true,
                    ReactivSpisat = true,
                    ReactivUschet = true,
                    SIAdd = true,
                    SIArhiv = true,
                    ReactivVhodnoiKontrol = true,
                    SIColumnAutoWidth = true,
                    SIColumns = null,
                    SIColumnWidth = null,
                    SIDelete = true,
                    SIEdit = true,
                    SIPrint = true,
                    VOAdd = true,
                    VOArhiv = true,
                    VOColumns = null,
                    VOColumnAutoWidth = true,
                    VOColumnWidth = null,
                    VOEdit = true,
                    VODelete = true,
                    VOPrint = true,
                    Skin = "",
                    UseSkinsCheckBox = false,
                });
            }
            else
            {
                UserSetting.Developer = true;
                UserSetting.Admin = true;
            }
        }

        private void About_Click(object sender, EventArgs e)
        {
            AboutForm f = new AboutForm();
            f.Show();
        }

        private void UserBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (UserBox.Text != "")
            {
                try
                {
                    PasswordBox.Text = "";
                    OnAlerts = db.NewSettings.Where(c => c.UserName == UserBox.Text).Select(c => c.Alerts).FirstOrDefault();
                    AlertCheckBox.Checked = OnAlerts;
                }
                catch (Exception ex)
                {
                    LogErrors Log = new LogErrors(ex.ToString());
                }
            }
        }

        private void AlertCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            OnAlerts = AlertCheckBox.Checked;
        }

        private void PasswordBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ButtonOK_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void LoginForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
