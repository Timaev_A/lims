﻿namespace LIMS
{
    partial class CheckVOColumnForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckVOColumnForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.CheckEditPravoSobstvennosti = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditPrimechanie = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditInventarniiN = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditGogVvodaVEkspluatachiu = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditN = new DevExpress.XtraEditors.CheckEdit();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.CancelButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEditSleduechegoTO = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDatePoslednegoTO = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateSledProverki = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateProverkiTehnichSostoiania = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditMestoUstanovkiHranenia = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditNaznachenie = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditGodVipuska = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditIzgotovitel = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditZavN = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditTip = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditName = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPravoSobstvennosti.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrimechanie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditInventarniiN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGogVvodaVEkspluatachiu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSleduechegoTO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDatePoslednegoTO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateSledProverki.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateProverkiTehnichSostoiania.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditMestoUstanovkiHranenia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNaznachenie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGodVipuska.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditIzgotovitel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditZavN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditTip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.CheckEditPravoSobstvennosti);
            this.panelControl1.Controls.Add(this.CheckEditPrimechanie);
            this.panelControl1.Controls.Add(this.CheckEditInventarniiN);
            this.panelControl1.Controls.Add(this.CheckEditGogVvodaVEkspluatachiu);
            this.panelControl1.Controls.Add(this.CheckEditN);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelButton1);
            this.panelControl1.Controls.Add(this.CheckEditSleduechegoTO);
            this.panelControl1.Controls.Add(this.CheckEditDatePoslednegoTO);
            this.panelControl1.Controls.Add(this.CheckEditDateSledProverki);
            this.panelControl1.Controls.Add(this.CheckEditDateProverkiTehnichSostoiania);
            this.panelControl1.Controls.Add(this.CheckEditMestoUstanovkiHranenia);
            this.panelControl1.Controls.Add(this.CheckEditNaznachenie);
            this.panelControl1.Controls.Add(this.CheckEditGodVipuska);
            this.panelControl1.Controls.Add(this.CheckEditIzgotovitel);
            this.panelControl1.Controls.Add(this.CheckEditZavN);
            this.panelControl1.Controls.Add(this.CheckEditTip);
            this.panelControl1.Controls.Add(this.CheckEditName);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(409, 246);
            this.panelControl1.TabIndex = 0;
            // 
            // CheckEditPravoSobstvennosti
            // 
            this.CheckEditPravoSobstvennosti.EditValue = true;
            this.CheckEditPravoSobstvennosti.Location = new System.Drawing.Point(177, 37);
            this.CheckEditPravoSobstvennosti.Name = "CheckEditPravoSobstvennosti";
            this.CheckEditPravoSobstvennosti.Properties.Caption = "Право собственнности";
            this.CheckEditPravoSobstvennosti.Size = new System.Drawing.Size(205, 19);
            this.CheckEditPravoSobstvennosti.TabIndex = 18;
            // 
            // CheckEditPrimechanie
            // 
            this.CheckEditPrimechanie.EditValue = true;
            this.CheckEditPrimechanie.Location = new System.Drawing.Point(177, 187);
            this.CheckEditPrimechanie.Name = "CheckEditPrimechanie";
            this.CheckEditPrimechanie.Properties.Caption = "Примечание";
            this.CheckEditPrimechanie.Size = new System.Drawing.Size(220, 19);
            this.CheckEditPrimechanie.TabIndex = 15;
            this.CheckEditPrimechanie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditInventarniiN
            // 
            this.CheckEditInventarniiN.EditValue = true;
            this.CheckEditInventarniiN.Location = new System.Drawing.Point(12, 187);
            this.CheckEditInventarniiN.Name = "CheckEditInventarniiN";
            this.CheckEditInventarniiN.Properties.Caption = "Инвентарный номер";
            this.CheckEditInventarniiN.Size = new System.Drawing.Size(127, 19);
            this.CheckEditInventarniiN.TabIndex = 8;
            this.CheckEditInventarniiN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditGogVvodaVEkspluatachiu
            // 
            this.CheckEditGogVvodaVEkspluatachiu.EditValue = true;
            this.CheckEditGogVvodaVEkspluatachiu.Location = new System.Drawing.Point(12, 162);
            this.CheckEditGogVvodaVEkspluatachiu.Name = "CheckEditGogVvodaVEkspluatachiu";
            this.CheckEditGogVvodaVEkspluatachiu.Properties.Caption = "Год ввода в эксплуатацию";
            this.CheckEditGogVvodaVEkspluatachiu.Size = new System.Drawing.Size(159, 19);
            this.CheckEditGogVvodaVEkspluatachiu.TabIndex = 7;
            this.CheckEditGogVvodaVEkspluatachiu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditN
            // 
            this.CheckEditN.EditValue = true;
            this.CheckEditN.Location = new System.Drawing.Point(12, 12);
            this.CheckEditN.Name = "CheckEditN";
            this.CheckEditN.Properties.Caption = "№ п/п";
            this.CheckEditN.Size = new System.Drawing.Size(112, 19);
            this.CheckEditN.TabIndex = 1;
            this.CheckEditN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(117, 212);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 16;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton1
            // 
            this.CancelButton1.Location = new System.Drawing.Point(198, 212);
            this.CancelButton1.Name = "CancelButton1";
            this.CancelButton1.Size = new System.Drawing.Size(75, 23);
            this.CancelButton1.TabIndex = 17;
            this.CancelButton1.Text = "Отмена";
            this.CancelButton1.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CheckEditSleduechegoTO
            // 
            this.CheckEditSleduechegoTO.EditValue = true;
            this.CheckEditSleduechegoTO.Location = new System.Drawing.Point(177, 162);
            this.CheckEditSleduechegoTO.Name = "CheckEditSleduechegoTO";
            this.CheckEditSleduechegoTO.Properties.Caption = "Дата следующего ТО";
            this.CheckEditSleduechegoTO.Size = new System.Drawing.Size(220, 19);
            this.CheckEditSleduechegoTO.TabIndex = 14;
            this.CheckEditSleduechegoTO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDatePoslednegoTO
            // 
            this.CheckEditDatePoslednegoTO.EditValue = true;
            this.CheckEditDatePoslednegoTO.Location = new System.Drawing.Point(177, 137);
            this.CheckEditDatePoslednegoTO.Name = "CheckEditDatePoslednegoTO";
            this.CheckEditDatePoslednegoTO.Properties.Caption = "Дата последнего ТО";
            this.CheckEditDatePoslednegoTO.Size = new System.Drawing.Size(220, 19);
            this.CheckEditDatePoslednegoTO.TabIndex = 13;
            this.CheckEditDatePoslednegoTO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDateSledProverki
            // 
            this.CheckEditDateSledProverki.EditValue = true;
            this.CheckEditDateSledProverki.Location = new System.Drawing.Point(177, 112);
            this.CheckEditDateSledProverki.Name = "CheckEditDateSledProverki";
            this.CheckEditDateSledProverki.Properties.Caption = "Дата следующей проверки";
            this.CheckEditDateSledProverki.Size = new System.Drawing.Size(220, 19);
            this.CheckEditDateSledProverki.TabIndex = 12;
            this.CheckEditDateSledProverki.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDateProverkiTehnichSostoiania
            // 
            this.CheckEditDateProverkiTehnichSostoiania.EditValue = true;
            this.CheckEditDateProverkiTehnichSostoiania.Location = new System.Drawing.Point(177, 87);
            this.CheckEditDateProverkiTehnichSostoiania.Name = "CheckEditDateProverkiTehnichSostoiania";
            this.CheckEditDateProverkiTehnichSostoiania.Properties.Caption = "Дата проверки технического состояния";
            this.CheckEditDateProverkiTehnichSostoiania.Size = new System.Drawing.Size(229, 19);
            this.CheckEditDateProverkiTehnichSostoiania.TabIndex = 11;
            this.CheckEditDateProverkiTehnichSostoiania.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditMestoUstanovkiHranenia
            // 
            this.CheckEditMestoUstanovkiHranenia.EditValue = true;
            this.CheckEditMestoUstanovkiHranenia.Location = new System.Drawing.Point(177, 62);
            this.CheckEditMestoUstanovkiHranenia.Name = "CheckEditMestoUstanovkiHranenia";
            this.CheckEditMestoUstanovkiHranenia.Properties.Caption = "Место установки или хранения";
            this.CheckEditMestoUstanovkiHranenia.Size = new System.Drawing.Size(205, 19);
            this.CheckEditMestoUstanovkiHranenia.TabIndex = 10;
            this.CheckEditMestoUstanovkiHranenia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditNaznachenie
            // 
            this.CheckEditNaznachenie.EditValue = true;
            this.CheckEditNaznachenie.Location = new System.Drawing.Point(177, 12);
            this.CheckEditNaznachenie.Name = "CheckEditNaznachenie";
            this.CheckEditNaznachenie.Properties.Caption = "Назначение";
            this.CheckEditNaznachenie.Size = new System.Drawing.Size(205, 19);
            this.CheckEditNaznachenie.TabIndex = 9;
            this.CheckEditNaznachenie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditGodVipuska
            // 
            this.CheckEditGodVipuska.EditValue = true;
            this.CheckEditGodVipuska.Location = new System.Drawing.Point(12, 137);
            this.CheckEditGodVipuska.Name = "CheckEditGodVipuska";
            this.CheckEditGodVipuska.Properties.Caption = "Год выпуска";
            this.CheckEditGodVipuska.Size = new System.Drawing.Size(127, 19);
            this.CheckEditGodVipuska.TabIndex = 6;
            this.CheckEditGodVipuska.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditIzgotovitel
            // 
            this.CheckEditIzgotovitel.EditValue = true;
            this.CheckEditIzgotovitel.Location = new System.Drawing.Point(12, 112);
            this.CheckEditIzgotovitel.Name = "CheckEditIzgotovitel";
            this.CheckEditIzgotovitel.Properties.Caption = "Изготовитель";
            this.CheckEditIzgotovitel.Size = new System.Drawing.Size(145, 19);
            this.CheckEditIzgotovitel.TabIndex = 5;
            this.CheckEditIzgotovitel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditZavN
            // 
            this.CheckEditZavN.EditValue = true;
            this.CheckEditZavN.Location = new System.Drawing.Point(12, 87);
            this.CheckEditZavN.Name = "CheckEditZavN";
            this.CheckEditZavN.Properties.Caption = "Заводской номер";
            this.CheckEditZavN.Size = new System.Drawing.Size(112, 19);
            this.CheckEditZavN.TabIndex = 4;
            this.CheckEditZavN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditTip
            // 
            this.CheckEditTip.EditValue = true;
            this.CheckEditTip.Location = new System.Drawing.Point(12, 62);
            this.CheckEditTip.Name = "CheckEditTip";
            this.CheckEditTip.Properties.Caption = "Тип, марка";
            this.CheckEditTip.Size = new System.Drawing.Size(112, 19);
            this.CheckEditTip.TabIndex = 3;
            this.CheckEditTip.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditName
            // 
            this.CheckEditName.EditValue = true;
            this.CheckEditName.Location = new System.Drawing.Point(12, 37);
            this.CheckEditName.Name = "CheckEditName";
            this.CheckEditName.Properties.Caption = "Наименование";
            this.CheckEditName.Size = new System.Drawing.Size(112, 19);
            this.CheckEditName.TabIndex = 2;
            this.CheckEditName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckVOColumnForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 246);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "CheckVOColumnForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ ВО";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckVOColumnForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPravoSobstvennosti.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrimechanie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditInventarniiN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGogVvodaVEkspluatachiu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSleduechegoTO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDatePoslednegoTO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateSledProverki.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateProverkiTehnichSostoiania.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditMestoUstanovkiHranenia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNaznachenie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGodVipuska.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditIzgotovitel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditZavN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditTip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit CheckEditName;
        private DevExpress.XtraEditors.CheckEdit CheckEditDatePoslednegoTO;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateSledProverki;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateProverkiTehnichSostoiania;
        private DevExpress.XtraEditors.CheckEdit CheckEditMestoUstanovkiHranenia;
        private DevExpress.XtraEditors.CheckEdit CheckEditNaznachenie;
        private DevExpress.XtraEditors.CheckEdit CheckEditGodVipuska;
        private DevExpress.XtraEditors.CheckEdit CheckEditIzgotovitel;
        private DevExpress.XtraEditors.CheckEdit CheckEditZavN;
        private DevExpress.XtraEditors.CheckEdit CheckEditTip;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.SimpleButton CancelButton1;
        private DevExpress.XtraEditors.CheckEdit CheckEditSleduechegoTO;
        private DevExpress.XtraEditors.CheckEdit CheckEditN;
        private DevExpress.XtraEditors.CheckEdit CheckEditGogVvodaVEkspluatachiu;
        private DevExpress.XtraEditors.CheckEdit CheckEditInventarniiN;
        private DevExpress.XtraEditors.CheckEdit CheckEditPrimechanie;
        private DevExpress.XtraEditors.CheckEdit CheckEditPravoSobstvennosti;
    }
}