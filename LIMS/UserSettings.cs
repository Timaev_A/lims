﻿using System;
using System.Data.Linq;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace LIMS
{
    public static class UserSettings
    {
        public static bool debug = false;
        public static bool Protect = true;

        public static string User = "";
        public static bool Admin = false;
        public static bool Developer = false;
        public static string Skin = "";
        public static bool UseSkinsCheckBox = true;
        public static string VisibleTabs = debug ? "1,1,1,1,1,1,1,1" : "0,0,0,0,0,0,0,0"; //"1,1,1,1,1,1,1,1,0,0,0";/*"1,1,1,1,1,1,1,1,1,1,1";*///
        public static string ReactivColumns = "1,1,1,1,1,1,1,1,1,1,1,1";
        public static string PersonalColumns = "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1";
        public static string NTDColumns = "1,1,1,1,1,1,1,1,1,1,1,1,1,1";
        public static string SIColumns = "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1";
        public static string IOColumns = "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1";
        public static string VOColumns = "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1";
        public static bool ReactivColumnAutoWidth = true;
        public static bool PersonalColumnAutoWidth = true;
        public static bool NTDColumnAutoWidth = true;
        public static bool SIColumnAutoWidth = true;
        public static bool IOColumnAutoWidth = true;
        public static bool VOColumnAutoWidth = true;
        public static string ReactivColumnWidth = "";
        public static string PersonalColumnWidth = "";
        public static string NTDColumnWidth = "";
        public static string SIColumnWidth = "";
        public static string IOColumnWidth = "";
        public static string VOColumnWidth = "";
        public static bool ReactivAdd = false;
        public static bool ReactivEdit = false;
        public static bool ReactivPrint = false;
        public static bool ReactivRashod = false;
        public static bool ReactivSpisat = false;
        public static bool ReactivUschet = false;
        public static bool ReactivVhodnoiKontrol = false;
        public static bool ReactivProverkaPrigodnosti = false;
        public static bool ReactivDelete = false;
        public static bool PersonalAdd = false;
        public static bool PersonalEdit = false;
        public static bool PersonalArhiv = false;
        public static bool PersonalPrint = false;
        public static bool PersonalDelete = false;
        public static bool NTDAdd = false;
        public static bool NTDEdit = false;
        public static bool NTDPrint = false;
        public static bool NTDArhiv = false;
        public static bool NTDDelete = false;
        public static bool SIAdd = false;
        public static bool SIEdit = false;
        public static bool SIPrint = false;
        public static bool SIArhiv = false;
        public static bool SIDelete = false;
        public static bool IOAdd = false;
        public static bool IOEdit = false;
        public static bool IOPrint = false;
        public static bool IOArhiv = false;
        public static bool IODelete = false;
        public static bool VOAdd = false;
        public static bool VOEdit = false;
        public static bool VOPrint = false;
        public static bool VOArhiv = false;
        public static bool VODelete = false;
        public static bool Alerts = false;
        public static bool ReactivCellMerge = false;
        public static bool PersonalCellMerge = false;
        public static bool NTDCellMerge = false;
        public static bool SICellMerge = false;
        public static bool IOCellMerge = false;
        public static bool VOCellMerge = false;
        public static bool VLKStabilnostCellMerge = false;
        public static bool VLKStabilnostProbiCellMerge = false;
        public static bool VLKProbiCellMerge = false;
        public static bool VLKOperativniiCellMerge = false;
        public static bool VLKOperativniiProbiCellMerge = false;
        public static bool VLKAnalizAdd = false;
        public static bool VLKAnalizEdit = false;
        public static bool VLKAnaliz = false;
        public static bool VLKAnalizDelete = false;
        public static bool VLKProbi = false;
        public static bool VLKProbiAdd = false;
        public static bool VLKProbiEdit = false;
        public static bool VLKProbiChangeData = false;
        public static bool VLKProbiPrint = false;
        public static bool VLKProbiDelete = false;
        public static bool VLKOperaivnii = false;
        public static bool VLKOperaivniiEdit = false;
        public static bool VLKOperaivniiPrint = false;
        public static bool VLKOperaivniiClose = false;
        public static bool VLKOperaivniiDelete = false;
        public static bool VLKStabilnosti = false;
        public static bool VLKStabilnostiEdit = false;
        public static bool VLKStabilnostiClose = false;
        public static bool VLKStabilnostiPrint = false;
        public static bool VLKStabilnostiProtokolOchenka = false;
        public static bool VLKStabilnostiDiagrams = false;
        public static bool VLKStabilnostiDelete = false;
        public static int? ReactivSplitWidth = null;
        public static int? PersonalSplitWidth = null;
        public static int? IOSplitWidth = null;
        public static int? NTDSplitWidth = null;
        public static int? SISplitWidth = null;
        public static int? VOSplitWidth = null;

        public static void SaveSettings()
        {
            new Thread(Save).Start();
        }

        private static void Save()
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    try
                    {
                        SaveData(db);
                        db.SubmitChanges();
                    }
                    catch(Exception ex)
                    {
                        db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                        {
                            try
                            {
                                SaveData(db);
                                db.SubmitChanges();
                            }
                            catch (ChangeConflictException)
                            {
                                LogErrors Log2 = new LogErrors(ex.ToString());
                                //MessageBox.Show("Конфликт при сохранении!\nПопробуйте повторить операцию.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);          
                                return;                      
                            }
                        }
                        LogErrors Log = new LogErrors(ex.ToString());
                    }                    
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private static void SaveData(DbDataContext db)
        {
            var Set = db.NewSettings.Where(c => c.UserName == User).FirstOrDefault();
            if (Set == null)
            {
                var VisibleTabsD = debug ? "1,1,1,1,1,1,1,1" : VisibleTabs;
                var SkinD = debug ? "" : Skin;
                var UseSkinsCheckBoxD = debug ? false : UseSkinsCheckBox;
                db.NewSettings.InsertOnSubmit(new NewSettings
                {
                    UserName = User,
                    Skin = SkinD,
                    Admin = Admin,
                    UseSkinsCheckBox = UseSkinsCheckBoxD,
                    VisibleTabs = VisibleTabsD,
                    ReactivColumns = ReactivColumns,
                    PersonalColumns = PersonalColumns,
                    NTDColumns = NTDColumns,
                    SIColumns = SIColumns,
                    IOColumns = IOColumns,
                    VOColumns = VOColumns,
                    ReactivColumnAutoWidth = ReactivColumnAutoWidth,
                    PersonalColumnAutoWidth = PersonalColumnAutoWidth,
                    NTDColumnAutoWidth = NTDColumnAutoWidth,
                    SIColumnAutoWidth = SIColumnAutoWidth,
                    IOColumnAutoWidth = IOColumnAutoWidth,
                    VOColumnAutoWidth = VOColumnAutoWidth,
                    ReactivColumnWidth = ReactivColumnWidth,
                    PersonalColumnWidth = PersonalColumnWidth,
                    NTDColumnWidth = NTDColumnWidth,
                    SIColumnWidth = SIColumnWidth,
                    IOColumnWidth = IOColumnWidth,
                    VOColumnWidth = VOColumnWidth,
                    Alerts = Alerts,
                    ReactivCellMerge = ReactivCellMerge,
                    PersonalCellMerge = PersonalCellMerge,
                    NTDCellMerge = NTDCellMerge,
                    SICellMerge = SICellMerge,
                    IOCellMerge = IOCellMerge,
                    VOCellMerge = VOCellMerge,
                    VLKStabilnostCellMerge = VLKStabilnostCellMerge,
                    VLKStabilnostProbiCellMerge = VLKStabilnostProbiCellMerge,
                    VLKProbiCellMerge = VLKProbiCellMerge,
                    VLKOperativniiCellMerge = VLKOperativniiCellMerge,
                    VLKOperativniiProbiCellMerge = VLKOperativniiProbiCellMerge,
                    VLKOperaivniiClose = VLKOperaivniiClose,
                    VLKProbiChangeData = VLKProbiChangeData,
                    VLKStabilnostiProtokolOchenka = VLKStabilnostiProtokolOchenka,
                    VLKStabilnostiDiagrams = VLKStabilnostiDiagrams,
                    VLKStabilnostiClose = VLKStabilnostiClose,
                    VLKProbi = VLKProbi,
                    ReactivSplitWidth = ReactivSplitWidth,
                    PersonalSplitWidth = PersonalSplitWidth,
                    IOSplitWidth = IOSplitWidth,
                    NTDSplitWidth = NTDSplitWidth,
                    SISplitWidth = SISplitWidth,
                    VOSplitWidth = VOSplitWidth,
                });
            }
            else
            {
                if (!debug) Set.Skin = Skin;
                Set.Admin = Admin;
                if (!debug) Set.UseSkinsCheckBox = UseSkinsCheckBox;
                if (!debug) Set.VisibleTabs = VisibleTabs;
                Set.ReactivColumns = ReactivColumns;
                Set.PersonalColumns = PersonalColumns;
                Set.NTDColumns = NTDColumns;
                Set.SIColumns = SIColumns;
                Set.IOColumns = IOColumns;
                Set.VOColumns = VOColumns;
                Set.ReactivColumnAutoWidth = ReactivColumnAutoWidth;
                Set.PersonalColumnAutoWidth = PersonalColumnAutoWidth;
                Set.NTDColumnAutoWidth = NTDColumnAutoWidth;
                Set.SIColumnAutoWidth = SIColumnAutoWidth;
                Set.IOColumnAutoWidth = IOColumnAutoWidth;
                Set.VOColumnAutoWidth = VOColumnAutoWidth;
                Set.ReactivColumnWidth = ReactivColumnWidth;
                Set.PersonalColumnWidth = PersonalColumnWidth;
                Set.NTDColumnWidth = NTDColumnWidth;
                Set.SIColumnWidth = SIColumnWidth;
                Set.IOColumnWidth = IOColumnWidth;
                Set.VOColumnWidth = VOColumnWidth;
                Set.Alerts = Alerts;
                Set.ReactivCellMerge = ReactivCellMerge;
                Set.PersonalCellMerge = PersonalCellMerge;
                Set.NTDCellMerge = NTDCellMerge;
                Set.SICellMerge = SICellMerge;
                Set.IOCellMerge = IOCellMerge;
                Set.VOCellMerge = VOCellMerge;
                Set.VLKStabilnostCellMerge = VLKStabilnostCellMerge;
                Set.VLKStabilnostProbiCellMerge = VLKStabilnostProbiCellMerge;
                Set.VLKProbiCellMerge = VLKProbiCellMerge;
                Set.VLKOperativniiCellMerge = VLKOperativniiCellMerge;
                Set.VLKOperativniiProbiCellMerge = VLKOperativniiProbiCellMerge;
                Set.VLKOperaivniiClose = VLKOperaivniiClose;
                Set.VLKProbiChangeData = VLKProbiChangeData;
                Set.VLKStabilnostiProtokolOchenka = VLKStabilnostiProtokolOchenka;
                Set.VLKStabilnostiDiagrams = VLKStabilnostiDiagrams;
                Set.VLKStabilnostiClose = VLKStabilnostiClose;
                Set.VLKProbi = VLKProbi;
                Set.ReactivSplitWidth = ReactivSplitWidth;
                Set.PersonalSplitWidth = PersonalSplitWidth;
                Set.IOSplitWidth = IOSplitWidth;
                Set.NTDSplitWidth = NTDSplitWidth;
                Set.SISplitWidth = SISplitWidth;
                Set.VOSplitWidth = VOSplitWidth;
            }
        }

        public static void GetSettings()
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Set = db.NewSettings.Where(c => c.UserName == User).FirstOrDefault();
                    if (Set != null)
                    {                        
                        Admin = Set.Admin;
                        Developer = Set.Developer;
                        if (!debug) Skin = Set.Skin;
                        if (!debug) UseSkinsCheckBox = Set.UseSkinsCheckBox;
                        if (!debug) VisibleTabs = Set.VisibleTabs;
                        ReactivColumns = Set.ReactivColumns != null && Set.ReactivColumns != "" ? Set.ReactivColumns : ReactivColumns;
                        PersonalColumns = Set.PersonalColumns != null && Set.PersonalColumns != "" ? Set.PersonalColumns : PersonalColumns;
                        NTDColumns = Set.NTDColumns != null && Set.NTDColumns != "" ? Set.NTDColumns : NTDColumns;
                        SIColumns = Set.SIColumns != null && Set.SIColumns != "" ? Set.SIColumns : SIColumns;
                        IOColumns = Set.IOColumns != null && Set.IOColumns != "" ? Set.IOColumns : IOColumns;
                        VOColumns = Set.VOColumns != null && Set.VOColumns != "" ? Set.VOColumns : VOColumns;
                        ReactivColumnAutoWidth = Set.ReactivColumnAutoWidth;
                        PersonalColumnAutoWidth = Set.PersonalColumnAutoWidth;
                        NTDColumnAutoWidth = Set.NTDColumnAutoWidth;
                        SIColumnAutoWidth = Set.SIColumnAutoWidth;
                        IOColumnAutoWidth = Set.IOColumnAutoWidth;
                        VOColumnAutoWidth = Set.VOColumnAutoWidth;
                        ReactivColumnWidth = Set.ReactivColumnWidth;
                        PersonalColumnWidth = Set.PersonalColumnWidth;
                        NTDColumnWidth = Set.NTDColumnWidth;
                        SIColumnWidth = Set.SIColumnWidth;
                        IOColumnWidth = Set.IOColumnWidth;
                        VOColumnWidth = Set.VOColumnWidth;
                        ReactivAdd = Set.ReactivAdd;
                        ReactivEdit = Set.ReactivEdit;
                        ReactivPrint = Set.ReactivPrint;
                        ReactivRashod = Set.ReactivRashod;
                        ReactivSpisat = Set.ReactivSpisat;
                        ReactivUschet = Set.ReactivUschet;
                        ReactivVhodnoiKontrol = Set.ReactivVhodnoiKontrol;
                        ReactivProverkaPrigodnosti = Set.ReactivProverkaPrigodnosti;
                        ReactivDelete = Set.ReactivDelete;
                        PersonalAdd = Set.PersonalAdd;
                        PersonalEdit = Set.PersonalEdit;
                        PersonalArhiv = Set.PersonalArhiv;
                        PersonalPrint = Set.PersonalPrint;
                        PersonalDelete = Set.PersonalDelete;
                        NTDAdd = Set.NTDAdd;
                        NTDEdit = Set.NTDEdit;
                        NTDPrint = Set.NTDPrint;
                        NTDArhiv = Set.NTDArhiv;
                        NTDDelete = Set.NTDDelete;
                        SIAdd = Set.SIAdd;
                        SIEdit = Set.SIEdit;
                        SIPrint = Set.SIPrint;
                        SIArhiv = Set.SIArhiv;
                        SIDelete = Set.SIDelete;
                        IOAdd = Set.IOAdd;
                        IOEdit = Set.IOEdit;
                        IOPrint = Set.IOPrint;
                        IOArhiv = Set.IOArhiv;
                        IODelete = Set.IODelete;
                        VOAdd = Set.VOAdd;
                        VOEdit = Set.VOEdit;
                        VOPrint = Set.VOPrint;
                        VOArhiv = Set.VOArhiv;
                        VODelete = Set.VODelete;
                        Alerts = Set.Alerts;
                        ReactivCellMerge = Set.ReactivCellMerge;
                        PersonalCellMerge = Set.PersonalCellMerge;
                        NTDCellMerge = Set.NTDCellMerge;
                        SICellMerge = Set.SICellMerge;
                        IOCellMerge = Set.IOCellMerge;
                        VOCellMerge = Set.VOCellMerge;
                        VLKStabilnostCellMerge = Set.VLKStabilnostCellMerge;
                        VLKStabilnostProbiCellMerge = Set.VLKStabilnostProbiCellMerge;
                        VLKProbiCellMerge = Set.VLKProbiCellMerge;
                        VLKOperativniiCellMerge = Set.VLKOperativniiCellMerge;
                        VLKOperativniiProbiCellMerge = Set.VLKOperativniiProbiCellMerge;
                        VLKAnalizAdd = Set.VLKAnalizAdd;
                        VLKAnalizEdit = Set.VLKAnalizEdit;
                        VLKAnaliz = Set.VLKAnaliz;
                        VLKAnalizDelete = Set.VLKAnalizDelete;
                        VLKProbiAdd = Set.VLKProbiAdd;
                        VLKProbiEdit = Set.VLKProbiEdit;
                        VLKProbiPrint = Set.VLKProbiPrint;
                        VLKProbiDelete = Set.VLKProbiDelete;
                        VLKOperaivnii = Set.VLKOperaivnii;
                        VLKOperaivniiEdit = Set.VLKOperaivniiEdit;
                        VLKOperaivniiPrint = Set.VLKOperaivniiPrint;
                        VLKOperaivniiDelete = Set.VLKOperaivniiDelete;
                        VLKStabilnosti = Set.VLKStabilnosti;
                        VLKStabilnostiEdit = Set.VLKStabilnostiEdit;
                        VLKStabilnostiPrint = Set.VLKStabilnostiPrint;
                        VLKStabilnostiDelete = Set.VLKStabilnostiDelete;
                        VLKOperaivniiClose = Set.VLKOperaivniiClose;
                        VLKProbiChangeData = Set.VLKProbiChangeData;
                        VLKStabilnostiProtokolOchenka = Set.VLKStabilnostiProtokolOchenka;
                        VLKStabilnostiDiagrams = Set.VLKStabilnostiDiagrams;
                        VLKStabilnostiClose = Set.VLKStabilnostiClose;
                        VLKProbi = Set.VLKProbi;
                        ReactivSplitWidth = Set.ReactivSplitWidth;
                        PersonalSplitWidth = Set.PersonalSplitWidth;
                        IOSplitWidth = Set.IOSplitWidth;
                        NTDSplitWidth = Set.NTDSplitWidth;
                        SISplitWidth = Set.SISplitWidth;
                        VOSplitWidth = Set.VOSplitWidth;
                        db.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

