﻿namespace LIMS
{
    partial class CheckNTDColumnForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckNTDColumnForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.CheckEditND = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditGroup = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditUroven = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditN = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditPrimechanie = new DevExpress.XtraEditors.CheckEdit();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.CancelButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEditSrokDeistvia = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateActualiz = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditRabMesto = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditRabKolvo = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditKontrolMesto = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditKontroKolvo = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateUtverch = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditRegistN = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditName = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditUroven.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrimechanie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokDeistvia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateActualiz.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRabMesto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRabKolvo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditKontrolMesto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditKontroKolvo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateUtverch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRegistN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.CheckEditND);
            this.panelControl1.Controls.Add(this.CheckEditGroup);
            this.panelControl1.Controls.Add(this.CheckEditUroven);
            this.panelControl1.Controls.Add(this.CheckEditN);
            this.panelControl1.Controls.Add(this.CheckEditPrimechanie);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelButton1);
            this.panelControl1.Controls.Add(this.CheckEditSrokDeistvia);
            this.panelControl1.Controls.Add(this.CheckEditDateActualiz);
            this.panelControl1.Controls.Add(this.CheckEditRabMesto);
            this.panelControl1.Controls.Add(this.CheckEditRabKolvo);
            this.panelControl1.Controls.Add(this.CheckEditKontrolMesto);
            this.panelControl1.Controls.Add(this.CheckEditKontroKolvo);
            this.panelControl1.Controls.Add(this.CheckEditDateUtverch);
            this.panelControl1.Controls.Add(this.CheckEditRegistN);
            this.panelControl1.Controls.Add(this.CheckEditName);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(565, 227);
            this.panelControl1.TabIndex = 0;
            // 
            // CheckEditND
            // 
            this.CheckEditND.EditValue = true;
            this.CheckEditND.Location = new System.Drawing.Point(12, 87);
            this.CheckEditND.Name = "CheckEditND";
            this.CheckEditND.Properties.Caption = "НД";
            this.CheckEditND.Size = new System.Drawing.Size(152, 19);
            this.CheckEditND.TabIndex = 4;
            this.CheckEditND.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditGroup
            // 
            this.CheckEditGroup.EditValue = true;
            this.CheckEditGroup.Location = new System.Drawing.Point(12, 37);
            this.CheckEditGroup.Name = "CheckEditGroup";
            this.CheckEditGroup.Properties.Caption = "Группа";
            this.CheckEditGroup.Size = new System.Drawing.Size(152, 19);
            this.CheckEditGroup.TabIndex = 2;
            this.CheckEditGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditUroven
            // 
            this.CheckEditUroven.EditValue = true;
            this.CheckEditUroven.Location = new System.Drawing.Point(12, 12);
            this.CheckEditUroven.Name = "CheckEditUroven";
            this.CheckEditUroven.Properties.Caption = "Уровень документа";
            this.CheckEditUroven.Size = new System.Drawing.Size(163, 19);
            this.CheckEditUroven.TabIndex = 1;
            this.CheckEditUroven.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditN
            // 
            this.CheckEditN.EditValue = true;
            this.CheckEditN.Location = new System.Drawing.Point(12, 62);
            this.CheckEditN.Name = "CheckEditN";
            this.CheckEditN.Properties.Caption = "№ п/п";
            this.CheckEditN.Size = new System.Drawing.Size(112, 19);
            this.CheckEditN.TabIndex = 3;
            this.CheckEditN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditPrimechanie
            // 
            this.CheckEditPrimechanie.EditValue = true;
            this.CheckEditPrimechanie.Location = new System.Drawing.Point(307, 162);
            this.CheckEditPrimechanie.Name = "CheckEditPrimechanie";
            this.CheckEditPrimechanie.Properties.Caption = "Примечание";
            this.CheckEditPrimechanie.Size = new System.Drawing.Size(220, 19);
            this.CheckEditPrimechanie.TabIndex = 14;
            this.CheckEditPrimechanie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(192, 192);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 15;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton1
            // 
            this.CancelButton1.Location = new System.Drawing.Point(298, 192);
            this.CancelButton1.Name = "CancelButton1";
            this.CancelButton1.Size = new System.Drawing.Size(75, 23);
            this.CancelButton1.TabIndex = 16;
            this.CancelButton1.Text = "Отмена";
            this.CancelButton1.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CheckEditSrokDeistvia
            // 
            this.CheckEditSrokDeistvia.EditValue = true;
            this.CheckEditSrokDeistvia.Location = new System.Drawing.Point(307, 137);
            this.CheckEditSrokDeistvia.Name = "CheckEditSrokDeistvia";
            this.CheckEditSrokDeistvia.Properties.Caption = "Срок действия";
            this.CheckEditSrokDeistvia.Size = new System.Drawing.Size(220, 19);
            this.CheckEditSrokDeistvia.TabIndex = 13;
            this.CheckEditSrokDeistvia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditDateActualiz
            // 
            this.CheckEditDateActualiz.EditValue = true;
            this.CheckEditDateActualiz.Location = new System.Drawing.Point(307, 112);
            this.CheckEditDateActualiz.Name = "CheckEditDateActualiz";
            this.CheckEditDateActualiz.Properties.Caption = "Дата последней актуализации";
            this.CheckEditDateActualiz.Size = new System.Drawing.Size(220, 19);
            this.CheckEditDateActualiz.TabIndex = 12;
            this.CheckEditDateActualiz.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditRabMesto
            // 
            this.CheckEditRabMesto.EditValue = true;
            this.CheckEditRabMesto.Location = new System.Drawing.Point(307, 87);
            this.CheckEditRabMesto.Name = "CheckEditRabMesto";
            this.CheckEditRabMesto.Properties.Caption = "Место нахождения (рабочий экземпляр)";
            this.CheckEditRabMesto.Size = new System.Drawing.Size(234, 19);
            this.CheckEditRabMesto.TabIndex = 11;
            this.CheckEditRabMesto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditRabKolvo
            // 
            this.CheckEditRabKolvo.EditValue = true;
            this.CheckEditRabKolvo.Location = new System.Drawing.Point(307, 62);
            this.CheckEditRabKolvo.Name = "CheckEditRabKolvo";
            this.CheckEditRabKolvo.Properties.Caption = "Количество (рабочий экземпляр)";
            this.CheckEditRabKolvo.Size = new System.Drawing.Size(252, 19);
            this.CheckEditRabKolvo.TabIndex = 10;
            this.CheckEditRabKolvo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditKontrolMesto
            // 
            this.CheckEditKontrolMesto.EditValue = true;
            this.CheckEditKontrolMesto.Location = new System.Drawing.Point(307, 37);
            this.CheckEditKontrolMesto.Name = "CheckEditKontrolMesto";
            this.CheckEditKontrolMesto.Properties.Caption = "Место нахождения (контрольный экземпляр)";
            this.CheckEditKontrolMesto.Size = new System.Drawing.Size(252, 19);
            this.CheckEditKontrolMesto.TabIndex = 9;
            this.CheckEditKontrolMesto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditKontroKolvo
            // 
            this.CheckEditKontroKolvo.EditValue = true;
            this.CheckEditKontroKolvo.Location = new System.Drawing.Point(307, 12);
            this.CheckEditKontroKolvo.Name = "CheckEditKontroKolvo";
            this.CheckEditKontroKolvo.Properties.Caption = "Количество (контрольный экземпляр)";
            this.CheckEditKontroKolvo.Size = new System.Drawing.Size(244, 19);
            this.CheckEditKontroKolvo.TabIndex = 8;
            this.CheckEditKontroKolvo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditDateUtverch
            // 
            this.CheckEditDateUtverch.EditValue = true;
            this.CheckEditDateUtverch.Location = new System.Drawing.Point(12, 162);
            this.CheckEditDateUtverch.Name = "CheckEditDateUtverch";
            this.CheckEditDateUtverch.Properties.Caption = "Дата утверждения";
            this.CheckEditDateUtverch.Size = new System.Drawing.Size(152, 19);
            this.CheckEditDateUtverch.TabIndex = 7;
            this.CheckEditDateUtverch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditRegistN
            // 
            this.CheckEditRegistN.EditValue = true;
            this.CheckEditRegistN.Location = new System.Drawing.Point(12, 137);
            this.CheckEditRegistN.Name = "CheckEditRegistN";
            this.CheckEditRegistN.Properties.Caption = "Регистр. № по журн. учёта и регистраций докум.";
            this.CheckEditRegistN.Size = new System.Drawing.Size(289, 19);
            this.CheckEditRegistN.TabIndex = 6;
            this.CheckEditRegistN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditName
            // 
            this.CheckEditName.EditValue = true;
            this.CheckEditName.Location = new System.Drawing.Point(12, 112);
            this.CheckEditName.Name = "CheckEditName";
            this.CheckEditName.Properties.Caption = "Наименование документа";
            this.CheckEditName.Size = new System.Drawing.Size(152, 19);
            this.CheckEditName.TabIndex = 5;
            this.CheckEditName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckNTDColumnForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 227);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "CheckNTDColumnForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ НТД";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckNTDColumnForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditUroven.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrimechanie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokDeistvia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateActualiz.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRabMesto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRabKolvo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditKontrolMesto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditKontroKolvo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateUtverch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRegistN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit CheckEditName;
        private DevExpress.XtraEditors.CheckEdit CheckEditSrokDeistvia;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateActualiz;
        private DevExpress.XtraEditors.CheckEdit CheckEditRabMesto;
        private DevExpress.XtraEditors.CheckEdit CheckEditRabKolvo;
        private DevExpress.XtraEditors.CheckEdit CheckEditKontrolMesto;
        private DevExpress.XtraEditors.CheckEdit CheckEditKontroKolvo;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateUtverch;
        private DevExpress.XtraEditors.CheckEdit CheckEditRegistN;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.SimpleButton CancelButton1;
        private DevExpress.XtraEditors.CheckEdit CheckEditPrimechanie;
        private DevExpress.XtraEditors.CheckEdit CheckEditGroup;
        private DevExpress.XtraEditors.CheckEdit CheckEditUroven;
        private DevExpress.XtraEditors.CheckEdit CheckEditN;
        private DevExpress.XtraEditors.CheckEdit CheckEditND;
    }
}