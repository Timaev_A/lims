﻿using DevExpress.LookAndFeel;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class AlertForm : DevExpress.XtraEditors.XtraForm
    {
        int? ID;
        int number;

        public AlertForm(string Text, int ТипСообщения, int id, int NumberOfDate)
        {
            InitializeComponent();
            //panelControl.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            ID = id;
            number = NumberOfDate;
            if (ТипСообщения == 1)
            {
                panelControl.Appearance.BackColor = Color.Orange;
                panelControl.Appearance.BackColor2 = Color.Orange;
                panelControl.Appearance.Options.UseBackColor = true;
                panelControl.LookAndFeel.UseDefaultLookAndFeel = false;
                panelControl.LookAndFeel.Style = LookAndFeelStyle.Office2003;
            }
            else if (ТипСообщения == 2)
            {
                panelControl.Appearance.BackColor = Color.Red;
                panelControl.Appearance.BackColor2 = Color.Red;
                panelControl.Appearance.Options.UseBackColor = true;
                panelControl.LookAndFeel.UseDefaultLookAndFeel = false;
                panelControl.LookAndFeel.Style = LookAndFeelStyle.Office2003;
            }
            MessageText.Text = Text;
            Opacity = 0;
            try
            {
                Timer timer = new Timer();
                timer.Tick += new EventHandler((sender, e) =>
                {
                    try { if ((Opacity += 0.05d) >= 1) timer.Stop(); } catch { }
                });
                timer.Interval = 100;
                timer.Start();
            }
            catch
            {
            }
        }

        private int x = 0; private int y = 0;

        private void OKButton1_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime? DateNew;
                switch (DateComboBox.Text)
                {
                    case "1 день":
                        DateNew = DateTime.Now.AddDays(1);
                        break;
                    case "5 дней":
                        DateNew = DateTime.Now.AddDays(5);
                        break;
                    case "15 дней":
                        DateNew = DateTime.Now.AddDays(15);
                        break;
                    case "1 месяц":
                        DateNew = DateTime.Now.AddMonths(1);
                        break;
                    case "2 месяца":
                        DateNew = DateTime.Now.AddMonths(2);
                        break;
                    case "3 месяца":
                        DateNew = DateTime.Now.AddMonths(3);
                        break;
                    case "Отключить оповещения до след. входа":
                        Alert.TurnONAlerts = false;
                        DateNew = null;
                        break;
                    default:
                        DateNew = null;
                        break;
                }
                if (DateNew != null)
                {
                    using (DbDataContext db = new DbDataContext())
                    {
                        PersonalAlerts PersonalStroke;
                        SIAlerts SIStroke;
                        IOAlerts IOStroke;
                        VOAlerts VOStroke;
                        switch (number)
                        {
                            case 1:
                                var ReactivStroke = db.ReactivAlerts.Where(c => c.UserName == UserSettings.User && c.ReactivID == ID).FirstOrDefault();
                                if (ReactivStroke == null)
                                {
                                    var NextID = db.ReactivAlerts != null && db.ReactivAlerts.Count() > 0 ? db.ReactivAlerts.Max(c => c.Id) + 1 : 1;
                                    db.ReactivAlerts.InsertOnSubmit(new ReactivAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        ReactivSrokGodnosti = DateNew,
                                        ReactivID = ID,
                                    });
                                }
                                else
                                {
                                    ReactivStroke.ReactivSrokGodnosti = DateNew;
                                }
                                break;
                            case 2:
                                PersonalStroke = db.PersonalAlerts.Where(c => c.UserName == UserSettings.User && c.PersonalID == ID).FirstOrDefault();
                                if (PersonalStroke == null)
                                {
                                    var NextID = db.PersonalAlerts != null && db.PersonalAlerts.Count() > 0 ? db.PersonalAlerts.Max(c => c.Id) + 1 : 1;
                                    db.PersonalAlerts.InsertOnSubmit(new PersonalAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        PersonalSrokAttestachii = DateNew,
                                        PersonalID = ID,
                                    });
                                }
                                else
                                {
                                    PersonalStroke.PersonalSrokAttestachii = DateNew;
                                }
                                break;
                            case 3:
                                PersonalStroke = db.PersonalAlerts.Where(c => c.UserName == UserSettings.User && c.PersonalID == ID).FirstOrDefault();
                                if (PersonalStroke == null)
                                {
                                    var NextID = db.PersonalAlerts != null && db.PersonalAlerts.Count() > 0 ? db.PersonalAlerts.Max(c => c.Id) + 1 : 1;
                                    db.PersonalAlerts.InsertOnSubmit(new PersonalAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        PersonalSrokPodvergdenia = DateNew,
                                        PersonalID = ID,
                                    });
                                }
                                else
                                {
                                    PersonalStroke.PersonalSrokPodvergdenia = DateNew;
                                }
                                break;
                            case 4:
                                PersonalStroke = db.PersonalAlerts.Where(c => c.UserName == UserSettings.User && c.PersonalID == ID).FirstOrDefault();
                                if (PersonalStroke == null)
                                {
                                    var NextID = db.PersonalAlerts != null && db.PersonalAlerts.Count() > 0 ? db.PersonalAlerts.Max(c => c.Id) + 1 : 1;
                                    db.PersonalAlerts.InsertOnSubmit(new PersonalAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        PersonalSrokPodvergdeniaGas = DateNew,
                                        PersonalID = ID,
                                    });
                                }
                                else
                                {
                                    PersonalStroke.PersonalSrokPodvergdeniaGas = DateNew;
                                }
                                break;
                            case 5:
                                PersonalStroke = db.PersonalAlerts.Where(c => c.UserName == UserSettings.User && c.PersonalID == ID).FirstOrDefault();
                                if (PersonalStroke == null)
                                {
                                    var NextID = db.PersonalAlerts != null && db.PersonalAlerts.Count() > 0 ? db.PersonalAlerts.Max(c => c.Id) + 1 : 1;
                                    db.PersonalAlerts.InsertOnSubmit(new PersonalAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        PersonalSrokProverkiZnaniiOTiTB = DateNew,
                                        PersonalID = ID,
                                    });
                                }
                                else
                                {
                                    PersonalStroke.PersonalSrokProverkiZnaniiOTiTB = DateNew;
                                }
                                break;
                            case 6:
                                var NTDStroke = db.NTDAlerts.Where(c => c.UserName == UserSettings.User && c.NTDID == ID).FirstOrDefault();
                                if (NTDStroke == null)
                                {
                                    var NextID = db.NTDAlerts != null && db.NTDAlerts.Count() > 0 ? db.NTDAlerts.Max(c => c.Id) + 1 : 1;
                                    db.NTDAlerts.InsertOnSubmit(new NTDAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        NTDSrokDeistvia = DateNew,
                                        NTDID = ID,
                                    });
                                }
                                else
                                {
                                    NTDStroke.NTDSrokDeistvia = DateNew;
                                }
                                break;
                            case 7:
                                SIStroke = db.SIAlerts.Where(c => c.UserName == UserSettings.User && c.SIID == ID).FirstOrDefault();
                                if (SIStroke == null)
                                {
                                    var NextID = db.SIAlerts != null && db.SIAlerts.Count() > 0 ? db.SIAlerts.Max(c => c.Id) + 1 : 1;
                                    db.SIAlerts.InsertOnSubmit(new SIAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        SIDataSledPoverki = DateNew,
                                        SIID = ID,
                                    });
                                }
                                else
                                {
                                    SIStroke.SIDataSledPoverki = DateNew;
                                }
                                break;
                            case 8:
                                SIStroke = db.SIAlerts.Where(c => c.UserName == UserSettings.User && c.SIID == ID).FirstOrDefault();
                                if (SIStroke == null)
                                {
                                    var NextID = db.SIAlerts != null && db.SIAlerts.Count() > 0 ? db.SIAlerts.Max(c => c.Id) + 1 : 1;
                                    db.SIAlerts.InsertOnSubmit(new SIAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        SIDateSledTO = DateNew,
                                        SIID = ID,
                                    });
                                }
                                else
                                {
                                    SIStroke.SIDateSledTO = DateNew;
                                }
                                break;
                            case 9:
                                IOStroke = db.IOAlerts.Where(c => c.UserName == UserSettings.User && c.IOID == ID).FirstOrDefault();
                                if (IOStroke == null)
                                {
                                    var NextID = db.IOAlerts != null && db.IOAlerts.Count() > 0 ? db.IOAlerts.Max(c => c.Id) + 1 : 1;
                                    db.IOAlerts.InsertOnSubmit(new IOAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        IODateSledAttestacii = DateNew,
                                        IOID = ID,
                                    });
                                }
                                else
                                {
                                    IOStroke.IODateSledAttestacii = DateNew;
                                }
                                break;
                            case 10:
                                IOStroke = db.IOAlerts.Where(c => c.UserName == UserSettings.User && c.IOID == ID).FirstOrDefault();
                                if (IOStroke == null)
                                {
                                    var NextID = db.IOAlerts != null && db.IOAlerts.Count() > 0 ? db.IOAlerts.Max(c => c.Id) + 1 : 1;
                                    db.IOAlerts.InsertOnSubmit(new IOAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        IODateSledTO = DateNew,
                                        IOID = ID,
                                    });
                                }
                                else
                                {
                                    IOStroke.IODateSledTO = DateNew;
                                }
                                break;
                            case 11:
                                VOStroke = db.VOAlerts.Where(c => c.UserName == UserSettings.User && c.VOID == ID).FirstOrDefault();
                                if (VOStroke == null)
                                {
                                    var NextID = db.VOAlerts != null && db.VOAlerts.Count() > 0 ? db.VOAlerts.Max(c => c.Id) + 1 : 1;
                                    db.VOAlerts.InsertOnSubmit(new VOAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        VODateSledPoverki = DateNew,
                                        VOID = ID,
                                    });
                                }
                                else
                                {
                                    VOStroke.VODateSledPoverki = DateNew;
                                }
                                break;
                            case 12:
                                VOStroke = db.VOAlerts.Where(c => c.UserName == UserSettings.User && c.VOID == ID).FirstOrDefault();
                                if (VOStroke == null)
                                {
                                    var NextID = db.VOAlerts != null && db.VOAlerts.Count() > 0 ? db.VOAlerts.Max(c => c.Id) + 1 : 1;
                                    db.VOAlerts.InsertOnSubmit(new VOAlerts
                                    {
                                        Id = NextID,
                                        UserName = UserSettings.User,
                                        VODateSledTO = DateNew,
                                        VOID = ID,
                                    });
                                }
                                else
                                {
                                    VOStroke.VODateSledTO = DateNew;
                                }
                                break;
                        }
                        db.SubmitChanges();
                        db.Dispose();
                    }
                }

                //Скрытие
                Opacity = 1;
                Timer timer = new Timer();
                timer.Tick += new EventHandler((sender2, e2) =>
                {
                    try
                    {
                        if ((Opacity -= 0.1) <= 0)
                        {
                            timer.Stop();
                            lock (Alert.WindowsHeights)
                                Alert.WindowsHeights.RemoveAt(Alert.WindowsHeights.FindIndex(c => c == Convert.ToInt32(PictureBox1.AccessibleName)));
                            this.Close();
                            this.Dispose();
                        }
                    }
                    catch { }
                });
                timer.Interval = 100;
                timer.Start();
            }
            catch { }
        }

        private void DateComboBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (DateComboBox.Items.Count > 6)
            {
                DateComboBox.Items.RemoveAt(0);
            }
        }

        private void panelControl_MouseDown(object sender, MouseEventArgs e)
        {
            x = e.X; y = e.Y;
        }

        private void panelControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Location = new System.Drawing.Point(this.Location.X + (e.X - x), this.Location.Y + (e.Y - y));
            }
        }

        private void AlertForm_Load(object sender, EventArgs e)
        {
            DateComboBox.SelectedIndex = 0;
            PictureBox1.AccessibleName = (Location.Y + Size.Height).ToString();
        }

        private void PictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            x = e.X; y = e.Y;
        }

        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Location = new System.Drawing.Point(this.Location.X + (e.X - x), this.Location.Y + (e.Y - y));
            }
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            x = e.X; y = e.Y;
        }

        private void label1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Location = new System.Drawing.Point(this.Location.X + (e.X - x), this.Location.Y + (e.Y - y));
            }
        }

        private void MessageText_MouseDown(object sender, MouseEventArgs e)
        {
            x = e.X; y = e.Y;
        }

        private void MessageText_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Location = new System.Drawing.Point(this.Location.X + (e.X - x), this.Location.Y + (e.Y - y));
            }
        }

        private void DateComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                     OKButton1_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void AlertForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
