﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class AddProbiForm : DevExpress.XtraEditors.XtraForm
    {
        MainForm MF;
        int? AnalizID;
        int ProbaID = -1;
        public AddProbiForm(MainForm Main, int id)
        {
            InitializeComponent();
            MF = Main;
            AnalizID = id;
            Date.Value = DateTime.Now;
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroke = db.Analiz.Where(c => c.Id == id).FirstOrDefault();
                    if (Stroke != null)
                    {
                        var Prob = db.Probi.Where(c => c.AnalizID == id && c.Дата > new DateTime(DateTime.Now.Year, 1, 1) && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1)).OrderByDescending(c => c.Id).ToList();
                        int NumderOfShifr = 0;
                        if (Prob != null && Prob.Count > 0)
                        {
                            var t = Prob[0].Шифр.Split('-');
                            NumderOfShifr = Convert.ToInt32(t[1]);
                        }
                        Shifr2.Text = (NumderOfShifr + 1).ToString();
                    }
                    Shifr1.Text = Stroke.Шифр + "-";
                    Shifr3.Text = DateTime.Now.Year.ToString().Substring(2, 2);
                    Date.Value = DateTime.Now;
                    KontroliruemiiObekt.Text = Stroke.КонтролируемыйОбъект;
                    OpredelyaemiiKomponent.Text = Stroke.ОпределяемыйКомпонент + ", " + Stroke.ЕдиницаИзмерения + ", " + Stroke.НД;
                    IspolnitelName.DataSource = GlobalStatic.GetPersonal();
                    VidalName.DataSource = GlobalStatic.GetPersonal();
                    PoluchilName1.DataSource = GlobalStatic.GetPersonal();
                    PoluchilName2.DataSource = GlobalStatic.GetPersonal();
                    IspolnitelName.Text = "";
                    VidalName.Text = "";
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        public AddProbiForm(MainForm Main, Probi Stroke)
        {
            InitializeComponent();
            Text = "АРМЛАБ Изменить";
            MF = Main;
            AnalizID = Stroke.AnalizID;
            ProbaID = Stroke.Id;
            Date.Value = Stroke.Дата.HasValue ? Stroke.Дата.Value : DateTime.Now;
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Shif = Stroke.Шифр.Split('-');
                    Shifr1.Text = Shif[0] + "-";
                    Shifr2.Text = Shif[1];
                    Shifr3.Text = Shif[2];
                    KontroliruemiiObekt.Text = Stroke.КонтролируемыйОбъект;
                    OpredelyaemiiKomponent.Text = Stroke.ОпределяемыйКомпонент;
                    AttestovannoeZnachenie.Value = Stroke.АттестованноеЗначение.HasValue ? (decimal)Stroke.АттестованноеЗначение.Value : 0;
                    //AttestovannoeZnachenie.Enabled = false;
                    IspolnitelName.DataSource = GlobalStatic.GetPersonal();
                    VidalName.DataSource = GlobalStatic.GetPersonal();
                    PoluchilName1.DataSource = GlobalStatic.GetPersonal();
                    PoluchilName2.DataSource = GlobalStatic.GetPersonal();
                    if (Stroke.Исполнитель != null && Stroke.Исполнитель != "")
                    {
                        var Ispolnit = Stroke.Исполнитель.Split(':');
                        if (Ispolnit[0] != "" && Ispolnit[0] != null)
                            SetTextToComboBox(Ispolnit[0], Ispolnitel, true);
                        else Ispolnitel.Text = "";
                        if (Ispolnit[1] != "" && Ispolnit[1] != null)
                            SetTextToComboBox(Ispolnit[1].Substring(1, Ispolnit[1].Length - 1), IspolnitelName);
                        else IspolnitelName.Text = "";
                    }
                    else
                    {
                        Ispolnitel.Text = "";
                        IspolnitelName.Text = "";
                    }
                    if (Stroke.Выдал != null && Stroke.Выдал != "")
                    {
                        var Vidacha = Stroke.Выдал.Split(':');
                        if (Vidacha[0] != "" && Vidacha[0] != null)
                            SetTextToComboBox(Vidacha[0], Vidal, true);
                        else Vidal.Text = "";
                        if (Vidacha[1] != "" && Vidacha[1] != null)
                            SetTextToComboBox(Vidacha[1].Substring(1, Vidacha[1].Length - 1), VidalName);
                        else VidalName.Text = "";
                    }
                    else
                    {
                        Vidal.Text = "";
                        VidalName.Text = "";
                    }
                    if (Stroke.Получил != null && Stroke.Получил != "")
                    {
                        var Poluchateli = Stroke.Получил.Split(Convert.ToChar(13));   //1049
                        if (Poluchateli.Length > 0)
                        {
                            var Poluchatel1 = Poluchateli[0].Split(':');
                            if (Poluchatel1[0] != "" && Poluchatel1[0] != null)
                                SetTextToComboBox(Poluchatel1[0], Poluchil1, true);
                            else Poluchil1.Text = "";
                            if (Poluchatel1[1] != "" && Poluchatel1[1] != null)
                                SetTextToComboBox(Poluchatel1[1].Substring(1, Poluchatel1[1].Length - 1), PoluchilName1);
                            else PoluchilName1.Text = "";
                        }
                        if (Poluchateli.Length > 1)
                        {
                            var Poluchatel2 = Poluchateli[1].Split(':');
                            if (Poluchatel2[0] != "" && Poluchatel2[0] != null)
                                SetTextToComboBox(Poluchatel2[0], Poluchil2, true);
                            else Poluchil2.Text = "";
                            if (Poluchatel2[1] != "" && Poluchatel2[1] != null)
                                SetTextToComboBox(Poluchatel2[1].Substring(1, Poluchatel2[1].Length - 1), PoluchilName2);
                            else PoluchilName2.Text = "";
                        }
                        else
                        {
                            Poluchil2.Text = "";
                            PoluchilName2.Text = "";
                        }
                    }
                    else
                    {
                        Poluchil1.Text = "";
                        PoluchilName1.Text = "";
                        Poluchil2.Text = "";
                        PoluchilName2.Text = "";
                    }
                    AddOperativniiKontrolCheckBox.Checked = Stroke.OperativniiKontrolID != null && Stroke.OperativniiKontrolID != 0 && Stroke.OperativniiKontrolID != null;
                    AddKontrolStabilnostiCheckBox.Checked = Stroke.KontrolStabilnostiID != null && Stroke.KontrolStabilnostiID != 0 && Stroke.KontrolStabilnostiID != null;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SetTextToComboBox(string Ispolnit, ComboBox CB, bool Item = false)
        {
            if (!Item)
            {
                var tempData = (List<string>)CB.DataSource;
                if (!tempData.Contains(Ispolnit))
                {
                    tempData.Add(Ispolnit);
                    CB.DataSource = tempData;
                }
            }
            else
            {
                if (!CB.Items.Contains(Ispolnit))
                    CB.Items.Add(Ispolnit);
            }
            CB.Text = Ispolnit;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ContextMenuEvent()
        {
            foreach (Control C in panelControl1.Controls)
            {
                if (C is TextBox || C is ComboBox)
                {
                    C.ContextMenuStrip = MF.ContextMenuStripMain;
                }
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void AddProbiForm_Load(object sender, EventArgs e)
        {
            ContextMenuEvent();
        }

        private void Save()
        {
            try
            {
                var t = Convert.ToInt32(Shifr2.Text);
            }
            catch
            {
                MessageBox.Show("Не корректные данные!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var AttestZN = (double)AttestovannoeZnachenie.Value;
                    int? OperativniiId = null;
                    int? StabilnostiId = null;
                    var NextID = db.Probi != null && db.Probi.Count() > 0 ? db.Probi.Max(c => c.Id) + 1 : 1;
                    var PoluchilSumm = PoluchilName1.Text != "" ? Poluchil1.Text + ": " + PoluchilName1.Text : "";
                    PoluchilSumm = PoluchilName1.Text != "" && PoluchilName2.Text != "" ? PoluchilSumm + "\r" : PoluchilSumm;
                    PoluchilSumm = PoluchilName2.Text != "" ? PoluchilSumm + Poluchil2.Text + ": " + PoluchilName2.Text : PoluchilSumm;
                    if (ProbaID >= 0) //Редактирование
                    {
                        var Stroka = db.Probi.Where(c => c.Id == ProbaID && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                        if (Stroka != null)
                        {
                            Stroka.Дата = Date.Value;
                            Stroka.Исполнитель = IspolnitelName.Text != "" ? Ispolnitel.Text + ": " + IspolnitelName.Text : "";
                            Stroka.Выдал = VidalName.Text != "" ? Vidal.Text + ": " + VidalName.Text : "";
                            Stroka.Получил = PoluchilSumm;
                            Stroka.Шифр = Shifr1.Text + Shifr2.Text + "-" + Shifr3.Text;
                            Stroka.КонтролируемыйОбъект = KontroliruemiiObekt.Text;
                            Stroka.ОпределяемыйКомпонент = OpredelyaemiiKomponent.Text;
                            Stroka.АттестованноеЗначение = (double)AttestovannoeZnachenie.Value;
                            if ((AddOperativniiKontrolCheckBox.Checked && Stroka.OperativniiKontrolID == null) || (!AddOperativniiKontrolCheckBox.Checked))
                                Stroka.OperativniiKontrolID = GetOperativniiKontrolID(db, AttestZN, Stroka);
                            if ((AddKontrolStabilnostiCheckBox.Checked && Stroka.KontrolStabilnostiID == null) || !AddKontrolStabilnostiCheckBox.Checked)
                                Stroka.KontrolStabilnostiID = GetKontrolStabilnostiID(db, AttestZN, Stroka);
                            Stroka.ДатаСозданияЗаписи = DateTime.Now;
                            Stroka.UserName = UserSettings.User;
                            Stroka.Owner = null;
                            Stroka.VisibleStatus = 0;
                            if (Stroka.АттестованноеЗначение.HasValue && Stroka.X1.HasValue && Stroka.X2.HasValue)
                            {
                                var N1 = VLKStatic.GetRoundNumber(Stroka.АттестованноеЗначение.Value);
                                var N2 = VLKStatic.GetRoundNumber(Stroka.X1.Value);
                                var N3 = VLKStatic.GetRoundNumber(Stroka.X2.Value);
                                var cont = N1 >= N2 ? N1 >= N3 ? N1 : N3 : N2 >= N3 ? N2 : N3;
                                Stroka.KK = Math.Round(Stroka.X.Value - Stroka.АттестованноеЗначение.Value, cont, MidpointRounding.AwayFromZero);
                            }
                            //История
                            db.Probi.InsertOnSubmit(new Probi
                            {
                                Id = NextID,
                                Дата = Stroka.Дата,
                                Исполнитель = Stroka.Исполнитель,
                                Шифр = Stroka.Шифр,
                                КонтролируемыйОбъект = Stroka.КонтролируемыйОбъект,
                                ОпределяемыйКомпонент = Stroka.ОпределяемыйКомпонент,
                                АттестованноеЗначение = Stroka.АттестованноеЗначение,
                                Выдал = Stroka.Выдал,
                                Получил = Stroka.Получил,
                                X1 = Stroka.X1,
                                X2 = Stroka.X2,
                                X = Stroka.X,
                                rSmall = Stroka.rSmall,
                                RBig = Stroka.RBig,
                                KK = Stroka.KK,
                                OperativniiKontrolID = Stroka.OperativniiKontrolID,
                                KontrolStabilnostiID = Stroka.KontrolStabilnostiID,
                                AnalizID = Stroka.AnalizID,
                                ДатаСозданияЗаписи = DateTime.Now,
                                UserName = UserSettings.User,
                                Owner = Stroka.Id,
                                VisibleStatus = 200,
                            });
                        }
                        else
                        {
                            MessageBox.Show("Ошибка сохранения данных!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    else //Добавить новый 
                    {
                        if (AttestovannoeZnachenie.Value == 0)
                        {
                            MessageBox.Show("Поле \"Аттестованное значение\" не должно быть равным нулю", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        if (AddOperativniiKontrolCheckBox.Checked)
                            OperativniiId = GetOperativniiKontrolID(db, AttestZN, null);
                        if (AddKontrolStabilnostiCheckBox.Checked)
                            StabilnostiId = GetKontrolStabilnostiID(db, AttestZN, null);
                        db.Probi.InsertOnSubmit(new Probi
                        {
                            Id = NextID,
                            Дата = Date.Value,
                            Исполнитель = IspolnitelName.Text != "" ? Ispolnitel.Text + ": " + IspolnitelName.Text : "",
                            Шифр = Shifr1.Text + Shifr2.Text + "-" + Shifr3.Text,
                            КонтролируемыйОбъект = KontroliruemiiObekt.Text,
                            ОпределяемыйКомпонент = OpredelyaemiiKomponent.Text,
                            АттестованноеЗначение = (double)AttestovannoeZnachenie.Value,
                            Выдал = VidalName.Text != "" ? Vidal.Text + ": " + VidalName.Text : "",
                            Получил = PoluchilSumm,
                            X1 = null,
                            X2 = null,
                            X = null,
                            rSmall = null,
                            RBig = null,
                            KK = null,
                            OperativniiKontrolID = OperativniiId,
                            KontrolStabilnostiID = StabilnostiId,
                            AnalizID = AnalizID,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = null,
                            VisibleStatus = 0,
                        });
                        db.Probi.InsertOnSubmit(new Probi
                        {
                            Id = NextID + 1,
                            Дата = Date.Value,
                            Исполнитель = IspolnitelName.Text != "" ? Ispolnitel.Text + ": " + IspolnitelName.Text : "",
                            Шифр = Shifr1.Text + Shifr2.Text + "-" + Shifr3.Text,
                            КонтролируемыйОбъект = KontroliruemiiObekt.Text,
                            ОпределяемыйКомпонент = OpredelyaemiiKomponent.Text,
                            АттестованноеЗначение = (double)AttestovannoeZnachenie.Value,
                            Выдал = VidalName.Text != "" ? Vidal.Text + ": " + VidalName.Text : "",
                            Получил = PoluchilSumm,
                            X1 = null,
                            X2 = null,
                            X = null,
                            rSmall = null,
                            RBig = null,
                            KK = null,
                            OperativniiKontrolID = OperativniiId,
                            KontrolStabilnostiID = StabilnostiId,
                            AnalizID = AnalizID,
                            ДатаСозданияЗаписи = DateTime.Now,
                            UserName = UserSettings.User,
                            Owner = NextID,
                            VisibleStatus = 100,
                        });
                    }                    
                    db.SubmitChanges();
                    db.Dispose();
                }
                Close();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Добавление пробы в оперативный контрол и возврат ID
        private int? GetOperativniiKontrolID(DbDataContext db, double AttestZN, Probi Stroke = null)
        {
            try
            {
                if (AddOperativniiKontrolCheckBox.Checked && (Stroke == null || Stroke.OperativniiKontrolID == null))
                {
                    var OperativniiKontr = db.OperativniiKontrol.Where(c => c.AnalizID == AnalizID && c.EndDate == null && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderByDescending(c => c.CreateDate).FirstOrDefault();
                    var NextIDOperativnii = db.OperativniiKontrol != null && db.OperativniiKontrol.Count() > 0 ? db.OperativniiKontrol.Max(c => c.Id) + 1 : 1;
                    if (OperativniiKontr != null)
                    {                        
                        OperativniiKontr.Колво = OperativniiKontr.Колво + 1;
                        OperativniiKontr.ДатаСозданияЗаписи = DateTime.Now;
                        OperativniiKontr.UserName = UserSettings.User;
                        db.OperativniiKontrol.InsertOnSubmit(new OperativniiKontrol
                        {
                            Id = NextIDOperativnii,
                            AnalizID = OperativniiKontr.AnalizID,
                            Book = OperativniiKontr.Book,
                            CreateDate = OperativniiKontr.CreateDate,
                            EndDate = OperativniiKontr.EndDate,
                            Owner = OperativniiKontr.Id,
                            UserName = UserSettings.User,
                            ДатаСозданияЗаписи = DateTime.Now,
                            VisibleStatus = 200,
                            Колво = OperativniiKontr.Колво,
                        });
                        db.SubmitChanges();
                        return OperativniiKontr.Id;
                    }
                    else
                    {
                        db.OperativniiKontrol.InsertOnSubmit(new OperativniiKontrol
                        {
                            Id = NextIDOperativnii,
                            AnalizID = AnalizID,
                            Book = 1,
                            CreateDate = DateTime.Now,
                            EndDate = null,
                            Owner = null,
                            UserName = UserSettings.User,
                            ДатаСозданияЗаписи = DateTime.Now,
                            VisibleStatus = 0,
                            Колво = 1,
                        });
                        //История
                        db.OperativniiKontrol.InsertOnSubmit(new OperativniiKontrol
                        {
                            Id = NextIDOperativnii + 1,
                            AnalizID = AnalizID,
                            Book = 1,
                            CreateDate = DateTime.Now,
                            EndDate = null,
                            Owner = NextIDOperativnii,
                            UserName = UserSettings.User,
                            ДатаСозданияЗаписи = DateTime.Now,
                            VisibleStatus = 100,
                            Колво = 1,
                        });
                        db.SubmitChanges();
                        return NextIDOperativnii;
                    }
                }
                else //  удаление если не отмечено Оперативный контрол
                {
                    if (!AddOperativniiKontrolCheckBox.Checked && Stroke.OperativniiKontrolID != null && Stroke.OperativniiKontrolID != 0)
                    {
                        var SelectedOperKontrol = db.OperativniiKontrol.Where(c => c.Id == Stroke.OperativniiKontrolID).FirstOrDefault();
                        var NextID = db.OperativniiKontrol != null && db.OperativniiKontrol.Count() > 0 ? db.OperativniiKontrol.Max(c => c.Id) + 1 : 1;
                        SelectedOperKontrol.Колво = SelectedOperKontrol.Колво <= 0 ? 0 : SelectedOperKontrol.Колво - 1;
                        SelectedOperKontrol.UserName = UserSettings.User;
                        SelectedOperKontrol.ДатаСозданияЗаписи = DateTime.Now;
                        db.OperativniiKontrol.InsertOnSubmit(new OperativniiKontrol
                        {
                            Id = NextID,
                            AnalizID = SelectedOperKontrol.AnalizID,
                            AttestovannoeZnachenie = SelectedOperKontrol.AttestovannoeZnachenie,
                            Book = SelectedOperKontrol.Book,
                            CreateDate = SelectedOperKontrol.CreateDate,
                            Колво = SelectedOperKontrol.Колво,
                            EndDate = SelectedOperKontrol.EndDate,
                            Owner = SelectedOperKontrol.Id,
                            UserName = UserSettings.User,
                            VisibleStatus = 200,
                            ДатаСозданияЗаписи = DateTime.Now,
                        });
                    }
                    db.SubmitChanges();
                    return null;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        //Добавление пробы в контроль стабильности и возврат ID
        private int? GetKontrolStabilnostiID(DbDataContext db, double AttestZN, Probi Stroke = null)
        {
            try
            {
                if (AddKontrolStabilnostiCheckBox.Checked && (Stroke == null || Stroke.KontrolStabilnostiID == null))
                {
                    var KontrolStabilnosti = db.KontrolStabilnosti.Where(c => c.AnalizID == AnalizID && c.AttestovannoeZnachenie == AttestZN && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderByDescending(c => c.CreateDate).FirstOrDefault(); 
                    var NextIDStabilnosti = db.KontrolStabilnosti != null && db.KontrolStabilnosti.Count() > 0 ? db.KontrolStabilnosti.Max(c => c.Id) + 1 : 1;
                    if (KontrolStabilnosti != null)
                    {
                        var AnalizTableID = ReturnAnalizTableID(db, KontrolStabilnosti.AnalizID, AttestZN);
                        int? KolvoProb = null;
                        if (AnalizTableID != null)
                            KolvoProb = db.AnalizTable.Where(c => c.Id == AnalizTableID).FirstOrDefault().КолВоПроб;
                        if (AnalizTableID != null && KolvoProb != null && KolvoProb <= KontrolStabilnosti.Колво)
                        {
                            DialogResult dialogResult = MessageBox.Show("Журнал контроля стабильности №" + KontrolStabilnosti.Book + " заполнен! Закрыть и начать новую?", "Контроль стабильности", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dialogResult == DialogResult.Yes)
                            {
                                KontrolStabilnosti.VisibleStatus = 1;
                                KontrolStabilnosti.EndDate = DateTime.Now;
                                db.KontrolStabilnosti.InsertOnSubmit(new KontrolStabilnosti
                                {
                                    Id = NextIDStabilnosti,
                                    AnalizID = AnalizID,
                                    Book = KontrolStabilnosti.Book + 1,
                                    CreateDate = DateTime.Now,
                                    AttestovannoeZnachenie = AttestZN,
                                    EndDate = null,
                                    Owner = null,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    VisibleStatus = 0,
                                    Колво = 1,
                                });
                                //История
                                db.KontrolStabilnosti.InsertOnSubmit(new KontrolStabilnosti
                                {
                                    Id = NextIDStabilnosti + 1,
                                    AnalizID = AnalizID,
                                    Book = KontrolStabilnosti.Book + 1,
                                    CreateDate = DateTime.Now,
                                    AttestovannoeZnachenie = AttestZN,
                                    EndDate = null,
                                    Owner = NextIDStabilnosti,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    VisibleStatus = 100,
                                    Колво = 1,
                                });
                                db.SubmitChanges();
                                return NextIDStabilnosti;
                            }
                            else
                            {
                                KontrolStabilnosti.Колво = KontrolStabilnosti.Колво + 1;
                                KontrolStabilnosti.ДатаСозданияЗаписи = DateTime.Now;
                                KontrolStabilnosti.UserName = UserSettings.User;
                                db.OperativniiKontrol.InsertOnSubmit(new OperativniiKontrol
                                {
                                    Id = NextIDStabilnosti,
                                    AnalizID = AnalizID,
                                    Book = KontrolStabilnosti.Book,
                                    CreateDate = DateTime.Now,
                                    AttestovannoeZnachenie = AttestZN,
                                    EndDate = null,
                                    Owner = KontrolStabilnosti.Id,
                                    UserName = UserSettings.User,
                                    ДатаСозданияЗаписи = DateTime.Now,
                                    VisibleStatus = 200,
                                    Колво = 1,
                                });
                                db.SubmitChanges();
                                return KontrolStabilnosti.Id;
                            }
                        }
                        else
                        {
                            KontrolStabilnosti.Колво = KontrolStabilnosti.Колво + 1;
                            KontrolStabilnosti.ДатаСозданияЗаписи = DateTime.Now;
                            KontrolStabilnosti.UserName = UserSettings.User;
                            db.KontrolStabilnosti.InsertOnSubmit(new KontrolStabilnosti
                            {
                                Id = NextIDStabilnosti,
                                AnalizID = KontrolStabilnosti.AnalizID,
                                Book = KontrolStabilnosti.Book,
                                CreateDate = KontrolStabilnosti.CreateDate,
                                AttestovannoeZnachenie = KontrolStabilnosti.AttestovannoeZnachenie,
                                EndDate = KontrolStabilnosti.EndDate,
                                Owner = KontrolStabilnosti.Id,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                VisibleStatus = 200,
                                Колво = KontrolStabilnosti.Колво,
                            });
                            db.SubmitChanges();
                            return KontrolStabilnosti.Id;
                        }
                    }
                    else
                    {
                        var NextBook = db.KontrolStabilnosti.Where(c => c.AnalizID == AnalizID) != null && db.KontrolStabilnosti.Where(c => c.AnalizID == AnalizID).Max(c => c.Book).HasValue ? db.KontrolStabilnosti.Where(c => c.AnalizID == AnalizID).Max(c => c.Book).Value + 1 : 1;
                        db.KontrolStabilnosti.InsertOnSubmit(new KontrolStabilnosti
                        {
                            Id = NextIDStabilnosti,
                            AnalizID = AnalizID,
                            Book = NextBook,
                            CreateDate = DateTime.Now,
                            AttestovannoeZnachenie = AttestZN,
                            EndDate = null,
                            Owner = null,
                            UserName = UserSettings.User,
                            ДатаСозданияЗаписи = DateTime.Now,
                            VisibleStatus = 0,
                            Колво = 1,
                        });
                        //История
                        db.KontrolStabilnosti.InsertOnSubmit(new KontrolStabilnosti
                        {
                            Id = NextIDStabilnosti + 1,
                            AnalizID = AnalizID,
                            Book = NextBook,
                            CreateDate = DateTime.Now,
                            AttestovannoeZnachenie = AttestZN,
                            EndDate = null,
                            Owner = NextIDStabilnosti,
                            UserName = UserSettings.User,
                            ДатаСозданияЗаписи = DateTime.Now,
                            VisibleStatus = 100,
                            Колво = 1,
                        });
                        db.SubmitChanges();
                        return NextIDStabilnosti;
                    }
                }
                else //  удаление если не отмечено Оперативный контрол
                {
                    if (!AddKontrolStabilnostiCheckBox.Checked && Stroke.KontrolStabilnostiID != null && Stroke.KontrolStabilnostiID != 0)
                    {
                        var SelectedKontrolStabilnosti = db.KontrolStabilnosti.Where(c => c.Id == Stroke.KontrolStabilnostiID).FirstOrDefault();
                        var NextID = db.KontrolStabilnosti != null && db.KontrolStabilnosti.Count() > 0 ? db.KontrolStabilnosti.Max(c => c.Id) + 1 : 1;
                        SelectedKontrolStabilnosti.Колво = SelectedKontrolStabilnosti.Колво <= 0 ? 0 : SelectedKontrolStabilnosti.Колво - 1;
                        SelectedKontrolStabilnosti.UserName = UserSettings.User;
                        SelectedKontrolStabilnosti.ДатаСозданияЗаписи = DateTime.Now;
                        db.KontrolStabilnosti.InsertOnSubmit(new KontrolStabilnosti
                        {
                            Id = NextID,
                            AnalizID = SelectedKontrolStabilnosti.AnalizID,
                            AttestovannoeZnachenie = SelectedKontrolStabilnosti.AttestovannoeZnachenie,
                            Book = SelectedKontrolStabilnosti.Book,
                            CreateDate = SelectedKontrolStabilnosti.CreateDate,
                            Колво = SelectedKontrolStabilnosti.Колво,
                            EndDate = SelectedKontrolStabilnosti.EndDate,
                            Owner = SelectedKontrolStabilnosti.Id,
                            UserName = UserSettings.User,
                            VisibleStatus = 200,
                            ДатаСозданияЗаписи = DateTime.Now,
                        });
                    }
                    db.SubmitChanges();
                    return null;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private int? ReturnAnalizTableID(DbDataContext db, int? AnalizID, double AttestovannoeZnachenie)
        {
            try
            {
                var AnalizTables = db.AnalizTable.Where(c => c.AnalizID == AnalizID && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1)).ToList();
                if (AnalizTables.Count == 1 && AnalizTables[0].Up == null && AnalizTables[0].Down == null && AnalizTables[0].РасчетноеЗначение != null)
                {
                    //Написать функцию расчета при расчетных значениях
                }
                if (AnalizTables.Count > 0)
                {
                    foreach (AnalizTable One in AnalizTables)
                    {
                        var newOt = One.Down.HasValue ? One.Down.Value : 0;
                        var newDo = One.Up.HasValue ? One.Up.Value : 9999999999999999999;
                        if (newOt < AttestovannoeZnachenie && newDo >= AttestovannoeZnachenie)
                        {
                            return One.Id;
                        }                        
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void Date_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    SaveButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void AddProbiForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
