﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class AddProbiDataForm : DevExpress.XtraEditors.XtraForm
    {
        int ProbaID;
        public AddProbiDataForm(MainForm MF, Probi Stroke)
        {
            InitializeComponent();
            ProbaID = Stroke.Id;
            Date.Text = Stroke.Дата.Value.ToShortDateString();
            Shifr.Text = Stroke.Шифр;
            if (Stroke.X1.HasValue)
                X1.Value = (decimal)Stroke.X1.Value;
            if (Stroke.X2.HasValue)
                X2.Value = (decimal)Stroke.X2.Value;
            try
            {
                if (X1.Value != 0 && X2.Value != 0)
                {
                    XCP.Value = (X1.Value + X2.Value) / 2;
                    Shodimost.Value = (X1.Value - X2.Value) > 0 ? X1.Value - X2.Value : X2.Value - X1.Value;
                }
            }
            catch
            {
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Stroka = db.Probi.Where(c => c.Id == ProbaID).FirstOrDefault();
                    if (Stroka != null)
                    {
                        var NextID = db.Probi != null && db.Probi.Count() > 0 ? db.Probi.Max(c => c.Id) + 1 : 1;
                        if (X1.Value == 0 && X2.Value == 0)
                        {
                            Stroka.X1 = null;
                            Stroka.X2 = null;
                            Stroka.X = null;
                            Stroka.rSmall = null;
                            Stroka.RBig = null;
                            Stroka.KK = null;
                            Stroka.НормативКонтроляК = null;
                            Stroka.ЗаключениеК = null;
                            db.Probi.InsertOnSubmit(new Probi
                            {
                                Id = NextID,
                                KontrolStabilnostiID = Stroka.KontrolStabilnostiID,
                                KK = null,
                                OperativniiKontrolID = Stroka.OperativniiKontrolID,
                                RBig = null,
                                rSmall = null,
                                X = null,
                                X1 = null,
                                X2 = null,
                                АттестованноеЗначение = Stroka.АттестованноеЗначение,
                                Выдал = Stroka.Выдал,
                                Дата = Stroka.Дата,
                                Исполнитель = Stroka.Исполнитель,
                                КонтролируемыйОбъект = Stroka.КонтролируемыйОбъект,
                                ОпределяемыйКомпонент = Stroka.ОпределяемыйКомпонент,
                                Получил = Stroka.Получил,
                                Шифр = Stroka.Шифр,
                                VisibleStatus = 200,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                AnalizID = Stroka.AnalizID,
                                Owner = Stroka.Id,
                                KontrolPogreshnostiID = Stroka.KontrolPogreshnostiID,
                                ЗаключениеК = Stroka.ЗаключениеК,
                                НормативКонтроляК = Stroka.НормативКонтроляК
                            });
                        }
                        else
                        {
                            Stroka.X1 = (double)X1.Value;
                            Stroka.X2 = (double)X2.Value;
                            var N1 = VLKStatic.GetRoundNumber(Stroka.АттестованноеЗначение.Value);
                            var N2 = VLKStatic.GetRoundNumber(Stroka.X1.Value);
                            var N3 = VLKStatic.GetRoundNumber(Stroka.X2.Value);
                            var cont = N1 >= N2 ? N1 >= N3 ? N1 : N3 : N2 >= N3 ? N2 : N3;
                            cont++;
                            Stroka.UserName = UserSettings.User;
                            Stroka.ДатаСозданияЗаписи = DateTime.Now;
                            Stroka.X = Math.Round((double)(X1.Value + X2.Value) / 2, cont, MidpointRounding.AwayFromZero);       //cont == 0 ? 0 : cont - 1);
                            var r = Math.Round((double)(X1.Value - X2.Value), cont, MidpointRounding.AwayFromZero);
                            Stroka.rSmall = r < 0 ? r * (-1) : r;
                            Stroka.KK = Math.Round(Stroka.X.Value - Stroka.АттестованноеЗначение.Value, cont, MidpointRounding.AwayFromZero);                            
                            db.SubmitChanges();
                            RaschetR(db, Stroka.KontrolStabilnostiID, cont);
                            db.Probi.InsertOnSubmit(new Probi
                            {
                                Id = NextID,
                                KontrolStabilnostiID = Stroka.KontrolStabilnostiID,
                                KK = Stroka.KK,
                                OperativniiKontrolID = Stroka.OperativniiKontrolID,
                                RBig = Stroka.RBig,
                                rSmall = Stroka.rSmall,
                                X = Stroka.X,
                                X1 = Stroka.X1,
                                X2 = Stroka.X2,
                                АттестованноеЗначение = Stroka.АттестованноеЗначение,
                                Выдал = Stroka.Выдал,
                                Дата = Stroka.Дата,
                                Исполнитель = Stroka.Исполнитель,
                                КонтролируемыйОбъект = Stroka.КонтролируемыйОбъект,
                                ОпределяемыйКомпонент = Stroka.ОпределяемыйКомпонент,
                                Получил = Stroka.Получил,
                                Шифр = Stroka.Шифр,
                                VisibleStatus = 200,
                                UserName = UserSettings.User,
                                ДатаСозданияЗаписи = DateTime.Now,
                                AnalizID = Stroka.AnalizID,
                                Owner = Stroka.Id,
                            });
                        }
                        db.SubmitChanges();
                        this.Close();
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RaschetR(DbDataContext db, int? KontrolStabilnostiID, int count)
        {
            var SelectedProbi = db.Probi.Where(c => c.KontrolStabilnostiID == KontrolStabilnostiID && (c.VisibleStatus == null || c.VisibleStatus < 2)).OrderBy(c => c.Дата).ToList();
            if (SelectedProbi != null && SelectedProbi.Count > 1)
            {                
                for (int i = 0; i < SelectedProbi.Count; i++)
                {
                    SelectedProbi[i].RBig = null;
                    if (SelectedProbi[i].X1.HasValue && SelectedProbi[i].X2.HasValue)
                    {
                        for(int k = i + 1; k < SelectedProbi.Count; k++)
                        {
                            if(SelectedProbi[k].X1.HasValue && SelectedProbi[k].X2.HasValue)
                            {
                                SelectedProbi[i].RBig = Math.Round(SelectedProbi[k].X.Value - SelectedProbi[i].X.Value, count, MidpointRounding.AwayFromZero);
                                if (SelectedProbi[i].RBig < 0) SelectedProbi[i].RBig = SelectedProbi[i].RBig * (-1);
                                break;
                            }
                        }                                       
                    } 
                    else
                    {
                        SelectedProbi[i].KK = null;
                        SelectedProbi[i].rSmall = null;
                        SelectedProbi[i].X = null;
                    }                                     
                }
                //SelectedProbi[SelectedProbi.Count - 1].RBig = null;  ///???
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddProbiDataForm_Load(object sender, EventArgs e)
        {
            if (X1.Text == "0,0000000")
            {
                X1.Text = "";
            }
            if (X2.Text == "0,0000000")
            {
                X2.Text = "";
            }
        }

        private void X1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void X1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (X1.Value != 0 && X2.Value != 0 && (XCP.Value == 0 || Shodimost.Value == 0))
                {
                    XCP.Value = (X1.Value + X2.Value) / 2;
                    Shodimost.Value = (X1.Value - X2.Value) > 0 ? X1.Value - X2.Value : X2.Value - X1.Value;
                }
            }
            catch
            {
            }
        }

        private void X2_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (X1.Value != 0 && X2.Value != 0 && (XCP.Value == 0 || Shodimost.Value == 0))
                {
                    XCP.Value = (X1.Value + X2.Value) / 2;
                    Shodimost.Value = (X1.Value - X2.Value) > 0 ? X1.Value - X2.Value : X2.Value - X1.Value;
                }
            }
            catch
            {
            }
        }

        private void XCP_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (XCP.Value != 0 && Shodimost.Value != 0)
                {
                    X1.Value = (XCP.Value + (Shodimost.Value / 2)) >= 0 ? (XCP.Value + (Shodimost.Value / 2)) : (XCP.Value + (Shodimost.Value / 2)) * (-1);
                    X2.Value = X1.Value - Shodimost.Value;
                }
            }
            catch
            {
            }
        }

        private void Shodimost_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (XCP.Value != 0 && Shodimost.Value != 0)
                {
                    X1.Value = (XCP.Value + (Shodimost.Value / 2)) >= 0 ? (XCP.Value + (Shodimost.Value / 2)) : (XCP.Value + (Shodimost.Value / 2)) * (-1);
                    X2.Value = X1.Value - Shodimost.Value;
                }
            }
            catch
            {
            }
        }

        private void AddProbiDataForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void CalculateX12_Click(object sender, EventArgs e)
        {
            try
            {
                if (XCP.Value != 0 && Shodimost.Value != 0)
                {
                    X1.Value = (XCP.Value + (Shodimost.Value / 2)) >= 0 ? (XCP.Value + (Shodimost.Value / 2)) : (XCP.Value + (Shodimost.Value / 2)) * (-1);
                    X2.Value = X1.Value - Shodimost.Value;
                }
            }
            catch
            {
            }
        }

        private void CalculateXcpShod_Click(object sender, EventArgs e)
        {
            try
            {
                if (X1.Value != 0 && X2.Value != 0)
                {
                    XCP.Value = (X1.Value + X2.Value) / 2;
                    Shodimost.Value = (X1.Value - X2.Value) > 0 ? X1.Value - X2.Value : X2.Value - X1.Value;
                }
            }
            catch
            {
            }
        }
    }
}
