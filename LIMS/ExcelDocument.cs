﻿using System;

namespace LIMS
{
    class ExcelDocument
    {        
        Object excelMissing = System.Reflection.Missing.Value;
        Object excelTrue = true;
        Object excelFalse = false;

        public Microsoft.Office.Interop.Excel._Application ExcelApplication;
        public Microsoft.Office.Interop.Excel._Workbook excelWorkbook;
        public Microsoft.Office.Interop.Excel._Worksheet excelWorksheet;
        public ExcelDocument(string templatePath = "")
        {
            if (templatePath != "")
            {
                createFromTemplate(templatePath);
            }
        }

        public void createFromTemplate(string templatePath)
        {
            //создаем обьект приложения  
            ExcelApplication = new Microsoft.Office.Interop.Excel.Application();
            ExcelApplication.Visible = false;
            //создаем обьект документа 
            //excelWorkbook = new Microsoft.Office.Interop.Excel.Workbook();
            //excelWorksheet = new Microsoft.Office.Interop.Excel.Worksheet();            
            //добавляем шаблон новым документом в наше приложение word 
            excelWorkbook = ExcelApplication.Workbooks.Add(templatePath);
            excelWorksheet = excelWorkbook.Worksheets[1];
        }

        public void SaveAs(string path)
        {
            object file = path;
            object missing = System.Reflection.Missing.Value;
            excelWorkbook.SaveAs(file);
        }

        public bool Visible
        {
            get { return ExcelApplication.Visible; }
            set { ExcelApplication.Visible = value; ExcelApplication.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMaximized; }
        }

        public void Close()
        {
            try
            {
                excelWorkbook.Close();
                ExcelApplication.Quit();
                excelWorkbook = null;
                excelWorksheet = null;
                ExcelApplication = null;
                GC.Collect();
                GC.Collect();
            }
            catch
            {
            }
        }       
    }
}

