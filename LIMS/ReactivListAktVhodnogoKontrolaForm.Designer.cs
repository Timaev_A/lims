﻿namespace LIMS
{
    partial class ReactivListAktVhodnogoKontrolaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReactivListAktVhodnogoKontrolaForm));
            this.ReactivAktGrid = new DevExpress.XtraGrid.GridControl();
            this.ReactivAktGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivAktGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivAktGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ReactivAktGrid
            // 
            this.ReactivAktGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReactivAktGrid.Location = new System.Drawing.Point(0, 0);
            this.ReactivAktGrid.MainView = this.ReactivAktGridView;
            this.ReactivAktGrid.Name = "ReactivAktGrid";
            this.ReactivAktGrid.Size = new System.Drawing.Size(775, 550);
            this.ReactivAktGrid.TabIndex = 1;
            this.ReactivAktGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ReactivAktGridView});
            this.ReactivAktGrid.Click += new System.EventHandler(this.ReactivAktGrid_Click);
            this.ReactivAktGrid.DoubleClick += new System.EventHandler(this.ReactivAktGrid_DoubleClick);
            // 
            // ReactivAktGridView
            // 
            this.ReactivAktGridView.Appearance.FixedLine.Options.UseTextOptions = true;
            this.ReactivAktGridView.Appearance.FixedLine.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivAktGridView.Appearance.FixedLine.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivAktGridView.Appearance.FixedLine.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivAktGridView.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.ReactivAktGridView.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivAktGridView.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivAktGridView.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivAktGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.ReactivAktGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivAktGridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivAktGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivAktGridView.Appearance.Row.Options.UseTextOptions = true;
            this.ReactivAktGridView.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivAktGridView.Appearance.Row.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivAktGridView.Appearance.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivAktGridView.Appearance.VertLine.Options.UseTextOptions = true;
            this.ReactivAktGridView.Appearance.VertLine.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivAktGridView.Appearance.VertLine.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivAktGridView.Appearance.VertLine.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivAktGridView.Appearance.ViewCaption.Options.UseTextOptions = true;
            this.ReactivAktGridView.Appearance.ViewCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivAktGridView.Appearance.ViewCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivAktGridView.Appearance.ViewCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivAktGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.ReactivAktGridView.GridControl = this.ReactivAktGrid;
            this.ReactivAktGridView.Name = "ReactivAktGridView";
            this.ReactivAktGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.ReactivAktGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.ReactivAktGridView.OptionsBehavior.AllowIncrementalSearch = true;
            this.ReactivAktGridView.OptionsBehavior.Editable = false;
            this.ReactivAktGridView.OptionsFind.AlwaysVisible = true;
            this.ReactivAktGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.ReactivAktGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.ReactivAktGridView.OptionsView.RowAutoHeight = true;
            this.ReactivAktGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.ReactivAktGridView_PopupMenuShowing);
            this.ReactivAktGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.ReactivAktGridView_FocusedRowChanged);
            // 
            // ReactivListAktVhodnogoKontrolaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 550);
            this.Controls.Add(this.ReactivAktGrid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "ReactivListAktVhodnogoKontrolaForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Акт входного контроля";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ReactivListAktVhodnogoKontrolaForm_FormClosed);
            this.Load += new System.EventHandler(this.ReactivListAktVhodnogoKontrolaForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivListAktVhodnogoKontrolaForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ReactivAktGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivAktGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraGrid.GridControl ReactivAktGrid;
        public DevExpress.XtraGrid.Views.Grid.GridView ReactivAktGridView;
    }
}