﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Linq;
using System.Configuration;

namespace LIMS
{
    public static class Crypt
    {
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);
            string key = GlobalStatic.SecurityKey;
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);
            string key = GlobalStatic.SecurityKey;

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
            {
                keyArray = Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Encoding.UTF8.GetString(resultArray);
        }
    }
}
//    private const int Keysize = 256;        
//    private const int DerivationIterations = 1000;
//    public static string Encrypt(string plainText, string passPhrase)
//    {            
//        var saltStringBytes = Generate256BitsOfRandomEntropy();
//        var ivStringBytes = Generate256BitsOfRandomEntropy();
//        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
//        using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
//        {
//            var keyBytes = password.GetBytes(Keysize / 8);
//            using (var symmetricKey = new RijndaelManaged())
//            {
//                symmetricKey.BlockSize = 256;
//                symmetricKey.Mode = CipherMode.CBC;
//                symmetricKey.Padding = PaddingMode.PKCS7;
//                using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
//                {
//                    using (var memoryStream = new MemoryStream())
//                    {
//                        using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
//                        {
//                            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
//                            cryptoStream.FlushFinalBlock();
//                            // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
//                            var cipherTextBytes = saltStringBytes;
//                            cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
//                            cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
//                            memoryStream.Close();
//                            cryptoStream.Close();
//                            return Convert.ToBase64String(cipherTextBytes);
//                        }
//                    }
//                }
//            }
//        }
//    }

//    public static string Decrypt(string cipherText, string passPhrase)
//    {            
//        var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);            
//        var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();            
//        var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();            
//        var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

//        using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
//        {
//            var keyBytes = password.GetBytes(Keysize / 8);
//            using (var symmetricKey = new RijndaelManaged())
//            {
//                symmetricKey.BlockSize = 256;
//                symmetricKey.Mode = CipherMode.CBC;
//                symmetricKey.Padding = PaddingMode.PKCS7;
//                using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
//                {
//                    using (var memoryStream = new MemoryStream(cipherTextBytes))
//                    {
//                        using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
//                        {
//                            var plainTextBytes = new byte[cipherTextBytes.Length];
//                            var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
//                            memoryStream.Close();
//                            cryptoStream.Close();
//                            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
//                        }
//                    }
//                }
//            }
//        }
//    }

//    private static byte[] Generate256BitsOfRandomEntropy()
//    {
//        byte[] bytes = new byte[LIMS.GlobalStatic.Salt.Length * sizeof(char)];
//        Buffer.BlockCopy(LIMS.GlobalStatic.Salt.ToCharArray(), 0, bytes, 0, bytes.Length);
//        return bytes;
//        //var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
//        //using (var rngCsp = new RNGCryptoServiceProvider())
//        //{                
//        //    rngCsp.GetBytes(randomBytes);
//        //}
//        //return randomBytes;
//    }
//}
//}