﻿using System;
using System.Windows.Forms;

namespace LIMS
{
    public partial class CheckVOColumnForm : DevExpress.XtraEditors.XtraForm
    {
        VOTab RT;
        public CheckVOColumnForm(MainForm MF, VOTab VOTab)
        {
            InitializeComponent();
            try
            {
                if (UserSettings.VOColumns != null)
                {
                    var CheckColumn = UserSettings.VOColumns.Split(',');
                    CheckEditN.Checked = CheckColumn[0] == "1";
                    CheckEditName.Checked = CheckColumn[1] == "1";
                    CheckEditTip.Checked = CheckColumn[2] == "1";
                    CheckEditZavN.Checked = CheckColumn[3] == "1";
                    CheckEditIzgotovitel.Checked = CheckColumn[4] == "1";
                    CheckEditGodVipuska.Checked = CheckColumn[5] == "1";
                    CheckEditGogVvodaVEkspluatachiu.Checked = CheckColumn[6] == "1";
                    CheckEditInventarniiN.Checked = CheckColumn[7] == "1";
                    CheckEditNaznachenie.Checked = CheckColumn[8] == "1";
                    CheckEditPravoSobstvennosti.Checked = CheckColumn[9] == "1";
                    CheckEditMestoUstanovkiHranenia.Checked = CheckColumn[10] == "1";
                    CheckEditDateProverkiTehnichSostoiania.Checked = CheckColumn[11] == "1";
                    CheckEditDateSledProverki.Checked = CheckColumn[12] == "1";
                    CheckEditDatePoslednegoTO.Checked = CheckColumn[13] == "1";
                    CheckEditSleduechegoTO.Checked = CheckColumn[14] == "1";
                    CheckEditPrimechanie.Checked = CheckColumn[15] == "1";
                }
            } catch { }
            RT = VOTab;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            string CheckString = "";
            if (CheckEditN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditName.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditTip.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditZavN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditIzgotovitel.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditGodVipuska.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditGogVvodaVEkspluatachiu.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditInventarniiN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditNaznachenie.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditPravoSobstvennosti.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditMestoUstanovkiHranenia.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDateProverkiTehnichSostoiania.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDateSledProverki.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDatePoslednegoTO.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditSleduechegoTO.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditPrimechanie.Checked == true) { CheckString = CheckString + "1"; } else { CheckString = CheckString + "0"; }
            MainForm main = this.Owner as MainForm;
            if (main != null)
            {
                UserSettings.VOColumns = CheckString;
                UserSettings.SaveSettings();
                RT.GetVO(main);
            }
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CheckEditN_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void CheckVOColumnForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
