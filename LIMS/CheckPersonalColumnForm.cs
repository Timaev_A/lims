﻿using System;
using System.Windows.Forms;

namespace LIMS
{
    public partial class CheckPersonalColumnForm : DevExpress.XtraEditors.XtraForm
    {
        PersonalTab RT;
        public CheckPersonalColumnForm(MainForm MF, PersonalTab PersonalTab)
        {
            InitializeComponent();
            if (UserSettings.PersonalColumns != null)
            {
                var CheckColumn = UserSettings.PersonalColumns.Split(',');
                CheckEditN.Checked = CheckColumn[0] == "1";
                CheckEditName.Checked = CheckColumn[1] == "1";
                CheckEditDolshnost.Checked = CheckColumn[2] == "1";
                CheckEditObrazovanie.Checked = CheckColumn[3] == "1";
                CheckEditObrazovaniePolnoe.Checked = CheckColumn[4] == "1";
                CheckEditOpit.Checked = CheckColumn[5] == "1";
                CheckEditPovishKvalif.Checked = CheckColumn[6] == "1";
                CheckEditSrokPodvershKvalif.Checked = CheckColumn[7] == "1";
                CheckEditGas.Checked = CheckColumn[8] == "1";
                CheckEditSrokGas.Checked = CheckColumn[9] == "1";
                CheckEditAttestachia.Checked = CheckColumn[10] == "1";
                CheckEditSrokAttestachii.Checked = CheckColumn[11] == "1";
                CheckEditProverkaOTiPB.Checked = CheckColumn[12] == "1";
                CheckEditSrokProverkiOTiPB.Checked = CheckColumn[13] == "1";
                CheckEditDopolnitelno.Checked = CheckColumn[14] == "1";
            }
            RT = PersonalTab;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            string CheckString = "";
            if (CheckEditN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditName.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDolshnost.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditObrazovanie.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditObrazovaniePolnoe.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditOpit.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditPovishKvalif.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditSrokPodvershKvalif.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditGas.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditSrokGas.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditAttestachia.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditSrokAttestachii.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditProverkaOTiPB.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditSrokProverkiOTiPB.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDopolnitelno.Checked == true) { CheckString = CheckString + "1"; } else { CheckString = CheckString + "0"; }
            MainForm main = this.Owner as MainForm;
            if (main != null)
            {
                UserSettings.PersonalColumns = CheckString;
                UserSettings.SaveSettings();
                RT.GetPersonal(main);
            }
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CheckEditDateGet_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void CheckEditN_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void CheckPersonalColumnForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
