﻿using DevExpress.XtraCharts;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class DiagrammaPrechezionnostForm : DevExpress.XtraEditors.XtraForm
    {
        public DiagrammaPrechezionnostForm(List<Probi> Probs)
        {
            InitializeComponent();
            try
            {
                int s = 0;
                foreach (var Pr in Probs)
                {
                    if (Pr.X1.HasValue && Pr.X2.HasValue)
                        break;
                    s++;
                }
                if ((s + 1 == Probs.Count) && !Probs[s].X1.HasValue && !Probs[s].X2.HasValue)
                    return;
                VLKStatic.CalculatePredel(Probs[s]);
                ((XYDiagram)ChartControl.Diagram).AxisX.VisualRange.MinValue = 1;
                ((XYDiagram)ChartControl.Diagram).AxisX.VisualRange.MaxValue = Probs.Count - 1;
                ((XYDiagram)ChartControl.Diagram).AxisX.VisualRange.Auto = true;
                //((XYDiagram)ChartControl.Diagram).AxisX.VisualRange.AutoSideMargins = true;
                ((XYDiagram)ChartControl.Diagram).AxisX.WholeRange.MinValue = 1;
                ((XYDiagram)ChartControl.Diagram).AxisX.WholeRange.MaxValue = Probs.Count - 1;
                ((XYDiagram)ChartControl.Diagram).AxisX.WholeRange.Auto = true;
                //((XYDiagram)ChartControl.Diagram).AxisX.WholeRange.AutoSideMargins = true;  

                ChartControl.Titles[0].Text = "Предел действия - " + VLKStatic.AddZero(VLKStatic.Prech1) + ";    Предел предупреждения - " + VLKStatic.AddZero(VLKStatic.Prech2) + ";    Средняя линия - " + VLKStatic.AddZero(VLKStatic.Prech3);
                int i = 0;
                double R = 0;
                int? ID = -1;
                foreach (Probi Stroke in Probs)
                {
                    if (i > 0)
                    {
                        ChartControl.Series[0].Points.Add(new SeriesPoint(i, R));
                        ChartControl.Series[1].Points.Add(new SeriesPoint(i, VLKStatic.Prech1));
                        ChartControl.Series[2].Points.Add(new SeriesPoint(i, VLKStatic.Prech2));
                        ChartControl.Series[3].Points.Add(new SeriesPoint(i, VLKStatic.Prech3));
                        ((XYDiagram)ChartControl.Diagram).AxisX.CustomLabels.Add(new CustomAxisLabel { AxisValue = i, Name = i.ToString() });
                    }                    
                    i++;
                    R = Stroke.RBig.HasValue ? Stroke.RBig.Value : 0;
                    ID = Stroke.AnalizID;
                }
                using (DbDataContext db = new DbDataContext())
                {
                    var Analiz = db.Analiz.Where(c => c.Id == ID).FirstOrDefault();
                    if (Analiz.НазваниеМетодаАнализа.Contains("содержания вод"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля внутрилабораторной прецизионности\rпри измерении содержания воды по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("механических примес"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля внутрилабораторной прецизионности\rпри измерении м.к. мех. примесей по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("хлористых сол"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля внутрилабораторной прецизионности\rпри измерении м.к. хлористых солей по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("плотност"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля внутрилабораторной прецизионности\rпри измерении плотности по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("насыщенных пар"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля внутрилабораторной прецизионности\rпри измерении давления насыщенных паров по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("спектрометр"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля внутрилабораторной прецизионности\rпри измерении массовой доли серы по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("сероводород"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля внутрилабораторной прецизионности\rпри определении массовой доли сероводорода по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("фракц"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля внутрилабораторной прецизионности\rпри определении фракционного состава по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("хлорорганическ"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля внутрилабораторной прецизионности\rпри измерении массовой доли хлорорганических соединений по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("парафин"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля внутрилабораторной прецизионности\rпри измерении массовой доли парафина по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    if (Analiz.НазваниеМетодаАнализа.Contains("вязкост"))
                        ChartControl.Titles[2].Text = "Контрольная карта Шухарта для контроля внутрилабораторной прецизионности\rпри измерении вязкости по " + Analiz.НД + "\r(в единицах измеряемых величин)";
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PrintButton_Click(object sender, EventArgs e)
        {
            PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
            pcl.Component = ChartControl;
            ChartControl.OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Zoom;
            pcl.RtfReportHeader = "Химико-аналитическая лаборатория \r";
            pcl.RtfReportFooter = "\rВыполнено с использованием АРМЛАБ";
            pcl.Landscape = true;
            pcl.ShowPreview();
        }

        private void ExcelButton_Click(object sender, EventArgs e)
        {
            try
            {
                PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
                pcl.Component = ChartControl;
                ChartControl.OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Zoom;
                pcl.RtfReportHeader = "Химико-аналитическая лаборатория \r";
                pcl.RtfReportFooter = "\rВыполнено с использованием АРМЛАБ";
                pcl.Landscape = true;
                System.IO.Directory.CreateDirectory(System.Windows.Forms.Application.StartupPath + @"\\Temp");
                var path = System.Windows.Forms.Application.StartupPath + @"\\Temp\\Контроль прецизионности " + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString() + ".xlsx";
                pcl.ExportToXlsx(path);
                FileInfo file = new FileInfo(path);
                if (file.Exists)
                {
                    System.Diagnostics.Process.Start(path);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DiagrammaPrechezionnostForm_Load(object sender, EventArgs e)
        {

        }

        private void DiagrammaPrechezionnostForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
