﻿namespace LIMS
{
    class GSOUpdateClass
    {
        public MainForm Main { set; get; }
        public int AnalizID { set; get; }
        public int ColumnTip { set; get; }
        public int OperControlID { set; get; }
    }
}
