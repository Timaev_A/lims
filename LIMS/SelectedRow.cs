﻿using System.Collections.Generic;

namespace LIMS
{
    public class SelectedRow
    {
        static List<RowIDClass> SelectedRowID = new List<RowIDClass>();
        class RowIDClass
        {
            internal string TabName { get; set; }
            internal string RowID { get; set; }
        }           
        
        public void AddNewRowID(string TabName, string ID)
        {            
            foreach(var stroke in SelectedRowID)
            {
                if(stroke.TabName == TabName)
                {
                    SelectedRowID.Remove(stroke);                    
                    break;
                }
            }
            SelectedRowID.Add(new RowIDClass { TabName = TabName, RowID = ID });
        }

        public string GetRowID(string TabName)
        {
            foreach (var stroke in SelectedRowID)
            {
                if (stroke.TabName == TabName)                
                    return stroke.RowID;
            }
            return "";
        }
    }    
}
