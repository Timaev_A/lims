﻿using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class ReactivUchetReactivovForm : DevExpress.XtraEditors.XtraForm
    {
        MainForm Main;
        int ReactivMainID;
        public DateTime? Ot = null;
        public DateTime? Do = null;
        public string VisibleColumns = "";
        string SelectedRowId = "";
        WordDocument WordDoc1;

        public ReactivUchetReactivovForm(MainForm MF)
        {
            InitializeComponent();
            Main = MF;
            ReactivMainID = -5;
        }

        public ReactivUchetReactivovForm(MainForm MF, int ID)
        {
            InitializeComponent();
            Main = MF;
            ReactivMainID = ID;
        }

        public void GetReactivUchet()
        {
            try
            {
                ColumnView View = ReactivUchetGridView;
                GridColumn column = View.Columns["Id"];
                using (DbDataContext db = new DbDataContext())
                {
                    List<ReactivUchet> ВсеЗначения;
                    if (!UserSettings.Developer)
                    {
                        if (ReactivMainID >= 0)
                        {
                            if (Ot == null) ВсеЗначения = db.ReactivUchet.Where(c => c.MainID == ReactivMainID && (c.VisibleStatus == 0 || c.VisibleStatus == null)).OrderByDescending(c => c.От).ToList();
                            else ВсеЗначения = db.ReactivUchet.Where(c => c.MainID == ReactivMainID && (c.VisibleStatus == 0 || c.VisibleStatus == null)).Where(c => c.От >= Ot.Value.AddDays(-1) && c.До <= Do.Value.AddDays(1)).OrderByDescending(c => c.От).ToList();
                        }
                        else
                        {
                            if (Ot == null) ВсеЗначения = db.ReactivUchet.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null).OrderByDescending(c => c.От).ToList();
                            else ВсеЗначения = db.ReactivUchet.Where(c => c.VisibleStatus == 0 || c.VisibleStatus == null).Where(c => c.От >= Ot.Value.AddDays(-1) && c.До <= Do.Value.AddDays(1)).OrderByDescending(c => c.От).ToList();
                        }
                    }
                    else ВсеЗначения = db.ReactivUchet.OrderByDescending(c => c.От).ToList();

                    ReactivUchetGridView.FocusedRowChanged -= ReactivUchetGridView_FocusedRowChanged;
                    ReactivUchetGrid.DataSource = ВсеЗначения;
                    if (!UserSettings.Developer)
                    {
                        ReactivUchetGridView.Columns[0].Visible = false;
                        ReactivUchetGridView.Columns[1].Visible = false;
                        ReactivUchetGridView.Columns[11].Visible = false;
                        ReactivUchetGridView.Columns[12].Visible = false;
                        ReactivUchetGridView.Columns[13].Visible = false;
                        ReactivUchetGridView.Columns[14].Visible = false;
                        ReactivUchetGridView.Columns[15].Visible = false;
                    }
                    if (VisibleColumns != "")
                    {
                        var CheckColumn = VisibleColumns.Split(',');
                        for (int i = 0; i < CheckColumn.Length; i++)
                        {
                            ReactivUchetGridView.Columns[i + 2].Visible = CheckColumn[i] == "1";
                        }
                    }

                    ReactivUchetGridView.FocusedRowChanged -= ReactivUchetGridView_FocusedRowChanged;
                    if (column != null)
                    {
                        int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                        if (rhFound != GridControl.InvalidRowHandle)
                        {
                            View.FocusedRowHandle = rhFound;
                            View.FocusedColumn = column;
                        }
                    }
                    ReactivUchetGridView.FocusedRowChanged += ReactivUchetGridView_FocusedRowChanged;

                    if (ВсеЗначения != null && ВсеЗначения.Count() > 0)
                    {
                        ReactivUchetGridView.Columns[2].Caption = "Наименование реактива";
                        ReactivUchetGridView.Columns[3].Caption = "Классификация";
                        ReactivUchetGridView.Columns[4].Caption = "Партия";
                        ReactivUchetGridView.Columns[5].Caption = "Дата изготовления";
                        ReactivUchetGridView.Columns[6].Caption = "От";
                        ReactivUchetGridView.Columns[7].Caption = "До";
                        ReactivUchetGridView.Columns[8].Caption = "Приход";
                        ReactivUchetGridView.Columns[9].Caption = "Расход";
                        ReactivUchetGridView.Columns[10].Caption = "Остаток";
                        ReactivUchetGridView.Columns[5].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        ReactivUchetGridView.Columns[5].DisplayFormat.FormatString = "dd/MM/yyyy";
                        ReactivUchetGridView.Columns[6].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        ReactivUchetGridView.Columns[6].DisplayFormat.FormatString = "dd/MM/yyyy";
                        ReactivUchetGridView.Columns[7].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        ReactivUchetGridView.Columns[7].DisplayFormat.FormatString = "dd/MM/yyyy";

                        //Wrap Header                        
                        ReactivUchetGridView.OptionsView.RowAutoHeight = true;
                        DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
                        MemoEdit.WordWrap = true;
                        ReactivUchetGridView.OptionsView.RowAutoHeight = true;
                        foreach (GridColumn My_GridColumn in ReactivUchetGridView.Columns)
                        {
                            My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                            My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            My_GridColumn.ColumnEdit = MemoEdit;
                        }
                    }
                    db.Dispose();
                }

                ReactivUchetGridView.FocusedRowChanged -= ReactivUchetGridView_FocusedRowChanged;
                if (column != null)
                {
                    int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                    if (rhFound != GridControl.InvalidRowHandle)
                    {
                        View.FocusedRowHandle = rhFound;
                        View.FocusedColumn = column;
                    }
                }
                ReactivUchetGridView.FocusedRowChanged += ReactivUchetGridView_FocusedRowChanged;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ReactivAktGrid_DoubleClick(object sender, EventArgs e)
        {

        }

        private void ReactivListAktVhodnogoKontrolaForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Main.Visible = true;
            Close();
        }

        private void ReactivListAktVhodnogoKontrolaForm_Load(object sender, EventArgs e)
        {
            GetReactivUchet();
            WindowState = FormWindowState.Maximized;
        }

        private void ReactivAktGridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            try
            {
                GridView view = sender as GridView;
                DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivUchetGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                var Stroke = (ReactivUchet)gv.GetRow(gv.FocusedRowHandle);
                if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
                {
                    int rowHandle = e.HitInfo.RowHandle;
                    e.Menu.Items.Clear();
                    if (UserSettings.ReactivPrint)
                    {
                        DXMenuCheckItem checkItem5 = new DXMenuCheckItem("&Печать",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivUchetPrintButton_Click));
                        checkItem5.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem5);
                    }
                    DXMenuCheckItem checkItem10 = new DXMenuCheckItem("&Фильтр...",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivUchetFiltr));
                    checkItem10.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem10);
                    if (UserSettings.ReactivDelete)
                    {
                        DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivUchetDeleteButton_Click));
                        checkItem3.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem3);
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void ReactivUchetFiltr(object sender, EventArgs e)
        {
            try
            {
                GlobalStatic.UchetFormDoValue = Do;
                GlobalStatic.UchetFormOtValue = Ot;
                ReactivUchetFiltrForm f = new ReactivUchetFiltrForm(Main, this);
                f.Owner = Main;
                f.ShowDialog();
                GetReactivUchet();
                f.Dispose();
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ReactivUchetPrintButton_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                Loader.Visible = true;
                worker.DoWork += Print;
                worker.RunWorkerAsync();
            }
            catch { }
        }

        private void Print(object sender, DoWorkEventArgs e)
        {
            try
            {
                WordDoc1 = new WordDocument(System.Windows.Forms.Application.StartupPath + @"\\Templates\\Журнал учета хим. реактивов.dotx");
                var RowCount = ReactivUchetGridView.RowCount;
                if (RowCount > 1) WordDoc1.AddTableCell(2, RowCount - 1);
                int Cell = 1;
                for (int rowHandle = 0; rowHandle < RowCount; rowHandle++)
                {
                    Cell++;
                    if (ReactivUchetGridView.IsDataRow(rowHandle) || Cell >= 39)
                    {
                        try
                        {
                            var SelectedRow = (ReactivUchet)ReactivUchetGridView.GetRow(rowHandle);
                            WordDoc1.AddTableValue(2, Cell, 1, SelectedRow.НаименованиеРеактива);
                            WordDoc1.AddTableValue(2, Cell, 2, SelectedRow.Квалификация);
                            WordDoc1.AddTableValue(2, Cell, 3, SelectedRow.Партия);
                            WordDoc1.AddTableValue(2, Cell, 4, SelectedRow.ДатаИзготовления.HasValue ? SelectedRow.ДатаИзготовления.Value.ToShortDateString() : "");
                            WordDoc1.AddTableValue(2, Cell, 5, SelectedRow.От.HasValue ? SelectedRow.От.Value.ToShortDateString() : "");
                            WordDoc1.AddTableValue(2, Cell, 6, SelectedRow.До.HasValue ? SelectedRow.До.Value.ToShortDateString() : "");
                            WordDoc1.AddTableValue(2, Cell, 7, SelectedRow.Приход.HasValue ? SelectedRow.Приход.Value.ToString() : "");
                            WordDoc1.AddTableValue(2, Cell, 8, SelectedRow.Расход.HasValue ? SelectedRow.Расход.Value.ToString() : "");
                            WordDoc1.AddTableValue(2, Cell, 9, SelectedRow.Остаток.HasValue ? SelectedRow.Остаток.Value.ToString() : "");
                        } catch { }
                    }
                }
                var Visibiliti = VisibleColumns.Split(',');
                for (int i = Visibiliti.Length - 1; i > 0; i--)
                {
                    if (Visibiliti[i] != "1")
                        WordDoc1.DeleteTableColumn(2, i + 1);
                }
                WordDoc1.TableResize(2);
                WordDoc1.Visible = true;
                WordDoc1.wordApplication.Activate();
            }
            catch (Exception ex)
            {
                try
                {
                    LogErrors Log = new LogErrors(ex.ToString());
                    Invoke(new System.Action(() => MessageBox.Show("Ошибка при открытие документа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    WordDoc1.Close();
                }
                catch { }

            }
            finally
            {
                try { Invoke(new System.Action(() => Loader.Visible = false)); }
                catch { }
            }
        }

        private void ReactivUchetDeleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    //Удаление
                    DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivUchetGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    var Stroke = (ReactivUchet)gv.GetRow(gv.FocusedRowHandle);
                    if (Stroke != null)
                    {
                        var OT = Stroke.От.HasValue ? Stroke.От.Value.ToShortDateString() : "-";
                        var DO = Stroke.До.HasValue ? Stroke.До.Value.ToShortDateString() : "-";
                        var DateIzgot = Stroke.ДатаИзготовления.HasValue ? Stroke.ДатаИзготовления.Value.ToShortDateString() : "-";
                        DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить событие?" + "\rНаименование реактива: " + Stroke.НаименованиеРеактива + "\rКлассификация: " + Stroke.Квалификация + "\rПартия: " + Stroke.Партия + "\rДата изготовления: " + DateIzgot + "\rОт: " + OT + "\rДо: " + DO + "\rПриход: " + Stroke.Приход.Value.ToString() + "\rРасход: " + Stroke.Расход.Value.ToString() + "\rОстаток: " + Stroke.Остаток.Value.ToString(), "Удалить?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            var Del = db.ReactivUchet.Where(c => c.Id == Stroke.Id).FirstOrDefault();
                            Del.VisibleStatus = Del.Owner == null ? 300 : 301;
                            Del.ДатаСозданияЗаписи = DateTime.Now;
                            Del.UserName = UserSettings.User;                                                       
                            db.SubmitChanges();
                            db.Dispose();
                            if (gv.FocusedRowHandle > 0)
                            {
                                var Stroka = (ReactivUchet)gv.GetRow(gv.FocusedRowHandle - 1);
                                if (Stroka != null) SelectedRowId = Stroka.Id.ToString();
                            }
                            GetReactivUchet();
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не выбрана строка для удаления!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        class RowInfo
        {
            public RowInfo(GridView view, int rowHandle)
            {
                this.RowHandle = rowHandle;
                this.View = view;
            }
            public GridView View;
            public int RowHandle;
        }

        private void ReactivUchetGridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivUchetGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (ReactivUchet)gv.GetRow(gv.FocusedRowHandle);
            if (Stroke != null)
                SelectedRowId = Stroke.Id.ToString();
        }

        private void ReactivUchetReactivovForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
