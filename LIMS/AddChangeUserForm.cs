﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class AddChangeUserForm : DevExpress.XtraEditors.XtraForm
    {
        public AddChangeUserForm()
        {
            InitializeComponent();
            UserName.DataSource = GlobalStatic.GetPersonal();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ReactivListAktVhodnogoKontrolaForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Close();
        }

        private void ReactivListAktVhodnogoKontrolaForm_Load(object sender, EventArgs e)
        {
            TipComboBox.SelectedIndex = 0;
            var screen = Screen.AllScreens;
            Width = screen[0].WorkingArea.Width;
            Location = new System.Drawing.Point(0, Location.Y);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (UserName.Text != "" && Password.Text != "")
                {
                    if (Password.Text.Length < 4)
                    {
                        MessageBox.Show("Пароль слишком короткий!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    using (DbDataContext db = new DbDataContext())
                    {
                        var SelectedUser = db.Users.Where(c => c.UserName == UserName.Text && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1 || c.VisibleStatus == 2)).FirstOrDefault();
                        var VisibleTabs = "";
                        VisibleTabs = ReactivCheckBox.Checked ? VisibleTabs + "1," : VisibleTabs + "0,";
                        VisibleTabs = PersonalCheckBox.Checked ? VisibleTabs + "1," : VisibleTabs + "0,";
                        VisibleTabs = NTDCheckBox.Checked ? VisibleTabs + "1," : VisibleTabs + "0,";
                        VisibleTabs = SICheckBox.Checked ? VisibleTabs + "1," : VisibleTabs + "0,";
                        VisibleTabs = IOCheckBox.Checked ? VisibleTabs + "1," : VisibleTabs + "0,";
                        VisibleTabs = VOCheckBox.Checked ? VisibleTabs + "1," : VisibleTabs + "0,";
                        VisibleTabs = VLKCheckBox.Checked ? VisibleTabs + "1," : VisibleTabs + "0,";
                        VisibleTabs = VisibleTabs + "1";
                        if (TipComboBox.SelectedIndex == 0) //добавить
                        {
                            if (SelectedUser != null)
                            {
                                MessageBox.Show("Пользователь с именем\"" + UserName.Text + "\" существует!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                db.Dispose();
                                return;
                            }
                            var NextID = db.Users != null && db.Users.Count() > 0 ? db.Users.Max(c => c.Id) + 1 : 1;
                            db.Users.InsertOnSubmit(new Users
                            {
                                Id = NextID,
                                UserName = UserName.Text,
                                Password = Crypt.Encrypt(Password.Text, true),
                                CreateDate = DateTime.Now,
                                VisibleStatus = DisabledCheckBox.Checked ? 2 : 0,
                                CreateUserName = UserSettings.User,
                                UserRol = null,
                                UseDocProtect = UseDocProtectCheckBox.Checked
                            });
                            ChangeSettings(UserName.Text, VisibleTabs);
                            db.SubmitChanges();
                            MessageBox.Show("Пользователь \"" + UserName.Text + "\" сохранен!", "Сохранение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else //изменить
                        {
                            SelectedUser.Password = Crypt.Encrypt(Password.Text, true);
                            SelectedUser.UseDocProtect = UseDocProtectCheckBox.Checked;
                            SelectedUser.VisibleStatus = DisabledCheckBox.Checked ? 2 : 0;
                            SelectedUser.CreateUserName = UserSettings.User;
                            ChangeSettings(UserName.Text, VisibleTabs);
                            db.SubmitChanges();
                            MessageBox.Show("Настройки пользователя \"" + UserName.Text + "\" сохранены!", "Сохранение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        if (UserName.Text == UserSettings.User) UserSettings.GetSettings();
                        db.Dispose();
                    }
                }
                else
                {
                    MessageBox.Show("Поля \"Пользователь\" и \"Пароль\" не должны быть пустыми!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ChangeSettings(string UserName2, string VisibleTabs)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var SelectedUserSettings = db.NewSettings.Where(c => c.UserName == UserName2).FirstOrDefault();
                    if (SelectedUserSettings == null)
                    {
                        db.NewSettings.InsertOnSubmit(new NewSettings
                        {
                            Admin = AdminCheckBox.Checked,
                            UserName = UserName2,
                            VisibleTabs = VisibleTabs,
                            ReactivAdd = ReactivAddCheckBox.Checked,
                            ReactivEdit = ReactivEditCheckBox.Checked,
                            ReactivPrint = ReactivPrintCheckBox.Checked,
                            ReactivRashod = ReactivRashodPrihodCheckBox.Checked,
                            ReactivSpisat = ReactivSpisatCheckBox.Checked,
                            ReactivUschet = ReactivUchetCheckBox.Checked,
                            ReactivVhodnoiKontrol = ReactivVhodnoiKontrolCheckBox.Checked,
                            ReactivProverkaPrigodnosti = ReactivProverkaPrigodnostiCheckBox.Checked,
                            ReactivDelete = ReactivDeleteCheckBox.Checked,
                            PersonalAdd = PersonalAddCheckBox.Checked,
                            PersonalEdit = PersonalEditCheckBox.Checked,
                            PersonalArhiv = PersonalUvolnenieCheckBox.Checked,
                            PersonalPrint = PersonalPrintCheckBox.Checked,
                            PersonalDelete = PersonalDeleteCheckBox.Checked,
                            NTDAdd = NTDAddCheckBox.Checked,
                            NTDEdit = NTDEditCheckBox.Checked,
                            NTDPrint = NTDPrintCheckBox.Checked,
                            NTDArhiv = NTDArhivCheckBox.Checked,
                            NTDDelete = NTDDelCheckBox.Checked,
                            SIAdd = SIAddCheckBox.Checked,
                            SIEdit = SIEditCheckBox.Checked,
                            SIPrint = SIPrintCheckBox.Checked,
                            SIArhiv = SIArhivCheckBox.Checked,
                            SIDelete = SIDeleteCheckBox.Checked,
                            IOAdd = IOAddCheckBox.Checked,
                            IOEdit = IOEditCheckBox.Checked,
                            IOPrint = IOPrintCheckBox.Checked,
                            IOArhiv = IOArhivCheckBox.Checked,
                            IODelete = IODeleteCheckBox.Checked,
                            VOAdd = VOAddCheckBox.Checked,
                            VOEdit = VOEditCheckBox.Checked,
                            VOPrint = VOPrintCheckBox.Checked,
                            VOArhiv = VOArhivCheckBox.Checked,
                            VODelete = VODeleteCheckBox.Checked,
                            Developer = DeveloperCheckBox.Checked,
                            Alerts = AlertsCheckBox.Checked,
                            VLKAnaliz = AnalizCheckBox.Checked,
                            VLKAnalizAdd = AnalizAddCheckBox.Checked,
                            VLKAnalizEdit = AnalizEditCheckBox.Checked,
                            VLKAnalizDelete = AnalizDeleteCheckBox.Checked,
                            VLKProbi = ProbiCheckBox.Checked,
                            VLKProbiAdd = ProbiAddCheckBox.Checked,
                            VLKProbiEdit = ProbiEditCheckBox.Checked,
                            VLKProbiChangeData = ProbiChangeDataCheckBox.Checked,
                            VLKProbiPrint = ProbiPrintCheckBox.Checked,
                            VLKProbiDelete = ProbiDeleteCheckBox.Checked,
                            VLKOperaivnii = OperativniiCheckBox.Checked,
                            VLKOperaivniiEdit = OperativniiEditCheckBox.Checked,
                            VLKOperaivniiClose = OperativniiCloseCheckBox.Checked,
                            VLKOperaivniiDelete = OperativniiDeleteCheckBox.Checked,
                            VLKOperaivniiPrint = OperativniiPrintCheckBox.Checked,
                            VLKStabilnosti = StabilnostiCheckBox.Checked,
                            VLKStabilnostiEdit = StabilnostiEditCheckBox.Checked,
                            VLKStabilnostiClose = StabilnostiCloseCheckBox.Checked,
                            VLKStabilnostiDelete = StabilnostiDeleteCheckBox.Checked,
                            VLKStabilnostiProtokolOchenka = StabilnostiKKSHCheckBox.Checked,
                            VLKStabilnostiDiagrams = StabilnostiDiagramsCheckBox.Checked,
                            VLKStabilnostiPrint = StabilnostiPrintCheckBox.Checked,                            
                        });
                    }
                    else
                    {
                        SelectedUserSettings.Admin = AdminCheckBox.Checked;
                        SelectedUserSettings.UserName = UserName2;
                        SelectedUserSettings.VisibleTabs = VisibleTabs;
                        SelectedUserSettings.ReactivAdd = ReactivAddCheckBox.Checked;
                        SelectedUserSettings.ReactivEdit = ReactivEditCheckBox.Checked;
                        SelectedUserSettings.ReactivPrint = ReactivPrintCheckBox.Checked;
                        SelectedUserSettings.ReactivRashod = ReactivRashodPrihodCheckBox.Checked;
                        SelectedUserSettings.ReactivSpisat = ReactivSpisatCheckBox.Checked;
                        SelectedUserSettings.ReactivUschet = ReactivUchetCheckBox.Checked;
                        SelectedUserSettings.ReactivVhodnoiKontrol = ReactivVhodnoiKontrolCheckBox.Checked;
                        SelectedUserSettings.ReactivProverkaPrigodnosti = ReactivProverkaPrigodnostiCheckBox.Checked;
                        SelectedUserSettings.ReactivDelete = ReactivDeleteCheckBox.Checked;
                        SelectedUserSettings.PersonalAdd = PersonalAddCheckBox.Checked;
                        SelectedUserSettings.PersonalEdit = PersonalEditCheckBox.Checked;
                        SelectedUserSettings.PersonalArhiv = PersonalUvolnenieCheckBox.Checked;
                        SelectedUserSettings.PersonalPrint = PersonalPrintCheckBox.Checked;
                        SelectedUserSettings.PersonalDelete = PersonalDeleteCheckBox.Checked;
                        SelectedUserSettings.NTDAdd = NTDAddCheckBox.Checked;
                        SelectedUserSettings.NTDEdit = NTDEditCheckBox.Checked;
                        SelectedUserSettings.NTDPrint = NTDPrintCheckBox.Checked;
                        SelectedUserSettings.NTDArhiv = NTDArhivCheckBox.Checked;
                        SelectedUserSettings.NTDDelete = NTDDelCheckBox.Checked;
                        SelectedUserSettings.SIAdd = SIAddCheckBox.Checked;
                        SelectedUserSettings.SIEdit = SIEditCheckBox.Checked;
                        SelectedUserSettings.SIPrint = SIPrintCheckBox.Checked;
                        SelectedUserSettings.SIArhiv = SIArhivCheckBox.Checked;
                        SelectedUserSettings.SIDelete = SIDeleteCheckBox.Checked;
                        SelectedUserSettings.IOAdd = IOAddCheckBox.Checked;
                        SelectedUserSettings.IOEdit = IOEditCheckBox.Checked;
                        SelectedUserSettings.IOPrint = IOPrintCheckBox.Checked;
                        SelectedUserSettings.IOArhiv = IOArhivCheckBox.Checked;
                        SelectedUserSettings.IODelete = IODeleteCheckBox.Checked;
                        SelectedUserSettings.VOAdd = VOAddCheckBox.Checked;
                        SelectedUserSettings.VOEdit = VOEditCheckBox.Checked;
                        SelectedUserSettings.VOPrint = VOPrintCheckBox.Checked;
                        SelectedUserSettings.VOArhiv = VOArhivCheckBox.Checked;
                        SelectedUserSettings.VODelete = VODeleteCheckBox.Checked;
                        SelectedUserSettings.Developer = DeveloperCheckBox.Checked;
                        SelectedUserSettings.Alerts = AlertsCheckBox.Checked;
                        SelectedUserSettings.VLKAnaliz = AnalizCheckBox.Checked;
                        SelectedUserSettings.VLKAnalizAdd = AnalizAddCheckBox.Checked;
                        SelectedUserSettings.VLKAnalizEdit = AnalizEditCheckBox.Checked;
                        SelectedUserSettings.VLKAnalizDelete = AnalizDeleteCheckBox.Checked;
                        SelectedUserSettings.VLKProbi = ProbiCheckBox.Checked;
                        SelectedUserSettings.VLKProbiAdd = ProbiAddCheckBox.Checked;
                        SelectedUserSettings.VLKProbiEdit = ProbiEditCheckBox.Checked;
                        SelectedUserSettings.VLKProbiChangeData = ProbiChangeDataCheckBox.Checked;
                        SelectedUserSettings.VLKProbiPrint = ProbiPrintCheckBox.Checked;
                        SelectedUserSettings.VLKProbiDelete = ProbiDeleteCheckBox.Checked;
                        SelectedUserSettings.VLKOperaivnii = OperativniiCheckBox.Checked;
                        SelectedUserSettings.VLKOperaivniiEdit = OperativniiEditCheckBox.Checked;
                        SelectedUserSettings.VLKOperaivniiClose = OperativniiCloseCheckBox.Checked;
                        SelectedUserSettings.VLKOperaivniiDelete = OperativniiDeleteCheckBox.Checked;
                        SelectedUserSettings.VLKOperaivniiPrint = OperativniiPrintCheckBox.Checked;
                        SelectedUserSettings.VLKStabilnosti = StabilnostiCheckBox.Checked;
                        SelectedUserSettings.VLKStabilnostiEdit = StabilnostiEditCheckBox.Checked;
                        SelectedUserSettings.VLKStabilnostiClose = StabilnostiCloseCheckBox.Checked;
                        SelectedUserSettings.VLKStabilnostiDelete = StabilnostiDeleteCheckBox.Checked;
                        SelectedUserSettings.VLKStabilnostiProtokolOchenka = StabilnostiKKSHCheckBox.Checked;
                        SelectedUserSettings.VLKStabilnostiDiagrams = StabilnostiDiagramsCheckBox.Checked;
                        SelectedUserSettings.VLKStabilnostiPrint = StabilnostiPrintCheckBox.Checked;   
                    }
                    db.SubmitChanges();
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void TipComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangedTipComboBox();
        }

        private void ChangedTipComboBox()
        {
            if (TipComboBox.SelectedIndex == 0)
            {
                UserName.DataSource = GlobalStatic.GetPersonal();
                UserName.DropDownStyle = ComboBoxStyle.DropDown;
                DeleteButton.Visible = false;
                ClearCheckS();
            }
            else
            {
                List<string> UserNames = new List<string>();
                try
                {
                    DeleteButton.Visible = true;
                    UserNames.Add("");
                    using (DbDataContext db = new DbDataContext())
                    {
                        var ВсеЗначения = db.Users.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1 || c.VisibleStatus == 2).OrderBy(c => c.UserName).Select(c => c.UserName).ToList();
                        if (ВсеЗначения != null && ВсеЗначения.Count() > 0)
                        {
                            foreach (string FullName in ВсеЗначения)
                            {
                                if (FullName == "Developer") continue;
                                UserNames.Add(FullName);
                            }
                            db.Dispose();
                            UserName.Text = "";
                            Password.Text = "";
                            AdminCheckBox.Checked = false;
                            DisabledCheckBox.Checked = false;
                            DeveloperCheckBox.Checked = false;
                            UseDocProtectCheckBox.Checked = true;
                            UserName.DropDownStyle = ComboBoxStyle.DropDownList;
                            UserName.DataSource = UserNames;
                            return;
                        }
                    }
                    UserName.DataSource = null;
                }
                catch (Exception ex)
                {
                    LogErrors Log = new LogErrors(ex.ToString());
                    MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    UserName.DataSource = null;
                }
            }
        }

        private void ClearCheckS()
        {
            UserName.Text = "";
            Password.Text = "";
            AdminCheckBox.Checked = false;
            DisabledCheckBox.Checked = false;
            DeveloperCheckBox.Checked = false;
            foreach (Control CBox in groupControl1.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    CB.Checked = true;
                }
            }
            foreach (Control CBox in groupControl2.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    CB.Checked = true;
                }
            }
            foreach (Control CBox in groupControl3.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    CB.Checked = true;
                }
            }
            foreach (Control CBox in groupControl4.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    CB.Checked = true;
                }
            }
            foreach (Control CBox in groupControl5.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    CB.Checked = true;
                }
            }
            foreach (Control CBox in groupControl6.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    CB.Checked = true;
                }
            }
            foreach (Control CBox in groupControl7.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    CB.Checked = true;
                }
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (UserName.Text != "")
                {
                    using (DbDataContext db = new DbDataContext())
                    {
                        var Use = db.Users.Where(c => c.UserName == UserName.Text && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1 || c.VisibleStatus == 2)).FirstOrDefault();
                        if (Use != null)
                        {
                            db.Users.DeleteOnSubmit(Use);
                            //Use.VisibleStatus = 300;
                            var Setting = db.NewSettings.Where(c => c.UserName == Use.UserName).FirstOrDefault();
                            if (Setting != null)
                            {
                                db.NewSettings.DeleteOnSubmit(Setting);
                            }
                            db.SubmitChanges();
                            MessageBox.Show("Пользователь \"" + UserName.Text + "\" удален!", "Удаление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Пользователь \"" + UserName.Text + "\" не существует!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        db.Dispose();
                    }
                    ChangedTipComboBox();
                }
                else
                {
                    MessageBox.Show("Поле \"Пользователь\" пустой!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ReactivCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control CBox in groupControl1.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "ReactivCheckBox") continue;
                    CB.Enabled = ReactivCheckBox.Checked;
                }
            }
        }

        private void PersonalCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control CBox in groupControl2.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "PersonalCheckBox") continue;
                    CB.Enabled = PersonalCheckBox.Checked;
                }
            }
        }

        private void NTDCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control CBox in groupControl3.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "NTDCheckBox") continue;
                    CB.Enabled = NTDCheckBox.Checked;
                }
            }
        }

        private void SICheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control CBox in groupControl4.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "SICheckBox") continue;
                    CB.Enabled = SICheckBox.Checked;
                }
            }
        }

        private void IOCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control CBox in groupControl5.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "IOCheckBox") continue;
                    CB.Enabled = IOCheckBox.Checked;
                }
            }
        }

        private void VOCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control CBox in groupControl6.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "VOCheckBox") continue;
                    CB.Enabled = VOCheckBox.Checked;
                }
            }
        }

        private void VLKCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control CBox in groupControl17.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;  
                    if(CB.Name == "AnalizCheckBox")
                    {
                        CB.Enabled = VLKCheckBox.Checked;
                        continue;
                    }                
                    CB.Enabled = VLKCheckBox.Checked ? AnalizCheckBox.Checked : VLKCheckBox.Checked;
                }
            }
            foreach (Control CBox in groupControl16.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "ProbiCheckBox")
                    {
                        CB.Enabled = VLKCheckBox.Checked;
                        continue;
                    }
                    CB.Enabled = VLKCheckBox.Checked ? ProbiCheckBox.Checked : VLKCheckBox.Checked;
                }
            }
            foreach (Control CBox in groupControl15.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "OperativniiCheckBox")
                    {
                        CB.Enabled = VLKCheckBox.Checked;
                        continue;
                    }
                    CB.Enabled = VLKCheckBox.Checked ? OperativniiCheckBox.Checked : VLKCheckBox.Checked;
                }
            }
            foreach (Control CBox in groupControl14.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "StabilnostiCheckBox")
                    {
                        CB.Enabled = VLKCheckBox.Checked;
                        continue;
                    }
                    CB.Enabled = VLKCheckBox.Checked ? StabilnostiCheckBox.Checked : VLKCheckBox.Checked;
                }
            }
        }

        private void UserName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TipComboBox.SelectedIndex == 1)
            {
                if (UserName.Text != "")
                {
                    using (DbDataContext db = new DbDataContext())
                    {
                        var Use = db.Users.Where(c => c.UserName == UserName.Text && (c.VisibleStatus == null || c.VisibleStatus == 0 || c.VisibleStatus == 1 || c.VisibleStatus == 2)).FirstOrDefault();
                        if (Use != null)
                        {
                            UseDocProtectCheckBox.Checked = Use.UseDocProtect;
                            Password.Text = Crypt.Decrypt(Use.Password, true);
                            DisabledCheckBox.Checked = Use.VisibleStatus == 2;
                            var UseSetting = db.NewSettings.Where(c => c.UserName == Use.UserName).FirstOrDefault();
                            if (UseSetting != null)
                            {
                                AdminCheckBox.Checked = UseSetting.Admin;
                                DeveloperCheckBox.Checked = UseSetting.Developer;
                                if (UseSetting.VisibleTabs != "")
                                {
                                    var Visibility = UseSetting.VisibleTabs.Split(',');
                                    ReactivCheckBox.Checked = Visibility[0] == "1";
                                    PersonalCheckBox.Checked = Visibility[1] == "1";
                                    NTDCheckBox.Checked = Visibility[2] == "1";
                                    SICheckBox.Checked = Visibility[3] == "1";
                                    IOCheckBox.Checked = Visibility[4] == "1";
                                    VOCheckBox.Checked = Visibility[5] == "1";
                                    VLKCheckBox.Checked = Visibility[6] == "1";
                                }
                                ReactivAddCheckBox.Checked = UseSetting.ReactivAdd;
                                ReactivEditCheckBox.Checked = UseSetting.ReactivEdit;
                                ReactivPrintCheckBox.Checked = UseSetting.ReactivPrint;
                                ReactivRashodPrihodCheckBox.Checked = UseSetting.ReactivRashod;
                                ReactivSpisatCheckBox.Checked = UseSetting.ReactivSpisat;
                                ReactivUchetCheckBox.Checked = UseSetting.ReactivUschet;
                                ReactivVhodnoiKontrolCheckBox.Checked = UseSetting.ReactivVhodnoiKontrol;
                                ReactivProverkaPrigodnostiCheckBox.Checked = UseSetting.ReactivProverkaPrigodnosti;
                                ReactivDeleteCheckBox.Checked = UseSetting.ReactivDelete;
                                PersonalAddCheckBox.Checked = UseSetting.PersonalAdd;
                                PersonalEditCheckBox.Checked = UseSetting.PersonalEdit;
                                PersonalUvolnenieCheckBox.Checked = UseSetting.PersonalArhiv;
                                PersonalPrintCheckBox.Checked = UseSetting.PersonalPrint;
                                PersonalDeleteCheckBox.Checked = UseSetting.PersonalDelete;
                                NTDAddCheckBox.Checked = UseSetting.NTDAdd;
                                NTDEditCheckBox.Checked = UseSetting.NTDEdit;
                                NTDPrintCheckBox.Checked = UseSetting.NTDPrint;
                                NTDArhivCheckBox.Checked = UseSetting.NTDArhiv;
                                NTDDelCheckBox.Checked = UseSetting.NTDDelete;
                                SIAddCheckBox.Checked = UseSetting.SIAdd;
                                SIEditCheckBox.Checked = UseSetting.SIEdit;
                                SIPrintCheckBox.Checked = UseSetting.SIPrint;
                                SIArhivCheckBox.Checked = UseSetting.SIArhiv;
                                SIDeleteCheckBox.Checked = UseSetting.SIDelete;
                                IOAddCheckBox.Checked = UseSetting.IOAdd;
                                IOEditCheckBox.Checked = UseSetting.IOEdit;
                                IOPrintCheckBox.Checked = UseSetting.IOPrint;
                                IOArhivCheckBox.Checked = UseSetting.IOArhiv;
                                IODeleteCheckBox.Checked = UseSetting.IODelete;
                                VOAddCheckBox.Checked = UseSetting.VOAdd;
                                VOEditCheckBox.Checked = UseSetting.VOEdit;
                                VOPrintCheckBox.Checked = UseSetting.VOPrint;
                                VOArhivCheckBox.Checked = UseSetting.VOArhiv;
                                VODeleteCheckBox.Checked = UseSetting.VODelete;
                                AlertsCheckBox.Checked = UseSetting.Alerts;
                                AnalizCheckBox.Checked = UseSetting.VLKAnaliz;
                                AnalizAddCheckBox.Checked = UseSetting.VLKAnalizAdd;
                                AnalizEditCheckBox.Checked = UseSetting.VLKAnalizEdit;
                                AnalizDeleteCheckBox.Checked = UseSetting.VLKAnalizDelete;
                                ProbiCheckBox.Checked = UseSetting.VLKProbi;
                                ProbiAddCheckBox.Checked = UseSetting.VLKProbiAdd;
                                ProbiEditCheckBox.Checked = UseSetting.VLKProbiEdit;
                                ProbiChangeDataCheckBox.Checked = UseSetting.VLKProbiChangeData;
                                ProbiPrintCheckBox.Checked = UseSetting.VLKProbiPrint;
                                ProbiDeleteCheckBox.Checked = UseSetting.VLKProbiDelete;
                                OperativniiCheckBox.Checked = UseSetting.VLKOperaivnii;
                                OperativniiEditCheckBox.Checked = UseSetting.VLKOperaivniiEdit;
                                OperativniiCloseCheckBox.Checked = UseSetting.VLKOperaivniiClose;
                                OperativniiDeleteCheckBox.Checked = UseSetting.VLKOperaivniiDelete;
                                OperativniiPrintCheckBox.Checked = UseSetting.VLKOperaivniiPrint;
                                StabilnostiCheckBox.Checked = UseSetting.VLKStabilnosti;
                                StabilnostiEditCheckBox.Checked = UseSetting.VLKStabilnostiEdit;
                                StabilnostiCloseCheckBox.Checked = UseSetting.VLKStabilnostiClose;
                                StabilnostiDeleteCheckBox.Checked = UseSetting.VLKStabilnostiDelete;
                                StabilnostiKKSHCheckBox.Checked = UseSetting.VLKStabilnostiProtokolOchenka;
                                StabilnostiDiagramsCheckBox.Checked = UseSetting.VLKStabilnostiDiagrams;
                                StabilnostiPrintCheckBox.Checked = UseSetting.VLKStabilnostiPrint;
                            }
                        }
                        db.Dispose();
                    }
                }
            }
        }

        private void AnalizCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control CBox in groupControl17.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "AnalizCheckBox") continue;
                    CB.Enabled = AnalizCheckBox.Checked;
                }
            }
        }

        private void ProbiCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control CBox in groupControl16.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "ProbiCheckBox") continue;
                    CB.Enabled = ProbiCheckBox.Checked;
                }
            }
        }

        private void OperativniiCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control CBox in groupControl15.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "OperativniiCheckBox") continue;
                    CB.Enabled = OperativniiCheckBox.Checked;
                }
            }
        }

        private void StabilnostiCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control CBox in groupControl14.Controls)
            {
                if (CBox is CheckBox)
                {
                    var CB = CBox as CheckBox;
                    if (CB.Name == "StabilnostiCheckBox") continue;
                    CB.Enabled = StabilnostiCheckBox.Checked;
                }
            }
        }

        private void Password_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    SaveButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void AddChangeUserForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
