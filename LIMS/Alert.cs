﻿using System.Collections.Generic;

namespace LIMS
{
    public static class Alert
    {
        public static int Height = 0;
        public static int Width = 0;
        public static List<int> WindowsHeights = new List<int>();
        public static bool TurnONAlerts = true;
    }
}
