﻿namespace LIMS
{
    partial class ReactivPrintAktvProverkiPrigodnostiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReactivPrintAktvProverkiPrigodnostiForm));
            this.CancelClickButton = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.ReactivName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Kvalif = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ND = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.NDiMVI = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ObrazechDlyaControla1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Zakluchenie = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.DateOkonSrokHran = new System.Windows.Forms.TextBox();
            this.PrintButton = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.DateIzgotovlenia = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Partia = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.AktCreateDate = new System.Windows.Forms.DateTimePicker();
            this.ID = new System.Windows.Forms.Label();
            this.Dolchnost = new System.Windows.Forms.ComboBox();
            this.ObrazechDlyaControla3 = new System.Windows.Forms.TextBox();
            this.ObrazechDlyaControla2 = new System.Windows.Forms.TextBox();
            this.SaveButton = new DevExpress.XtraEditors.SimpleButton();
            this.label6 = new System.Windows.Forms.Label();
            this.FIO = new System.Windows.Forms.ComboBox();
            this.AktProverkaPrigodnostiGridView = new System.Windows.Forms.DataGridView();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IspolnitelTabl1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.IspolnitelTabl2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Attectat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RezultatMassovii = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MassovaiaKonchentrachia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NormativKontrolia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ot = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.N = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Loader = new DevExpress.XtraWaitForm.ProgressPanel();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AktProverkaPrigodnostiGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // CancelClickButton
            // 
            this.CancelClickButton.Location = new System.Drawing.Point(475, 516);
            this.CancelClickButton.Name = "CancelClickButton";
            this.CancelClickButton.Size = new System.Drawing.Size(75, 23);
            this.CancelClickButton.TabIndex = 20;
            this.CancelClickButton.Text = "Отмена";
            this.CancelClickButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Наименование реактива:";
            // 
            // ReactivName
            // 
            this.ReactivName.Location = new System.Drawing.Point(146, 39);
            this.ReactivName.Name = "ReactivName";
            this.ReactivName.Size = new System.Drawing.Size(200, 21);
            this.ReactivName.TabIndex = 3;
            this.ReactivName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Квалификация:";
            // 
            // Kvalif
            // 
            this.Kvalif.Location = new System.Drawing.Point(146, 66);
            this.Kvalif.Name = "Kvalif";
            this.Kvalif.Size = new System.Drawing.Size(200, 21);
            this.Kvalif.TabIndex = 4;
            this.Kvalif.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Дата окон. срока хран.:";
            // 
            // ND
            // 
            this.ND.Location = new System.Drawing.Point(146, 93);
            this.ND.Name = "ND";
            this.ND.Size = new System.Drawing.Size(200, 21);
            this.ND.TabIndex = 5;
            this.ND.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(81, 205);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "НД и МВИ:";
            // 
            // NDiMVI
            // 
            this.NDiMVI.Location = new System.Drawing.Point(146, 202);
            this.NDiMVI.Multiline = true;
            this.NDiMVI.Name = "NDiMVI";
            this.NDiMVI.Size = new System.Drawing.Size(200, 38);
            this.NDiMVI.TabIndex = 9;
            this.NDiMVI.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(367, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Образец для контроля:";
            // 
            // ObrazechDlyaControla1
            // 
            this.ObrazechDlyaControla1.Location = new System.Drawing.Point(500, 12);
            this.ObrazechDlyaControla1.Multiline = true;
            this.ObrazechDlyaControla1.Name = "ObrazechDlyaControla1";
            this.ObrazechDlyaControla1.Size = new System.Drawing.Size(312, 48);
            this.ObrazechDlyaControla1.TabIndex = 11;
            this.ObrazechDlyaControla1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(420, 180);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 13);
            this.label15.TabIndex = 42;
            this.label15.Text = "Заключение:";
            // 
            // Zakluchenie
            // 
            this.Zakluchenie.Location = new System.Drawing.Point(500, 177);
            this.Zakluchenie.Multiline = true;
            this.Zakluchenie.Name = "Zakluchenie";
            this.Zakluchenie.Size = new System.Drawing.Size(312, 38);
            this.Zakluchenie.TabIndex = 14;
            this.Zakluchenie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(50, 96);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 13);
            this.label20.TabIndex = 52;
            this.label20.Text = "НД на реактив:";
            // 
            // DateOkonSrokHran
            // 
            this.DateOkonSrokHran.Location = new System.Drawing.Point(146, 175);
            this.DateOkonSrokHran.Name = "DateOkonSrokHran";
            this.DateOkonSrokHran.Size = new System.Drawing.Size(200, 21);
            this.DateOkonSrokHran.TabIndex = 8;
            this.DateOkonSrokHran.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // PrintButton
            // 
            this.PrintButton.Location = new System.Drawing.Point(313, 516);
            this.PrintButton.Name = "PrintButton";
            this.PrintButton.Size = new System.Drawing.Size(75, 23);
            this.PrintButton.TabIndex = 18;
            this.PrintButton.Text = "Печать";
            this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.DateIzgotovlenia);
            this.panelControl1.Controls.Add(this.label9);
            this.panelControl1.Controls.Add(this.label8);
            this.panelControl1.Controls.Add(this.Partia);
            this.panelControl1.Controls.Add(this.label7);
            this.panelControl1.Controls.Add(this.AktCreateDate);
            this.panelControl1.Controls.Add(this.ID);
            this.panelControl1.Controls.Add(this.Dolchnost);
            this.panelControl1.Controls.Add(this.ObrazechDlyaControla3);
            this.panelControl1.Controls.Add(this.ObrazechDlyaControla2);
            this.panelControl1.Controls.Add(this.SaveButton);
            this.panelControl1.Controls.Add(this.label6);
            this.panelControl1.Controls.Add(this.FIO);
            this.panelControl1.Controls.Add(this.AktProverkaPrigodnostiGridView);
            this.panelControl1.Controls.Add(this.Ot);
            this.panelControl1.Controls.Add(this.label27);
            this.panelControl1.Controls.Add(this.N);
            this.panelControl1.Controls.Add(this.label17);
            this.panelControl1.Controls.Add(this.PrintButton);
            this.panelControl1.Controls.Add(this.DateOkonSrokHran);
            this.panelControl1.Controls.Add(this.label20);
            this.panelControl1.Controls.Add(this.Zakluchenie);
            this.panelControl1.Controls.Add(this.label15);
            this.panelControl1.Controls.Add(this.ObrazechDlyaControla1);
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Controls.Add(this.NDiMVI);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.ND);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.Kvalif);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.ReactivName);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.CancelClickButton);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(824, 551);
            this.panelControl1.TabIndex = 0;
            // 
            // DateIzgotovlenia
            // 
            this.DateIzgotovlenia.Checked = false;
            this.DateIzgotovlenia.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateIzgotovlenia.Location = new System.Drawing.Point(146, 148);
            this.DateIzgotovlenia.Name = "DateIzgotovlenia";
            this.DateIzgotovlenia.ShowCheckBox = true;
            this.DateIzgotovlenia.Size = new System.Drawing.Size(200, 21);
            this.DateIzgotovlenia.TabIndex = 7;
            this.DateIzgotovlenia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(30, 154);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 13);
            this.label9.TabIndex = 98;
            this.label9.Text = "Дата изготовления:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(88, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 97;
            this.label8.Text = "Партия:";
            // 
            // Partia
            // 
            this.Partia.Location = new System.Drawing.Point(146, 120);
            this.Partia.Name = "Partia";
            this.Partia.Size = new System.Drawing.Size(200, 21);
            this.Partia.TabIndex = 6;
            this.Partia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 251);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 95;
            this.label7.Text = "Дата создания акта:";
            // 
            // AktCreateDate
            // 
            this.AktCreateDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.AktCreateDate.Location = new System.Drawing.Point(146, 247);
            this.AktCreateDate.Name = "AktCreateDate";
            this.AktCreateDate.Size = new System.Drawing.Size(200, 21);
            this.AktCreateDate.TabIndex = 10;
            this.AktCreateDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ID.Location = new System.Drawing.Point(14, 190);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(0, 13);
            this.ID.TabIndex = 93;
            this.ID.Visible = false;
            // 
            // Dolchnost
            // 
            this.Dolchnost.FormattingEnabled = true;
            this.Dolchnost.Items.AddRange(new object[] {
            "Инженер химического анализа"});
            this.Dolchnost.Location = new System.Drawing.Point(240, 489);
            this.Dolchnost.Name = "Dolchnost";
            this.Dolchnost.Size = new System.Drawing.Size(209, 21);
            this.Dolchnost.TabIndex = 16;
            this.Dolchnost.Text = "Инженер химического анализа";
            this.Dolchnost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // ObrazechDlyaControla3
            // 
            this.ObrazechDlyaControla3.Location = new System.Drawing.Point(500, 123);
            this.ObrazechDlyaControla3.Multiline = true;
            this.ObrazechDlyaControla3.Name = "ObrazechDlyaControla3";
            this.ObrazechDlyaControla3.Size = new System.Drawing.Size(312, 48);
            this.ObrazechDlyaControla3.TabIndex = 13;
            this.ObrazechDlyaControla3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // ObrazechDlyaControla2
            // 
            this.ObrazechDlyaControla2.Location = new System.Drawing.Point(500, 69);
            this.ObrazechDlyaControla2.Multiline = true;
            this.ObrazechDlyaControla2.Name = "ObrazechDlyaControla2";
            this.ObrazechDlyaControla2.Size = new System.Drawing.Size(312, 48);
            this.ObrazechDlyaControla2.TabIndex = 12;
            this.ObrazechDlyaControla2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(394, 516);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 19;
            this.SaveButton.Text = "Сохранить";
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(175, 492);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 88;
            this.label6.Text = "Составил:";
            // 
            // FIO
            // 
            this.FIO.FormattingEnabled = true;
            this.FIO.Location = new System.Drawing.Point(455, 489);
            this.FIO.Name = "FIO";
            this.FIO.Size = new System.Drawing.Size(201, 21);
            this.FIO.TabIndex = 17;
            this.FIO.Text = "";
            this.FIO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // AktProverkaPrigodnostiGridView
            // 
            this.AktProverkaPrigodnostiGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AktProverkaPrigodnostiGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.AktProverkaPrigodnostiGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.AktProverkaPrigodnostiGridView.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.AktProverkaPrigodnostiGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AktProverkaPrigodnostiGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.AktProverkaPrigodnostiGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AktProverkaPrigodnostiGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.IspolnitelTabl1,
            this.IspolnitelTabl2,
            this.Attectat,
            this.RezultatMassovii,
            this.MassovaiaKonchentrachia,
            this.NormativKontrolia});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AktProverkaPrigodnostiGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.AktProverkaPrigodnostiGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.AktProverkaPrigodnostiGridView.Location = new System.Drawing.Point(12, 274);
            this.AktProverkaPrigodnostiGridView.Name = "AktProverkaPrigodnostiGridView";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AktProverkaPrigodnostiGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.AktProverkaPrigodnostiGridView.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AktProverkaPrigodnostiGridView.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AktProverkaPrigodnostiGridView.Size = new System.Drawing.Size(800, 209);
            this.AktProverkaPrigodnostiGridView.TabIndex = 15;
            this.AktProverkaPrigodnostiGridView.Enter += new System.EventHandler(this.AktProverkaPrigodnostiGridView_Enter);
            // 
            // Date
            // 
            this.Date.HeaderText = "Дата проведения измерения";
            this.Date.Name = "Date";
            this.Date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // IspolnitelTabl1
            // 
            this.IspolnitelTabl1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.IspolnitelTabl1.HeaderText = "Исполнитель1";
            this.IspolnitelTabl1.MaxDropDownItems = 100;
            this.IspolnitelTabl1.Name = "IspolnitelTabl1";
            this.IspolnitelTabl1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // IspolnitelTabl2
            // 
            this.IspolnitelTabl2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.IspolnitelTabl2.HeaderText = "Исполнитель2";
            this.IspolnitelTabl2.MaxDropDownItems = 20;
            this.IspolnitelTabl2.Name = "IspolnitelTabl2";
            this.IspolnitelTabl2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Attectat
            // 
            this.Attectat.HeaderText = "Аттестованное значение";
            this.Attectat.Name = "Attectat";
            this.Attectat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // RezultatMassovii
            // 
            this.RezultatMassovii.HeaderText = "Результат измерений";
            this.RezultatMassovii.Name = "RezultatMassovii";
            this.RezultatMassovii.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // MassovaiaKonchentrachia
            // 
            this.MassovaiaKonchentrachia.HeaderText = "Результат контрольной процедуры, Кк";
            this.MassovaiaKonchentrachia.Name = "MassovaiaKonchentrachia";
            this.MassovaiaKonchentrachia.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NormativKontrolia
            // 
            this.NormativKontrolia.HeaderText = "Норматив контроля К";
            this.NormativKontrolia.Name = "NormativKontrolia";
            this.NormativKontrolia.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Ot
            // 
            this.Ot.Location = new System.Drawing.Point(168, 12);
            this.Ot.Name = "Ot";
            this.Ot.Size = new System.Drawing.Size(178, 21);
            this.Ot.TabIndex = 2;
            this.Ot.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(143, 15);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(19, 13);
            this.label27.TabIndex = 84;
            this.label27.Text = "от";
            // 
            // N
            // 
            this.N.Location = new System.Drawing.Point(44, 12);
            this.N.Name = "N";
            this.N.Size = new System.Drawing.Size(92, 21);
            this.N.TabIndex = 1;
            this.N.KeyDown += new System.Windows.Forms.KeyEventHandler(this.N_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(14, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(24, 13);
            this.label17.TabIndex = 82;
            this.label17.Text = "№:";
            // 
            // Loader
            // 
            this.Loader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Loader.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Loader.Appearance.Options.UseBackColor = true;
            this.Loader.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Loader.AppearanceCaption.Options.UseFont = true;
            this.Loader.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Loader.AppearanceDescription.Options.UseFont = true;
            this.Loader.BarAnimationElementThickness = 2;
            this.Loader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.Loader.Caption = "Подождите";
            this.Loader.Description = "Формирование документа ...";
            this.Loader.Location = new System.Drawing.Point(289, 242);
            this.Loader.Name = "Loader";
            this.Loader.Size = new System.Drawing.Size(246, 66);
            this.Loader.TabIndex = 100;
            this.Loader.Visible = false;
            // 
            // ReactivPrintAktvProverkiPrigodnostiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 551);
            this.Controls.Add(this.Loader);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "ReactivPrintAktvProverkiPrigodnostiForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Акт регистрации результатов  проверки пригодности реактивов";
            this.TopMost = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ReactivPrintAktvRegistResultForm_FormClosed);
            this.Load += new System.EventHandler(this.ReactivPrintAktvProverkiPrigodnostiForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivPrintAktvProverkiPrigodnostiForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AktProverkaPrigodnostiGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton CancelClickButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ReactivName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Kvalif;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ND;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox NDiMVI;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox ObrazechDlyaControla1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Zakluchenie;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox DateOkonSrokHran;
        private DevExpress.XtraEditors.SimpleButton PrintButton;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.TextBox Ot;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox N;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridView AktProverkaPrigodnostiGridView;
        private System.Windows.Forms.ComboBox FIO;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.SimpleButton SaveButton;
        private System.Windows.Forms.TextBox ObrazechDlyaControla3;
        private System.Windows.Forms.TextBox ObrazechDlyaControla2;
        private System.Windows.Forms.ComboBox Dolchnost;
        private System.Windows.Forms.Label ID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker AktCreateDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewComboBoxColumn IspolnitelTabl1;
        private System.Windows.Forms.DataGridViewComboBoxColumn IspolnitelTabl2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Attectat;
        private System.Windows.Forms.DataGridViewTextBoxColumn RezultatMassovii;
        private System.Windows.Forms.DataGridViewTextBoxColumn MassovaiaKonchentrachia;
        private System.Windows.Forms.DataGridViewTextBoxColumn NormativKontrolia;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Partia;
        private System.Windows.Forms.DateTimePicker DateIzgotovlenia;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraWaitForm.ProgressPanel Loader;
    }
}