﻿using System;
using System.Windows.Forms;

namespace LIMS
{
    public partial class CheckReactivColumnForm : DevExpress.XtraEditors.XtraForm
    {
        ReactivTab RT;
        public CheckReactivColumnForm(MainForm MF, ReactivTab ReactivTab)
        {
            InitializeComponent();
            if (UserSettings.ReactivColumns != null)
            {
                var CheckColumn = UserSettings.ReactivColumns.Split(',');
                CheckEditN.Checked = CheckColumn[0] == "1";
                CheckEditName.Checked = CheckColumn[1] == "1";
                CheckEditClass.Checked = CheckColumn[2] == "1";
                CheckEditND.Checked = CheckColumn[3] == "1";
                CheckEditNPartii.Checked = CheckColumn[4] == "1";
                CheckEditDateIzgot.Checked = CheckColumn[5] == "1";
                CheckEditSrokGodnosti.Checked = CheckColumn[6] == "1";
                CheckEditDateGet.Checked = CheckColumn[7] == "1";
                CheckEditEdIzm.Checked = CheckColumn[8] == "1";
                CheckEditKolvo.Checked = CheckColumn[9] == "1";
                CheckEditNaznachenie.Checked = CheckColumn[10] == "1";
                CheckEditProdlenieSroka.Checked = CheckColumn[11] == "1";
            }
            RT = ReactivTab;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            string CheckString = "";
            if (CheckEditN.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditName.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditClass.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditND.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditNPartii.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDateIzgot.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditSrokGodnosti.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditDateGet.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditEdIzm.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditKolvo.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditNaznachenie.Checked == true) { CheckString = CheckString + "1,"; } else { CheckString = CheckString + "0,"; }
            if (CheckEditProdlenieSroka.Checked == true) { CheckString = CheckString + "1"; } else { CheckString = CheckString + "0"; }
            MainForm main = this.Owner as MainForm;
            if (main != null)
            {
                UserSettings.ReactivColumns = CheckString;
                UserSettings.SaveSettings();
                RT.GetReactiv(main);
            }
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CheckEditN_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    OKButton_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void CheckReactivColumnForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
