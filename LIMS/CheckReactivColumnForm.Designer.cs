﻿namespace LIMS
{
    partial class CheckReactivColumnForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckReactivColumnForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.CheckEditN = new DevExpress.XtraEditors.CheckEdit();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.CancelClickButton = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEditProdlenieSroka = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditNaznachenie = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditKolvo = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditEdIzm = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateGet = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditSrokGodnosti = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateIzgot = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditNPartii = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditND = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditClass = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditName = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditProdlenieSroka.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNaznachenie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditKolvo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditEdIzm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateGet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokGodnosti.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateIzgot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNPartii.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditClass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.CheckEditN);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelClickButton);
            this.panelControl1.Controls.Add(this.CheckEditProdlenieSroka);
            this.panelControl1.Controls.Add(this.CheckEditNaznachenie);
            this.panelControl1.Controls.Add(this.CheckEditKolvo);
            this.panelControl1.Controls.Add(this.CheckEditEdIzm);
            this.panelControl1.Controls.Add(this.CheckEditDateGet);
            this.panelControl1.Controls.Add(this.CheckEditSrokGodnosti);
            this.panelControl1.Controls.Add(this.CheckEditDateIzgot);
            this.panelControl1.Controls.Add(this.CheckEditNPartii);
            this.panelControl1.Controls.Add(this.CheckEditND);
            this.panelControl1.Controls.Add(this.CheckEditClass);
            this.panelControl1.Controls.Add(this.CheckEditName);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(265, 194);
            this.panelControl1.TabIndex = 0;
            // 
            // CheckEditN
            // 
            this.CheckEditN.EditValue = true;
            this.CheckEditN.Location = new System.Drawing.Point(12, 12);
            this.CheckEditN.Name = "CheckEditN";
            this.CheckEditN.Properties.Caption = "№ п/п";
            this.CheckEditN.Size = new System.Drawing.Size(112, 19);
            this.CheckEditN.TabIndex = 1;
            this.CheckEditN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(49, 162);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 13;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelClickButton
            // 
            this.CancelClickButton.Location = new System.Drawing.Point(145, 162);
            this.CancelClickButton.Name = "CancelClickButton";
            this.CancelClickButton.Size = new System.Drawing.Size(75, 23);
            this.CancelClickButton.TabIndex = 14;
            this.CancelClickButton.Text = "Отмена";
            this.CancelClickButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CheckEditProdlenieSroka
            // 
            this.CheckEditProdlenieSroka.EditValue = true;
            this.CheckEditProdlenieSroka.Location = new System.Drawing.Point(145, 137);
            this.CheckEditProdlenieSroka.Name = "CheckEditProdlenieSroka";
            this.CheckEditProdlenieSroka.Properties.Caption = "Продление срока";
            this.CheckEditProdlenieSroka.Size = new System.Drawing.Size(109, 19);
            this.CheckEditProdlenieSroka.TabIndex = 12;
            this.CheckEditProdlenieSroka.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditNaznachenie
            // 
            this.CheckEditNaznachenie.EditValue = true;
            this.CheckEditNaznachenie.Location = new System.Drawing.Point(145, 112);
            this.CheckEditNaznachenie.Name = "CheckEditNaznachenie";
            this.CheckEditNaznachenie.Properties.Caption = "Назначение";
            this.CheckEditNaznachenie.Size = new System.Drawing.Size(109, 19);
            this.CheckEditNaznachenie.TabIndex = 11;
            this.CheckEditNaznachenie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditKolvo
            // 
            this.CheckEditKolvo.EditValue = true;
            this.CheckEditKolvo.Location = new System.Drawing.Point(145, 87);
            this.CheckEditKolvo.Name = "CheckEditKolvo";
            this.CheckEditKolvo.Properties.Caption = "Ед. изм.";
            this.CheckEditKolvo.Size = new System.Drawing.Size(109, 19);
            this.CheckEditKolvo.TabIndex = 10;
            this.CheckEditKolvo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditEdIzm
            // 
            this.CheckEditEdIzm.EditValue = true;
            this.CheckEditEdIzm.Location = new System.Drawing.Point(145, 62);
            this.CheckEditEdIzm.Name = "CheckEditEdIzm";
            this.CheckEditEdIzm.Properties.Caption = "Количество";
            this.CheckEditEdIzm.Size = new System.Drawing.Size(109, 19);
            this.CheckEditEdIzm.TabIndex = 9;
            this.CheckEditEdIzm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDateGet
            // 
            this.CheckEditDateGet.EditValue = true;
            this.CheckEditDateGet.Location = new System.Drawing.Point(145, 37);
            this.CheckEditDateGet.Name = "CheckEditDateGet";
            this.CheckEditDateGet.Properties.Caption = "Дата получения";
            this.CheckEditDateGet.Size = new System.Drawing.Size(109, 19);
            this.CheckEditDateGet.TabIndex = 8;
            this.CheckEditDateGet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditSrokGodnosti
            // 
            this.CheckEditSrokGodnosti.EditValue = true;
            this.CheckEditSrokGodnosti.Location = new System.Drawing.Point(145, 12);
            this.CheckEditSrokGodnosti.Name = "CheckEditSrokGodnosti";
            this.CheckEditSrokGodnosti.Properties.Caption = "Срок годности";
            this.CheckEditSrokGodnosti.Size = new System.Drawing.Size(109, 19);
            this.CheckEditSrokGodnosti.TabIndex = 7;
            this.CheckEditSrokGodnosti.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDateIzgot
            // 
            this.CheckEditDateIzgot.EditValue = true;
            this.CheckEditDateIzgot.Location = new System.Drawing.Point(12, 137);
            this.CheckEditDateIzgot.Name = "CheckEditDateIzgot";
            this.CheckEditDateIzgot.Properties.Caption = "Дата изготовления";
            this.CheckEditDateIzgot.Size = new System.Drawing.Size(127, 19);
            this.CheckEditDateIzgot.TabIndex = 6;
            this.CheckEditDateIzgot.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditNPartii
            // 
            this.CheckEditNPartii.EditValue = true;
            this.CheckEditNPartii.Location = new System.Drawing.Point(12, 112);
            this.CheckEditNPartii.Name = "CheckEditNPartii";
            this.CheckEditNPartii.Properties.Caption = "Номер партии";
            this.CheckEditNPartii.Size = new System.Drawing.Size(112, 19);
            this.CheckEditNPartii.TabIndex = 5;
            this.CheckEditNPartii.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditND
            // 
            this.CheckEditND.EditValue = true;
            this.CheckEditND.Location = new System.Drawing.Point(12, 87);
            this.CheckEditND.Name = "CheckEditND";
            this.CheckEditND.Properties.Caption = "НД на реактив";
            this.CheckEditND.Size = new System.Drawing.Size(112, 19);
            this.CheckEditND.TabIndex = 4;
            this.CheckEditND.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditClass
            // 
            this.CheckEditClass.EditValue = true;
            this.CheckEditClass.Location = new System.Drawing.Point(12, 62);
            this.CheckEditClass.Name = "CheckEditClass";
            this.CheckEditClass.Properties.Caption = "Классификация";
            this.CheckEditClass.Size = new System.Drawing.Size(112, 19);
            this.CheckEditClass.TabIndex = 3;
            this.CheckEditClass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditName
            // 
            this.CheckEditName.EditValue = true;
            this.CheckEditName.Location = new System.Drawing.Point(12, 37);
            this.CheckEditName.Name = "CheckEditName";
            this.CheckEditName.Properties.Caption = "Наименование";
            this.CheckEditName.Size = new System.Drawing.Size(112, 19);
            this.CheckEditName.TabIndex = 2;
            this.CheckEditName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckReactivColumnForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 194);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "CheckReactivColumnForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Реактивы";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckReactivColumnForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditProdlenieSroka.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNaznachenie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditKolvo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditEdIzm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateGet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokGodnosti.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateIzgot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditNPartii.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditClass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit CheckEditName;
        private DevExpress.XtraEditors.CheckEdit CheckEditNaznachenie;
        private DevExpress.XtraEditors.CheckEdit CheckEditKolvo;
        private DevExpress.XtraEditors.CheckEdit CheckEditEdIzm;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateGet;
        private DevExpress.XtraEditors.CheckEdit CheckEditSrokGodnosti;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateIzgot;
        private DevExpress.XtraEditors.CheckEdit CheckEditNPartii;
        private DevExpress.XtraEditors.CheckEdit CheckEditND;
        private DevExpress.XtraEditors.CheckEdit CheckEditClass;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.SimpleButton CancelClickButton;
        private DevExpress.XtraEditors.CheckEdit CheckEditProdlenieSroka;
        private DevExpress.XtraEditors.CheckEdit CheckEditN;
    }
}