﻿using System;
using System.Windows.Forms;

namespace LIMS
{
    public partial class PredeliForm : DevExpress.XtraEditors.XtraForm
    {
        public PredeliForm(Probi Stroke)
        {
            InitializeComponent();
            try
            {
                if (Stroke != null)
                {
                    VLKStatic.CalculatePredel(Stroke);
                    X.Text = VLKStatic.AddZero(VLKStatic.X.ToString());
                    rKSmall.Text = VLKStatic.AddZero(VLKStatic.rk.ToString(), VLKStatic.NumberRound - 1);
                    RKBig.Text = VLKStatic.AddZero(VLKStatic.Rk.ToString(), VLKStatic.NumberRound - 1);
                    rSmall.Text = VLKStatic.AddZero(VLKStatic.r.ToString(), VLKStatic.NumberRound - 1);
                    RBig.Text = VLKStatic.R.ToString();
                    O1r.Text = VLKStatic.Or.ToString();
                    O2R.Text = VLKStatic.OR.ToString();

                    O3Rl.Text = VLKStatic.ORl.ToString();
                    A1.Text = VLKStatic.A.ToString();
                    A2l.Text = VLKStatic.Al.ToString();
                    //Таблица
                    Tabl11.Text = VLKStatic.AddZero(VLKStatic.Povt2);
                    Tabl21.Text = VLKStatic.AddZero(VLKStatic.Povt1);
                    Tabl31.Text = VLKStatic.AddZero(VLKStatic.Povt3);
                    Tabl12.Text = VLKStatic.AddZero(VLKStatic.Prech2);
                    Tabl22.Text = VLKStatic.AddZero(VLKStatic.Prech1);
                    Tabl32.Text = VLKStatic.AddZero(VLKStatic.Prech3);
                    Tabl13.Text = "\u00B1" + VLKStatic.AddZero(VLKStatic.Pogr1);
                    Tabl23.Text = "\u00B1" + VLKStatic.AddZero(VLKStatic.Pogr2);
                    Tabl33.Text = VLKStatic.AddZero("");
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private int GetRondNumber(double value)
        {
            try
            {
                int i = 0;
                while (Math.Round(value, i) != value)
                {
                    i++;
                }
                return i;
            }
            catch
            {
                return 0;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PredeliForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
