﻿using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LIMS
{
    public partial class ReactivListAktProverkiPrigodnostiForm : DevExpress.XtraEditors.XtraForm
    {
        MainForm Main;
        string ReactivName;
        string SelectedRowId = "";

        public ReactivListAktProverkiPrigodnostiForm(MainForm MF)
        {
            InitializeComponent();
            Main = MF;
            ReactivName = "";
        }

        public ReactivListAktProverkiPrigodnostiForm(MainForm MF, string RN)
        {
            InitializeComponent();
            Main = MF;
            ReactivName = RN;
        }

        public void GetReactivAkt()
        {
            try
            {
                ColumnView View = ReactivAktGridView;
                GridColumn column = View.Columns["Id"];
                using (DbDataContext db = new DbDataContext())
                {
                    List<ReactivAktProverkiPrigodnosti> ВсеЗначения;
                    if (!UserSettings.Developer)
                    {
                        if (ReactivName == "")
                        {
                            ВсеЗначения = db.ReactivAktProverkiPrigodnosti.Where(c => c.VisibleStatus == null || c.VisibleStatus == 0).OrderByDescending(c => c.Дата).ToList();
                        }
                        else
                        {
                            ВсеЗначения = db.ReactivAktProverkiPrigodnosti.Where(c => c.Наименование == ReactivName && (c.VisibleStatus == null || c.VisibleStatus == 0)).OrderByDescending(c => c.Дата).ToList();
                        }
                    }
                    else ВсеЗначения = db.ReactivAktProverkiPrigodnosti.OrderByDescending(c => c.Дата).ToList();

                    ReactivAktGridView.FocusedRowChanged -= ReactivAktGridView_FocusedRowChanged;
                    ReactivAktGrid.DataSource = ВсеЗначения;
                    if (!UserSettings.Developer)
                    {
                        ReactivAktGridView.Columns[0].Visible = false;                        
                        ReactivAktGridView.Columns[11].Visible = false;
                        ReactivAktGridView.Columns[12].Visible = false;
                        ReactivAktGridView.Columns[13].Visible = false;
                        ReactivAktGridView.Columns[14].Visible = false;
                        ReactivAktGridView.Columns[15].Visible = false;
                    }

                    ReactivAktGridView.FocusedRowChanged += ReactivAktGridView_FocusedRowChanged;                    
                    if (column != null)
                    {
                        int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                        if (rhFound != GridControl.InvalidRowHandle)
                        {
                            View.FocusedRowHandle = rhFound;
                            View.FocusedColumn = column;
                        }
                    }

                    if (ВсеЗначения != null && ВсеЗначения.Count() > 0)
                    {
                        ReactivAktGridView.Columns[1].Caption = "Дата создания акта";
                        ReactivAktGridView.Columns[2].Caption = "№ п/п";
                        ReactivAktGridView.Columns[3].Caption = "Наименование реактива";
                        ReactivAktGridView.Columns[4].Caption = "Квалификация";
                        ReactivAktGridView.Columns[5].Caption = "НД на реактив";
                        ReactivAktGridView.Columns[6].Caption = "Номер партии";
                        ReactivAktGridView.Columns[7].Caption = "Дата изготовления";
                        ReactivAktGridView.Columns[8].Caption = "Дата окончания срока хранения";
                        ReactivAktGridView.Columns[9].Caption = "НД и МВИ определяемый компонент";
                        ReactivAktGridView.Columns[10].Caption = "Заключение";
                        ReactivAktGridView.Columns[1].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        ReactivAktGridView.Columns[1].DisplayFormat.FormatString = "dd/MM/yyyy";
                        ReactivAktGridView.Columns[7].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        ReactivAktGridView.Columns[7].DisplayFormat.FormatString = "dd/MM/yyyy";
                        ReactivAktGridView.Columns[8].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                        ReactivAktGridView.Columns[8].DisplayFormat.FormatString = "dd/MM/yyyy";

                        //Wrap Header                        
                        ReactivAktGridView.OptionsView.RowAutoHeight = true;
                        DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit MemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit(); // инициализируем MemoEdit с именем “MyGrid_MemoEdit”
                        MemoEdit.WordWrap = true;
                        ReactivAktGridView.OptionsView.RowAutoHeight = true;
                        foreach (GridColumn My_GridColumn in ReactivAktGridView.Columns)
                        {
                            My_GridColumn.AppearanceCell.Options.UseTextOptions = true;
                            My_GridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                            My_GridColumn.ColumnEdit = MemoEdit;
                        }
                    }
                    db.Dispose();
                }

                ReactivAktGridView.FocusedRowChanged -= ReactivAktGridView_FocusedRowChanged;                
                if (column != null)
                {
                    int rhFound = View.LocateByDisplayText(View.FocusedRowHandle + 1, column, SelectedRowId);
                    if (rhFound != GridControl.InvalidRowHandle)
                    {
                        View.FocusedRowHandle = rhFound;
                        View.FocusedColumn = column;
                    }
                }
                ReactivAktGridView.FocusedRowChanged += ReactivAktGridView_FocusedRowChanged;
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ReactivAktGrid_DoubleClick(object sender, EventArgs e)
        {
            EditAkt();
        }

        private void EditAkt()
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivAktGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                var Stroke = (ReactivAktProverkiPrigodnosti)gv.GetRow(gv.FocusedRowHandle);
                if (Stroke != null)
                {
                    ReactivPrintAktvProverkiPrigodnostiForm f = new ReactivPrintAktvProverkiPrigodnostiForm(Main, Stroke);
                    f.Owner = this;
                    this.Visible = false;
                    f.ShowDialog();
                    GetReactivAkt();
                    f.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ReactivListAktVhodnogoKontrolaForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Main.Visible = true;
            Close();
        }

        private void ReactivListAktVhodnogoKontrolaForm_Load(object sender, EventArgs e)
        {
            GetReactivAkt();
            WindowState = FormWindowState.Maximized;
        }

        private void ReactivAktGridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            try
            {
                GridView view = sender as GridView;
                DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivAktGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                var Stroke = (ReactivAktProverkiPrigodnosti)gv.GetRow(gv.FocusedRowHandle);
                if (e.MenuType == DevExpress.XtraGrid.Views.Grid.GridMenuType.Row)
                {
                    int rowHandle = e.HitInfo.RowHandle;
                    e.Menu.Items.Clear();
                    DXMenuCheckItem checkItem10 = new DXMenuCheckItem("&Редактировать",
                    view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivEdit));
                    checkItem10.Tag = new RowInfo(view, rowHandle);
                    e.Menu.Items.Add(checkItem10);
                    if (UserSettings.ReactivDelete)
                    {
                        DXMenuCheckItem checkItem3 = new DXMenuCheckItem("&Удалить",
                        view.OptionsView.AllowCellMerge, null, new EventHandler(ReactivAktDeleteButton_Click));
                        checkItem3.Tag = new RowInfo(view, rowHandle);
                        e.Menu.Items.Add(checkItem3);
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void ReactivAktDeleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    //Удаление
                    DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivAktGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
                    var Stroke = (ReactivAktProverkiPrigodnosti)gv.GetRow(gv.FocusedRowHandle);
                    if (Stroke != null)
                    {
                        DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить " + Stroke.Наименование + " ?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            var Del = db.ReactivAktProverkiPrigodnosti.Where(c => c.Id == Stroke.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).FirstOrDefault();
                            Del.VisibleStatus = Del.Owner == null ? 300 : 301;
                            Del.UserName = UserSettings.User;
                            Del.ДатаСозданияЗаписи = DateTime.Now;
                            var Table = db.ReactivAktProverkiPrigodnostiTable.Where(c => c.MainID == Del.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                            foreach (var TableStroke in Table)
                            {
                                TableStroke.VisibleStatus = TableStroke.Owner == null ? 300 : 301;
                                TableStroke.UserName = UserSettings.User;
                                TableStroke.ДатаСозданияЗаписи = DateTime.Now;
                            }
                            var Obrazech = db.ReactivAktProverkiPrigodnostiObrazech.Where(c => c.MainID == Del.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                            foreach (var OtherStroke in Obrazech)
                            {
                                OtherStroke.VisibleStatus = OtherStroke.Owner == null ? 300 : 301;
                                OtherStroke.UserName = UserSettings.User;
                                OtherStroke.ДатаСозданияЗаписи = DateTime.Now;
                            }
                            var Vipolnil = db.ReactivAktProverkiPrigodnostiVipolnil.Where(c => c.MainID == Del.Id && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                            foreach (var KomissiyaStroke in Vipolnil)
                            {
                                KomissiyaStroke.VisibleStatus = KomissiyaStroke.Owner == null ? 300 : 301;
                                KomissiyaStroke.UserName = UserSettings.User;
                                KomissiyaStroke.ДатаСозданияЗаписи = DateTime.Now;
                            }
                            db.SubmitChanges();
                            db.Dispose();                            
                            if (gv.FocusedRowHandle > 0)
                            {
                                Stroke = (ReactivAktProverkiPrigodnosti)gv.GetRow(gv.FocusedRowHandle - 1);
                                if (Stroke != null) SelectedRowId = Stroke.Id.ToString();
                            }
                            GetReactivAkt();
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не выбрана строка для удаления!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        private void ReactivEdit(object sender, EventArgs e)
        {
            EditAkt();
        }

        class RowInfo
        {
            public RowInfo(GridView view, int rowHandle)
            {
                this.RowHandle = rowHandle;
                this.View = view;
            }
            public GridView View;
            public int RowHandle;
        }

        private void ReactivAktGridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = ReactivAktGrid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var Stroke = (ReactivAktProverkiPrigodnosti)gv.GetRow(gv.FocusedRowHandle);
            if (Stroke == null) return;
            SelectedRowId = Stroke.Id.ToString();
        }

        private void ReactivListAktProverkiPrigodnostiForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
