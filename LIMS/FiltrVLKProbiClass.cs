﻿using DevExpress.Data.Filtering;
using DevExpress.XtraGrid.Columns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace LIMS
{
    public class FiltrVLKProbiClass
    {
        static List<FiltrVLKProbi> SelectedFiltr = new List<FiltrVLKProbi>();
        public class FiltrVLKProbi
        {
            public string AnalizID { set; get; }
            public int? OperKontrolID { set; get; }            
            public DateTime? StartDate { set; get; }
            public DateTime? EndDate { set; get; }
            public int? ShifrOtN { set; get; }
            public int? ShifrOtYear { set; get; }
            public int? ShifrDoN { set; get; }
            public int? ShifrDoYear { set; get; }
        }
        public static void AddNewFiltr(FiltrVLKProbi NewFiltr)
        {
            foreach (var stroke in SelectedFiltr)
            {
                if (NewFiltr.AnalizID == stroke.AnalizID && NewFiltr.OperKontrolID == stroke.OperKontrolID)
                {
                    SelectedFiltr.Remove(stroke);
                    break;
                }
            }
            SelectedFiltr.Add(NewFiltr);

            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    VLKFiltr Filtr = new VLKFiltr();
                    if(NewFiltr.OperKontrolID == null)
                        Filtr = db.VLKFiltr.Where(c => c.User == UserSettings.User && c.AnalizID == NewFiltr.AnalizID && !c.OperKontrolID.HasValue).FirstOrDefault();
                    else
                        Filtr = db.VLKFiltr.Where(c => c.User == UserSettings.User && c.AnalizID == NewFiltr.AnalizID && c.OperKontrolID.HasValue && c.OperKontrolID.Value == NewFiltr.OperKontrolID).FirstOrDefault();                   
                    
                    if (Filtr == null)
                    {
                        db.VLKFiltr.InsertOnSubmit(new VLKFiltr
                        {
                            AnalizID = NewFiltr.AnalizID,
                            OperKontrolID = NewFiltr.OperKontrolID,
                            User = UserSettings.User,
                            EndDate = NewFiltr.EndDate,
                            ShifrDoN = NewFiltr.ShifrDoN,
                            ShifrDoYear = NewFiltr.ShifrDoYear,
                            ShifrOtN = NewFiltr.ShifrOtN,
                            ShifrOtYear = NewFiltr.ShifrOtYear,
                            StartDate = NewFiltr.StartDate
                        });
                    }
                    else
                    {
                        Filtr.EndDate = NewFiltr.EndDate;
                        Filtr.ShifrDoN = NewFiltr.ShifrDoN;
                        Filtr.ShifrDoYear = NewFiltr.ShifrDoYear;
                        Filtr.ShifrOtN = NewFiltr.ShifrOtN;
                        Filtr.ShifrOtYear = NewFiltr.ShifrOtYear;
                        Filtr.StartDate = NewFiltr.StartDate;
                    }
                    db.SubmitChanges();                    
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
            }
        }

        public static FiltrVLKProbi GetFiltr(string AnalizID, int? OperKontrolID = null)
        {
            foreach (var stroke in SelectedFiltr)
            {
                if (stroke.AnalizID == AnalizID && stroke.OperKontrolID == OperKontrolID)
                    return stroke;
            }
            return null;
        }

        public static void SetFiltr(MainForm Main, string AnalizID, int? OperKontrolID = null)
        {
            try
            {
                Main.GSOGridView.ColumnFilterChanged -= Main.GSOGridView_ColumnFilterChanged;
                foreach (var stroke in SelectedFiltr)
                {
                    if (stroke.AnalizID == AnalizID && stroke.OperKontrolID == OperKontrolID)
                    {
                        string DisplayText = "";
                        if (stroke.StartDate.HasValue || stroke.EndDate.HasValue)
                        {
                            if (stroke.StartDate.HasValue && stroke.EndDate.HasValue)
                            {
                                CriteriaOperator NewFilter = new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Дата", stroke.StartDate.Value, BinaryOperatorType.GreaterOrEqual),
                                        new BinaryOperator("Дата", stroke.EndDate.Value, BinaryOperatorType.LessOrEqual));
                                DisplayText = String.Format("[Дата] между {0:d} и {1:d}", stroke.StartDate.Value.ToShortDateString(), stroke.EndDate.Value.ToShortDateString());
                                ColumnFilterInfo dateFilter = new ColumnFilterInfo(NewFilter.ToString(), DisplayText);
                                Main.GSOGridView.Columns["Дата"].FilterInfo = dateFilter;
                            }
                            else if (stroke.StartDate.HasValue)
                            {
                                CriteriaOperator NewFilter = new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Дата", stroke.StartDate.Value, BinaryOperatorType.GreaterOrEqual));
                                DisplayText = String.Format("[Дата] больше {0:d}", stroke.StartDate.Value.ToShortDateString());
                                ColumnFilterInfo dateFilter = new ColumnFilterInfo(NewFilter.ToString(), DisplayText);
                                Main.GSOGridView.Columns["Дата"].FilterInfo = dateFilter;
                            }
                            else if (stroke.EndDate.HasValue)
                            {
                                CriteriaOperator NewFilter = new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Дата", stroke.EndDate.Value, BinaryOperatorType.LessOrEqual));
                                DisplayText = String.Format("[Дата] меньше {0:d}", stroke.EndDate.Value.ToShortDateString());
                                ColumnFilterInfo dateFilter = new ColumnFilterInfo(NewFilter.ToString(), DisplayText);
                                Main.GSOGridView.Columns["Дата"].FilterInfo = dateFilter;
                            }
                        }

                        if (stroke.ShifrOtN.HasValue || stroke.ShifrDoN.HasValue)
                        {
                            using (DbDataContext db = new DbDataContext())
                            {
                                var Analiz = db.Analiz.Where(c => c.Id == Convert.ToInt32(AnalizID)).FirstOrDefault();
                                if (Analiz != null)
                                {
                                    var Start1 = stroke.ShifrOtN.HasValue ? stroke.ShifrOtN.Value : 0;
                                    var Start2 = stroke.ShifrOtYear.HasValue ? stroke.ShifrOtYear.Value : 0;
                                    var End1 = stroke.ShifrDoN.HasValue ? stroke.ShifrDoN.Value : 0;
                                    var End2 = stroke.ShifrDoYear.HasValue ? stroke.ShifrDoYear.Value : 0;

                                    var Probi = db.Probi.Where(c => c.AnalizID == Convert.ToInt32(AnalizID) && (c.VisibleStatus == null || c.VisibleStatus == 0)).ToList();
                                    List<string> Shifrs = new List<string>();

                                    if (stroke.ShifrOtN.HasValue && stroke.ShifrDoN.HasValue)
                                    {
                                        foreach (var Prob in Probi)
                                        {
                                            var shif = Prob.Шифр.Split('-');
                                            if (Convert.ToInt32(shif[2]) >= Start2 && Convert.ToInt32(shif[2]) <= End2)
                                            {
                                                if (End2 == Start2)
                                                {
                                                    if (Convert.ToInt32(shif[1]) >= Start1 && Convert.ToInt32(shif[1]) <= End1)
                                                    {
                                                        if (!Shifrs.Contains(Prob.Шифр)) Shifrs.Add(Prob.Шифр);
                                                    }
                                                }
                                                else if (Convert.ToInt32(shif[2]) == Start2)
                                                {
                                                    if (Convert.ToInt32(shif[1]) >= Start1)
                                                    {
                                                        if (!Shifrs.Contains(Prob.Шифр)) Shifrs.Add(Prob.Шифр);
                                                    }
                                                }
                                                else if (Convert.ToInt32(shif[2]) == End2)
                                                {
                                                    if (Convert.ToInt32(shif[1]) <= End1)
                                                    {
                                                        if (!Shifrs.Contains(Prob.Шифр)) Shifrs.Add(Prob.Шифр);
                                                    }
                                                }
                                                else if (!Shifrs.Contains(Prob.Шифр)) Shifrs.Add(Prob.Шифр);
                                            }
                                        }
                                        BinaryOperator[] BO = new BinaryOperator[Shifrs.Count];
                                        for (int i = 0; i < Shifrs.Count; i++)
                                        {
                                            BO[i] = new BinaryOperator("Шифр", Shifrs[i], BinaryOperatorType.Equal);
                                        }
                                        ////CriteriaOperator NewFilter = new GroupOperator(GroupOperatorType.And,
                                        //// new BinaryOperator("Шифр пробы, анализируемой для целей контроля", Shifrs, BinaryOperatorType.Equal));
                                        CriteriaOperator NewFilter = new GroupOperator(GroupOperatorType.Or, BO);
                                        string filterDisplayText = String.Format("[Шифр пробы] между {0} и {1}", Analiz.Шифр + "-" + stroke.ShifrOtN.Value.ToString() + "-" + stroke.ShifrOtYear.Value.ToString(), Analiz.Шифр + "-" + stroke.ShifrDoN.Value.ToString() + "-" + stroke.ShifrDoYear.Value.ToString());
                                        if (DisplayText != "")
                                            filterDisplayText = DisplayText + ", " + filterDisplayText;
                                        ColumnFilterInfo dateFilter = new ColumnFilterInfo(NewFilter.ToString(), filterDisplayText);
                                        Main.GSOGridView.Columns[3].FilterInfo = dateFilter;
                                    }
                                    else if (stroke.ShifrOtN.HasValue)
                                    {
                                        foreach (var Prob in Probi)
                                        {
                                            var shif = Prob.Шифр.Split('-');
                                            if (Convert.ToInt32(shif[2]) >= Start2)
                                            {
                                                if (Convert.ToInt32(shif[2]) == Start2)
                                                {
                                                    if (Convert.ToInt32(shif[1]) >= Start1)
                                                    {
                                                        if (!Shifrs.Contains(Prob.Шифр)) Shifrs.Add(Prob.Шифр);
                                                    }
                                                }
                                                else if (!Shifrs.Contains(Prob.Шифр)) Shifrs.Add(Prob.Шифр);
                                            }
                                        }
                                        BinaryOperator[] BO = new BinaryOperator[Shifrs.Count];
                                        for (int i = 0; i < Shifrs.Count; i++)
                                        {
                                            BO[i] = new BinaryOperator("Шифр", Shifrs[i], BinaryOperatorType.Equal);
                                        }
                                        CriteriaOperator NewFilter = new GroupOperator(GroupOperatorType.Or, BO);
                                        string filterDisplayText = String.Format("[Шифр] от {0}", Analiz.Шифр + "-" + stroke.ShifrOtN.Value.ToString() + "-" + stroke.ShifrOtYear.Value.ToString());
                                        if (DisplayText != "")
                                            filterDisplayText = DisplayText + ", " + filterDisplayText;
                                        ColumnFilterInfo dateFilter = new ColumnFilterInfo(NewFilter.ToString(), filterDisplayText);
                                        Main.GSOGridView.Columns[3].FilterInfo = dateFilter;
                                    }
                                    else if (stroke.ShifrDoN.HasValue)
                                    {
                                        foreach (var Prob in Probi)
                                        {
                                            var shif = Prob.Шифр.Split('-');
                                            if (Convert.ToInt32(shif[2]) <= End2)
                                            {
                                                if (Convert.ToInt32(shif[2]) == End2)
                                                {
                                                    if (Convert.ToInt32(shif[1]) <= End1)
                                                    {
                                                        if (!Shifrs.Contains(Prob.Шифр)) Shifrs.Add(Prob.Шифр);
                                                    }
                                                }
                                                else if (!Shifrs.Contains(Prob.Шифр)) Shifrs.Add(Prob.Шифр);
                                            }
                                        }
                                        BinaryOperator[] BO = new BinaryOperator[Shifrs.Count];
                                        for (int i = 0; i < Shifrs.Count; i++)
                                        {
                                            BO[i] = new BinaryOperator("Шифр", Shifrs[i], BinaryOperatorType.Equal);
                                        }
                                        CriteriaOperator NewFilter = new GroupOperator(GroupOperatorType.Or, BO);
                                        string filterDisplayText = String.Format("[Шифр] до {0}", Analiz.Шифр + "-" + stroke.ShifrDoN.Value.ToString() + "-" + stroke.ShifrDoYear.Value.ToString());
                                        if (DisplayText != "")
                                            filterDisplayText = DisplayText + ", " + filterDisplayText;
                                        ColumnFilterInfo dateFilter = new ColumnFilterInfo(NewFilter.ToString(), filterDisplayText);
                                        Main.GSOGridView.Columns[3].FilterInfo = dateFilter;
                                    }
                                }
                                db.Dispose();
                            }
                        }
                        return;
                        //CriteriaOperator filter = new GroupOperator(GroupOperatorType.And,
                        //    new BinaryOperator("Дата", stroke.StartDate.Value, BinaryOperatorType.GreaterOrEqual),
                        //    new BinaryOperator("Дата", stroke.EndDate.Value, BinaryOperatorType.LessOrEqual));
                        //string filterDisplayText = String.Format("Дата между {0:d} и {1:d}", stroke.StartDate.Value.ToShortDateString(), stroke.EndDate.Value.ToShortDateString());
                        //ColumnFilterInfo dateFilter = new ColumnFilterInfo(filter.ToString(), filterDisplayText);
                        //Main.GSOGridView.Columns["Дата"].FilterInfo = dateFilter;
                    }
                }
            }
            catch (Exception ex)
            {                
                LogErrors Log = new LogErrors(ex.ToString());
            }
            finally
            {
                Main.GSOGridView.ColumnFilterChanged += Main.GSOGridView_ColumnFilterChanged;
            }
        }

        internal static void LoadFiltr()
        {
            try
            {
                using (DbDataContext db = new DbDataContext())
                {
                    var Filtr = db.VLKFiltr.Where(c => c.User == UserSettings.User).ToList();
                    foreach(var F in Filtr)
                    {
                        SelectedFiltr.Add(new FiltrVLKProbi
                        {
                            AnalizID = F.AnalizID,
                            OperKontrolID = F.OperKontrolID,
                            EndDate = F.EndDate,
                            ShifrDoN = F.ShifrDoN, 
                            ShifrDoYear = F.ShifrDoYear,
                            ShifrOtN = F.ShifrOtN,
                            ShifrOtYear = F.ShifrOtYear,
                            StartDate = F.StartDate
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
             }
        }
    }
}
