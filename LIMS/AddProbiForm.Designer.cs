﻿namespace LIMS
{
    partial class AddProbiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddProbiForm));
            this.CancelClickButton = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.OpredelyaemiiKomponent = new System.Windows.Forms.TextBox();
            this.Shifr1 = new System.Windows.Forms.Label();
            this.Shifr3r = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.Shifr3 = new System.Windows.Forms.TextBox();
            this.AddOperativniiKontrolCheckBox = new System.Windows.Forms.CheckBox();
            this.AddKontrolStabilnostiCheckBox = new System.Windows.Forms.CheckBox();
            this.AttestovannoeZnachenie = new System.Windows.Forms.NumericUpDown();
            this.Poluchil2 = new System.Windows.Forms.ComboBox();
            this.PoluchilName2 = new System.Windows.Forms.ComboBox();
            this.Poluchil1 = new System.Windows.Forms.ComboBox();
            this.PoluchilName1 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Vidal = new System.Windows.Forms.ComboBox();
            this.VidalName = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Ispolnitel = new System.Windows.Forms.ComboBox();
            this.IspolnitelName = new System.Windows.Forms.ComboBox();
            this.Date = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.ID = new System.Windows.Forms.Label();
            this.SaveButton = new DevExpress.XtraEditors.SimpleButton();
            this.KontroliruemiiObekt = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Shifr2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AttestovannoeZnachenie)).BeginInit();
            this.SuspendLayout();
            // 
            // CancelClickButton
            // 
            this.CancelClickButton.Location = new System.Drawing.Point(258, 302);
            this.CancelClickButton.Name = "CancelClickButton";
            this.CancelClickButton.Size = new System.Drawing.Size(75, 23);
            this.CancelClickButton.TabIndex = 17;
            this.CancelClickButton.Text = "Отмена";
            this.CancelClickButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Исполнитель:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Контролируемый обьект:";
            // 
            // OpredelyaemiiKomponent
            // 
            this.OpredelyaemiiKomponent.Location = new System.Drawing.Point(163, 92);
            this.OpredelyaemiiKomponent.Name = "OpredelyaemiiKomponent";
            this.OpredelyaemiiKomponent.Size = new System.Drawing.Size(322, 21);
            this.OpredelyaemiiKomponent.TabIndex = 4;
            this.OpredelyaemiiKomponent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // Shifr1
            // 
            this.Shifr1.AutoSize = true;
            this.Shifr1.Location = new System.Drawing.Point(162, 41);
            this.Shifr1.Name = "Shifr1";
            this.Shifr1.Size = new System.Drawing.Size(31, 13);
            this.Shifr1.TabIndex = 20;
            this.Shifr1.Text = "ХСН-";
            // 
            // Shifr3r
            // 
            this.Shifr3r.AutoSize = true;
            this.Shifr3r.Location = new System.Drawing.Point(258, 41);
            this.Shifr3r.Name = "Shifr3r";
            this.Shifr3r.Size = new System.Drawing.Size(11, 13);
            this.Shifr3r.TabIndex = 22;
            this.Shifr3r.Text = "-";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 95);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(145, 13);
            this.label20.TabIndex = 52;
            this.label20.Text = "Определяемый компонент:";
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.Shifr3);
            this.panelControl1.Controls.Add(this.AddOperativniiKontrolCheckBox);
            this.panelControl1.Controls.Add(this.AddKontrolStabilnostiCheckBox);
            this.panelControl1.Controls.Add(this.AttestovannoeZnachenie);
            this.panelControl1.Controls.Add(this.Poluchil2);
            this.panelControl1.Controls.Add(this.PoluchilName2);
            this.panelControl1.Controls.Add(this.Poluchil1);
            this.panelControl1.Controls.Add(this.PoluchilName1);
            this.panelControl1.Controls.Add(this.label10);
            this.panelControl1.Controls.Add(this.Vidal);
            this.panelControl1.Controls.Add(this.VidalName);
            this.panelControl1.Controls.Add(this.label7);
            this.panelControl1.Controls.Add(this.Ispolnitel);
            this.panelControl1.Controls.Add(this.IspolnitelName);
            this.panelControl1.Controls.Add(this.Date);
            this.panelControl1.Controls.Add(this.label9);
            this.panelControl1.Controls.Add(this.ID);
            this.panelControl1.Controls.Add(this.SaveButton);
            this.panelControl1.Controls.Add(this.KontroliruemiiObekt);
            this.panelControl1.Controls.Add(this.label27);
            this.panelControl1.Controls.Add(this.Shifr2);
            this.panelControl1.Controls.Add(this.label17);
            this.panelControl1.Controls.Add(this.label20);
            this.panelControl1.Controls.Add(this.Shifr3r);
            this.panelControl1.Controls.Add(this.Shifr1);
            this.panelControl1.Controls.Add(this.OpredelyaemiiKomponent);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.CancelClickButton);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(497, 337);
            this.panelControl1.TabIndex = 0;
            // 
            // Shifr3
            // 
            this.Shifr3.Location = new System.Drawing.Point(275, 38);
            this.Shifr3.Name = "Shifr3";
            this.Shifr3.Size = new System.Drawing.Size(44, 21);
            this.Shifr3.TabIndex = 2;
            // 
            // AddOperativniiKontrolCheckBox
            // 
            this.AddOperativniiKontrolCheckBox.AutoSize = true;
            this.AddOperativniiKontrolCheckBox.Checked = true;
            this.AddOperativniiKontrolCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AddOperativniiKontrolCheckBox.Location = new System.Drawing.Point(163, 279);
            this.AddOperativniiKontrolCheckBox.Name = "AddOperativniiKontrolCheckBox";
            this.AddOperativniiKontrolCheckBox.Size = new System.Drawing.Size(207, 17);
            this.AddOperativniiKontrolCheckBox.TabIndex = 15;
            this.AddOperativniiKontrolCheckBox.Text = "Добавить в оперативный контроль";
            this.AddOperativniiKontrolCheckBox.UseVisualStyleBackColor = true;
            this.AddOperativniiKontrolCheckBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // AddKontrolStabilnostiCheckBox
            // 
            this.AddKontrolStabilnostiCheckBox.AutoSize = true;
            this.AddKontrolStabilnostiCheckBox.Checked = true;
            this.AddKontrolStabilnostiCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AddKontrolStabilnostiCheckBox.Location = new System.Drawing.Point(163, 255);
            this.AddKontrolStabilnostiCheckBox.Name = "AddKontrolStabilnostiCheckBox";
            this.AddKontrolStabilnostiCheckBox.Size = new System.Drawing.Size(209, 17);
            this.AddKontrolStabilnostiCheckBox.TabIndex = 14;
            this.AddKontrolStabilnostiCheckBox.Text = "Добавить в контроль стабильности";
            this.AddKontrolStabilnostiCheckBox.UseVisualStyleBackColor = true;
            this.AddKontrolStabilnostiCheckBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // AttestovannoeZnachenie
            // 
            this.AttestovannoeZnachenie.DecimalPlaces = 6;
            this.AttestovannoeZnachenie.Location = new System.Drawing.Point(163, 119);
            this.AttestovannoeZnachenie.Maximum = new decimal(new int[] {
            -559939585,
            902409669,
            54,
            0});
            this.AttestovannoeZnachenie.Name = "AttestovannoeZnachenie";
            this.AttestovannoeZnachenie.Size = new System.Drawing.Size(118, 21);
            this.AttestovannoeZnachenie.TabIndex = 5;
            this.AttestovannoeZnachenie.ThousandsSeparator = true;
            this.AttestovannoeZnachenie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // Poluchil2
            // 
            this.Poluchil2.FormattingEnabled = true;
            this.Poluchil2.Items.AddRange(new object[] {
            "",
            "Инженер химического анализа",
            "Лаборант химического анализа",
            "Начальник ХАЛ"});
            this.Poluchil2.Location = new System.Drawing.Point(163, 227);
            this.Poluchil2.Name = "Poluchil2";
            this.Poluchil2.Size = new System.Drawing.Size(187, 21);
            this.Poluchil2.TabIndex = 12;
            this.Poluchil2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // PoluchilName2
            // 
            this.PoluchilName2.FormattingEnabled = true;
            this.PoluchilName2.Location = new System.Drawing.Point(356, 227);
            this.PoluchilName2.Name = "PoluchilName2";
            this.PoluchilName2.Size = new System.Drawing.Size(129, 21);
            this.PoluchilName2.TabIndex = 13;
            this.PoluchilName2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // Poluchil1
            // 
            this.Poluchil1.FormattingEnabled = true;
            this.Poluchil1.Items.AddRange(new object[] {
            "",
            "Инженер химического анализа",
            "Лаборант химического анализа",
            "Начальник ХАЛ"});
            this.Poluchil1.Location = new System.Drawing.Point(163, 200);
            this.Poluchil1.Name = "Poluchil1";
            this.Poluchil1.Size = new System.Drawing.Size(187, 21);
            this.Poluchil1.TabIndex = 10;
            this.Poluchil1.Text = "Лаборант химического анализа";
            this.Poluchil1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // PoluchilName1
            // 
            this.PoluchilName1.FormattingEnabled = true;
            this.PoluchilName1.Location = new System.Drawing.Point(356, 200);
            this.PoluchilName1.Name = "PoluchilName1";
            this.PoluchilName1.Size = new System.Drawing.Size(129, 21);
            this.PoluchilName1.TabIndex = 11;
            this.PoluchilName1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(103, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 106;
            this.label10.Text = "Получил:";
            // 
            // Vidal
            // 
            this.Vidal.FormattingEnabled = true;
            this.Vidal.Items.AddRange(new object[] {
            "",
            "Инженер химического анализа",
            "Начальник ХАЛ"});
            this.Vidal.Location = new System.Drawing.Point(163, 173);
            this.Vidal.Name = "Vidal";
            this.Vidal.Size = new System.Drawing.Size(187, 21);
            this.Vidal.TabIndex = 8;
            this.Vidal.Text = "Начальник ХАЛ";
            this.Vidal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // VidalName
            // 
            this.VidalName.FormattingEnabled = true;
            this.VidalName.Location = new System.Drawing.Point(356, 173);
            this.VidalName.Name = "VidalName";
            this.VidalName.Size = new System.Drawing.Size(129, 21);
            this.VidalName.TabIndex = 9;
            this.VidalName.Text = "";
            this.VidalName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(113, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 103;
            this.label7.Text = "Выдал:";
            // 
            // Ispolnitel
            // 
            this.Ispolnitel.FormattingEnabled = true;
            this.Ispolnitel.Items.AddRange(new object[] {
            "",
            "Инженер химического анализа",
            "Начальник ХАЛ"});
            this.Ispolnitel.Location = new System.Drawing.Point(163, 146);
            this.Ispolnitel.Name = "Ispolnitel";
            this.Ispolnitel.Size = new System.Drawing.Size(187, 21);
            this.Ispolnitel.TabIndex = 6;
            this.Ispolnitel.Text = "Начальник ХАЛ";
            this.Ispolnitel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // IspolnitelName
            // 
            this.IspolnitelName.FormattingEnabled = true;
            this.IspolnitelName.Location = new System.Drawing.Point(356, 146);
            this.IspolnitelName.Name = "IspolnitelName";
            this.IspolnitelName.Size = new System.Drawing.Size(129, 21);
            this.IspolnitelName.TabIndex = 7;
            this.IspolnitelName.Text = "";
            this.IspolnitelName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // Date
            // 
            this.Date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Date.Location = new System.Drawing.Point(163, 12);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(118, 21);
            this.Date.TabIndex = 0;
            this.Date.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 122);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 13);
            this.label9.TabIndex = 98;
            this.label9.Text = "Аттестованное значение:";
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ID.Location = new System.Drawing.Point(14, 190);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(0, 13);
            this.ID.TabIndex = 93;
            this.ID.Visible = false;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(177, 302);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 16;
            this.SaveButton.Text = "OK";
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // KontroliruemiiObekt
            // 
            this.KontroliruemiiObekt.Location = new System.Drawing.Point(163, 65);
            this.KontroliruemiiObekt.Name = "KontroliruemiiObekt";
            this.KontroliruemiiObekt.Size = new System.Drawing.Size(322, 21);
            this.KontroliruemiiObekt.TabIndex = 3;
            this.KontroliruemiiObekt.Text = "Нефть";
            this.KontroliruemiiObekt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(113, 41);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(44, 13);
            this.label27.TabIndex = 84;
            this.label27.Text = "Шифр: ";
            // 
            // Shifr2
            // 
            this.Shifr2.Location = new System.Drawing.Point(199, 38);
            this.Shifr2.Name = "Shifr2";
            this.Shifr2.Size = new System.Drawing.Size(53, 21);
            this.Shifr2.TabIndex = 1;
            this.Shifr2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Date_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(120, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 13);
            this.label17.TabIndex = 82;
            this.label17.Text = "Дата:";
            // 
            // AddProbiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 337);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "AddProbiForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Добавить";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.AddProbiForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AddProbiForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AttestovannoeZnachenie)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton CancelClickButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox OpredelyaemiiKomponent;
        private System.Windows.Forms.Label Shifr1;
        private System.Windows.Forms.Label Shifr3r;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.TextBox KontroliruemiiObekt;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox Shifr2;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.SimpleButton SaveButton;
        private System.Windows.Forms.Label ID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox Poluchil2;
        private System.Windows.Forms.ComboBox PoluchilName2;
        private System.Windows.Forms.ComboBox Poluchil1;
        private System.Windows.Forms.ComboBox PoluchilName1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox Vidal;
        private System.Windows.Forms.ComboBox VidalName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox Ispolnitel;
        private System.Windows.Forms.ComboBox IspolnitelName;
        private System.Windows.Forms.DateTimePicker Date;
        private System.Windows.Forms.NumericUpDown AttestovannoeZnachenie;
        private System.Windows.Forms.CheckBox AddOperativniiKontrolCheckBox;
        private System.Windows.Forms.CheckBox AddKontrolStabilnostiCheckBox;
        private System.Windows.Forms.TextBox Shifr3;
    }
}