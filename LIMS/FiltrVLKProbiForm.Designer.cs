﻿namespace LIMS
{
    partial class FiltrVLKProbiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FiltrVLKProbiForm));
            this.CancelClickButton = new DevExpress.XtraEditors.SimpleButton();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ShifrDo3 = new System.Windows.Forms.NumericUpDown();
            this.ShifrOt3 = new System.Windows.Forms.NumericUpDown();
            this.ShifrDo2 = new System.Windows.Forms.NumericUpDown();
            this.ShifrOt2 = new System.Windows.Forms.NumericUpDown();
            this.ShifrDoCheckBox = new System.Windows.Forms.CheckBox();
            this.ShifrOtCheckBox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ShifrDo1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ShifrOt1 = new System.Windows.Forms.Label();
            this.MounthComboBox = new System.Windows.Forms.ComboBox();
            this.DateRadioButton = new System.Windows.Forms.RadioButton();
            this.MounthRadioButton = new System.Windows.Forms.RadioButton();
            this.DateDo = new System.Windows.Forms.DateTimePicker();
            this.sdfsd = new System.Windows.Forms.Label();
            this.DateCheckBox = new System.Windows.Forms.CheckBox();
            this.dgdrvxer = new System.Windows.Forms.Label();
            this.ShifrCheckBox = new System.Windows.Forms.CheckBox();
            this.DateOt = new System.Windows.Forms.DateTimePicker();
            this.AnalizName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShifrDo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShifrOt3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShifrDo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShifrOt2)).BeginInit();
            this.SuspendLayout();
            // 
            // CancelClickButton
            // 
            this.CancelClickButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelClickButton.Location = new System.Drawing.Point(206, 222);
            this.CancelClickButton.Name = "CancelClickButton";
            this.CancelClickButton.Size = new System.Drawing.Size(75, 26);
            this.CancelClickButton.TabIndex = 15;
            this.CancelClickButton.Text = "Отмена";
            this.CancelClickButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(125, 222);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 26);
            this.OKButton.TabIndex = 14;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.groupBox2);
            this.panelControl1.Controls.Add(this.AnalizName);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelClickButton);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(403, 257);
            this.panelControl1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.ShifrDo3);
            this.groupBox2.Controls.Add(this.ShifrOt3);
            this.groupBox2.Controls.Add(this.ShifrDo2);
            this.groupBox2.Controls.Add(this.ShifrOt2);
            this.groupBox2.Controls.Add(this.ShifrDoCheckBox);
            this.groupBox2.Controls.Add(this.ShifrOtCheckBox);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.ShifrDo1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.ShifrOt1);
            this.groupBox2.Controls.Add(this.MounthComboBox);
            this.groupBox2.Controls.Add(this.DateRadioButton);
            this.groupBox2.Controls.Add(this.MounthRadioButton);
            this.groupBox2.Controls.Add(this.DateDo);
            this.groupBox2.Controls.Add(this.sdfsd);
            this.groupBox2.Controls.Add(this.DateCheckBox);
            this.groupBox2.Controls.Add(this.dgdrvxer);
            this.groupBox2.Controls.Add(this.ShifrCheckBox);
            this.groupBox2.Controls.Add(this.DateOt);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(379, 204);
            this.groupBox2.TabIndex = 37;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Отбор по:";
            // 
            // ShifrDo3
            // 
            this.ShifrDo3.Location = new System.Drawing.Point(240, 172);
            this.ShifrDo3.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.ShifrDo3.Name = "ShifrDo3";
            this.ShifrDo3.Size = new System.Drawing.Size(67, 21);
            this.ShifrDo3.TabIndex = 13;
            this.ShifrDo3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCheckBox_KeyDown);
            // 
            // ShifrOt3
            // 
            this.ShifrOt3.Location = new System.Drawing.Point(240, 145);
            this.ShifrOt3.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.ShifrOt3.Name = "ShifrOt3";
            this.ShifrOt3.Size = new System.Drawing.Size(67, 21);
            this.ShifrOt3.TabIndex = 10;
            this.ShifrOt3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCheckBox_KeyDown);
            // 
            // ShifrDo2
            // 
            this.ShifrDo2.Location = new System.Drawing.Point(150, 172);
            this.ShifrDo2.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.ShifrDo2.Name = "ShifrDo2";
            this.ShifrDo2.Size = new System.Drawing.Size(67, 21);
            this.ShifrDo2.TabIndex = 12;
            this.ShifrDo2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCheckBox_KeyDown);
            // 
            // ShifrOt2
            // 
            this.ShifrOt2.Location = new System.Drawing.Point(150, 145);
            this.ShifrOt2.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.ShifrOt2.Name = "ShifrOt2";
            this.ShifrOt2.Size = new System.Drawing.Size(67, 21);
            this.ShifrOt2.TabIndex = 9;
            this.ShifrOt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCheckBox_KeyDown);
            // 
            // ShifrDoCheckBox
            // 
            this.ShifrDoCheckBox.AutoSize = true;
            this.ShifrDoCheckBox.Checked = true;
            this.ShifrDoCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShifrDoCheckBox.Enabled = false;
            this.ShifrDoCheckBox.Location = new System.Drawing.Point(74, 173);
            this.ShifrDoCheckBox.Name = "ShifrDoCheckBox";
            this.ShifrDoCheckBox.Size = new System.Drawing.Size(43, 17);
            this.ShifrDoCheckBox.TabIndex = 11;
            this.ShifrDoCheckBox.Text = "до:";
            this.ShifrDoCheckBox.UseVisualStyleBackColor = true;
            this.ShifrDoCheckBox.CheckedChanged += new System.EventHandler(this.ShifrDoCheckBox_CheckedChanged);
            this.ShifrDoCheckBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCheckBox_KeyDown);
            // 
            // ShifrOtCheckBox
            // 
            this.ShifrOtCheckBox.AutoSize = true;
            this.ShifrOtCheckBox.Checked = true;
            this.ShifrOtCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShifrOtCheckBox.Enabled = false;
            this.ShifrOtCheckBox.Location = new System.Drawing.Point(74, 146);
            this.ShifrOtCheckBox.Name = "ShifrOtCheckBox";
            this.ShifrOtCheckBox.Size = new System.Drawing.Size(42, 17);
            this.ShifrOtCheckBox.TabIndex = 8;
            this.ShifrOtCheckBox.Text = "от:";
            this.ShifrOtCheckBox.UseVisualStyleBackColor = true;
            this.ShifrOtCheckBox.CheckedChanged += new System.EventHandler(this.ShifrOtCheckBox_CheckedChanged);
            this.ShifrOtCheckBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCheckBox_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Location = new System.Drawing.Point(223, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "-";
            // 
            // ShifrDo1
            // 
            this.ShifrDo1.AutoSize = true;
            this.ShifrDo1.Enabled = false;
            this.ShifrDo1.Location = new System.Drawing.Point(120, 174);
            this.ShifrDo1.Name = "ShifrDo1";
            this.ShifrDo1.Size = new System.Drawing.Size(18, 13);
            this.ShifrDo1.TabIndex = 49;
            this.ShifrDo1.Text = "С-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(223, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 13);
            this.label2.TabIndex = 45;
            this.label2.Text = "-";
            // 
            // ShifrOt1
            // 
            this.ShifrOt1.AutoSize = true;
            this.ShifrOt1.Enabled = false;
            this.ShifrOt1.Location = new System.Drawing.Point(120, 147);
            this.ShifrOt1.Name = "ShifrOt1";
            this.ShifrOt1.Size = new System.Drawing.Size(18, 13);
            this.ShifrOt1.TabIndex = 43;
            this.ShifrOt1.Text = "С-";
            // 
            // MounthComboBox
            // 
            this.MounthComboBox.Enabled = false;
            this.MounthComboBox.FormattingEnabled = true;
            this.MounthComboBox.Location = new System.Drawing.Point(205, 18);
            this.MounthComboBox.Name = "MounthComboBox";
            this.MounthComboBox.Size = new System.Drawing.Size(163, 21);
            this.MounthComboBox.TabIndex = 3;
            this.MounthComboBox.SelectedIndexChanged += new System.EventHandler(this.MounthComboBox_SelectedIndexChanged);
            this.MounthComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCheckBox_KeyDown);
            // 
            // DateRadioButton
            // 
            this.DateRadioButton.AutoSize = true;
            this.DateRadioButton.Checked = true;
            this.DateRadioButton.Enabled = false;
            this.DateRadioButton.Location = new System.Drawing.Point(74, 43);
            this.DateRadioButton.Name = "DateRadioButton";
            this.DateRadioButton.Size = new System.Drawing.Size(125, 17);
            this.DateRadioButton.TabIndex = 4;
            this.DateRadioButton.TabStop = true;
            this.DateRadioButton.Text = "Произвольная дата";
            this.DateRadioButton.UseVisualStyleBackColor = true;
            this.DateRadioButton.CheckedChanged += new System.EventHandler(this.DateRadioButton_CheckedChanged);
            // 
            // MounthRadioButton
            // 
            this.MounthRadioButton.AutoSize = true;
            this.MounthRadioButton.Enabled = false;
            this.MounthRadioButton.Location = new System.Drawing.Point(74, 20);
            this.MounthRadioButton.Name = "MounthRadioButton";
            this.MounthRadioButton.Size = new System.Drawing.Size(56, 17);
            this.MounthRadioButton.TabIndex = 2;
            this.MounthRadioButton.Text = "Месяц";
            this.MounthRadioButton.UseVisualStyleBackColor = true;
            this.MounthRadioButton.CheckedChanged += new System.EventHandler(this.MounthRadioButton_CheckedChanged);
            // 
            // DateDo
            // 
            this.DateDo.Enabled = false;
            this.DateDo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateDo.Location = new System.Drawing.Point(205, 91);
            this.DateDo.Name = "DateDo";
            this.DateDo.ShowCheckBox = true;
            this.DateDo.Size = new System.Drawing.Size(104, 21);
            this.DateDo.TabIndex = 6;
            this.DateDo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCheckBox_KeyDown);
            // 
            // sdfsd
            // 
            this.sdfsd.AutoSize = true;
            this.sdfsd.Location = new System.Drawing.Point(180, 94);
            this.sdfsd.Name = "sdfsd";
            this.sdfsd.Size = new System.Drawing.Size(19, 13);
            this.sdfsd.TabIndex = 37;
            this.sdfsd.Text = "по";
            // 
            // DateCheckBox
            // 
            this.DateCheckBox.AutoSize = true;
            this.DateCheckBox.Location = new System.Drawing.Point(6, 20);
            this.DateCheckBox.Name = "DateCheckBox";
            this.DateCheckBox.Size = new System.Drawing.Size(52, 17);
            this.DateCheckBox.TabIndex = 1;
            this.DateCheckBox.Text = "Дата";
            this.DateCheckBox.UseVisualStyleBackColor = true;
            this.DateCheckBox.CheckedChanged += new System.EventHandler(this.DateCheckBox_CheckedChanged);
            this.DateCheckBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCheckBox_KeyDown);
            // 
            // dgdrvxer
            // 
            this.dgdrvxer.AutoSize = true;
            this.dgdrvxer.Location = new System.Drawing.Point(181, 66);
            this.dgdrvxer.Name = "dgdrvxer";
            this.dgdrvxer.Size = new System.Drawing.Size(12, 13);
            this.dgdrvxer.TabIndex = 34;
            this.dgdrvxer.Text = "c";
            // 
            // ShifrCheckBox
            // 
            this.ShifrCheckBox.AutoSize = true;
            this.ShifrCheckBox.Location = new System.Drawing.Point(6, 123);
            this.ShifrCheckBox.Name = "ShifrCheckBox";
            this.ShifrCheckBox.Size = new System.Drawing.Size(91, 17);
            this.ShifrCheckBox.TabIndex = 7;
            this.ShifrCheckBox.Text = "Шифр пробы";
            this.ShifrCheckBox.UseVisualStyleBackColor = true;
            this.ShifrCheckBox.CheckedChanged += new System.EventHandler(this.ShifrCheckBox_CheckedChanged);
            this.ShifrCheckBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCheckBox_KeyDown);
            // 
            // DateOt
            // 
            this.DateOt.Enabled = false;
            this.DateOt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateOt.Location = new System.Drawing.Point(205, 63);
            this.DateOt.Name = "DateOt";
            this.DateOt.ShowCheckBox = true;
            this.DateOt.Size = new System.Drawing.Size(104, 21);
            this.DateOt.TabIndex = 5;
            this.DateOt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateCheckBox_KeyDown);
            // 
            // AnalizName
            // 
            this.AnalizName.AutoSize = true;
            this.AnalizName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AnalizName.Location = new System.Drawing.Point(372, 170);
            this.AnalizName.Name = "AnalizName";
            this.AnalizName.Size = new System.Drawing.Size(0, 13);
            this.AnalizName.TabIndex = 33;
            this.AnalizName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FiltrVLKProbiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelClickButton;
            this.ClientSize = new System.Drawing.Size(403, 257);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FiltrVLKProbiForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ ВЛК Фильтр";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FiltrVLKProbiForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShifrDo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShifrOt3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShifrDo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShifrOt2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton CancelClickButton;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.DateTimePicker DateOt;
        private System.Windows.Forms.Label dgdrvxer;
        private System.Windows.Forms.Label AnalizName;
        private System.Windows.Forms.CheckBox DateCheckBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox ShifrCheckBox;
        private System.Windows.Forms.Label sdfsd;
        private System.Windows.Forms.DateTimePicker DateDo;
        private System.Windows.Forms.RadioButton DateRadioButton;
        private System.Windows.Forms.RadioButton MounthRadioButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label ShifrDo1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label ShifrOt1;
        private System.Windows.Forms.ComboBox MounthComboBox;
        private System.Windows.Forms.CheckBox ShifrDoCheckBox;
        private System.Windows.Forms.CheckBox ShifrOtCheckBox;
        private System.Windows.Forms.NumericUpDown ShifrDo3;
        private System.Windows.Forms.NumericUpDown ShifrOt3;
        private System.Windows.Forms.NumericUpDown ShifrDo2;
        private System.Windows.Forms.NumericUpDown ShifrOt2;
    }
}