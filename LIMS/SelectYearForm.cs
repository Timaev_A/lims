﻿using System;
using System.Windows.Forms;

namespace LIMS
{
    public partial class SelectYearForm : DevExpress.XtraEditors.XtraForm
    {       
        public SelectYearForm()
        {
            InitializeComponent();
            try
            {
                Year.Text = DateTime.Now.Year.ToString();                
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    GlobalStatic.BuferInt = Convert.ToInt32(Year.Text);
                }
                catch { MessageBox.Show("Год указан неправильно!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }
                Close();              
            }
            catch (Exception ex)
            {
                LogErrors Log = new LogErrors(ex.ToString());
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }         

        private void CancelButton_Click(object sender, EventArgs e)
        {
            GlobalStatic.BuferInt = 0;
            this.Close();
        }

        private void SelectYearForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
