﻿using System;

namespace LIMS
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.printPreviewStaticItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.printPreviewStaticItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.TabControlMain = new DevExpress.XtraTab.XtraTabControl();
            this.Reactiv = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.ReactivPanel = new DevExpress.XtraEditors.PanelControl();
            this.ReactivDateGetTip = new System.Windows.Forms.ComboBox();
            this.ReactivDateIzgotTip = new System.Windows.Forms.ComboBox();
            this.ReactivNaznachenie = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.ReactivN = new System.Windows.Forms.TextBox();
            this.ReactivCopyButton = new DevExpress.XtraEditors.SimpleButton();
            this.ReactivDateIzgot = new System.Windows.Forms.DateTimePicker();
            this.ReactivID = new System.Windows.Forms.Label();
            this.ReactivProdlenieSroka = new System.Windows.Forms.TextBox();
            this.ReactivSrokGodnostiDate = new System.Windows.Forms.DateTimePicker();
            this.ReactivSrokGodnostiMounth = new System.Windows.Forms.NumericUpDown();
            this.ReactivSrokGodnoctiTip = new System.Windows.Forms.ComboBox();
            this.ReactivSrokGodnostiText = new System.Windows.Forms.TextBox();
            this.ReactivEdIzm = new System.Windows.Forms.ComboBox();
            this.ReactivClass = new System.Windows.Forms.ComboBox();
            this.ReactivNPartii = new System.Windows.Forms.TextBox();
            this.ReactivND = new System.Windows.Forms.TextBox();
            this.ReactivName = new System.Windows.Forms.TextBox();
            this.ReactivDateGet = new System.Windows.Forms.DateTimePicker();
            this.ReactivKol_vo = new System.Windows.Forms.NumericUpDown();
            this.ReactivEditCheckBox = new System.Windows.Forms.CheckBox();
            this.ReactivNewButton = new DevExpress.XtraEditors.SimpleButton();
            this.ReactivSaveButton = new DevExpress.XtraEditors.SimpleButton();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ReactivGrid = new DevExpress.XtraGrid.GridControl();
            this.ReactivGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ReactivPanelControl = new DevExpress.XtraEditors.PanelControl();
            this.ReactivCellMergeButton = new DevExpress.XtraEditors.SimpleButton();
            this.ReactivVisibleSpisatButton = new DevExpress.XtraEditors.SimpleButton();
            this.ReactivCheckColumnButton = new DevExpress.XtraEditors.SimpleButton();
            this.RefreshReactivButton = new DevExpress.XtraEditors.SimpleButton();
            this.ReactivPrintButton = new DevExpress.XtraEditors.SimpleButton();
            this.ReactivDeleteButton = new DevExpress.XtraEditors.SimpleButton();
            this.Personal = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.PersonalPanel = new DevExpress.XtraEditors.PanelControl();
            this.PersonalZnaniaOTiPBTip = new System.Windows.Forms.ComboBox();
            this.PersonalAttestechiaTip = new System.Windows.Forms.ComboBox();
            this.PersonalGasTip = new System.Windows.Forms.ComboBox();
            this.PersonalSrokPodvergKvalifTip = new System.Windows.Forms.ComboBox();
            this.PersonalSrokZnaniiOTiPBTip = new System.Windows.Forms.ComboBox();
            this.PersonalSrokAttestachiiTip = new System.Windows.Forms.ComboBox();
            this.PersonalSrokGasTip = new System.Windows.Forms.ComboBox();
            this.PersonalN = new System.Windows.Forms.TextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.PersonalObrasovanie = new System.Windows.Forms.TextBox();
            this.PersonalCopyButton = new DevExpress.XtraEditors.SimpleButton();
            this.PersonalPovishKvalif = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.PersonalSrokZnaniiOTiPBDate = new System.Windows.Forms.DateTimePicker();
            this.PersonalZnaniaOTiPB = new System.Windows.Forms.DateTimePicker();
            this.PersonalSrokAttestachiiDate = new System.Windows.Forms.DateTimePicker();
            this.PersonalAttestechia = new System.Windows.Forms.DateTimePicker();
            this.PersonalDolgnost = new System.Windows.Forms.TextBox();
            this.PersonalObrasovaniePoln = new System.Windows.Forms.TextBox();
            this.PersonalSrokGasDate = new System.Windows.Forms.DateTimePicker();
            this.PersonalGas = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.PersonalSrokAttestachiiKol = new System.Windows.Forms.NumericUpDown();
            this.PersonalSrokZnaniiOTiPBKol = new System.Windows.Forms.NumericUpDown();
            this.PersonalSrokGasKol = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.PersonalID = new System.Windows.Forms.Label();
            this.PersonalDopolnitelno = new System.Windows.Forms.TextBox();
            this.PersonalOpitNumber = new System.Windows.Forms.NumericUpDown();
            this.PersonalOpitLet = new System.Windows.Forms.ComboBox();
            this.PersonalName = new System.Windows.Forms.TextBox();
            this.PersonalSrokPodvergKvalif = new System.Windows.Forms.DateTimePicker();
            this.PersonalEditCheckBox = new System.Windows.Forms.CheckBox();
            this.PersonalNewButton = new DevExpress.XtraEditors.SimpleButton();
            this.PersonalSaveButton = new DevExpress.XtraEditors.SimpleButton();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.PersonalGrid = new DevExpress.XtraGrid.GridControl();
            this.PersonalGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.PersonalCellMergeButton = new DevExpress.XtraEditors.SimpleButton();
            this.PersonalUvolenButton = new DevExpress.XtraEditors.SimpleButton();
            this.PersonalColumnCheckButton = new DevExpress.XtraEditors.SimpleButton();
            this.PersonalUpdateButton = new DevExpress.XtraEditors.SimpleButton();
            this.PersonalPrintButton = new DevExpress.XtraEditors.SimpleButton();
            this.PersonalDeleteButton = new DevExpress.XtraEditors.SimpleButton();
            this.NTD = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.NTDPanel = new DevExpress.XtraEditors.PanelControl();
            this.NTDSrokDeisrviaTip = new System.Windows.Forms.ComboBox();
            this.NTDDateAktualizTip = new System.Windows.Forms.ComboBox();
            this.NTDDateUtvergdeniaTip = new System.Windows.Forms.ComboBox();
            this.NTDGroup = new System.Windows.Forms.ComboBox();
            this.NTDUroven = new System.Windows.Forms.ComboBox();
            this.NTDN = new System.Windows.Forms.ComboBox();
            this.NTDND1 = new System.Windows.Forms.ComboBox();
            this.NTDND2 = new System.Windows.Forms.TextBox();
            this.label132 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.NTDPrimechanie = new System.Windows.Forms.TextBox();
            this.NTDKontrolMesto = new System.Windows.Forms.ComboBox();
            this.NTDRabMesto = new System.Windows.Forms.ComboBox();
            this.NTDCopyButton = new DevExpress.XtraEditors.SimpleButton();
            this.NTDSrokDeisrvia = new System.Windows.Forms.DateTimePicker();
            this.NTDDateAktualiz = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.NTDName = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.NTDDateUtvergdenia = new System.Windows.Forms.DateTimePicker();
            this.NTDID = new System.Windows.Forms.Label();
            this.NTDKontrolKolvo = new System.Windows.Forms.NumericUpDown();
            this.NTDRegistN = new System.Windows.Forms.TextBox();
            this.NTDRabKolvo = new System.Windows.Forms.NumericUpDown();
            this.NTDEditCheckBox = new System.Windows.Forms.CheckBox();
            this.NTDNewButton = new DevExpress.XtraEditors.SimpleButton();
            this.NTDSaveButton = new DevExpress.XtraEditors.SimpleButton();
            this.label31 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.NTDGrid = new DevExpress.XtraGrid.GridControl();
            this.NTDGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.NTDCellMergeButton = new DevExpress.XtraEditors.SimpleButton();
            this.NTDArhivButton = new DevExpress.XtraEditors.SimpleButton();
            this.NTDFiltrButton = new DevExpress.XtraEditors.SimpleButton();
            this.NTDColumnCheckButton = new DevExpress.XtraEditors.SimpleButton();
            this.NTDUpdateButton = new DevExpress.XtraEditors.SimpleButton();
            this.NTDPrintButton = new DevExpress.XtraEditors.SimpleButton();
            this.NTDDeleteButton = new DevExpress.XtraEditors.SimpleButton();
            this.SI = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.SIPanel = new DevExpress.XtraEditors.PanelControl();
            this.SIPravoSobstvennosti = new System.Windows.Forms.ComboBox();
            this.label112 = new System.Windows.Forms.Label();
            this.SIDataPoverki = new System.Windows.Forms.TextBox();
            this.SIDataPosledTOTip = new System.Windows.Forms.ComboBox();
            this.SIDataPoverkiTip = new System.Windows.Forms.ComboBox();
            this.SIDataSledTOKol = new System.Windows.Forms.NumericUpDown();
            this.SIDateSledPoverkiKol = new System.Windows.Forms.NumericUpDown();
            this.SIDateSledPoverkiTip = new System.Windows.Forms.ComboBox();
            this.SIDataSledTOTip = new System.Windows.Forms.ComboBox();
            this.SIMestoUstanovki = new System.Windows.Forms.ComboBox();
            this.SISrokDeystv = new System.Windows.Forms.ComboBox();
            this.SIDataSledTODate = new System.Windows.Forms.DateTimePicker();
            this.SIDataPosledTO = new System.Windows.Forms.DateTimePicker();
            this.label45 = new System.Windows.Forms.Label();
            this.SIN = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.SITip = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.SIZavodNomer = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.SIGod = new System.Windows.Forms.TextBox();
            this.SIInventarNomer = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.SICopyButton = new DevExpress.XtraEditors.SimpleButton();
            this.SIPrimechanie = new System.Windows.Forms.TextBox();
            this.SISvidetelstvoOPoverke = new System.Windows.Forms.TextBox();
            this.SIClassTochnosti = new System.Windows.Forms.TextBox();
            this.SIVvodVEkspluatachiu = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.SIIzgotovitel = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.SINameSI = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.SIDateSledPoverkiDate = new System.Windows.Forms.DateTimePicker();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.SIID = new System.Windows.Forms.Label();
            this.SIDiapozonIzm = new System.Windows.Forms.TextBox();
            this.SINameProd = new System.Windows.Forms.TextBox();
            this.SIEditCheckBox = new System.Windows.Forms.CheckBox();
            this.SINewButton = new DevExpress.XtraEditors.SimpleButton();
            this.SISaveButton = new DevExpress.XtraEditors.SimpleButton();
            this.label52 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.SIGrid = new DevExpress.XtraGrid.GridControl();
            this.SIGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.SICellMergeButton = new DevExpress.XtraEditors.SimpleButton();
            this.SIArhivButton = new DevExpress.XtraEditors.SimpleButton();
            this.SIColumnCheckButton = new DevExpress.XtraEditors.SimpleButton();
            this.SIUpdateButton = new DevExpress.XtraEditors.SimpleButton();
            this.SIPrintButton = new DevExpress.XtraEditors.SimpleButton();
            this.SIDeleteButton = new DevExpress.XtraEditors.SimpleButton();
            this.IO = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.IOPanel = new DevExpress.XtraEditors.PanelControl();
            this.IOPravoSobstvennosti = new System.Windows.Forms.ComboBox();
            this.label113 = new System.Windows.Forms.Label();
            this.IODataINAttestachiiTip = new System.Windows.Forms.ComboBox();
            this.IODataPosledTOTip = new System.Windows.Forms.ComboBox();
            this.IODataSledTOKol = new System.Windows.Forms.NumericUpDown();
            this.IODataSledTOTip = new System.Windows.Forms.ComboBox();
            this.IODateSledAttistachiiKol = new System.Windows.Forms.NumericUpDown();
            this.IODateSledAttistachiiTip = new System.Windows.Forms.ComboBox();
            this.IOMestoUstanovkiIHranenia = new System.Windows.Forms.ComboBox();
            this.IOPeriodich = new System.Windows.Forms.ComboBox();
            this.IODataSledTODate = new System.Windows.Forms.DateTimePicker();
            this.IODataPosledTO = new System.Windows.Forms.DateTimePicker();
            this.IODataINAttestachii = new System.Windows.Forms.DateTimePicker();
            this.label65 = new System.Windows.Forms.Label();
            this.IOTipMarka = new System.Windows.Forms.TextBox();
            this.IOZavodskoy = new System.Windows.Forms.TextBox();
            this.label102 = new System.Windows.Forms.Label();
            this.IOInventarNomer = new System.Windows.Forms.TextBox();
            this.label101 = new System.Windows.Forms.Label();
            this.IOGodVyp = new System.Windows.Forms.TextBox();
            this.IONomerDokumentaObAttes = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.IOCopyButton = new DevExpress.XtraEditors.SimpleButton();
            this.label80 = new System.Windows.Forms.Label();
            this.ION = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.IOPrimechanie = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.IOGodVvodaVEkspluatachiu = new System.Windows.Forms.TextBox();
            this.IOIzgotovitel = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.IOName = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.IONameVidovIspitanii = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.IODateSledAttistachiiDate = new System.Windows.Forms.DateTimePicker();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.IOID = new System.Windows.Forms.Label();
            this.IOTehnichHaracter = new System.Windows.Forms.TextBox();
            this.IOEditCheckBox = new System.Windows.Forms.CheckBox();
            this.IONewButton = new DevExpress.XtraEditors.SimpleButton();
            this.IOSaveButton = new DevExpress.XtraEditors.SimpleButton();
            this.label70 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.IOGrid = new DevExpress.XtraGrid.GridControl();
            this.IOGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.IOCellMergeButton = new DevExpress.XtraEditors.SimpleButton();
            this.IOArhivButton = new DevExpress.XtraEditors.SimpleButton();
            this.IOColumnCheckButton = new DevExpress.XtraEditors.SimpleButton();
            this.IOUpdateButton = new DevExpress.XtraEditors.SimpleButton();
            this.IOPrintButton = new DevExpress.XtraEditors.SimpleButton();
            this.IODeleteButton = new DevExpress.XtraEditors.SimpleButton();
            this.VO = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.VOPanel = new DevExpress.XtraEditors.PanelControl();
            this.VOPravoSobstvennosti = new System.Windows.Forms.ComboBox();
            this.label114 = new System.Windows.Forms.Label();
            this.VODataPoslTOTip = new System.Windows.Forms.ComboBox();
            this.VODateProverkiTehnicheskogoSostoianiaTip = new System.Windows.Forms.ComboBox();
            this.VOSledProvTOKol = new System.Windows.Forms.NumericUpDown();
            this.VOSledProvTOTip = new System.Windows.Forms.ComboBox();
            this.VODateSledProverkiKol = new System.Windows.Forms.NumericUpDown();
            this.VODateSledProverkiTip = new System.Windows.Forms.ComboBox();
            this.VOMestoUstanovkiIHranenia = new System.Windows.Forms.ComboBox();
            this.VOSledProvTODate = new System.Windows.Forms.DateTimePicker();
            this.VODataPoslTO = new System.Windows.Forms.DateTimePicker();
            this.label107 = new System.Windows.Forms.Label();
            this.VOTipMarka = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.VOZavodNomer = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.VOGodVypuska = new System.Windows.Forms.TextBox();
            this.VOInventarNomer = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.VOPrimechanie = new System.Windows.Forms.TextBox();
            this.VOCopyButton = new DevExpress.XtraEditors.SimpleButton();
            this.VODateProverkiTehnicheskogoSostoiania = new System.Windows.Forms.DateTimePicker();
            this.label69 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.VON = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.VONaznachenie = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.VOGodVvodaVEkspltatechiu = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.VOIzgotovitel = new System.Windows.Forms.TextBox();
            this.VODateSledProverkiDate = new System.Windows.Forms.DateTimePicker();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.VOID = new System.Windows.Forms.Label();
            this.VOName = new System.Windows.Forms.TextBox();
            this.VOEditCheckBox = new System.Windows.Forms.CheckBox();
            this.VONewButton = new DevExpress.XtraEditors.SimpleButton();
            this.VOSaveButton = new DevExpress.XtraEditors.SimpleButton();
            this.label90 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label94 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.VOGrid = new DevExpress.XtraGrid.GridControl();
            this.VOGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.VOCellMergeButton = new DevExpress.XtraEditors.SimpleButton();
            this.VOArhivButton = new DevExpress.XtraEditors.SimpleButton();
            this.VOColumnCheckButton = new DevExpress.XtraEditors.SimpleButton();
            this.VOUpdateButton = new DevExpress.XtraEditors.SimpleButton();
            this.VOPrintButton = new DevExpress.XtraEditors.SimpleButton();
            this.VODeleteButton = new DevExpress.XtraEditors.SimpleButton();
            this.VLK = new DevExpress.XtraTab.XtraTabPage();
            this.GSOGrid = new DevExpress.XtraGrid.GridControl();
            this.GSOGridView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.CloseAllTabsButton = new DevExpress.XtraEditors.SimpleButton();
            this.FiltrButton = new DevExpress.XtraEditors.SimpleButton();
            this.VLKCellMergeButton = new DevExpress.XtraEditors.SimpleButton();
            this.DiagramButton = new DevExpress.XtraEditors.SimpleButton();
            this.VLKUpdateButton = new DevExpress.XtraEditors.SimpleButton();
            this.VLKPrintButton = new DevExpress.XtraEditors.SimpleButton();
            this.AddAnalizProbiButton = new DevExpress.XtraEditors.SimpleButton();
            this.GSOSpisokTab = new DevExpress.XtraTab.XtraTabControl();
            this.MainGSOTab = new DevExpress.XtraTab.XtraTabPage();
            this.Nastroiki = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.AddUser = new DevExpress.XtraEditors.SimpleButton();
            this.UseSkinsCheckBox = new System.Windows.Forms.CheckBox();
            this.SkinComboBox = new System.Windows.Forms.ComboBox();
            this.label133 = new System.Windows.Forms.Label();
            this.ContextMenuStripMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Cut = new System.Windows.Forms.ToolStripMenuItem();
            this.Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.Clear = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.PlusMinus = new System.Windows.Forms.ToolStripMenuItem();
            this.Paste0 = new System.Windows.Forms.ToolStripMenuItem();
            this.Paste2 = new System.Windows.Forms.ToolStripMenuItem();
            this.Paste3 = new System.Windows.Forms.ToolStripMenuItem();
            this.PasteProcent = new System.Windows.Forms.ToolStripMenuItem();
            this.PasteGradus = new System.Windows.Forms.ToolStripMenuItem();
            this.PasteFaringeit = new System.Windows.Forms.ToolStripMenuItem();
            this.Pasteмм2с = new System.Windows.Forms.ToolStripMenuItem();
            this.Pasteмм2с2 = new System.Windows.Forms.ToolStripMenuItem();
            this.Pasteмгдм3 = new System.Windows.Forms.ToolStripMenuItem();
            this.PasteКгм3 = new System.Windows.Forms.ToolStripMenuItem();
            this.Pasteмгм3 = new System.Windows.Forms.ToolStripMenuItem();
            this.Pastecм3 = new System.Windows.Forms.ToolStripMenuItem();
            this.Pasteдм3 = new System.Windows.Forms.ToolStripMenuItem();
            this.Pasteм3 = new System.Windows.Forms.ToolStripMenuItem();
            this.PasteмкСмсм = new System.Windows.Forms.ToolStripMenuItem();
            this.Pasteмкгг = new System.Windows.Forms.ToolStripMenuItem();
            this.Pasteppm = new System.Windows.Forms.ToolStripMenuItem();
            this.PasteрН = new System.Windows.Forms.ToolStripMenuItem();
            this.Loader = new DevExpress.XtraWaitForm.ProgressPanel();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlMain)).BeginInit();
            this.TabControlMain.SuspendLayout();
            this.Reactiv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivPanel)).BeginInit();
            this.ReactivPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivSrokGodnostiMounth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivKol_vo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivPanelControl)).BeginInit();
            this.ReactivPanelControl.SuspendLayout();
            this.Personal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalPanel)).BeginInit();
            this.PersonalPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalSrokAttestachiiKol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalSrokZnaniiOTiPBKol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalSrokGasKol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalOpitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.NTD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NTDPanel)).BeginInit();
            this.NTDPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NTDKontrolKolvo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NTDRabKolvo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NTDGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NTDGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            this.SI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SIPanel)).BeginInit();
            this.SIPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SIDataSledTOKol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SIDateSledPoverkiKol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SIGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SIGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            this.IO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IOPanel)).BeginInit();
            this.IOPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IODataSledTOKol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IODateSledAttistachiiKol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IOGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IOGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            this.VO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VOPanel)).BeginInit();
            this.VOPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VOSledProvTOKol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VODateSledProverkiKol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VOGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VOGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            this.VLK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GSOGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GSOGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GSOSpisokTab)).BeginInit();
            this.GSOSpisokTab.SuspendLayout();
            this.Nastroiki.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            this.ContextMenuStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.AutoHideEmptyItems = true;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.printPreviewStaticItem1,
            this.barStaticItem1,
            this.barButtonItem1,
            this.printPreviewStaticItem2});
            this.ribbonControl1.Location = new System.Drawing.Point(1, 23);
            this.ribbonControl1.MaxItemId = 124;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.repositoryItemZoomTrackBar1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl1.Size = new System.Drawing.Size(977, 116);
            this.ribbonControl1.TransparentEditors = true;
            // 
            // printPreviewStaticItem1
            // 
            this.printPreviewStaticItem1.Caption = "(нет)";
            this.printPreviewStaticItem1.Id = 117;
            this.printPreviewStaticItem1.LeftIndent = 1;
            this.printPreviewStaticItem1.Name = "printPreviewStaticItem1";
            this.printPreviewStaticItem1.RightIndent = 1;
            this.printPreviewStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.printPreviewStaticItem1.Type = "PageOfPages";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Id = 118;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInRuntime;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonItem1.Enabled = false;
            this.barButtonItem1.Id = 121;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInRuntime;
            // 
            // printPreviewStaticItem2
            // 
            this.printPreviewStaticItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.printPreviewStaticItem2.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.printPreviewStaticItem2.Caption = "100%";
            this.printPreviewStaticItem2.Id = 122;
            this.printPreviewStaticItem2.Name = "printPreviewStaticItem2";
            this.printPreviewStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            this.printPreviewStaticItem2.Type = "ZoomFactorText";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "ribbonPageGroup1";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // repositoryItemZoomTrackBar1
            // 
            this.repositoryItemZoomTrackBar1.Alignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemZoomTrackBar1.AllowFocused = false;
            this.repositoryItemZoomTrackBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemZoomTrackBar1.Maximum = 180;
            this.repositoryItemZoomTrackBar1.Middle = 90;
            this.repositoryItemZoomTrackBar1.Name = "repositoryItemZoomTrackBar1";
            this.repositoryItemZoomTrackBar1.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            // 
            // TabControlMain
            // 
            this.TabControlMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.TabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControlMain.Location = new System.Drawing.Point(0, 0);
            this.TabControlMain.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.TabControlMain.Name = "TabControlMain";
            this.TabControlMain.SelectedTabPage = this.Reactiv;
            this.TabControlMain.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.TabControlMain.Size = new System.Drawing.Size(994, 731);
            this.TabControlMain.TabIndex = 1;
            this.TabControlMain.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Reactiv,
            this.Personal,
            this.NTD,
            this.SI,
            this.IO,
            this.VO,
            this.VLK,
            this.Nastroiki});
            this.TabControlMain.Click += new System.EventHandler(this.TabControlMain_Click);
            // 
            // Reactiv
            // 
            this.Reactiv.Controls.Add(this.splitContainerControl6);
            this.Reactiv.Name = "Reactiv";
            this.Reactiv.Size = new System.Drawing.Size(988, 703);
            this.Reactiv.Text = "Реактивы";
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl6.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.ReactivPanel);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel1.SizeChanged += new System.EventHandler(this.splitContainerControl6_Panel1_SizeChanged);
            this.splitContainerControl6.Panel2.Controls.Add(this.ReactivGrid);
            this.splitContainerControl6.Panel2.Controls.Add(this.ReactivPanelControl);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(988, 703);
            this.splitContainerControl6.SplitterPosition = 313;
            this.splitContainerControl6.TabIndex = 1;
            this.splitContainerControl6.Text = "splitContainerControl6";
            // 
            // ReactivPanel
            // 
            this.ReactivPanel.Controls.Add(this.ReactivDateGetTip);
            this.ReactivPanel.Controls.Add(this.ReactivDateIzgotTip);
            this.ReactivPanel.Controls.Add(this.ReactivNaznachenie);
            this.ReactivPanel.Controls.Add(this.label61);
            this.ReactivPanel.Controls.Add(this.ReactivN);
            this.ReactivPanel.Controls.Add(this.ReactivCopyButton);
            this.ReactivPanel.Controls.Add(this.ReactivDateIzgot);
            this.ReactivPanel.Controls.Add(this.ReactivID);
            this.ReactivPanel.Controls.Add(this.ReactivProdlenieSroka);
            this.ReactivPanel.Controls.Add(this.ReactivSrokGodnostiDate);
            this.ReactivPanel.Controls.Add(this.ReactivSrokGodnostiMounth);
            this.ReactivPanel.Controls.Add(this.ReactivSrokGodnoctiTip);
            this.ReactivPanel.Controls.Add(this.ReactivSrokGodnostiText);
            this.ReactivPanel.Controls.Add(this.ReactivEdIzm);
            this.ReactivPanel.Controls.Add(this.ReactivClass);
            this.ReactivPanel.Controls.Add(this.ReactivNPartii);
            this.ReactivPanel.Controls.Add(this.ReactivND);
            this.ReactivPanel.Controls.Add(this.ReactivName);
            this.ReactivPanel.Controls.Add(this.ReactivDateGet);
            this.ReactivPanel.Controls.Add(this.ReactivKol_vo);
            this.ReactivPanel.Controls.Add(this.ReactivEditCheckBox);
            this.ReactivPanel.Controls.Add(this.ReactivNewButton);
            this.ReactivPanel.Controls.Add(this.ReactivSaveButton);
            this.ReactivPanel.Controls.Add(this.label12);
            this.ReactivPanel.Controls.Add(this.pictureBox1);
            this.ReactivPanel.Controls.Add(this.label11);
            this.ReactivPanel.Controls.Add(this.label10);
            this.ReactivPanel.Controls.Add(this.label9);
            this.ReactivPanel.Controls.Add(this.label8);
            this.ReactivPanel.Controls.Add(this.label7);
            this.ReactivPanel.Controls.Add(this.label6);
            this.ReactivPanel.Controls.Add(this.label5);
            this.ReactivPanel.Controls.Add(this.label4);
            this.ReactivPanel.Controls.Add(this.label3);
            this.ReactivPanel.Controls.Add(this.label2);
            this.ReactivPanel.Controls.Add(this.label1);
            this.ReactivPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReactivPanel.Location = new System.Drawing.Point(0, 0);
            this.ReactivPanel.Name = "ReactivPanel";
            this.ReactivPanel.Size = new System.Drawing.Size(313, 703);
            this.ReactivPanel.TabIndex = 0;
            // 
            // ReactivDateGetTip
            // 
            this.ReactivDateGetTip.Enabled = false;
            this.ReactivDateGetTip.FormattingEnabled = true;
            this.ReactivDateGetTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.ReactivDateGetTip.Location = new System.Drawing.Point(123, 257);
            this.ReactivDateGetTip.Name = "ReactivDateGetTip";
            this.ReactivDateGetTip.Size = new System.Drawing.Size(54, 21);
            this.ReactivDateGetTip.TabIndex = 12;
            this.ReactivDateGetTip.Text = "дата";
            this.ReactivDateGetTip.SelectedIndexChanged += new System.EventHandler(this.ReactivDateGetTip_SelectedIndexChanged);
            // 
            // ReactivDateIzgotTip
            // 
            this.ReactivDateIzgotTip.Enabled = false;
            this.ReactivDateIzgotTip.FormattingEnabled = true;
            this.ReactivDateIzgotTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.ReactivDateIzgotTip.Location = new System.Drawing.Point(123, 203);
            this.ReactivDateIzgotTip.Name = "ReactivDateIzgotTip";
            this.ReactivDateIzgotTip.Size = new System.Drawing.Size(54, 21);
            this.ReactivDateIzgotTip.TabIndex = 8;
            this.ReactivDateIzgotTip.Text = "дата";
            this.ReactivDateIzgotTip.SelectedIndexChanged += new System.EventHandler(this.ReactivDateIzgotTip_SelectedIndexChanged);
            // 
            // ReactivNaznachenie
            // 
            this.ReactivNaznachenie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivNaznachenie.Enabled = false;
            this.ReactivNaznachenie.FormattingEnabled = true;
            this.ReactivNaznachenie.Items.AddRange(new object[] {
            "ГОСТ 11851-85 Массовая доля парафина",
            "ГОСТ 1756-00 Давление насыщенных паров",
            "ГОСТ 21534-76 Массовая концентрация хлористых солей",
            "ГОСТ 2177-99 Фракционный состав",
            "ГОСТ 2477-65 Массовая доля воды",
            "ГОСТ 33-2000 Вязкость",
            "ГОСТ 3900-85 Плотность",
            "ГОСТ 6370-83 Массовая доля механических примесей",
            "ГОСТ Р 50802-95 Массовая доля сероводорода и меркаптанов",
            "ГОСТ Р 51947-02 Массовая доля серы",
            "ГОСТ Р 52247-04 Массовая доля хлорорганических соединений",
            "МВИ Плотность"});
            this.ReactivNaznachenie.Location = new System.Drawing.Point(123, 338);
            this.ReactivNaznachenie.Name = "ReactivNaznachenie";
            this.ReactivNaznachenie.Size = new System.Drawing.Size(185, 21);
            this.ReactivNaznachenie.Sorted = true;
            this.ReactivNaznachenie.TabIndex = 16;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(71, 70);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(46, 13);
            this.label61.TabIndex = 32;
            this.label61.Text = " № п/п:";
            // 
            // ReactivN
            // 
            this.ReactivN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivN.Location = new System.Drawing.Point(123, 67);
            this.ReactivN.Name = "ReactivN";
            this.ReactivN.ReadOnly = true;
            this.ReactivN.Size = new System.Drawing.Size(185, 21);
            this.ReactivN.TabIndex = 3;
            // 
            // ReactivCopyButton
            // 
            this.ReactivCopyButton.AllowDrop = true;
            this.ReactivCopyButton.AutoSize = true;
            this.ReactivCopyButton.Image = global::LIMS.Properties.Resources.copy_16x16;
            this.ReactivCopyButton.Location = new System.Drawing.Point(209, 39);
            this.ReactivCopyButton.Name = "ReactivCopyButton";
            this.ReactivCopyButton.Size = new System.Drawing.Size(89, 22);
            this.ReactivCopyButton.TabIndex = 2;
            this.ReactivCopyButton.Text = "Копировать";
            this.ReactivCopyButton.Click += new System.EventHandler(this.ReactivCopyButton_Click);
            // 
            // ReactivDateIzgot
            // 
            this.ReactivDateIzgot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivDateIzgot.Checked = false;
            this.ReactivDateIzgot.Enabled = false;
            this.ReactivDateIzgot.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ReactivDateIzgot.Location = new System.Drawing.Point(183, 203);
            this.ReactivDateIzgot.Name = "ReactivDateIzgot";
            this.ReactivDateIzgot.Size = new System.Drawing.Size(125, 21);
            this.ReactivDateIzgot.TabIndex = 9;
            // 
            // ReactivID
            // 
            this.ReactivID.AutoSize = true;
            this.ReactivID.Location = new System.Drawing.Point(260, 25);
            this.ReactivID.Name = "ReactivID";
            this.ReactivID.Size = new System.Drawing.Size(0, 13);
            this.ReactivID.TabIndex = 27;
            this.ReactivID.Visible = false;
            // 
            // ReactivProdlenieSroka
            // 
            this.ReactivProdlenieSroka.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivProdlenieSroka.Location = new System.Drawing.Point(123, 365);
            this.ReactivProdlenieSroka.Multiline = true;
            this.ReactivProdlenieSroka.Name = "ReactivProdlenieSroka";
            this.ReactivProdlenieSroka.ReadOnly = true;
            this.ReactivProdlenieSroka.Size = new System.Drawing.Size(185, 34);
            this.ReactivProdlenieSroka.TabIndex = 17;
            // 
            // ReactivSrokGodnostiDate
            // 
            this.ReactivSrokGodnostiDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivSrokGodnostiDate.Checked = false;
            this.ReactivSrokGodnostiDate.Enabled = false;
            this.ReactivSrokGodnostiDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ReactivSrokGodnostiDate.Location = new System.Drawing.Point(183, 230);
            this.ReactivSrokGodnostiDate.Name = "ReactivSrokGodnostiDate";
            this.ReactivSrokGodnostiDate.Size = new System.Drawing.Size(125, 21);
            this.ReactivSrokGodnostiDate.TabIndex = 11;
            // 
            // ReactivSrokGodnostiMounth
            // 
            this.ReactivSrokGodnostiMounth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivSrokGodnostiMounth.Location = new System.Drawing.Point(183, 230);
            this.ReactivSrokGodnostiMounth.Name = "ReactivSrokGodnostiMounth";
            this.ReactivSrokGodnostiMounth.Size = new System.Drawing.Size(125, 21);
            this.ReactivSrokGodnostiMounth.TabIndex = 3;
            this.ReactivSrokGodnostiMounth.Visible = false;
            // 
            // ReactivSrokGodnoctiTip
            // 
            this.ReactivSrokGodnoctiTip.Enabled = false;
            this.ReactivSrokGodnoctiTip.FormattingEnabled = true;
            this.ReactivSrokGodnoctiTip.Items.AddRange(new object[] {
            "дата",
            "мес.",
            "др."});
            this.ReactivSrokGodnoctiTip.Location = new System.Drawing.Point(123, 230);
            this.ReactivSrokGodnoctiTip.Name = "ReactivSrokGodnoctiTip";
            this.ReactivSrokGodnoctiTip.Size = new System.Drawing.Size(54, 21);
            this.ReactivSrokGodnoctiTip.TabIndex = 10;
            this.ReactivSrokGodnoctiTip.Text = "дата";
            this.ReactivSrokGodnoctiTip.SelectedIndexChanged += new System.EventHandler(this.SrokGodnoctiTip_SelectedIndexChanged_1);
            // 
            // ReactivSrokGodnostiText
            // 
            this.ReactivSrokGodnostiText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivSrokGodnostiText.Location = new System.Drawing.Point(183, 230);
            this.ReactivSrokGodnostiText.Name = "ReactivSrokGodnostiText";
            this.ReactivSrokGodnostiText.Size = new System.Drawing.Size(125, 21);
            this.ReactivSrokGodnostiText.TabIndex = 38;
            this.ReactivSrokGodnostiText.Visible = false;
            // 
            // ReactivEdIzm
            // 
            this.ReactivEdIzm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivEdIzm.Enabled = false;
            this.ReactivEdIzm.FormattingEnabled = true;
            this.ReactivEdIzm.Items.AddRange(new object[] {
            "амп.",
            "г.",
            "кг.",
            "л.",
            "мл."});
            this.ReactivEdIzm.Location = new System.Drawing.Point(123, 311);
            this.ReactivEdIzm.Name = "ReactivEdIzm";
            this.ReactivEdIzm.Size = new System.Drawing.Size(185, 21);
            this.ReactivEdIzm.Sorted = true;
            this.ReactivEdIzm.TabIndex = 15;
            // 
            // ReactivClass
            // 
            this.ReactivClass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivClass.Enabled = false;
            this.ReactivClass.FormattingEnabled = true;
            this.ReactivClass.Items.AddRange(new object[] {
            "имп.",
            "о.с.ч.",
            "тех.",
            "х.ч.",
            "ч.",
            "ч.д.а.",
            "этал."});
            this.ReactivClass.Location = new System.Drawing.Point(123, 121);
            this.ReactivClass.Name = "ReactivClass";
            this.ReactivClass.Size = new System.Drawing.Size(185, 21);
            this.ReactivClass.Sorted = true;
            this.ReactivClass.TabIndex = 5;
            // 
            // ReactivNPartii
            // 
            this.ReactivNPartii.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivNPartii.Location = new System.Drawing.Point(123, 174);
            this.ReactivNPartii.Name = "ReactivNPartii";
            this.ReactivNPartii.ReadOnly = true;
            this.ReactivNPartii.Size = new System.Drawing.Size(185, 21);
            this.ReactivNPartii.TabIndex = 7;
            // 
            // ReactivND
            // 
            this.ReactivND.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivND.Location = new System.Drawing.Point(123, 147);
            this.ReactivND.Name = "ReactivND";
            this.ReactivND.ReadOnly = true;
            this.ReactivND.Size = new System.Drawing.Size(185, 21);
            this.ReactivND.TabIndex = 6;
            // 
            // ReactivName
            // 
            this.ReactivName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivName.Location = new System.Drawing.Point(123, 94);
            this.ReactivName.Name = "ReactivName";
            this.ReactivName.ReadOnly = true;
            this.ReactivName.Size = new System.Drawing.Size(185, 21);
            this.ReactivName.TabIndex = 4;
            // 
            // ReactivDateGet
            // 
            this.ReactivDateGet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivDateGet.Checked = false;
            this.ReactivDateGet.Enabled = false;
            this.ReactivDateGet.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ReactivDateGet.Location = new System.Drawing.Point(183, 257);
            this.ReactivDateGet.Name = "ReactivDateGet";
            this.ReactivDateGet.Size = new System.Drawing.Size(125, 21);
            this.ReactivDateGet.TabIndex = 13;
            // 
            // ReactivKol_vo
            // 
            this.ReactivKol_vo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReactivKol_vo.DecimalPlaces = 3;
            this.ReactivKol_vo.Enabled = false;
            this.ReactivKol_vo.Location = new System.Drawing.Point(123, 284);
            this.ReactivKol_vo.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.ReactivKol_vo.Name = "ReactivKol_vo";
            this.ReactivKol_vo.Size = new System.Drawing.Size(185, 21);
            this.ReactivKol_vo.TabIndex = 14;
            // 
            // ReactivEditCheckBox
            // 
            this.ReactivEditCheckBox.AutoSize = true;
            this.ReactivEditCheckBox.Location = new System.Drawing.Point(123, 405);
            this.ReactivEditCheckBox.Name = "ReactivEditCheckBox";
            this.ReactivEditCheckBox.Size = new System.Drawing.Size(105, 17);
            this.ReactivEditCheckBox.TabIndex = 18;
            this.ReactivEditCheckBox.Text = "Редактировать";
            this.ReactivEditCheckBox.UseVisualStyleBackColor = true;
            this.ReactivEditCheckBox.CheckedChanged += new System.EventHandler(this.ReactivEditCheckBox_CheckedChanged);
            // 
            // ReactivNewButton
            // 
            this.ReactivNewButton.AllowDrop = true;
            this.ReactivNewButton.AutoSize = true;
            this.ReactivNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
            this.ReactivNewButton.Location = new System.Drawing.Point(123, 39);
            this.ReactivNewButton.Name = "ReactivNewButton";
            this.ReactivNewButton.Size = new System.Drawing.Size(78, 22);
            this.ReactivNewButton.TabIndex = 1;
            this.ReactivNewButton.Text = "Добавить";
            this.ReactivNewButton.Click += new System.EventHandler(this.ReactivNewButton_Click);
            // 
            // ReactivSaveButton
            // 
            this.ReactivSaveButton.AutoSize = true;
            this.ReactivSaveButton.Image = global::LIMS.Properties.Resources.save_16x16;
            this.ReactivSaveButton.Location = new System.Drawing.Point(123, 428);
            this.ReactivSaveButton.Name = "ReactivSaveButton";
            this.ReactivSaveButton.Size = new System.Drawing.Size(83, 22);
            this.ReactivSaveButton.TabIndex = 20;
            this.ReactivSaveButton.Text = "Сохранить";
            this.ReactivSaveButton.Click += new System.EventHandler(this.ReactivSaveButton_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(148, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 16);
            this.label12.TabIndex = 23;
            this.label12.Text = "Реактивы";
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = global::LIMS.Properties.Resources.kolba;
            this.pictureBox1.Location = new System.Drawing.Point(10, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 57);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 367);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Продление срока:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(46, 341);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Назначение:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(46, 285);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Количество:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(65, 314);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Ед. изм.:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 259);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Дата получения:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 233);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Срок годности:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Дата изготовления:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 177);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Номер партии:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "НД на реактив:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Квалификация:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Наименование:";
            // 
            // ReactivGrid
            // 
            this.ReactivGrid.AccessibleDescription = "ReactivGrid";
            this.ReactivGrid.AccessibleName = "ReactivGrid";
            this.ReactivGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReactivGrid.Location = new System.Drawing.Point(0, 40);
            this.ReactivGrid.MainView = this.ReactivGridView;
            this.ReactivGrid.Name = "ReactivGrid";
            this.ReactivGrid.Size = new System.Drawing.Size(670, 663);
            this.ReactivGrid.TabIndex = 99999;
            this.ReactivGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ReactivGridView});
            this.ReactivGrid.Click += new System.EventHandler(this.ReactivGrid_Click);
            // 
            // ReactivGridView
            // 
            this.ReactivGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.ReactivGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivGridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivGridView.AppearancePrint.EvenRow.Options.UseTextOptions = true;
            this.ReactivGridView.AppearancePrint.EvenRow.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivGridView.AppearancePrint.EvenRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivGridView.AppearancePrint.GroupRow.Options.UseTextOptions = true;
            this.ReactivGridView.AppearancePrint.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivGridView.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.ReactivGridView.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivGridView.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivGridView.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivGridView.AppearancePrint.Lines.Options.UseTextOptions = true;
            this.ReactivGridView.AppearancePrint.Lines.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivGridView.AppearancePrint.OddRow.Options.UseTextOptions = true;
            this.ReactivGridView.AppearancePrint.OddRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivGridView.AppearancePrint.Preview.Options.UseTextOptions = true;
            this.ReactivGridView.AppearancePrint.Preview.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivGridView.AppearancePrint.Row.Options.UseTextOptions = true;
            this.ReactivGridView.AppearancePrint.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ReactivGridView.AppearancePrint.Row.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ReactivGridView.AppearancePrint.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ReactivGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.ReactivGridView.GridControl = this.ReactivGrid;
            this.ReactivGridView.Name = "ReactivGridView";
            this.ReactivGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.ReactivGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.ReactivGridView.OptionsBehavior.AllowIncrementalSearch = true;
            this.ReactivGridView.OptionsBehavior.Editable = false;
            this.ReactivGridView.OptionsCustomization.AllowRowSizing = true;
            this.ReactivGridView.OptionsFind.AlwaysVisible = true;
            this.ReactivGridView.OptionsNavigation.AutoFocusNewRow = true;
            this.ReactivGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.ReactivGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.ReactivGridView.OptionsView.RowAutoHeight = true;
            this.ReactivGridView.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.ReactivGridView_RowClick);
            this.ReactivGridView.ColumnWidthChanged += new DevExpress.XtraGrid.Views.Base.ColumnEventHandler(this.NTDGridView_ColumnWidthChanged);
            this.ReactivGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.ReactivGridView_PopupMenuShowing);
            this.ReactivGridView.StartSorting += new System.EventHandler(this.ReactivGridView_StartSorting);
            // 
            // ReactivPanelControl
            // 
            this.ReactivPanelControl.Controls.Add(this.ReactivCellMergeButton);
            this.ReactivPanelControl.Controls.Add(this.ReactivVisibleSpisatButton);
            this.ReactivPanelControl.Controls.Add(this.ReactivCheckColumnButton);
            this.ReactivPanelControl.Controls.Add(this.RefreshReactivButton);
            this.ReactivPanelControl.Controls.Add(this.ReactivPrintButton);
            this.ReactivPanelControl.Controls.Add(this.ReactivDeleteButton);
            this.ReactivPanelControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.ReactivPanelControl.Location = new System.Drawing.Point(0, 0);
            this.ReactivPanelControl.Name = "ReactivPanelControl";
            this.ReactivPanelControl.Size = new System.Drawing.Size(670, 40);
            this.ReactivPanelControl.TabIndex = 0;
            // 
            // ReactivCellMergeButton
            // 
            this.ReactivCellMergeButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.ReactivCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            this.ReactivCellMergeButton.Location = new System.Drawing.Point(122, 2);
            this.ReactivCellMergeButton.Name = "ReactivCellMergeButton";
            this.ReactivCellMergeButton.Size = new System.Drawing.Size(40, 36);
            this.ReactivCellMergeButton.TabIndex = 24;
            this.ReactivCellMergeButton.Click += new System.EventHandler(this.ReactivCellMergeButton_Click);
            // 
            // ReactivVisibleSpisatButton
            // 
            this.ReactivVisibleSpisatButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.ReactivVisibleSpisatButton.Image = global::LIMS.Properties.Resources.checkbuttons_32x32;
            this.ReactivVisibleSpisatButton.Location = new System.Drawing.Point(82, 2);
            this.ReactivVisibleSpisatButton.Name = "ReactivVisibleSpisatButton";
            this.ReactivVisibleSpisatButton.Size = new System.Drawing.Size(40, 36);
            this.ReactivVisibleSpisatButton.TabIndex = 23;
            this.ReactivVisibleSpisatButton.Click += new System.EventHandler(this.ReactivVisibleSpisatButton_Click);
            // 
            // ReactivCheckColumnButton
            // 
            this.ReactivCheckColumnButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.ReactivCheckColumnButton.Image = global::LIMS.Properties.Resources.insertcolumns_32x32;
            this.ReactivCheckColumnButton.Location = new System.Drawing.Point(588, 2);
            this.ReactivCheckColumnButton.Name = "ReactivCheckColumnButton";
            this.ReactivCheckColumnButton.Size = new System.Drawing.Size(40, 36);
            this.ReactivCheckColumnButton.TabIndex = 25;
            this.ReactivCheckColumnButton.Click += new System.EventHandler(this.CheckColumnButton_Click);
            // 
            // RefreshReactivButton
            // 
            this.RefreshReactivButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.RefreshReactivButton.Image = global::LIMS.Properties.Resources.refresh_32x32;
            this.RefreshReactivButton.Location = new System.Drawing.Point(628, 2);
            this.RefreshReactivButton.Name = "RefreshReactivButton";
            this.RefreshReactivButton.Size = new System.Drawing.Size(40, 36);
            this.RefreshReactivButton.TabIndex = 26;
            this.RefreshReactivButton.Click += new System.EventHandler(this.RefreshReactivButton_Click);
            // 
            // ReactivPrintButton
            // 
            this.ReactivPrintButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.ReactivPrintButton.Image = global::LIMS.Properties.Resources.printer_32x32;
            this.ReactivPrintButton.Location = new System.Drawing.Point(42, 2);
            this.ReactivPrintButton.Name = "ReactivPrintButton";
            this.ReactivPrintButton.Size = new System.Drawing.Size(40, 36);
            this.ReactivPrintButton.TabIndex = 22;
            this.ReactivPrintButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // ReactivDeleteButton
            // 
            this.ReactivDeleteButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.ReactivDeleteButton.Image = global::LIMS.Properties.Resources.delete_32x32;
            this.ReactivDeleteButton.Location = new System.Drawing.Point(2, 2);
            this.ReactivDeleteButton.Name = "ReactivDeleteButton";
            this.ReactivDeleteButton.Size = new System.Drawing.Size(40, 36);
            this.ReactivDeleteButton.TabIndex = 21;
            this.ReactivDeleteButton.Click += new System.EventHandler(this.ReactivDeleteButton_Click);
            // 
            // Personal
            // 
            this.Personal.Controls.Add(this.splitContainerControl1);
            this.Personal.Name = "Personal";
            this.Personal.Size = new System.Drawing.Size(988, 703);
            this.Personal.Text = "Персонал";
            this.Personal.TooltipIconType = DevExpress.Utils.ToolTipIconType.WindLogo;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.PersonalPanel);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel1.SizeChanged += new System.EventHandler(this.splitContainerControl1_Panel1_SizeChanged);
            this.splitContainerControl1.Panel2.Controls.Add(this.PersonalGrid);
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl4);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(988, 703);
            this.splitContainerControl1.SplitterPosition = 304;
            this.splitContainerControl1.TabIndex = 2;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // PersonalPanel
            // 
            this.PersonalPanel.Controls.Add(this.PersonalZnaniaOTiPBTip);
            this.PersonalPanel.Controls.Add(this.PersonalAttestechiaTip);
            this.PersonalPanel.Controls.Add(this.PersonalGasTip);
            this.PersonalPanel.Controls.Add(this.PersonalSrokPodvergKvalifTip);
            this.PersonalPanel.Controls.Add(this.PersonalSrokZnaniiOTiPBTip);
            this.PersonalPanel.Controls.Add(this.PersonalSrokAttestachiiTip);
            this.PersonalPanel.Controls.Add(this.PersonalSrokGasTip);
            this.PersonalPanel.Controls.Add(this.PersonalN);
            this.PersonalPanel.Controls.Add(this.label110);
            this.PersonalPanel.Controls.Add(this.label109);
            this.PersonalPanel.Controls.Add(this.label108);
            this.PersonalPanel.Controls.Add(this.PersonalObrasovanie);
            this.PersonalPanel.Controls.Add(this.PersonalCopyButton);
            this.PersonalPanel.Controls.Add(this.PersonalPovishKvalif);
            this.PersonalPanel.Controls.Add(this.label19);
            this.PersonalPanel.Controls.Add(this.label30);
            this.PersonalPanel.Controls.Add(this.PersonalSrokZnaniiOTiPBDate);
            this.PersonalPanel.Controls.Add(this.PersonalZnaniaOTiPB);
            this.PersonalPanel.Controls.Add(this.PersonalSrokAttestachiiDate);
            this.PersonalPanel.Controls.Add(this.PersonalAttestechia);
            this.PersonalPanel.Controls.Add(this.PersonalDolgnost);
            this.PersonalPanel.Controls.Add(this.PersonalObrasovaniePoln);
            this.PersonalPanel.Controls.Add(this.PersonalSrokGasDate);
            this.PersonalPanel.Controls.Add(this.PersonalGas);
            this.PersonalPanel.Controls.Add(this.label28);
            this.PersonalPanel.Controls.Add(this.label29);
            this.PersonalPanel.Controls.Add(this.PersonalSrokAttestachiiKol);
            this.PersonalPanel.Controls.Add(this.PersonalSrokZnaniiOTiPBKol);
            this.PersonalPanel.Controls.Add(this.PersonalSrokGasKol);
            this.PersonalPanel.Controls.Add(this.label27);
            this.PersonalPanel.Controls.Add(this.label26);
            this.PersonalPanel.Controls.Add(this.label13);
            this.PersonalPanel.Controls.Add(this.PersonalID);
            this.PersonalPanel.Controls.Add(this.PersonalDopolnitelno);
            this.PersonalPanel.Controls.Add(this.PersonalOpitNumber);
            this.PersonalPanel.Controls.Add(this.PersonalOpitLet);
            this.PersonalPanel.Controls.Add(this.PersonalName);
            this.PersonalPanel.Controls.Add(this.PersonalSrokPodvergKvalif);
            this.PersonalPanel.Controls.Add(this.PersonalEditCheckBox);
            this.PersonalPanel.Controls.Add(this.PersonalNewButton);
            this.PersonalPanel.Controls.Add(this.PersonalSaveButton);
            this.PersonalPanel.Controls.Add(this.label14);
            this.PersonalPanel.Controls.Add(this.pictureBox2);
            this.PersonalPanel.Controls.Add(this.label16);
            this.PersonalPanel.Controls.Add(this.label17);
            this.PersonalPanel.Controls.Add(this.label18);
            this.PersonalPanel.Controls.Add(this.label20);
            this.PersonalPanel.Controls.Add(this.label21);
            this.PersonalPanel.Controls.Add(this.label22);
            this.PersonalPanel.Controls.Add(this.label23);
            this.PersonalPanel.Controls.Add(this.label24);
            this.PersonalPanel.Controls.Add(this.label25);
            this.PersonalPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PersonalPanel.Location = new System.Drawing.Point(0, 0);
            this.PersonalPanel.Name = "PersonalPanel";
            this.PersonalPanel.Size = new System.Drawing.Size(304, 703);
            this.PersonalPanel.TabIndex = 0;
            // 
            // PersonalZnaniaOTiPBTip
            // 
            this.PersonalZnaniaOTiPBTip.Enabled = false;
            this.PersonalZnaniaOTiPBTip.FormattingEnabled = true;
            this.PersonalZnaniaOTiPBTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.PersonalZnaniaOTiPBTip.Location = new System.Drawing.Point(144, 449);
            this.PersonalZnaniaOTiPBTip.Name = "PersonalZnaniaOTiPBTip";
            this.PersonalZnaniaOTiPBTip.Size = new System.Drawing.Size(54, 21);
            this.PersonalZnaniaOTiPBTip.Sorted = true;
            this.PersonalZnaniaOTiPBTip.TabIndex = 21;
            this.PersonalZnaniaOTiPBTip.Text = "дата";
            this.PersonalZnaniaOTiPBTip.SelectedIndexChanged += new System.EventHandler(this.PersonalZnaniaOTiPBTip_SelectedIndexChanged);
            // 
            // PersonalAttestechiaTip
            // 
            this.PersonalAttestechiaTip.Enabled = false;
            this.PersonalAttestechiaTip.FormattingEnabled = true;
            this.PersonalAttestechiaTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.PersonalAttestechiaTip.Location = new System.Drawing.Point(144, 395);
            this.PersonalAttestechiaTip.Name = "PersonalAttestechiaTip";
            this.PersonalAttestechiaTip.Size = new System.Drawing.Size(54, 21);
            this.PersonalAttestechiaTip.Sorted = true;
            this.PersonalAttestechiaTip.TabIndex = 17;
            this.PersonalAttestechiaTip.Text = "дата";
            this.PersonalAttestechiaTip.SelectedIndexChanged += new System.EventHandler(this.PersonalAttestechiaTip_SelectedIndexChanged);
            // 
            // PersonalGasTip
            // 
            this.PersonalGasTip.Enabled = false;
            this.PersonalGasTip.FormattingEnabled = true;
            this.PersonalGasTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.PersonalGasTip.Location = new System.Drawing.Point(144, 341);
            this.PersonalGasTip.Name = "PersonalGasTip";
            this.PersonalGasTip.Size = new System.Drawing.Size(54, 21);
            this.PersonalGasTip.Sorted = true;
            this.PersonalGasTip.TabIndex = 13;
            this.PersonalGasTip.Text = "дата";
            this.PersonalGasTip.SelectedIndexChanged += new System.EventHandler(this.PersonalGasTip_SelectedIndexChanged);
            // 
            // PersonalSrokPodvergKvalifTip
            // 
            this.PersonalSrokPodvergKvalifTip.Enabled = false;
            this.PersonalSrokPodvergKvalifTip.FormattingEnabled = true;
            this.PersonalSrokPodvergKvalifTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.PersonalSrokPodvergKvalifTip.Location = new System.Drawing.Point(144, 314);
            this.PersonalSrokPodvergKvalifTip.Name = "PersonalSrokPodvergKvalifTip";
            this.PersonalSrokPodvergKvalifTip.Size = new System.Drawing.Size(54, 21);
            this.PersonalSrokPodvergKvalifTip.Sorted = true;
            this.PersonalSrokPodvergKvalifTip.TabIndex = 11;
            this.PersonalSrokPodvergKvalifTip.Text = "дата";
            this.PersonalSrokPodvergKvalifTip.SelectedIndexChanged += new System.EventHandler(this.PersonalSrokPodvergKvalifTip_SelectedIndexChanged);
            // 
            // PersonalSrokZnaniiOTiPBTip
            // 
            this.PersonalSrokZnaniiOTiPBTip.Enabled = false;
            this.PersonalSrokZnaniiOTiPBTip.FormattingEnabled = true;
            this.PersonalSrokZnaniiOTiPBTip.Items.AddRange(new object[] {
            "дата",
            "мес.",
            "нет"});
            this.PersonalSrokZnaniiOTiPBTip.Location = new System.Drawing.Point(145, 476);
            this.PersonalSrokZnaniiOTiPBTip.Name = "PersonalSrokZnaniiOTiPBTip";
            this.PersonalSrokZnaniiOTiPBTip.Size = new System.Drawing.Size(54, 21);
            this.PersonalSrokZnaniiOTiPBTip.Sorted = true;
            this.PersonalSrokZnaniiOTiPBTip.TabIndex = 23;
            this.PersonalSrokZnaniiOTiPBTip.Text = "дата";
            this.PersonalSrokZnaniiOTiPBTip.SelectedIndexChanged += new System.EventHandler(this.PersonalSrokZnaniiOTiPBTip_SelectedIndexChanged);
            // 
            // PersonalSrokAttestachiiTip
            // 
            this.PersonalSrokAttestachiiTip.Enabled = false;
            this.PersonalSrokAttestachiiTip.FormattingEnabled = true;
            this.PersonalSrokAttestachiiTip.Items.AddRange(new object[] {
            "дата",
            "мес.",
            "нет"});
            this.PersonalSrokAttestachiiTip.Location = new System.Drawing.Point(144, 422);
            this.PersonalSrokAttestachiiTip.Name = "PersonalSrokAttestachiiTip";
            this.PersonalSrokAttestachiiTip.Size = new System.Drawing.Size(54, 21);
            this.PersonalSrokAttestachiiTip.Sorted = true;
            this.PersonalSrokAttestachiiTip.TabIndex = 19;
            this.PersonalSrokAttestachiiTip.Text = "дата";
            this.PersonalSrokAttestachiiTip.SelectedIndexChanged += new System.EventHandler(this.PersonalSrokAttestachiiTip_SelectedIndexChanged);
            // 
            // PersonalSrokGasTip
            // 
            this.PersonalSrokGasTip.Enabled = false;
            this.PersonalSrokGasTip.FormattingEnabled = true;
            this.PersonalSrokGasTip.Items.AddRange(new object[] {
            "дата",
            "мес.",
            "нет"});
            this.PersonalSrokGasTip.Location = new System.Drawing.Point(144, 368);
            this.PersonalSrokGasTip.Name = "PersonalSrokGasTip";
            this.PersonalSrokGasTip.Size = new System.Drawing.Size(54, 21);
            this.PersonalSrokGasTip.Sorted = true;
            this.PersonalSrokGasTip.TabIndex = 15;
            this.PersonalSrokGasTip.Text = "дата";
            this.PersonalSrokGasTip.SelectedIndexChanged += new System.EventHandler(this.PersonalSrokGasTip_SelectedIndexChanged);
            // 
            // PersonalN
            // 
            this.PersonalN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalN.Location = new System.Drawing.Point(103, 74);
            this.PersonalN.Name = "PersonalN";
            this.PersonalN.ReadOnly = true;
            this.PersonalN.Size = new System.Drawing.Size(196, 21);
            this.PersonalN.TabIndex = 3;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(54, 77);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(43, 13);
            this.label110.TabIndex = 49;
            this.label110.Text = "№ п/п:";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(32, 215);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(55, 13);
            this.label109.TabIndex = 48;
            this.label109.Text = "(полное):";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(23, 202);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(74, 13);
            this.label108.TabIndex = 47;
            this.label108.Text = "Образование";
            // 
            // PersonalObrasovanie
            // 
            this.PersonalObrasovanie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalObrasovanie.Location = new System.Drawing.Point(103, 172);
            this.PersonalObrasovanie.Name = "PersonalObrasovanie";
            this.PersonalObrasovanie.ReadOnly = true;
            this.PersonalObrasovanie.Size = new System.Drawing.Size(196, 21);
            this.PersonalObrasovanie.TabIndex = 6;
            // 
            // PersonalCopyButton
            // 
            this.PersonalCopyButton.AllowDrop = true;
            this.PersonalCopyButton.AutoSize = true;
            this.PersonalCopyButton.Image = global::LIMS.Properties.Resources.copy_16x16;
            this.PersonalCopyButton.Location = new System.Drawing.Point(189, 45);
            this.PersonalCopyButton.Name = "PersonalCopyButton";
            this.PersonalCopyButton.Size = new System.Drawing.Size(89, 22);
            this.PersonalCopyButton.TabIndex = 2;
            this.PersonalCopyButton.Text = "Копировать";
            this.PersonalCopyButton.Click += new System.EventHandler(this.PersonalCopyButton_Click);
            // 
            // PersonalPovishKvalif
            // 
            this.PersonalPovishKvalif.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalPovishKvalif.Location = new System.Drawing.Point(145, 270);
            this.PersonalPovishKvalif.Multiline = true;
            this.PersonalPovishKvalif.Name = "PersonalPovishKvalif";
            this.PersonalPovishKvalif.ReadOnly = true;
            this.PersonalPovishKvalif.Size = new System.Drawing.Size(154, 38);
            this.PersonalPovishKvalif.TabIndex = 10;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(73, 458);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 43;
            this.label19.Text = "по ОТ и ПБ:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(44, 445);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(94, 13);
            this.label30.TabIndex = 42;
            this.label30.Text = "Проверка знаний";
            this.label30.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PersonalSrokZnaniiOTiPBDate
            // 
            this.PersonalSrokZnaniiOTiPBDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalSrokZnaniiOTiPBDate.Checked = false;
            this.PersonalSrokZnaniiOTiPBDate.Enabled = false;
            this.PersonalSrokZnaniiOTiPBDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.PersonalSrokZnaniiOTiPBDate.Location = new System.Drawing.Point(204, 476);
            this.PersonalSrokZnaniiOTiPBDate.Name = "PersonalSrokZnaniiOTiPBDate";
            this.PersonalSrokZnaniiOTiPBDate.Size = new System.Drawing.Size(95, 21);
            this.PersonalSrokZnaniiOTiPBDate.TabIndex = 24;
            // 
            // PersonalZnaniaOTiPB
            // 
            this.PersonalZnaniaOTiPB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalZnaniaOTiPB.Checked = false;
            this.PersonalZnaniaOTiPB.Enabled = false;
            this.PersonalZnaniaOTiPB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.PersonalZnaniaOTiPB.Location = new System.Drawing.Point(204, 449);
            this.PersonalZnaniaOTiPB.Name = "PersonalZnaniaOTiPB";
            this.PersonalZnaniaOTiPB.Size = new System.Drawing.Size(95, 21);
            this.PersonalZnaniaOTiPB.TabIndex = 22;
            // 
            // PersonalSrokAttestachiiDate
            // 
            this.PersonalSrokAttestachiiDate.AccessibleDescription = "";
            this.PersonalSrokAttestachiiDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalSrokAttestachiiDate.Checked = false;
            this.PersonalSrokAttestachiiDate.Enabled = false;
            this.PersonalSrokAttestachiiDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.PersonalSrokAttestachiiDate.Location = new System.Drawing.Point(204, 423);
            this.PersonalSrokAttestachiiDate.Name = "PersonalSrokAttestachiiDate";
            this.PersonalSrokAttestachiiDate.Size = new System.Drawing.Size(95, 21);
            this.PersonalSrokAttestachiiDate.TabIndex = 20;
            // 
            // PersonalAttestechia
            // 
            this.PersonalAttestechia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalAttestechia.Checked = false;
            this.PersonalAttestechia.Enabled = false;
            this.PersonalAttestechia.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.PersonalAttestechia.Location = new System.Drawing.Point(204, 395);
            this.PersonalAttestechia.Name = "PersonalAttestechia";
            this.PersonalAttestechia.Size = new System.Drawing.Size(95, 21);
            this.PersonalAttestechia.TabIndex = 18;
            // 
            // PersonalDolgnost
            // 
            this.PersonalDolgnost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalDolgnost.Location = new System.Drawing.Point(103, 128);
            this.PersonalDolgnost.Multiline = true;
            this.PersonalDolgnost.Name = "PersonalDolgnost";
            this.PersonalDolgnost.ReadOnly = true;
            this.PersonalDolgnost.Size = new System.Drawing.Size(196, 38);
            this.PersonalDolgnost.TabIndex = 5;
            // 
            // PersonalObrasovaniePoln
            // 
            this.PersonalObrasovaniePoln.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalObrasovaniePoln.Location = new System.Drawing.Point(103, 199);
            this.PersonalObrasovaniePoln.Multiline = true;
            this.PersonalObrasovaniePoln.Name = "PersonalObrasovaniePoln";
            this.PersonalObrasovaniePoln.ReadOnly = true;
            this.PersonalObrasovaniePoln.Size = new System.Drawing.Size(196, 38);
            this.PersonalObrasovaniePoln.TabIndex = 7;
            // 
            // PersonalSrokGasDate
            // 
            this.PersonalSrokGasDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalSrokGasDate.Checked = false;
            this.PersonalSrokGasDate.Enabled = false;
            this.PersonalSrokGasDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.PersonalSrokGasDate.Location = new System.Drawing.Point(204, 369);
            this.PersonalSrokGasDate.Name = "PersonalSrokGasDate";
            this.PersonalSrokGasDate.Size = new System.Drawing.Size(95, 21);
            this.PersonalSrokGasDate.TabIndex = 16;
            // 
            // PersonalGas
            // 
            this.PersonalGas.AccessibleDescription = "";
            this.PersonalGas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalGas.Checked = false;
            this.PersonalGas.Enabled = false;
            this.PersonalGas.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.PersonalGas.Location = new System.Drawing.Point(204, 341);
            this.PersonalGas.Name = "PersonalGas";
            this.PersonalGas.Size = new System.Drawing.Size(95, 21);
            this.PersonalGas.TabIndex = 14;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(19, 349);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(119, 13);
            this.label28.TabIndex = 32;
            this.label28.Text = "газоанализаторщика:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(5, 336);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(133, 13);
            this.label29.TabIndex = 31;
            this.label29.Text = "Обучение на лаборанта-";
            // 
            // PersonalSrokAttestachiiKol
            // 
            this.PersonalSrokAttestachiiKol.Location = new System.Drawing.Point(205, 423);
            this.PersonalSrokAttestachiiKol.Name = "PersonalSrokAttestachiiKol";
            this.PersonalSrokAttestachiiKol.ReadOnly = true;
            this.PersonalSrokAttestachiiKol.Size = new System.Drawing.Size(94, 21);
            this.PersonalSrokAttestachiiKol.TabIndex = 55;
            this.PersonalSrokAttestachiiKol.Visible = false;
            // 
            // PersonalSrokZnaniiOTiPBKol
            // 
            this.PersonalSrokZnaniiOTiPBKol.Location = new System.Drawing.Point(205, 476);
            this.PersonalSrokZnaniiOTiPBKol.Name = "PersonalSrokZnaniiOTiPBKol";
            this.PersonalSrokZnaniiOTiPBKol.ReadOnly = true;
            this.PersonalSrokZnaniiOTiPBKol.Size = new System.Drawing.Size(94, 21);
            this.PersonalSrokZnaniiOTiPBKol.TabIndex = 56;
            this.PersonalSrokZnaniiOTiPBKol.Visible = false;
            // 
            // PersonalSrokGasKol
            // 
            this.PersonalSrokGasKol.Location = new System.Drawing.Point(204, 369);
            this.PersonalSrokGasKol.Name = "PersonalSrokGasKol";
            this.PersonalSrokGasKol.ReadOnly = true;
            this.PersonalSrokGasKol.Size = new System.Drawing.Size(95, 21);
            this.PersonalSrokGasKol.TabIndex = 54;
            this.PersonalSrokGasKol.Visible = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(35, 291);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(104, 13);
            this.label27.TabIndex = 30;
            this.label27.Text = "по осн. профессии:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(48, 504);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(91, 13);
            this.label26.TabIndex = 29;
            this.label26.Text = "Дополнительно:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 479);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(125, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Срок проверки знаний:";
            // 
            // PersonalID
            // 
            this.PersonalID.AutoSize = true;
            this.PersonalID.Location = new System.Drawing.Point(260, 25);
            this.PersonalID.Name = "PersonalID";
            this.PersonalID.Size = new System.Drawing.Size(0, 13);
            this.PersonalID.TabIndex = 27;
            this.PersonalID.Visible = false;
            // 
            // PersonalDopolnitelno
            // 
            this.PersonalDopolnitelno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalDopolnitelno.Location = new System.Drawing.Point(145, 503);
            this.PersonalDopolnitelno.Multiline = true;
            this.PersonalDopolnitelno.Name = "PersonalDopolnitelno";
            this.PersonalDopolnitelno.ReadOnly = true;
            this.PersonalDopolnitelno.Size = new System.Drawing.Size(154, 38);
            this.PersonalDopolnitelno.TabIndex = 25;
            // 
            // PersonalOpitNumber
            // 
            this.PersonalOpitNumber.Enabled = false;
            this.PersonalOpitNumber.Location = new System.Drawing.Point(145, 243);
            this.PersonalOpitNumber.Name = "PersonalOpitNumber";
            this.PersonalOpitNumber.Size = new System.Drawing.Size(53, 21);
            this.PersonalOpitNumber.TabIndex = 8;
            // 
            // PersonalOpitLet
            // 
            this.PersonalOpitLet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalOpitLet.Enabled = false;
            this.PersonalOpitLet.FormattingEnabled = true;
            this.PersonalOpitLet.Items.AddRange(new object[] {
            "месяц",
            "месяца",
            "месяцев",
            "год",
            "год  месяцев",
            "года",
            "лет"});
            this.PersonalOpitLet.Location = new System.Drawing.Point(204, 243);
            this.PersonalOpitLet.Name = "PersonalOpitLet";
            this.PersonalOpitLet.Size = new System.Drawing.Size(95, 21);
            this.PersonalOpitLet.TabIndex = 9;
            // 
            // PersonalName
            // 
            this.PersonalName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalName.Location = new System.Drawing.Point(103, 101);
            this.PersonalName.Name = "PersonalName";
            this.PersonalName.ReadOnly = true;
            this.PersonalName.Size = new System.Drawing.Size(196, 21);
            this.PersonalName.TabIndex = 4;
            // 
            // PersonalSrokPodvergKvalif
            // 
            this.PersonalSrokPodvergKvalif.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalSrokPodvergKvalif.Checked = false;
            this.PersonalSrokPodvergKvalif.Enabled = false;
            this.PersonalSrokPodvergKvalif.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.PersonalSrokPodvergKvalif.Location = new System.Drawing.Point(204, 314);
            this.PersonalSrokPodvergKvalif.Name = "PersonalSrokPodvergKvalif";
            this.PersonalSrokPodvergKvalif.Size = new System.Drawing.Size(95, 21);
            this.PersonalSrokPodvergKvalif.TabIndex = 12;
            // 
            // PersonalEditCheckBox
            // 
            this.PersonalEditCheckBox.AutoSize = true;
            this.PersonalEditCheckBox.Location = new System.Drawing.Point(145, 547);
            this.PersonalEditCheckBox.Name = "PersonalEditCheckBox";
            this.PersonalEditCheckBox.Size = new System.Drawing.Size(105, 17);
            this.PersonalEditCheckBox.TabIndex = 26;
            this.PersonalEditCheckBox.Text = "Редактировать";
            this.PersonalEditCheckBox.UseVisualStyleBackColor = true;
            this.PersonalEditCheckBox.CheckedChanged += new System.EventHandler(this.PersonalEditCheckBox_CheckedChanged);
            // 
            // PersonalNewButton
            // 
            this.PersonalNewButton.AllowDrop = true;
            this.PersonalNewButton.AutoSize = true;
            this.PersonalNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
            this.PersonalNewButton.Location = new System.Drawing.Point(103, 45);
            this.PersonalNewButton.Name = "PersonalNewButton";
            this.PersonalNewButton.Size = new System.Drawing.Size(78, 22);
            this.PersonalNewButton.TabIndex = 1;
            this.PersonalNewButton.Text = "Добавить";
            this.PersonalNewButton.Click += new System.EventHandler(this.PersonalNewButton_Click);
            // 
            // PersonalSaveButton
            // 
            this.PersonalSaveButton.AutoSize = true;
            this.PersonalSaveButton.Enabled = false;
            this.PersonalSaveButton.Image = global::LIMS.Properties.Resources.save_16x16;
            this.PersonalSaveButton.Location = new System.Drawing.Point(145, 570);
            this.PersonalSaveButton.Name = "PersonalSaveButton";
            this.PersonalSaveButton.Size = new System.Drawing.Size(83, 22);
            this.PersonalSaveButton.TabIndex = 27;
            this.PersonalSaveButton.Text = "Сохранить";
            this.PersonalSaveButton.Click += new System.EventHandler(this.PersonalSaveButton_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(144, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 16);
            this.label14.TabIndex = 23;
            this.label14.Text = "Персонал";
            // 
            // pictureBox2
            // 
            this.pictureBox2.ErrorImage = null;
            this.pictureBox2.Image = global::LIMS.Properties.Resources.Group;
            this.pictureBox2.Location = new System.Drawing.Point(10, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(85, 70);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(41, 425);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 9;
            this.label16.Text = "Срок аттестации:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(17, 371);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(121, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Срок подтверждения:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(67, 398);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 13);
            this.label18.TabIndex = 7;
            this.label18.Text = "Аттестация:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(17, 314);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(121, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "Срок подтверждения:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 273);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(123, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "Повыш. квалификации";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(26, 246);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(112, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "Практический опыт:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(19, 175);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(78, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Образование:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(29, 131);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "Должность:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(63, 104);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(34, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "ФИО:";
            // 
            // PersonalGrid
            // 
            this.PersonalGrid.AccessibleDescription = "";
            this.PersonalGrid.AccessibleName = "";
            this.PersonalGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PersonalGrid.Location = new System.Drawing.Point(0, 40);
            this.PersonalGrid.MainView = this.PersonalGridView;
            this.PersonalGrid.Name = "PersonalGrid";
            this.PersonalGrid.Size = new System.Drawing.Size(679, 663);
            this.PersonalGrid.TabIndex = 99999;
            this.PersonalGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.PersonalGridView});
            this.PersonalGrid.Click += new System.EventHandler(this.PersonalGridControl_Click);
            // 
            // PersonalGridView
            // 
            this.PersonalGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.PersonalGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PersonalGridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PersonalGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PersonalGridView.AppearancePrint.EvenRow.Options.UseTextOptions = true;
            this.PersonalGridView.AppearancePrint.EvenRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PersonalGridView.AppearancePrint.GroupRow.Options.UseTextOptions = true;
            this.PersonalGridView.AppearancePrint.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PersonalGridView.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.PersonalGridView.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PersonalGridView.AppearancePrint.Lines.Options.UseTextOptions = true;
            this.PersonalGridView.AppearancePrint.Lines.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PersonalGridView.AppearancePrint.OddRow.Options.UseTextOptions = true;
            this.PersonalGridView.AppearancePrint.OddRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PersonalGridView.AppearancePrint.Preview.Options.UseTextOptions = true;
            this.PersonalGridView.AppearancePrint.Preview.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PersonalGridView.AppearancePrint.Row.Options.UseTextOptions = true;
            this.PersonalGridView.AppearancePrint.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PersonalGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.PersonalGridView.GridControl = this.PersonalGrid;
            this.PersonalGridView.Name = "PersonalGridView";
            this.PersonalGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.PersonalGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.PersonalGridView.OptionsBehavior.AllowIncrementalSearch = true;
            this.PersonalGridView.OptionsBehavior.Editable = false;
            this.PersonalGridView.OptionsCustomization.AllowRowSizing = true;
            this.PersonalGridView.OptionsFind.AlwaysVisible = true;
            this.PersonalGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.PersonalGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.PersonalGridView.OptionsView.RowAutoHeight = true;
            this.PersonalGridView.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.PersonalGridView_RowClick);
            this.PersonalGridView.ColumnWidthChanged += new DevExpress.XtraGrid.Views.Base.ColumnEventHandler(this.PersonalGridView_ColumnWidthChanged);
            this.PersonalGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.PersonalGridView_PopupMenuShowing);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.PersonalCellMergeButton);
            this.panelControl4.Controls.Add(this.PersonalUvolenButton);
            this.panelControl4.Controls.Add(this.PersonalColumnCheckButton);
            this.panelControl4.Controls.Add(this.PersonalUpdateButton);
            this.panelControl4.Controls.Add(this.PersonalPrintButton);
            this.panelControl4.Controls.Add(this.PersonalDeleteButton);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(679, 40);
            this.panelControl4.TabIndex = 0;
            // 
            // PersonalCellMergeButton
            // 
            this.PersonalCellMergeButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.PersonalCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            this.PersonalCellMergeButton.Location = new System.Drawing.Point(122, 2);
            this.PersonalCellMergeButton.Name = "PersonalCellMergeButton";
            this.PersonalCellMergeButton.Size = new System.Drawing.Size(40, 36);
            this.PersonalCellMergeButton.TabIndex = 31;
            this.PersonalCellMergeButton.Click += new System.EventHandler(this.PersonalCellMergeButton_Click);
            // 
            // PersonalUvolenButton
            // 
            this.PersonalUvolenButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.PersonalUvolenButton.Image = global::LIMS.Properties.Resources.checkbuttons_32x32;
            this.PersonalUvolenButton.Location = new System.Drawing.Point(82, 2);
            this.PersonalUvolenButton.Name = "PersonalUvolenButton";
            this.PersonalUvolenButton.Size = new System.Drawing.Size(40, 36);
            this.PersonalUvolenButton.TabIndex = 30;
            this.PersonalUvolenButton.Click += new System.EventHandler(this.PersonalUvolenButton_Click);
            // 
            // PersonalColumnCheckButton
            // 
            this.PersonalColumnCheckButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.PersonalColumnCheckButton.Image = global::LIMS.Properties.Resources.insertcolumns_32x32;
            this.PersonalColumnCheckButton.Location = new System.Drawing.Point(597, 2);
            this.PersonalColumnCheckButton.Name = "PersonalColumnCheckButton";
            this.PersonalColumnCheckButton.Size = new System.Drawing.Size(40, 36);
            this.PersonalColumnCheckButton.TabIndex = 32;
            this.PersonalColumnCheckButton.Click += new System.EventHandler(this.PersonalColumnCheckButton_Click);
            // 
            // PersonalUpdateButton
            // 
            this.PersonalUpdateButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.PersonalUpdateButton.Image = global::LIMS.Properties.Resources.refresh_32x32;
            this.PersonalUpdateButton.Location = new System.Drawing.Point(637, 2);
            this.PersonalUpdateButton.Name = "PersonalUpdateButton";
            this.PersonalUpdateButton.Size = new System.Drawing.Size(40, 36);
            this.PersonalUpdateButton.TabIndex = 33;
            this.PersonalUpdateButton.Click += new System.EventHandler(this.PersonalUpdateButton_Click);
            // 
            // PersonalPrintButton
            // 
            this.PersonalPrintButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.PersonalPrintButton.Image = global::LIMS.Properties.Resources.printer_32x32;
            this.PersonalPrintButton.Location = new System.Drawing.Point(42, 2);
            this.PersonalPrintButton.Name = "PersonalPrintButton";
            this.PersonalPrintButton.Size = new System.Drawing.Size(40, 36);
            this.PersonalPrintButton.TabIndex = 29;
            this.PersonalPrintButton.Click += new System.EventHandler(this.PersonalPrintButton_Click);
            // 
            // PersonalDeleteButton
            // 
            this.PersonalDeleteButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.PersonalDeleteButton.Image = global::LIMS.Properties.Resources.delete_32x32;
            this.PersonalDeleteButton.Location = new System.Drawing.Point(2, 2);
            this.PersonalDeleteButton.Name = "PersonalDeleteButton";
            this.PersonalDeleteButton.Size = new System.Drawing.Size(40, 36);
            this.PersonalDeleteButton.TabIndex = 28;
            this.PersonalDeleteButton.Click += new System.EventHandler(this.PersonalDeleteButton_Click);
            // 
            // NTD
            // 
            this.NTD.Controls.Add(this.splitContainerControl2);
            this.NTD.Name = "NTD";
            this.NTD.Size = new System.Drawing.Size(988, 703);
            this.NTD.Text = "НТД";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.NTDPanel);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel1.SizeChanged += new System.EventHandler(this.splitContainerControl2_Panel1_SizeChanged);
            this.splitContainerControl2.Panel2.Controls.Add(this.NTDGrid);
            this.splitContainerControl2.Panel2.Controls.Add(this.panelControl15);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(988, 703);
            this.splitContainerControl2.SplitterPosition = 336;
            this.splitContainerControl2.TabIndex = 2;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // NTDPanel
            // 
            this.NTDPanel.Controls.Add(this.NTDSrokDeisrviaTip);
            this.NTDPanel.Controls.Add(this.NTDDateAktualizTip);
            this.NTDPanel.Controls.Add(this.NTDDateUtvergdeniaTip);
            this.NTDPanel.Controls.Add(this.NTDGroup);
            this.NTDPanel.Controls.Add(this.NTDUroven);
            this.NTDPanel.Controls.Add(this.NTDN);
            this.NTDPanel.Controls.Add(this.NTDND1);
            this.NTDPanel.Controls.Add(this.NTDND2);
            this.NTDPanel.Controls.Add(this.label132);
            this.NTDPanel.Controls.Add(this.label131);
            this.NTDPanel.Controls.Add(this.label130);
            this.NTDPanel.Controls.Add(this.label111);
            this.NTDPanel.Controls.Add(this.NTDPrimechanie);
            this.NTDPanel.Controls.Add(this.NTDKontrolMesto);
            this.NTDPanel.Controls.Add(this.NTDRabMesto);
            this.NTDPanel.Controls.Add(this.NTDCopyButton);
            this.NTDPanel.Controls.Add(this.NTDSrokDeisrvia);
            this.NTDPanel.Controls.Add(this.NTDDateAktualiz);
            this.NTDPanel.Controls.Add(this.label15);
            this.NTDPanel.Controls.Add(this.NTDName);
            this.NTDPanel.Controls.Add(this.label35);
            this.NTDPanel.Controls.Add(this.label36);
            this.NTDPanel.Controls.Add(this.label34);
            this.NTDPanel.Controls.Add(this.label44);
            this.NTDPanel.Controls.Add(this.label43);
            this.NTDPanel.Controls.Add(this.NTDDateUtvergdenia);
            this.NTDPanel.Controls.Add(this.NTDID);
            this.NTDPanel.Controls.Add(this.NTDKontrolKolvo);
            this.NTDPanel.Controls.Add(this.NTDRegistN);
            this.NTDPanel.Controls.Add(this.NTDRabKolvo);
            this.NTDPanel.Controls.Add(this.NTDEditCheckBox);
            this.NTDPanel.Controls.Add(this.NTDNewButton);
            this.NTDPanel.Controls.Add(this.NTDSaveButton);
            this.NTDPanel.Controls.Add(this.label31);
            this.NTDPanel.Controls.Add(this.pictureBox3);
            this.NTDPanel.Controls.Add(this.label32);
            this.NTDPanel.Controls.Add(this.label33);
            this.NTDPanel.Controls.Add(this.label37);
            this.NTDPanel.Controls.Add(this.label38);
            this.NTDPanel.Controls.Add(this.label39);
            this.NTDPanel.Controls.Add(this.label40);
            this.NTDPanel.Controls.Add(this.label41);
            this.NTDPanel.Controls.Add(this.label42);
            this.NTDPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NTDPanel.Location = new System.Drawing.Point(0, 0);
            this.NTDPanel.Name = "NTDPanel";
            this.NTDPanel.Size = new System.Drawing.Size(336, 703);
            this.NTDPanel.TabIndex = 0;
            // 
            // NTDSrokDeisrviaTip
            // 
            this.NTDSrokDeisrviaTip.Enabled = false;
            this.NTDSrokDeisrviaTip.FormattingEnabled = true;
            this.NTDSrokDeisrviaTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.NTDSrokDeisrviaTip.Location = new System.Drawing.Point(152, 439);
            this.NTDSrokDeisrviaTip.Name = "NTDSrokDeisrviaTip";
            this.NTDSrokDeisrviaTip.Size = new System.Drawing.Size(54, 21);
            this.NTDSrokDeisrviaTip.Sorted = true;
            this.NTDSrokDeisrviaTip.TabIndex = 18;
            this.NTDSrokDeisrviaTip.Text = "дата";
            this.NTDSrokDeisrviaTip.SelectedIndexChanged += new System.EventHandler(this.NTDSrokDeisrviaTip_SelectedIndexChanged);
            // 
            // NTDDateAktualizTip
            // 
            this.NTDDateAktualizTip.Enabled = false;
            this.NTDDateAktualizTip.FormattingEnabled = true;
            this.NTDDateAktualizTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.NTDDateAktualizTip.Location = new System.Drawing.Point(153, 412);
            this.NTDDateAktualizTip.Name = "NTDDateAktualizTip";
            this.NTDDateAktualizTip.Size = new System.Drawing.Size(54, 21);
            this.NTDDateAktualizTip.Sorted = true;
            this.NTDDateAktualizTip.TabIndex = 16;
            this.NTDDateAktualizTip.Text = "дата";
            this.NTDDateAktualizTip.SelectedIndexChanged += new System.EventHandler(this.NTDDateAktualizTip_SelectedIndexChanged);
            // 
            // NTDDateUtvergdeniaTip
            // 
            this.NTDDateUtvergdeniaTip.Enabled = false;
            this.NTDDateUtvergdeniaTip.FormattingEnabled = true;
            this.NTDDateUtvergdeniaTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.NTDDateUtvergdeniaTip.Location = new System.Drawing.Point(153, 239);
            this.NTDDateUtvergdeniaTip.Name = "NTDDateUtvergdeniaTip";
            this.NTDDateUtvergdeniaTip.Size = new System.Drawing.Size(54, 21);
            this.NTDDateUtvergdeniaTip.Sorted = true;
            this.NTDDateUtvergdeniaTip.TabIndex = 10;
            this.NTDDateUtvergdeniaTip.Text = "дата";
            this.NTDDateUtvergdeniaTip.SelectedIndexChanged += new System.EventHandler(this.NTDDateUtvergdeniaTip_SelectedIndexChanged);
            // 
            // NTDGroup
            // 
            this.NTDGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDGroup.Enabled = false;
            this.NTDGroup.FormattingEnabled = true;
            this.NTDGroup.Items.AddRange(new object[] {
            "1.",
            "2.",
            "3.",
            "4.",
            "5.",
            "6.",
            "7.",
            "8.",
            "9.",
            "10.",
            "11.",
            "12.",
            "13.",
            "14.",
            "15."});
            this.NTDGroup.Location = new System.Drawing.Point(153, 105);
            this.NTDGroup.Name = "NTDGroup";
            this.NTDGroup.Size = new System.Drawing.Size(178, 21);
            this.NTDGroup.TabIndex = 4;
            // 
            // NTDUroven
            // 
            this.NTDUroven.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDUroven.Enabled = false;
            this.NTDUroven.FormattingEnabled = true;
            this.NTDUroven.Items.AddRange(new object[] {
            "I.",
            "II.",
            "III.",
            "IV.",
            "V.",
            "VI.",
            "VII.",
            "VIII.",
            "IX.",
            "X.",
            "XI.",
            "XII.",
            "XIII.",
            "XIV.",
            "XV."});
            this.NTDUroven.Location = new System.Drawing.Point(153, 78);
            this.NTDUroven.Name = "NTDUroven";
            this.NTDUroven.Size = new System.Drawing.Size(178, 21);
            this.NTDUroven.TabIndex = 3;
            // 
            // NTDN
            // 
            this.NTDN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDN.Enabled = false;
            this.NTDN.FormattingEnabled = true;
            this.NTDN.Items.AddRange(new object[] {
            "1.",
            "2.",
            "3.",
            "4.",
            "5.",
            "6.",
            "7.",
            "8.",
            "9.",
            "10.",
            "11.",
            "12.",
            "13.",
            "14.",
            "15."});
            this.NTDN.Location = new System.Drawing.Point(153, 132);
            this.NTDN.Name = "NTDN";
            this.NTDN.Size = new System.Drawing.Size(178, 21);
            this.NTDN.TabIndex = 5;
            // 
            // NTDND1
            // 
            this.NTDND1.Enabled = false;
            this.NTDND1.FormattingEnabled = true;
            this.NTDND1.Items.AddRange(new object[] {
            "ASTM",
            "ISO",
            "АСТМ",
            "ГОСТ",
            "ГОСТ Р",
            "Журнал",
            "МВИ",
            "МИ",
            "ПР",
            "ПРЖ/А-04-",
            "Приказ",
            "Р",
            "РД",
            "РМГ",
            "СНиП",
            "СП",
            "ТУ",
            "ФЗ"});
            this.NTDND1.Location = new System.Drawing.Point(153, 159);
            this.NTDND1.Name = "NTDND1";
            this.NTDND1.Size = new System.Drawing.Size(66, 21);
            this.NTDND1.Sorted = true;
            this.NTDND1.TabIndex = 6;
            // 
            // NTDND2
            // 
            this.NTDND2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDND2.Location = new System.Drawing.Point(225, 159);
            this.NTDND2.Name = "NTDND2";
            this.NTDND2.ReadOnly = true;
            this.NTDND2.Size = new System.Drawing.Size(106, 21);
            this.NTDND2.TabIndex = 7;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(120, 162);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(26, 13);
            this.label132.TabIndex = 48;
            this.label132.Text = "НД:";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(99, 108);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(47, 13);
            this.label131.TabIndex = 46;
            this.label131.Text = "Группа:";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(34, 81);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(112, 13);
            this.label130.TabIndex = 44;
            this.label130.Text = "Уровень документа:";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(74, 469);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(72, 13);
            this.label111.TabIndex = 43;
            this.label111.Text = "Примечание:";
            // 
            // NTDPrimechanie
            // 
            this.NTDPrimechanie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDPrimechanie.Location = new System.Drawing.Point(152, 466);
            this.NTDPrimechanie.Multiline = true;
            this.NTDPrimechanie.Name = "NTDPrimechanie";
            this.NTDPrimechanie.ReadOnly = true;
            this.NTDPrimechanie.Size = new System.Drawing.Size(178, 38);
            this.NTDPrimechanie.TabIndex = 20;
            // 
            // NTDKontrolMesto
            // 
            this.NTDKontrolMesto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDKontrolMesto.Enabled = false;
            this.NTDKontrolMesto.FormattingEnabled = true;
            this.NTDKontrolMesto.Items.AddRange(new object[] {
            "Аналитическая лаборатория",
            "Весовая",
            "Кабинет начальника лаборатории",
            "Комната определения сероводорода",
            "Комната определения фракционного состава",
            "Комната приема проб",
            "Комната хранения посуды",
            "Склад хранения химических реактивов и арбитражных проб"});
            this.NTDKontrolMesto.Location = new System.Drawing.Point(153, 312);
            this.NTDKontrolMesto.Name = "NTDKontrolMesto";
            this.NTDKontrolMesto.Size = new System.Drawing.Size(177, 21);
            this.NTDKontrolMesto.Sorted = true;
            this.NTDKontrolMesto.TabIndex = 13;
            // 
            // NTDRabMesto
            // 
            this.NTDRabMesto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDRabMesto.Enabled = false;
            this.NTDRabMesto.FormattingEnabled = true;
            this.NTDRabMesto.Items.AddRange(new object[] {
            "Аналитическая лаборатория",
            "Весовая",
            "Кабинет начальника лаборатории",
            "Комната определения сероводорода",
            "Комната определения фракционного состава",
            "Комната приема проб",
            "Комната хранения посуды",
            "Склад хранения химических реактивов и арбитражных проб"});
            this.NTDRabMesto.Location = new System.Drawing.Point(153, 385);
            this.NTDRabMesto.Name = "NTDRabMesto";
            this.NTDRabMesto.Size = new System.Drawing.Size(178, 21);
            this.NTDRabMesto.Sorted = true;
            this.NTDRabMesto.TabIndex = 15;
            // 
            // NTDCopyButton
            // 
            this.NTDCopyButton.AllowDrop = true;
            this.NTDCopyButton.AutoSize = true;
            this.NTDCopyButton.Image = global::LIMS.Properties.Resources.copy_16x16;
            this.NTDCopyButton.Location = new System.Drawing.Point(242, 50);
            this.NTDCopyButton.Name = "NTDCopyButton";
            this.NTDCopyButton.Size = new System.Drawing.Size(89, 22);
            this.NTDCopyButton.TabIndex = 2;
            this.NTDCopyButton.Text = "Копировать";
            this.NTDCopyButton.Click += new System.EventHandler(this.NTDCopyButton_Click);
            // 
            // NTDSrokDeisrvia
            // 
            this.NTDSrokDeisrvia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDSrokDeisrvia.Checked = false;
            this.NTDSrokDeisrvia.Enabled = false;
            this.NTDSrokDeisrvia.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.NTDSrokDeisrvia.Location = new System.Drawing.Point(213, 439);
            this.NTDSrokDeisrvia.Name = "NTDSrokDeisrvia";
            this.NTDSrokDeisrvia.Size = new System.Drawing.Size(118, 21);
            this.NTDSrokDeisrvia.TabIndex = 19;
            // 
            // NTDDateAktualiz
            // 
            this.NTDDateAktualiz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDDateAktualiz.Checked = false;
            this.NTDDateAktualiz.Enabled = false;
            this.NTDDateAktualiz.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.NTDDateAktualiz.Location = new System.Drawing.Point(213, 412);
            this.NTDDateAktualiz.Name = "NTDDateAktualiz";
            this.NTDDateAktualiz.Size = new System.Drawing.Size(118, 21);
            this.NTDDateAktualiz.TabIndex = 17;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(44, 222);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(103, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "по журналу учета:";
            // 
            // NTDName
            // 
            this.NTDName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDName.Location = new System.Drawing.Point(153, 186);
            this.NTDName.Name = "NTDName";
            this.NTDName.ReadOnly = true;
            this.NTDName.Size = new System.Drawing.Size(178, 21);
            this.NTDName.TabIndex = 8;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(39, 388);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(108, 13);
            this.label35.TabIndex = 33;
            this.label35.Text = "Место нахождения:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(76, 360);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(71, 13);
            this.label36.TabIndex = 32;
            this.label36.Text = "Количество:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label34.Location = new System.Drawing.Point(62, 340);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(202, 13);
            this.label34.TabIndex = 31;
            this.label34.Text = "Наличие рабочего экземпляра (копия)";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label44.Location = new System.Drawing.Point(69, 267);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(185, 13);
            this.label44.TabIndex = 30;
            this.label44.Text = "Наличие контрольного экземпляра";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label43.Location = new System.Drawing.Point(161, 27);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(90, 16);
            this.label43.TabIndex = 29;
            this.label43.Text = "документация";
            // 
            // NTDDateUtvergdenia
            // 
            this.NTDDateUtvergdenia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDDateUtvergdenia.Checked = false;
            this.NTDDateUtvergdenia.Enabled = false;
            this.NTDDateUtvergdenia.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.NTDDateUtvergdenia.Location = new System.Drawing.Point(213, 239);
            this.NTDDateUtvergdenia.Name = "NTDDateUtvergdenia";
            this.NTDDateUtvergdenia.Size = new System.Drawing.Size(118, 21);
            this.NTDDateUtvergdenia.TabIndex = 11;
            // 
            // NTDID
            // 
            this.NTDID.AutoSize = true;
            this.NTDID.Location = new System.Drawing.Point(260, 25);
            this.NTDID.Name = "NTDID";
            this.NTDID.Size = new System.Drawing.Size(0, 13);
            this.NTDID.TabIndex = 27;
            this.NTDID.Visible = false;
            // 
            // NTDKontrolKolvo
            // 
            this.NTDKontrolKolvo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDKontrolKolvo.Enabled = false;
            this.NTDKontrolKolvo.Location = new System.Drawing.Point(153, 285);
            this.NTDKontrolKolvo.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.NTDKontrolKolvo.Name = "NTDKontrolKolvo";
            this.NTDKontrolKolvo.Size = new System.Drawing.Size(178, 21);
            this.NTDKontrolKolvo.TabIndex = 12;
            // 
            // NTDRegistN
            // 
            this.NTDRegistN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDRegistN.Location = new System.Drawing.Point(153, 212);
            this.NTDRegistN.Name = "NTDRegistN";
            this.NTDRegistN.ReadOnly = true;
            this.NTDRegistN.Size = new System.Drawing.Size(178, 21);
            this.NTDRegistN.TabIndex = 9;
            // 
            // NTDRabKolvo
            // 
            this.NTDRabKolvo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NTDRabKolvo.Enabled = false;
            this.NTDRabKolvo.Location = new System.Drawing.Point(153, 358);
            this.NTDRabKolvo.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.NTDRabKolvo.Name = "NTDRabKolvo";
            this.NTDRabKolvo.Size = new System.Drawing.Size(178, 21);
            this.NTDRabKolvo.TabIndex = 14;
            // 
            // NTDEditCheckBox
            // 
            this.NTDEditCheckBox.AutoSize = true;
            this.NTDEditCheckBox.Location = new System.Drawing.Point(152, 510);
            this.NTDEditCheckBox.Name = "NTDEditCheckBox";
            this.NTDEditCheckBox.Size = new System.Drawing.Size(105, 17);
            this.NTDEditCheckBox.TabIndex = 21;
            this.NTDEditCheckBox.Text = "Редактировать";
            this.NTDEditCheckBox.UseVisualStyleBackColor = true;
            this.NTDEditCheckBox.CheckedChanged += new System.EventHandler(this.NTDEditCheckBox_CheckedChanged);
            // 
            // NTDNewButton
            // 
            this.NTDNewButton.AllowDrop = true;
            this.NTDNewButton.AutoSize = true;
            this.NTDNewButton.Image = global::LIMS.Properties.Resources.add_16x16;
            this.NTDNewButton.Location = new System.Drawing.Point(153, 50);
            this.NTDNewButton.Name = "NTDNewButton";
            this.NTDNewButton.Size = new System.Drawing.Size(78, 22);
            this.NTDNewButton.TabIndex = 1;
            this.NTDNewButton.Text = "Добавить";
            this.NTDNewButton.Click += new System.EventHandler(this.NTDNewButton_Click);
            // 
            // NTDSaveButton
            // 
            this.NTDSaveButton.AutoSize = true;
            this.NTDSaveButton.Image = global::LIMS.Properties.Resources.save_16x16;
            this.NTDSaveButton.Location = new System.Drawing.Point(152, 533);
            this.NTDSaveButton.Name = "NTDSaveButton";
            this.NTDSaveButton.Size = new System.Drawing.Size(83, 22);
            this.NTDSaveButton.TabIndex = 22;
            this.NTDSaveButton.Text = "Сохранить";
            this.NTDSaveButton.Click += new System.EventHandler(this.NTDSaveButton_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(132, 11);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(157, 16);
            this.label31.TabIndex = 23;
            this.label31.Text = "Нормативно-техническая";
            // 
            // pictureBox3
            // 
            this.pictureBox3.ErrorImage = null;
            this.pictureBox3.Image = global::LIMS.Properties.Resources.documents;
            this.pictureBox3.Location = new System.Drawing.Point(27, 6);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(96, 67);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 11;
            this.pictureBox3.TabStop = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(59, 443);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(87, 13);
            this.label32.TabIndex = 10;
            this.label32.Text = "Срок действия:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(11, 416);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(141, 13);
            this.label33.TabIndex = 9;
            this.label33.Text = "Дата посл. актуализации:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(38, 315);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(108, 13);
            this.label37.TabIndex = 5;
            this.label37.Text = "Место нахождения:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(76, 287);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(71, 13);
            this.label38.TabIndex = 4;
            this.label38.Text = "Количество:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(37, 244);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(109, 13);
            this.label39.TabIndex = 3;
            this.label39.Text = "Дата утверждения:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(17, 209);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(130, 13);
            this.label40.TabIndex = 2;
            this.label40.Text = "Регистрационный номер";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(5, 189);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(142, 13);
            this.label41.TabIndex = 1;
            this.label41.Text = "Наименование документа:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(103, 135);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(43, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "№ п/п:";
            // 
            // NTDGrid
            // 
            this.NTDGrid.AccessibleDescription = "";
            this.NTDGrid.AccessibleName = "";
            this.NTDGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NTDGrid.Location = new System.Drawing.Point(0, 40);
            this.NTDGrid.MainView = this.NTDGridView;
            this.NTDGrid.Name = "NTDGrid";
            this.NTDGrid.Size = new System.Drawing.Size(647, 663);
            this.NTDGrid.TabIndex = 99999;
            this.NTDGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.NTDGridView});
            this.NTDGrid.Click += new System.EventHandler(this.NTDGrid_Click);
            // 
            // NTDGridView
            // 
            this.NTDGridView.Appearance.FixedLine.Options.UseTextOptions = true;
            this.NTDGridView.Appearance.FixedLine.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NTDGridView.Appearance.FixedLine.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NTDGridView.Appearance.FixedLine.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NTDGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.NTDGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NTDGridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.NTDGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NTDGridView.GridControl = this.NTDGrid;
            this.NTDGridView.Name = "NTDGridView";
            this.NTDGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.NTDGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.NTDGridView.OptionsBehavior.Editable = false;
            this.NTDGridView.OptionsFind.AlwaysVisible = true;
            this.NTDGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.NTDGridView.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.NTDGridView_RowClick);
            this.NTDGridView.ColumnWidthChanged += new DevExpress.XtraGrid.Views.Base.ColumnEventHandler(this.NTDGridView_ColumnWidthChanged);
            this.NTDGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.NTDGridView_PopupMenuShowing);
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.NTDCellMergeButton);
            this.panelControl15.Controls.Add(this.NTDArhivButton);
            this.panelControl15.Controls.Add(this.NTDFiltrButton);
            this.panelControl15.Controls.Add(this.NTDColumnCheckButton);
            this.panelControl15.Controls.Add(this.NTDUpdateButton);
            this.panelControl15.Controls.Add(this.NTDPrintButton);
            this.panelControl15.Controls.Add(this.NTDDeleteButton);
            this.panelControl15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl15.Location = new System.Drawing.Point(0, 0);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(647, 40);
            this.panelControl15.TabIndex = 0;
            // 
            // NTDCellMergeButton
            // 
            this.NTDCellMergeButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.NTDCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            this.NTDCellMergeButton.Location = new System.Drawing.Point(162, 2);
            this.NTDCellMergeButton.Name = "NTDCellMergeButton";
            this.NTDCellMergeButton.Size = new System.Drawing.Size(40, 36);
            this.NTDCellMergeButton.TabIndex = 27;
            this.NTDCellMergeButton.Click += new System.EventHandler(this.NTDCellMergeButton_Click);
            // 
            // NTDArhivButton
            // 
            this.NTDArhivButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.NTDArhivButton.Image = global::LIMS.Properties.Resources.checkbuttons_32x32;
            this.NTDArhivButton.Location = new System.Drawing.Point(122, 2);
            this.NTDArhivButton.Name = "NTDArhivButton";
            this.NTDArhivButton.Size = new System.Drawing.Size(40, 36);
            this.NTDArhivButton.TabIndex = 26;
            this.NTDArhivButton.Click += new System.EventHandler(this.NTDVisibleArhivButton_Click_1);
            // 
            // NTDFiltrButton
            // 
            this.NTDFiltrButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.NTDFiltrButton.Image = global::LIMS.Properties.Resources.masterfilter_32x32;
            this.NTDFiltrButton.Location = new System.Drawing.Point(82, 2);
            this.NTDFiltrButton.Name = "NTDFiltrButton";
            this.NTDFiltrButton.Size = new System.Drawing.Size(40, 36);
            this.NTDFiltrButton.TabIndex = 25;
            this.NTDFiltrButton.Visible = false;
            this.NTDFiltrButton.Click += new System.EventHandler(this.NTDFiltrButton_Click);
            // 
            // NTDColumnCheckButton
            // 
            this.NTDColumnCheckButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.NTDColumnCheckButton.Image = global::LIMS.Properties.Resources.insertcolumns_32x32;
            this.NTDColumnCheckButton.Location = new System.Drawing.Point(565, 2);
            this.NTDColumnCheckButton.Name = "NTDColumnCheckButton";
            this.NTDColumnCheckButton.Size = new System.Drawing.Size(40, 36);
            this.NTDColumnCheckButton.TabIndex = 28;
            this.NTDColumnCheckButton.Click += new System.EventHandler(this.NTDColumnCheckButton_Click);
            // 
            // NTDUpdateButton
            // 
            this.NTDUpdateButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.NTDUpdateButton.Image = global::LIMS.Properties.Resources.refresh_32x32;
            this.NTDUpdateButton.Location = new System.Drawing.Point(605, 2);
            this.NTDUpdateButton.Name = "NTDUpdateButton";
            this.NTDUpdateButton.Size = new System.Drawing.Size(40, 36);
            this.NTDUpdateButton.TabIndex = 29;
            this.NTDUpdateButton.Click += new System.EventHandler(this.NTDUpdateButton_Click);
            // 
            // NTDPrintButton
            // 
            this.NTDPrintButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.NTDPrintButton.Image = global::LIMS.Properties.Resources.printer_32x32;
            this.NTDPrintButton.Location = new System.Drawing.Point(42, 2);
            this.NTDPrintButton.Name = "NTDPrintButton";
            this.NTDPrintButton.Size = new System.Drawing.Size(40, 36);
            this.NTDPrintButton.TabIndex = 24;
            this.NTDPrintButton.Click += new System.EventHandler(this.NTDPrintButton_Click);
            // 
            // NTDDeleteButton
            // 
            this.NTDDeleteButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.NTDDeleteButton.Image = global::LIMS.Properties.Resources.delete_32x32;
            this.NTDDeleteButton.Location = new System.Drawing.Point(2, 2);
            this.NTDDeleteButton.Name = "NTDDeleteButton";
            this.NTDDeleteButton.Size = new System.Drawing.Size(40, 36);
            this.NTDDeleteButton.TabIndex = 23;
            this.NTDDeleteButton.Click += new System.EventHandler(this.NTDDeleteButton_Click);
            // 
            // SI
            // 
            this.SI.Controls.Add(this.splitContainerControl4);
            this.SI.Name = "SI";
            this.SI.Size = new System.Drawing.Size(988, 703);
            this.SI.Text = "СИ";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.SIPanel);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel1.SizeChanged += new System.EventHandler(this.splitContainerControl4_Panel1_SizeChanged);
            this.splitContainerControl4.Panel2.Controls.Add(this.SIGrid);
            this.splitContainerControl4.Panel2.Controls.Add(this.panelControl10);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(988, 703);
            this.splitContainerControl4.SplitterPosition = 360;
            this.splitContainerControl4.TabIndex = 3;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // SIPanel
            // 
            this.SIPanel.Controls.Add(this.SIPravoSobstvennosti);
            this.SIPanel.Controls.Add(this.label112);
            this.SIPanel.Controls.Add(this.SIDataPoverki);
            this.SIPanel.Controls.Add(this.SIDataPosledTOTip);
            this.SIPanel.Controls.Add(this.SIDataPoverkiTip);
            this.SIPanel.Controls.Add(this.SIDataSledTOKol);
            this.SIPanel.Controls.Add(this.SIDateSledPoverkiKol);
            this.SIPanel.Controls.Add(this.SIDateSledPoverkiTip);
            this.SIPanel.Controls.Add(this.SIDataSledTOTip);
            this.SIPanel.Controls.Add(this.SIMestoUstanovki);
            this.SIPanel.Controls.Add(this.SISrokDeystv);
            this.SIPanel.Controls.Add(this.SIDataSledTODate);
            this.SIPanel.Controls.Add(this.SIDataPosledTO);
            this.SIPanel.Controls.Add(this.label45);
            this.SIPanel.Controls.Add(this.SIN);
            this.SIPanel.Controls.Add(this.label95);
            this.SIPanel.Controls.Add(this.SITip);
            this.SIPanel.Controls.Add(this.label93);
            this.SIPanel.Controls.Add(this.SIZavodNomer);
            this.SIPanel.Controls.Add(this.label92);
            this.SIPanel.Controls.Add(this.SIGod);
            this.SIPanel.Controls.Add(this.SIInventarNomer);
            this.SIPanel.Controls.Add(this.label51);
            this.SIPanel.Controls.Add(this.label91);
            this.SIPanel.Controls.Add(this.label89);
            this.SIPanel.Controls.Add(this.label86);
            this.SIPanel.Controls.Add(this.label82);
            this.SIPanel.Controls.Add(this.SICopyButton);
            this.SIPanel.Controls.Add(this.SIPrimechanie);
            this.SIPanel.Controls.Add(this.SISvidetelstvoOPoverke);
            this.SIPanel.Controls.Add(this.SIClassTochnosti);
            this.SIPanel.Controls.Add(this.SIVvodVEkspluatachiu);
            this.SIPanel.Controls.Add(this.label50);
            this.SIPanel.Controls.Add(this.SIIzgotovitel);
            this.SIPanel.Controls.Add(this.label49);
            this.SIPanel.Controls.Add(this.SINameSI);
            this.SIPanel.Controls.Add(this.label48);
            this.SIPanel.Controls.Add(this.SIDateSledPoverkiDate);
            this.SIPanel.Controls.Add(this.label46);
            this.SIPanel.Controls.Add(this.label47);
            this.SIPanel.Controls.Add(this.SIID);
            this.SIPanel.Controls.Add(this.SIDiapozonIzm);
            this.SIPanel.Controls.Add(this.SINameProd);
            this.SIPanel.Controls.Add(this.SIEditCheckBox);
            this.SIPanel.Controls.Add(this.SINewButton);
            this.SIPanel.Controls.Add(this.SISaveButton);
            this.SIPanel.Controls.Add(this.label52);
            this.SIPanel.Controls.Add(this.pictureBox4);
            this.SIPanel.Controls.Add(this.label53);
            this.SIPanel.Controls.Add(this.label54);
            this.SIPanel.Controls.Add(this.label55);
            this.SIPanel.Controls.Add(this.label56);
            this.SIPanel.Controls.Add(this.label57);
            this.SIPanel.Controls.Add(this.label58);
            this.SIPanel.Controls.Add(this.label59);
            this.SIPanel.Controls.Add(this.label60);
            this.SIPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SIPanel.Location = new System.Drawing.Point(0, 0);
            this.SIPanel.Name = "SIPanel";
            this.SIPanel.Size = new System.Drawing.Size(360, 703);
            this.SIPanel.TabIndex = 0;
            // 
            // SIPravoSobstvennosti
            // 
            this.SIPravoSobstvennosti.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIPravoSobstvennosti.Enabled = false;
            this.SIPravoSobstvennosti.FormattingEnabled = true;
            this.SIPravoSobstvennosti.Items.AddRange(new object[] {
            "собственность",
            "собственность"});
            this.SIPravoSobstvennosti.Location = new System.Drawing.Point(179, 486);
            this.SIPravoSobstvennosti.Name = "SIPravoSobstvennosti";
            this.SIPravoSobstvennosti.Size = new System.Drawing.Size(176, 21);
            this.SIPravoSobstvennosti.Sorted = true;
            this.SIPravoSobstvennosti.TabIndex = 73;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(53, 489);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(120, 13);
            this.label112.TabIndex = 72;
            this.label112.Text = "Право собственности:";
            // 
            // SIDataPoverki
            // 
            this.SIDataPoverki.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIDataPoverki.Location = new System.Drawing.Point(239, 405);
            this.SIDataPoverki.Name = "SIDataPoverki";
            this.SIDataPoverki.ReadOnly = true;
            this.SIDataPoverki.Size = new System.Drawing.Size(116, 21);
            this.SIDataPoverki.TabIndex = 16;
            // 
            // SIDataPosledTOTip
            // 
            this.SIDataPosledTOTip.Enabled = false;
            this.SIDataPosledTOTip.FormattingEnabled = true;
            this.SIDataPosledTOTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.SIDataPosledTOTip.Location = new System.Drawing.Point(179, 540);
            this.SIDataPosledTOTip.Name = "SIDataPosledTOTip";
            this.SIDataPosledTOTip.Size = new System.Drawing.Size(54, 21);
            this.SIDataPosledTOTip.Sorted = true;
            this.SIDataPosledTOTip.TabIndex = 21;
            this.SIDataPosledTOTip.Text = "дата";
            this.SIDataPosledTOTip.SelectedIndexChanged += new System.EventHandler(this.SIDataPosledTOTip_SelectedIndexChanged);
            // 
            // SIDataPoverkiTip
            // 
            this.SIDataPoverkiTip.Enabled = false;
            this.SIDataPoverkiTip.FormattingEnabled = true;
            this.SIDataPoverkiTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.SIDataPoverkiTip.Location = new System.Drawing.Point(179, 405);
            this.SIDataPoverkiTip.Name = "SIDataPoverkiTip";
            this.SIDataPoverkiTip.Size = new System.Drawing.Size(54, 21);
            this.SIDataPoverkiTip.Sorted = true;
            this.SIDataPoverkiTip.TabIndex = 15;
            this.SIDataPoverkiTip.Text = "дата";
            this.SIDataPoverkiTip.SelectedIndexChanged += new System.EventHandler(this.SIDataPoverkiTip_SelectedIndexChanged);
            // 
            // SIDataSledTOKol
            // 
            this.SIDataSledTOKol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIDataSledTOKol.Location = new System.Drawing.Point(239, 570);
            this.SIDataSledTOKol.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.SIDataSledTOKol.Name = "SIDataSledTOKol";
            this.SIDataSledTOKol.ReadOnly = true;
            this.SIDataSledTOKol.Size = new System.Drawing.Size(116, 21);
            this.SIDataSledTOKol.TabIndex = 24;
            this.SIDataSledTOKol.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.SIDataSledTOKol.Visible = false;
            // 
            // SIDateSledPoverkiKol
            // 
            this.SIDateSledPoverkiKol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIDateSledPoverkiKol.Location = new System.Drawing.Point(239, 459);
            this.SIDateSledPoverkiKol.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.SIDateSledPoverkiKol.Name = "SIDateSledPoverkiKol";
            this.SIDateSledPoverkiKol.ReadOnly = true;
            this.SIDateSledPoverkiKol.Size = new System.Drawing.Size(116, 21);
            this.SIDateSledPoverkiKol.TabIndex = 19;
            this.SIDateSledPoverkiKol.Visible = false;
            // 
            // SIDateSledPoverkiTip
            // 
            this.SIDateSledPoverkiTip.Enabled = false;
            this.SIDateSledPoverkiTip.FormattingEnabled = true;
            this.SIDateSledPoverkiTip.Items.AddRange(new object[] {
            "дата",
            "мес.",
            "нет"});
            this.SIDateSledPoverkiTip.Location = new System.Drawing.Point(179, 459);
            this.SIDateSledPoverkiTip.Name = "SIDateSledPoverkiTip";
            this.SIDateSledPoverkiTip.Size = new System.Drawing.Size(54, 21);
            this.SIDateSledPoverkiTip.Sorted = true;
            this.SIDateSledPoverkiTip.TabIndex = 18;
            this.SIDateSledPoverkiTip.Text = "дата";
            this.SIDateSledPoverkiTip.SelectedIndexChanged += new System.EventHandler(this.SIDateSledPoverkiTip_SelectedIndexChanged);
            // 
            // SIDataSledTOTip
            // 
            this.SIDataSledTOTip.Enabled = false;
            this.SIDataSledTOTip.FormattingEnabled = true;
            this.SIDataSledTOTip.Items.AddRange(new object[] {
            "дата",
            "мес.",
            "нет"});
            this.SIDataSledTOTip.Location = new System.Drawing.Point(179, 570);
            this.SIDataSledTOTip.Name = "SIDataSledTOTip";
            this.SIDataSledTOTip.Size = new System.Drawing.Size(54, 21);
            this.SIDataSledTOTip.Sorted = true;
            this.SIDataSledTOTip.TabIndex = 23;
            this.SIDataSledTOTip.Text = "дата";
            this.SIDataSledTOTip.SelectedIndexChanged += new System.EventHandler(this.SIDataSledTOTip_SelectedIndexChanged);
            // 
            // SIMestoUstanovki
            // 
            this.SIMestoUstanovki.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIMestoUstanovki.Enabled = false;
            this.SIMestoUstanovki.FormattingEnabled = true;
            this.SIMestoUstanovki.Items.AddRange(new object[] {
            "Аналитическая лаборатория",
            "Весовая",
            "Комната определения сероводорода",
            "Комната определения фракционного состава",
            "Комната приема проб",
            "Комната хранения хим. посуды",
            "Склад хранения арбитражных проб и хим. реактивов"});
            this.SIMestoUstanovki.Location = new System.Drawing.Point(179, 513);
            this.SIMestoUstanovki.Name = "SIMestoUstanovki";
            this.SIMestoUstanovki.Size = new System.Drawing.Size(176, 21);
            this.SIMestoUstanovki.Sorted = true;
            this.SIMestoUstanovki.TabIndex = 20;
            // 
            // SISrokDeystv
            // 
            this.SISrokDeystv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SISrokDeystv.Enabled = false;
            this.SISrokDeystv.FormattingEnabled = true;
            this.SISrokDeystv.Items.AddRange(new object[] {
            "12 мес.",
            "18 мес.",
            "24 мес.",
            "36 мес.",
            "48 мес.",
            "60 мес."});
            this.SISrokDeystv.Location = new System.Drawing.Point(179, 432);
            this.SISrokDeystv.Name = "SISrokDeystv";
            this.SISrokDeystv.Size = new System.Drawing.Size(176, 21);
            this.SISrokDeystv.Sorted = true;
            this.SISrokDeystv.TabIndex = 17;
            this.SISrokDeystv.TextChanged += new System.EventHandler(this.SISrokDeystv_TextChanged);
            // 
            // SIDataSledTODate
            // 
            this.SIDataSledTODate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIDataSledTODate.Enabled = false;
            this.SIDataSledTODate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.SIDataSledTODate.Location = new System.Drawing.Point(239, 570);
            this.SIDataSledTODate.Name = "SIDataSledTODate";
            this.SIDataSledTODate.Size = new System.Drawing.Size(116, 21);
            this.SIDataSledTODate.TabIndex = 71;
            // 
            // SIDataPosledTO
            // 
            this.SIDataPosledTO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIDataPosledTO.Enabled = false;
            this.SIDataPosledTO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.SIDataPosledTO.Location = new System.Drawing.Point(239, 540);
            this.SIDataPosledTO.Name = "SIDataPosledTO";
            this.SIDataPosledTO.Size = new System.Drawing.Size(116, 21);
            this.SIDataPosledTO.TabIndex = 22;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(130, 83);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(43, 13);
            this.label45.TabIndex = 69;
            this.label45.Text = "№ п/п:";
            // 
            // SIN
            // 
            this.SIN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIN.Location = new System.Drawing.Point(179, 81);
            this.SIN.Name = "SIN";
            this.SIN.ReadOnly = true;
            this.SIN.Size = new System.Drawing.Size(176, 21);
            this.SIN.TabIndex = 3;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(103, 165);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(70, 13);
            this.label95.TabIndex = 67;
            this.label95.Text = "Тип (марка):";
            // 
            // SITip
            // 
            this.SITip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SITip.Location = new System.Drawing.Point(179, 162);
            this.SITip.Name = "SITip";
            this.SITip.ReadOnly = true;
            this.SITip.Size = new System.Drawing.Size(176, 21);
            this.SITip.TabIndex = 6;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(59, 192);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(114, 13);
            this.label93.TabIndex = 65;
            this.label93.Text = "Заводской номер №:";
            // 
            // SIZavodNomer
            // 
            this.SIZavodNomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIZavodNomer.Location = new System.Drawing.Point(179, 189);
            this.SIZavodNomer.Name = "SIZavodNomer";
            this.SIZavodNomer.ReadOnly = true;
            this.SIZavodNomer.Size = new System.Drawing.Size(176, 21);
            this.SIZavodNomer.TabIndex = 7;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(97, 219);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(76, 13);
            this.label92.TabIndex = 63;
            this.label92.Text = "Год выпуска:";
            // 
            // SIGod
            // 
            this.SIGod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIGod.Location = new System.Drawing.Point(179, 216);
            this.SIGod.Name = "SIGod";
            this.SIGod.ReadOnly = true;
            this.SIGod.Size = new System.Drawing.Size(176, 21);
            this.SIGod.TabIndex = 8;
            // 
            // SIInventarNomer
            // 
            this.SIInventarNomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIInventarNomer.Location = new System.Drawing.Point(179, 297);
            this.SIInventarNomer.Name = "SIInventarNomer";
            this.SIInventarNomer.ReadOnly = true;
            this.SIInventarNomer.Size = new System.Drawing.Size(176, 21);
            this.SIInventarNomer.TabIndex = 11;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(132, 388);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(41, 13);
            this.label51.TabIndex = 60;
            this.label51.Text = "номер:";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(91, 408);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(82, 13);
            this.label91.TabIndex = 59;
            this.label91.Text = "Дата поверки:";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(28, 435);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(146, 13);
            this.label89.TabIndex = 57;
            this.label89.Text = "Межповерочный интервал:";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(57, 546);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(116, 13);
            this.label86.TabIndex = 55;
            this.label86.Text = "Дата последнего ТО:";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(51, 573);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(122, 13);
            this.label82.TabIndex = 53;
            this.label82.Text = "Дата следующего ТО:";
            // 
            // SICopyButton
            // 
            this.SICopyButton.AllowDrop = true;
            this.SICopyButton.AutoSize = true;
            this.SICopyButton.Image = global::LIMS.Properties.Resources.copy_16x16;
            this.SICopyButton.Location = new System.Drawing.Point(268, 50);
            this.SICopyButton.Name = "SICopyButton";
            this.SICopyButton.Size = new System.Drawing.Size(89, 22);
            this.SICopyButton.TabIndex = 2;
            this.SICopyButton.Text = "Копировать";
            this.SICopyButton.Click += new System.EventHandler(this.SICopyButton_Click);
            // 
            // SIPrimechanie
            // 
            this.SIPrimechanie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIPrimechanie.Location = new System.Drawing.Point(179, 597);
            this.SIPrimechanie.Name = "SIPrimechanie";
            this.SIPrimechanie.ReadOnly = true;
            this.SIPrimechanie.Size = new System.Drawing.Size(176, 21);
            this.SIPrimechanie.TabIndex = 25;
            // 
            // SISvidetelstvoOPoverke
            // 
            this.SISvidetelstvoOPoverke.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SISvidetelstvoOPoverke.Location = new System.Drawing.Point(179, 378);
            this.SISvidetelstvoOPoverke.Name = "SISvidetelstvoOPoverke";
            this.SISvidetelstvoOPoverke.ReadOnly = true;
            this.SISvidetelstvoOPoverke.Size = new System.Drawing.Size(176, 21);
            this.SISvidetelstvoOPoverke.TabIndex = 14;
            // 
            // SIClassTochnosti
            // 
            this.SIClassTochnosti.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIClassTochnosti.Location = new System.Drawing.Point(179, 351);
            this.SIClassTochnosti.Name = "SIClassTochnosti";
            this.SIClassTochnosti.ReadOnly = true;
            this.SIClassTochnosti.Size = new System.Drawing.Size(176, 21);
            this.SIClassTochnosti.TabIndex = 13;
            // 
            // SIVvodVEkspluatachiu
            // 
            this.SIVvodVEkspluatachiu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIVvodVEkspluatachiu.Location = new System.Drawing.Point(179, 270);
            this.SIVvodVEkspluatachiu.Name = "SIVvodVEkspluatachiu";
            this.SIVvodVEkspluatachiu.ReadOnly = true;
            this.SIVvodVEkspluatachiu.Size = new System.Drawing.Size(176, 21);
            this.SIVvodVEkspluatachiu.TabIndex = 10;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(44, 300);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(129, 13);
            this.label50.TabIndex = 44;
            this.label50.Text = "Инвентарный номер №:";
            // 
            // SIIzgotovitel
            // 
            this.SIIzgotovitel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIIzgotovitel.Location = new System.Drawing.Point(179, 243);
            this.SIIzgotovitel.Name = "SIIzgotovitel";
            this.SIIzgotovitel.ReadOnly = true;
            this.SIIzgotovitel.Size = new System.Drawing.Size(176, 21);
            this.SIIzgotovitel.TabIndex = 9;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(52, 251);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(121, 13);
            this.label49.TabIndex = 42;
            this.label49.Text = "предприятие, фирма):";
            // 
            // SINameSI
            // 
            this.SINameSI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SINameSI.Location = new System.Drawing.Point(179, 135);
            this.SINameSI.Name = "SINameSI";
            this.SINameSI.ReadOnly = true;
            this.SINameSI.Size = new System.Drawing.Size(176, 21);
            this.SINameSI.TabIndex = 5;
            // 
            // label48
            // 
            this.label48.AutoEllipsis = true;
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(27, 116);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(146, 13);
            this.label48.TabIndex = 40;
            this.label48.Text = "характеристик продукции:";
            // 
            // SIDateSledPoverkiDate
            // 
            this.SIDateSledPoverkiDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIDateSledPoverkiDate.Enabled = false;
            this.SIDateSledPoverkiDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.SIDateSledPoverkiDate.Location = new System.Drawing.Point(239, 459);
            this.SIDateSledPoverkiDate.Name = "SIDateSledPoverkiDate";
            this.SIDateSledPoverkiDate.Size = new System.Drawing.Size(116, 21);
            this.SIDateSledPoverkiDate.TabIndex = 38;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(28, 462);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(145, 13);
            this.label46.TabIndex = 33;
            this.label46.Text = "Дата следующей поверки:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(12, 375);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(161, 13);
            this.label47.TabIndex = 32;
            this.label47.Text = "Свидетельство о поверке СИ,";
            // 
            // SIID
            // 
            this.SIID.AutoSize = true;
            this.SIID.Location = new System.Drawing.Point(260, 25);
            this.SIID.Name = "SIID";
            this.SIID.Size = new System.Drawing.Size(0, 13);
            this.SIID.TabIndex = 27;
            this.SIID.Visible = false;
            // 
            // SIDiapozonIzm
            // 
            this.SIDiapozonIzm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SIDiapozonIzm.Location = new System.Drawing.Point(179, 324);
            this.SIDiapozonIzm.Name = "SIDiapozonIzm";
            this.SIDiapozonIzm.ReadOnly = true;
            this.SIDiapozonIzm.Size = new System.Drawing.Size(176, 21);
            this.SIDiapozonIzm.TabIndex = 12;
            // 
            // SINameProd
            // 
            this.SINameProd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SINameProd.Location = new System.Drawing.Point(179, 108);
            this.SINameProd.Name = "SINameProd";
            this.SINameProd.ReadOnly = true;
            this.SINameProd.Size = new System.Drawing.Size(176, 21);
            this.SINameProd.TabIndex = 4;
            // 
            // SIEditCheckBox
            // 
            this.SIEditCheckBox.AutoSize = true;
            this.SIEditCheckBox.Location = new System.Drawing.Point(179, 624);
            this.SIEditCheckBox.Name = "SIEditCheckBox";
            this.SIEditCheckBox.Size = new System.Drawing.Size(105, 17);
            this.SIEditCheckBox.TabIndex = 26;
            this.SIEditCheckBox.Text = "Редактировать";
            this.SIEditCheckBox.UseVisualStyleBackColor = true;
            this.SIEditCheckBox.CheckedChanged += new System.EventHandler(this.SIEditCheckBox_CheckedChanged);
            // 
            // SINewButton
            // 
            this.SINewButton.AllowDrop = true;
            this.SINewButton.AutoSize = true;
            this.SINewButton.Image = global::LIMS.Properties.Resources.add_16x16;
            this.SINewButton.Location = new System.Drawing.Point(179, 50);
            this.SINewButton.Name = "SINewButton";
            this.SINewButton.Size = new System.Drawing.Size(78, 22);
            this.SINewButton.TabIndex = 1;
            this.SINewButton.Text = "Добавить";
            this.SINewButton.Click += new System.EventHandler(this.SINewButton_Click);
            // 
            // SISaveButton
            // 
            this.SISaveButton.AutoSize = true;
            this.SISaveButton.Image = global::LIMS.Properties.Resources.save_16x16;
            this.SISaveButton.Location = new System.Drawing.Point(179, 647);
            this.SISaveButton.Name = "SISaveButton";
            this.SISaveButton.Size = new System.Drawing.Size(83, 22);
            this.SISaveButton.TabIndex = 27;
            this.SISaveButton.Text = "Сохранить";
            this.SISaveButton.Click += new System.EventHandler(this.SISaveButton_Click);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label52.Location = new System.Drawing.Point(185, 11);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(130, 16);
            this.label52.TabIndex = 23;
            this.label52.Text = "Средства измерения";
            // 
            // pictureBox4
            // 
            this.pictureBox4.ErrorImage = null;
            this.pictureBox4.Image = global::LIMS.Properties.Resources.image;
            this.pictureBox4.Location = new System.Drawing.Point(42, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(106, 75);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(101, 600);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(72, 13);
            this.label53.TabIndex = 10;
            this.label53.Text = "Примечание:";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(3, 516);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(170, 13);
            this.label54.TabIndex = 9;
            this.label54.Text = "Место установки или хранения:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(10, 354);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(163, 13);
            this.label55.TabIndex = 5;
            this.label55.Text = "Класс точности, погрешность:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(57, 327);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(116, 13);
            this.label56.TabIndex = 4;
            this.label56.Text = "Диапозон измерений:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(24, 273);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(149, 13);
            this.label57.TabIndex = 3;
            this.label57.Text = "Год ввода в эксплуатацию:";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(49, 238);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(124, 13);
            this.label58.TabIndex = 2;
            this.label58.Text = "Изготовитель (страна,";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(72, 138);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(101, 13);
            this.label59.TabIndex = 1;
            this.label59.Text = "Наименование СИ:";
            // 
            // label60
            // 
            this.label60.AutoEllipsis = true;
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(21, 103);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(152, 13);
            this.label60.TabIndex = 0;
            this.label60.Text = "Наименование определямых";
            // 
            // SIGrid
            // 
            this.SIGrid.AccessibleDescription = "";
            this.SIGrid.AccessibleName = "";
            this.SIGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SIGrid.Location = new System.Drawing.Point(0, 40);
            this.SIGrid.MainView = this.SIGridView;
            this.SIGrid.Name = "SIGrid";
            this.SIGrid.Size = new System.Drawing.Size(623, 663);
            this.SIGrid.TabIndex = 9999999;
            this.SIGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.SIGridView});
            this.SIGrid.Click += new System.EventHandler(this.SIGrid_Click);
            // 
            // SIGridView
            // 
            this.SIGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.SIGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SIGridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SIGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SIGridView.AppearancePrint.EvenRow.Options.UseTextOptions = true;
            this.SIGridView.AppearancePrint.EvenRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SIGridView.AppearancePrint.GroupRow.Options.UseTextOptions = true;
            this.SIGridView.AppearancePrint.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SIGridView.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.SIGridView.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SIGridView.AppearancePrint.Lines.Options.UseTextOptions = true;
            this.SIGridView.AppearancePrint.Lines.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SIGridView.AppearancePrint.OddRow.Options.UseTextOptions = true;
            this.SIGridView.AppearancePrint.OddRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SIGridView.AppearancePrint.Preview.Options.UseTextOptions = true;
            this.SIGridView.AppearancePrint.Preview.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SIGridView.AppearancePrint.Row.Options.UseTextOptions = true;
            this.SIGridView.AppearancePrint.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SIGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.SIGridView.GridControl = this.SIGrid;
            this.SIGridView.Name = "SIGridView";
            this.SIGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.SIGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.SIGridView.OptionsBehavior.AllowIncrementalSearch = true;
            this.SIGridView.OptionsBehavior.Editable = false;
            this.SIGridView.OptionsCustomization.AllowRowSizing = true;
            this.SIGridView.OptionsFind.AlwaysVisible = true;
            this.SIGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.SIGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.SIGridView.OptionsView.RowAutoHeight = true;
            this.SIGridView.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.SIGridView_RowClick);
            this.SIGridView.ColumnWidthChanged += new DevExpress.XtraGrid.Views.Base.ColumnEventHandler(this.SIGridView_ColumnWidthChanged);
            this.SIGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.SIGridView_PopupMenuShowing);
            this.SIGridView.StartSorting += new System.EventHandler(this.SIGridView_StartSorting);
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.SICellMergeButton);
            this.panelControl10.Controls.Add(this.SIArhivButton);
            this.panelControl10.Controls.Add(this.SIColumnCheckButton);
            this.panelControl10.Controls.Add(this.SIUpdateButton);
            this.panelControl10.Controls.Add(this.SIPrintButton);
            this.panelControl10.Controls.Add(this.SIDeleteButton);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl10.Location = new System.Drawing.Point(0, 0);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(623, 40);
            this.panelControl10.TabIndex = 0;
            // 
            // SICellMergeButton
            // 
            this.SICellMergeButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.SICellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            this.SICellMergeButton.Location = new System.Drawing.Point(122, 2);
            this.SICellMergeButton.Name = "SICellMergeButton";
            this.SICellMergeButton.Size = new System.Drawing.Size(40, 36);
            this.SICellMergeButton.TabIndex = 31;
            this.SICellMergeButton.Click += new System.EventHandler(this.SICellMergeButton_Click);
            // 
            // SIArhivButton
            // 
            this.SIArhivButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.SIArhivButton.Image = global::LIMS.Properties.Resources.checkbuttons_32x32;
            this.SIArhivButton.Location = new System.Drawing.Point(82, 2);
            this.SIArhivButton.Name = "SIArhivButton";
            this.SIArhivButton.Size = new System.Drawing.Size(40, 36);
            this.SIArhivButton.TabIndex = 30;
            this.SIArhivButton.Click += new System.EventHandler(this.SIArhivButton_Click_1);
            // 
            // SIColumnCheckButton
            // 
            this.SIColumnCheckButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.SIColumnCheckButton.Image = global::LIMS.Properties.Resources.insertcolumns_32x32;
            this.SIColumnCheckButton.Location = new System.Drawing.Point(541, 2);
            this.SIColumnCheckButton.Name = "SIColumnCheckButton";
            this.SIColumnCheckButton.Size = new System.Drawing.Size(40, 36);
            this.SIColumnCheckButton.TabIndex = 32;
            this.SIColumnCheckButton.Click += new System.EventHandler(this.SIColumnCheckButton_Click);
            // 
            // SIUpdateButton
            // 
            this.SIUpdateButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.SIUpdateButton.Image = global::LIMS.Properties.Resources.refresh_32x32;
            this.SIUpdateButton.Location = new System.Drawing.Point(581, 2);
            this.SIUpdateButton.Name = "SIUpdateButton";
            this.SIUpdateButton.Size = new System.Drawing.Size(40, 36);
            this.SIUpdateButton.TabIndex = 33;
            this.SIUpdateButton.Click += new System.EventHandler(this.SIUpdateButton_Click);
            // 
            // SIPrintButton
            // 
            this.SIPrintButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.SIPrintButton.Image = global::LIMS.Properties.Resources.printer_32x32;
            this.SIPrintButton.Location = new System.Drawing.Point(42, 2);
            this.SIPrintButton.Name = "SIPrintButton";
            this.SIPrintButton.Size = new System.Drawing.Size(40, 36);
            this.SIPrintButton.TabIndex = 29;
            this.SIPrintButton.Click += new System.EventHandler(this.SIPrintButton_Click);
            // 
            // SIDeleteButton
            // 
            this.SIDeleteButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.SIDeleteButton.Image = global::LIMS.Properties.Resources.delete_32x32;
            this.SIDeleteButton.Location = new System.Drawing.Point(2, 2);
            this.SIDeleteButton.Name = "SIDeleteButton";
            this.SIDeleteButton.Size = new System.Drawing.Size(40, 36);
            this.SIDeleteButton.TabIndex = 28;
            this.SIDeleteButton.Click += new System.EventHandler(this.SIDeleteButton_Click);
            // 
            // IO
            // 
            this.IO.Controls.Add(this.splitContainerControl3);
            this.IO.Name = "IO";
            this.IO.Size = new System.Drawing.Size(988, 703);
            this.IO.Text = "ИО";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.IOPanel);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel1.SizeChanged += new System.EventHandler(this.splitContainerControl3_Panel1_SizeChanged);
            this.splitContainerControl3.Panel2.Controls.Add(this.IOGrid);
            this.splitContainerControl3.Panel2.Controls.Add(this.panelControl7);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(988, 703);
            this.splitContainerControl3.SplitterPosition = 369;
            this.splitContainerControl3.TabIndex = 4;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // IOPanel
            // 
            this.IOPanel.Controls.Add(this.IOPravoSobstvennosti);
            this.IOPanel.Controls.Add(this.label113);
            this.IOPanel.Controls.Add(this.IODataINAttestachiiTip);
            this.IOPanel.Controls.Add(this.IODataPosledTOTip);
            this.IOPanel.Controls.Add(this.IODataSledTOKol);
            this.IOPanel.Controls.Add(this.IODataSledTOTip);
            this.IOPanel.Controls.Add(this.IODateSledAttistachiiKol);
            this.IOPanel.Controls.Add(this.IODateSledAttistachiiTip);
            this.IOPanel.Controls.Add(this.IOMestoUstanovkiIHranenia);
            this.IOPanel.Controls.Add(this.IOPeriodich);
            this.IOPanel.Controls.Add(this.IODataSledTODate);
            this.IOPanel.Controls.Add(this.IODataPosledTO);
            this.IOPanel.Controls.Add(this.IODataINAttestachii);
            this.IOPanel.Controls.Add(this.label65);
            this.IOPanel.Controls.Add(this.IOTipMarka);
            this.IOPanel.Controls.Add(this.IOZavodskoy);
            this.IOPanel.Controls.Add(this.label102);
            this.IOPanel.Controls.Add(this.IOInventarNomer);
            this.IOPanel.Controls.Add(this.label101);
            this.IOPanel.Controls.Add(this.IOGodVyp);
            this.IOPanel.Controls.Add(this.IONomerDokumentaObAttes);
            this.IOPanel.Controls.Add(this.label100);
            this.IOPanel.Controls.Add(this.label99);
            this.IOPanel.Controls.Add(this.label97);
            this.IOPanel.Controls.Add(this.IOCopyButton);
            this.IOPanel.Controls.Add(this.label80);
            this.IOPanel.Controls.Add(this.ION);
            this.IOPanel.Controls.Add(this.label79);
            this.IOPanel.Controls.Add(this.IOPrimechanie);
            this.IOPanel.Controls.Add(this.label62);
            this.IOPanel.Controls.Add(this.IOGodVvodaVEkspluatachiu);
            this.IOPanel.Controls.Add(this.IOIzgotovitel);
            this.IOPanel.Controls.Add(this.label63);
            this.IOPanel.Controls.Add(this.IOName);
            this.IOPanel.Controls.Add(this.label64);
            this.IOPanel.Controls.Add(this.IONameVidovIspitanii);
            this.IOPanel.Controls.Add(this.label66);
            this.IOPanel.Controls.Add(this.IODateSledAttistachiiDate);
            this.IOPanel.Controls.Add(this.label67);
            this.IOPanel.Controls.Add(this.label68);
            this.IOPanel.Controls.Add(this.IOID);
            this.IOPanel.Controls.Add(this.IOTehnichHaracter);
            this.IOPanel.Controls.Add(this.IOEditCheckBox);
            this.IOPanel.Controls.Add(this.IONewButton);
            this.IOPanel.Controls.Add(this.IOSaveButton);
            this.IOPanel.Controls.Add(this.label70);
            this.IOPanel.Controls.Add(this.pictureBox5);
            this.IOPanel.Controls.Add(this.label71);
            this.IOPanel.Controls.Add(this.label72);
            this.IOPanel.Controls.Add(this.label73);
            this.IOPanel.Controls.Add(this.label74);
            this.IOPanel.Controls.Add(this.label75);
            this.IOPanel.Controls.Add(this.label76);
            this.IOPanel.Controls.Add(this.label77);
            this.IOPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IOPanel.Location = new System.Drawing.Point(0, 0);
            this.IOPanel.Name = "IOPanel";
            this.IOPanel.Size = new System.Drawing.Size(369, 703);
            this.IOPanel.TabIndex = 0;
            // 
            // IOPravoSobstvennosti
            // 
            this.IOPravoSobstvennosti.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOPravoSobstvennosti.AutoCompleteCustomSource.AddRange(new string[] {
            "Аналитическая лаборатория",
            "Весовая",
            "Комната приема проб",
            "Комната определения фракционного состава",
            "Комната определения сероводорода",
            "Комната хранения хим. посуды",
            "Склад хранения арбитражных проб и хим. реактивов"});
            this.IOPravoSobstvennosti.Enabled = false;
            this.IOPravoSobstvennosti.FormattingEnabled = true;
            this.IOPravoSobstvennosti.Items.AddRange(new object[] {
            "собственность",
            "собственность"});
            this.IOPravoSobstvennosti.Location = new System.Drawing.Point(186, 486);
            this.IOPravoSobstvennosti.Name = "IOPravoSobstvennosti";
            this.IOPravoSobstvennosti.Size = new System.Drawing.Size(178, 21);
            this.IOPravoSobstvennosti.Sorted = true;
            this.IOPravoSobstvennosti.TabIndex = 73;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(60, 489);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(120, 13);
            this.label113.TabIndex = 72;
            this.label113.Text = "Право собственности:";
            // 
            // IODataINAttestachiiTip
            // 
            this.IODataINAttestachiiTip.Enabled = false;
            this.IODataINAttestachiiTip.FormattingEnabled = true;
            this.IODataINAttestachiiTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.IODataINAttestachiiTip.Location = new System.Drawing.Point(186, 405);
            this.IODataINAttestachiiTip.Name = "IODataINAttestachiiTip";
            this.IODataINAttestachiiTip.Size = new System.Drawing.Size(54, 21);
            this.IODataINAttestachiiTip.Sorted = true;
            this.IODataINAttestachiiTip.TabIndex = 14;
            this.IODataINAttestachiiTip.Text = "дата";
            this.IODataINAttestachiiTip.SelectedIndexChanged += new System.EventHandler(this.IODataINAttestachiiTip_SelectedIndexChanged);
            // 
            // IODataPosledTOTip
            // 
            this.IODataPosledTOTip.Enabled = false;
            this.IODataPosledTOTip.FormattingEnabled = true;
            this.IODataPosledTOTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.IODataPosledTOTip.Location = new System.Drawing.Point(186, 540);
            this.IODataPosledTOTip.Name = "IODataPosledTOTip";
            this.IODataPosledTOTip.Size = new System.Drawing.Size(54, 21);
            this.IODataPosledTOTip.Sorted = true;
            this.IODataPosledTOTip.TabIndex = 20;
            this.IODataPosledTOTip.Text = "дата";
            this.IODataPosledTOTip.SelectedIndexChanged += new System.EventHandler(this.IODataPosledTOTip_SelectedIndexChanged);
            // 
            // IODataSledTOKol
            // 
            this.IODataSledTOKol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IODataSledTOKol.Location = new System.Drawing.Point(246, 567);
            this.IODataSledTOKol.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.IODataSledTOKol.Name = "IODataSledTOKol";
            this.IODataSledTOKol.ReadOnly = true;
            this.IODataSledTOKol.Size = new System.Drawing.Size(118, 21);
            this.IODataSledTOKol.TabIndex = 23;
            this.IODataSledTOKol.Visible = false;
            // 
            // IODataSledTOTip
            // 
            this.IODataSledTOTip.Enabled = false;
            this.IODataSledTOTip.FormattingEnabled = true;
            this.IODataSledTOTip.Items.AddRange(new object[] {
            "дата",
            "мес.",
            "нет"});
            this.IODataSledTOTip.Location = new System.Drawing.Point(186, 567);
            this.IODataSledTOTip.Name = "IODataSledTOTip";
            this.IODataSledTOTip.Size = new System.Drawing.Size(54, 21);
            this.IODataSledTOTip.Sorted = true;
            this.IODataSledTOTip.TabIndex = 22;
            this.IODataSledTOTip.Text = "дата";
            this.IODataSledTOTip.SelectedIndexChanged += new System.EventHandler(this.IODataSledTOTip_SelectedIndexChanged);
            // 
            // IODateSledAttistachiiKol
            // 
            this.IODateSledAttistachiiKol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IODateSledAttistachiiKol.Location = new System.Drawing.Point(246, 459);
            this.IODateSledAttistachiiKol.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.IODateSledAttistachiiKol.Name = "IODateSledAttistachiiKol";
            this.IODateSledAttistachiiKol.ReadOnly = true;
            this.IODateSledAttistachiiKol.Size = new System.Drawing.Size(118, 21);
            this.IODateSledAttistachiiKol.TabIndex = 18;
            this.IODateSledAttistachiiKol.Visible = false;
            // 
            // IODateSledAttistachiiTip
            // 
            this.IODateSledAttistachiiTip.Enabled = false;
            this.IODateSledAttistachiiTip.FormattingEnabled = true;
            this.IODateSledAttistachiiTip.Items.AddRange(new object[] {
            "дата",
            "мес.",
            "нет"});
            this.IODateSledAttistachiiTip.Location = new System.Drawing.Point(186, 459);
            this.IODateSledAttistachiiTip.Name = "IODateSledAttistachiiTip";
            this.IODateSledAttistachiiTip.Size = new System.Drawing.Size(54, 21);
            this.IODateSledAttistachiiTip.Sorted = true;
            this.IODateSledAttistachiiTip.TabIndex = 17;
            this.IODateSledAttistachiiTip.Text = "дата";
            this.IODateSledAttistachiiTip.SelectedIndexChanged += new System.EventHandler(this.IODateSledAttistachiiTip_SelectedIndexChanged);
            // 
            // IOMestoUstanovkiIHranenia
            // 
            this.IOMestoUstanovkiIHranenia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOMestoUstanovkiIHranenia.AutoCompleteCustomSource.AddRange(new string[] {
            "Аналитическая лаборатория",
            "Весовая",
            "Комната приема проб",
            "Комната определения фракционного состава",
            "Комната определения сероводорода",
            "Комната хранения хим. посуды",
            "Склад хранения арбитражных проб и хим. реактивов"});
            this.IOMestoUstanovkiIHranenia.Enabled = false;
            this.IOMestoUstanovkiIHranenia.FormattingEnabled = true;
            this.IOMestoUstanovkiIHranenia.Items.AddRange(new object[] {
            "Аналитическая лаборатория",
            "Весовая",
            "Комната определения сероводорода",
            "Комната определения фракционного состава",
            "Комната приема проб",
            "Комната хранения хим. посуды",
            "Склад хранения арбитражных проб и хим. реактивов"});
            this.IOMestoUstanovkiIHranenia.Location = new System.Drawing.Point(186, 513);
            this.IOMestoUstanovkiIHranenia.Name = "IOMestoUstanovkiIHranenia";
            this.IOMestoUstanovkiIHranenia.Size = new System.Drawing.Size(178, 21);
            this.IOMestoUstanovkiIHranenia.Sorted = true;
            this.IOMestoUstanovkiIHranenia.TabIndex = 19;
            // 
            // IOPeriodich
            // 
            this.IOPeriodich.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOPeriodich.Enabled = false;
            this.IOPeriodich.FormattingEnabled = true;
            this.IOPeriodich.Items.AddRange(new object[] {
            "12 мес.",
            "18 мес.",
            "24 мес.",
            "36 мес.",
            "48 мес.",
            "60 мес."});
            this.IOPeriodich.Location = new System.Drawing.Point(186, 432);
            this.IOPeriodich.Name = "IOPeriodich";
            this.IOPeriodich.Size = new System.Drawing.Size(178, 21);
            this.IOPeriodich.TabIndex = 16;
            this.IOPeriodich.SelectedIndexChanged += new System.EventHandler(this.IOPeriodich_SelectedIndexChanged);
            // 
            // IODataSledTODate
            // 
            this.IODataSledTODate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IODataSledTODate.Checked = false;
            this.IODataSledTODate.Enabled = false;
            this.IODataSledTODate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.IODataSledTODate.Location = new System.Drawing.Point(246, 567);
            this.IODataSledTODate.Name = "IODataSledTODate";
            this.IODataSledTODate.Size = new System.Drawing.Size(118, 21);
            this.IODataSledTODate.TabIndex = 71;
            // 
            // IODataPosledTO
            // 
            this.IODataPosledTO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IODataPosledTO.Checked = false;
            this.IODataPosledTO.Enabled = false;
            this.IODataPosledTO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.IODataPosledTO.Location = new System.Drawing.Point(246, 540);
            this.IODataPosledTO.Name = "IODataPosledTO";
            this.IODataPosledTO.Size = new System.Drawing.Size(118, 21);
            this.IODataPosledTO.TabIndex = 21;
            // 
            // IODataINAttestachii
            // 
            this.IODataINAttestachii.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IODataINAttestachii.Checked = false;
            this.IODataINAttestachii.Enabled = false;
            this.IODataINAttestachii.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.IODataINAttestachii.Location = new System.Drawing.Point(246, 405);
            this.IODataINAttestachii.Name = "IODataINAttestachii";
            this.IODataINAttestachii.Size = new System.Drawing.Size(118, 21);
            this.IODataINAttestachii.TabIndex = 15;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(114, 192);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(66, 13);
            this.label65.TabIndex = 68;
            this.label65.Text = "Тип, марка:";
            // 
            // IOTipMarka
            // 
            this.IOTipMarka.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOTipMarka.Location = new System.Drawing.Point(186, 189);
            this.IOTipMarka.Name = "IOTipMarka";
            this.IOTipMarka.ReadOnly = true;
            this.IOTipMarka.Size = new System.Drawing.Size(178, 21);
            this.IOTipMarka.TabIndex = 6;
            // 
            // IOZavodskoy
            // 
            this.IOZavodskoy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOZavodskoy.Location = new System.Drawing.Point(186, 216);
            this.IOZavodskoy.Name = "IOZavodskoy";
            this.IOZavodskoy.ReadOnly = true;
            this.IOZavodskoy.Size = new System.Drawing.Size(178, 21);
            this.IOZavodskoy.TabIndex = 7;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(67, 244);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(113, 13);
            this.label102.TabIndex = 65;
            this.label102.Text = "Инвентарный номер:";
            // 
            // IOInventarNomer
            // 
            this.IOInventarNomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOInventarNomer.Location = new System.Drawing.Point(186, 243);
            this.IOInventarNomer.Name = "IOInventarNomer";
            this.IOInventarNomer.ReadOnly = true;
            this.IOInventarNomer.Size = new System.Drawing.Size(178, 21);
            this.IOInventarNomer.TabIndex = 8;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(104, 300);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(76, 13);
            this.label101.TabIndex = 63;
            this.label101.Text = "Год выпуска:";
            // 
            // IOGodVyp
            // 
            this.IOGodVyp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOGodVyp.Location = new System.Drawing.Point(186, 297);
            this.IOGodVyp.Name = "IOGodVyp";
            this.IOGodVyp.ReadOnly = true;
            this.IOGodVyp.Size = new System.Drawing.Size(178, 21);
            this.IOGodVyp.TabIndex = 10;
            // 
            // IONomerDokumentaObAttes
            // 
            this.IONomerDokumentaObAttes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IONomerDokumentaObAttes.Location = new System.Drawing.Point(186, 378);
            this.IONomerDokumentaObAttes.Name = "IONomerDokumentaObAttes";
            this.IONomerDokumentaObAttes.ReadOnly = true;
            this.IONomerDokumentaObAttes.Size = new System.Drawing.Size(178, 21);
            this.IONomerDokumentaObAttes.TabIndex = 13;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(90, 435);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(90, 13);
            this.label100.TabIndex = 60;
            this.label100.Text = "Периодичность:";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(64, 543);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(116, 13);
            this.label99.TabIndex = 58;
            this.label99.Text = "Дата последнего ТО:";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(58, 570);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(122, 13);
            this.label97.TabIndex = 56;
            this.label97.Text = "Дата следующего ТО:";
            // 
            // IOCopyButton
            // 
            this.IOCopyButton.AllowDrop = true;
            this.IOCopyButton.AutoSize = true;
            this.IOCopyButton.Image = global::LIMS.Properties.Resources.copy_16x16;
            this.IOCopyButton.Location = new System.Drawing.Point(275, 62);
            this.IOCopyButton.Name = "IOCopyButton";
            this.IOCopyButton.Size = new System.Drawing.Size(89, 22);
            this.IOCopyButton.TabIndex = 2;
            this.IOCopyButton.Text = "Копировать";
            this.IOCopyButton.Click += new System.EventHandler(this.IOCopyButtonButton_Click);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(34, 143);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(146, 13);
            this.label80.TabIndex = 53;
            this.label80.Text = "характеристик продукции:";
            // 
            // ION
            // 
            this.ION.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ION.Location = new System.Drawing.Point(186, 94);
            this.ION.Name = "ION";
            this.ION.ReadOnly = true;
            this.ION.Size = new System.Drawing.Size(178, 21);
            this.ION.TabIndex = 3;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(137, 94);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(43, 13);
            this.label79.TabIndex = 52;
            this.label79.Text = "№ п/п:";
            // 
            // IOPrimechanie
            // 
            this.IOPrimechanie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOPrimechanie.Location = new System.Drawing.Point(186, 594);
            this.IOPrimechanie.Name = "IOPrimechanie";
            this.IOPrimechanie.ReadOnly = true;
            this.IOPrimechanie.Size = new System.Drawing.Size(178, 21);
            this.IOPrimechanie.TabIndex = 24;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(81, 408);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(99, 13);
            this.label62.TabIndex = 47;
            this.label62.Text = "Дата аттестации:";
            // 
            // IOGodVvodaVEkspluatachiu
            // 
            this.IOGodVvodaVEkspluatachiu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOGodVvodaVEkspluatachiu.Location = new System.Drawing.Point(186, 351);
            this.IOGodVvodaVEkspluatachiu.Name = "IOGodVvodaVEkspluatachiu";
            this.IOGodVvodaVEkspluatachiu.ReadOnly = true;
            this.IOGodVvodaVEkspluatachiu.Size = new System.Drawing.Size(178, 21);
            this.IOGodVvodaVEkspluatachiu.TabIndex = 12;
            // 
            // IOIzgotovitel
            // 
            this.IOIzgotovitel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOIzgotovitel.Location = new System.Drawing.Point(186, 270);
            this.IOIzgotovitel.Name = "IOIzgotovitel";
            this.IOIzgotovitel.ReadOnly = true;
            this.IOIzgotovitel.Size = new System.Drawing.Size(178, 21);
            this.IOIzgotovitel.TabIndex = 9;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(84, 278);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(96, 13);
            this.label63.TabIndex = 44;
            this.label63.Text = "приятие, фирма):";
            // 
            // IOName
            // 
            this.IOName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOName.Location = new System.Drawing.Point(186, 162);
            this.IOName.Name = "IOName";
            this.IOName.ReadOnly = true;
            this.IOName.Size = new System.Drawing.Size(178, 21);
            this.IOName.TabIndex = 5;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(84, 219);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(98, 13);
            this.label64.TabIndex = 42;
            this.label64.Text = "Заводской номер:";
            // 
            // IONameVidovIspitanii
            // 
            this.IONameVidovIspitanii.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IONameVidovIspitanii.Location = new System.Drawing.Point(186, 121);
            this.IONameVidovIspitanii.Multiline = true;
            this.IONameVidovIspitanii.Name = "IONameVidovIspitanii";
            this.IONameVidovIspitanii.ReadOnly = true;
            this.IONameVidovIspitanii.Size = new System.Drawing.Size(178, 35);
            this.IONameVidovIspitanii.TabIndex = 4;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(25, 130);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(155, 13);
            this.label66.TabIndex = 39;
            this.label66.Text = "испытаний и определеннных";
            // 
            // IODateSledAttistachiiDate
            // 
            this.IODateSledAttistachiiDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IODateSledAttistachiiDate.Checked = false;
            this.IODateSledAttistachiiDate.Enabled = false;
            this.IODateSledAttistachiiDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.IODateSledAttistachiiDate.Location = new System.Drawing.Point(246, 459);
            this.IODateSledAttistachiiDate.Name = "IODateSledAttistachiiDate";
            this.IODateSledAttistachiiDate.Size = new System.Drawing.Size(118, 21);
            this.IODateSledAttistachiiDate.TabIndex = 38;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(18, 461);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(162, 13);
            this.label67.TabIndex = 33;
            this.label67.Text = "Дата следующей аттестации:";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(105, 381);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(75, 13);
            this.label68.TabIndex = 32;
            this.label68.Text = "Аттестат №:";
            // 
            // IOID
            // 
            this.IOID.AutoSize = true;
            this.IOID.Location = new System.Drawing.Point(260, 25);
            this.IOID.Name = "IOID";
            this.IOID.Size = new System.Drawing.Size(0, 13);
            this.IOID.TabIndex = 27;
            this.IOID.Visible = false;
            // 
            // IOTehnichHaracter
            // 
            this.IOTehnichHaracter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IOTehnichHaracter.Location = new System.Drawing.Point(186, 324);
            this.IOTehnichHaracter.Name = "IOTehnichHaracter";
            this.IOTehnichHaracter.ReadOnly = true;
            this.IOTehnichHaracter.Size = new System.Drawing.Size(178, 21);
            this.IOTehnichHaracter.TabIndex = 11;
            // 
            // IOEditCheckBox
            // 
            this.IOEditCheckBox.AutoSize = true;
            this.IOEditCheckBox.Location = new System.Drawing.Point(186, 621);
            this.IOEditCheckBox.Name = "IOEditCheckBox";
            this.IOEditCheckBox.Size = new System.Drawing.Size(105, 17);
            this.IOEditCheckBox.TabIndex = 25;
            this.IOEditCheckBox.Text = "Редактировать";
            this.IOEditCheckBox.UseVisualStyleBackColor = true;
            this.IOEditCheckBox.CheckedChanged += new System.EventHandler(this.IOEditCheckBox_CheckedChanged);
            // 
            // IONewButton
            // 
            this.IONewButton.AllowDrop = true;
            this.IONewButton.AutoSize = true;
            this.IONewButton.Image = global::LIMS.Properties.Resources.add_16x16;
            this.IONewButton.Location = new System.Drawing.Point(186, 62);
            this.IONewButton.Name = "IONewButton";
            this.IONewButton.Size = new System.Drawing.Size(78, 22);
            this.IONewButton.TabIndex = 1;
            this.IONewButton.Text = "Добавить";
            this.IONewButton.Click += new System.EventHandler(this.IONewButton_Click);
            // 
            // IOSaveButton
            // 
            this.IOSaveButton.AutoSize = true;
            this.IOSaveButton.Image = global::LIMS.Properties.Resources.save_16x16;
            this.IOSaveButton.Location = new System.Drawing.Point(186, 644);
            this.IOSaveButton.Name = "IOSaveButton";
            this.IOSaveButton.Size = new System.Drawing.Size(83, 22);
            this.IOSaveButton.TabIndex = 26;
            this.IOSaveButton.Text = "Сохранить";
            this.IOSaveButton.Click += new System.EventHandler(this.IOSaveButton_Click);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label70.Location = new System.Drawing.Point(159, 11);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(186, 16);
            this.label70.TabIndex = 23;
            this.label70.Text = "Испытательное оборудование";
            // 
            // pictureBox5
            // 
            this.pictureBox5.ErrorImage = null;
            this.pictureBox5.Image = global::LIMS.Properties.Resources.Ispitalka;
            this.pictureBox5.Location = new System.Drawing.Point(37, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(111, 91);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 11;
            this.pictureBox5.TabStop = false;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(108, 597);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(72, 13);
            this.label71.TabIndex = 10;
            this.label71.Text = "Примечание:";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(10, 516);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(170, 13);
            this.label72.TabIndex = 9;
            this.label72.Text = "Место установки или хранения:";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(31, 354);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(149, 13);
            this.label73.TabIndex = 5;
            this.label73.Text = "Год ввода в эксплуатацию:";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(1, 324);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(179, 13);
            this.label74.TabIndex = 4;
            this.label74.Text = "Основные техн. характеристики:";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(24, 265);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(156, 13);
            this.label75.TabIndex = 3;
            this.label75.Text = "Изготовитель (страна, пред-";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(78, 165);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(102, 13);
            this.label76.TabIndex = 2;
            this.label76.Text = "Наименование ИО:";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(66, 117);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(114, 13);
            this.label77.TabIndex = 1;
            this.label77.Text = "Наименование видов";
            // 
            // IOGrid
            // 
            this.IOGrid.AccessibleDescription = "";
            this.IOGrid.AccessibleName = "";
            this.IOGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IOGrid.Location = new System.Drawing.Point(0, 40);
            this.IOGrid.MainView = this.IOGridView;
            this.IOGrid.Name = "IOGrid";
            this.IOGrid.Size = new System.Drawing.Size(614, 663);
            this.IOGrid.TabIndex = 999999;
            this.IOGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.IOGridView});
            this.IOGrid.Click += new System.EventHandler(this.IOGrid_Click);
            // 
            // IOGridView
            // 
            this.IOGridView.Appearance.ColumnFilterButton.Options.UseTextOptions = true;
            this.IOGridView.Appearance.ColumnFilterButton.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IOGridView.Appearance.ColumnFilterButton.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.IOGridView.Appearance.ColumnFilterButton.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IOGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.IOGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IOGridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.IOGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IOGridView.AppearancePrint.EvenRow.Options.UseTextOptions = true;
            this.IOGridView.AppearancePrint.EvenRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IOGridView.AppearancePrint.GroupRow.Options.UseTextOptions = true;
            this.IOGridView.AppearancePrint.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IOGridView.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.IOGridView.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IOGridView.AppearancePrint.Lines.Options.UseTextOptions = true;
            this.IOGridView.AppearancePrint.Lines.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IOGridView.AppearancePrint.OddRow.Options.UseTextOptions = true;
            this.IOGridView.AppearancePrint.OddRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IOGridView.AppearancePrint.Preview.Options.UseTextOptions = true;
            this.IOGridView.AppearancePrint.Preview.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IOGridView.AppearancePrint.Row.Options.UseTextOptions = true;
            this.IOGridView.AppearancePrint.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IOGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.IOGridView.GridControl = this.IOGrid;
            this.IOGridView.Name = "IOGridView";
            this.IOGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.IOGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.IOGridView.OptionsBehavior.AllowIncrementalSearch = true;
            this.IOGridView.OptionsBehavior.Editable = false;
            this.IOGridView.OptionsCustomization.AllowRowSizing = true;
            this.IOGridView.OptionsFind.AlwaysVisible = true;
            this.IOGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.IOGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.IOGridView.OptionsView.RowAutoHeight = true;
            this.IOGridView.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.IOGridView_RowClick);
            this.IOGridView.ColumnWidthChanged += new DevExpress.XtraGrid.Views.Base.ColumnEventHandler(this.IOGridView_ColumnWidthChanged);
            this.IOGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.IOGridView_PopupMenuShowing);
            this.IOGridView.StartSorting += new System.EventHandler(this.IOGridView_StartSorting);
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.IOCellMergeButton);
            this.panelControl7.Controls.Add(this.IOArhivButton);
            this.panelControl7.Controls.Add(this.IOColumnCheckButton);
            this.panelControl7.Controls.Add(this.IOUpdateButton);
            this.panelControl7.Controls.Add(this.IOPrintButton);
            this.panelControl7.Controls.Add(this.IODeleteButton);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(0, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(614, 40);
            this.panelControl7.TabIndex = 0;
            // 
            // IOCellMergeButton
            // 
            this.IOCellMergeButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.IOCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            this.IOCellMergeButton.Location = new System.Drawing.Point(122, 2);
            this.IOCellMergeButton.Name = "IOCellMergeButton";
            this.IOCellMergeButton.Size = new System.Drawing.Size(40, 36);
            this.IOCellMergeButton.TabIndex = 30;
            this.IOCellMergeButton.Click += new System.EventHandler(this.IOCellMergeButton_Click);
            // 
            // IOArhivButton
            // 
            this.IOArhivButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.IOArhivButton.Image = global::LIMS.Properties.Resources.checkbuttons_32x32;
            this.IOArhivButton.Location = new System.Drawing.Point(82, 2);
            this.IOArhivButton.Name = "IOArhivButton";
            this.IOArhivButton.Size = new System.Drawing.Size(40, 36);
            this.IOArhivButton.TabIndex = 29;
            this.IOArhivButton.Click += new System.EventHandler(this.IOArhivButton_Click_1);
            // 
            // IOColumnCheckButton
            // 
            this.IOColumnCheckButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.IOColumnCheckButton.Image = global::LIMS.Properties.Resources.insertcolumns_32x32;
            this.IOColumnCheckButton.Location = new System.Drawing.Point(532, 2);
            this.IOColumnCheckButton.Name = "IOColumnCheckButton";
            this.IOColumnCheckButton.Size = new System.Drawing.Size(40, 36);
            this.IOColumnCheckButton.TabIndex = 31;
            this.IOColumnCheckButton.Click += new System.EventHandler(this.IOColumnCheckButton_Click);
            // 
            // IOUpdateButton
            // 
            this.IOUpdateButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.IOUpdateButton.Image = global::LIMS.Properties.Resources.refresh_32x32;
            this.IOUpdateButton.Location = new System.Drawing.Point(572, 2);
            this.IOUpdateButton.Name = "IOUpdateButton";
            this.IOUpdateButton.Size = new System.Drawing.Size(40, 36);
            this.IOUpdateButton.TabIndex = 32;
            this.IOUpdateButton.Click += new System.EventHandler(this.IOUpdateButton_Click);
            // 
            // IOPrintButton
            // 
            this.IOPrintButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.IOPrintButton.Image = global::LIMS.Properties.Resources.printer_32x32;
            this.IOPrintButton.Location = new System.Drawing.Point(42, 2);
            this.IOPrintButton.Name = "IOPrintButton";
            this.IOPrintButton.Size = new System.Drawing.Size(40, 36);
            this.IOPrintButton.TabIndex = 28;
            this.IOPrintButton.Click += new System.EventHandler(this.IOPrintButton_Click);
            // 
            // IODeleteButton
            // 
            this.IODeleteButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.IODeleteButton.Image = global::LIMS.Properties.Resources.delete_32x32;
            this.IODeleteButton.Location = new System.Drawing.Point(2, 2);
            this.IODeleteButton.Name = "IODeleteButton";
            this.IODeleteButton.Size = new System.Drawing.Size(40, 36);
            this.IODeleteButton.TabIndex = 27;
            this.IODeleteButton.Click += new System.EventHandler(this.IODeleteButton_Click);
            // 
            // VO
            // 
            this.VO.Controls.Add(this.splitContainerControl5);
            this.VO.Name = "VO";
            this.VO.Size = new System.Drawing.Size(988, 703);
            this.VO.Text = "ВО";
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl5.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Controls.Add(this.VOPanel);
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel1.SizeChanged += new System.EventHandler(this.splitContainerControl5_Panel1_SizeChanged);
            this.splitContainerControl5.Panel2.Controls.Add(this.VOGrid);
            this.splitContainerControl5.Panel2.Controls.Add(this.panelControl8);
            this.splitContainerControl5.Panel2.Text = "Panel2";
            this.splitContainerControl5.Size = new System.Drawing.Size(988, 703);
            this.splitContainerControl5.SplitterPosition = 381;
            this.splitContainerControl5.TabIndex = 5;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // VOPanel
            // 
            this.VOPanel.Controls.Add(this.VOPravoSobstvennosti);
            this.VOPanel.Controls.Add(this.label114);
            this.VOPanel.Controls.Add(this.VODataPoslTOTip);
            this.VOPanel.Controls.Add(this.VODateProverkiTehnicheskogoSostoianiaTip);
            this.VOPanel.Controls.Add(this.VOSledProvTOKol);
            this.VOPanel.Controls.Add(this.VOSledProvTOTip);
            this.VOPanel.Controls.Add(this.VODateSledProverkiKol);
            this.VOPanel.Controls.Add(this.VODateSledProverkiTip);
            this.VOPanel.Controls.Add(this.VOMestoUstanovkiIHranenia);
            this.VOPanel.Controls.Add(this.VOSledProvTODate);
            this.VOPanel.Controls.Add(this.VODataPoslTO);
            this.VOPanel.Controls.Add(this.label107);
            this.VOPanel.Controls.Add(this.VOTipMarka);
            this.VOPanel.Controls.Add(this.label106);
            this.VOPanel.Controls.Add(this.VOZavodNomer);
            this.VOPanel.Controls.Add(this.label105);
            this.VOPanel.Controls.Add(this.VOGodVypuska);
            this.VOPanel.Controls.Add(this.VOInventarNomer);
            this.VOPanel.Controls.Add(this.label104);
            this.VOPanel.Controls.Add(this.label103);
            this.VOPanel.Controls.Add(this.label78);
            this.VOPanel.Controls.Add(this.VOPrimechanie);
            this.VOPanel.Controls.Add(this.VOCopyButton);
            this.VOPanel.Controls.Add(this.VODateProverkiTehnicheskogoSostoiania);
            this.VOPanel.Controls.Add(this.label69);
            this.VOPanel.Controls.Add(this.label85);
            this.VOPanel.Controls.Add(this.VON);
            this.VOPanel.Controls.Add(this.label81);
            this.VOPanel.Controls.Add(this.VONaznachenie);
            this.VOPanel.Controls.Add(this.label83);
            this.VOPanel.Controls.Add(this.VOGodVvodaVEkspltatechiu);
            this.VOPanel.Controls.Add(this.label84);
            this.VOPanel.Controls.Add(this.VOIzgotovitel);
            this.VOPanel.Controls.Add(this.VODateSledProverkiDate);
            this.VOPanel.Controls.Add(this.label87);
            this.VOPanel.Controls.Add(this.label88);
            this.VOPanel.Controls.Add(this.VOID);
            this.VOPanel.Controls.Add(this.VOName);
            this.VOPanel.Controls.Add(this.VOEditCheckBox);
            this.VOPanel.Controls.Add(this.VONewButton);
            this.VOPanel.Controls.Add(this.VOSaveButton);
            this.VOPanel.Controls.Add(this.label90);
            this.VOPanel.Controls.Add(this.pictureBox6);
            this.VOPanel.Controls.Add(this.label94);
            this.VOPanel.Controls.Add(this.label96);
            this.VOPanel.Controls.Add(this.label98);
            this.VOPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VOPanel.Location = new System.Drawing.Point(0, 0);
            this.VOPanel.Name = "VOPanel";
            this.VOPanel.Size = new System.Drawing.Size(381, 703);
            this.VOPanel.TabIndex = 0;
            // 
            // VOPravoSobstvennosti
            // 
            this.VOPravoSobstvennosti.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOPravoSobstvennosti.AutoCompleteCustomSource.AddRange(new string[] {
            "Аналитическая лаборатория",
            "Весовая",
            "Комната приема проб",
            "Комната определения фракционного состава",
            "Комната определения сероводорода",
            "Комната хранения хим. посуды",
            "Склад хранения арбитражных проб и хим. реактивов"});
            this.VOPravoSobstvennosti.Enabled = false;
            this.VOPravoSobstvennosti.FormattingEnabled = true;
            this.VOPravoSobstvennosti.Items.AddRange(new object[] {
            "собственность",
            "собственность"});
            this.VOPravoSobstvennosti.Location = new System.Drawing.Point(198, 327);
            this.VOPravoSobstvennosti.Name = "VOPravoSobstvennosti";
            this.VOPravoSobstvennosti.Size = new System.Drawing.Size(178, 21);
            this.VOPravoSobstvennosti.Sorted = true;
            this.VOPravoSobstvennosti.TabIndex = 73;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(65, 330);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(123, 13);
            this.label114.TabIndex = 72;
            this.label114.Text = " Право собственности:";
            // 
            // VODataPoslTOTip
            // 
            this.VODataPoslTOTip.Enabled = false;
            this.VODataPoslTOTip.FormattingEnabled = true;
            this.VODataPoslTOTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.VODataPoslTOTip.Location = new System.Drawing.Point(198, 435);
            this.VODataPoslTOTip.Name = "VODataPoslTOTip";
            this.VODataPoslTOTip.Size = new System.Drawing.Size(54, 21);
            this.VODataPoslTOTip.Sorted = true;
            this.VODataPoslTOTip.TabIndex = 17;
            this.VODataPoslTOTip.Text = "дата";
            this.VODataPoslTOTip.SelectedIndexChanged += new System.EventHandler(this.VODataPoslTOTip_SelectedIndexChanged);
            // 
            // VODateProverkiTehnicheskogoSostoianiaTip
            // 
            this.VODateProverkiTehnicheskogoSostoianiaTip.Enabled = false;
            this.VODateProverkiTehnicheskogoSostoianiaTip.FormattingEnabled = true;
            this.VODateProverkiTehnicheskogoSostoianiaTip.Items.AddRange(new object[] {
            "дата",
            "нет"});
            this.VODateProverkiTehnicheskogoSostoianiaTip.Location = new System.Drawing.Point(198, 381);
            this.VODateProverkiTehnicheskogoSostoianiaTip.Name = "VODateProverkiTehnicheskogoSostoianiaTip";
            this.VODateProverkiTehnicheskogoSostoianiaTip.Size = new System.Drawing.Size(54, 21);
            this.VODateProverkiTehnicheskogoSostoianiaTip.Sorted = true;
            this.VODateProverkiTehnicheskogoSostoianiaTip.TabIndex = 13;
            this.VODateProverkiTehnicheskogoSostoianiaTip.Text = "дата";
            this.VODateProverkiTehnicheskogoSostoianiaTip.SelectedIndexChanged += new System.EventHandler(this.VODateProverkiTehnicheskogoSostoianiaTip_SelectedIndexChanged);
            // 
            // VOSledProvTOKol
            // 
            this.VOSledProvTOKol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOSledProvTOKol.Location = new System.Drawing.Point(258, 462);
            this.VOSledProvTOKol.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.VOSledProvTOKol.Name = "VOSledProvTOKol";
            this.VOSledProvTOKol.ReadOnly = true;
            this.VOSledProvTOKol.Size = new System.Drawing.Size(119, 21);
            this.VOSledProvTOKol.TabIndex = 20;
            this.VOSledProvTOKol.Visible = false;
            // 
            // VOSledProvTOTip
            // 
            this.VOSledProvTOTip.Enabled = false;
            this.VOSledProvTOTip.FormattingEnabled = true;
            this.VOSledProvTOTip.Items.AddRange(new object[] {
            "дата",
            "мес.",
            "нет"});
            this.VOSledProvTOTip.Location = new System.Drawing.Point(198, 462);
            this.VOSledProvTOTip.Name = "VOSledProvTOTip";
            this.VOSledProvTOTip.Size = new System.Drawing.Size(54, 21);
            this.VOSledProvTOTip.Sorted = true;
            this.VOSledProvTOTip.TabIndex = 19;
            this.VOSledProvTOTip.Text = "дата";
            this.VOSledProvTOTip.SelectedIndexChanged += new System.EventHandler(this.VOSledProvTOTip_SelectedIndexChanged);
            // 
            // VODateSledProverkiKol
            // 
            this.VODateSledProverkiKol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VODateSledProverkiKol.Location = new System.Drawing.Point(258, 408);
            this.VODateSledProverkiKol.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.VODateSledProverkiKol.Name = "VODateSledProverkiKol";
            this.VODateSledProverkiKol.ReadOnly = true;
            this.VODateSledProverkiKol.Size = new System.Drawing.Size(119, 21);
            this.VODateSledProverkiKol.TabIndex = 16;
            this.VODateSledProverkiKol.Visible = false;
            // 
            // VODateSledProverkiTip
            // 
            this.VODateSledProverkiTip.Enabled = false;
            this.VODateSledProverkiTip.FormattingEnabled = true;
            this.VODateSledProverkiTip.Items.AddRange(new object[] {
            "дата",
            "мес.",
            "нет"});
            this.VODateSledProverkiTip.Location = new System.Drawing.Point(198, 408);
            this.VODateSledProverkiTip.Name = "VODateSledProverkiTip";
            this.VODateSledProverkiTip.Size = new System.Drawing.Size(54, 21);
            this.VODateSledProverkiTip.Sorted = true;
            this.VODateSledProverkiTip.TabIndex = 15;
            this.VODateSledProverkiTip.Text = "дата";
            this.VODateSledProverkiTip.SelectedIndexChanged += new System.EventHandler(this.VODateSledProverkiTip_SelectedIndexChanged);
            // 
            // VOMestoUstanovkiIHranenia
            // 
            this.VOMestoUstanovkiIHranenia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOMestoUstanovkiIHranenia.AutoCompleteCustomSource.AddRange(new string[] {
            "Аналитическая лаборатория",
            "Весовая",
            "Комната приема проб",
            "Комната определения фракционного состава",
            "Комната определения сероводорода",
            "Комната хранения хим. посуды",
            "Склад хранения арбитражных проб и хим. реактивов"});
            this.VOMestoUstanovkiIHranenia.Enabled = false;
            this.VOMestoUstanovkiIHranenia.FormattingEnabled = true;
            this.VOMestoUstanovkiIHranenia.Items.AddRange(new object[] {
            "Аналитическая лаборатория",
            "Весовая",
            "Комната определения сероводорода",
            "Комната определения фракционного состава",
            "Комната приема проб",
            "Комната хранения хим. посуды",
            "Склад хранения арбитражных проб и хим. реактивов"});
            this.VOMestoUstanovkiIHranenia.Location = new System.Drawing.Point(198, 354);
            this.VOMestoUstanovkiIHranenia.Name = "VOMestoUstanovkiIHranenia";
            this.VOMestoUstanovkiIHranenia.Size = new System.Drawing.Size(178, 21);
            this.VOMestoUstanovkiIHranenia.Sorted = true;
            this.VOMestoUstanovkiIHranenia.TabIndex = 12;
            // 
            // VOSledProvTODate
            // 
            this.VOSledProvTODate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOSledProvTODate.Checked = false;
            this.VOSledProvTODate.Enabled = false;
            this.VOSledProvTODate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.VOSledProvTODate.Location = new System.Drawing.Point(258, 462);
            this.VOSledProvTODate.Name = "VOSledProvTODate";
            this.VOSledProvTODate.Size = new System.Drawing.Size(118, 21);
            this.VOSledProvTODate.TabIndex = 71;
            // 
            // VODataPoslTO
            // 
            this.VODataPoslTO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VODataPoslTO.Checked = false;
            this.VODataPoslTO.Enabled = false;
            this.VODataPoslTO.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.VODataPoslTO.Location = new System.Drawing.Point(258, 435);
            this.VODataPoslTO.Name = "VODataPoslTO";
            this.VODataPoslTO.Size = new System.Drawing.Size(119, 21);
            this.VODataPoslTO.TabIndex = 18;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(122, 141);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(66, 13);
            this.label107.TabIndex = 69;
            this.label107.Text = "Тип, марка:";
            // 
            // VOTipMarka
            // 
            this.VOTipMarka.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOTipMarka.Location = new System.Drawing.Point(198, 138);
            this.VOTipMarka.Name = "VOTipMarka";
            this.VOTipMarka.ReadOnly = true;
            this.VOTipMarka.Size = new System.Drawing.Size(179, 21);
            this.VOTipMarka.TabIndex = 5;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(90, 166);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(98, 13);
            this.label106.TabIndex = 67;
            this.label106.Text = "Заводской номер:";
            // 
            // VOZavodNomer
            // 
            this.VOZavodNomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOZavodNomer.Location = new System.Drawing.Point(198, 165);
            this.VOZavodNomer.Name = "VOZavodNomer";
            this.VOZavodNomer.ReadOnly = true;
            this.VOZavodNomer.Size = new System.Drawing.Size(179, 21);
            this.VOZavodNomer.TabIndex = 6;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(112, 221);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(76, 13);
            this.label105.TabIndex = 65;
            this.label105.Text = "Год выпуска:";
            // 
            // VOGodVypuska
            // 
            this.VOGodVypuska.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOGodVypuska.Location = new System.Drawing.Point(198, 219);
            this.VOGodVypuska.Name = "VOGodVypuska";
            this.VOGodVypuska.ReadOnly = true;
            this.VOGodVypuska.Size = new System.Drawing.Size(179, 21);
            this.VOGodVypuska.TabIndex = 8;
            // 
            // VOInventarNomer
            // 
            this.VOInventarNomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOInventarNomer.Location = new System.Drawing.Point(198, 273);
            this.VOInventarNomer.Name = "VOInventarNomer";
            this.VOInventarNomer.ReadOnly = true;
            this.VOInventarNomer.Size = new System.Drawing.Size(179, 21);
            this.VOInventarNomer.TabIndex = 10;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(72, 438);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(116, 13);
            this.label104.TabIndex = 62;
            this.label104.Text = "Дата последнего ТО:";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(66, 465);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(122, 13);
            this.label103.TabIndex = 60;
            this.label103.Text = "Дата следующего ТО:";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(116, 493);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(72, 13);
            this.label78.TabIndex = 58;
            this.label78.Text = "Примечание:";
            // 
            // VOPrimechanie
            // 
            this.VOPrimechanie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOPrimechanie.Location = new System.Drawing.Point(198, 490);
            this.VOPrimechanie.Name = "VOPrimechanie";
            this.VOPrimechanie.ReadOnly = true;
            this.VOPrimechanie.Size = new System.Drawing.Size(179, 21);
            this.VOPrimechanie.TabIndex = 21;
            // 
            // VOCopyButton
            // 
            this.VOCopyButton.AllowDrop = true;
            this.VOCopyButton.AutoSize = true;
            this.VOCopyButton.Image = global::LIMS.Properties.Resources.copy_16x16;
            this.VOCopyButton.Location = new System.Drawing.Point(286, 52);
            this.VOCopyButton.Name = "VOCopyButton";
            this.VOCopyButton.Size = new System.Drawing.Size(89, 22);
            this.VOCopyButton.TabIndex = 2;
            this.VOCopyButton.Text = "Копировать";
            this.VOCopyButton.Click += new System.EventHandler(this.VOCopyButton_Click);
            // 
            // VODateProverkiTehnicheskogoSostoiania
            // 
            this.VODateProverkiTehnicheskogoSostoiania.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VODateProverkiTehnicheskogoSostoiania.Checked = false;
            this.VODateProverkiTehnicheskogoSostoiania.Enabled = false;
            this.VODateProverkiTehnicheskogoSostoiania.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.VODateProverkiTehnicheskogoSostoiania.Location = new System.Drawing.Point(258, 381);
            this.VODateProverkiTehnicheskogoSostoiania.Name = "VODateProverkiTehnicheskogoSostoiania";
            this.VODateProverkiTehnicheskogoSostoiania.Size = new System.Drawing.Size(119, 21);
            this.VODateProverkiTehnicheskogoSostoiania.TabIndex = 14;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(76, 200);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(112, 13);
            this.label69.TabIndex = 54;
            this.label69.Text = "вание организации):";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(15, 187);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(173, 13);
            this.label85.TabIndex = 53;
            this.label85.Text = "Изготовитель (страна, наимено-";
            // 
            // VON
            // 
            this.VON.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VON.Location = new System.Drawing.Point(198, 84);
            this.VON.Name = "VON";
            this.VON.ReadOnly = true;
            this.VON.Size = new System.Drawing.Size(179, 21);
            this.VON.TabIndex = 3;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(145, 84);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(43, 13);
            this.label81.TabIndex = 52;
            this.label81.Text = "№ п/п:";
            // 
            // VONaznachenie
            // 
            this.VONaznachenie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VONaznachenie.Location = new System.Drawing.Point(198, 300);
            this.VONaznachenie.Name = "VONaznachenie";
            this.VONaznachenie.ReadOnly = true;
            this.VONaznachenie.Size = new System.Drawing.Size(179, 21);
            this.VONaznachenie.TabIndex = 11;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(117, 303);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(71, 13);
            this.label83.TabIndex = 44;
            this.label83.Text = "Назначение:";
            // 
            // VOGodVvodaVEkspltatechiu
            // 
            this.VOGodVvodaVEkspltatechiu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOGodVvodaVEkspltatechiu.Location = new System.Drawing.Point(198, 246);
            this.VOGodVvodaVEkspltatechiu.Name = "VOGodVvodaVEkspltatechiu";
            this.VOGodVvodaVEkspltatechiu.ReadOnly = true;
            this.VOGodVvodaVEkspltatechiu.Size = new System.Drawing.Size(179, 21);
            this.VOGodVvodaVEkspltatechiu.TabIndex = 9;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(75, 275);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(113, 13);
            this.label84.TabIndex = 42;
            this.label84.Text = "Инвентарный номер:";
            // 
            // VOIzgotovitel
            // 
            this.VOIzgotovitel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOIzgotovitel.Location = new System.Drawing.Point(198, 192);
            this.VOIzgotovitel.Name = "VOIzgotovitel";
            this.VOIzgotovitel.ReadOnly = true;
            this.VOIzgotovitel.Size = new System.Drawing.Size(179, 21);
            this.VOIzgotovitel.TabIndex = 7;
            // 
            // VODateSledProverkiDate
            // 
            this.VODateSledProverkiDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VODateSledProverkiDate.Checked = false;
            this.VODateSledProverkiDate.Enabled = false;
            this.VODateSledProverkiDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.VODateSledProverkiDate.Location = new System.Drawing.Point(258, 408);
            this.VODateSledProverkiDate.Name = "VODateSledProverkiDate";
            this.VODateSledProverkiDate.Size = new System.Drawing.Size(119, 21);
            this.VODateSledProverkiDate.TabIndex = 38;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(37, 412);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(151, 13);
            this.label87.TabIndex = 33;
            this.label87.Text = "Дата следующей проверки:";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(2, 384);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(186, 13);
            this.label88.TabIndex = 32;
            this.label88.Text = "Дата проверки технич. состояния:";
            // 
            // VOID
            // 
            this.VOID.AutoSize = true;
            this.VOID.Location = new System.Drawing.Point(260, 25);
            this.VOID.Name = "VOID";
            this.VOID.Size = new System.Drawing.Size(0, 13);
            this.VOID.TabIndex = 27;
            this.VOID.Visible = false;
            // 
            // VOName
            // 
            this.VOName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VOName.Location = new System.Drawing.Point(198, 111);
            this.VOName.Name = "VOName";
            this.VOName.ReadOnly = true;
            this.VOName.Size = new System.Drawing.Size(179, 21);
            this.VOName.TabIndex = 4;
            // 
            // VOEditCheckBox
            // 
            this.VOEditCheckBox.AutoSize = true;
            this.VOEditCheckBox.Location = new System.Drawing.Point(198, 517);
            this.VOEditCheckBox.Name = "VOEditCheckBox";
            this.VOEditCheckBox.Size = new System.Drawing.Size(105, 17);
            this.VOEditCheckBox.TabIndex = 22;
            this.VOEditCheckBox.Text = "Редактировать";
            this.VOEditCheckBox.UseVisualStyleBackColor = true;
            this.VOEditCheckBox.CheckedChanged += new System.EventHandler(this.VOEditCheckBox_CheckedChanged);
            // 
            // VONewButton
            // 
            this.VONewButton.AllowDrop = true;
            this.VONewButton.AutoSize = true;
            this.VONewButton.Image = global::LIMS.Properties.Resources.add_16x16;
            this.VONewButton.Location = new System.Drawing.Point(197, 52);
            this.VONewButton.Name = "VONewButton";
            this.VONewButton.Size = new System.Drawing.Size(78, 22);
            this.VONewButton.TabIndex = 1;
            this.VONewButton.Text = "Добавить";
            this.VONewButton.Click += new System.EventHandler(this.VONewButton_Click);
            // 
            // VOSaveButton
            // 
            this.VOSaveButton.AutoSize = true;
            this.VOSaveButton.Image = global::LIMS.Properties.Resources.save_16x16;
            this.VOSaveButton.Location = new System.Drawing.Point(198, 540);
            this.VOSaveButton.Name = "VOSaveButton";
            this.VOSaveButton.Size = new System.Drawing.Size(83, 22);
            this.VOSaveButton.TabIndex = 23;
            this.VOSaveButton.Text = "Сохранить";
            this.VOSaveButton.Click += new System.EventHandler(this.VOSaveButton_Click);
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label90.Location = new System.Drawing.Point(159, 11);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(197, 16);
            this.label90.TabIndex = 23;
            this.label90.Text = "Вспомогательное оборудование";
            // 
            // pictureBox6
            // 
            this.pictureBox6.ErrorImage = null;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(33, 7);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(106, 81);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 11;
            this.pictureBox6.TabStop = false;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(30, 357);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(158, 13);
            this.label94.TabIndex = 4;
            this.label94.Text = "Место установки и хранения:";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(39, 246);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(149, 13);
            this.label96.TabIndex = 2;
            this.label96.Text = "Год ввода в эксплуатацию:";
            // 
            // label98
            // 
            this.label98.AutoEllipsis = true;
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(104, 114);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(84, 13);
            this.label98.TabIndex = 0;
            this.label98.Text = "Наименование:";
            // 
            // VOGrid
            // 
            this.VOGrid.AccessibleDescription = "";
            this.VOGrid.AccessibleName = "";
            this.VOGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VOGrid.Location = new System.Drawing.Point(0, 40);
            this.VOGrid.MainView = this.VOGridView;
            this.VOGrid.Name = "VOGrid";
            this.VOGrid.Size = new System.Drawing.Size(602, 663);
            this.VOGrid.TabIndex = 999999;
            this.VOGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.VOGridView});
            // 
            // VOGridView
            // 
            this.VOGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.VOGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VOGridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.VOGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.VOGridView.AppearancePrint.EvenRow.Options.UseTextOptions = true;
            this.VOGridView.AppearancePrint.EvenRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.VOGridView.AppearancePrint.GroupRow.Options.UseTextOptions = true;
            this.VOGridView.AppearancePrint.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.VOGridView.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.VOGridView.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.VOGridView.AppearancePrint.Lines.Options.UseTextOptions = true;
            this.VOGridView.AppearancePrint.Lines.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.VOGridView.AppearancePrint.OddRow.Options.UseTextOptions = true;
            this.VOGridView.AppearancePrint.OddRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.VOGridView.AppearancePrint.Preview.Options.UseTextOptions = true;
            this.VOGridView.AppearancePrint.Preview.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.VOGridView.AppearancePrint.Row.Options.UseTextOptions = true;
            this.VOGridView.AppearancePrint.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.VOGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.VOGridView.GridControl = this.VOGrid;
            this.VOGridView.Name = "VOGridView";
            this.VOGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.VOGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.VOGridView.OptionsBehavior.AllowIncrementalSearch = true;
            this.VOGridView.OptionsBehavior.Editable = false;
            this.VOGridView.OptionsCustomization.AllowRowSizing = true;
            this.VOGridView.OptionsFind.AlwaysVisible = true;
            this.VOGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.VOGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.VOGridView.OptionsView.RowAutoHeight = true;
            this.VOGridView.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.VOGridView_RowClick);
            this.VOGridView.ColumnWidthChanged += new DevExpress.XtraGrid.Views.Base.ColumnEventHandler(this.VOGridView_ColumnWidthChanged);
            this.VOGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.VOGridView_PopupMenuShowing);
            this.VOGridView.StartSorting += new System.EventHandler(this.VOGridView_StartSorting);
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.VOCellMergeButton);
            this.panelControl8.Controls.Add(this.VOArhivButton);
            this.panelControl8.Controls.Add(this.VOColumnCheckButton);
            this.panelControl8.Controls.Add(this.VOUpdateButton);
            this.panelControl8.Controls.Add(this.VOPrintButton);
            this.panelControl8.Controls.Add(this.VODeleteButton);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(0, 0);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(602, 40);
            this.panelControl8.TabIndex = 0;
            // 
            // VOCellMergeButton
            // 
            this.VOCellMergeButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.VOCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            this.VOCellMergeButton.Location = new System.Drawing.Point(122, 2);
            this.VOCellMergeButton.Name = "VOCellMergeButton";
            this.VOCellMergeButton.Size = new System.Drawing.Size(40, 36);
            this.VOCellMergeButton.TabIndex = 27;
            this.VOCellMergeButton.Click += new System.EventHandler(this.VOCellMergeButton_Click);
            // 
            // VOArhivButton
            // 
            this.VOArhivButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.VOArhivButton.Image = global::LIMS.Properties.Resources.checkbuttons_32x32;
            this.VOArhivButton.Location = new System.Drawing.Point(82, 2);
            this.VOArhivButton.Name = "VOArhivButton";
            this.VOArhivButton.Size = new System.Drawing.Size(40, 36);
            this.VOArhivButton.TabIndex = 26;
            this.VOArhivButton.Click += new System.EventHandler(this.VOArhivButton_Click_1);
            // 
            // VOColumnCheckButton
            // 
            this.VOColumnCheckButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.VOColumnCheckButton.Image = global::LIMS.Properties.Resources.insertcolumns_32x32;
            this.VOColumnCheckButton.Location = new System.Drawing.Point(520, 2);
            this.VOColumnCheckButton.Name = "VOColumnCheckButton";
            this.VOColumnCheckButton.Size = new System.Drawing.Size(40, 36);
            this.VOColumnCheckButton.TabIndex = 28;
            this.VOColumnCheckButton.Click += new System.EventHandler(this.VOColumnCheckButton_Click);
            // 
            // VOUpdateButton
            // 
            this.VOUpdateButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.VOUpdateButton.Image = global::LIMS.Properties.Resources.refresh_32x32;
            this.VOUpdateButton.Location = new System.Drawing.Point(560, 2);
            this.VOUpdateButton.Name = "VOUpdateButton";
            this.VOUpdateButton.Size = new System.Drawing.Size(40, 36);
            this.VOUpdateButton.TabIndex = 29;
            this.VOUpdateButton.Click += new System.EventHandler(this.VOUpdateButton_Click);
            // 
            // VOPrintButton
            // 
            this.VOPrintButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.VOPrintButton.Image = global::LIMS.Properties.Resources.printer_32x32;
            this.VOPrintButton.Location = new System.Drawing.Point(42, 2);
            this.VOPrintButton.Name = "VOPrintButton";
            this.VOPrintButton.Size = new System.Drawing.Size(40, 36);
            this.VOPrintButton.TabIndex = 25;
            this.VOPrintButton.Click += new System.EventHandler(this.VOPrintButton_Click);
            // 
            // VODeleteButton
            // 
            this.VODeleteButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.VODeleteButton.Image = global::LIMS.Properties.Resources.delete_32x32;
            this.VODeleteButton.Location = new System.Drawing.Point(2, 2);
            this.VODeleteButton.Name = "VODeleteButton";
            this.VODeleteButton.Size = new System.Drawing.Size(40, 36);
            this.VODeleteButton.TabIndex = 24;
            this.VODeleteButton.Click += new System.EventHandler(this.VODeleteButton_Click);
            // 
            // VLK
            // 
            this.VLK.Controls.Add(this.GSOGrid);
            this.VLK.Controls.Add(this.panelControl6);
            this.VLK.Controls.Add(this.GSOSpisokTab);
            this.VLK.Name = "VLK";
            this.VLK.Size = new System.Drawing.Size(988, 703);
            this.VLK.Text = "ВЛК";
            // 
            // GSOGrid
            // 
            this.GSOGrid.AccessibleDescription = "";
            this.GSOGrid.AccessibleName = "";
            this.GSOGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GSOGrid.Location = new System.Drawing.Point(0, 62);
            this.GSOGrid.MainView = this.GSOGridView;
            this.GSOGrid.Name = "GSOGrid";
            this.GSOGrid.Size = new System.Drawing.Size(988, 641);
            this.GSOGrid.TabIndex = 99999;
            this.GSOGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GSOGridView});
            this.GSOGrid.DoubleClick += new System.EventHandler(this.GSOGrid_DoubleClick);
            // 
            // GSOGridView
            // 
            this.GSOGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.GSOGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GSOGridView.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GSOGridView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GSOGridView.AppearancePrint.EvenRow.Options.UseTextOptions = true;
            this.GSOGridView.AppearancePrint.EvenRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GSOGridView.AppearancePrint.GroupRow.Options.UseTextOptions = true;
            this.GSOGridView.AppearancePrint.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GSOGridView.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.GSOGridView.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GSOGridView.AppearancePrint.Lines.Options.UseTextOptions = true;
            this.GSOGridView.AppearancePrint.Lines.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GSOGridView.AppearancePrint.OddRow.Options.UseTextOptions = true;
            this.GSOGridView.AppearancePrint.OddRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GSOGridView.AppearancePrint.Preview.Options.UseTextOptions = true;
            this.GSOGridView.AppearancePrint.Preview.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GSOGridView.AppearancePrint.Row.Options.UseTextOptions = true;
            this.GSOGridView.AppearancePrint.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GSOGridView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3});
            this.GSOGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.GSOGridView.GridControl = this.GSOGrid;
            this.GSOGridView.Name = "GSOGridView";
            this.GSOGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.GSOGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.GSOGridView.OptionsBehavior.AllowIncrementalSearch = true;
            this.GSOGridView.OptionsBehavior.Editable = false;
            this.GSOGridView.OptionsCustomization.AllowRowSizing = true;
            this.GSOGridView.OptionsFind.AlwaysVisible = true;
            this.GSOGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.GSOGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.GSOGridView.OptionsView.ShowGroupPanel = false;
            this.GSOGridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.GSOGridView_CustomDrawCell);
            this.GSOGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GSOGridView_PopupMenuShowing);
            this.GSOGridView.StartSorting += new System.EventHandler(this.GSOGridView_StartSorting);
            this.GSOGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GSOGridView_FocusedRowChanged);
            this.GSOGridView.ColumnFilterChanged += new System.EventHandler(this.GSOGridView_ColumnFilterChanged);
            this.GSOGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.GSOGridView_MouseUp);
            // 
            // gridBand3
            // 
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 0;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.CloseAllTabsButton);
            this.panelControl6.Controls.Add(this.FiltrButton);
            this.panelControl6.Controls.Add(this.VLKCellMergeButton);
            this.panelControl6.Controls.Add(this.DiagramButton);
            this.panelControl6.Controls.Add(this.VLKUpdateButton);
            this.panelControl6.Controls.Add(this.VLKPrintButton);
            this.panelControl6.Controls.Add(this.AddAnalizProbiButton);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(0, 22);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(988, 40);
            this.panelControl6.TabIndex = 4;
            // 
            // CloseAllTabsButton
            // 
            this.CloseAllTabsButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.CloseAllTabsButton.Image = global::LIMS.Properties.Resources.tab_close_other_converted1;
            this.CloseAllTabsButton.Location = new System.Drawing.Point(202, 2);
            this.CloseAllTabsButton.Name = "CloseAllTabsButton";
            this.CloseAllTabsButton.Size = new System.Drawing.Size(40, 36);
            this.CloseAllTabsButton.TabIndex = 6;
            this.CloseAllTabsButton.Click += new System.EventHandler(this.CloseAllTabsButton_Click);
            // 
            // FiltrButton
            // 
            this.FiltrButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.FiltrButton.Image = global::LIMS.Properties.Resources.adateoccurring_32x32;
            this.FiltrButton.Location = new System.Drawing.Point(162, 2);
            this.FiltrButton.Name = "FiltrButton";
            this.FiltrButton.Size = new System.Drawing.Size(40, 36);
            this.FiltrButton.TabIndex = 5;
            this.FiltrButton.Click += new System.EventHandler(this.FiltrButton_Click);
            // 
            // VLKCellMergeButton
            // 
            this.VLKCellMergeButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.VLKCellMergeButton.Image = global::LIMS.Properties.Resources.gridmergecells_32x32;
            this.VLKCellMergeButton.Location = new System.Drawing.Point(122, 2);
            this.VLKCellMergeButton.Name = "VLKCellMergeButton";
            this.VLKCellMergeButton.Size = new System.Drawing.Size(40, 36);
            this.VLKCellMergeButton.TabIndex = 4;
            this.VLKCellMergeButton.Click += new System.EventHandler(this.VLKCellMergeButton_Click);
            // 
            // DiagramButton
            // 
            this.DiagramButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.DiagramButton.Image = global::LIMS.Properties.Resources._3dline_32x32;
            this.DiagramButton.Location = new System.Drawing.Point(82, 2);
            this.DiagramButton.Name = "DiagramButton";
            this.DiagramButton.Size = new System.Drawing.Size(40, 36);
            this.DiagramButton.TabIndex = 3;
            this.DiagramButton.Visible = false;
            this.DiagramButton.Click += new System.EventHandler(this.DiagramButton_Click);
            // 
            // VLKUpdateButton
            // 
            this.VLKUpdateButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.VLKUpdateButton.Image = global::LIMS.Properties.Resources.refresh_32x32;
            this.VLKUpdateButton.Location = new System.Drawing.Point(946, 2);
            this.VLKUpdateButton.Name = "VLKUpdateButton";
            this.VLKUpdateButton.Size = new System.Drawing.Size(40, 36);
            this.VLKUpdateButton.TabIndex = 7;
            this.VLKUpdateButton.Click += new System.EventHandler(this.VLKUpdateButton_Click);
            // 
            // VLKPrintButton
            // 
            this.VLKPrintButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.VLKPrintButton.Image = global::LIMS.Properties.Resources.printer_32x32;
            this.VLKPrintButton.Location = new System.Drawing.Point(42, 2);
            this.VLKPrintButton.Name = "VLKPrintButton";
            this.VLKPrintButton.Size = new System.Drawing.Size(40, 36);
            this.VLKPrintButton.TabIndex = 2;
            this.VLKPrintButton.Visible = false;
            this.VLKPrintButton.Click += new System.EventHandler(this.VLKPrintButton_Click);
            // 
            // AddAnalizProbiButton
            // 
            this.AddAnalizProbiButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.AddAnalizProbiButton.Image = global::LIMS.Properties.Resources.add_32x32;
            this.AddAnalizProbiButton.Location = new System.Drawing.Point(2, 2);
            this.AddAnalizProbiButton.Name = "AddAnalizProbiButton";
            this.AddAnalizProbiButton.Size = new System.Drawing.Size(40, 36);
            this.AddAnalizProbiButton.TabIndex = 1;
            this.AddAnalizProbiButton.Visible = false;
            this.AddAnalizProbiButton.Click += new System.EventHandler(this.AddAnalizButton_Click);
            // 
            // GSOSpisokTab
            // 
            this.GSOSpisokTab.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            this.GSOSpisokTab.Dock = System.Windows.Forms.DockStyle.Top;
            this.GSOSpisokTab.ImeMode = System.Windows.Forms.ImeMode.Close;
            this.GSOSpisokTab.Location = new System.Drawing.Point(0, 0);
            this.GSOSpisokTab.Name = "GSOSpisokTab";
            this.GSOSpisokTab.SelectedTabPage = this.MainGSOTab;
            this.GSOSpisokTab.Size = new System.Drawing.Size(988, 22);
            this.GSOSpisokTab.TabIndex = 3;
            this.GSOSpisokTab.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.MainGSOTab});
            this.GSOSpisokTab.CloseButtonClick += new System.EventHandler(this.GSOSpisokTab_CloseButtonClick);
            this.GSOSpisokTab.Click += new System.EventHandler(this.GSOSpisokTab_Click);
            // 
            // MainGSOTab
            // 
            this.MainGSOTab.Name = "MainGSOTab";
            this.MainGSOTab.ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            this.MainGSOTab.Size = new System.Drawing.Size(982, 0);
            this.MainGSOTab.Text = "Главная";
            // 
            // Nastroiki
            // 
            this.Nastroiki.Controls.Add(this.panelControl9);
            this.Nastroiki.Name = "Nastroiki";
            this.Nastroiki.Size = new System.Drawing.Size(988, 703);
            this.Nastroiki.Text = "Настройки";
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.AddUser);
            this.panelControl9.Controls.Add(this.UseSkinsCheckBox);
            this.panelControl9.Controls.Add(this.SkinComboBox);
            this.panelControl9.Controls.Add(this.label133);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl9.Location = new System.Drawing.Point(0, 0);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(988, 703);
            this.panelControl9.TabIndex = 0;
            // 
            // AddUser
            // 
            this.AddUser.AutoSize = true;
            this.AddUser.Location = new System.Drawing.Point(15, 66);
            this.AddUser.Name = "AddUser";
            this.AddUser.Size = new System.Drawing.Size(162, 33);
            this.AddUser.TabIndex = 9;
            this.AddUser.Text = "Добавление/изменение \r\nучетной записи пользователя";
            this.AddUser.Visible = false;
            this.AddUser.Click += new System.EventHandler(this.AddUser_Click);
            // 
            // UseSkinsCheckBox
            // 
            this.UseSkinsCheckBox.AutoSize = true;
            this.UseSkinsCheckBox.Checked = true;
            this.UseSkinsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UseSkinsCheckBox.Location = new System.Drawing.Point(15, 5);
            this.UseSkinsCheckBox.Name = "UseSkinsCheckBox";
            this.UseSkinsCheckBox.Size = new System.Drawing.Size(131, 17);
            this.UseSkinsCheckBox.TabIndex = 3;
            this.UseSkinsCheckBox.Text = "Использовать скины";
            this.UseSkinsCheckBox.UseVisualStyleBackColor = true;
            this.UseSkinsCheckBox.CheckedChanged += new System.EventHandler(this.UseSkinsCheckBox_CheckedChanged);
            // 
            // SkinComboBox
            // 
            this.SkinComboBox.FormattingEnabled = true;
            this.SkinComboBox.Location = new System.Drawing.Point(54, 28);
            this.SkinComboBox.Name = "SkinComboBox";
            this.SkinComboBox.Size = new System.Drawing.Size(125, 21);
            this.SkinComboBox.TabIndex = 2;
            this.SkinComboBox.SelectedValueChanged += new System.EventHandler(this.SkinComboBox_SelectedValueChanged);
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(12, 31);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(36, 13);
            this.label133.TabIndex = 1;
            this.label133.Text = "Скин:";
            // 
            // ContextMenuStripMain
            // 
            this.ContextMenuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Cut,
            this.Copy,
            this.Paste,
            this.Clear,
            this.toolStripSeparator1,
            this.PlusMinus,
            this.Paste0,
            this.Paste2,
            this.Paste3,
            this.PasteProcent,
            this.PasteGradus,
            this.PasteFaringeit,
            this.Pasteмм2с,
            this.Pasteмм2с2,
            this.Pasteмгдм3,
            this.PasteКгм3,
            this.Pasteмгм3,
            this.Pastecм3,
            this.Pasteдм3,
            this.Pasteм3,
            this.PasteмкСмсм,
            this.Pasteмкгг,
            this.Pasteppm,
            this.PasteрН});
            this.ContextMenuStripMain.Name = "ContextMenuStrip";
            this.ContextMenuStripMain.Size = new System.Drawing.Size(188, 516);
            // 
            // Cut
            // 
            this.Cut.Name = "Cut";
            this.Cut.Size = new System.Drawing.Size(187, 22);
            this.Cut.Text = "Вырезать";
            this.Cut.Click += new System.EventHandler(this.Cut_Click);
            // 
            // Copy
            // 
            this.Copy.Name = "Copy";
            this.Copy.Size = new System.Drawing.Size(187, 22);
            this.Copy.Text = "Копировать";
            this.Copy.Click += new System.EventHandler(this.Copy_Click);
            // 
            // Paste
            // 
            this.Paste.Name = "Paste";
            this.Paste.Size = new System.Drawing.Size(187, 22);
            this.Paste.Text = "Вставить";
            this.Paste.Click += new System.EventHandler(this.Paste_Click);
            // 
            // Clear
            // 
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(187, 22);
            this.Clear.Text = "Очистить";
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(184, 6);
            // 
            // PlusMinus
            // 
            this.PlusMinus.Name = "PlusMinus";
            this.PlusMinus.Size = new System.Drawing.Size(187, 22);
            this.PlusMinus.Text = "Вставить \"±\"";
            this.PlusMinus.Click += new System.EventHandler(this.PlusMinus_Click);
            // 
            // Paste0
            // 
            this.Paste0.Name = "Paste0";
            this.Paste0.Size = new System.Drawing.Size(187, 22);
            this.Paste0.Text = "Вставить \"°\"";
            this.Paste0.Click += new System.EventHandler(this.Paste0_Click);
            // 
            // Paste2
            // 
            this.Paste2.Name = "Paste2";
            this.Paste2.Size = new System.Drawing.Size(187, 22);
            this.Paste2.Text = "Вставить \"²\"";
            this.Paste2.Click += new System.EventHandler(this.Paste2_Click);
            // 
            // Paste3
            // 
            this.Paste3.Name = "Paste3";
            this.Paste3.Size = new System.Drawing.Size(187, 22);
            this.Paste3.Text = "Вставить \"³\"";
            this.Paste3.Click += new System.EventHandler(this.Paste3_Click);
            // 
            // PasteProcent
            // 
            this.PasteProcent.Name = "PasteProcent";
            this.PasteProcent.Size = new System.Drawing.Size(187, 22);
            this.PasteProcent.Text = "Вставить \"%\"";
            this.PasteProcent.Click += new System.EventHandler(this.PasteProcent_Click);
            // 
            // PasteGradus
            // 
            this.PasteGradus.Name = "PasteGradus";
            this.PasteGradus.Size = new System.Drawing.Size(187, 22);
            this.PasteGradus.Text = "Вставить \"°С\"";
            this.PasteGradus.Click += new System.EventHandler(this.PasteGradus_Click);
            // 
            // PasteFaringeit
            // 
            this.PasteFaringeit.Name = "PasteFaringeit";
            this.PasteFaringeit.Size = new System.Drawing.Size(187, 22);
            this.PasteFaringeit.Text = "Вставить \"°F\"";
            this.PasteFaringeit.Click += new System.EventHandler(this.PasteFaringeit_Click);
            // 
            // Pasteмм2с
            // 
            this.Pasteмм2с.Name = "Pasteмм2с";
            this.Pasteмм2с.Size = new System.Drawing.Size(187, 22);
            this.Pasteмм2с.Text = "Вставить \"мм2/с\"";
            this.Pasteмм2с.Click += new System.EventHandler(this.Pasteмм2с_Click);
            // 
            // Pasteмм2с2
            // 
            this.Pasteмм2с2.Name = "Pasteмм2с2";
            this.Pasteмм2с2.Size = new System.Drawing.Size(187, 22);
            this.Pasteмм2с2.Text = "Вставить \"мм2/с2\"";
            this.Pasteмм2с2.Click += new System.EventHandler(this.Pasteмм2с2_Click);
            // 
            // Pasteмгдм3
            // 
            this.Pasteмгдм3.Name = "Pasteмгдм3";
            this.Pasteмгдм3.Size = new System.Drawing.Size(187, 22);
            this.Pasteмгдм3.Text = "Вставить \"мг/дм3\"";
            this.Pasteмгдм3.Click += new System.EventHandler(this.Pasteмгдм3_Click);
            // 
            // PasteКгм3
            // 
            this.PasteКгм3.Name = "PasteКгм3";
            this.PasteКгм3.Size = new System.Drawing.Size(187, 22);
            this.PasteКгм3.Text = "Вставить \"кг/м3\"";
            this.PasteКгм3.Click += new System.EventHandler(this.PasteКгм3_Click);
            // 
            // Pasteмгм3
            // 
            this.Pasteмгм3.Name = "Pasteмгм3";
            this.Pasteмгм3.Size = new System.Drawing.Size(187, 22);
            this.Pasteмгм3.Text = "Вставить \"мг/м3\"";
            this.Pasteмгм3.Click += new System.EventHandler(this.Pasteмгм3_Click);
            // 
            // Pastecм3
            // 
            this.Pastecм3.Name = "Pastecм3";
            this.Pastecм3.Size = new System.Drawing.Size(187, 22);
            this.Pastecм3.Text = "Вставить \"cм3\"";
            this.Pastecм3.Click += new System.EventHandler(this.Pastecм3_Click);
            // 
            // Pasteдм3
            // 
            this.Pasteдм3.Name = "Pasteдм3";
            this.Pasteдм3.Size = new System.Drawing.Size(187, 22);
            this.Pasteдм3.Text = "Вставить \"дм3\"";
            this.Pasteдм3.Click += new System.EventHandler(this.Pasteдм3_Click);
            // 
            // Pasteм3
            // 
            this.Pasteм3.Name = "Pasteм3";
            this.Pasteм3.Size = new System.Drawing.Size(187, 22);
            this.Pasteм3.Text = "Вставить \"м3\"";
            this.Pasteм3.Click += new System.EventHandler(this.Pasteм3_Click);
            // 
            // PasteмкСмсм
            // 
            this.PasteмкСмсм.Name = "PasteмкСмсм";
            this.PasteмкСмсм.Size = new System.Drawing.Size(187, 22);
            this.PasteмкСмсм.Text = "Вставить \"мкСм/см\"";
            this.PasteмкСмсм.Click += new System.EventHandler(this.PasteмкСмсм_Click);
            // 
            // Pasteмкгг
            // 
            this.Pasteмкгг.Name = "Pasteмкгг";
            this.Pasteмкгг.Size = new System.Drawing.Size(187, 22);
            this.Pasteмкгг.Text = "Вставить \"мкг/г\"";
            this.Pasteмкгг.Click += new System.EventHandler(this.Pasteмкгг_Click);
            // 
            // Pasteppm
            // 
            this.Pasteppm.Name = "Pasteppm";
            this.Pasteppm.Size = new System.Drawing.Size(187, 22);
            this.Pasteppm.Text = "Вставить \"ppm\"";
            this.Pasteppm.Click += new System.EventHandler(this.Pasteppm_Click);
            // 
            // PasteрН
            // 
            this.PasteрН.Name = "PasteрН";
            this.PasteрН.Size = new System.Drawing.Size(187, 22);
            this.PasteрН.Text = "Вставить \"рН\"";
            this.PasteрН.Click += new System.EventHandler(this.PasteрН_Click);
            // 
            // Loader
            // 
            this.Loader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Loader.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Loader.Appearance.Options.UseBackColor = true;
            this.Loader.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Loader.AppearanceCaption.Options.UseFont = true;
            this.Loader.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Loader.AppearanceDescription.Options.UseFont = true;
            this.Loader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.Loader.Caption = "Подождите";
            this.Loader.Description = "Формирование документа ...";
            this.Loader.Location = new System.Drawing.Point(382, 352);
            this.Loader.Name = "Loader";
            this.Loader.Size = new System.Drawing.Size(246, 66);
            this.Loader.TabIndex = 2;
            this.Loader.Visible = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 731);
            this.Controls.Add(this.Loader);
            this.Controls.Add(this.TabControlMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlMain)).EndInit();
            this.TabControlMain.ResumeLayout(false);
            this.Reactiv.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ReactivPanel)).EndInit();
            this.ReactivPanel.ResumeLayout(false);
            this.ReactivPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivSrokGodnostiMounth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivKol_vo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactivPanelControl)).EndInit();
            this.ReactivPanelControl.ResumeLayout(false);
            this.Personal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PersonalPanel)).EndInit();
            this.PersonalPanel.ResumeLayout(false);
            this.PersonalPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalSrokAttestachiiKol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalSrokZnaniiOTiPBKol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalSrokGasKol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalOpitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.NTD.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NTDPanel)).EndInit();
            this.NTDPanel.ResumeLayout(false);
            this.NTDPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NTDKontrolKolvo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NTDRabKolvo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NTDGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NTDGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            this.SI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SIPanel)).EndInit();
            this.SIPanel.ResumeLayout(false);
            this.SIPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SIDataSledTOKol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SIDateSledPoverkiKol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SIGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SIGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.IO.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IOPanel)).EndInit();
            this.IOPanel.ResumeLayout(false);
            this.IOPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IODataSledTOKol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IODateSledAttistachiiKol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IOGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IOGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.VO.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VOPanel)).EndInit();
            this.VOPanel.ResumeLayout(false);
            this.VOPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VOSledProvTOKol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VODateSledProverkiKol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VOGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VOGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.VLK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GSOGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GSOGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GSOSpisokTab)).EndInit();
            this.GSOSpisokTab.ResumeLayout(false);
            this.Nastroiki.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            this.ContextMenuStripMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraTab.XtraTabPage Reactiv;
        private DevExpress.XtraTab.XtraTabPage Personal;
        private DevExpress.XtraTab.XtraTabPage NTD;
        private DevExpress.XtraTab.XtraTabPage SI;
        private DevExpress.XtraTab.XtraTabPage VO;
        private DevExpress.XtraTab.XtraTabPage VLK;
        private DevExpress.XtraEditors.PanelControl ReactivPanelControl;
        public DevExpress.XtraGrid.Views.Grid.GridView ReactivGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.SimpleButton ReactivPrintButton;
        private DevExpress.XtraEditors.SimpleButton ReactivDeleteButton;
        public DevExpress.XtraGrid.GridControl ReactivGrid;
        private DevExpress.XtraEditors.SimpleButton RefreshReactivButton;
        private DevExpress.XtraTab.XtraTabPage IO;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.SimpleButton ReactivCheckColumnButton;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        public DevExpress.XtraGrid.GridControl PersonalGrid;
        public DevExpress.XtraGrid.Views.Grid.GridView PersonalGridView;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton PersonalColumnCheckButton;
        private DevExpress.XtraEditors.SimpleButton PersonalUpdateButton;
        private DevExpress.XtraEditors.SimpleButton PersonalPrintButton;
        private DevExpress.XtraEditors.SimpleButton PersonalDeleteButton;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label30;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        public DevExpress.XtraGrid.GridControl NTDGrid;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.SimpleButton NTDColumnCheckButton;
        private DevExpress.XtraEditors.SimpleButton NTDUpdateButton;
        private DevExpress.XtraEditors.SimpleButton NTDPrintButton;
        private DevExpress.XtraEditors.SimpleButton NTDDeleteButton;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        public DevExpress.XtraGrid.GridControl SIGrid;
        public DevExpress.XtraGrid.Views.Grid.GridView SIGridView;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.SimpleButton SIColumnCheckButton;
        private DevExpress.XtraEditors.SimpleButton SIUpdateButton;
        private DevExpress.XtraEditors.SimpleButton SIPrintButton;
        private DevExpress.XtraEditors.SimpleButton SIDeleteButton;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        public DevExpress.XtraGrid.GridControl IOGrid;
        public DevExpress.XtraGrid.Views.Grid.GridView IOGridView;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.SimpleButton IOColumnCheckButton;
        private DevExpress.XtraEditors.SimpleButton IOUpdateButton;
        private DevExpress.XtraEditors.SimpleButton IOPrintButton;
        private DevExpress.XtraEditors.SimpleButton IODeleteButton;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label98;
        public DevExpress.XtraGrid.GridControl VOGrid;
        public DevExpress.XtraGrid.Views.Grid.GridView VOGridView;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.SimpleButton VOColumnCheckButton;
        private DevExpress.XtraEditors.SimpleButton VOUpdateButton;
        private DevExpress.XtraEditors.SimpleButton VOPrintButton;
        private DevExpress.XtraEditors.SimpleButton VODeleteButton;
        public System.Windows.Forms.DateTimePicker VODateProverkiTehnicheskogoSostoiania;
        public System.Windows.Forms.TextBox VON;
        public System.Windows.Forms.TextBox VONaznachenie;
        public System.Windows.Forms.TextBox VOGodVvodaVEkspltatechiu;
        public System.Windows.Forms.TextBox VOIzgotovitel;
        public System.Windows.Forms.DateTimePicker VODateSledProverkiDate;
        public System.Windows.Forms.TextBox VOName;
        public System.Windows.Forms.CheckBox VOEditCheckBox;
        public DevExpress.XtraEditors.SimpleButton VONewButton;
        public DevExpress.XtraEditors.SimpleButton VOSaveButton;
        public System.Windows.Forms.Label VOID;
        public DevExpress.XtraEditors.PanelControl VOPanel;
        public System.Windows.Forms.DateTimePicker SIDateSledPoverkiDate;
        public System.Windows.Forms.Label SIID;
        public System.Windows.Forms.TextBox SIDiapozonIzm;
        public System.Windows.Forms.TextBox SINameProd;
        public System.Windows.Forms.CheckBox SIEditCheckBox;
        public DevExpress.XtraEditors.SimpleButton SINewButton;
        public DevExpress.XtraEditors.SimpleButton SISaveButton;
        public System.Windows.Forms.TextBox SIPrimechanie;
        public System.Windows.Forms.TextBox SISvidetelstvoOPoverke;
        public System.Windows.Forms.TextBox SIClassTochnosti;
        public System.Windows.Forms.TextBox SIVvodVEkspluatachiu;
        public System.Windows.Forms.TextBox SIIzgotovitel;
        public System.Windows.Forms.TextBox SINameSI;
        public DevExpress.XtraEditors.PanelControl IOPanel;
        public DevExpress.XtraEditors.PanelControl SIPanel;
        public System.Windows.Forms.TextBox IOPrimechanie;
        public System.Windows.Forms.TextBox IOGodVvodaVEkspluatachiu;
        public System.Windows.Forms.TextBox IOIzgotovitel;
        public System.Windows.Forms.TextBox IOName;
        public System.Windows.Forms.TextBox IONameVidovIspitanii;
        public System.Windows.Forms.DateTimePicker IODateSledAttistachiiDate;
        public System.Windows.Forms.TextBox IOTehnichHaracter;
        public System.Windows.Forms.CheckBox IOEditCheckBox;
        public DevExpress.XtraEditors.SimpleButton IONewButton;
        public DevExpress.XtraEditors.SimpleButton IOSaveButton;
        public System.Windows.Forms.TextBox ION;
        public System.Windows.Forms.Label IOID;
        public System.Windows.Forms.DateTimePicker NTDDateUtvergdenia;
        public System.Windows.Forms.Label NTDID;
        public System.Windows.Forms.NumericUpDown NTDKontrolKolvo;
        public System.Windows.Forms.TextBox NTDRegistN;
        public System.Windows.Forms.NumericUpDown NTDRabKolvo;
        public System.Windows.Forms.CheckBox NTDEditCheckBox;
        public DevExpress.XtraEditors.SimpleButton NTDNewButton;
        public DevExpress.XtraEditors.SimpleButton NTDSaveButton;
        public System.Windows.Forms.TextBox NTDName;
        public System.Windows.Forms.DateTimePicker NTDSrokDeisrvia;
        public System.Windows.Forms.DateTimePicker NTDDateAktualiz;
        public DevExpress.XtraEditors.PanelControl NTDPanel;
        public DevExpress.XtraEditors.PanelControl PersonalPanel;
        public System.Windows.Forms.TextBox PersonalDopolnitelno;
        public System.Windows.Forms.NumericUpDown PersonalOpitNumber;
        public System.Windows.Forms.ComboBox PersonalOpitLet;
        public System.Windows.Forms.TextBox PersonalName;
        public System.Windows.Forms.DateTimePicker PersonalSrokPodvergKvalif;
        public System.Windows.Forms.CheckBox PersonalEditCheckBox;
        public DevExpress.XtraEditors.SimpleButton PersonalNewButton;
        public DevExpress.XtraEditors.SimpleButton PersonalSaveButton;
        public System.Windows.Forms.DateTimePicker PersonalSrokGasDate;
        public System.Windows.Forms.DateTimePicker PersonalGas;
        public System.Windows.Forms.DateTimePicker PersonalAttestechia;
        public System.Windows.Forms.TextBox PersonalDolgnost;
        public System.Windows.Forms.TextBox PersonalObrasovaniePoln;
        public System.Windows.Forms.DateTimePicker PersonalSrokZnaniiOTiPBDate;
        public System.Windows.Forms.DateTimePicker PersonalZnaniaOTiPB;
        public System.Windows.Forms.DateTimePicker PersonalSrokAttestachiiDate;
        public System.Windows.Forms.TextBox PersonalPovishKvalif;
        public System.Windows.Forms.Label PersonalID;
        private System.Windows.Forms.Label label61;
        public DevExpress.XtraEditors.SimpleButton ReactivNewButton;
        public DevExpress.XtraEditors.SimpleButton ReactivSaveButton;
        public System.Windows.Forms.CheckBox ReactivEditCheckBox;
        public System.Windows.Forms.NumericUpDown ReactivKol_vo;
        public System.Windows.Forms.DateTimePicker ReactivDateGet;
        public System.Windows.Forms.TextBox ReactivName;
        public System.Windows.Forms.ComboBox ReactivClass;
        public System.Windows.Forms.TextBox ReactivNPartii;
        public System.Windows.Forms.TextBox ReactivND;
        public System.Windows.Forms.ComboBox ReactivSrokGodnoctiTip;
        public System.Windows.Forms.ComboBox ReactivEdIzm;
        public System.Windows.Forms.DateTimePicker ReactivSrokGodnostiDate;
        public System.Windows.Forms.NumericUpDown ReactivSrokGodnostiMounth;
        public System.Windows.Forms.TextBox ReactivProdlenieSroka;
        public System.Windows.Forms.Label ReactivID;
        public System.Windows.Forms.DateTimePicker ReactivDateIzgot;
        public DevExpress.XtraEditors.SimpleButton ReactivCopyButton;
        public System.Windows.Forms.TextBox ReactivN;
        public DevExpress.XtraEditors.PanelControl ReactivPanel;
        public DevExpress.XtraEditors.SimpleButton PersonalCopyButton;
        public DevExpress.XtraEditors.SimpleButton NTDCopyButton;
        public DevExpress.XtraEditors.SimpleButton SICopyButton;
        public DevExpress.XtraEditors.SimpleButton IOCopyButton;
        public DevExpress.XtraEditors.SimpleButton VOCopyButton;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label51;
        public System.Windows.Forms.TextBox SIInventarNomer;
        public System.Windows.Forms.TextBox SIGod;
        private System.Windows.Forms.Label label92;
        public System.Windows.Forms.TextBox SIZavodNomer;
        public System.Windows.Forms.TextBox SITip;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label95;
        public System.Windows.Forms.TextBox SIN;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        public System.Windows.Forms.TextBox IONomerDokumentaObAttes;
        public System.Windows.Forms.TextBox IOGodVyp;
        private System.Windows.Forms.Label label101;
        public System.Windows.Forms.TextBox IOInventarNomer;
        public System.Windows.Forms.TextBox IOZavodskoy;
        private System.Windows.Forms.Label label102;
        public System.Windows.Forms.TextBox IOTipMarka;
        private System.Windows.Forms.Label label65;
        public System.Windows.Forms.TextBox VOPrimechanie;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        public System.Windows.Forms.TextBox VOInventarNomer;
        public System.Windows.Forms.TextBox VOGodVypuska;
        private System.Windows.Forms.Label label105;
        public System.Windows.Forms.TextBox VOZavodNomer;
        public System.Windows.Forms.TextBox VOTipMarka;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        public System.Windows.Forms.TextBox PersonalObrasovanie;
        private System.Windows.Forms.Label label108;
        public System.Windows.Forms.DateTimePicker SIDataPosledTO;
        public System.Windows.Forms.DateTimePicker SIDataSledTODate;
        public System.Windows.Forms.DateTimePicker IODataINAttestachii;
        public System.Windows.Forms.DateTimePicker IODataPosledTO;
        public System.Windows.Forms.DateTimePicker IODataSledTODate;
        public System.Windows.Forms.DateTimePicker VODataPoslTO;
        public System.Windows.Forms.DateTimePicker VOSledProvTODate;
        public DevExpress.XtraGrid.Views.Grid.GridView NTDGridView;
        private System.Windows.Forms.Label label109;
        public System.Windows.Forms.TextBox PersonalN;
        private System.Windows.Forms.Label label110;
        public System.Windows.Forms.ComboBox IOPeriodich;
        public System.Windows.Forms.ComboBox IOMestoUstanovkiIHranenia;
        public System.Windows.Forms.ComboBox VOMestoUstanovkiIHranenia;
        public System.Windows.Forms.ComboBox SIMestoUstanovki;
        public System.Windows.Forms.ComboBox SISrokDeystv;
        public System.Windows.Forms.ComboBox NTDRabMesto;
        public System.Windows.Forms.ComboBox NTDKontrolMesto;
        public System.Windows.Forms.ComboBox PersonalSrokGasTip;
        public System.Windows.Forms.NumericUpDown PersonalSrokZnaniiOTiPBKol;
        public System.Windows.Forms.NumericUpDown PersonalSrokAttestachiiKol;
        public System.Windows.Forms.NumericUpDown PersonalSrokGasKol;
        public System.Windows.Forms.ComboBox PersonalSrokZnaniiOTiPBTip;
        public System.Windows.Forms.ComboBox PersonalSrokAttestachiiTip;
        public System.Windows.Forms.ComboBox SIDataSledTOTip;
        public System.Windows.Forms.ComboBox SIDateSledPoverkiTip;
        public System.Windows.Forms.NumericUpDown SIDataSledTOKol;
        public System.Windows.Forms.NumericUpDown SIDateSledPoverkiKol;
        public System.Windows.Forms.NumericUpDown IODateSledAttistachiiKol;
        public System.Windows.Forms.ComboBox IODateSledAttistachiiTip;
        public System.Windows.Forms.NumericUpDown VODateSledProverkiKol;
        public System.Windows.Forms.ComboBox VODateSledProverkiTip;
        public System.Windows.Forms.ComboBox IODataSledTOTip;
        public System.Windows.Forms.NumericUpDown IODataSledTOKol;
        public System.Windows.Forms.NumericUpDown VOSledProvTOKol;
        public System.Windows.Forms.ComboBox VOSledProvTOTip;
        private System.Windows.Forms.Label label111;
        public System.Windows.Forms.TextBox NTDPrimechanie;
        public DevExpress.XtraGrid.GridControl GSOGrid;
        private DevExpress.XtraTab.XtraTabPage MainGSOTab;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        public System.Windows.Forms.ComboBox ReactivNaznachenie;
        public System.Windows.Forms.ComboBox NTDGroup;
        public System.Windows.Forms.ComboBox NTDUroven;
        public System.Windows.Forms.ComboBox NTDN;
        public System.Windows.Forms.ComboBox NTDND1;
        public System.Windows.Forms.TextBox NTDND2;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label130;
        private DevExpress.XtraTab.XtraTabPage Nastroiki;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.ComboBox SkinComboBox;
        private System.Windows.Forms.CheckBox UseSkinsCheckBox;
        private DevExpress.XtraEditors.SimpleButton AddUser;
        private DevExpress.XtraEditors.SimpleButton NTDFiltrButton;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar1;
        private System.Windows.Forms.ToolStripMenuItem PlusMinus;
        public System.Windows.Forms.ContextMenuStrip ContextMenuStripMain;
        private System.Windows.Forms.ToolStripMenuItem Copy;
        private System.Windows.Forms.ToolStripMenuItem Paste;
        private System.Windows.Forms.ToolStripMenuItem Clear;
        private System.Windows.Forms.ToolStripMenuItem Paste0;
        private System.Windows.Forms.ToolStripMenuItem Paste2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem Paste3;
        private System.Windows.Forms.ToolStripMenuItem PasteProcent;
        private System.Windows.Forms.ToolStripMenuItem PasteGradus;
        private System.Windows.Forms.ToolStripMenuItem PasteFaringeit;
        private System.Windows.Forms.ToolStripMenuItem Pasteмм2с2;
        private System.Windows.Forms.ToolStripMenuItem PasteКгм3;
        private System.Windows.Forms.ToolStripMenuItem Pasteмгдм3;
        private System.Windows.Forms.ToolStripMenuItem Pasteмгм3;
        private System.Windows.Forms.ToolStripMenuItem Pastecм3;
        private System.Windows.Forms.ToolStripMenuItem Pasteдм3;
        private System.Windows.Forms.ToolStripMenuItem Pasteм3;
        private System.Windows.Forms.ToolStripMenuItem PasteмкСмсм;
        private System.Windows.Forms.ToolStripMenuItem Pasteмкгг;
        private System.Windows.Forms.ToolStripMenuItem Pasteppm;
        private System.Windows.Forms.ToolStripMenuItem PasteрН;
        private System.Windows.Forms.ToolStripMenuItem Cut;
        public DevExpress.XtraWaitForm.ProgressPanel Loader;
        private System.Windows.Forms.ToolStripMenuItem Pasteмм2с;
        internal DevExpress.XtraEditors.SimpleButton IOArhivButton;
        internal DevExpress.XtraEditors.SimpleButton PersonalUvolenButton;
        internal DevExpress.XtraEditors.SimpleButton ReactivVisibleSpisatButton;
        internal DevExpress.XtraTab.XtraTabControl TabControlMain;
        internal DevExpress.XtraEditors.SimpleButton NTDArhivButton;
        internal DevExpress.XtraEditors.SimpleButton SIArhivButton;
        internal DevExpress.XtraEditors.SimpleButton VOArhivButton;
        public System.Windows.Forms.ComboBox SIDataPosledTOTip;
        public System.Windows.Forms.ComboBox SIDataPoverkiTip;
        public System.Windows.Forms.ComboBox IODataINAttestachiiTip;
        public System.Windows.Forms.ComboBox IODataPosledTOTip;
        public System.Windows.Forms.ComboBox VODataPoslTOTip;
        public System.Windows.Forms.ComboBox VODateProverkiTehnicheskogoSostoianiaTip;
        public System.Windows.Forms.ComboBox ReactivDateGetTip;
        public System.Windows.Forms.ComboBox ReactivDateIzgotTip;
        public System.Windows.Forms.ComboBox PersonalZnaniaOTiPBTip;
        public System.Windows.Forms.ComboBox PersonalAttestechiaTip;
        public System.Windows.Forms.ComboBox PersonalGasTip;
        public System.Windows.Forms.ComboBox PersonalSrokPodvergKvalifTip;
        public System.Windows.Forms.ComboBox NTDSrokDeisrviaTip;
        public System.Windows.Forms.ComboBox NTDDateAktualizTip;
        public System.Windows.Forms.ComboBox NTDDateUtvergdeniaTip;
        public System.Windows.Forms.TextBox SIDataPoverki;
        public DevExpress.XtraTab.XtraTabControl GSOSpisokTab;
        internal DevExpress.XtraEditors.SimpleButton AddAnalizProbiButton;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridView GSOGridView;
        internal DevExpress.XtraEditors.SimpleButton VLKPrintButton;
        private DevExpress.XtraEditors.SimpleButton VLKUpdateButton;
        internal DevExpress.XtraEditors.SimpleButton DiagramButton;
        internal DevExpress.XtraEditors.SimpleButton ReactivCellMergeButton;
        internal DevExpress.XtraEditors.SimpleButton PersonalCellMergeButton;
        internal DevExpress.XtraEditors.SimpleButton NTDCellMergeButton;
        internal DevExpress.XtraEditors.SimpleButton SICellMergeButton;
        internal DevExpress.XtraEditors.SimpleButton IOCellMergeButton;
        internal DevExpress.XtraEditors.SimpleButton VOCellMergeButton;
        internal DevExpress.XtraEditors.SimpleButton VLKCellMergeButton;
        internal System.Windows.Forms.TextBox ReactivSrokGodnostiText;
        internal DevExpress.XtraEditors.SimpleButton CloseAllTabsButton;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        internal DevExpress.XtraEditors.SimpleButton FiltrButton;
        public System.Windows.Forms.ComboBox SIPravoSobstvennosti;
        private System.Windows.Forms.Label label112;
        public System.Windows.Forms.ComboBox IOPravoSobstvennosti;
        private System.Windows.Forms.Label label113;
        public System.Windows.Forms.ComboBox VOPravoSobstvennosti;
        private System.Windows.Forms.Label label114;
    }
}

