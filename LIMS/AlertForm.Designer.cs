﻿namespace LIMS
{
    partial class AlertForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlertForm));
            this.MessageText = new System.Windows.Forms.Label();
            this.DateComboBox = new System.Windows.Forms.ComboBox();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl = new DevExpress.XtraEditors.PanelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl)).BeginInit();
            this.panelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // MessageText
            // 
            this.MessageText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MessageText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MessageText.Location = new System.Drawing.Point(85, 25);
            this.MessageText.Name = "MessageText";
            this.MessageText.Size = new System.Drawing.Size(303, 74);
            this.MessageText.TabIndex = 0;
            this.MessageText.Text = "Тут должен быть текст";
            this.MessageText.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MessageText_MouseDown);
            this.MessageText.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MessageText_MouseMove);
            // 
            // DateComboBox
            // 
            this.DateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DateComboBox.Items.AddRange(new object[] {
            "Напомнить через...",
            "1 день",
            "5 дней",
            "15 дней",
            "1 месяц",
            "2 месяца",
            "3 месяца",
            "Отключить оповещения до след. входа"});
            this.DateComboBox.Location = new System.Drawing.Point(85, 103);
            this.DateComboBox.MaxDropDownItems = 20;
            this.DateComboBox.Name = "DateComboBox";
            this.DateComboBox.Size = new System.Drawing.Size(224, 21);
            this.DateComboBox.TabIndex = 1;
            this.DateComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateComboBox_KeyDown);
            this.DateComboBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DateComboBox_MouseClick);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(315, 102);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 2;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton1_Click);
            // 
            // panelControl
            // 
            this.panelControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl.Controls.Add(this.label1);
            this.panelControl.Controls.Add(this.PictureBox1);
            this.panelControl.Controls.Add(this.OKButton);
            this.panelControl.Controls.Add(this.MessageText);
            this.panelControl.Controls.Add(this.DateComboBox);
            this.panelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl.Location = new System.Drawing.Point(0, 0);
            this.panelControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl.Name = "panelControl";
            this.panelControl.Size = new System.Drawing.Size(400, 135);
            this.panelControl.TabIndex = 3;
            this.panelControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelControl_MouseDown);
            this.panelControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelControl_MouseMove);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(83, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(303, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Внимание!";
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            this.label1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label1_MouseMove);
            // 
            // PictureBox1
            // 
            this.PictureBox1.Image = global::LIMS.Properties.Resources._29821_1;
            this.PictureBox1.InitialImage = global::LIMS.Properties.Resources._29821_1;
            this.PictureBox1.Location = new System.Drawing.Point(5, 32);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(74, 71);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox1.TabIndex = 3;
            this.PictureBox1.TabStop = false;
            this.PictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBox1_MouseDown);
            this.PictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBox1_MouseMove);
            // 
            // AlertForm
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 135);
            this.Controls.Add(this.panelControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AlertForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.AlertForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AlertForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl)).EndInit();
            this.panelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label MessageText;
        private System.Windows.Forms.ComboBox DateComboBox;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.PanelControl panelControl;
        private System.Windows.Forms.PictureBox PictureBox1;
        private System.Windows.Forms.Label label1;
    }
}