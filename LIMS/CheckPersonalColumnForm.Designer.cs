﻿namespace LIMS
{
    partial class CheckPersonalColumnForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckPersonalColumnForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.CheckEditDopolnitelno = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditSrokPodvershKvalif = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditPovishKvalif = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditN = new DevExpress.XtraEditors.CheckEdit();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.CancelButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEditSrokProverkiOTiPB = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditProverkaOTiPB = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditSrokAttestachii = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditAttestachia = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditSrokGas = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditGas = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditOpit = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditObrazovaniePolnoe = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditObrazovanie = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDolshnost = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditName = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDopolnitelno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokPodvershKvalif.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPovishKvalif.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokProverkiOTiPB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditProverkaOTiPB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokAttestachii.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditAttestachia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokGas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditOpit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditObrazovaniePolnoe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditObrazovanie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDolshnost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.CheckEditDopolnitelno);
            this.panelControl1.Controls.Add(this.CheckEditSrokPodvershKvalif);
            this.panelControl1.Controls.Add(this.CheckEditPovishKvalif);
            this.panelControl1.Controls.Add(this.CheckEditN);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelButton1);
            this.panelControl1.Controls.Add(this.CheckEditSrokProverkiOTiPB);
            this.panelControl1.Controls.Add(this.CheckEditProverkaOTiPB);
            this.panelControl1.Controls.Add(this.CheckEditSrokAttestachii);
            this.panelControl1.Controls.Add(this.CheckEditAttestachia);
            this.panelControl1.Controls.Add(this.CheckEditSrokGas);
            this.panelControl1.Controls.Add(this.CheckEditGas);
            this.panelControl1.Controls.Add(this.CheckEditOpit);
            this.panelControl1.Controls.Add(this.CheckEditObrazovaniePolnoe);
            this.panelControl1.Controls.Add(this.CheckEditObrazovanie);
            this.panelControl1.Controls.Add(this.CheckEditDolshnost);
            this.panelControl1.Controls.Add(this.CheckEditName);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(557, 219);
            this.panelControl1.TabIndex = 0;
            // 
            // CheckEditDopolnitelno
            // 
            this.CheckEditDopolnitelno.EditValue = true;
            this.CheckEditDopolnitelno.Location = new System.Drawing.Point(233, 162);
            this.CheckEditDopolnitelno.Name = "CheckEditDopolnitelno";
            this.CheckEditDopolnitelno.Properties.Caption = "Дополнительно";
            this.CheckEditDopolnitelno.Size = new System.Drawing.Size(220, 19);
            this.CheckEditDopolnitelno.TabIndex = 15;
            this.CheckEditDopolnitelno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditSrokPodvershKvalif
            // 
            this.CheckEditSrokPodvershKvalif.EditValue = true;
            this.CheckEditSrokPodvershKvalif.Location = new System.Drawing.Point(12, 187);
            this.CheckEditSrokPodvershKvalif.Name = "CheckEditSrokPodvershKvalif";
            this.CheckEditSrokPodvershKvalif.Properties.Caption = "Срок подтверж. (повыш.) квалиф.";
            this.CheckEditSrokPodvershKvalif.Size = new System.Drawing.Size(217, 19);
            this.CheckEditSrokPodvershKvalif.TabIndex = 8;
            this.CheckEditSrokPodvershKvalif.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditPovishKvalif
            // 
            this.CheckEditPovishKvalif.EditValue = true;
            this.CheckEditPovishKvalif.Location = new System.Drawing.Point(12, 162);
            this.CheckEditPovishKvalif.Name = "CheckEditPovishKvalif";
            this.CheckEditPovishKvalif.Properties.Caption = "Повыш. квалификации по осн. работе";
            this.CheckEditPovishKvalif.Size = new System.Drawing.Size(217, 19);
            this.CheckEditPovishKvalif.TabIndex = 7;
            this.CheckEditPovishKvalif.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditN
            // 
            this.CheckEditN.EditValue = true;
            this.CheckEditN.Location = new System.Drawing.Point(12, 12);
            this.CheckEditN.Name = "CheckEditN";
            this.CheckEditN.Properties.Caption = "№ п/п";
            this.CheckEditN.Size = new System.Drawing.Size(112, 19);
            this.CheckEditN.TabIndex = 1;
            this.CheckEditN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(297, 184);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 16;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton1
            // 
            this.CancelButton1.Location = new System.Drawing.Point(378, 184);
            this.CancelButton1.Name = "CancelButton1";
            this.CancelButton1.Size = new System.Drawing.Size(75, 23);
            this.CancelButton1.TabIndex = 17;
            this.CancelButton1.Text = "Отмена";
            this.CancelButton1.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CheckEditSrokProverkiOTiPB
            // 
            this.CheckEditSrokProverkiOTiPB.EditValue = true;
            this.CheckEditSrokProverkiOTiPB.Location = new System.Drawing.Point(233, 137);
            this.CheckEditSrokProverkiOTiPB.Name = "CheckEditSrokProverkiOTiPB";
            this.CheckEditSrokProverkiOTiPB.Properties.Caption = "Срок проверки знаний по ОТ и ПБ";
            this.CheckEditSrokProverkiOTiPB.Size = new System.Drawing.Size(220, 19);
            this.CheckEditSrokProverkiOTiPB.TabIndex = 14;
            this.CheckEditSrokProverkiOTiPB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditProverkaOTiPB
            // 
            this.CheckEditProverkaOTiPB.EditValue = true;
            this.CheckEditProverkaOTiPB.Location = new System.Drawing.Point(233, 112);
            this.CheckEditProverkaOTiPB.Name = "CheckEditProverkaOTiPB";
            this.CheckEditProverkaOTiPB.Properties.Caption = "Проверка знаний по ОТ и ПБ";
            this.CheckEditProverkaOTiPB.Size = new System.Drawing.Size(220, 19);
            this.CheckEditProverkaOTiPB.TabIndex = 13;
            this.CheckEditProverkaOTiPB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditSrokAttestachii
            // 
            this.CheckEditSrokAttestachii.EditValue = true;
            this.CheckEditSrokAttestachii.Location = new System.Drawing.Point(233, 87);
            this.CheckEditSrokAttestachii.Name = "CheckEditSrokAttestachii";
            this.CheckEditSrokAttestachii.Properties.Caption = "Срок аттестации";
            this.CheckEditSrokAttestachii.Size = new System.Drawing.Size(109, 19);
            this.CheckEditSrokAttestachii.TabIndex = 12;
            this.CheckEditSrokAttestachii.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditAttestachia
            // 
            this.CheckEditAttestachia.EditValue = true;
            this.CheckEditAttestachia.Location = new System.Drawing.Point(233, 62);
            this.CheckEditAttestachia.Name = "CheckEditAttestachia";
            this.CheckEditAttestachia.Properties.Caption = "Аттестация";
            this.CheckEditAttestachia.Size = new System.Drawing.Size(109, 19);
            this.CheckEditAttestachia.TabIndex = 11;
            this.CheckEditAttestachia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditSrokGas
            // 
            this.CheckEditSrokGas.EditValue = true;
            this.CheckEditSrokGas.Location = new System.Drawing.Point(233, 37);
            this.CheckEditSrokGas.Name = "CheckEditSrokGas";
            this.CheckEditSrokGas.Properties.Caption = "Срок подтверж. квалиф. лаборанта-газоанализаторщика";
            this.CheckEditSrokGas.Size = new System.Drawing.Size(319, 19);
            this.CheckEditSrokGas.TabIndex = 10;
            this.CheckEditSrokGas.CheckedChanged += new System.EventHandler(this.CheckEditDateGet_CheckedChanged);
            this.CheckEditSrokGas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditGas
            // 
            this.CheckEditGas.EditValue = true;
            this.CheckEditGas.Location = new System.Drawing.Point(233, 12);
            this.CheckEditGas.Name = "CheckEditGas";
            this.CheckEditGas.Properties.Caption = "Обуч. на лаборанта газоанализаторщика";
            this.CheckEditGas.Size = new System.Drawing.Size(239, 19);
            this.CheckEditGas.TabIndex = 9;
            this.CheckEditGas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditOpit
            // 
            this.CheckEditOpit.EditValue = true;
            this.CheckEditOpit.Location = new System.Drawing.Point(12, 137);
            this.CheckEditOpit.Name = "CheckEditOpit";
            this.CheckEditOpit.Properties.Caption = "Практический опыт";
            this.CheckEditOpit.Size = new System.Drawing.Size(127, 19);
            this.CheckEditOpit.TabIndex = 6;
            this.CheckEditOpit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditObrazovaniePolnoe
            // 
            this.CheckEditObrazovaniePolnoe.EditValue = true;
            this.CheckEditObrazovaniePolnoe.Location = new System.Drawing.Point(12, 112);
            this.CheckEditObrazovaniePolnoe.Name = "CheckEditObrazovaniePolnoe";
            this.CheckEditObrazovaniePolnoe.Properties.Caption = "Образование (полное)";
            this.CheckEditObrazovaniePolnoe.Size = new System.Drawing.Size(145, 19);
            this.CheckEditObrazovaniePolnoe.TabIndex = 5;
            this.CheckEditObrazovaniePolnoe.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditObrazovanie
            // 
            this.CheckEditObrazovanie.EditValue = true;
            this.CheckEditObrazovanie.Location = new System.Drawing.Point(12, 87);
            this.CheckEditObrazovanie.Name = "CheckEditObrazovanie";
            this.CheckEditObrazovanie.Properties.Caption = "Образование";
            this.CheckEditObrazovanie.Size = new System.Drawing.Size(112, 19);
            this.CheckEditObrazovanie.TabIndex = 4;
            this.CheckEditObrazovanie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditDolshnost
            // 
            this.CheckEditDolshnost.EditValue = true;
            this.CheckEditDolshnost.Location = new System.Drawing.Point(12, 62);
            this.CheckEditDolshnost.Name = "CheckEditDolshnost";
            this.CheckEditDolshnost.Properties.Caption = "Должность";
            this.CheckEditDolshnost.Size = new System.Drawing.Size(112, 19);
            this.CheckEditDolshnost.TabIndex = 3;
            this.CheckEditDolshnost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckEditName
            // 
            this.CheckEditName.EditValue = true;
            this.CheckEditName.Location = new System.Drawing.Point(12, 37);
            this.CheckEditName.Name = "CheckEditName";
            this.CheckEditName.Properties.Caption = "ФИО сотрудника";
            this.CheckEditName.Size = new System.Drawing.Size(112, 19);
            this.CheckEditName.TabIndex = 2;
            this.CheckEditName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditN_KeyDown);
            // 
            // CheckPersonalColumnForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 219);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "CheckPersonalColumnForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Персонал";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckPersonalColumnForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDopolnitelno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokPodvershKvalif.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPovishKvalif.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokProverkiOTiPB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditProverkaOTiPB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokAttestachii.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditAttestachia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokGas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditOpit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditObrazovaniePolnoe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditObrazovanie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDolshnost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit CheckEditName;
        private DevExpress.XtraEditors.CheckEdit CheckEditProverkaOTiPB;
        private DevExpress.XtraEditors.CheckEdit CheckEditSrokAttestachii;
        private DevExpress.XtraEditors.CheckEdit CheckEditAttestachia;
        private DevExpress.XtraEditors.CheckEdit CheckEditSrokGas;
        private DevExpress.XtraEditors.CheckEdit CheckEditGas;
        private DevExpress.XtraEditors.CheckEdit CheckEditOpit;
        private DevExpress.XtraEditors.CheckEdit CheckEditObrazovaniePolnoe;
        private DevExpress.XtraEditors.CheckEdit CheckEditObrazovanie;
        private DevExpress.XtraEditors.CheckEdit CheckEditDolshnost;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.SimpleButton CancelButton1;
        private DevExpress.XtraEditors.CheckEdit CheckEditSrokProverkiOTiPB;
        private DevExpress.XtraEditors.CheckEdit CheckEditN;
        private DevExpress.XtraEditors.CheckEdit CheckEditPovishKvalif;
        private DevExpress.XtraEditors.CheckEdit CheckEditSrokPodvershKvalif;
        private DevExpress.XtraEditors.CheckEdit CheckEditDopolnitelno;
    }
}