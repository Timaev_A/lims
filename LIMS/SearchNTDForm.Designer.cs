﻿namespace LIMS
{
    partial class SearchNTDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchNTDForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SrokDeistvia2 = new System.Windows.Forms.DateTimePicker();
            this.SrokDeistvia1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DateAktualizasii2 = new System.Windows.Forms.DateTimePicker();
            this.DateAktualizasii1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DateUtvergdenia2 = new System.Windows.Forms.DateTimePicker();
            this.ClearButton = new DevExpress.XtraEditors.SimpleButton();
            this.Primechanie = new System.Windows.Forms.TextBox();
            this.MestoRab = new System.Windows.Forms.ComboBox();
            this.KolvoRab = new System.Windows.Forms.ComboBox();
            this.KolvoKontrol = new System.Windows.Forms.ComboBox();
            this.MestoKontrol = new System.Windows.Forms.ComboBox();
            this.DateUtvergdenia1 = new System.Windows.Forms.DateTimePicker();
            this.ND = new System.Windows.Forms.TextBox();
            this.N = new System.Windows.Forms.ComboBox();
            this.Group = new System.Windows.Forms.ComboBox();
            this.Uroven = new System.Windows.Forms.ComboBox();
            this.NTDName = new System.Windows.Forms.TextBox();
            this.RegistN = new System.Windows.Forms.TextBox();
            this.CheckEditND = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditGroup = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditUroven = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditN = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditPrimechanie = new DevExpress.XtraEditors.CheckEdit();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.CancelButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEditSrokDeistvia = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateActualiz = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditRabMesto = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditRabKolvo = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditKontrolMesto = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditKontroKolvo = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditDateUtverch = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditRegistN = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEditName = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditUroven.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrimechanie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokDeistvia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateActualiz.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRabMesto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRabKolvo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditKontrolMesto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditKontroKolvo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateUtverch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRegistN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.label5);
            this.panelControl1.Controls.Add(this.label6);
            this.panelControl1.Controls.Add(this.SrokDeistvia2);
            this.panelControl1.Controls.Add(this.SrokDeistvia1);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Controls.Add(this.label4);
            this.panelControl1.Controls.Add(this.DateAktualizasii2);
            this.panelControl1.Controls.Add(this.DateAktualizasii1);
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.DateUtvergdenia2);
            this.panelControl1.Controls.Add(this.ClearButton);
            this.panelControl1.Controls.Add(this.Primechanie);
            this.panelControl1.Controls.Add(this.MestoRab);
            this.panelControl1.Controls.Add(this.KolvoRab);
            this.panelControl1.Controls.Add(this.KolvoKontrol);
            this.panelControl1.Controls.Add(this.MestoKontrol);
            this.panelControl1.Controls.Add(this.DateUtvergdenia1);
            this.panelControl1.Controls.Add(this.ND);
            this.panelControl1.Controls.Add(this.N);
            this.panelControl1.Controls.Add(this.Group);
            this.panelControl1.Controls.Add(this.Uroven);
            this.panelControl1.Controls.Add(this.NTDName);
            this.panelControl1.Controls.Add(this.RegistN);
            this.panelControl1.Controls.Add(this.CheckEditND);
            this.panelControl1.Controls.Add(this.CheckEditGroup);
            this.panelControl1.Controls.Add(this.CheckEditUroven);
            this.panelControl1.Controls.Add(this.CheckEditN);
            this.panelControl1.Controls.Add(this.CheckEditPrimechanie);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelButton1);
            this.panelControl1.Controls.Add(this.CheckEditSrokDeistvia);
            this.panelControl1.Controls.Add(this.CheckEditDateActualiz);
            this.panelControl1.Controls.Add(this.CheckEditRabMesto);
            this.panelControl1.Controls.Add(this.CheckEditRabKolvo);
            this.panelControl1.Controls.Add(this.CheckEditKontrolMesto);
            this.panelControl1.Controls.Add(this.CheckEditKontroKolvo);
            this.panelControl1.Controls.Add(this.CheckEditDateUtverch);
            this.panelControl1.Controls.Add(this.CheckEditRegistN);
            this.panelControl1.Controls.Add(this.CheckEditName);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(588, 413);
            this.panelControl1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(439, 331);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 44;
            this.label5.Text = "до";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(292, 331);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "от";
            // 
            // SrokDeistvia2
            // 
            this.SrokDeistvia2.Enabled = false;
            this.SrokDeistvia2.Location = new System.Drawing.Point(461, 328);
            this.SrokDeistvia2.Name = "SrokDeistvia2";
            this.SrokDeistvia2.Size = new System.Drawing.Size(121, 21);
            this.SrokDeistvia2.TabIndex = 29;
            this.SrokDeistvia2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // SrokDeistvia1
            // 
            this.SrokDeistvia1.Enabled = false;
            this.SrokDeistvia1.Location = new System.Drawing.Point(312, 328);
            this.SrokDeistvia1.Name = "SrokDeistvia1";
            this.SrokDeistvia1.Size = new System.Drawing.Size(121, 21);
            this.SrokDeistvia1.TabIndex = 28;
            this.SrokDeistvia1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(439, 304);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "до";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(292, 304);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "от";
            // 
            // DateAktualizasii2
            // 
            this.DateAktualizasii2.Enabled = false;
            this.DateAktualizasii2.Location = new System.Drawing.Point(461, 301);
            this.DateAktualizasii2.Name = "DateAktualizasii2";
            this.DateAktualizasii2.Size = new System.Drawing.Size(121, 21);
            this.DateAktualizasii2.TabIndex = 26;
            this.DateAktualizasii2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // DateAktualizasii1
            // 
            this.DateAktualizasii1.Enabled = false;
            this.DateAktualizasii1.Location = new System.Drawing.Point(312, 301);
            this.DateAktualizasii1.Name = "DateAktualizasii1";
            this.DateAktualizasii1.Size = new System.Drawing.Size(121, 21);
            this.DateAktualizasii1.TabIndex = 25;
            this.DateAktualizasii1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(439, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 36;
            this.label2.Text = "до";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(292, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "от";
            // 
            // DateUtvergdenia2
            // 
            this.DateUtvergdenia2.Enabled = false;
            this.DateUtvergdenia2.Location = new System.Drawing.Point(461, 167);
            this.DateUtvergdenia2.Name = "DateUtvergdenia2";
            this.DateUtvergdenia2.Size = new System.Drawing.Size(121, 21);
            this.DateUtvergdenia2.TabIndex = 15;
            this.DateUtvergdenia2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(267, 382);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 33;
            this.ClearButton.Text = "Сброс";
            // 
            // Primechanie
            // 
            this.Primechanie.Enabled = false;
            this.Primechanie.Location = new System.Drawing.Point(295, 355);
            this.Primechanie.Name = "Primechanie";
            this.Primechanie.Size = new System.Drawing.Size(287, 21);
            this.Primechanie.TabIndex = 31;
            this.Primechanie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // MestoRab
            // 
            this.MestoRab.Enabled = false;
            this.MestoRab.FormattingEnabled = true;
            this.MestoRab.Location = new System.Drawing.Point(295, 274);
            this.MestoRab.Name = "MestoRab";
            this.MestoRab.Size = new System.Drawing.Size(287, 21);
            this.MestoRab.TabIndex = 23;
            this.MestoRab.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // KolvoRab
            // 
            this.KolvoRab.Enabled = false;
            this.KolvoRab.FormattingEnabled = true;
            this.KolvoRab.Location = new System.Drawing.Point(295, 247);
            this.KolvoRab.Name = "KolvoRab";
            this.KolvoRab.Size = new System.Drawing.Size(287, 21);
            this.KolvoRab.TabIndex = 21;
            this.KolvoRab.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // KolvoKontrol
            // 
            this.KolvoKontrol.Enabled = false;
            this.KolvoKontrol.FormattingEnabled = true;
            this.KolvoKontrol.Location = new System.Drawing.Point(295, 193);
            this.KolvoKontrol.Name = "KolvoKontrol";
            this.KolvoKontrol.Size = new System.Drawing.Size(287, 21);
            this.KolvoKontrol.TabIndex = 17;
            this.KolvoKontrol.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // MestoKontrol
            // 
            this.MestoKontrol.Enabled = false;
            this.MestoKontrol.FormattingEnabled = true;
            this.MestoKontrol.Location = new System.Drawing.Point(295, 220);
            this.MestoKontrol.Name = "MestoKontrol";
            this.MestoKontrol.Size = new System.Drawing.Size(287, 21);
            this.MestoKontrol.TabIndex = 19;
            this.MestoKontrol.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // DateUtvergdenia1
            // 
            this.DateUtvergdenia1.Enabled = false;
            this.DateUtvergdenia1.Location = new System.Drawing.Point(312, 167);
            this.DateUtvergdenia1.Name = "DateUtvergdenia1";
            this.DateUtvergdenia1.Size = new System.Drawing.Size(121, 21);
            this.DateUtvergdenia1.TabIndex = 14;
            this.DateUtvergdenia1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // ND
            // 
            this.ND.Enabled = false;
            this.ND.Location = new System.Drawing.Point(295, 85);
            this.ND.Name = "ND";
            this.ND.Size = new System.Drawing.Size(287, 21);
            this.ND.TabIndex = 8;
            this.ND.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // N
            // 
            this.N.Enabled = false;
            this.N.FormattingEnabled = true;
            this.N.Location = new System.Drawing.Point(295, 59);
            this.N.Name = "N";
            this.N.Size = new System.Drawing.Size(287, 21);
            this.N.TabIndex = 6;
            this.N.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // Group
            // 
            this.Group.Enabled = false;
            this.Group.FormattingEnabled = true;
            this.Group.Location = new System.Drawing.Point(295, 32);
            this.Group.Name = "Group";
            this.Group.Size = new System.Drawing.Size(287, 21);
            this.Group.TabIndex = 4;
            this.Group.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // Uroven
            // 
            this.Uroven.Enabled = false;
            this.Uroven.FormattingEnabled = true;
            this.Uroven.Location = new System.Drawing.Point(295, 5);
            this.Uroven.Name = "Uroven";
            this.Uroven.Size = new System.Drawing.Size(287, 21);
            this.Uroven.TabIndex = 2;
            this.Uroven.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // NTDName
            // 
            this.NTDName.Enabled = false;
            this.NTDName.Location = new System.Drawing.Point(295, 112);
            this.NTDName.Name = "NTDName";
            this.NTDName.Size = new System.Drawing.Size(287, 21);
            this.NTDName.TabIndex = 10;
            this.NTDName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // RegistN
            // 
            this.RegistN.Enabled = false;
            this.RegistN.Location = new System.Drawing.Point(295, 139);
            this.RegistN.Name = "RegistN";
            this.RegistN.Size = new System.Drawing.Size(287, 21);
            this.RegistN.TabIndex = 12;
            this.RegistN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditND
            // 
            this.CheckEditND.Location = new System.Drawing.Point(12, 85);
            this.CheckEditND.Name = "CheckEditND";
            this.CheckEditND.Properties.Caption = "НД";
            this.CheckEditND.Size = new System.Drawing.Size(152, 19);
            this.CheckEditND.TabIndex = 7;
            this.CheckEditND.CheckedChanged += new System.EventHandler(this.CheckEditND_CheckedChanged);
            this.CheckEditND.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditGroup
            // 
            this.CheckEditGroup.Location = new System.Drawing.Point(12, 32);
            this.CheckEditGroup.Name = "CheckEditGroup";
            this.CheckEditGroup.Properties.Caption = "Группа";
            this.CheckEditGroup.Size = new System.Drawing.Size(152, 19);
            this.CheckEditGroup.TabIndex = 3;
            this.CheckEditGroup.CheckedChanged += new System.EventHandler(this.CheckEditGroup_CheckedChanged);
            this.CheckEditGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditUroven
            // 
            this.CheckEditUroven.Location = new System.Drawing.Point(12, 5);
            this.CheckEditUroven.Name = "CheckEditUroven";
            this.CheckEditUroven.Properties.Caption = "Уровень документа";
            this.CheckEditUroven.Size = new System.Drawing.Size(163, 19);
            this.CheckEditUroven.TabIndex = 1;
            this.CheckEditUroven.CheckedChanged += new System.EventHandler(this.CheckEditUroven_CheckedChanged);
            this.CheckEditUroven.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditN
            // 
            this.CheckEditN.Location = new System.Drawing.Point(12, 59);
            this.CheckEditN.Name = "CheckEditN";
            this.CheckEditN.Properties.Caption = "№ п/п";
            this.CheckEditN.Size = new System.Drawing.Size(112, 19);
            this.CheckEditN.TabIndex = 5;
            this.CheckEditN.CheckedChanged += new System.EventHandler(this.CheckEditN_CheckedChanged);
            this.CheckEditN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditPrimechanie
            // 
            this.CheckEditPrimechanie.Location = new System.Drawing.Point(12, 355);
            this.CheckEditPrimechanie.Name = "CheckEditPrimechanie";
            this.CheckEditPrimechanie.Properties.Caption = "Примечание";
            this.CheckEditPrimechanie.Size = new System.Drawing.Size(220, 19);
            this.CheckEditPrimechanie.TabIndex = 30;
            this.CheckEditPrimechanie.CheckedChanged += new System.EventHandler(this.CheckEditPrimechanie_CheckedChanged);
            this.CheckEditPrimechanie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(186, 382);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 32;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton1
            // 
            this.CancelButton1.Location = new System.Drawing.Point(348, 382);
            this.CancelButton1.Name = "CancelButton1";
            this.CancelButton1.Size = new System.Drawing.Size(75, 23);
            this.CancelButton1.TabIndex = 34;
            this.CancelButton1.Text = "Отмена";
            this.CancelButton1.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CheckEditSrokDeistvia
            // 
            this.CheckEditSrokDeistvia.Location = new System.Drawing.Point(12, 329);
            this.CheckEditSrokDeistvia.Name = "CheckEditSrokDeistvia";
            this.CheckEditSrokDeistvia.Properties.Caption = "Срок действия";
            this.CheckEditSrokDeistvia.Size = new System.Drawing.Size(220, 19);
            this.CheckEditSrokDeistvia.TabIndex = 27;
            this.CheckEditSrokDeistvia.CheckedChanged += new System.EventHandler(this.CheckEditSrokDeistvia_CheckedChanged);
            this.CheckEditSrokDeistvia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditDateActualiz
            // 
            this.CheckEditDateActualiz.Location = new System.Drawing.Point(12, 302);
            this.CheckEditDateActualiz.Name = "CheckEditDateActualiz";
            this.CheckEditDateActualiz.Properties.Caption = "Дата последней актуализации";
            this.CheckEditDateActualiz.Size = new System.Drawing.Size(220, 19);
            this.CheckEditDateActualiz.TabIndex = 24;
            this.CheckEditDateActualiz.CheckedChanged += new System.EventHandler(this.CheckEditDateActualiz_CheckedChanged);
            this.CheckEditDateActualiz.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditRabMesto
            // 
            this.CheckEditRabMesto.Location = new System.Drawing.Point(12, 274);
            this.CheckEditRabMesto.Name = "CheckEditRabMesto";
            this.CheckEditRabMesto.Properties.Caption = "Место нахождения (рабочий экземпляр)";
            this.CheckEditRabMesto.Size = new System.Drawing.Size(234, 19);
            this.CheckEditRabMesto.TabIndex = 22;
            this.CheckEditRabMesto.CheckedChanged += new System.EventHandler(this.CheckEditRabMesto_CheckedChanged);
            this.CheckEditRabMesto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditRabKolvo
            // 
            this.CheckEditRabKolvo.Location = new System.Drawing.Point(12, 247);
            this.CheckEditRabKolvo.Name = "CheckEditRabKolvo";
            this.CheckEditRabKolvo.Properties.Caption = "Количество (рабочий экземпляр)";
            this.CheckEditRabKolvo.Size = new System.Drawing.Size(252, 19);
            this.CheckEditRabKolvo.TabIndex = 20;
            this.CheckEditRabKolvo.CheckedChanged += new System.EventHandler(this.CheckEditRabKolvo_CheckedChanged);
            this.CheckEditRabKolvo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditKontrolMesto
            // 
            this.CheckEditKontrolMesto.Location = new System.Drawing.Point(12, 220);
            this.CheckEditKontrolMesto.Name = "CheckEditKontrolMesto";
            this.CheckEditKontrolMesto.Properties.Caption = "Место нахождения (контрольный экземпляр)";
            this.CheckEditKontrolMesto.Size = new System.Drawing.Size(252, 19);
            this.CheckEditKontrolMesto.TabIndex = 18;
            this.CheckEditKontrolMesto.CheckedChanged += new System.EventHandler(this.CheckEditKontrolMesto_CheckedChanged);
            this.CheckEditKontrolMesto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditKontroKolvo
            // 
            this.CheckEditKontroKolvo.Location = new System.Drawing.Point(12, 193);
            this.CheckEditKontroKolvo.Name = "CheckEditKontroKolvo";
            this.CheckEditKontroKolvo.Properties.Caption = "Количество (контрольный экземпляр)";
            this.CheckEditKontroKolvo.Size = new System.Drawing.Size(244, 19);
            this.CheckEditKontroKolvo.TabIndex = 16;
            this.CheckEditKontroKolvo.CheckedChanged += new System.EventHandler(this.CheckEditKontroKolvo_CheckedChanged);
            this.CheckEditKontroKolvo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditDateUtverch
            // 
            this.CheckEditDateUtverch.Location = new System.Drawing.Point(12, 167);
            this.CheckEditDateUtverch.Name = "CheckEditDateUtverch";
            this.CheckEditDateUtverch.Properties.Caption = "Дата утверждения";
            this.CheckEditDateUtverch.Size = new System.Drawing.Size(152, 19);
            this.CheckEditDateUtverch.TabIndex = 13;
            this.CheckEditDateUtverch.CheckedChanged += new System.EventHandler(this.CheckEditDateUtverch_CheckedChanged);
            this.CheckEditDateUtverch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditRegistN
            // 
            this.CheckEditRegistN.Location = new System.Drawing.Point(12, 139);
            this.CheckEditRegistN.Name = "CheckEditRegistN";
            this.CheckEditRegistN.Properties.Caption = "Регистр. № по журн. учёта и регистраций докум.";
            this.CheckEditRegistN.Size = new System.Drawing.Size(277, 19);
            this.CheckEditRegistN.TabIndex = 11;
            this.CheckEditRegistN.CheckedChanged += new System.EventHandler(this.CheckEditRegistN_CheckedChanged);
            this.CheckEditRegistN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // CheckEditName
            // 
            this.CheckEditName.Location = new System.Drawing.Point(12, 112);
            this.CheckEditName.Name = "CheckEditName";
            this.CheckEditName.Properties.Caption = "Наименование документа";
            this.CheckEditName.Size = new System.Drawing.Size(152, 19);
            this.CheckEditName.TabIndex = 9;
            this.CheckEditName.CheckedChanged += new System.EventHandler(this.CheckEditName_CheckedChanged);
            this.CheckEditName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckEditUroven_KeyDown);
            // 
            // SearchNTDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 413);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "SearchNTDForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ НТД Фильтр";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SearchNTDForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SearchNTDForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditUroven.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditPrimechanie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditSrokDeistvia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateActualiz.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRabMesto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRabKolvo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditKontrolMesto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditKontroKolvo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditDateUtverch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditRegistN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEditName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit CheckEditName;
        private DevExpress.XtraEditors.CheckEdit CheckEditSrokDeistvia;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateActualiz;
        private DevExpress.XtraEditors.CheckEdit CheckEditRabMesto;
        private DevExpress.XtraEditors.CheckEdit CheckEditRabKolvo;
        private DevExpress.XtraEditors.CheckEdit CheckEditKontrolMesto;
        private DevExpress.XtraEditors.CheckEdit CheckEditKontroKolvo;
        private DevExpress.XtraEditors.CheckEdit CheckEditDateUtverch;
        private DevExpress.XtraEditors.CheckEdit CheckEditRegistN;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.SimpleButton CancelButton1;
        private DevExpress.XtraEditors.CheckEdit CheckEditPrimechanie;
        private DevExpress.XtraEditors.CheckEdit CheckEditGroup;
        private DevExpress.XtraEditors.CheckEdit CheckEditUroven;
        private DevExpress.XtraEditors.CheckEdit CheckEditN;
        private DevExpress.XtraEditors.CheckEdit CheckEditND;
        private System.Windows.Forms.TextBox NTDName;
        private System.Windows.Forms.TextBox RegistN;
        private System.Windows.Forms.TextBox ND;
        private System.Windows.Forms.ComboBox N;
        private System.Windows.Forms.ComboBox Group;
        private System.Windows.Forms.ComboBox Uroven;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker SrokDeistvia2;
        private System.Windows.Forms.DateTimePicker SrokDeistvia1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker DateAktualizasii2;
        private System.Windows.Forms.DateTimePicker DateAktualizasii1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker DateUtvergdenia2;
        private DevExpress.XtraEditors.SimpleButton ClearButton;
        private System.Windows.Forms.TextBox Primechanie;
        private System.Windows.Forms.ComboBox MestoRab;
        private System.Windows.Forms.ComboBox KolvoRab;
        private System.Windows.Forms.ComboBox KolvoKontrol;
        private System.Windows.Forms.ComboBox MestoKontrol;
        private System.Windows.Forms.DateTimePicker DateUtvergdenia1;
    }
}