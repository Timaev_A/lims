﻿namespace LIMS
{
    partial class ReactivRashodForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReactivRashodForm));
            this.CancelClickButton = new DevExpress.XtraEditors.SimpleButton();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Ot = new System.Windows.Forms.DateTimePicker();
            this.Do = new System.Windows.Forms.DateTimePicker();
            this.DateIzgot = new System.Windows.Forms.Label();
            this.Partia = new System.Windows.Forms.Label();
            this.ReactivName = new System.Windows.Forms.Label();
            this.ID = new System.Windows.Forms.Label();
            this.Kolvo = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Kolvo)).BeginInit();
            this.SuspendLayout();
            // 
            // CancelClickButton
            // 
            this.CancelClickButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelClickButton.Location = new System.Drawing.Point(145, 128);
            this.CancelClickButton.Name = "CancelClickButton";
            this.CancelClickButton.Size = new System.Drawing.Size(75, 26);
            this.CancelClickButton.TabIndex = 5;
            this.CancelClickButton.Text = "Отмена";
            this.CancelClickButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(56, 128);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 26);
            this.OKButton.TabIndex = 4;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.label2);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.Ot);
            this.panelControl1.Controls.Add(this.Do);
            this.panelControl1.Controls.Add(this.DateIzgot);
            this.panelControl1.Controls.Add(this.Partia);
            this.panelControl1.Controls.Add(this.ReactivName);
            this.panelControl1.Controls.Add(this.ID);
            this.panelControl1.Controls.Add(this.Kolvo);
            this.panelControl1.Controls.Add(this.OKButton);
            this.panelControl1.Controls.Add(this.CancelClickButton);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(270, 166);
            this.panelControl1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(138, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "До";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "От";
            // 
            // Ot
            // 
            this.Ot.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Ot.Location = new System.Drawing.Point(36, 69);
            this.Ot.Name = "Ot";
            this.Ot.Size = new System.Drawing.Size(96, 21);
            this.Ot.TabIndex = 1;
            this.Ot.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Ot_KeyDown);
            // 
            // Do
            // 
            this.Do.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Do.Location = new System.Drawing.Point(165, 69);
            this.Do.Name = "Do";
            this.Do.Size = new System.Drawing.Size(96, 21);
            this.Do.TabIndex = 2;
            this.Do.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Ot_KeyDown);
            // 
            // DateIzgot
            // 
            this.DateIzgot.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DateIzgot.Location = new System.Drawing.Point(12, 46);
            this.DateIzgot.Name = "DateIzgot";
            this.DateIzgot.Size = new System.Drawing.Size(246, 16);
            this.DateIzgot.TabIndex = 17;
            this.DateIzgot.Text = "label1";
            this.DateIzgot.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Partia
            // 
            this.Partia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Partia.Location = new System.Drawing.Point(12, 30);
            this.Partia.Name = "Partia";
            this.Partia.Size = new System.Drawing.Size(246, 16);
            this.Partia.TabIndex = 16;
            this.Partia.Text = "Partia";
            this.Partia.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ReactivName
            // 
            this.ReactivName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ReactivName.Location = new System.Drawing.Point(12, 14);
            this.ReactivName.Name = "ReactivName";
            this.ReactivName.Size = new System.Drawing.Size(246, 14);
            this.ReactivName.TabIndex = 15;
            this.ReactivName.Text = "Name";
            this.ReactivName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Location = new System.Drawing.Point(174, 60);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(0, 13);
            this.ID.TabIndex = 1;
            this.ID.Visible = false;
            // 
            // Kolvo
            // 
            this.Kolvo.DecimalPlaces = 3;
            this.Kolvo.Location = new System.Drawing.Point(52, 96);
            this.Kolvo.Maximum = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this.Kolvo.Name = "Kolvo";
            this.Kolvo.Size = new System.Drawing.Size(181, 21);
            this.Kolvo.TabIndex = 3;
            this.Kolvo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Ot_KeyDown);
            // 
            // ReactivRashodForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelClickButton;
            this.ClientSize = new System.Drawing.Size(270, 166);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReactivRashodForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "АРМЛАБ Расход";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReactivRashodForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Kolvo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton CancelClickButton;
        private DevExpress.XtraEditors.SimpleButton OKButton;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.NumericUpDown Kolvo;
        private System.Windows.Forms.Label ID;
        private System.Windows.Forms.Label ReactivName;
        private System.Windows.Forms.Label DateIzgot;
        private System.Windows.Forms.Label Partia;
        private System.Windows.Forms.DateTimePicker Do;
        private System.Windows.Forms.DateTimePicker Ot;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}